<?php
include("admin/conf.php");
require_once('admin/includes/tienda/cart/inc/functions.inc.php');
include("funciones.php");
if(isset($_GET['op'])){
    $op=$_GET['op'];
    } else{
        $op="home";
    }
	//echo listaCliente();
	include("php-html-css-js-minifier.php");
include("includes/cookies.php");

if(isset($_COOKIE['usuario_id'])){
    $cliente = consulta_bd("nombre, tipo_registro, nombre_empresa, parent, email, telefono, rut","clientes","id = ".$_COOKIE['usuario_id'],"");
    $usuarioID = $_COOKIE['usuario_id'];
    $usuarioNombre = $cliente[0][0];
    $usuarioTelefono = $cliente[0][5];
    $usuarioRut = $cliente[0][6];
    $usuarioTipo = $cliente[0][1];
    $usuarioNombreEmpresa = $cliente[0][2];
    $usuarioParent = $cliente[0][3];
    $usuario = tipoCliente($usuarioID);
    $listaPrecio = $usuario['idLista'];
    $usuarioEmail = $cliente[0][4];
} else {
    $listaPrecio = 3;
}

if(isset($_COOKIE['Vendedor_id'])){
    $vendedor = consulta_bd("nombre, email, telefono, rut, apellido, datos_orden, forma_pago, datos_banco","vendedores","id = ".$_COOKIE['Vendedor_id'],"");
    $vendedorID = $_COOKIE['Vendedor_id'];
    $vendedorNombre = $vendedor[0][0];
    $vendedorApellido = $vendedor[0][4];
    $vendedorEmail = $vendedor[0][1];
    $vendedorTelefono = $vendedor[0][2];
    $vendedorRut = $vendedor[0][3];
    $datosOrden = $vendedor[0][5];
    $formaPago = $vendedor[0][6];
    $datosBanco = $vendedor[0][7];    
} 
?>

<?php 
	ob_start(); # apertura de bufer
	include("includes/head.php");
	$head = ob_get_contents();
	ob_end_clean(); # cierre de bufer
	echo minify_html($head);
?>



<body>
<div class="cont_loading">
	<div class="lds-dual-ring"></div>
</div>
    <?php 
        ob_start(); # apertura de bufer
        include("includes/popUpTraking.php");
        $popUpTraking = ob_get_contents();
        ob_end_clean(); # cierre de bufer
        echo minify_html($popUpTraking);
    ?> 
    
    
<div id='popUp'></div>
	<?php 
		
        if($op == "carro-cotizacion"){
            ob_start(); # apertura de bufer
            include("includes/headerCarro.php");
            $header = ob_get_contents();
            ob_end_clean(); # cierre de bufer
            echo minify_html($header);
        }elseif($op == "envio-y-pago" or $op == "resumen"){
            $header = ob_get_contents();
            ob_end_clean(); # cierre de bufer
            echo minify_html($header);
        } else {
            ob_start(); # apertura de bufer
            include("includes/header.php");
            $header = ob_get_contents();
            ob_end_clean(); # cierre de bufer
            echo minify_html($header);
        }
       ob_start(); # apertura de bufer
        include("pags/$op.php");
        $pags = ob_get_contents();
        ob_end_clean(); # cierre de bufer
        echo minify_html($pags);
		
		ob_start(); # apertura de bufer
        include("includes/footer.php");
        $footer = ob_get_contents();
        ob_end_clean(); # cierre de bufer
        echo minify_html($footer);
    ?>
 
<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.uniform.standalone.js"></script>
<script type="text/javascript">
	$(function() {
		$("select, input:checkbox, input:radio").uniform();
	}); 
</script>

<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
<script src="js/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	$(function(){
		$(".editarDireccion").fancybox({
			maxWidth	: 800,
			maxHeight	: 600,
			fitToView	: false,
			width		: '70%',
			height		: '70%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
		$("#nuevaDireccion").fancybox({
			'titlePosition'		: 'inside',
			'transitionIn'		: 'none',
			'transitionOut'		: 'none'
			});
		$(".fancybox").fancybox({
			'titlePosition'		: 'inside',
			'transitionIn'		: 'none',
			'transitionOut'		: 'none'
			});
		
		});
	
</script>   
	<script type="text/javascript" src="js/jquery.Rut.js"></script>
	<script type="text/javascript" src="admin/js/jquery.numeric.js"></script>
    <script type="text/javascript">$(function(){$("#telefono, #phone, #telefono_retiro, #cantidad, #cantm2, #telefonoCotizar").numeric();});</script>
    <script type="text/javascript" src="js/validacionCompraRapida.js"></script>
    <script type="text/javascript" src="js/formularios.js"></script>
    <script type="text/javascript" src="js/agrega_desde_ficha.js"></script>
    <script type="text/javascript" src="js/cotiza_desde_ficha.js"></script>
    <script type="text/javascript" src="js/codigoDescuento.js"></script>
         
		<!-- $codigoDescuento = file_get_contents('js/codigoDescuento.js');
		echo '<script>';
		echo minify_css($codigoDescuento);
		echo '</script>'; -->
	<script type="text/javascript" src="js/js.cookie.min.js"></script>
    <script type="text/javascript" src="js/funciones.js"></script>
    <script type="text/javascript">function cerrar(){ $(".fondoPopUp").remove();} </script>
    <script type="text/javascript" src="js/agrega_quita_elimina_ShowCart.js"></script>
	<script type="text/javascript" src="js/agrega_quita_elimina_ShowCartCotizacion.js"></script>
    <script type="text/javascript" src="js/lista_productos.js"></script>
    <script type="text/javascript" src="js/validacionCotizacion.js"></script>
    
    <?php if($op == "ficha"){?><script type="text/javascript" src="js/fixedFicha.js"></script><?php } ?>
    <?php if($op == "carro-resumen"){?>
		<script type="text/javascript" src="js/fixedCarro1.js"></script>
        <script type="text/javascript" src="js/cambiosDePrecio.js"></script>
	<?php } ?>
    <?php 
    if (isset($_GET["error"])) {
        $msj = $_GET["error"];
        echo"<script>
            Swal.fire({ icon: 'error', title: 'Oops...', text: '$msj!' });
        </script>";
    }
    if (isset($_GET["ok"])) {
        $msj = $_GET["ok"];
        echo"<script>
            Swal.fire({ icon: 'success', title: 'Exito!', text: '$msj!' });
        </script>";
    }
    
    if ($_GET['msje']) {
		if($_GET['a'] == 1){$type = "success";} else {$type = "error";}
		?>
	<script type="text/javascript">
        $(function() { 
				Swal.fire('','<?= $_GET['msje'];?>','<?= $type; ?>'); 
			});
    </script>     
    <?php } ?>
   <script type="text/javascript">ga('send', 'pageview');</script> 
   
  <?php /*?> 
    <!--<div class="cont100">
        <div class="cont100Centro">
            <?php 
                echo '<pre>';
                print_r($_SESSION);
                echo '</pre>';
            ?>
        </div>
    </div>
    <div class="cont100">
        <div class="cont100Centro">
            <?php 
                echo '<pre>';
                print_r($_COOKIE);
                echo '</pre>';
            ?>
        </div>
    </div>-->
    <?php */ ?> 
    <div style="clear:both;"></div>
</body>
</html>
<?php mysqli_close($conexion);?>