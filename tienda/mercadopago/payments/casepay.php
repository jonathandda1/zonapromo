<?php 
session_start();

include('../../../admin/conf.php');
require_once "../lib/mercadopago.php";
include '../../../Mailin.php';
include('../../../admin/includes/tienda/cart/inc/functions.inc.php');

include('../claves.php');


// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


date_default_timezone_set('America/Santiago');

if (!$_GET) {
    header('Location: ../index.php');
}else{

    $oc = mysqli_real_escape_string($conexion, $_GET['oc']);
    $case = mysqli_real_escape_string($conexion, $_GET['case']);
    $cod_descuento = mysqli_real_escape_string($conexion, $_GET['cod']);
    $pedidoIdMeli = mysqli_real_escape_string($conexion, $_GET['collection_id']);

    $fecha_hoy = date("Y-m-d H:i:s", time());
    $fecha = date("Y-m-d H:i:s", time());

    //die("$oc");


    try {
        $mp = new MP($keys['client_id'], $keys['client_secret']);

        $payment_info = $mp->get_payment_info($pedidoIdMeli);
        $payment = $payment_info["response"]["collection"];

        $id_compra = $payment['id'];
        $total = (int)$payment["total_paid_amount"];
        $tipo_pago = $payment["payment_type"];
        $metodo_pago = $payment["payment_method_id"];
        $cantidad_cuotas = (int)$payment["installments"];
        $valor_cuotas = (int)$payment["installment_amount"];
        $cod_auth = $payment['authorization_code'];
        $card_number = $payment['last_four_digits'];
        $fecha = $payment["date_created"];
        $estado_id = 0;

        if ($payment_info["status"] == 200) {
            if ($payment["status"] == 'approved') {
                $estado_id = 2; 
            }elseif ($payment["status"] == 'pending' || $payment["status"] == 'in_process') {
                $estado_id = 1;
            }elseif ($payment["status"] == 'cancelled' || $payment["status"] == 'rejected') {
                $estado_id = 3;
            }

            $payment_status = $payment["status"];

            update_bd("pedidos","estado_id = $estado_id, mp_payment_type = '$tipo_pago', mp_payment_method = '$metodo_pago', mp_auth_code = '$cod_auth', mp_paid_amount = $total, mp_card_number = '$card_number', mp_id_paid = '$id_compra', mp_cuotas = $cantidad_cuotas, mp_valor_cuotas = $valor_cuotas, mp_transaction_date = '$fecha'","oc='$oc'");

            $id_pedido = consulta_bd("id, retiro_en_tienda","pedidos","oc='$oc'","");


            if ($estado_id == 2) {
                //funciones para avisar al cliente y al administrador de la venta 
                // Se ejecutan con la funcion exec para agilizar el proceso de carga y que el envío de correo pase a segundo plano.

                // if ($id_pedido[0][1] == 0) {
                //     exec("php -f ../../../ajax/postEnviame.php {$id_pedido[0][0]}", $output, $return_var);
                // }else{
                //     update_bd('pedidos', "estado_envio = 'Retiro en tienda'", "oc = '$oc'");
                // }

                enviarComprobanteCliente($oc);
                enviarComprobanteAdmin($oc,"","");

                $reducirStock = consulta_bd("pp.productos_detalle_id, pp.cantidad","productos_pedidos pp join pedidos p on p.id = pp.pedido_id","p.oc = '$oc'","");
                for($i=0; $i<sizeof($reducirStock); $i++){
                  $cantActual = update_bd("productos_detalles", "stock = (stock - {$reducirStock[$i][1]})","id=".$reducirStock[$i][0]);
                }

                //si existe codigo de descuento, lo descuento del total y elimino la sesion

                // if($cod_descuento != null){
                //     $codigoActual = consulta_bd("oc","codigo_descuento","codigo = '$cod_descuento'","");
                //     $ocActualizada = $codigoActual[0][0].$oc.',';
                //     //var_dump($codigoActual);

                //     $desactivoCodigo = update_bd("codigo_descuento","fecha_modificacion = NOW(),fecha_uso = NOW(), oc = '$ocActualizada', usados = usados +1","codigo = '$cod_descuento'");
                // }

                //elimino la cookie del carro de compras
                setcookie('cart_alfa_cm', null, -1, '/');

                $mensaje_cron = "Notificación pedido casepay $oc | Id pago mercadopago: $id_compra | Estado: $payment_status";
            }elseif($estado_id == 1){
                $mensaje_cron = "Notificación pedido casepay $oc | Estado: pendiente";
            }elseif($estado_id == 3){
                $mensaje_cron = "Notificación pedido casepay $oc | Estado: rechazado";
            }
            
            // echo $payment["status"];

        }else{
            $mensaje_cron = "Notificación pedido $oc | !status 200";
        }

        //$insert = insert_bd('cron_logs', "cron, fecha", "'$mensaje_cron', '$fecha'");

    } catch (Exception $e) {

        $msje = array(

            'status' => 'error',
            'message' => $e->getMessage(),
            'oc' => $oc,
            'meli_id' => $pedidoIdMeli

        );

        $msje = json_encode($msje);

        // var_dump($msje);

        //insert_bd('cron_logs', 'cron, fecha', "'Error casepay mpago: $msje', '$fecha_hoy'");
    }


     if ($oc != null) {
        switch ($case) {
            case 'success':
                header('Location: '.$url_base.'exito?oc='.$oc);
                die();
            break;
            case 'failure':
                header('Location: '.$url_base.'fracaso?oc='.$oc);
                die();
            break;
            case 'pending':
                header('Location: '.$url_base.'fracaso?oc='.$oc);
                die();
            break;
        }
    } 
    else{
        header('Location: ../index.php');
    }
}
?>