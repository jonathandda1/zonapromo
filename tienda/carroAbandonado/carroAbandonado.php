<?php
require_once('../../PHPMailer/PHPMailerAutoload.php');
session_start();  
function update_bd($table, $values, $conditions){
		$conexion = $GLOBALS['conexion'];
		$sql = "UPDATE $table SET $values ";
		if ($conditions != ''){
			$sql .= "WHERE $conditions";
		}
		
		$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion)."<br /><br />$sql");;
		if ($run){
			return true;
		} else {
			return false;
		}
	}
	
function consulta_bd($campos, $table, $conditions, $orden){
		$conexion = $GLOBALS['conexion'];
		if ($campos == '*'){
			$sql0 = "SHOW columns FROM $table";
			$run0 = mysqli_query($conexion, $sql0);
			$cant_columnas = mysqli_affected_rows($conexion);
		} else {
			$columnas = explode(",",$campos);
			$cant_columnas = count($columnas);
		}
		
		$sql = "SELECT $campos FROM $table";
		if ($conditions != ''){
			$sql .= " WHERE $conditions";
		}
		if ($orden != ''){
			$sql .= " ORDER BY $orden";
		}
		
		if (!mysqli_query($conexion, $sql)){
			//" . mysqli_error($conexion).";
		  die("Error description: " . mysqli_error($conexion)."<br /><br />$sql");
		}
  
  		$run = mysqli_query($conexion, $sql);
		$i = 0;
		while($line = mysqli_fetch_array($run)){
			$j = 0;
			while($j <= $cant_columnas){
				$resulset[$i][$j] = stripslashes($line[$j]);
				$j++;	
			}
			$i++;
		}
		return $resulset;
	}
	function opciones($campo){
		$opciones = consulta_bd("valor","opciones","nombre = '$campo'","");
		$valor = $opciones[0][0];
		return $valor;	
		}


	$die_start = "<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Error de base de datos:</span> <br /><br />";
	$die_end = "</p>";

	//Por lo general no cambiar.
	$host = '127.0.0.1';
	//Usuario de la base de datos.
	$user = 'root';	
	//Paswrod del usuario.
	$pass = '';	
	//Base de datos a trabajar.
	$base = 'pruebas';
	$conexion = mysqli_connect("$host", "$user", "$pass", "$base");
	if (!$conexion) {
		echo "Error: No se pudo establecer una conexion con la base de datos:." . PHP_EOL;
		echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
		exit;
	} else {
		//echo "Éxito: Se realizó una conexión apropiada a MySQL! La base de datos mi_bd es genial." . PHP_EOL;
		//echo "Información del host: " . mysqli_get_host_info($conexion) . PHP_EOL;
	}

	mysqli_query ($conexion, "SET NAMES 'utf8'");



$fecha = date('Y-m-j H:i:s');
$nuevafecha = strtotime ( '-2 hour' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'Y-m-j H:i:s' , $nuevafecha );

$comprasAbandonadas = consulta_bd("DISTINCT(correo), productos, oc","carros_abandonados","fecha < '$nuevafecha' and (fecha_notificacion = '' || fecha_notificacion is NULL)","");
//die();
for($i=0; $i<sizeof($comprasAbandonadas); $i++){
	$ocConsulta = $comprasAbandonadas[$i][2];
    $procesadas = consulta_bd("estado_id","pedidos","oc = '$ocConsulta'","");
	$cantPedidos = mysqli_affected_rows($conexion);
	if($cantPedidos > 0){
		if($procesadas[0][0] != 2){
				//carro abandonado llego a pedidos pero quedo con estado rechazado o pendiente
				enviarCompraAbandonada($comprasAbandonadas[$i][1], $comprasAbandonadas[$i][0]);
                echo $comprasAbandonadas[$i][0]."<br/>";
			} else {
				//pedido fue cancelado, por ende no hago nada
			}
		} else {
			//carro abandonado no esta relacionado con ninguna oc
			enviarCompraAbandonada($comprasAbandonadas[$i][1], $comprasAbandonadas[$i][0]);
            echo $comprasAbandonadas[$i][0]."<br/>";
		}
	
}


function enviarCompraAbandonada($productos, $email){
	
    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $email = $email;
	
    $asunto = "Dejaste estos productos olvidados";
	
	
	$cart = $productos;
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output = '';
		
        $i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.thumbs, lower(p.nombre), pd.precio, pd.descuento, p.id","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id=$prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "sin-imagen.jpg";
			}
			
			if($producto[0][3] > 0){
				$valor = $producto[0][3]*$qty;
			} else {
				$valor = $producto[0][2]*$qty;
			}
			
			$output .= '
					<div style="float:left; width:98%; border:solid 1px #ccc; margin-bottom:10px;">
						<div style="width:20%; border-right:solid 1px #ccc; float:left; text-align:center;">
							<img src="https://'.$nombre_corto.'/imagenes/productos/'.$thumbs.'" width="100%" />
						</div>
						<div style="float:left; width:46%; height:50px; color:#666; font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-left:2%; margin-top:20px; font-weight:bold;">'.$producto[0][1].'</div>
						<div style="float:left; width:16%; height:50px; color:#ee5713; font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-left:2%; margin-top:20px; font-weight:bold;">$'.number_format($valor,0,",",".").'</div>
						<div style="float:left; width:11%; height:50px; color:#666; font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-right:2%; margin-top:20px; font-weight:bold; text-align:right;">'.$qty.'</div>
					</div><!--Fila-->';
		}
		
	}
		
	$msg2 = '
    <html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>'.$nombre_sitio.'</title>
		</head>
		
		<body style="margin:0;">
			<div style="width:90%; float:left; margin-left:5%;">
				<div style="float:left; width:100%; padding:10px 0;"><img src="https://mercadojardin.cl/images/logo.png" /></div>
				<div style="float:left; width:100%;">
					<img src="https://'.$nombre_corto.'/tienda/carroAbandonado/imgCarroAbandonado.jpg" width="100%" />
				</div>
			
			
				<div style="float:left;width:90%; margin:30px 5%;">
				'.$output.'	
				</div>
			
				<div style="float:left;width:100%; text-align:center;">
					<a style="width:70%; margin-left:15%; text-align:center; color:#fff; background:#ee5713; float:left; height:50px; line-height:50px; font-family:Arial, Helvetica, sans-serif; font-size:14px; text-decoration:none;" href="https://'.$nombre_corto.'/tienda/carroAbandonado/cargaCarroAbandonado.php?lista='.$productos.'&email='.$email.'">REANUDAR CARRO</a>
				</div>
				
				<div style="float:left;width:100%; text-align:center;">
					<img src="https://'.$nombre_corto.'/tienda/carroAbandonado/transbankCompraRapida.jpg" />
				</div>
				<div style="float:left;width:100%;">
					<img src="https://'.$nombre_corto.'/tienda/carroAbandonado/imgFooter.jpg" width="100%" />
				</div>
				<div style="float:left;width:100%; background:#0d3229; padding:10px 0; text-align:center; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px;">Todos los derechos reservados. '.$nombre_sitio.'</div>
				
			</div>
		</body>
		</html>';
	//return $msg2;
    
	
    $update = update_bd("carros_abandonados","fecha_notificacion = NOW()","correo= '$email' and productos = '$productos'");
	
	$mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");
	
    $mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
    //Set an alternative reply-to address

    $mail->addAddress($email, "Estimado cliente");
    $mail->Subject = $asunto;
    $mail->msgHTML($msg2);
    $mail->AltBody = $msg2;
    $mail->CharSet = 'UTF-8';
    //send the message, check for errors
    if (!$mail->send()) {
        return "Mailer Error: " . $mail->ErrorInfo;
    } else {
        return 'envio exitoso';
    }
	
	
}

?>


