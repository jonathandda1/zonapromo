<?php
header('Content-type: text/html; charset=utf-8');
include("../../admin/conf.php");
require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');
include '../../Mailin.php';

$oc = (isset($_GET['oc'])) ? mysqli_real_escape_string($conexion, $_GET['oc']) : 0;
$envio = (isset($_GET['envio'])) ? mysqli_real_escape_string($conexion, $_GET['envio']) : 0;
if ($envio == "cliente" && $envio != "") {
    $enviarCliente = enviarCotizacionCliente($oc);
    // echo $enviarCliente;
    if ($enviarCliente == 1) {
        header("Location: ".$url_base."cotizaciones-vendedor/".$oc."?ok=La cotización fue enviada al cliente");
        die();
    }else{
        header("Location: ".$url_base."cotizaciones-vendedor/".$oc."?error=Ocurrio un error al intentar enviar");
        die();
    }
}else {
    $idEjecutiva = $_COOKIE['Vendedor_id'];
    $ejecutiva = consulta_bd("nombre, apellido, perfil, datos_orden, forma_pago, datos_banco","vendedores","id = '$idEjecutiva'","");
    $datosOrden = $ejecutiva[0][3];
    $formaPago = $ejecutiva[0][4];
    $datosBanco = $ejecutiva[0][5];
    $cotizaciones = consulta_bd("id, oc, nombre, total, total_unitario, nota_especial", "cotizaciones", "oc= '$oc'", "");
    $existeOC = mysqli_affected_rows($conexion);
    $totalNeto = $cotizaciones[0][3];
    $iva = $cotizaciones[0][3] * 0.19;
    $totalBruto = $cotizaciones[0][3] + $iva;
    $nota_cliente = $cotizaciones[0][5];
        
    if($existeOC == 0){
            echo '<script>parent.location = "'.$url_base.'404";</script>';
    }

    $html = '<body> 
        <div class="bordeSup"></div>
        <div class="contenedor">
            <div class="header"><img src="logo.png" width="300" height="150" /></div>
            <h2>HOLA, '.$cotizaciones[0][2].'</h2>
            <h3>Aquí esta el detalle de su cotización</h3>
            <h4>Su número de cotización es: <span class="ocAzul">'.$oc.'</span></h4>
            <h5>Productos cotizados</h5>

            <div class="cont100 FilaDatosUsuario table-responsive" >
                <table id="example" class="table table-striped table-bordered tablas responsive" style="width:100%">
                    <thead>
                        <tr>
                            <th>SKU producto</th>
                            <th>Foto producto</th>
                            <th>Nombre producto</th>
                            <th>Tipo de logo</th>
                            <th>Cant. de productos</th>
                            <th>Precio unitario</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>';
        $idCotizaciones = $cotizaciones[0][0];
        $prodCot = consulta_bd("productos_detalle_id, codigo, logo, cantidad, precio_unitario_neto, precio_final_neto", "productos_cotizaciones", "cotizacion_id= '$idCotizaciones' AND condicion_boton != 'sin stock' ", "");
        foreach ($prodCot as $value) {
            $codigo = $value[1];
            $idProductoCot = $value[0];
            $prodDetalle = consulta_bd("imagen1, nombre", "productos_detalles", "id = $idProductoCot AND sku = '$codigo'", "");
            $html .= '
                <tr>
                    <td>'.$value[1].'</td>
                    <td><img class="img-tabla" src="../../imagenes/productos_detalles/'.$prodDetalle[0][0].'"></td>
                    <td>'.$prodDetalle[0][1].'</td>
                    <td>'.$value[2].'</td>
                    <td>'.$value[3].'</td>
                    <td>'.$value[4].'</td>
                    <td>'.$value[5].'</td>
                </tr>';
        }

                        
        $html .= '
                    </tbody>
                </table>    
            </div>';    
        $html .= '
            <section class="cont100 contInfoTablas">
                <article class="calugasInfo1">
                        <h1>Nota especial</h1>
                        <div class="contenedorCalugaInfo1 notaEspecial">';
                        $html .= nl2br($nota_cliente); 
        $html .= '    </div>
                        <h1>Datos para emitir Orden de Compra</h1>
                        <div class="contenedorCalugaInfo1 notaEspecial">';
                        $html .= nl2br($datosOrden); 
        $html .= '
                        </div>
                        <h1>Forma de pago</h1>
                        <div class="contenedorCalugaInfo1 notaEspecial">';
                        $html .= nl2br($formaPago); 
        $html .= '
                        </div>
                        <h1>Datos de banco</h1>
                        <div class="contenedorCalugaInfo1 notaEspecial">';
                        $html .= nl2br($datosBanco); 
        $html .= '
                        </div>
                </article>
                <article class="calugasInfo2">
                    <div class="contenedorCalugaInfo2">
                        <p class="precioReferencia">Valor total Neto</p>
                        <p class="precioNumero">$'.number_format($totalNeto,0,",",".").'</p>

                        <p class="precioReferencia">Iva(19%)</p>
                        <p class="precioNumero">$'.number_format($iva,0,",",".").'</p>
                        
                        <p class="precioReferencia">Valor total bruto</p>
                        <p class="precioNumero">$'.number_format($totalBruto,0,",",".").'</p>
                    </div>
                </article>
            </section>
            <div class="footer">
                <h3>Muchas gracias por cotizar con nosotros</h3>
                <h4>Saludos coordiales, Equipo Zonapromo</h4>
            </div>
        </body>';
    //echo $html;
    //==============================================================
    //==============================================================
    //==============================================================
}
$path = __DIR__;
//die("$path");
require_once '../../vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf();
$mpdf->SetDisplayMode('fullpage');
// LOAD a stylesheet
$stylesheet = file_get_contents('estilos.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
//==============================================================
//==============================================================
//==============================================================

?>