<?php
include("../../admin/conf.php");
include '../../Mailin.php';
require_once('../../admin/includes/tienda/cart/inc/functions.inc.php');

include '../../vendor/autoload.php';
use Transbank\Webpay\WebpayPlus\Transaction;

/*desarrollo*/

Transbank\Webpay\WebpayPlus::setCommerceCode('597055555532');
Transbank\Webpay\WebpayPlus::setApiKey('579B532A7440BB0C9079DED94D31EA1615BACEB56610332264630D42D0A36B1C');
Transbank\Webpay\WebpayPlus::setIntegrationType('TEST');



/*produccion*/
// Transbank\Webpay\WebpayPlus::setCommerceCode('597034634750');
// Transbank\Webpay\WebpayPlus::setApiKey('b82c503589a28a76d859cc7c16f480e6');
// Transbank\Webpay\WebpayPlus::setIntegrationType('LIVE');



$monto = mysqli_real_escape_string($conexion, $_POST[TBK_MONTO]); 
$oc = $_POST[TBK_ORDEN_COMPRA];  
$idSesion = $_POST[TBK_ID_SESION];  
$result = $_POST[URL_RETURN];  
$final = $_POST[URL_FINAL];
$exito = opciones("url_sitio")."/exito?oc=";
$fracaso = opciones("url_sitio")."/fracaso?oc=";



if(isset($_POST[TBK_ORDEN_COMPRA]) and $_POST[TBK_ORDEN_COMPRA] != ''){
    $_SESSION['ocRechazo'] = $_POST[TBK_ORDEN_COMPRA];
   // echo $_SESSION['ocRechazo']."Siexiste";
} else {
    //echo $_SESSION['ocRechazo']."noexiste";    
}


function write_log($tipo,$cadena){
	if (file_exists("log/TBK-WS_".date("Y-m-d").".log")) {
        //si existe no hago nada
    } else {
        //si no existe lo creo
        $fileName = "log/TBK-WS_".date("Y-m-d").".log";
        $contenidoInicial = "Log transbank ".date("Y-m-d")."\n";
        file_put_contents($fileName, $contenidoInicial);
    }
    $arch = fopen("log/TBK-WS_".date("Y-m-d").".log", "a+"); 
    fwrite($arch, $tipo.": ".$cadena."\n");
	fclose($arch);
}


$action = isset($_GET["action"]) ? $_GET["action"]: 'init';
$montoAPagar = consulta_bd("total_pagado","pedidos","oc = '$oc'","");

switch ($action) {

	default: 
 		$tx_step = "Init";

        $error = 0;
        if(is_numeric($monto)){
            $message  = "======================================================================= \r\n";
            $message .= "ERROR!: La variable monto es numérica (Posible intento de hackeo). OC = {$oc} \r\n";
            $message .= "=======================================================================";
            write_log("Mensaje: ", $message);
            $error = 1;
        }else{
            $monto_decode = consulta_bd('monto, token', 'checkout_token', "token = '$monto'","");
            $fecha = date("d-m-Y H");

            if ($monto_decode < 1) {
                $message = "======================================================================== \r\n";
                $message .= "ERROR!: Token no autorizado para la compra (Posible intento de hackeo). \r\n";
                $message .= "------------------------------------------------------------------------ \r\n";
                $message .= "Token Ingresado: {$monto_decode[0][0]} \r\n";
                $message .= "Fecha Operación: {$fecha} \r\n";
                $message .= "Orden de compra: {$oc} \r\n";
                $message .= "=======================================================================";
                write_log("mensaje: ",$message);
                $error = 1;
            }
        }

        if ($error == 1) {
            del_bd_generic('checkout_token', 'oc', "{$oc}");
            header("Location: $fracaso".$_SESSION['ocRechazo']);
            die();
        }

        $buy_order = $oc;
        $session_id = $idSesion;
        $amount = $monto_decode[0][0];
        $return_url = $result;

        $response = Transaction::create($buy_order, $session_id, $amount, $return_url);

        $next_page = $response->getUrl();
        $webpay_token = $response->getToken();
        
        /*echo "<pre>";
        print_r($response);
        echo "</pre>";*/
        
       // Verificamos respuesta de inicio en webpay
		if (strlen($webpay_token)) {
			$message = "Sesion iniciada con exito en Webpay";
			$next_page = $response->getUrl();
            write_log("     Response Mensaje","$message");
        } else {
			$message = "webpay no disponible";
            write_log("     Response Mensaje","$message");
            //al no coinidir los certificados retorno al fracaso
            header("Location: $fracaso".$_SESSION['ocRechazo']);
		}

		break;

	case "result":
 		
        $tx_step = "Get Result";
		if (!isset($_POST["token_ws"])) {
            header("Location: ?action=end");
            break;
        }
        
        
		$token = filter_input(INPUT_POST, 'token_ws');
        $request = array( 
			"token"  => filter_input(INPUT_POST, 'token_ws')
		);
        $webpay_token = $token;
		
        $response = Transaction::commit($token);
        //datos que retornan de transbank
        /*$cardExpirationDate = $result->cardDetail->cardExpirationDate;
        $sharesNumber = $result->detailOutput->sharesNumber;
        $commerceCode = $result->detailOutput->commerceCode;
        */   
        $cardExpirationDate = "";
        $sharesNumber = "";
        $commerceCode = "";  
        $VCI = $response->vci; //OK
        $amount = $response->amount; //OK
        $status = $response->status; //OK
        $buyOrder = $response->buyOrder; //OK
        $cardNumber = $response->cardDetail['card_number'];//OK
        $accountingDate = $response->accountingDate; //OK
        $transactionDate = $response->transactionDate;
        $authorizationCode = $response->authorizationCode; //OK
        $paymentTypeCode = $response->paymentTypeCode; //OK
        $responseCode = $response->responseCode; //OK
        $sharesNumber = $response->installmentsNumber; //OK
        $ValorCuota = $response->installmentsAmount; //OK
        $webpay_token = $token;
        //$token = $_POST["token_ws"];
        
        
       /*echo "<pre>";
        print_r($response);
        echo "</pre>";*/
        
        ////////////////// ----------- ELIMINO STOCK TEMPORAL ----------- //////////////////
        $stock_temporal = del_bd_generic("stock_temporal","oc","$buyOrder");
        ////////////////// ----------- ////////////////// ----------- //////////////////

        
        
        
        // Verificamos resultado del pago
        //si el pago es exitoso
		if ($responseCode === 0) {
			$message = "Pago ACEPTADO por webpay (se deben guardatos para mostrar voucher)";
			//$next_page = $response->urlRedirection;
			
            $next_page = "?action=end";
            write_log("Resultado","");
            write_log("     Exito","$message");
            
            $next_page_title = "Finalizar Pago";
            
            $estado = 2;
            //actualizao la bd con los datos recibidos
            update_bd("pedidos","accounting_date = '$accountingDate', card_number = '$cardNumber', card_expiration_date = '$cardExpirationDate', authorization_code = '$authorizationCode', payment_type_code = '$paymentTypeCode', response_code = '$responseCode', shares_number = '$sharesNumber', amount = '$amount', commerce_code = '$commerceCode', transaction_date = '$transactionDate', vci = '$VCI', token = '$token', estado_id = $estado","oc='$buyOrder'");
            
            //obtengo el id del pedido
            $id_pedido = consulta_bd("id, retiro_en_tienda, region","pedidos","oc = '$buyOrder'","");
            $reducirStock = consulta_bd("productos_detalle_id, cantidad","productos_pedidos","pedido_id = ".$id_pedido[0][0],"");
            //redusco el stock de los productos comprados.
            for($i=0; $i<sizeof($reducirStock); $i++){
                $cantActual = consulta_bd("id,stock","productos_detalles","id = ".$reducirStock[$i][0],"");
                $qtyFinal = $cantActual[0][1]- $reducirStock[$i][1];
				
				//si no hay productos disponibles despublico la entrada.
				if($qtyFinal == 0){
					$despublico = ', publicado = 0';
				}
                update_bd("productos_detalles","stock=$qtyFinal $despublico","id=".$reducirStock[$i][0]);
            }
			
			//si existe codigo de descuento, lo descuento del total y elimino la sesion
			
			if($_SESSION["descuento"]){
				$codigo = $_SESSION["descuento"];
				$codigoActual = consulta_bd("oc","codigo_descuento","codigo = '$codigo'","");
				$ocActualizada = $codigoActual[0][0].$buyOrder.',';

                $codigo_primera = update_bd("first_buy", "usado = usado+1", "codigo = '$codigo'");

				$desactivoCodigo = update_bd("codigo_descuento","fecha_modificacion = NOW(),fecha_uso = NOW(), oc = '$ocActualizada', usados = usados +1","codigo = '$codigo'");
				unset($_SESSION["descuento"]);
                unset($_SESSION["val_descuento"]);
			}
          
            //funciones para avisar al cliente y al administrador de la venta
			//en el envio del cliente se guarda en mailchimp el correo 
            enviarComprobanteCliente($buyOrder);
            enviarComprobanteAdmin($buyOrder,"","");
          
            
			//elimino la cookie del carro de compras
			setcookie('cart_alfa_cm', null, -1, '/');
			
			
			
			
		} else {
			$message = "Pago RECHAZADO por webpay - $authorizationCode ---- $status";
            write_log("Resultado","".$_SESSION['ocRechazo']);
            write_log("     Rechazo","$message");
            
            
            $next_page= $fracaso.$buyOrder;//$_SESSION['ocRechazo'];
            
            $estado = 3;
            update_bd("pedidos","accounting_date = '$accountingDate', card_number = '$cardNumber', card_expiration_date = '$cardExpirationDate', authorization_code = '$authorizationCode', payment_type_code = '$paymentTypeCode', response_code = '$responseCode', shares_number = '$sharesNumber', amount = '$amount', commerce_code = '$commerceCode', transaction_date = '$transactionDate', vci = '$VCI', token = '$token', estado_id = $estado","oc='$buyOrder'");
            //header("Location: $fracaso.$buyOrder");
        }
        
        
       break;
		
        

	case "end":
        $token_ws = $_POST['token_ws'];
        
        $oc = consulta_bd("oc","pedidos","token = '$token_ws' and vci <> ''","");
        $cantresult = count($oc);
        if($cantresult > 0){
            
            $tx_step = "End";
            write_log("Transaccion exitosa","$tx_step");

            $request= '';
            $result = $_POST;
            $message = "Transacion Finalizada";
            $next_page = $exito.$oc[0][0];
            break;
        } else {
            $tx_step = "End";
            write_log("Transaccion Cancelada","$tx_step");
            $request= '';
            $result = $_POST;
            $message = "Transacion Finalizada";
            $next_page= $fracaso.$_SESSION['ocRechazo'];

            //save_in_mailchimp($_SESSION['ocRechazo'], 'pendiente');
            //save_in_mailchimp($_SESSION['ocRechazo'], 'todas_compras');
            
            //header("Location: $fracaso".$_POST[TBK_ORDEN_COMPRA]);
            break;
        }
       
}

del_bd_generic('checkout_token', 'oc', "{$oc}");

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<?php     if (strlen($next_page)) {    ?>
<form style="display:none;" action="<?php echo $next_page; ?>" method="post" id="formProceso">
	<input type="hidden" name="token_ws" value="<?php echo $webpay_token; ?>">
	<input type="submit" value="Continuar &raquo;">
</form>
<?php } ?>




<script type="text/javascript">
    $(function(){
        $('#formProceso').submit();	
    });
</script>