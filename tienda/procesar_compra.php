<?php
//conf
include('../admin/conf.php');

//funciones carro
// Include functions
include '../Mailin.php';
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');

require_once "mercadopago/lib/mercadopago.php";
include('mercadopago/claves.php');


    


$info_email = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : 0;
$info_tele  = (isset($_POST['telefono'])) ? mysqli_real_escape_string($conexion, $_POST['telefono']) : 0;
$info_name  = (isset($_POST['nombre'])) ? mysqli_real_escape_string($conexion, $_POST['nombre']) : 0;
$rut  = (isset($_POST['rut'])) ? mysqli_real_escape_string($conexion, $_POST['rut']) : 0;


if(isset($_COOKIE[usuario_id])){
	$current_user = $_COOKIE[usuario_id];
	$idClienteInsert = $_COOKIE[usuario_id];
	} else {
		//usuario no logueado
			$info_email = trim(strtolower($info_email));		
			$clientes = consulta_bd("id","clientes","LOWER(email) = '$info_email'","");
			$cantClientes = mysqli_affected_rows($conexion);
			if($cantClientes > 0){
				$current_user = $clientes[0][0];
			} else {
                $lc = listaCliente();
				$inserCliente = insert_bd("clientes","nombre,email,telefono, tipo_registro, lista_id, rut","'$info_name','$info_email','$info_tele', 'persona', $lc, '$rut'");
                
				$idClienteInsert = mysqli_insert_id($conexion);
				$current_user = $idClienteInsert;
			}
		//usuario no logueado
		}





$date = date('Ymdhis');
if($medioPago == 'notaVenta'){ 
    $oc = "NV_$date";
    $oc2 = "NV_$date";
} else{
    $oc = "OC_$date";
    $oc2 = "OC_$date";
}


//GET ID ADDRESS
$id = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;

$id_direccion = (isset($_POST['address_id'])) ? mysqli_real_escape_string($conexion, $_POST['address_id']) : 0; 
$tiendaRetiro = (isset($_POST['idSucursal'])) ? mysqli_real_escape_string($conexion, $_POST['idSucursal']) : 0; 

$ciudad = "sin relacion";
$envio = (isset($_POST['envio'])) ? mysqli_real_escape_string($conexion, $_POST['envio']) : 0;

/*===================================================
 =            Definiendo el tipo de envio            =
 ===================================================*/
	if($envio == "despachoDomicilio") {
        $retiroEnTienda = 0;
        $instalaciones = "Despacho a Domicilio";
    //DESPACHO A LA DIRECCION INDICADA POR EL CLIENTE
	
	$comuna_id = (is_numeric($_POST['comuna'])) ? mysqli_real_escape_string($conexion, $_POST['comuna']) : 0;
	$localidad_id = (is_numeric($_POST['localidades'])) ? mysqli_real_escape_string($conexion, $_POST['localidades']) : 0;
	$campos = "r.nombre, c.nombre, c.id, r.id";
	$tablas = "comunas c, regiones r";
	$where = "r.id = c.region_id and c.id = $comuna_id";
	$address = consulta_bd($campos,$tablas,$where,"");
	
	
	$calle = (isset($_POST['direccion'])) ? mysqli_real_escape_string($conexion, $_POST['direccion']) : 0;
	$numero 	= '';//$address[0][1];
	$region    	= $address[0][0];
	//$ciudad 	= $address[0][1];
	$comuna     = $address[0][1];
	//$localidad  = $address[0][6];
	
	$direccion = $calle;
	
	//$ciudad_id = $address[0][5];
	//var_dump($address);
	//die("$ciudad_id");
	$region_id = (is_numeric($_POST['region'])) ? mysqli_real_escape_string($conexion, $_POST['region']) : 0;
	
	
	//ingreso la direccion del cliente para futuras compras
	if($cantClientes == 0){
		if(!isset($_COOKIE[usuario_id])){
            $ed = consulta_bd("id","clientes_direcciones","comuna_id = $comuna_id and region_id = $region_id and cliente_id = $current_user","");
            if(count($ed) == 0){
                $inserCliente = insert_bd("clientes_direcciones","cliente_id,region_id, comuna_id, calle, nombre, fecha_creacion", "$idClienteInsert, $region_id, $comuna_id,'$calle', 'Direccion Despacho',NOW()");
                
                $inserCliente = insert_bd("clientes_direcciones","cliente_id,region_id, comuna_id, calle, nombre, fecha_creacion", "$idClienteInsert, $region_id, $comuna_id,'$calle', 'Direccion Facturacion', NOW()");
            }
            
        }
    }
	
	
	//valor de despacho por comunas
	//$valor_despacho = consulta_bd("precio_envio","comunas","id=".$comuna_id,"");
	//DESPACHO
	$despacho = valorDespacho($comuna_id,$retiroEnTienda,$tiendaRetiro);
    $despacho_id = $tiendaRetiro;
    } else {
        $retiroEnTienda = 1;
        $instalaciones = "Retiro en Tienda";
        $sucursales = consulta_bd("s.id, s.nombre, s.direccion, s.comuna_id, c.id as id_comuna, c.nombre, r.nombre as region_nombre","sucursales s, comunas c, regiones r","s.comuna_id = c.id and c.region_id = r.id and s.id = $tiendaRetiro","s.posicion asc");
	//if($tiendaRetiro == 'D1'){
		//tienda las hualtatas
		//$ciudad 	= $sucursales[0][5];//'Santiago';
		$comuna     = $sucursales[0][5];
		//$localidad  = $sucursales[0][6];
		$region    	= $sucursales[0][6]; //'Metropolitana';
		$direccion  = $sucursales[0][2];
		//DESPACHO
		$despacho = 0;
		$despacho_id = $tiendaRetiro;        
		$comuna_id = $sucursales[0][4];//(is_numeric($_POST[comuna])) ? mysqli_real_escape_string($conexion, $_POST[comuna]) : 0;
        
	//}
    }
 /*=====  End of Definiendo el tipo de envio  ======*/


//datos retiro en tienda
$nombre_retiro = (isset($_POST['nombre_retiro'])) ? mysqli_real_escape_string($conexion, $_POST['nombre_retiro']) : 0;
$telefono_retiro = (isset($_POST['telefono_retiro'])) ? mysqli_real_escape_string($conexion, $_POST['telefono_retiro']) : 0;

$rut_retiro = (isset($_POST[rut_retiro])) ? mysqli_real_escape_string($conexion, $_POST[rut_retiro]) : 0;
//$patente_retiro = (isset($_POST[patente_retiro])) ? mysqli_real_escape_string($conexion, $_POST[patente_retiro]) : 0;

//Comentarios envios
$comentarios_envio = (isset($_POST['comentarios_envio'])) ? mysqli_real_escape_string($conexion, $_POST['comentarios_envio']) : 0;





//REGALO
$regalo = (isset($_POST['regalo'])) ? mysqli_real_escape_string($conexion, $_POST['regalo']) : 0; //mysql_real_escape_string($_POST[regalo]);
$name_regalo = 0;//mysql_real_escape_string($_POST[name_regalo]);
$tel_regalo = 0;//mysql_real_escape_string($_POST[tel_regalo]);



$comentarios_envio = (isset($_POST['comentarios_envio'])) ? mysqli_real_escape_string($conexion, $_POST['comentarios_envio']) : 0;



//facturacion
	$factura  = (isset($_POST['factura'])) ? mysqli_real_escape_string($conexion, $_POST['factura']) : 0;
	$datosFacturaHBT = consulta_bd("giro, rut","clientes","email = '$info_email'","");
	if($factura == 'no'){
		$giro = 'Particular';
		$factura = 0;
	}else{
		$giro = (isset($_POST['giro_factura'])) ? mysqli_real_escape_string($conexion, $_POST['giro_factura']) : 0;
		$factura = 1;
	}
	$razon_social = (isset($_POST['razon_social'])) ? mysqli_real_escape_string($conexion, $_POST['razon_social']) : 0;
	$rut_factura = (isset($_POST['rut_factura'])) ? mysqli_real_escape_string($conexion, $_POST['rut_factura']) : 0;
	$email_factura = (isset($_POST['email_factura'])) ? mysqli_real_escape_string($conexion, $_POST['email_factura']) : 0;
	$direccion_factura = (isset($_POST['direccion_factura'])) ? mysqli_real_escape_string($conexion, $_POST['direccion_factura']) : 0;
	
	$id_cliente_hbt = $datosFacturaHBT[0][2];
		
		

$subtotal = round(get_total_price($comuna_id));

$cod = 'null';
//descuento
if($_SESSION["descuento"]){
	$valor_descuento = $_SESSION['val_descuento'];
	// Condición session descuento
	$cod = $_SESSION['descuento'];
} else {
	$valor_descuento = 0;
	$cod = '';
}

$instalacion = 0;
						
$medioPago  = (isset($_POST[medioPago])) ? mysqli_real_escape_string($conexion, $_POST[medioPago]) : 0;

//TOTAL
$total = $subtotal + $despacho - $valor_descuento;

//CART EXPLODE AND COUNT ITEMS
if(!isset($_COOKIE[cart_alfa_cm])){
	setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
$cart = $_COOKIE[cart_alfa_cm];
	

$items = explode(',',$cart);
foreach ($items as $item) {
	$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
}



////////////////// ----------- REVISA STOCK TEMPORAL ----------- //////////////////

	foreach ($contents as $id=>$qty){

		$isPack = consulta_bd("p.pack, p.codigos","productos p, productos_detalles pd","p.id = pd.producto_id AND pd.id = $id","");

		if($isPack[0][0]){

			$codes_pack = explode(',', $isPack[0][1]);
			foreach ($codes_pack as $value){
				$value 			= trim($value);
				$stock 			= consulta_bd("id, stock, stock_reserva, sku","productos_detalles","sku = '$value'","");
				$stock_temporal = consulta_bd("stock","stock_temporal","sku = ".$stock[0][3],"");
				if($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])){
					header("location: mi-carro?stock=$id");
					die();
				}
			}

		}else{
			$stock 			= consulta_bd("id, stock, stock_reserva, sku","productos_detalles","id=$id","");
			$stock_temporal = consulta_bd("stock","stock_temporal","sku='".$stock[0][3]."'","");
			if($qty > ($stock[0][1] - $stock[0][2] - $stock_temporal[0][0])){
				header("location: mi-carro?stock=$id");
				die();
			}	
		}

		
	}

////////////////// ----------- ////////////////// ----------- //////////////////




$nombre_retiro = (isset($_POST[nombre_retiro])) ? mysqli_real_escape_string($conexion, $_POST[nombre_retiro]) : 0;
$apellidos_retiro = (isset($_POST[apellidos_retiro])) ? mysqli_real_escape_string($conexion, $_POST[apellidos_retiro]) : 0;
$rut_retiro = (isset($_POST[rut_retiro])) ? mysqli_real_escape_string($conexion, $_POST[rut_retiro]) : 0;


if ($_COOKIE['cart_alfa_cm']!="") {
	
	$estado_pendiente = 1;
	$campos = "oc, 
	cliente_id, 
	fecha, 
	estado_id, 
	codigo_descuento, 
	descuento, 
	total, 
	valor_despacho, 
	total_pagado, 
	regalo, 
	nombre, 
	telefono, 
	email, 
	direccion, 
	comuna, 
	region, 
	ciudad, 
	localidad, 
	giro, 
	rut, 
	retiro_en_tienda, 
	sucursal_id,
	factura, razon_social, 
	direccion_factura, 
	rut_factura, 
	email_factura, comentarios_envio, medio_de_pago, id_notificacion, nombre_retiro, telefono_retiro, rut_retiro, costo_instalacion, estados_pedido_id";
	$values = "NULL, '$current_user', NOW(), $estado_pendiente, '$cod', '$valor_descuento', $subtotal, $despacho, $total, 0, '$info_name', '$info_tele', '$info_email', '$direccion', '$comuna', '$region', '$ciudad', '$localidad', '$giro', '$rut', '$instalaciones', $despacho_id, $factura, '$razon_social', '$direccion_factura', '$rut_factura', '$email_factura', '$comentarios_envio', '$medioPago', 0, '$nombre_retiro', '$telefono_retiro','$rut_retiro', $instalacion, 1";
	
    $generar_pedido = insert_bd("pedidos","$campos","$values");
			
	$pedido_id = mysqli_insert_id($conexion);

	$new_oc = $oc.$pedido_id;
	update_bd('pedidos', "oc = '$new_oc'", "id = $pedido_id");

    
    
    
	/* CARRO ABANDONADO */
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
	$pdca = $_COOKIE[cart_alfa_cm];
	update_bd("carros_abandonados","oc='$new_oc'","productos = '$pdca' and correo = '$info_email'");
	$notificacion = (isset($_SESSION['notificacion'])) ? mysqli_real_escape_string($conexion, $_SESSION['notificacion']) : 0;

	$cantTotal = 0;

		foreach ($contents as $id=>$qty) {
			//consulto el precio segun la lista de precios
			$prod = consulta_bd("pd.precio, pd.descuento, p.pack, p.codigos, pd.sku, p.costo_instalacion, p.id","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $id","");
			////Veo si el producto es un pack, lo manejo de cierta forma///////////////////////
			if($prod[0][2] == 1){
				
				$precio_final = $prod[0][0];
				$codigo_pack = $prod[0][4];
				$codigosProductos = trim($prod[0][3]);
				$productosPack = explode(",", $codigosProductos);
				
				
				//saco la suma de los productos reales
				$totalReal = 0;
				foreach($productosPack as $skuPack) {
					$skuPack = trim($skuPack);
					$valorProdDetalle = consulta_bd("precio","productos_detalles","sku = '$skuPack'","");
					$totalReal = $totalReal + $valorProdDetalle[0][0];
				}
				//obtengo el porcentaje de descuento
				$descPorcentajeProducto = ($totalReal - $precio_final)/$totalReal;
				//$descPorcentajeProducto2 = (($precio_final - $totalReal)/$totalReal) * -1;
				//die("$descPorcentajeProducto /////-///// $descPorcentajeProducto2");
				
				$retorno = "";
				$idPd = 0;
				foreach($productosPack as $skuPack) {
					$skuPack = trim($skuPack);
					$valorProdDetalle2 = consulta_bd("precio, id","productos_detalles","sku = '$skuPack'","");
					
					$precioUnitario = $valorProdDetalle2[0][0];
					$idProductoDetalle = $valorProdDetalle2[0][1];
					
					$precioDescuento = $valorProdDetalle2[0][0] * (1 - $descPorcentajeProducto);
					$precioTotal = round($precioDescuento * $qty);
					
					$desc = 1 - $descPorcentajeProducto;
					//die("$precioUnitario  --  $precioTotal");
					$sku = $skuPack;
					
					
					$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, descuento, codigo, precio_total, fecha_creacion, codigo_pack";
					$values = "$pedido_id,$idProductoDetalle,'$qty',$precioUnitario, $precioDescuento, '$sku', $precioTotal, NOW(), '$codigo_pack'";
					$detalle = insert_bd("productos_pedidos",$campos,$values);
					$cantTotal = $cantTotal + $qty;
				}//fin foreach
				
				
				
			} else {
			///////////////////////////
			
				/*if($prod[0][1] > 0){
					$precio_final = $prod[0][1];
					$descuento = round(100 - (($prod[0][1] * 100) / $prod[0][0]));
				}else{
					$precio_final = $prod[0][0];
					$descuento = 0;
				}*/
                
                if ($is_cyber and is_cyber_product($prod[0][6])){
				//si es cyber
					if($prod[0][7] > 0){
						//guardo el precio cyber
							$precio_final = $prod[0][7];
							$descuento = round(100 - (($prod[0][7] * 100) / $prod[0][0]));
						} else if($prod[0][1] > 0){
							//guardo el precio con desceunto
							$precio_final = $prod[0][1];
							$descuento = round(100 - (($prod[0][1] * 100) / $prod[0][0]));
						} else{
							//guardo el precio normal
							$precio_final = $prod[0][0];
							$descuento = 0;
						}
				} else {
					//no es cyber
					if($prod[0][1] > 0){
							$precio_final = $prod[0][1];
							$descuento = round(100 - (($prod[0][1] * 100) / $prod[0][0]));
						}else{
							$precio_final = $prod[0][0];
							$descuento = 0;
						}
					}//fin else no es cyber
				
                $costoInstalacion = 0;
				if($prod[0][5] > 0){
					if(opcionInstalacion($id) == 1){
						$costoInstalacion = ($prod[0][5] * $qty);
						} else {
                        $costoInstalacion = 0;
                    }
				}
                
				
				$precioUnitario = $precio_final;
				$precioTotal = $precioUnitario * $qty;
				$porcentaje_descuento = $descuento;//$prod[0][1];//$valoresFinales[descuento];
				
				$all_sku = consulta_bd("sku","productos_detalles","id = $id","");
				$sku = $all_sku[0][0];
				/* $precio = $all_sku[0][1]; */
	
				$campos = "pedido_id, productos_detalle_id, cantidad, precio_unitario, codigo, precio_total, fecha_creacion, descuento, costo_instalacion";
				$values = "$pedido_id, $id, $qty, $precioUnitario,'$sku', $precioTotal, NOW(), $porcentaje_descuento, $costoInstalacion";
				$detalle = insert_bd("productos_pedidos",$campos,$values);
				$cantTotal = $cantTotal + $qty;
			}//fin del ciclo


			////////////////// ----------- INSERTO STOCK TEMPORAL ----------- //////////////////


				$isPack = consulta_bd("p.pack, p.codigos","productos p, productos_detalles pd","p.id = pd.producto_id AND pd.id = $id","");

				if($isPack[0][0]){
					$codes_pack = explode(',', $isPack[0][1]);
					foreach ($codes_pack as $value){
						$value 			= trim($value);
						$stock  = consulta_bd("id, stock, stock_reserva, sku","productos_detalles","sku = '$value'","");
						$stock_temporal = insert_bd("stock_temporal","oc, sku, stock, fecha_creacion","'$new_oc','".$stock[0][3]."', $qty, NOW()");
					}

				}else{

		        	$stock  = consulta_bd("id, stock, stock_reserva, sku","productos_detalles","id=$id","");
					$stock_temporal = insert_bd("stock_temporal","oc, sku, stock, fecha_creacion","'$new_oc','".$stock[0][3]."', $qty, NOW()");

				}


			////////////////// ----------- ////////////////// ----------- //////////////////
		}

		$qtyFinal = update_bd("pedidos","cant_productos = $cantTotal, fecha_creacion = NOW()","id=$pedido_id");

        unset($_SESSION["notificacion"]);

        $token = generateToken($new_oc, $total);

    if ($medioPago == 'mercadopago') {
    	// mpago 
		$mp = new MP ($keys['client_id'], $keys['client_secret']);

		$preference_data = array(
			"items" => array(
				array(
					"id" => $new_oc,
					"title" => 'Compra de productos '.opciones('nombre_cliente'),
					"currency_id" => "CLP",
					"picture_url" =>"https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
					// "description" => "Description",
					// "category_id" => "Category",
					"quantity" => 1,
					"unit_price" => $total
				)
			),
			"payment_method_id" => array('visa','master','webpay'),
			"payer" => array(
		        "first_name" => $info_email,
		        "email" => $info_name
		    ),
			"back_urls" => array(
				"success" => $url_base . "tienda/mercadopago/payments/casepay.php?oc={$new_oc}&case=success",
				"failure" => $url_base . "tienda/mercadopago/payments/casepay.php?oc={$new_oc}&case=failure",
				"pending" => $url_base . "tienda/mercadopago/payments/casepay.php?oc={$new_oc}&case=pending"
			),	
			"auto_return" => "approved",
		   "notification_url" => $url_base."tienda/mercadopago/notificacion.php?oc={$new_oc}&cod={$cod}",
		   "expires" => false,
		   "expiration_date_from" => null,
		   "expiration_date_to" => null

		);

		$preference = $mp->create_preference($preference_data);
    }

} else {
	header("location:404");
}
?>
<div style="position:absolute; top:0; left:0; width:100%; height:100%; z-index:999; background-image: url(<?= $url_base; ?>tienda/trama.png);">
    <div style="margin: 0 auto; width: 100%;text-align:center;font-family:verdana;font-size: 12px;padding-top:200px;">
        <img src="img/logo.png" border="0" width="155" /><br /><br />
            Estamos procesando su compra, espere por favor...<br />
            cant productos = <?php echo $cantTotal; ?><br />
            total = <?php echo round($total); ?>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


<?php if($medioPago == 'webpay'){ ?>
<form id="form" action="tienda/webpay/tbk-normal_api-rest.php" method="post" >
    <input type="hidden" name="TBK_MONTO" value="<?php echo $token; ?>" />
    <input type="hidden" name="TBK_ORDEN_COMPRA" value="<?php echo $new_oc;?>" />
    <input type="hidden" name="TBK_ID_SESION" value="<?php echo $new_oc;?>" />
    <input type="hidden" name="URL_RETURN" value="<?= opciones("url_sitio"); ?>/tienda/webpay/tbk-normal_api-rest.php?action=result" />
    <input type="hidden" name="URL_FINAL" value="<?= opciones("url_sitio"); ?>/tienda/webpay/tbk-normal_api-rest.php?action=end" />
</form>
<script type="text/javascript">
$(function(){
	$('#form').submit();
});
</script>
<?php } else if($medioPago == 'notaVenta'){ 
    /*no direcciono a ningun medio de pago, solo redirecciono al sitio con exito */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    function write_log($tipo,$cadena){
	if (file_exists("log_nv/NTV_".date("Y-m-d").".log")) {
            //si existe no hago nada
        } else {
            //si no existe lo creo
            $fileName = "log_nv/NTV_".date("Y-m-d").".log";
            $contenidoInicial = "Log nota de venta ".date("Y-m-d")."\n";
            file_put_contents($fileName, $contenidoInicial);
        }
        $arch = fopen("log_nv/NTV_".date("Y-m-d").".log", "a+"); 
        fwrite($arch, $tipo.": ".$cadena."\n");
        fclose($arch);
    }
            $message = "////////////////////////////////////////////////////////////////////////////////";
            $message .= $new_oc;
            $message .= "Pago ACEPTADO por nota de credito";
			$message .= "////////////////////////////////////////////////////////////////////////////////";
            write_log("Resultado","");
            write_log("     Exito","$message");
            
            $estado = 4;
            //actualizao la bd con los datos recibidos
            update_bd("pedidos","estado_id = $estado","oc='$new_oc'");
            
            //obtengo el id del pedido
            $reducirStock = consulta_bd("productos_detalle_id, cantidad","productos_pedidos","pedido_id = $pedido_id","");
            //redusco el stock de los productos comprados.
            for($i=0; $i<sizeof($reducirStock); $i++){
                $cantActual = consulta_bd("id,stock","productos_detalles","id = ".$reducirStock[$i][0],"", array("i", ));
                $qtyFinal = $cantActual[0][1]- $reducirStock[$i][1];
				
				//si no hay productos disponibles despublico la entrada.
				if($qtyFinal == 0){
					$despublico = ', publicado = 0';
				}
                update_bd("productos_detalles","stock=$qtyFinal $despublico","id=".$reducirStock[$i][0]);
            }
			
			$stock_temporal = del_bd_generic("stock_temporal","oc","$new_oc");
            //funciones para avisar al cliente y al administrador de la venta
			//en el envio del cliente se guarda en mailchimp el correo 
    
            //envio a enviame
            enviame($pedido_id);
            //inserto pedido en zap
            insertoEnZap($new_oc, "notaVenta");
            //envio las notificaciones al cliente
            enviarComprobanteCliente($new_oc);
            enviarComprobanteAdmin($new_oc,"","");

            
			//elimino la cookie del carro de compras
			setcookie('cart_alfa_cm', null, -1, '/');
			
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
    
    
?>
    <script type="text/javascript">
		setTimeout(function(){
		window.location.href = "exito-empresa?oc=<?= $new_oc; ?>";
		}, 1000); // 2Segs
	</script>

<?php } else if($medioPago == 'transferencia'){ 
    /*no direcciono a ningun medio de pago, solo redirecciono al sitio con exito */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    function write_log($tipo,$cadena){
	if (file_exists("log_trnsf/TRF_".date("Y-m-d").".log")) {
            //si existe no hago nada
        } else {
            //si no existe lo creo
            $fileName = "log_trnsf/TRF_".date("Y-m-d").".log";
            $contenidoInicial = "Log nota de venta ".date("Y-m-d")."\n";
            file_put_contents($fileName, $contenidoInicial);
        }
        $arch = fopen("log_trnsf/TRF_".date("Y-m-d").".log", "a+"); 
        fwrite($arch, $tipo.": ".$cadena."\n");
        fclose($arch);
    }
            $message = "////////////////////////////////////////////////////////////////////////////////";
            $message .= $new_oc;
            $message .= "Pago ACEPTADO por transferencia";
			$message .= "////////////////////////////////////////////////////////////////////////////////";
            write_log("Resultado","");
            write_log("     Exito","$message");
            
            $estado = 5;
            //actualizao la bd con los datos recibidos
            update_bd("pedidos","estado_id = $estado","oc='$new_oc'");
            
            
            enviarComprobanteCliente($new_oc);
            enviarComprobanteAdmin($new_oc,"","");
    
			//elimino la cookie del carro de compras
			setcookie('cart_alfa_cm', null, -1, '/');
			
            
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
    
    
?>
    <script type="text/javascript">
		setTimeout(function(){
		window.location.href = "exito-transferencia?oc=<?= $new_oc; ?>";
		}, 1000); // 2Segs
	</script>
<?php } else { //envia form mercadopago ?>
    <script type="text/javascript">
		setTimeout(function(){
		window.location.href = "<?=$preference["response"]["init_point"]?>";
		}, 1000); // 2Segs
	</script>
<?php } ?>

<?php mysqli_close($conexion); ?>

