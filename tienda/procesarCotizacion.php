<?php
//conf
include('../admin/conf.php');
include '../Mailin.php';
// require_once('../PHPMailer/PHPMailerAutoload.php');
// Include functions
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');
$nombre  = (isset($_POST['nombreCotizar'])) ? mysqli_real_escape_string($conexion, $_POST['nombreCotizar']) : 0;
$apellido  = (isset($_POST['apellidoCotizar'])) ? mysqli_real_escape_string($conexion, $_POST['apellidoCotizar']) : 0;
$rutCotizar  = (isset($_POST['rutCotizar'])) ? mysqli_real_escape_string($conexion, $_POST['rutCotizar']) : 0;
$telefono  = (isset($_POST['telefonoCotizar'])) ? mysqli_real_escape_string($conexion, $_POST['telefonoCotizar']) : 0;
$email = (isset($_POST['emailCotizar'])) ? mysqli_real_escape_string($conexion, $_POST['emailCotizar']) : 0;
$empresa  = (isset($_POST['empresaCotizar'])) ? mysqli_real_escape_string($conexion, $_POST['empresaCotizar']) : 0;
$rutEmpresa  = (isset($_POST['rutEmpresaCotizar'])) ? mysqli_real_escape_string($conexion, $_POST['rutEmpresaCotizar']) : 0;
$comentarios  = (isset($_POST['comentariosCotizar'])) ? mysqli_real_escape_string($conexion, $_POST['comentariosCotizar']) : 0;
// $hola = enviarComprobanteAdminCotizacion('COT_20211223103951');
$empresaNombre = strtoupper(trim($empresa));
$empresas = consulta_bd("id,nombre, rut","empresas","UPPER(nombre) = '$empresaNombre'","");
$cantEmpresas = mysqli_affected_rows($conexion);
$idVendedor = 0;
if($cantEmpresas <= 0){
	$codigoEmpresa = "#".$empresaNombre;
	$insertEmpresa = insert_bd("empresas","publicada, nombre, rut, lista_id, codigo, fecha_creacion","1, '$empresa','$rutEmpresa', 1, '$codigoEmpresa', NOW()");
	$idEmpresa = mysqli_insert_id($conexion);
	$vendedorEmpresa = consulta_bd("vendedor_id, id","empresas_vendedores","empresa_id = '$idEmpresa'","");
	$cantvendedorEmpresa = mysqli_affected_rows($conexion);
	if($cantvendedorEmpresa > 0){
		$asociar = 1;
	}else{
		$asociar = 0;
	}
}else{
	$idEmpresa = $empresas[0][0];
	$vendedorEmpresa = consulta_bd("vendedor_id, id","empresas_vendedores","empresa_id = '$idEmpresa'","");
	$cantvendedorEmpresa = mysqli_affected_rows($conexion);
	if($cantvendedorEmpresa > 0){
		$idVendedor = $vendedorEmpresa[0][0];
		$asociar = 1;
	}else{
		$asociar = 0;
	}
}

if(isset($_COOKIE['usuario_id'])){
	$current_user = $_COOKIE['usuario_id'];
	$idClienteInsert = $_COOKIE['usuario_id'];
} else {
	//usuario no logueado
	$email = trim(strtolower($email));		
	$clientes = consulta_bd("id,empresa_id","clientes","LOWER(email) = '$email'","");
	$cantClientes = mysqli_affected_rows($conexion);
	if($cantClientes > 0){
		$current_user = $clientes[0][0];
		if ($clientes[0][1] == "" || $clientes[0][1] == null || $clientes[0][1] == 0) {
			$actualizarIdEmpresa = update_bd("clientes","empresa_id = $idEmpresa","LOWER(email) = '$email'");
		}
	} else {
		$lc = listaCliente();
		$inserCliente = insert_bd("clientes","nombre, apellido, email,telefono, nombre_empresa, rut_empresa, tipo_registro, lista_id, rut, fecha_creacion, empresa_id","'$nombre','$apellido','$email','$telefono','$empresa', '$rutEmpresa', 'persona', $lc, '$rutCotizar', NOW(), $idEmpresa");
		$idClienteInsert = mysqli_insert_id($conexion);
		$current_user = $idClienteInsert;
	}
	//usuario no logueado
}
		


$date = date('Ymdhis');
$oc = "COT_$date";


// $subtotal = round(get_total_price($comuna_id));
// $total = $subtotal;

//CART EXPLODE AND COUNT ITEMS
if(!isset($_COOKIE['cart_alfa_cm'])){
	setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}  
$cart = $_COOKIE['cart_alfa_cm'];
$items = explode(',',$cart);
foreach ($items as $item) {
	$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
}
if(!isset($_COOKIE['colorProd'])){
	setcookie('colorProd', "", time() + (365 * 24 * 60 * 60), "/");
}
$colorProducto = json_decode(stripslashes( $_COOKIE['colorProd']) , true);
// var_dump($colorProducto);
if(!isset($_COOKIE['cantColorProd'])){
	setcookie('cantColorProd', "", time() + (365 * 24 * 60 * 60), "/");
}
$cantColorProducto = json_decode(stripslashes( $_COOKIE['cantColorProd']) , true);

if ($_COOKIE['cart_alfa_cm']!="") {
	
	$estado_pendiente = 1;
	$campos = "oc, 
	cliente_id,
	vendedor_id, 
	fecha, 
	estado_id, 
	total, 
	nombre, 
	apellido,
	rut,
	telefono, 
	email, 
	empresa,
	rut_empresa, 
	comentarios,
	asociada";
	$values = "'$oc', $current_user, $idVendedor, NOW(), $estado_pendiente, 0, '$nombre', '$apellido', '$rutCotizar', '$telefono', '$email', '$empresa', '$rutEmpresa', '$comentarios', '$asociar'";
	
    $generar_pedido = insert_bd("cotizaciones","$campos","$values");
			
	$cotizacion_id = mysqli_insert_id($conexion);
	$cantTotal = 0;
		
		foreach ($contents as $id=>$qty) {
			// Defino la cantidad de colores del producto
			$logo = "";
			$colorProd = "";
			$idCantColor = 0;
			$idColor = 0;
			if ($cantColorProducto[$id] != 0) {
				$idCantColor = $cantColorProducto[$id];
				$cantColor = consulta_bd("l.nombre", "logo l", "l.id = $idCantColor", "l.nombre asc");
				$logo = $cantColor[0][0];
			}
			if ($colorProducto[$id] != 0) {
				$idColor = $colorProducto[$id];
				$color = consulta_bd("c.nombre", "color_productos c", "c.id = $idColor", "c.nombre asc");
				$colorProd = $color[0][0];
			}
			//consulto el precio segun la lista de precios
			$prod = consulta_bd("pd.precio, pd.descuento, p.pack, p.codigos, pd.sku","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $id","");
			////Veo si el producto es un pack, lo manejo de cierta forma///////////////////////
			if($prod[0][2] == 1){
				
				$precio_final = $prod[0][0];
				$codigo_pack = $prod[0][4];
				$codigosProductos = trim($prod[0][3]);
				$productosPack = explode(",", $codigosProductos);
				
				
				//saco la suma de los productos reales
				$totalReal = 0;
				foreach($productosPack as $skuPack) {
					$valorProdDetalle = consulta_bd("precio","productos_detalles","sku = '$skuPack'","");
					$totalReal += $valorProdDetalle[0][0];
				}
				//obtengo el porcentaje de descuento
				$descPorcentajeProducto = ($totalReal - $precio_final)/$totalReal;
				
				
				$retorno = "";
				foreach($productosPack as $skuPack) {
					$valorProdDetalle2 = consulta_bd("precio, id","productos_detalles","sku = '$skuPack'","");
					$idProductoDetalle = $valorProdDetalle2[0][1];
					$precioUnitario = $valorProdDetalle2[0][0] * (1 - $descPorcentajeProducto);
					$precioTotal = round($precioUnitario * $qty);
					
					$sku = $skuPack;
					/* $precio = $all_sku[0][1]; */
		
					$campos = "cotizacion_id, productos_detalle_id, cantidad, codigo, fecha_creacion, codigo_pack, logo, color,id_color,id_logo";
					$values = "$cotizacion_id,'$idProductoDetalle','$qty','$sku', NOW(), '$codigo_pack', '$logo', '$colorProd','$idColor', '$idCantColor'";
					$detalle = insert_bd("productos_cotizaciones","$campos","$values");
					$cantTotal = $cantTotal + $qty;
				}//fin foreach
				
				
				
			} else {
				
				if($prod[0][1] > 0){
					$precio_final = $prod[0][1];
					$descuento = round(100 - (($prod[0][1] * 100) / $prod[0][0]));
				}else{
					$precio_final = $prod[0][0];
					$descuento = 0;
				}
				
				$precioUnitario = $precio_final;
				$precioTotal = $precioUnitario * $qty;
				$porcentaje_descuento = $descuento;
				
				$all_sku = consulta_bd("sku","productos_detalles","id = '$id'","");
				$sku = $all_sku[0][0];
				/* $precio = $all_sku[0][1]; */
	
				$campos = "cotizacion_id, productos_detalle_id, cantidad, codigo, fecha_creacion, descuento, logo, color, id_color, id_logo";
				$values = "$cotizacion_id, $id, $qty, '$sku', NOW(), $porcentaje_descuento, '$logo', '$colorProd', '$idColor', '$idCantColor'";
				$detalle = insert_bd("productos_cotizaciones","$campos","$values");
				$cantTotal = $cantTotal + $qty;
			}//fin del ciclo
		}

		$qtyFinal = update_bd("cotizaciones","cant_productos = $cantTotal, fecha_creacion = NOW()","id=$cotizacion_id");
		
		//elimino cookie
		setcookie('cart_alfa_cm', null, -1, '/');
		setcookie('cantColorProd', null, -1, '/');
		setcookie('colorProd', null, -1, '/');
		
		//envio notificacion
		// enviarComprobanteClienteCotizacion($oc);
        // enviarComprobanteAdminCotizacion($oc);
		$envioCliente = enviarComprobanteClienteCotizacion($oc);
		$envioAdministradores = enviarComprobanteAdminCotizacion($oc);
		if ($envioCliente == 1) {
			header("location: ../exito-cotizacion?oc=$oc");
		}else{
			header("location: ../carro-cotizacion?error=Ocurrio un error al realizar el envio de la cotización");
		}
	
} else {
	header("location:404");
}
?>
