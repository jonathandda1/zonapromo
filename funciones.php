<?php 
function ofertaGrilla($id){
	$monto = consulta_bd("precio, descuento","productos_detalles","producto_id= $id","");
	$cantInt = count($monto);
	if($cantInt == 0){
		return "vacio";
	}
	if($monto[0][1] > 0 and $monto[0][1] < $monto[0][0]){
		$valorDescuento = (1-($monto[0][1] / $monto[0][0]))*100;
		$valores = '<div class="etiqueta-oferta etiqueta-oferta-left">-'.round($valorDescuento).'%</div>';
		return $valores;
	} else {
		$valores = '';
		return $valores;
	}
}

function precioGrilla($id){
	$monto = consulta_bd("precio, descuento","productos_detalles","producto_id=$id","id asc");
	$cantInt = count($monto);
	if($cantInt == 0){
		return "vacio";
	}
	if($monto[0][1] > 0 and $monto[0][1] < $monto[0][0]){
		$valores = '<p class="precio"><h itemtype="price">$'.number_format($monto[0][1],0,",",".").'</h><br><small>Normal: $'.number_format($monto[0][0],0,",",".").'</small></p>';
		return $valores;
	} else {
		$valores = '<p class="precio" itemtype="price">$'.number_format($monto[0][0],0,",",".").'</p>';
		return $valores;
	}
}

//funcion para manejar los descuento

//funcion para manejar los descuento segun linea categoria o subcategoria
function precioGrillaDescuentosGeneral($id){
	//id, corresponde al id puntual de cada producto
	$id = $id;
	$descuento = 0;
	$descuentoEspecifico = 0;
	//consulto si el producto tiene descuento, si lo tiene veo a que porcentage corresponde el descuento y lo guardo en una variable
	$monto = consulta_bd("pd.precio as precio, pd.descuento as descuento, p.pack as pack","productos_detalles pd, productos p","pd.producto_id = p.id and p.id = $id","p.id asc");
	$cantInt = count($monto);
	if($monto[0][1] > 0){
		$montoFinal = $monto[0][1];
		$descuento = 100 - ($monto[0][1] *100)/$monto[0][0];
		$descuentoEspecifico = $descuento;
	} else {
		$montoFinal = $monto[0][0];
	}
	
	//recorro todas las categorias a las que pertenece el producto y veo el porcentaje de descuento
	$dCat = consulta_bd("DISTINCT(c.id), c.nombre, c.descuento","categorias c, categorias_productos cp","c.id = cp.categoria_id and cp.producto_id = $id","");
	for($i=0; $i<sizeof($dCat); $i++) {
		//si el descrunto de la categoria es mayor al del producto, le asigno el descuento mayor.
		if($dCat[$i][2] > $descuento){
			$descuento = $dCat[$i][2];
			} else {
				$descuento = $descuento;
				}
		}
	
	
	$dSubcat = consulta_bd("DISTINCT(sc.id) as id, sc.nombre as nombre, sc.descuento as descuento","subcategorias sc, categorias_productos cp","sc.id = cp.subcategoria_id and cp.producto_id = $id","");
	
	for($i=0; $i<sizeof($dSubcat); $i++) {
		//si el descrunto de la categoria es mayor al del producto, le asigno el descuento mayor.
		if($dSubcat[$i][2] > $descuento){
			$descuento = $dSubcat[$i][2];
			} else {
				$descuento = $descuento;
				}
		}
		
		
	
	$dLinea = consulta_bd("DISTINCT(l.id) as id, l.nombre as nombre, l.descuento as descuento","categorias c, categorias_productos cp, lineas l","l.id = c.linea_id and c.id = cp.categoria_id and cp.producto_id = $id","");
	
	for($i=0; $i<sizeof($dLinea); $i++) {
		//si el descrunto de la categoria es mayor al del producto, le asigno el descuento mayor.
		if($dLinea[$i][2] > $descuento){
			$descuento = $dLinea[$i][2];
			} else {
				$descuento = $descuento;
				}
		}
	
	
	
	if($monto[0][2] == 1){
		$descuento = 0;
		$pack = 1;
	} else {
		$pack = 0;
	}
	// en el caso que el producto puntual ya tiene descuento, solo le aplico ese descuento y no el de las lineas
	if($descuentoEspecifico > 0){
		$descuento = 100 - ($monto[0][1] *100)/$monto[0][0];
	}
	$valorConDescuento = round($monto[0][0] * (1 - ($descuento /100)));
	$descuento;
	$valorOriginal = $monto[0][0];
	
	
	if($cantInt == 0){
		return "vacio";
	}
	
	if($descuento > 0){
		$valores = '<p class="precio"><h itemtype="price">$'.number_format($valorConDescuento,0,",",".").'</h><br><small>Normal: $'.number_format($valorOriginal,0,",",".").'</small></p>';
		
		$htmlDescuento = '<div class="etiqueta-oferta etiqueta-oferta-left">-'.round($descuento).'%</div>';
		//return $valores;
	} else {
		$valores = '<p class="precio" itemtype="price">$'.number_format($valorOriginal,0,",",".").'</p>';
		$htmlDescuento = "";
		//return $valores;
	}
	
	
	return array(
				"descuento" => $descuento, 
				"valorOriginal" => $valorOriginal, 
				"valorConDescuento" => $valorConDescuento,
				"htmlGrilla" => $valores,
				"htmlDescuento" => $htmlDescuento,
				"packProductos" => $pack
				);
	
}
//fin funcion para manejar los descuento segun linea categoria o subcategoria
//deje una funcion similar en function inc solo para armar el carro y revisar el precio final


?>