<?php
header('Content-type: text/html; charset=utf-8');
include("admin/conf.php");
include 'Mailin.php';
/********************************************************\
|  Alfastudio Ltda - Mail sender V0.5			 		 |
|  Fecha Modificacion: 16/06/2011		                 |
|  Todos los derechos reservados © Alfastudio Ltda 2011  |
|  Prohibida su copia parcial o total, 					 |
|  venta, comercializacion o distribucion				 |
|  https://www.moldeable.com/                             |
\********************************************************/

//******************Cambiar sólo estos valores*************************//



$nombre_sitio = opciones("nombre_cliente");
$nombre_corto = opciones("dominio");
$noreply = "no-reply@".$nombre_corto;
$url_sitio = "https://".$nombre_corto;
$correo_venta = opciones("correo_venta");
$color_logo = opciones("color_logo");
	
$logo = opciones("logo_mail");
$campo_oculto = 'validacion';
$para = 'contacto@zonapromo.cl';
$asunto = "Contacto $nombre_sitio";
$ruta_retorno = 'contacto';
$ruta_retorno_exito = 'exito-contacto';
$campo_mensaje = 'mensaje';
$obligatorios = "nombre, email, $campo_mensaje";
$lang = (isset($_GET[lang])) ? "ing": "esp";



$file_field = ""; //Nombre del campo del archivo adjunto, dejar vacÃ­o si no se va a usar

$from = "noreply@zonapromo.cl";
$save_in_db = true;
if ($save_in_db)
$insert = insert_entry("validacion", "contactos");

//Campos del formulario correspondiente a nombre e email.
$nombre_cl = $_POST['nombre'];
$email_cl = $_POST['email'];

//******************Comienzo del código, no modificar*********************//

if ($_POST[$campo_oculto] == ''){
	$vars = get_post('');
	$obligatorios = explode(',', $obligatorios);
    
    
	foreach($obligatorios as $o)
	{
		$campo = trim($o);
		if ($_POST["$campo"] === '')
		{
            $error = ($lang == 'esp') ? "Los campos marcados con * son necesarios." : "* indicates requiered fields.";
			header("location:$ruta_retorno?msje=$error");
            die("");
            
		}
        
	}

?>

<?php

$msg ='
<html>
    <head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap" rel="stylesheet">
    <title>'.$nombre_sitio.'</title>
    </head>
    <body>
<!--[if mso]>
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="800">
 <![endif]-->

 <!--[if !mso]> <!---->
<table border="0" style="max-width: 800px; padding: 10px; margin:0 auto; border-collapse: collapse;">
 <!-- <![endif]-->
	<tr>
		<td style="background-color: #f1f1f3; padding: 0">
		<div style="color: #34495e; margin: 10px 20px 10px 20px; text-align: justify;font-family: sans-serif">
			<a href="'.$url_sitio.'">
				<img width="255" style="display:block; margin: 10px 20px 10px 20px;" src="'.$logo.'" alt="'.$logo.'"/>
			</a>
		</div>
		</td>
	</tr>	
	<tr>
		<td style="background-color: #F9F9F9">
			<div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
				<p style="font-size:36px; color: '.$color_logo.';"><strong>Hola, '.$nombre_sitio.':</strong></p>
                <p>Se ha enviado una consulta a trav&eacute;s del formulario web Servicio tecnico. <br />Los datos de la persona que contacta son:<br /></p>
                <ul>';

                foreach($_POST as $key=>$val)
                {

                    if ($key != 'enviar' AND $key != $campo_mensaje AND $key != '')
                    {
                        $nombreVal = ucwords($key);
                        $msg .= "<li style='min-height:30px;'>".$nombreVal.": ".htmlentities($val,ENT_QUOTES,"UTF-8")."</li>";
                    }
                }
                $msg .= '</ul>
                <p align="justify" style="font-size:20px; color: #009be5;">El cliente ha dejado el siguiente mensaje: <br /><br />
                    <em>'.htmlentities($_POST[$campo_mensaje],ENT_QUOTES,"UTF-8").'</em>
                </p>

                <p>Muchas gracias<br /> Atte,</p>
                <p><strong>Equipo de '.$nombre_sitio.'</strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td style="background-color: #f1f1f3">
			<div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
			<table cellspacing="0" cellpadding="0" border="0" width="100%" style="background:#f1f1f3;">
                <tr>
                    <th colspan="4" align="left">
                        <p style="font-size:26px; color: '.$color_logo.';"><strong>¿Por qué comprar en '.$nombre_sitio.'?</strong></p>
                    </th>
                </tr>    
                <tr>
                    <td align="center" style="width:25%;">
                        <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_envio.png" alt="Imagen de beneficios" class="">
                    </td>
                    <td align="center" style="width:25%;">
                        <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_calidad.png" alt="Imagen de beneficios" class="">
                    </td>
                    <td align="center" style="width:25%;">
                        <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_pagoSeguro.png" alt="Imagen de beneficios" class="">
                    </td>
                    <td align="center" style="width:25%;">
                        <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_medioPago.png" alt="Imagen de beneficios" class="">
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width:25%;">
                        <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Envio todo Chile</p>
                    </td>
                    <td align="center" style="width:25%;">
                        <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Calidad y precios bajos</p>
                    </td>
                    <td align="center" style="width:25%;">
                        <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Pago Seguro</p>
                    </td>
                    <td align="center" style="width:25%;">
                        <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Todo medio de pago</p>
                    </td>
                </tr>
            </table>
                
            </div>
        </td>
    </tr>
    <tr>
		<td style="background-color: #252c39">
			<div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
				<p style="color:#fff; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                        <p style="color:#fff; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                        <p>
                            <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                            </a>
                            <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                            </a>
                        </p>

                        <p>
                            <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                            <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                        </p>
			</div>
        </td>
    </tr>
</table>
</body>
</html>';
// echo $msg."<br>";



	$msg3 = '
	<html>
	    <head>
	    <link rel="preconnect" href="https://fonts.googleapis.com">
	    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap" rel="stylesheet">
	    <title>'.$nombre_sitio.'</title>
	    </head>
	    <body>
	<!--[if mso]>
	    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="800">
	<![endif]-->

	<!--[if !mso]> <!---->
	   <table border="0" style="max-width: 800px; padding: 10px; margin:0 auto; border-collapse: collapse;">
	<!-- <![endif]-->
    		<tr>
    			<td style="background-color: #f1f1f3;">
    			<div style="color: #34495e; margin: 10px 20px 10px 20px; text-align: justify;font-family: sans-serif">
    				<a href="'.$url_sitio.'">
    					<img width="255" style="display:block; margin: 10px 20px 10px 20px;" src="'.$logo.'" alt="'.$logo.'"/>
    				</a>
    			</div>
    			</td>
    		</tr>
            <tr>
                <td style="background-color: #F9F9F9">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                        <p style="font-size:36px; color: '.$color_logo.';"><strong>Hola, '.$nombre_cl.':</strong></p>
                        <p>Hemos recibido tu solicitud de contacto, pronto nos comunicaremos contigo.<br /></p>
                        <p>Muchas gracias.<br /> Atte,</p>
                        <p><strong>Equipo de '.$nombre_sitio.'</strong></p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #f1f1f3">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="background:#f1f1f3;">
                        <tr>
                            <th colspan="4" align="left">
                                <p style="font-size:26px; color: '.$color_logo.';"><strong>¿Por qué comprar en '.$nombre_sitio.'?</strong></p>
                            </th>
                        </tr>    
                        <tr>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_envio.png" alt="Imagen de beneficios" class="">
                            </td>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_calidad.png" alt="Imagen de beneficios" class="">
                            </td>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_pagoSeguro.png" alt="Imagen de beneficios" class="">
                            </td>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_medioPago.png" alt="Imagen de beneficios" class="">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Envio todo Chile</p>
                            </td>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Calidad y precios bajos</p>
                            </td>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Pago Seguro</p>
                            </td>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Todo medio de pago</p>
                            </td>
                        </tr>
                    </table>
                        
                    </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #252c39">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                        <p style="color:#fff; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                                <p style="color:#fff; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                                <p>
                                    <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                                    </a>
                                    <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                                    </a>
                                </p>

                                <p>
                                    <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                                    <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                                </p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>';
    // echo $msg3."<br>";
}
// $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
//             $mailin->
//                 addTo('jonathandda@live.com','Portal')->
//                 addTo('jonathandda@gmail.com','Portal')->
//                 setFrom('portal@zonapromo.cl', 'Zona Promo')->
//                 setSubject("$asunto")->
//                 setText("$msg2")->
//                     setHtml("$msg2");
//             $res = $mailin->send();
//             $res2 = json_decode($res);


$correo = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : 0;
if ($_POST['email']) {
    $msg2 .= '
    <html>
        <head>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body>
    <!--[if mso]>
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="800">
    <![endif]-->

    <!--[if !mso]> <!---->
       <table border="0" style="max-width: 800px; padding: 10px; margin:0 auto; border-collapse: collapse;">
    <!-- <![endif]-->
            <tr>
                <td style="background-color: #f1f1f3; padding: 0">
                <div style="color: #34495e; margin: 10px 20px 10px 20px; text-align: justify;font-family: sans-serif">
                    <a href="'.$url_sitio.'">
                        <img width="255" style="display:block; margin: 10px 20px 10px 20px;" src="'.$logo.'" alt="'.$logo.'"/>
                    </a>
                </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #F9F9F9">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                        <p style="font-size:36px; color: '.$color_logo.'; margin-bottom:0;"><strong>Hola, '.$usuario.':</strong></p>
                        <p>Se ha solicitado recuperar clave a través de la página web '.$nombre_sitio.'. <br />
                        se recomienda cambiarla en cuanto reciba este correo:<br /></p>
                        <p>Nueva clave: <strong>'.$pass_nueva.'</strong></p>
                        <p>Muchas gracias<br /> Atte,</p>
                        <p><strong>Equipo de '.$nombre_sitio.'</strong></p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #f1f1f3">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="background:#f1f1f3;">
                        <tr>
                            <th colspan="4" align="left">
                                <p style="font-size:26px; color: '.$color_logo.';"><strong>¿Por qué comprar en '.$nombre_sitio.'?</strong></p>
                            </th>
                        </tr>    
                        <tr>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_envio.png" alt="Imagen de beneficios" class="">
                            </td>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_calidad.png" alt="Imagen de beneficios" class="">
                            </td>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_pagoSeguro.png" alt="Imagen de beneficios" class="">
                            </td>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_medioPago.png" alt="Imagen de beneficios" class="">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Envio todo Chile</p>
                            </td>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Calidad y precios bajos</p>
                            </td>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Pago Seguro</p>
                            </td>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Todo medio de pago</p>
                            </td>
                        </tr>
                    </table>
                        
                    </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #252c39">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                        <p style="color:#fff; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                                <p style="color:#fff; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                                <p>
                                    <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                                    </a>
                                    <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                                    </a>
                                </p>

                                <p>
                                    <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                                    <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                                </p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>';
 // echo $msg2;
}
// $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
//             $mailin->
//                 addTo('jonathandda@live.com','Portal')->
//                 addTo('jonathandda@gmail.com','Portal')->
//                 setFrom('portal@zonapromo.cl', 'Zona Promo')->
//                 setSubject("$asunto")->
//                 setText("$msg2")->
//                     setHtml("$msg2");
//             $res = $mailin->send();
//             $res2 = json_decode($res);

enviarComprobanteCliente('OC_2021092501471712');
function enviarComprobanteCliente($oc){

    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
    $color_logo = opciones("color_logo");
    $msje_despacho = 'Su pedido fue recibido y será procesado para despacho.';
    $datos_cliente = consulta_bd("nombre,email,id,direccion, medio_de_pago, retiro_en_tienda, region","pedidos","oc = '$oc'","");
    
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
      
    $id_pedido = $datos_cliente[0][2];
    
    //$id_pedidoAdminitrador = $datos_cliente[0][4];  
    $medioPago = $datos_cliente[0][4];
      
    $detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack","productos_pedidos","pedido_id=$id_pedido and codigo_pack is NULL","");
    
    $detalle_pack = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, count(codigo_pack) as cantidad_articulos","productos_pedidos","pedido_id=$id_pedido and codigo_pack <> '' group by codigo_pack","");
    
    $despacho = $datos_cliente[0][3];
    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="padding: 10px 10px 10px 0px; border-bottom: 2px solid #ccc;">';
           
    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id = ".$detalle_pedido[$i][0],"");
                        $id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs,marca_id";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $id_marca = $productos[0][3];
                        $marca = consulta_bd("nombre","marcas","id='$id_marca'","");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td style="border-bottom: 2px solid #f1f1f3;" valign="top" align="left" width="23%">
                                                <p style="float:left; margin:3px 3px 3px 0; ">
                                                    <img style="width:100px; border: solid 1px #DDDCDC; height:100px;" src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].'" width="100" height="100"/>
                                                </p>
                                                
                                            </td>';
                        $tabla_compra .= ' 
                                            <td valign="top" align="left" width="27%" style="border-bottom: 2px solid #f1f1f3;color:#797979;">
                                                <p style="float:left; width:100%; margin:3px 3px 3px 0; font-weight: bold; color: #000; font-size:14px; font-family: Nunito Sans, sans-serif;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; color:#297edd; font-weight:900; font-size:16px; margin:0 0 5px 0;">$'.number_format($detalle_pedido[$i][2],0,",",".").'</p>
											</td>
                                            <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #f1f1f3;color:#797979;">
                                            <p style="text-align: center; color:#297edd; font-size:12px; float:left; width:100%; margin:10px 0 5px 0;">Cantidad</p>
                                            <p style="width:100%; margin: 10px 0 10px 0;">
                                                <div style="width:30%; float:left; text-align: center; line-height: 30px;">-</div>
                                                <div style="width:40%; height: 30px; max-width:30px; float:left; border: 1px solid #d7d7d7; border-radius: 50%; font-size: 12px; background-color: #1f80d6; color: #fff; line-height: 30px; text-align: center;">'.$detalle_pedido[$i][1].'</div>
                                                <div style="width:30%; float:left; text-align: center; line-height: 30px;">+</div>
                                            </p>
											</td>';
                        $tabla_compra .= '  <td valign="top" align="right" width="20%" style="border-bottom: 2px solid #f1f1f3;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">Subtotal</p>
                                                <p style="color:#297edd; float:left; width:100%; margin:0 0 5px 0;font-weight:900; font-size:15px; ">$'.number_format(($detalle_pedido[$i][2]*$detalle_pedido[$i][1]),0,",",".").'</p>			
											</td>'; //nombre producto
                        
                        $tabla_compra .= '</tr>';

                    }
                    
                    
                    for ($i=0; $i <sizeof($detalle_pack) ; $i++) {
                        $skuPack = $detalle_pack[$i][4];
                        
                        $cantProdPack = $detalle_pack[$i][5];
                        $pDP = consulta_bd("p.codigos, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id = pd.producto_id and pd.sku = '$skuPack'","");
                        $productosPorPack = explode(",", $pDP[0][0]);
                        $cantArreglo = count($productosPorPack);
                    
                        
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","sku='$skuPack'","");
                        $id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        
                        if($pDP[0][2] > 0){
                            $precio_unitario = $pDP[0][2];
                        } else {
                            $precio_unitario = $pDP[0][1];
                        }
                        //die("$cantProdPack");
                        $cantidad = $detalle_pack[$i][1];
                        $subtotalFila = $precio_unitario;

                        
                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
                                                <p style="float:left; width:100%; margin:10px 0 5px 0;">
                                                    <img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].' " width="90%"/>
                                                </p>
                                                
                                            </td>';
                        $tabla_compra .= '  <td  align="left" width="70%" >
                                                <p style="float:left; width:100%; margin:10px 0 5px 0;"><strong>'.$productos[0][1].'</strong></p>
                                                <p style="float:left; width:100%; margin:0 0 5px 0;">'.$skuPack.'</p>
                                                <p style="float:left; width:100%; margin:0 0 5px 0;">'.$cantidad.'</p>
                                                <p style="float:left; width:100%; margin:0 0 5px 0;">$'.number_format($subtotalFila,0,",",".").'</p>
                                            </td>'; //nombre producto
                        $tabla_compra .= '</tr>';

                    }
                    

                 

            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado, costo_instalacion","pedidos","oc = '$oc'","");
            
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][4],0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

            if($totales[0][1] > 0){
                $tabla_compra .='<tr class="cart_total_price">';
                $tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
                $tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][1],0,",",".").'</span></td>';
                $tabla_compra .='</tr>';
            }
            
            
    
            if($totales[0][3] > 0){
                $tabla_compra .='     <tr class="cart_total_price">';
                $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Valor envío:</span></td>';
                $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][3],0,",",".").'</span></td>';
                $tabla_compra .='     </tr>'; 
            }
            
    
    
            

    
            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'; float:right;">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color: #297edd; font-size:16px; font-family: Nunito Sans, sans-serif; text-align:right;">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            // $tabla_compra .='<br /><br />';

    
    
    $msg2 = '
    <html>
        <head>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body>
    <!--[if mso]>
        <table style="border-top:4px solid #1b61ad;" align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="650">
    <![endif]-->

    <!--[if !mso]> <!---->
       <table border="0" style="border-top:4px solid #1b61ad; max-width: 650px; padding: 10px; margin:0 auto; border-collapse: collapse;">
    <!-- <![endif]-->
            <tr>
                <td style="background-color: #fff; padding: 0">
                <div style="color: #34495e; margin: 10px 20px 10px 20px; text-align: justify;font-family: sans-serif">
                    <a href="'.$url_sitio.'">
                        <img width="200" style="display:block; margin: 10px 20px 10px 0;" src="'.$logo.'" alt="'.$logo.'"/>
                    </a>
                </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #fff">
                    <div style="color: #34495e; margin: 20px 10px 10px 20px; text-align: justify;font-family: sans-serif">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="text-align: justify;">';
                            if($medioPago === "transferencia"){
                            //inicio medio de pago es transferencia
                            $msg2 .= '<tr>
                                            <th align="left" width="100%" style="color:#797979;"> 
                                                <p style="text-transform: uppercase;float:right;width:100%; font-size:20px;">Hola, '.$nombre_cliente.'</p>
                                                <p style="float:right;width:100%;margin: 0px;">Gracias por su compra con transferencia</p>
                                                <p style="float:right;width:100%;margin: 0 0 10px 0;">Su número de compra es:<strong style="color:#1f80d6; text-decoration: underline">'.$oc.'</strong></p>
                                                
                                                <p style="text-align: justify;">A continuación encontrarás los datos para que realices la transferencia antes de 48 horas, después de ese periodo se anulara el pedido.</p>

                                                <p style="text-align: justify;">Para asistencia sobre el pago o dudas del producto, por favor contáctenos al mail <strong style="color:#1f80d6; text-decoration: underline">ventas@zonapromo.cl</strong>, en horario de atención de tienda.</p>

                                                <p style="text-align: justify;">En el caso que el producto incluya despacho pagado o por pagar el o los productos serán enviados 24 horas después de haber realizado el pago, previa confirmación de la transacción. En el caso que la transacción se realice un día viernes, fin de semana o feriado el despacho se realizara en los primeros días hábiles siguientes.</p>

                                                <p style="text-align: justify;">
                                                Banco: Banco Santander / Banefe<br>
                                                Cuenta Corriente: 0234006318<br>
                                                Rut: 11.111.111-1<br>
                                                Nombre: Zona Promo <br>
                                                Email: <strong style="color:#1f80d6; text-decoration: underline">ventas@zonapromo.cl</strong></p>
                                            </th>
                                        </tr>';
                            //fin medio de pago es transferencia
                        } else if($medioPago == "notaVenta"){
                            //inicio medio de pago es Nota de venta
                            $msg2 .= '<tr>
                                            <th align="left" width="100%" style="color:#797979;"> 
                                                <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Hola, '.$nombre_cliente.'</p>
                                                <p style="float:right;width:100%;margin: 0px;">Gracias por su compra</p>
                                                <p style="float:right;width:100%;">Su número de compra es: <br />
                                                <strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                                
                                                <p style="float:right;width:100%;margin: 15px 0px;">
                                                    <a style="display:inline-block; color:#fff;padding:10px 20px 10px 20px; margin: 5px 10px; text-decoration:none; border-radius:5px; background-color:#2677d2;" href="'.$url_sitio.'/tienda/voucher/voucherCompra.php?oc='.$oc.'">
                                                        VER VOUCHER
                                                    </a>
                                                </p>
                                                <br><br>
                                                <p style="float:right;width:100%;margin: 0 0 10px 0;">Su compra fue realizada con nota de venta y tiene opcion de pago de XXX dias (Consultar dias con GGH).</p>
                                            </th>
                                        </tr>';
                            //fin medio de pago es Nota de venta
                        } else {
                            //inicio medio de pago es transbank o mercado pago
                            $msg2 .= '<tr>
                                        <th align="left" width="100%" style="color:#7b7c7d;"> 
                                            <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:30px;color: '.$color_logo.';"><strong>Hola, '.$nombre_cliente.'</strong></p>
                                            <p style="float:right;width:100%;margin: 0px; padding:10px 0; font-size: 20px; color:#7b7c7d;">Gracias por tu compra</p>
                                            <p style="float:right;width:100%;margin: 0px;">Su número de compra es: <strong style="color:#1f80d6; text-decoration: underline; font-size: 20px;">'.$oc.'</strong></p>
                                            
                                            <p style="float:right;width:100%;margin: 15px 0px;">
                                                <a style="display:inline-block; color:#fff;padding:15px 20px 15px 20px; margin: 5px 0 5px 0; text-decoration:none; border-radius:5px; background-color:#2677d2;" href="'.$url_sitio.'/boucher?oc='.$oc.'">
                                                    VER COMPROBANTE
                                                </a>
                                                
                                            </p>
                                        </th>
                                    </tr>';
                        //fin medio de pago es transbank o mercado pago
                        }
        $msg2 .= '        
                        </table>
                    </div>
                </td>
            </tr>';
                        
        $msg2 .= '
            <tr>
                <td style="background-color: #fff">
                    <div style="color: #34495e; margin: 20px 10px 10px 10px; text-align: justify;font-family: sans-serif">
                        <table style="border-bottom: 2px solid #ccc;font-family: Nunito Sans, sans-serif;padding:10px;font-weight:bold;" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #009be5; font-size:28px; font-family: Nunito Sans, sans-serif;">Productos comprados</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="left" style="margin:0;color:#7b7c7d; font-size:20px; font-family: Nunito Sans, sans-serif;">'.$msje_despacho.'Gracias.</p> 
                                    <p align="left" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong style="color:#7b7c7d;">Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
                        <br/>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="border-top:2px solid #1b61ad; background-color: #f5f6f7; color:#8b8b8b;">
                    <div style="color: #34495e; margin: 20px 10px 10px 10px; text-align: justify;font-family: sans-serif">
                        <p style="color:#8b8b8b;; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                                <p style="color:#8b8b8b;; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                                <p>
                                    <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                                    </a>
                                    <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                                    </a>
                                </p>

                                <p>
                                    <a style="color:#8b8b8b;; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                                    <a style="color:#8b8b8b;; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                                </p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>';

// echo $msg2;
	//return $msg2;

    //die();
    //save_in_mailchimp($oc, 'exito');
    //save_in_mailchimp($oc, 'todas_compras');
    
	
    
    // $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
    // $mailin->
    //     addTo($email_cliente, $nombre_cliente)->
    //     setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
    //     setSubject("$asunto")->
    //     setText("$msg2")->
    //         setHtml("$msg2");
    // $res = $mailin->send();
    // $res2 = json_decode($res);

    // if ($res2->{'result'} == true) {
    //     return 1;
    // } else {
    //     return 2;
    // }
}

enviarComprobanteAdmin('OC_2021092710252713',"","");
$msg2 = "";

function enviarComprobanteAdmin($oc, $imprimir, $correoForzarNotificacion){

    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
    $color_logo = opciones("color_logo");
    
    
    $fontFamily = ($imprimir == 1) ? "font-family: Trebuchet MS, sans-serif;" : "font-family: Open Sans, sans-serif;";
    
    /*
$email_admin = 'ventas@moldeable.com';
    $email_admin = 'htorres@moldeable.com';
*/
    
    
    $msje_despacho = 'Su pedido sera procesado y despachado dentro de 24 horas.';
    $datos_cliente = consulta_bd("nombre,email,id,direccion","pedidos","oc = '$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
    $id_pedido = $datos_cliente[0][2];
    
    $datos_cliente = consulta_bd("nombre,
    email,
    id,
    direccion, 
    region,
    ciudad,
    comuna,
    direccion,
    telefono,
    rut,
    factura,
    direccion_factura,
    giro,
    email_factura,
    rut_factura,
    fecha_creacion,
    telefono,
    regalo,
    razon_social,
    payment_type_code,
    shares_number, 
    transaction_date, 
    comentarios_envio, 
    nombre_retiro,
    telefono_retiro, 
    rut_retiro, 
    nombre, 
    retiro_en_tienda, 
    medio_de_pago, 
    fecha, 
    cliente_id, 
    
    mp_payment_type, mp_payment_method, mp_auth_code, mp_card_number, mp_id_paid, mp_cuotas, mp_valor_cuotas, mp_transaction_date, sucursal_id","pedidos","oc = '$oc'","");
    $idSucursal = $datos_cliente[0][39];
    $sucursal = consulta_bd("nombre, direccion, horarios","sucursales","id=$idSucursal","");
    $nombre_cliente = $datos_cliente[0][0];
    
    $medioPago = $datos_cliente[0][28];
    
    $method = ($datos_cliente[0][28] == "webpay") ? "tbk" : "mpago";
    
    // $tipo_pago = tipo_pago($datos_cliente[0][19], $datos_cliente[0][20], $method);
    
    $detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, descuento","productos_pedidos","pedido_id = $id_pedido","");
    $despacho = $datos_cliente[0][3];
    
  
    $tabla_compra = '
                <table width="100%"  border="0" cellspacing="0" cellpadding="5" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc; '.$fontFamily.' color:#666; float:left;">
                    <thead>
                    <tr>
                        <th align="left" width="10%" style="border-bottom:2px solid #ccc;"></th>
                        <th align="left" width="40%" style="border-bottom:2px solid #ccc;">Producto</th>
                        <th align="center" width="10%" style="border-bottom:2px solid #ccc;">SKU</th>
                        
                        <th align="center" width="5%" style="border-bottom:2px solid #ccc;">qty</th>
                        <th align="right" width="12%" style="border-bottom:2px solid #ccc;">Precio <br>Unitario</th>
                        <th align="right" width="12%" style="border-bottom:2px solid #ccc;">Total item</th>
                    </tr>
                    </thead>
                <tbody>';
           
                    
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id = ".$detalle_pedido[$i][0],"");
                        $id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id = $id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;
                        
                        if($detalle_pedido[$i][4] != ""){
                            $tabla_compra .= '<tr bgcolor="#efefef">';
                            } else {
                            $tabla_compra .= '<tr>';
                            }
                        
                        
                        $tabla_compra .= '  <td style="border-bottom: 2px solid #ccc;">
                                                <img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].'" width="100%"/>
                                            </td>';
                        $tabla_compra .= '  <td style="border-bottom: 2px solid #ccc;color:#8b8b8b; '.$fontFamily.'">'.$productos[0][1].'</td>'; //nombre producto
                        $tabla_compra .= '  <td align="center" style="border-bottom: 2px solid #ccc;color:#797979;'.$fontFamily.'">'.$detalle_pedido[$i][3].'</td>'; //codigo producto SKU
                        
                        
                        $tabla_compra .= '  <td align="center" style="border-bottom: 2px solid #ccc;color:#797979; '.$fontFamily.'">'.$detalle_pedido[$i][1].'</td>'; //cantidad
                        
                        if($detalle_pedido[$i][5] > 0){
                            $tabla_compra .= '  <td align="right" style="font-weight:bold; border-bottom: 2px solid #ccc; color:#3366ff; '.$fontFamily.'">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; //precio producto
                            $tabla_compra .= '  <td align="right" style="font-weight:bold; border-bottom: 2px solid #ccc; color:#e10613; '.$fontFamily.'">$'.number_format(($detalle_pedido[$i][2]*$detalle_pedido[$i][1]),0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';
                            } else {
                            $tabla_compra .= '  <td align="right" style="font-weight:bold; border-bottom: 2px solid #ccc; color:#3366ff; '.$fontFamily.'">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; 
                            $tabla_compra .= '  <td align="right" style="font-weight:bold; border-bottom: 2px solid #ccc; color:#e10613; '.$fontFamily.'">$'.number_format(($detalle_pedido[$i][2]*$detalle_pedido[$i][1]),0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';
                            }
                        

                    }

                 
            $tabla_compra .= '</tbody>';
            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado, costo_instalacion","pedidos","oc = '$oc'","");
            
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Total neto:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][4]/1.19,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';
            
            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">IVA:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format(($totales[0][4]/1.19)*0.19,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

            if($totales[0][1] > 0){
                $tabla_compra .='<tr class="cart_total_price">';
                $tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
                $tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][1],0,",",".").'</span></td>';
                $tabla_compra .='</tr>';
            }
            
            

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999">Valor envío:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999">$'.number_format($totales[0][3],0,",",".").'</span></td>';
            $tabla_compra .='     </tr>';
    
            

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#e10613;">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';
    

    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $msg2 = "<html>";
    if($imprimir != 1){
        $msg2 .= '
            <head>
            <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
            <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap" rel="stylesheet">
            <title>'.$nombre_sitio.'</title>
            </head>
            <body>
        <!--[if mso]>
            <table style="border-top: 4px solid #1b61ad;" align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="650">
        <![endif]-->

        <!--[if !mso]> <!---->
           <table border="0" style="border-top: 4px solid #1b61ad; max-width: 650px; padding: 10px; margin:0 auto; border-collapse: collapse;">
        <!-- <![endif]-->';

    } else {
    $msg2 .= '<body style="background:#fff; float:left;">
                <table border="0" style="width: 98%; padding: 10px; margin:0 auto; border-collapse: collapse;">';
    }
            
    $msg2 .= '
    <tr>
    <td style="background-color: #fff; padding: 0">
        <div style="color: #34495e; margin: 10px 20px 10px 20px; text-align: justify;font-family: sans-serif">
            <a href="'.$url_sitio.'">
         <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:10px 10px 10px 10px;">
            <tr>
                <th align="left" width="50%">
                    <p>
                        <a href="'.$url_sitio.'+-" style="color:#8CC63F;">
                            <img width="200" style="display:block; margin: 10px 20px 10px 20px;" src="'.$logo.'" alt="'.$logo.'"/>
                        </a>
                    </p>
                </th>
                <th align="right" width="50%"> 
                    <p style="text-transform: uppercase;float:right;width:100%;margin: 10px 20px 0 0; '.$fontFamily.'">'.$nombre_cliente.'</p>
                    <p style="color:#797979;float:right;width:100%;margin: 10px 20px 0 0;line-height:20px; '.$fontFamily.'">Ha generado una compra web.</p>
                    <p style="color:#797979; float:right; width:100%; margin: 10px 20px 0 0; '.$fontFamily.'">Su número de compra es: <strong style="color:#1f80d6; text-decoration:underline; '.$fontFamily.'">'.$oc.'</strong></p>';
                
                if($imprimir != 1 && $medioPago != "transferencia"){
                    $msg2 .= '<p style="float:right;width:100%;margin: 10px 20px 0 0;">
                        <a style="text-decoration: none; float:right; background-color:#2471c8; border-radius:5px; padding:15px 20px 15px 20px; color:#fff;" href="'.$url_sitio.'/tienda/voucher/voucherCompra.php?oc='.$oc.'">
                            Ver voucher
                        </a>
                    </p>';
                    }

        $msg2 .= '</th>
                </tr>
            </table>
            </div>
        </td>
    <tr>';
                        
    if($medioPago == "transferencia"){
      $msg2 .= '
    <tr>
        <td style="background-color: #fff">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">  
            <table width="100%" cellpadding="20" style="background-color: #e10613;color: #fff;">
                <tr>
                    <td>Compra realizada con transferencia electronica, corroborar que se realizo esta antes de enviar los productos.</td>
                </tr>
            </table>
            </div>
        </td>
    <tr>';

    }  else if($medioPago == "notaVenta"){
    $msg2 .= '
    <tr>
        <td style="background-color: #fff">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
            <table width="100%" cellpadding="20" style="background-color: #3366ff;color: #fff;">
                <tr>
                    <td>Compra realizada con Nota de venta, corroborar el credito disponible y los dias maximos permitidos para el pago.</td>
                </tr>
            </table>
            </div>
        </td>
    <tr>';
    }
                                
    $msg2 .= '
    <tr>
        <td style="background-color: #fff">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px; float:left;">
                    <tr>
                        <td valign="top" width="50%">';
                        if($datos_cliente[0][27] == "Instalación a Domicilio"){
                                
                        $msg2.='<h3 style="'.$fontFamily.'">'.$datos_cliente[0][27].'</h3>
                            <h5 style="'.$fontFamily.'">Dirección de instalación</h5>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][3].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Region: </strong>'.$datos_cliente[0][4].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Comuna: </strong>'.$datos_cliente[0][6].'</li>
                                </ul>';
                            }else if ($datos_cliente[0][27] == "Despacho a Domicilio") {
                        $msg2.='<h3 style="'.$fontFamily.'">'.$datos_cliente[0][27].'</h3>
                            <h5 style="'.$fontFamily.'">Dirección de envio</h5>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][3].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Region: </strong>'.$datos_cliente[0][4].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Comuna: </strong>'.$datos_cliente[0][6].'</li>
                                </ul>';
                            }else if ($datos_cliente[0][27] == "Instalación en Taller") {
                        $msg2.='<h3 style="'.$fontFamily.'">'.$datos_cliente[0][27].'</h3>
                                <h5 style="'.$fontFamily.'">Datos de intalación</h5>                                
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Sucursal: </strong>'.$sucursal[0][0].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Dirección: </strong>'.$sucursal[0][1].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Horario: </strong>'.$sucursal[0][2].'</li>
                                </ul>';
                            }else {
                        $msg2.='<h3 style="'.$fontFamily.'">Datos retiro</h3>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][23].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Telefono: </strong>'.$datos_cliente[0][24].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Sucursal: </strong>'.$sucursal[0][0].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Dirección: </strong>'.$sucursal[0][1].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Horario: </strong>'.$sucursal[0][2].'</li>
                                </ul>';
                            }
                                 
                $msg2.='</td>
                        <td valign="top" width="50%">
                            <h3 style="'.$fontFamily.'">Datos usuario</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][0].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Correo: </strong>'.$datos_cliente[0][1].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Teléfono: </strong>'.$datos_cliente[0][8].'</li>
                                </ul>
                            </p>
                        </td>
                    </tr>
                </table>
                </div>
        </td>
    <tr>';
    
    $msg2.='
    <tr>
        <td style="background-color: #fff">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
            <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px; float:left;">

            <tr>';
                if($datos_cliente[0][28] == "notaVenta"){
                   $urlLink = "";
                   $cliente = consulta_bd("nombre, tipo_registro, nombre_empresa, parent, email, telefono","clientes","id = ".$datos_cliente[0][30],"");
                    $usuarioTipo = $cliente[0][1];
                    $usuarioNombreEmpresa = $cliente[0][2];
                    $usuarioParent = $cliente[0][3];
                    
                    if($usuarioParent == 0){
                        $msnv = consulta_bd("mp.mensaje","clientes c, mensajes_pagos mp","mp.id = c.mensajes_pago_id and c.id = ".$datos_cliente[0][30],"");
                    } else {
                        $msnv = consulta_bd("mp.mensaje","clientes c, mensajes_pagos mp","mp.id = c.mensajes_pago_id and c.id = ".$usuarioParent,"");
                    }
                   
                        
                    $msg2.='
                        <td valign="top" width="50%" height="100">
                            <h3 style="'.$fontFamily.'">Datos Nota de venta</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Fecha: </strong>'.$datos_cliente[0][29].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Modo pago: </strong>'.$msnv[0][0].'</li>
                                </ul>
                            </p>
                        </td>';
                } else if($datos_cliente[0][28] == "mercadopago"){
                    $urlLink = "?op=231a";
                     
                    $msg2.='
                        <td valign="top" width="50%" height="100">
                            <h3 style="'.$fontFamily.'">Datos Mercadopago</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de pago: </strong>'.$datos_cliente[0][31].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Método de pago: </strong>'.$datos_cliente[0][32].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Código de autorización: </strong>'.$datos_cliente[0][33].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Número tarjeta: </strong>**** **** **** '.$datos_cliente[0][34].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">ID Transacción: </strong> '.$datos_cliente[0][35].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Cuotas: </strong> '.$datos_cliente[0][36].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Valor cuotas: </strong> '.$datos_cliente[0][37].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Fecha transacción: </strong> '.$datos_cliente[0][38].'</li>
                                </ul>
                            </p>
                        </td>';
                } else if($datos_cliente[0][28] == "transferencia"){
                    $urlLink = "?op=275a";
                    $msg2.='
                        <td valign="top" width="50%" height="100">
                            <h3 style="'.$fontFamily.'">Pago por transferencia</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de pago: </strong>Transferencia</li>                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nota: </strong> Verifique el pago antes de hacer el envio</li>
                                </ul>
                            </p>
                        </td>';
                } else {
                    $urlLink = "?op=231a";
                    $msg2.='
                        <td valign="top" width="50%" height="100">
                            <h3 style="'.$fontFamily.'">Datos Transbank</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de pago: </strong>'.$tipo_pago[tipo_pago].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de cuota: </strong>'.$tipo_pago[tipo_cuota].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nº Cuotas: </strong>'.$tipo_pago[cuota].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Fecha: </strong>'.$datos_cliente[0][21].'</li>
                                </ul>
                            </p>
                        </td>';
                }
                
                   
                $msg2.='<td valign="top" width="50%">';
                        if($datos_cliente[0][14] != ''){
                    $msg2.='<h3 style="'.$fontFamily.'">Datos de facturación</h3>
                            <ul>
                                <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Razon Social: </strong>'.$datos_cliente[0][18].'</li>
                                <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][11].'</li>
                                <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Giro: </strong>'.$datos_cliente[0][12].'</li>
                                <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Rut: </strong>'.$datos_cliente[0][14].'</li>
                            </ul>';
                        }
            $msg2.='</td>
                </tr>
            </table>
            </div> 
        </td>
    </tr>';
                
                           
    $msg2.='
    <tr>
        <td style="background-color: #fff">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="float:left; margin-top:50px;">
                            <tr>
                                <td valign="top">';
                            $msg2.='<h3 style="'.$fontFamily.' margin-bottom:20px;">PRODUCTOS COMPRADOS</h3>
                                    <p>'.$tabla_compra.'</p>';
                         
                        //Muestro los datos solo si es para enviar por correo
                        if($imprimir != 1){
                            $msg2.='<p align="center" style="margin:0;color:#000; '.$fontFamily.'">Para ver el detalle de la compra y datos del cliente puede pinchar el siguiente <a href="'.$url_sitio.'/admin/index.php'.$urlLink.'">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999; '.$fontFamily.'">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;'.$fontFamily.'">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>';
                        }
                        //Muestro los datos solo si es para enviar por correo
                       $msg2.='</td>
                            </tr>
                        </table>
                        </div> 
                    </td>
                </tr>
            <tr style="border-top: 2px solid #1b61ad;">
                <td style="background-color: #f5f6f7">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                        <p style="color:#8b8b8b; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                                <p style="color:#8b8b8b; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                                <p>
                                    <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                                    </a>
                                    <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                                    </a>
                                </p>

                                <p>
                                    <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                                    <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                                </p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>';

     echo $msg2."<br>";

    
    // if($imprimir == 1){
    //     //Muestro el html para transformarlo en un pdf
    //     return $msg2;   
        
    // } else if($correoForzarNotificacion != ""){
    //     //envio la notificacion al correo que me indican en la variable
        
        // $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
        // $mailin->
        //     addTo('jonathandda@live.com', "Notificacion Venta")->
        //     setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        //     setSubject("$asunto")->
        //     setText("$msg2")->
        //         setHtml("$msg2");
        // $res = $mailin->send();
        // $res2 = json_decode($res);

    //     if ($res2->{'result'} == true) {
    //         return 1;
    //     } else {
    //         return 2;
    //     }
        
    //     //envio la notificacion al correo que me indican en la variable
        
    // } else {
    //     //si envio las otras 2 variables vacias ejecuto la funcion con normalidad
        
    //     $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
    //     if(opciones("correo_admin1") != ""){
    //         $mailin-> addTo(opciones("correo_admin1"), opciones("nombre_correo_admin1"));
    //     }
    //     if(opciones("correo_admin2") != ""){
    //         $mailin-> addTo(opciones("correo_admin2"), opciones("nombre_correo_admin2"));
    //     }
    //     if(opciones("correo_admin3") != ""){
    //         $mailin-> addTo(opciones("correo_admin3"), opciones("nombre_correo_admin3"));
    //     }
        
        
        
    //     $mailin->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
    //         setSubject("$asunto")->
    //         setText("$msg2")->
    //             setHtml("$msg2");
    //     $res = $mailin->send();
    //     $res2 = json_decode($res);

    //     if ($res2->{'result'} == true) {
    //         return 1;
    //     } else {
    //         return 2;
    //     }
    //     //si envio las otras 2 variables vacias ejecuto la funcion con normalidad
    // } 

}

$msje = 
	'
	<html>
	<head><link href="https://fonts.googleapis.com/css?family=lato" rel="stylesheet"></head>
	<body style="font-family: \'Open Sans\', sans-serif; color: #333;">
	<div style="background: #fff; width: 100%;">
		<div style="background: #fff; border-top: 2px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px; margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
			<p style="font-size: 14px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
			<p style="font-size: 13px; margin-top: 0;">Se ha enviado un nuevo mensaje de contacto a través de la página web de '.$nombre_sitio.'. <br>
			Los datos de la persona que contacta son:</p>
			<blockquote style="font-size: 13px;">
			<strong>Nombre:</strong> '.$nombre.'<br>
			<strong>E-mail:</strong> '.$email.' <br>
			<strong>Teléfono:</strong> '.$telefono.' <br>
			<strong>Mensaje:</strong> '.$mensaje.' <br>
			</blockquote>

			<p style="font-size: 13px;">Rogamos contactarse lo antes posible con el cliente.<br><br>
			Muchas gracias. <br>
			Atte,<br>
			<b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
		</div>
	</div>
	</body>
	</html>';
    // echo $msje."<br>";

    $msje_cliente = 
	'
	<html>
	<head><link href="https://fonts.googleapis.com/css?family=lato" rel="stylesheet"></head>
	<body style="font-family: \'Open Sans\', sans-serif; color: #333;">
	<div style="background: #f1f1f1; width: 100%;">
		<div style="background: #fff; border-top: 3px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
            <h2 style="font-size: 26px; text-transform:uppercase; margin-bottom:5px;color:'.$color_logo.';">Hola, '.$nombre_cl.':</h2>
			<p style="font-size: 20px; margin-top: 0; color:'.$color_logo.';">Hemos recibido tu solicitud de contacto, pronto nos comunicaremos contigo.</p>
			<p style="font-size: 20px; color:'.$color_logo.';">
			Muchas gracias. <br>
			Atte,<br>
			<b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
		</div>
        <div style="background: #f5f6f7; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
            <p style="color:'.$color_logo.'; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
            <p>
                <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                    <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                </a>
                <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                    <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                </a>
            </p>

            <p>
                <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
            </p>
		</div>
        
	</div>
	</body>
	</html>';
    // echo $msje_cliente."<br>";
?>
	