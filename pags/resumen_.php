<?php 

$email = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : 0;
$nombre = (isset($_POST['nombre'])) ? mysqli_real_escape_string($conexion, $_POST['nombre']) : 0;
$telefono = (isset($_POST['telefono'])) ? mysqli_real_escape_string($conexion, $_POST['telefono']) : 0;

$envio = (isset($_POST['envio'])) ? mysqli_real_escape_string($conexion, $_POST['envio']) : 0; 
$region = (is_numeric($_POST['region'])) ? mysqli_real_escape_string($conexion, $_POST['region']) : 0;
$comuna = (is_numeric($_POST['comuna'])) ? mysqli_real_escape_string($conexion, $_POST['comuna']) : 0;
$direccion = (isset($_POST['direccion'])) ? mysqli_real_escape_string($conexion, $_POST['direccion']) : 0; 

$factura = (isset($_POST['factura'])) ? mysqli_real_escape_string($conexion, $_POST['factura']) : 0; 

$retiro = (isset($_POST['retiro'])) ? mysqli_real_escape_string($conexion, $_POST['retiro']) : 0; 
$nombre_retiro = (isset($_POST['nombre_retiro'])) ? mysqli_real_escape_string($conexion, $_POST['nombre_retiro']) : 0; 
$telefono_retiro = (isset($_POST['telefono_retiro'])) ? mysqli_real_escape_string($conexion, $_POST['telefono_retiro']) : 0; 

$rut_factura = (isset($_POST['rut_factura'])) ? mysqli_real_escape_string($conexion, $_POST['rut_factura']) : 0; 
$giro_factura = (isset($_POST['giro_factura'])) ? mysqli_real_escape_string($conexion, $_POST['giro_factura']) : 0; 
$razon_social = (isset($_POST['razon_social'])) ? mysqli_real_escape_string($conexion, $_POST['razon_social']) : 0; 
$direccion_factura = (isset($_POST['direccion_factura'])) ? mysqli_real_escape_string($conexion, $_POST['direccion_factura']) : 0; 

if ($email === 0){
	echo '<script>parent.location = "envio-y-pago";</script>';
}
if(qty_pro() == 0){
    echo '<script>parent.location = "home";</script>';
}

$nomComuna = consulta_bd("nombre", "comunas", "id = $comuna", "")
?>

<?php if(!isset($_COOKIE['usuario_id'])){ ?>                      
<form method="post" action="envio-y-pago" id="volverForm">
    <input type="hidden" name="recuperar_datos" value="1">    
    <input type="hidden" name="email" value="<?= $email; ?>"> 
    <input type="hidden" name="nombre" value="<?= $nombre; ?>"> 
    <input type="hidden" name="telefono" value="<?= $telefono; ?>"> 
    <input type="hidden" name="envio" value="<?= $envio; ?>"> <!--direccionCliente-->
    <input type="hidden" name="region" value="<?= $region; ?>">
    <input type="hidden" name="comuna" value="<?= $comuna; ?>"> 
    <input type="hidden" name="direccion" value="<?= $direccion; ?>"> 

    <input type="hidden" name="retiro" value="<?= $retiro; ?>"> <!--id de la sucursar de retiro-->
    <input type="hidden" name="nombre_retiro" value="<?= $nombre_retiro; ?>"> 
    <input type="hidden" name="telefono_retiro" value="<?= $telefono_retiro; ?>"> 

    <input type="hidden" name="factura" value="<?= $factura; ?>"> <!--si o no-->
    <input type="hidden" name="rut_factura" value="<?= $rut_factura; ?>"> 
    <input type="hidden" name="giro_factura" value="<?= $giro_factura; ?>"> 
    <input type="hidden" name="razon_social" value="<?= $razon_social; ?>"> 
    <input type="hidden" name="direccion_factura" value="<?= $direccion_factura; ?>">
</form>
<?php } ?>

<form method="post" action="proceso-pago" id="formCompra" class="bl_form">
    <input type="hidden" name="email" value="<?= $email; ?>"> 
    <input type="hidden" name="nombre" value="<?= $nombre; ?>"> 
    <input type="hidden" name="telefono" value="<?= $telefono; ?>"> 
    <input type="hidden" name="envio" value="<?= $envio; ?>"> <!--direccionCliente-->
    <input type="hidden" name="region" value="<?= $region; ?>">
    <input type="hidden" name="comuna" value="<?= $comuna; ?>"> 
    <input type="hidden" name="direccion" value="<?= $direccion; ?>"> 

    <input type="hidden" name="retiro" value="<?= $retiro; ?>"> <!--id de la sucursar de retiro-->
    <input type="hidden" name="nombre_retiro" value="<?= $nombre_retiro; ?>"> 
    <input type="hidden" name="telefono_retiro" value="<?= $telefono_retiro; ?>"> 

    <input type="hidden" name="factura" value="<?= $factura; ?>"> <!--si o no-->
    <input type="hidden" name="rut_factura" value="<?= $rut_factura; ?>"> 
    <input type="hidden" name="giro_factura" value="<?= $giro_factura; ?>"> 
    <input type="hidden" name="razon_social" value="<?= $razon_social; ?>"> 
    <input type="hidden" name="direccion_factura" value="<?= $direccion_factura; ?>">
   
    <?php 
    if($envio == "retiroEnTienda") {
        $retiroEnTienda = 1;
    } else {
        $retiroEnTienda = 0;
    }
    ?>
    
       <div class="carroDerechaMovil">
        <a class="filaCarroMovil">
            <i class="material-icons">shopping_cart</i> 
            <span>Resumen del pedido</span> 
            <i class="material-icons azul">keyboard_arrow_down</i>
            
            <span class="totalesMovil">$<?= number_format(resumenCompraShortTotales($comuna, $retiroEnTienda),0,",",".");?></span>
        </a>
        <div class="contOcultoCarroResponsive">
            <div id="contDatosCartVariables2">
                <div class="contCartCompraRapida">
                  <?= resumenCompraShort($comuna, $retiroEnTienda); ?>
                </div>
            </div>
            <a href="javascript:void(0)" class="btnTerminarCarro" id="comprarMovil" />Siguiente</a>
        </div><!--fin contOcultoCarroResponsive-->
        
    </div><!--resumen-->

        <div class="carroContIzquierda">
            <div class="contDatosPasoEnvio">
                <div class="cont100">
                    <h2>
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?>
                        <span>Informacion de envio</span>
                        <a class="btnVolver" id="btnVolverResumen" href="javascript:void(0)">
                            <i class="fas fa-long-arrow-alt-left"></i> volver
                        </a> 
                        <?php } else { ?>
                        <span>Informacion de envio</span>
                        <a class="btnVolver" href="envio-y-pago">
                            <i class="fas fa-long-arrow-alt-left"></i> volver
                        </a> 
                        <?php } ?> 
                    </h2>
                </div>
                
                <div class="cont100 filaResumen filaResumenCarro">
                    <div class="iconoFilaResumen">
                        <span class="material-icons">mail_outline</span>
                    </div>
                    <div class="contIzquierdaCampo">
                        <span class="tituloCampoResumen">Correo electrónico</span>
                        <span class="valorCampoResumen"><?= $email; ?></span>
                    </div>
                    <div class="contDerechaCampo">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?> 
                        <a href="javascript:void(0)" class="editarCampoResumen editSinUsuario">Editar correo</a>
                        <?php } else { ?> 
                        <a href="envio-y-pago" class="editarCampoResumen">Editar correo</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if ($envio == "direccionCliente"){ ?>
                 <div class="cont100 filaResumen filaResumenCarro">
                    <div class="iconoFilaResumen">
                        <span class="material-icons">place</span>
                    </div>
                    <div class="contIzquierdaCampo">
                        <span class="tituloCampoResumen">Direccion de envio</span>
                        <span class="valorCampoResumen"><?= $direccion.", ".$nomComuna[0][0]; ?></span>
                    </div>
                    <?php if(!isset($_COOKIE['usuario_id'])){ ?> 
                        <a href="javascript:void(0)" class="editarCampoResumen editSinUsuario">Editar dirección</a>
                        <?php } else { ?> 
                        <a href="envio-y-pago" class="editarCampoResumen">Editar dirección</a>
                        <?php } ?>
                </div>
                <div class="cont100 filaResumen filaResumenCarro">
                    <div class="iconoFilaResumen">
                        <span class="material-icons">call</span>
                    </div>
                    <div class="contIzquierdaCampo">
                        <span class="tituloCampoResumen">Teléfono</span>
                        <span class="valorCampoResumen"><?= $telefono; ?></span>
                    </div>
                    <div class="contDerechaCampo">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?> 
                        <a href="javascript:void(0)" class="editarCampoResumen editSinUsuario">Editar teléfono</a>
                        <?php } else { ?> 
                        <a href="envio-y-pago" class="editarCampoResumen">Editar teléfono</a>
                        <?php } ?>
                    </div>
                </div>
                <?php } else { 
                    $sucursal = consulta_bd("s.nombre, s.direccion, c.nombre","sucursales s, comunas c","c.id = s.comuna_id and s.id = $retiro","");
                ?>
                <div class="cont100 filaResumen filaResumenCarro">
                    <div class="iconoFilaResumen">
                        <span class="material-icons">store_mall_directory</span>
                    </div>
                    <div class="contIzquierdaCampo">
                        <span class="tituloCampoResumen">Dirección de retiro</span>
                        <span class="valorCampoResumen"><?= $sucursal[0][0].": ".$sucursal[0][1].", ".$sucursal[0][2]; ?></span>
                    </div>
                    <div class="contDerechaCampo">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?> 
                        <a href="javascript:void(0)" class="editarCampoResumen editSinUsuario">Editar dirección</a>
                        <?php } else { ?> 
                        <a href="envio-y-pago" class="editarCampoResumen">Editar dirección</a>
                        <?php } ?>
                    </div>
                </div>
                
                <div class="cont100 filaResumen filaResumenCarro">
                    <div class="iconoFilaResumen">
                        <span class="material-icons">person_outline</span>
                    </div>
                    <div class="contIzquierdaCampo">
                        <span class="tituloCampoResumen">¿Quien Retira?</span>
                        <span class="valorCampoResumen"><?= "Nombre: ".$nombre_retiro."<br>Teléfono: ".$telefono_retiro; ?></span>
                    </div>
                    <div class="contDerechaCampo">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?> 
                        <a href="javascript:void(0)" class="editarCampoResumen editSinUsuario">Editar</a>
                        <?php } else { ?> 
                        <a href="envio-y-pago" class="editarCampoResumen">Editar</a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
                
                
                <?php if($factura == "si"){?>
                <div class="cont100 filaResumen filaResumenCarro">
                    <div class="iconoFilaResumen">
                        <span class="material-icons">business</span>
                    </div>
                    <div class="contIzquierdaCampo">
                        <span class="tituloCampoResumen">Datos facturación</span>
                        <span class="valorCampoResumen"><?= "Rut: ".$rut_factura."<br>Giro: ".$giro_factura."<br>Razón social: ".$razon_social."<br>Dirección: ".$direccion_factura; ?></span>
                    </div>
                    <div class="contDerechaCampo">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?> 
                        <a href="javascript:void(0)" class="editarCampoResumen editSinUsuario">Editar facturación</a>
                        <?php } else { ?> 
                        <a href="envio-y-pago" class="editarCampoResumen">Editar facturación</a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
                 
                
                <!--<div class="cont100">
                    <a class="btnVerMiCarro" href="mi-carro" target="_blank">Ver mi carro <i class="fas fa-shopping-cart"></i></a>
                </div>-->
                
              
                <div class="cont100 menuTerminos">
                    <a href="como-comprar">¿Cómo comprar?</a>
                    <a href="politicas-de-privacidad">Políticas de privacidad</a>
                    <a href="terminos-y-condiciones">Términos y condiciones</a>
                </div>
               
                
                
            </div><!--fin contDatosPasoEnvio-->
        </div> <!--datosForm-->


        <div class="carroContDerecha">
            <div id="contDatosCartVariables" class="contDatosCartVariables">
                
                <div class="contCartCompraRapida">
                  <?= resumenTotales($comuna, $retiroEnTienda); ?>
                </div>
                
                <div class="cont100 opcionesPago">
                    <h2>Medios de pago</h2>
                    <div class="datosFinalizacionPago"> 
                        <div class="formasDePago">

                            <div class="contFormasPago cont100">
                                <div class="contMedioPago">
                                    <input type="radio" name="medioPago" id="webpay" value="webpay" checked="checked" />
                                    <span class="imagenMedioPago">
                                        <img src="tienda/webpay.jpg" width="80%"/>
                                    </span>
                                </div>


                                <div class="contMedioPago">
                                    <input type="radio" name="medioPago" id="mercadopago" value="mercadopago" />
                                    <span class="imagenMedioPago">
                                        <img src="tienda/mercadopago.jpg" width="80%"/>
                                    </span>
                                </div>
                                
                                <div class="contMedioPago">
                                    <input type="radio" name="medioPago" id="transferencia" value="transferencia" />
                                    <span class="imagenMedioPago">
                                        <img src="tienda/transferencia.png" />
                                    </span>
                                </div>
                                
                                <div class="mensajeMP">Paga en 6 cuotas sin interés con Mercado Pago</div>

                            </div>
                        </div>
                    </div><!--fin datosFinalizacionPago -->
                    
                    
                    
                </div>
                
                <div class="txtTerminosAcepto">
                    <input type="checkbox" name="terminos" id="terminos" value="1" />
                    <a href="<?php echo $url_base; ?>terminos-y-condiciones">
                         Al comprar acepto los Términos y condiciones
                    </a>
                </div>

                <a href="javascript:void(0)" class="btnTerminarCarro" id="btnTerminarCarro" />Pagar</a>
                <!--<div class="mensajesDespacho"></div>-->
                
                
            </div>
        </div><!--resumen-->
   
</form>     
            