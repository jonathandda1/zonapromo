<?php 
require_once 'paginador/paginator.class.php';
    $page = (isset($_GET[page])) ? mysqli_real_escape_string($conexion, $_GET[page]) : 0;
	$ipp = (isset($_GET[ipp])) ? mysqli_real_escape_string($conexion, $_GET[ipp]) : 4;


$id = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
$cat = (isset($_GET[nombre])) ? mysqli_real_escape_string($conexion, $_GET[nombre]) : 0;

    


    $categorias = consulta_bd("id, nombre","catblog","publicada = 1"," nombre asc");




    

    $ent = consulta_bd("e.titulo, e.portada, e.bajada, a.nombre, a.thumbs, e.id", "entradas e, autores a, catblog cb, catblog_entradas cbr", "e.autor_id = a.id and e.publicada = 1 and e.id = cbr.entrada_id and cbr.catblo_id = cb.id and cb.id = $id", "e.fecha_creacion desc");
    $total = mysqli_affected_rows($conexion);

    $pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 6; 
	$rutaRetorno = "blog";
    $pages->paginate($rutaRetorno);


    $entradas = consulta_bd("e.titulo, e.portada, e.bajada, a.nombre, a.thumbs, e.id", "entradas e, autores a, catblog cb, catblog_entradas cbr", "e.autor_id = a.id and e.publicada = 1 and e.id = cbr.entrada_id and cbr.catblo_id = cb.id and cb.id = $id", "e.fecha_creacion desc $pages->limit");

    
?> 

<div class="cont100">
    <div class="cont100Centro">
        <?php 
         $autores = consulta_bd("titulo, nombre, thumbs, banner, descripcion", "autores", "destacado = 1", "id desc");
        if(count($autores) > 0){
        ?> 
         <div class="contFondoBlogger" style="background-image:url(imagenes/autores/<?= $autores[0][3]; ?>);">
            <h3><?= $autores[0][1]; ?></h3>
            <div class="decripcionBlogger"><?= $autores[0][4]; ?></div>
        </div>
       <?php } ?>
        
        
        <div class="cont100 contMenuBlog">
            <?php for($i=0; $i<sizeof($categorias); $i++) { ?> 
            <a class="<?php if($categorias[$i][0] == $id){echo "seleccionada";}?>" href="categorias_blog/<?= $categorias[$i][0];?>/<?= url_amigables($categorias[$i][1]);?>"><?= $categorias[$i][1];?></a>
            <?php } ?> 
        </div>
            
        
        
        
        <div class="cont100 contGrillasBlog">
            <?php for($i=0; $i<sizeof($entradas); $i++) { ?> 
                <div class="grillaEntrada">
                    <div class="headEntrada">
                        <div class="thumbsAutor" style="background-image: url(imagenes/autores/<?= $entradas[$i][4]; ?>);"></div>
                        <div class="tituloEntradaGrilla"><?= $entradas[$i][0]; ?></div>
                    </div>
                    <div class="imgPortadaGrilla">
                        <img src="imagenes/entradas/<?= $entradas[$i][1]; ?>" width="100%">
                    </div>
                    <div class="contDatosGrillaEntrada">
                        <div class="bajadaEntradaGrilla">
                            <?= $entradas[$i][2]; ?>
                        </div>

                        <a href="entradas/<?= $entradas[$i][5]; ?>/<?= url_amigables($entradas[$i][0]); ?>" class="btnGrillaBlog">ver artículo</a>
                    </div>
                </div>
            <?php } ?> 
        </div><!--fin contGrillasBlog -->
        
        
        <div class="cont100">
            <div class="cont100Centro paginador">
                <?= $pages->display_pages(); ?>
            </div>
        </div><!--Fin paginador -->
        
    </div>
</div>
