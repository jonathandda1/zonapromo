<?php 
require_once 'paginador/paginator.class.php';
/*============================================
=            Recibo variables get            =
============================================*/
$page = (isset($_GET['page'])) ? mysqli_real_escape_string($conexion, $_GET['page']) : 0;
$ipp = (isset($_GET['ipp'])) ? mysqli_real_escape_string($conexion, $_GET['ipp']) : 4;

$marca = (isset($_GET['marca'])) ? mysqli_real_escape_string($conexion, $_GET['marca']) : 0;
$busqueda = (isset($_GET['busqueda'])) ? mysqli_real_escape_string($conexion, $_GET['busqueda']) : 0;

$orden = (isset($_GET['orden'])) ? mysqli_real_escape_string($conexion, $_GET['orden']) : 0;
$oferta = (is_numeric($_GET['oferta'])) ? mysqli_real_escape_string($conexion, $_GET['oferta']) : 0;
$breadcrum = "";
/*=====  End of Recibo variables get  ======*/

/*==============================================
=            Filtros Personalizados            =
==============================================*/
// Nota: La varible a es para retirar los breadcrum
$variablesGetExcluidas = array("page", "ipp", "busqueda", "oferta", "marca", "orden", "op","a");
$valoresTodos = "";
$contValoresTodos = 1;
$urlFiltrosCT = "";
// Se define que variables GET no se toman en cuenta
foreach($_GET as $key=>$val){
    
    if(!in_array($key, $variablesGetExcluidas)){
        $urlFiltrosCT .= "&".$key."=".$val;;
        // and is_numeric($val)
        if($val != ""){
            if($contValoresTodos == 1){
            $valoresTodos .= $val;
            }else {
                $valoresTodos .= "-".$val;
            }
            $contValoresTodos = $contValoresTodos + 1;
        }       
    }
}
$valoresTodos = explode("-", $valoresTodos);
$valoresTodosFinales = "";
$c2 = 1;
foreach($valoresTodos as $val){
    if($c2 == 1){
        if($val != "" and is_numeric($val)){
            $valoresTodosFinales .= $val;
            $c2 = $c2 + 1;
        }
    } else {
        if($val != "" and is_numeric($val)){
            $valoresTodosFinales .= ",".$val;
            $c2 = $c2 + 1;
        }
    }
    
}

/*lo ocupo para completar las url de los filtros que pasan por htaccess*/
$urlNeutra = $_SERVER['REQUEST_URI'];
$urlArregloParaMarcas = explode("?",$urlNeutra);
if($urlArregloParaMarcas[1] != ""){
    $varSiguientes = "?".$urlArregloParaMarcas[1];
} else {
    $varSiguientes = "";
}
// var_dump($varSiguientes);
// $b = str_replace(15,"",$varSiguientes);
// var_dump($b);
// $a = str_replace($varSiguientes,"",$urlNeutra);
// var_dump($a);     
/*lo ocupo para completar las url de los filtros que pasan por htaccess*
/*fin  filtros personalizados*/

/*=============================================================
=            Defino SQL para FilTros Personalizados            =
=============================================================*/
if($valoresTodosFinales != ""){
    $whereFiltros = " and p.id = fp.producto_id and fp.valores_filtro_id IN($valoresTodosFinales)";
    $tablasFiltro = ", valores_filtros vf, filtros_productos fp";
    $nv = consulta_bd("vf.nombre, f.id, fp.valores_filtro_id","filtros f, productos p, valores_filtros vf, filtros_productos fp","fp.valores_filtro_id IN($valoresTodosFinales) and fp.valores_filtro_id = vf.id and f.id = fp.filtro_id GROUP BY fp.valores_filtro_id","");
        foreach ($nv as $key => $val) {
            $breadcrum .= "<p>".$val[0]." <a href='".str_replace("filtro".$val[1]."=","a=",$urlNeutra)."'><b>x</b></a></p>";
        }
} else {
    $whereFiltros = "";
    $tablasFiltro = "";
}
/*=====  End of Defino  SQL para FilTros Personalizados  ======*/
/*fin  filtros personalizados*/

/*=================================================
=            Administrando la busqueda            =
=================================================*/

//Actualizo o inserto busqueda en BD
$busquedaOriginal = consulta_bd("busqueda_original, veces_buscada, busqueda_reemplazo","busquedas","busqueda_original = '$busqueda'","");
$cantBusquedaOriginal = mysqli_affected_rows($conexion);

$vecesBuscada = $busquedaOriginal[0][1];
$busquedaOriginal2 = $busquedaOriginal[0][0];
$busquedaReemplazo = $busquedaOriginal[0][2];

if($cantBusquedaOriginal > 0){
    update_bd("busquedas","veces_buscada = $vecesBuscada + 1, fecha_modificacion = NOW()","busqueda_original = '$busqueda'");
} else {
    insert_bd("busquedas","veces_buscada, busqueda_original, fecha_creacion","1, '$busqueda', NOW()");
}

if($busquedaReemplazo != ""){
    $busqueda = $busquedaReemplazo;
}
//condiciones para las busquedas por ranking

// busco por nombre lo que viene
$busquedaNombre = consulta_bd("p.id, p.nombre","productos p","p.nombre like '%$busqueda%' group by(p.id)","");
$nombre = array();
//lo inserto en un array con su id y le digo que se consiguio 1 vez
for($i=0; $i < sizeof($busquedaNombre); $i++){
    $id = $busquedaNombre[$i][0];
    $nombre[$id] = 1;
}
// busco por Marcas lo que viene
$busquedaMarcas = consulta_bd("p.id,m.id, m.nombre","productos p, marcas m"," p.marca_id = m.id and m.nombre like '%$busqueda%' group by(p.id)","");
$marcasB = array();
//lo inserto en un array con su id y le digo que se consiguio 1 vez
for($i=0; $i < sizeof($busquedaMarcas); $i++){
    $id = $busquedaMarcas[$i][0];
    $marcasB[$id] = 1;
}
//Cruzo marcas con productos por lo que si encuentra un producto que ya se encontro le sumo 1 a las busquedas es decir que los que se encontraron en ambos casos ya valen 2
foreach ($marcasB as $key => $value) {
    $nombre[$key] += 1;
}
// busco por Descripcion lo que viene
$busquedaDescripcion = consulta_bd("p.id, p.descripcion","productos p","p.descripcion like '%$busqueda%' group by(p.id)","");
$descripcion = array();
//lo inserto en un array con su id y le digo que se consiguio 1 vez
for($i=0; $i < sizeof($busquedaDescripcion); $i++){
    $id = $busquedaDescripcion[$i][0];
    $descripcion[$id] = 1;
}
//Cruzo descripcion con productos actualizado es decir que los que se encontraron en ambos casos que valen 2, le sumo uno mas quedando en 3.
foreach ($descripcion as $key => $value) {
    $nombre[$key] += 1;
}
// var_dump($nombre);
$tres = "";$dos = "";$uno = "";
// defino el orden en ql que mostraré los productos
foreach ($nombre as $key => $value) {
    if ($value == 3) {
        $tres .= $key.",";
    }else if($value == 2){
        $dos .= $key.","; 
    }else{
        $uno .= $key.",";
    }
}
$idsBuscar = $tres.$dos.$uno;
// le quito la ultima coma para no generar errores.
$idsBuscar = rtrim($idsBuscar, ", ");
// echo $idsBuscar;
if ($idsBuscar != "" || $idsBuscar != null) {
    $whereBusqueda = "AND p.id IN(".$idsBuscar.")";
    $ordenElegido =  "FIELD (p.id,".$idsBuscar.") ASC";
}else{
    $whereBusqueda = "AND (pd.nombre like '%$busqueda%' OR pd.sku like '%$busqueda%')";
    $ordenElegido =  "p.id desc";
}

// $whereBusqueda = "AND (p.nombre like '%$busqueda%' OR pd.nombre like '%$busqueda%' OR pd.sku like '%$busqueda%' OR m.nombre like '%$busqueda%')";

/*=====  End of Administrando la busqueda  ======*/

/*=========================================
=            Filtros de Marcas            =
=========================================*/

if($marca != 0){
    $marcaTodas = explode("-", $marca);
    $marcasFinal = "";
    $auxM = 1;
    foreach($marcaTodas as $valMarca){ 
        if($auxM == 1){
            if($valMarca != "" and is_numeric($valMarca)){
                $marcasFinal .= $valMarca;
                $auxM = $auxM + 1;
            }
            } else {
                if($valMarca != "" and is_numeric($valMarca)){
                    $marcasFinal .= ",".$valMarca;
                    $auxM = $auxM + 1;
            }
        }          
    }
    $whereMarca = " and p.marca_id IN ($marcasFinal)";
    $nm = consulta_bd("nombre, id","marcas","id IN ($marcasFinal)","");
    foreach ($nm as $key => $val) {
        $breadcrum .= "<p>".$val[0]." <a href='javascript:void(0)'><b class='quitarFiltro' id_marca='".$val[1]."'>x</b></a></p>";
    }
} else {
    $whereMarca;
}
/*=====  End of Filtros de Marcas  ======*/

/*=====================================
=            Filtro Oferta            =
=====================================*/

if($oferta == 1){
	$whereOferta = " and pd.descuento > 0 and pd.descuento < pd.precio";
} else {
	$whereOferta;
}
/*=====  End of Filtro Oferta  ======*/

/*====================================
=            filtro Orden            =
====================================*/
if($orden === "valor-desc"){
    $orderSql = ' p.nombre desc';
    $nombreOrden = "Mayor precio";
} else if($orden === "valor-asc"){
    $orderSql = ' p.nombre asc';
    $nombreOrden = "Menor precio";
} else {
    $orderSql = $ordenElegido;
    $nombreOrden = "Más relevantes";
}
/*=====  End of filtro Orden  ======*/

/*======================================================
=            Defino Consultas SQL Generales            =
======================================================*/

/*consulta para mostrar filtros dinamicos segun productos existentes*/
$productosFiltros = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, lp.linea_id","productos p, productos_detalles pd, marcas m, lineas_productos lp","p.id = pd.producto_id and p.marca_id = m.id and p.publicado=1 $whereBusqueda  group by(p.id)","$orderSql");
/*consulta para mostrar filtros dinamicos segun productos existentes*/

$productosPaginador = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, lp.linea_id","productos p, productos_detalles pd, marcas m, lineas_productos lp $tablasFiltro","p.id = pd.producto_id and p.marca_id = m.id and p.publicado=1 $whereBusqueda $whereMarca $whereOferta $whereFiltros group by(p.id)","$orderSql");
$total = mysqli_affected_rows($conexion);

    $pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 8; 
    $rutaRetorno = "busquedas/$busqueda/$marca/$orden/$oferta";
    $pages->paginate($rutaRetorno);

$productos = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, lp.linea_id, m.nombre, p.descripcion_breve, pd.stock, pd.stock_reserva","productos p, productos_detalles pd, marcas m, lineas_productos lp $tablasFiltro","p.id = pd.producto_id and p.marca_id = m.id AND p.publicado = 1 $whereBusqueda $whereMarca $whereOferta $whereFiltros group by(p.id)"," $orderSql $pages->limit");
$totalProductos = mysqli_affected_rows($conexion);


/*=====  End of Defino Consultas SQL Generales  ======*/
?>

<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
    	<ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Busquedas</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->




<?php if($total > 0){ ?>
<div class="cont100">
    <div class="cont100Centro">               
        <h1 class="nomCategoria">Resultados para: <?= $busqueda; ?><span class="cantProdCat"> <!--&nbsp;&nbsp;&nbsp;&nbsp;<span class="tituloSpan">Mostrando:</span><?= $totalReal; ?></span>--></h1>

        <?php $url = "busquedas/".$busqueda; ?>
        <input id="url" type="hidden" value="<?= $url ?>">
        <input id="orden" type="hidden" value="<?= $orden ?>">
        <input id="ofertas" type="hidden" value="<?= $oferta ?>">
        <input id="variablesFiltros" type="hidden" value="<?= $urlFiltrosCT ?>">
        <!-- <div class="contFIltrosMovil">
            <div class="filtrosOrden">
            <div class="contenedorFiltro">
                <span class="contNomFiltroActual">
                    <span><i class="fas fa-chevron-down"></i></span>
                    <span class="filtroActual"><?= $nombreOrden ?></span>
                </span>
                <ul>
                    <li class="<?php if($orden === 0){ echo 'filtroSeleccionado';} ?>">
                        <a href="busquedas?busqueda=<?= $busqueda;?>">Ordenar por relevancia</a>
                    </li>
                    <li class="<?php if($orden === "valor-desc"){ echo 'filtroSeleccionado';} ?>">
                        <a href="busquedas/<?= $busqueda."/".$marca."/valor-desc/$oferta".$varSiguientes; ?>">Precio Mayor a Menor</a>
                    </li>
                    <li class="<?php if($orden === "valor-asc"){ echo 'filtroSeleccionado';} ?>">
                        <a href="busquedas/<?= $busqueda."/".$marca."/valor-asc/$oferta".$varSiguientes; ?>">Precio Menor a Mayor</a>
                    </li>
                </ul>
            </div> -->
            <!--Fin contenedorFiltro -->
            <!-- </div> -->
            <!--fin filtros-->                    
                <!-- <a href="javascript:void(0)" class="btnFiltrosMovil">
                    Filtros <i class="material-icons">keyboard_arrow_down</i>
                </a>
        </div> -->
        <!--fin contFIltrosMovil -->    
            
    </div>
</div><!--Fin contenido del sitio -->

<?php
    $nombreLinea2 = "CATEGORÍAS";
    $lineasCategorias = consulta_bd("id, nombre", "lineas", "publicado = 1", "posicion asc");
    $subcategoriasCat = consulta_bd("id, nombre", "categorias", "publicada = 1", "posicion ASC, nombre asc");
?>
<div class="cont100 fondoGris">
    <section class="contFiltrosExtras">
        <article class="filtrosIzquierda">
            <div class="divCategorias">
                <ul class="menuGrilla">
                    <li><a href="javascript:void(0)">
                        <span><?= $nombreLinea2 ?></span></a>
                            <i><img src="img/icono_flecha_abajo.png" alt="img de flecha"></i>
                            <ul>
                                <?php
                                /*====================================
                                =            CARGA LINEAS            =
                                ====================================*/
                                foreach ($lineasCategorias as $lid) { ?>
                                    <li>
                                        <a href="lineas/<?= $lid[0]; ?>/<?= url_amigables($lid[1]); ?>"><?= $lid[1]; ?></a>
                                    </li>
                                <?php  } ?>
                            </ul>
                    </li>
                </ul>
            </div>
            <div class="divCategorias">
                <ul class="menuGrilla">
                    <li><a href="javascript:void(0)">
                        <span>SUBCATEGORÍAS</span></a>
                            <i><img src="img/icono_flecha_abajo.png" alt="img de flecha"></i>
                            <ul>
                                <?php
                                /*====================================
                                =            CARGA Subcategorias            =
                                ====================================*/
                                foreach ($subcategoriasCat as $ca) { ?>
                                    <li class="categoriasLI">
                                        <a class="categoriasA" href="categorias/<?= $ca[0]; ?>/<?= url_amigables($ca[1]); ?>"><?= $ca[1] ?></a>
                                    </li>
                                <?php  } ?>
                            </ul>
                    </li>
                </ul>
            </div>
        </article>
        <?php  
            $ordenado = "ORDENAR POR";
            if($orden === "recomendados"){ 
                $ordenado = "RECOMENDADOS";
            }
            if($orden === "valor-asc"){ 
                $ordenado = "ORDEN A-Z";
            }
            if($orden === "valor-desc"){ 
                $ordenado = "ORDEN Z-A";
            }
        ?>
        <article>
            <div class="divCategorias">
                <ul class="menuGrilla">
                    <li><a href="javascript:void(0)">
                        <span><?= $ordenado ?></span></a>
                        <i><img src="img/icono_flecha_abajo.png" alt="img de flecha"></i>
                        <ul>
                            <li class="<?php if($orden === "recomendados"){ echo 'filtroSeleccionado';} ?>">
                            <a href="busquedas/<?= $busqueda."/".$marca."/recomendados/$oferta".$varSiguientes; ?>">RECOMENDADOS</a>
                            </li>
                            <li class="<?php if($orden === "valor-asc"){ echo 'filtroSeleccionado';} ?>">
                                <a href="busquedas/<?= $busqueda."/".$marca."/valor-asc/$oferta".$varSiguientes; ?>">ORDEN A-Z</a>
                            </li>
                            <li class="<?php if($orden === "valor-desc"){ echo 'filtroSeleccionado';} ?>">
                                <a href="busquedas/<?= $busqueda."/".$marca."/valor-desc/$oferta".$varSiguientes; ?>">ORDEN Z-A</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </article>
    </section>
</div>
    
<div class="cont100 marginTop20">
    <div class="cont100Centro">
        <div class="fondoLateralFiltro">
            <div class="lateralFiltro">
                <!--filtros escritorio-->
                <div class="filtrosDesktop">
                    <?php
                    if ($breadcrum != "") {
                        echo "
                        <span class='tituloFiltroBreadcrumb'>Filtrado por:</span>
                        <div class='contBreadcrumb'>".$breadcrum."
                        <a class='borrarFiltroBreadcrumb' href='busquedas/".$busqueda."'><h4>Borrar filtros</h4></a>
                        </div>";
                    }
                    
                    /*=====================================
                    =            Filtro Marcas            =
                    =====================================*/
                    $pm = consulta_bd("p.marca_id", "productos p, productos_detalles pd, marcas m", "p.id = pd.producto_id and p.marca_id = m.id and p.publicado=1 $whereBusqueda group by(p.id)", "");
                    /*arreglo marcas activas*/
                    $idsMarcas = array();
                    for($i=0; $i<sizeof($pm); $i++) {
                        if (in_array($pm[$i][0], $idsMarcas)) {
                            //echo "Existe mac";
                        } else {
                            array_push($idsMarcas, $pm[$i][0]);
                        }
                    }
                    $contador = 0;
                    $idsMarcas2 = "";
                    foreach ($idsMarcas as $valor) {
                        if($contador == 0){
                            $idsMarcas2 .= $valor;
                        } else {
                           $idsMarcas2 .= " ,".$valor; 
                        }

                        $contador = $contador + 1;
                    }
                    /*Fin arreglo marcas activas*/
                    if($idsMarcas2 != ""){
                        $marcas = consulta_bd("id, nombre","marcas","id > 0 and id IN($idsMarcas2)","id asc");    
                    }
                    ?>         
                    <!--marcas-->
                     <div class="filtros">
                        <div class="contenedorFiltroLateral">
                        <span class="tituloFiltro">Marcas</span>
                            <ul class="listadoMarcas">
                                <?php 
                                $chequeo = "";
                                for($i=0; $i<sizeof($marcas); $i++) { 
                                    if($marca != 0){
                                        $marcaCheq = explode("-", $marca);
                                        foreach($marcaCheq as $valMarca){
                                            // echo $valMarca."<br>";
                                            // echo $marcas[$i][0]."<br>";
                                            if ($valMarca == $marcas[$i][0]) {
                                                $chequeo = "checked";
                                                break 1;
                                            }else {
                                                $chequeo = "";
                                            }
                                        }                        
                                    }
                                ?>
                                <label><input class="checkMarcas" type="checkbox" id="<?= $marcas[$i][0]?>" value="<?= $marcas[$i][0]?>" <?= $chequeo?>><?= $marcas[$i][1]?></label><br> 
                                <?php } ?>

                            </ul>
                        </div><!--Fin contenedorFiltroLateral -->
                    </div><!--fin filtros-->
                    <!-- FIN de filtro MArcas -->

                    <?php
                    /*=============================================================
                    =            Filtros Personalizados con Selectores            =
                    =============================================================*/
                    $contSelectores;
                    $idsProd = array();
                    for($i=0; $i<sizeof($productosFiltros); $i++) {
                        if (in_array($productosFiltros[$i][0], $idsProd)) {
                            //echo "Existe";
                        } else {
                            array_push($idsProd, $productosFiltros[$i][0]);
                        }
                    }
                    $contadorProd = 0;
                    $idsProd2 = "";
                    foreach ($idsProd as $valor) {
                        if($contadorProd == 0){
                            $idsProd2 .= $valor;
                        } else {
                           $idsProd2 .= " ,".$valor; 
                        }

                        $contadorProd = $contadorProd + 1;
                    }
                    /*Fin arreglo marcas activas*/

                    if($idsProd2 != ""){
                        $filtros = consulta_bd("distinct(f.id), f.nombre","filtros f, filtros_productos fp","f.id = fp.filtro_id and fp.producto_id IN($idsProd2)","");
                    }
                    ?>                                                       
                    <?php for($i=0; $i<sizeof($filtros); $i++) { ?> 

                    <!--filtros personalizados-->
                     <div class="filtros">
                        <div class="contenedorFiltroLateral">
                        <span class="tituloFiltro"><?= $filtros[$i][1]; ?></span> 
                            <ul class="listadoMarcas"> 
                                <?php 
                                $valoresFiltros = consulta_bd("distinct(vf.id), vf.nombre,fp.valores_filtro_id","valores_filtros vf, filtros_productos fp","vf.id = fp.valores_filtro_id and fp.valores_filtro_id > 0 and fp.filtro_id = ".$filtros[$i][0]." and fp.producto_id IN($idsProd2)","");
                                for($j=0; $j<sizeof($valoresFiltros); $j++) {
                                    $conjFiltro = "filtro".$filtros[$i][0];
                                    $chequeo2 = "";
                                    if(isset($_GET[$conjFiltro]) and $_GET[$conjFiltro] != 0){
                                        //existe y debo modificarla
                                        foreach($_GET as $key=>$val){
                                            /*diferencio de las variables que vienen por htaccess*/
                                            if(!in_array($key, $variablesGetExcluidas)){
                                                
                                                if($key == $conjFiltro){
                                                    if($val != 0 and $val > 0){
                                                        $valoresFiltroActual = explode("-",$val);
                                                        if(in_array($valoresFiltros[$j][0], $valoresFiltroActual)){
                                                            //ya existe
                                                            $chequeo2 = "checked";
                                                        } 
                                                }
                                            }                                 }
                                            /*diferencio de las variables que vienen por htaccess*/
                                        }
                                        //fin existe y debo modificar
                                    }
                                     
                                    // echo "<option ".$selectorELegido1." fil=".$conjFiltro."=".$valoresFiltros[$j][2]." value=''>".$valoresFiltros[$j][1]."</option>";
                                    echo'<label><input class="checkFiltroEsp" type="checkbox" id="<?= $marcas[$i][0]?>" value="'.$conjFiltro."=".$valoresFiltros[$j][2].'" '.$chequeo2.' >'.$valoresFiltros[$j][1].'</label><br>'; 
                                } ?>
                            </ul>
                        </div><!--Fin contenedorFiltroLateral -->
                    </div><!--fin filtros personalizados-->
                    <?php } ?>
                    <!--End of Filtros Personalizados con Selectores  -->
            
                <!-- Botones de accion -->
                <!-- <a id="aplicarFiltrosLineas" href='javascript:void(0)' class="ofertasLateral">Aplicar Filtros</a>  -->
                <a href='<?= "busquedas/$busqueda/$marca/$orden/1".$varSiguientes; ?>' class="ofertasLateral">ver solo  Ofertas</a>
                <a href='<?= "busquedas/$busqueda"; ?>' class="ofertasLateral">Quitar filtros</a>
                <!-- FINAL de Botones de accion -->  
        </div><!--fin filtros escritorio-->
    </div><!--Fin lateral Filtro-->
</div><!--fin fondoLateralFiltro-->     
        
                
                
            <!--Contenedor grillas -->
            <div class="contGrillasProductos">
                   
                <?php for($i=0; $i<sizeof($productos); $i++){ ?>
                       
                      <div class="grilla">
                        <a href="#" class="like" rel="<?= $productos[$i][6]; ?>">
                        <i class="<?= (guardadoParaDespues($productos[$i][6])) ? 'fas':'far';?> fa-heart"></i>
                        </a>
                        
                        <?php if(($productos[$i][11] - $productos[$i][12]) <= 0){?> 
                        <div class="cintaSinStockGrilla">
                            <span class="textoCintaSinStock">Este producto se encuentra sin stock</span>
                        </div>
                        <?php } ?>

                        <?php if(ultimasUnidades($productos[$i][6])){ ?>
                        <div class="etq-ultimas">ultimas unidades</div>
                        <?php } ?>
                        

                        <?php if ($is_cyber AND is_cyber_product($productos[$i][0])): ?>
                            <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                        <?php endif ?>
                        <?= porcentajeDescuento($productos[$i][0]); ?>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="imgGrilla">
                            <?php 
                                if ($productos [$i][2] == "") {
                                    $thumbsFinal = "sin-imagen.jpg";
                                }else{
                                    $thumbsFinal = $productos [$i][2];
                                }
                            ?>
                            <img src="<?= imagen("imagenes/productos/", $thumbsFinal);?>" width="100%">
                        </a>
                        <a class="btnFicha" href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>"><span class="botonDetalle">Ver Ficha</span></a>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="valorGrilla">
                        <?php if ($is_cyber AND is_cyber_product($productos[$i][0])):
                            $precios = get_cyber_price($productos[$i][6]); ?>
                                <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                                <span class="antes"> $<?= number_format($precios['precio'],0,",",".") ?></span>
                            
                        <?php else: ?>
                            <?php if(tieneDescuento($productos[$i][6])){ ?>
                                <span class='ahorroValor'><?= ahorras($productos[$i][6]); ?></span>
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>    
                                <span class="antes"> $<?= number_format(getPrecioNormal($productos[$i][6]),0,",",".") ?></span>
                                
                            <?php }else{ ?>
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                                <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                
                            <?php } ?>
                        <?php endif ?>
                        </a>

                        <!-- <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="marcaGrilla"><?= nombreMarca($productos[$i][0]);?></a> -->
                        
                        <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="nombreGrilla"><?= $productos[$i][1]; ?></a>
                        <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="descripcionGrilla"><?= strip_tags(preview($productos[$i][10], 130)); ?></a>
                        
                    </div><!--Fin Grilla -->
            
                <?php } ?>

                <div class="cont100 paginadorGrid">
                    <div class="paginador">
                        <?= $pages->display_pages(); ?>
                    </div>
                </div><!--Fin paginador -->
            </div>
            <!--Fin contenedor grillas -->


            
                
                
                
            <?php } else { ?>
                
                <div class="cont100 fondoSinResultado">
                    <div class="cont100Centro">
                        <h1 class="nomCategoria">No hay ningún resultados para <strong>"<?= $busqueda;?>"</strong></h1>
                        <h3 class="subtituloBusquedas subtituloSinResultados">Revisa el texto o intenta buscar algo menos específico.</h3>
                    
                        <div class="buscadorPag">
                            <!-- Buscador -->
                            <div class="buscador buscadorEscritorio">
                                <form id="formBuscador" method="get" action="busquedas" style="float:none;">
                                    <input type="text" name="busqueda" class="campo_buscador" placeholder="<?php if(isset($_GET['buscar'])){echo $_GET['buscar'];} else {echo 'Buscar productos';}?>">

                                    <a href="javascript:void(0)" id="cerrarBuscadorHeader"><i class="material-icons">close</i></a>
                                    <input class="btn_search" type="submit" value=" " name="b">

                                </form>
                            </div>
                        </div>  
                    </div> 
                </div>

                <div class="cont100 grillaNormal">   
                    <div class="cont100Centro">
                        <h4 class="tituloVistosRecien">Te recomendamos</h4>
                        <div class="cont100 ultimosVistos">
                            <?= vistosRecientemente("grilla", 5); ?>
                        </div>
                        <!--Productos relacionados -->
                    </div><!--Fin centroDocumentos -->
                </div><!--FIn cont100 -->

                <div class="cont100 fondoGris grillaResponsive grillaResponsiveficha">   
                    <div class="cont100Centro">
                        <h4 class="tituloVistosRecien">Te recomendamos</h4>
                        <div class="cont100" style="background: #fff;">
                            <?= vistosRecientemente("lista", 4); ?>
                        </div>
                        <!--Productos relacionados -->
                    </div><!--Fin centroDocumentos -->
                </div><!--FIn cont100 -->
                
            <?php } ?>



            

        </div>
</div>
