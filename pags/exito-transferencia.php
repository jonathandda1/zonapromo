<?php
$oc = (isset($_GET['oc'])) ? mysqli_real_escape_string($conexion, $_GET['oc']) : 0;
  $PA = consulta_bd("id, estado_id, oc, medio_de_pago","pedidos","oc = '$oc'","");
  $cant = mysqli_affected_rows($conexion);
    
    $method = (isset($_GET[method])) ? mysql_real_escape_string($_GET[method]) : 'tbk';
  
  if($cant > 0 and $PA[0][1] != 5){
    echo '<script>parent.location = "'.$url_base.'fracaso?oc='.$oc.'";</script>';
  } else if($oc=="" || $cant == 0){
      echo '<script>parent.location = "'.$url_base.'404";</script>';
  }else{

    

    $campos = "id,nombre,direccion,email,telefono,";//0-4
    $campos.= "regalo,total,valor_despacho,";//5-7
    
    $campos.= "comuna,region,fecha,estado_id,";//15-18
    $campos.= "factura, direccion_factura, giro, email_factura, rut_factura, fecha_creacion, region,ciudad,estado_id, rut, retiro_en_tienda,medio_de_pago,total_pagado";//19-30

    $pedido = consulta_bd($campos,"pedidos","oc = '$oc'","");

    //detalle comprador
    $direccion_cliente = $pedido[0][2];
    $comuna_cliente    = $pedido[0][8];
    $region_cliente    = $pedido[0][9];
    $ciudad_cliente    = $pedido[0][19];
    $localidad_cliente    = $pedido[0][29];

    //detalle pedido
    $pedido_id     = $pedido[0][0];
    $total_pedido      = $pedido[0][6];
    $total_despacho   = $pedido[0][7];
    $fecha       = $pedido[0][17];
    $hora_pago       = date('H:s:i', time($pedido[0][8]));

    //detalle transbank
    $num_tarjeta     = $pedido[0][10];
    $num_cuotas      = $pedido[0][11];
    $cod_aurizacion   = $pedido[0][12];
    $tipo_pago     = $pedido[0][13];
    $medioPago = $pedido[0][23];

    //USER
    $id_usuario    = $pedido[0][14];
    //$info = info_user($id_usuario);

    //validar si esta pagado
    $validador = $pedido[0][18];
    $contador_validador = count($validador);

    //total
    $total_productos = $pedido[0][6];
    $total_pagado = number_format($pedido[0][24],0,",",".");

    //funcion tipo pago
    $tipo_pago = tipo_pago($tipo_pago,$num_cuotas, $method);

    //detalle productos
    $detalle_productos = consulta_bd("productos_detalle_id, cantidad, precio_unitario","productos_pedidos","pedido_id= ".$pedido[0][0],"");
 }
?>

<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Éxito en Compra</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->


<div class="cont100">
    <div class="cont100Centro">
        <div class="centroExito">
            <div class="iconoExito"><img src="img/icono_exito.png" alt="Imagen de éxito"></div>
            <h2 class="tituloExito">Transacción exitosa</h2><!--fin titulos_interiores -->

            <div class="top-identificacion">
                <div class="txtExito">¡Muchas gracias por comprar en <?= opciones('nombre_cliente') ?>! <br>Su número de orden de compra es: <span class="ocExito"><?= $oc; ?></span></div>
                
            </div>

            <div class="cont100 contBotonesCarro">
                <a href="<?php echo $url_base; ?>home">Seguir en el sitio</a>
            </div>

            <div class="infoClienteExito">
                <p>A continuación encontrarás los datos para que realices la transferencia antes de 48 horas, después de ese periodo se anulara el pedido.</p>

                <p>Para asistencia sobre el pago o dudas del producto, por favor contáctenos al mail ventas@zonapromo.cl, en horario de atención de tienda.</p>

                <p>En el caso que el producto incluya despacho pagado o por pagar el o los productos serán enviados 24 horas después de haber realizado el pago, previa confirmación de la transacción. En el caso que la transacción se realice un día viernes, fin de semana o feriado el despacho se realizara en los primeros días hábiles siguientes.</p>

                <div class="cont100">
                    <p>
                        <strong>Banco:</strong> Santander / Banefe<br>
                        <strong>Cuenta Corriente:</strong> 0234006318<br>
                        <strong>Rut:</strong> 11.111.111-1<br>
                        <strong>Nombre:</strong> Zona Promo Spa<br>
                        <strong>Email:</strong> <a href="mailto:ventas@zonapromo.cl">ventas@zonapromo.cl</a><br>
                    </p>
                </div>
                <div class="filaExito">
                    <div class="t-paso4"><i class="fas fa-user"></i>Información personal</div>
                    <div class="cont100">
                        <div class="t2-paso4">Nombre:</div>
                        <div class="dato-paso4"><?= $pedido[0][1]; ?></div>
                    </div>
                    <div class="cont100">
                        <div class="t2-paso4">Teléfono:</div>
                        <div class="dato-paso4"><?= $pedido[0][4]; ?></div>
                    </div>
                    <div class="cont100">
                        <div class="t2-paso4">Rut:</div>
                        <div class="dato-paso4"><?= $pedido[0][21]; ?></div>
                    </div>
                    <div class="cont100">
                        <div class="t2-paso4">Email:</div>
                        <div class="dato-paso4"><?= $pedido[0][3]; ?></div>
                    </div>
                </div><!--fin ancho33 -->
                <div class="filaExito">
                    <div class="t-paso4"><i class="fas fa-truck-moving"></i>Información de envío</div>
                    <div class="cont100">
                    <div class="t2-paso4">Fecha:</div>
                    <div class="dato-paso4"><?= $fecha ?></div>
                    </div>
                    <div class="t2-paso4">Dirección:</div>
                    <div class="dato-paso4" style="margin-bottom:15px;">
                    <?php echo $direccion_cliente ?>, <?php echo $localidad_cliente; ?>
                    <?php echo $comuna_cliente ?>, <?php echo $region_cliente ?>
                    </div>
                </div><!--fin ancho33-->
                
                <?php if ($pedido[0][12] == 1){ ?>
                <div class="filaExito">
                <div class="t-paso4"><i class="fas fa-receipt"></i>Información de facturación</div>
                
                <div class="cont100">
                    <div class="t2-paso4">Nombre:</div>
                    <div class="dato-paso4"><?= $pedido[0][1]; ?></div>
                </div>
                <div class="cont100">
                    <div class="t2-paso4">Direccion:</div>
                    <div class="dato-paso4"><?= $pedido[0][13]; ?></div>
                </div>
                <div class="cont100">
                    <div class="t2-paso4">Rut:</div>
                    <div class="dato-paso4"><?= $pedido[0][16] ?></div>
                </div>
                </div><!--Fin ancho33-->
                <?php } ?>
            </div>
            
            <?php if(!$_COOKIE[usuario_id]){
                $email = $pedido[0][3];
                $cliente = consulta_bd("id","clientes","email = '$email' and clave is NULL","");
                $cantEmail = mysqli_affected_rows($conexion);
                if($cantEmail > 0){ ?>
                    <div class="cont100 contBotonesCarro crearCuentaDesdeExito">
                    <h2>¡CREA TU CUENTA EN UN SOLO CLICK!</h2>
                    <p>Crea tu cuenta en un solo click y podrás revisar el estado de tus despachos,</p>
                    <p>historial de compras, productos guardados en tu dashboard y mucho más.</p>
                    <form method="post" id="crearCuentaDesdeExito" action="crear-cuenta-exito">
                                    <input type="hidden" name="cliente_id" value="<?= $cliente[0][0]; ?>" />
                                    <input class="btnCuentaExito" type="submit" name="crearCuenta" value="CREAR CUENTA" />
                                </form>
                            </div>
                <?php }
                
                }?>
        </div>
    </div><!--fin cont100Centro-->
</div><!--Fin breadcrumbs -->
