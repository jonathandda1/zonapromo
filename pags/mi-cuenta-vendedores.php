<?php 
	if(!isset($_COOKIE['Vendedor_id'])){
		echo 'location.href = "home?error=Debes iniciar sesión en la sección para vendedores en el footer de la pagina"';
	}
    $empresasAsociadas = consulta_bd("ev.id, e.rut","empresas_vendedores ev, empresas e","ev.vendedor_id = $vendedorID AND e.id = ev.empresa_id","");
    if (count($empresasAsociadas) > 0) {
        $aux = 1;
        $rutsEmpresas = "";
        foreach($empresasAsociadas as $valRutEmpresa){ 
            if($aux == 1){
                if($valRutEmpresa != ""){
                    $rutsEmpresas .= "'".$valRutEmpresa[1]."'";
                    $aux = $aux + 1;
                }
                } else {
                    if($valRutEmpresa != ""){
                        $rutsEmpresas .= ",'".$valRutEmpresa[1]."'";
                        $aux = $aux + 1;
                    }
                }
        } 
    $cotizaciones1 = consulta_bd("id, estado_id, oc","cotizaciones","rut_empresa IN($rutsEmpresas)","");
    $cotizacionesTotales = count($cotizaciones1);
    $cotizaciones2 = consulta_bd("id, estado_id, oc","cotizaciones","rut_empresa IN($rutsEmpresas) AND estado_id = 1","");
    $cotizacionesPendientes = count($cotizaciones2);
    $cotizaciones3 = consulta_bd("id, estado_id, oc","cotizaciones","rut_empresa IN($rutsEmpresas) AND estado_id = 2","");
    $cotizacionesCompletadas = count($cotizaciones3);
    $cotizaciones4 = consulta_bd("id, estado_id, oc","cotizaciones","rut_empresa IN($rutsEmpresas) AND estado_id = 3","");
    $cotizacionesVendidas = count($cotizaciones4);
?>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Mi cuenta</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro">
        <?php include("pags/menuVendedores.php"); ?>
        
        <section class="contCuentaVendedor">
            <article class="explicaVendedor">
                <span>Hola, <?= $vendedorNombre;?></span>
                <p>Aquí encontrarás tu datos personales, podrás agregar direcciones, revisar tus pedidos, seguir tus compras y revisar todo lo relacionado con tu cuenta en Zona Promo.</p>
            </article>
        </section>

        <section class="contInfosVendedor">
            <div class="divInfoVen">
                <a href="cotizaciones-vendedor?filtro=Pendiente">
                    <p>COTIZACIONES PENDIENTES <br><span class="numeroCotizaciones"><?= $cotizacionesPendientes ?></span></p>
                </a>
            </div>
            <div class="divInfoVen">
                <a href="cotizaciones-vendedor?filtro=Completada">
                    <p>COTIZACIONES COMPLETADAS <br><span class="numeroCotizaciones"><?= $cotizacionesCompletadas ?></span></p>
                </a>
            </div>
            <div class="divInfoVen">
                <a href="cotizaciones-vendedor?filtro=Vendida">
                    <p>COTIZACIONES VENDIDAS <br><span class="numeroCotizaciones"><?= $cotizacionesVendidas ?></span></p>
                </a>
            </div>
            <div class="divInfoVen">
                <a href="cotizaciones-vendedor">
                    <p>CANTIDAD DE COTIZACIONES <br><span class="numeroCotizaciones"><?= $cotizacionesTotales ?></span></p>
                </a>
            </div>
        </section>
               
    </div>
</div>
<?php }else{ ?>
    <div class="cont100 contBreadCrumbs">
        <div class="cont100Centro">
            <ul class="breadcrumb">
            <li><a href="home">Home</a></li>
            <li class="active">Mi cuenta</li>
            </ul>
        </div>
    </div>

    <div class="cont100">
    <div class="cont100Centro">
        <?php include("pags/menuVendedores.php"); ?>
        <script>
            $(".disableSinEmpresa").css("display","none");
        </script>
        <section class="contCuentaVendedor">
            <article class="explicaVendedor">
                <span>Hola, <?= $vendedorNombre;?></span>
                <p>Aquí encontrarás tu datos personales, podrás agregar direcciones, revisar tus pedidos, seguir tus compras y revisar todo lo relacionado con tu cuenta en Zona Promo.</p>
            </article>
        </section>

        <section class="contInfosVendedor">
            <div class="divInfoVen">
                <h1>Aviso Importante!</h1>
                <p>Aún no tienes ninguna empresa asociada a tu perfil de vendedor@, por lo cual debes asociar primero almenos una empresa a tu perfil para iniciar actividades y tener acceso a todos los módulos.</p>
            </div>
        </section>
               
    </div>
</div>
<?php } ?>