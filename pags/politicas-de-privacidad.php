<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Políticas de privacidad</li>
        </ul>
    </div>
</div>





<div class="cont100 fondoServicio">
    <div class="cont100Centro conPoliticas">
        <section class="contNosotros">
            <div class="tituloContacto">Políticas de privacidad</div>
            <article class="contTextosFijos">
                <h4>Términos y Condiciones</h4>
                <p>COMERCIALIZADORA ZONA PROMO SPA, mantendrá siempre tus datos aislados de terceras empresas o personas que puedan hacer usos de esta información para otros fines. Se garantiza confidencialidad, privacidad y seguridad en la manipulación y uso de cualquier información personal de los clientes inscritos en el sitio web.</p>
                <p></p>
                
                <h4>Confidencialidad de datos</h4>
                <p>Actualmente en todo el mundo, la manera más eficaz de resguardar la seguridad de las compras es vía certificados SSl. Estos certificados son los encargados de cifrar o codificar los datos de las transacciones, impidiendo que estas sean intervenidas y descifradas por terceros. Un certificado SSL presente y activo en un sitio web, entrega a los compradores la tranquilidad necesaria par sabe que sus datos no serán obtenidos por una tercera persona.</p>
                <p></p>
                
                <h4>Compromiso de Seguridad</h4>
                <p>COMERCIALIZADORA ZONA PROMO SPA velará enérgicamente por el uso, la mantención y mejora de los estándares de seguridad y privacidad hacia sus clientes registrados en el sitio web. Se mantendrán los estándares requeridos, tanto en infraestructura, herramientas y dotación para cumplir con este compromiso de resguardo de toda información otorgada en los formularios de registros.</p>
                <p></p>
                
                <h4>Compromiso de Seguridad</h4>
                <p>Las informaciones requeridas en el formulario de registro tiene por finalidad el identificar a los clientes, mantenerlos informados, solucionar reclamos, revisar pedidos, consultar por despachos o recibir consultas y sugerencias. Esto permite tener un contacto más cercano, rápido y detallado que ayuda a un accionar más rápido y eficiente en cualquiera de las situaciones.</p>
                <p></p>
                <h4>Datos Personales</h4>
                <p>Conforme a la Ley Nº 19.628 sobre protección de datos de carácter personal, cualquier usuario podría en todo momento modificar, revisar o eliminar su información personal de nuestros registros. Esto puede hacerse por las siguientes vías:</p>
                <p>Escribiendo a:  contacto@zonapromo.cl </p>
                <p>Llamando al teléfono: +56 2 22193484 en horario hábil. </p>
                <p></p>
                
            </article>
        </div>
    </section>
</div>
