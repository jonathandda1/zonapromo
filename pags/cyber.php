<?php 
require_once 'paginador/paginator.class.php';

    $page = (isset($_GET['page'])) ? mysqli_real_escape_string($conexion, $_GET['page']) : 0;
	$ipp = (isset($_GET['ipp'])) ? mysqli_real_escape_string($conexion, $_GET['ipp']) : 4;
	
	$id_rango = (is_numeric($_GET[rango])) ? mysqli_real_escape_string($conexion, $_GET['rango']) : 0;
    
    $id_marca = (is_numeric($_GET['id_marca'])) ? mysqli_real_escape_string($conexion, $_GET['id_marca']) : 0;
	$orden = (isset($_GET['orden'])) ? mysqli_real_escape_string($conexion, $_GET['orden']) : 0;
	



	if($id_marca != 0){
		$whereSql = " and marca_id = $id_marca";
	} else {
		$whereSql;
	}

    if($id_rango != 0){
        $rgs = consulta_bd("valor_inicial, valor_final","rangos","id = $id_rango","");
        $cantRGS = mysqli_affected_rows($conexion);
		if($cantRGS > 0){
            $valorInicial = $rgs[0][0];
            $valorFinal = $rgs[0][1];
                
            $whereRango = " and pd.precio_cyber >= $valorInicial and pd.precio_cyber <= $valorFinal";
        } else {
            $whereRango = "";
        }
    } else {
		$whereRango = "";
	}

    
	if($orden === "valor-desc"){
		$orderSql = ' pd.precio_cyber desc';
		$nombreOrden = "Mayor precio";
	} else if($orden === "valor-asc"){
		$orderSql = ' pd.precio_cyber asc';
		$nombreOrden = "Menor precio";
	} else {
		$orderSql = " p.fecha_creacion desc";
		$nombreOrden = "Más relevantes";
	}



$productosPaginador = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id", "productos p, productos_detalles pd", "p.id = pd.producto_id AND p.publicado = 1 and p.cyber = 1 and pd.precio_cyber > 0 $whereRango $whereSql group by p.id ", "$orderSql");

$total = mysqli_affected_rows($conexion);
$pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 8; 
	$rutaRetorno = "cyber/$id_marca/$orden/$id_rango";
    $pages->paginate($rutaRetorno);
    

$productos = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id", "productos p, productos_detalles pd", "p.id = pd.producto_id AND p.publicado = 1 and p.cyber = 1 and pd.precio_cyber > 0 $whereSql $whereRango group by p.id", "$orderSql $pages->limit");
 $cant_productos = mysqli_affected_rows($conexion);   
    

?>


  <div class="cont100 contBreadCrumbs">
        <div class="cont100Centro">
        	<ul class="breadcrumb">
              <li><a href="home">Home</a></li>
              <li class="active">Ofertas</li>
            </ul>
        </div>
    </div><!--Fin breadcrumbs -->

<?php $PF = consulta_bd("p.id", "productos p, productos_detalles pd", "p.id = pd.producto_id AND p.publicado = 1 and p.cyber = 1 and pd.precio_cyber > 0 group by p.id", ""); 
$cantPF = mysqli_affected_rows($conexion); ?> 

    <div class="cont100">
        <div class="cont100Centro">
            <h1 class="nomCategoria">Cyber<span class="cantProdCat">(<?= $cantPF; ?>)</span></h1>

            <div class="contFIltrosMovil">
                <div class="filtrosOrden">
                <div class="contenedorFiltro">
                    <span class="contNomFiltroActual">
                        <span><i class="fas fa-chevron-down"></i></span>
                        <span class="filtroActual"><?= $nombreOrden ?></span>
                    </span>
                    <ul>
                        <li class="<?php if($orden === 0){ echo 'filtroSeleccionado';} ?>">
                            <a href="cyber">Ordenar por relevancia</a>
                        </li>
                        <li class="<?php if($orden === "valor-desc"){ echo 'filtroSeleccionado';} ?>">
                            <a href="cyber<?= "/".$id_marca."/valor-desc/$id_rango"; ?>">Precio Mayor a Menor</a>
                        </li>
                        <li class="<?php if($orden === "valor-asc"){ echo 'filtroSeleccionado';} ?>">
                            <a href="cyber<?= "/$id_marca/valor-asc/$id_rango"; ?>">Precio Menor a Mayor</a>
                        </li>
                    </ul>
                </div><!--Fin contenedorFiltro -->
            </div><!--fin filtros-->

                <a href="javascript:void(0)" class="btnFiltrosMovil">
                    Filtros <i class="material-icons">keyboard_arrow_down</i>
                </a>
            </div><!--fin contFIltrosMovil -->
            
        </div>
    </div>
  
    
    <div class="cont100">
        <div class="cont100Centro">
        	<div class="fondoLateralFiltro">
                <div class="lateralFiltro">
                
                
                
                <?php 
                    $marcas = consulta_bd("p.id, count(m.id) as cantidad, m.id, m.nombre", "productos p, productos_detalles pd, marcas m", "p.id = pd. producto_id and p.marca_id = m.id and p.publicado = 1 and m.id <> 0 $whereOferta group by m.id", "");
                    
                    //$marcas = consulta_bd("id,nombre","marcas","id <> 0","nombre asc");
                    $cantMarcas = mysqli_affected_rows($conexion);

                    if($cantMarcas > 0){?>   
                     <div class="filtros">   
                        <div class="contenedorFiltro contenedorFiltroLateral"><!-- Filtro marcas -->
                            <div class="filtroActual2">Marcas</div>
                            <ul>
                                <?php for($i=0; $i<sizeof($marcas); $i++){ ?>
                                    <?php if($id_marca == $marcas[$i][2]){ ?>
                                        <li class="marcaNormal filtroSeleccionado">
                                            <a href="cyber/0/<?= $orden."/".$id_rango; ?>">
                                                <?= $marcas[$i][3]; ?> 
                                            </a>
                                        </li>
                                    <?php } else { ?>
                                        <li class="marcaNormal">
                                            <a href="cyber/<?= $marcas[$i][2]."/$orden/$id_rango"; ?>">
                                                <?= $marcas[$i][3]; ?>
                                            </a>
                                        </li>
                                    <?php } ?> 
                                
                                <?php } ?>
                            </ul>
                        </div><!-- Fin Filtro marcas -->
                    </div><!--fin filtros-->
                    <?php } ?>
                
                     
                    <?php 
                        $rangos = consulta_bd("id,nombre, valor_inicial, valor_final","rangos","","id asc");
                        $cantFormatos = mysqli_affected_rows($conexion);
                        if($cantFormatos > 0){	?>   
                    <div class="filtros">   
                        <div class="contenedorFiltro contenedorFiltroLateral"><!-- Filtro marcas -->
                            <div class="filtroActual2">Precio</div>
                            <ul>
                                <?php for($i=0; $i<sizeof($rangos); $i++){ ?>
                                    <?php if($id_rango == $rangos[$i][0]){ ?>
                                        <li class="rangoNormal filtroSeleccionado">
                                            <a href="cyber/<?= $id_marca."/$orden/0"; ?>">
                                                <?= $rangos[$i][1]; ?>
                                            </a>
                                        </li>
                                    <?php } else { ?>
                                        <li class="rangoNormal">
                                            <a href="cyber/<?= $id_marca."/$orden/".$rangos[$i][0]; ?>">
                                                <?= $rangos[$i][1]; ?>
                                            </a>
                                        </li>
                                    <?php } ?> 
                                
                                <?php } ?>
                            </ul>
                        </div><!-- Fin Filtro marcas -->
                    </div><!--fin filtros-->
                    <?php } ?>
                    
                
                <a href='cyber' class="ofertasLateral">Quitar filtros</a>
                
            </div><!--Fin lateral Filtro-->
            </div><!--fin fondoLateralFiltro -->
            
            
            <!--Contenedor grillas -->
	<div class="contGrillasProductos">
        <?php 
        $banner = consulta_bd("banner","banner_cyber","publicado = 1","id desc");
        if(count($banner) > 0){?>
            <a href="<?= $banner[0][1]; ?>" class="bannerOfertas">
                <img src="<?= imagen("imagenes/banner_cyber/",$banner[0][0]);?>" width="100%">
            </a>
        <?php } ?>
        
            <?php for($i=0; $i<sizeof($productos); $i++){ ?>
                <div class="grilla">
                    <a href="#" class="like" rel="<?= $productos[$i][6]; ?>">
                        <i class="<?= (guardadoParaDespues($productos[$i][6])) ? 'fas':'far';?> fa-heart"></i>
                    </a>
                    <?= porcentajeDescuento($productos[$i][0]); ?>
                    <?php if(ultimasUnidades($productos[$i][6])){ ?>
                    <div class="etq-ultimas">ultimas unidades</div>
                    <?php } ?>
                    
                    <?php /*if(ofertaTiempo($productos[$i][6])){ ?>
                    <div class="countdown" rel="<?= ofertaTiempoHasta($productos[$i][6]); ?>"></div>
                    <?php }*/ ?>

                    <?php if ($is_cyber AND is_cyber_product($productos[$i][0])): ?>
                        <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                    <?php endif ?>
                    
                	<a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="imgGrilla">
                        <img src="<?= imagen("imagenes/productos/", $productos[$i][2]);?>" width="100%">
                        <?= dctoCantidad($productos[$i][6]); ?>
                        <?= ahorras($productos[$i][0]); ?>
                    </a>
                    <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="nombreGrilla"><?= $productos[$i][1]; ?></a>
                    <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="valorGrilla">
                    <?php if ($is_cyber AND is_cyber_product($productos[$i][0])):
                        $precios = get_cyber_price($productos[$i][6]); ?>
                        <span class="antes"> $<?= number_format($precios['precio'],0,",",".") ?></span>
                        <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                    <?php else: ?>
                        <?php if(tieneDescuento($productos[$i][6])){ ?>
                            <span class="antes"> $<?= number_format(getPrecioNormal($productos[$i][6]),0,",",".") ?></span>
                            <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                        <?php }else{ ?>
                            <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                        <?php } ?>
                    <?php endif ?>
                    </a>
                    
                </div><!--Fin Grilla -->
            <?php } ?>
            
        </div>
    <!--Fin contenedor grillas -->
            
            
            
        </div> <!--fin cont centro-->
    </div><!--Fin contGeneral -->
    




	
    
    
    
    <div class="cont100">
        <div class="cont100Centro paginador">
        	<?= $pages->display_pages(); ?>
        </div>
    </div><!--Fin paginador -->

