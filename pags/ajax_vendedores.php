<?php 
require_once '../admin/conf.php';
?>
              
<?php 
	$id = (is_numeric($_GET[id])) ? mysql_real_escape_string($_GET[id]) : 0;
	$vendedores = consulta_bd("id, nombre, telefono, correo, direccion, mapa, imagen, zona_id","vendedores","id = $id","");
?>

<table width="400" border="0" cellspacing="0" cellpadding="0" style="margin:10px 20px; float:left;">
      <tr>
        <td width="72" valign="top"><span class="rojo">Nombre:</span></td>
        <td width="183" valign="top"><?php echo $vendedores[0][1] ?></td>
        <td width="145" rowspan="6" valign="top">
        	<?php if ($vendedores[0][6] != ''){ ?>
            <img src="imagenes/vendedores/<?php echo $vendedores[0][6] ?>" width="145" />
            <?php }?>
        </td>
      </tr>
      <tr>
        <td valign="top"><span class="rojo">Teléfono:</span></td>
        <td valign="top"><?php echo $vendedores[0][2] ?></td>
      </tr>
      <tr>
        <td valign="top"><span class="rojo">Correo:</span></td>
        <td valign="top"><a href="mailto:<?php echo $vendedores[0][3] ?>"></a><?php echo $vendedores[0][3] ?></td>
      </tr>
      <tr>
      	<?php $zonas = consulta_bd("id, nombre", "zonas", "id = ".$vendedores[0][7], "");?>
        <td valign="top"><span class="rojo">Zona:</span></td>
        <td valign="top"><?php echo $zonas[0][1] ?></td>
      </tr>
      <tr>
        <td valign="top"><span class="rojo">Dirección:</span></td>
        <td valign="top"><?php echo $vendedores[0][4] ?></td>
      </tr>
      <tr>
        <td valign="top"><span class="rojo">Mapa:</span></td>
        <td valign="top">
        	<?php if ($vendedores[0][5] != ''){ ?>
            <a href="<?php echo $vendedores[0][5] ?>" class="mapa_colorbox" title="<?php echo $vendedores[0][4] ?>">
                <img src="img/ver_mapa.png" />
            </a>
            <?php }?>
        </td>
      </tr>
    </table>
    <?php mysqli_close($conexion); ?>