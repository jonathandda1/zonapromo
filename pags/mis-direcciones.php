<script>
<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo 'location.href = "inicio-sesion"';
	}
?>
</script>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Mis Direcciones</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        
        
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            
            
            <div class="datosCliente">
                <div class="titulosCuenta">
                    <span>Mis Direcciones</span>
                    <p>Modifica tus direcciones a continuación para que tu cuenta esté actualizada.</p>
                </div>
                <?php 
                
                     $direcciones = consulta_bd("cd.id, cd.nombre, cd.region_id, cd.ciudad_id, cd.comuna_id, cd.localidad_id, cd.calle, cd.numero, com.nombre, r.nombre","clientes_direcciones cd, regiones r, comunas com","r.id = com.region_id and com.id = cd.comuna_id and cd.cliente_id = ".$_COOKIE['usuario_id'],"cd.id asc"); 

        ?>        	
                    <div class="contDireccionesCuenta">
                        <?php for($i=0; $i<sizeof($direcciones); $i++) {?>
                        <div class="direcciones contDirecciones" id="dir_<?= $direcciones[$i][0]; ?>">
                            
                            
                            <div class="contNombresDirecciones">
                                <div class="cont100">
                                    <div class="ancho100">
                                        <strong class="nombreDireccion"><?= $direcciones[$i][1]; ?>: <?= $direcciones[$i][10]; ?></strong><br>
                                        <span class="tipoDireccion"><?= $direcciones[$i][10]; ?></span>
                                    </div>
                                </div>
                                <div class="cont100 direccionCuentas">
                                    <?= $direcciones[$i][6].", ".$direcciones[$i][8].", ".$direcciones[$i][9]; ?>
                                </div>
                            </div><!--fin contNombresDirecciones-->

                            <div class="iconosDirecciones">
                                <a href="javascript:void(0)" class="deleteDireccion" rel="<?= $direcciones[$i][0]; ?>">Eliminar</a>
                                <a data-fancybox data-type="ajax" data-src="pags/editarDireccion.php?id=<?= $direcciones[$i][0]; ?>" href="javascript:;" class="editarDireccion fancybox.ajax">Editar</a>
                            </div>
                            
                        </div>
                        <?php } ?>
                    </div>

                     
                    <div class="cont100 contBtnNuevaDireccion">
                        <a href="#contNuevaDireccion" id="nuevaDireccion" class="nuevaDireccion">AGREGAR DIRECCIÓN</a>
                    </div>
                    
            </div><!-- fin datosCliente -->
            
            <?php include("pags/tipsMiCuenta.php");?> 
        
       
       
			
            <div style="display:none;">
            	<div class="contNuevaDireccion" id="contNuevaDireccion">
                    <div class="cont100 direcciones">
					<?php $regiones = consulta_bd("id, nombre","regiones"," id <> 16","posicion asc"); ?>            
                        <form method="post" action="ajax/nuevaDireccion.php" id="formNuevaDireccion" class="bl_form">
                        	<input type="hidden" name="usuario_id" value="<?= $_COOKIE['usuario_id']; ?>" />
                        	<h2>Agregar nueva dirección</h2>
                            <div class="cont100">
                                <div class="cont100">
                                    <!--<label for="">Nombre asignado</label>-->
                                    <input class="campoGrande2 label_better" type="text" name="nombre" id="nombre" value="" data-new-placeholder="Nombre" placeholder="Nombre"  />
                                </div>
                                
                            </div>
                            <div class="cont100">
                                <div class="col1-50">
                                    <select name="region_id" id="region">
                                        <option>Seleccione región</option>
                                        <?php for($i=0; $i<sizeof($regiones); $i++) {?>
                                        <option value="<?= $regiones[$i][0];?>"><?= $regiones[$i][1];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col2-50">
                                    <select name="comuna_id" id="comuna">
                                        <option value="">Seleccione Comuna</option>
                                    </select>
                                </div>
                                
                              </div> 
                               
                             
                            <div class="cont100">
                                <input class="campoGrande2 label_better" type="text" name="calle" value="" id="calle" data-new-placeholder="Dirección" placeholder="Dirección" />
                                
                            </div>
                            <div class="cont100">
                                <a class="guardarDireccion" id="guardarDireccion" href="javascript:void(0)">Guardar dirección</a>
                            </div>
                            <input type="hidden" name="origen" value="mi-cuenta">
                        </form>
                    </div>
                </div>
            </div>
            <!--Fin direcciones-->
         
            
        </div><!--fin contenidoMiCuenta-->
    
    </div>
</div>