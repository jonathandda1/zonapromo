<?php 
require_once 'paginador/paginator.class.php';

    $page = (isset($_GET['page'])) ? mysqli_real_escape_string($conexion, $_GET['page']) : 0;
	$ipp = (isset($_GET['ipp'])) ? mysqli_real_escape_string($conexion, $_GET['ipp']) : 4;
	
	$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
	$nombreSubCategoria = (isset($_GET['nombre'])) ? mysqli_real_escape_string($conexion, $_GET['nombre']) : 0;
	$oferta = (is_numeric($_GET['oferta'])) ? mysqli_real_escape_string($conexion, $_GET['oferta']) : 0;
    $marca = (is_numeric($_GET['marca'])) ? mysqli_real_escape_string($conexion, $_GET['marca']) : 0;
	$orden = (isset($_GET['orden'])) ? mysqli_real_escape_string($conexion, $_GET['orden']) : 0;
	
    
    $lineas = consulta_bd("l.id, l.nombre, c.id, c.nombre, sc.id, sc.nombre","lineas l, categorias c, subcategorias sc","l.id = c.linea_id and c.id = sc.categoria_id and sc.id = $id","");
    $breadcrum = "";

/*==============================================
=            Filtros Personalizados            =
==============================================*/
// Nota: La varible a es para retirar los breadcrum

/*filtros personalizados*/
$variablesGetExcluidas = array("page", "ipp", "id", "nombre", "oferta", "marca", "orden", "op","a");
$valoresTodos = "";
$contValoresTodos = 1;
$urlFiltrosCT = "";
// Se define que variables GET no se toman en cuenta
foreach($_GET as $key=>$val){
    
    if(!in_array($key, $variablesGetExcluidas)){
        $urlFiltrosCT .= "&".$key."=".$val;;
        // and is_numeric($val)
        if($val != ""){
            if($contValoresTodos == 1){
            $valoresTodos .= $val;
            }else {
                $valoresTodos .= "-".$val;
            }
            $contValoresTodos = $contValoresTodos + 1;
        }       
    }
}
$valoresTodos = explode("-", $valoresTodos);
$valoresTodosFinales = "";
$c2 = 1;
foreach($valoresTodos as $val){
    if($c2 == 1){
        if($val != "" and is_numeric($val)){
            $valoresTodosFinales .= $val;
            $c2 = $c2 + 1;
        }
    } else {
        if($val != "" and is_numeric($val)){
            $valoresTodosFinales .= ",".$val;
            $c2 = $c2 + 1;
        }
    }
    
}

/*lo ocupo para completar las url de los filtros que pasan por htaccess*/
$urlNeutra = $_SERVER['REQUEST_URI'];
$urlArregloParaMarcas = explode("?",$urlNeutra);
if($urlArregloParaMarcas[1] != ""){
    $varSiguientes = "?".$urlArregloParaMarcas[1];
} else {
    $varSiguientes = "";
}
/*lo ocupo para completar las url de los filtros que pasan por htaccess*
/*fin  filtros personalizados*/

/*=============================================================
=            Defino SQL para FilTros Personalizados            =
=============================================================*/
if($valoresTodosFinales != ""){
    $whereFiltros = " and p.id = fp.producto_id and fp.valores_filtro_id IN($valoresTodosFinales)";
    $tablasFiltro = ", valores_filtros vf, filtros_productos fp";
    $nv = consulta_bd("vf.nombre, f.id","filtros f, productos p, valores_filtros vf, filtros_productos fp","fp.valores_filtro_id IN($valoresTodosFinales) and fp.valores_filtro_id = vf.id and f.id = fp.filtro_id GROUP BY fp.valores_filtro_id","");
        foreach ($nv as $key => $val) {
            $breadcrum .= "<p>".$val[0]." <a href='".str_replace("filtro".$val[1]."=","a=",$urlNeutra)."'><b>x</b></a></p>";
        }
} else {
    $whereFiltros = "";
    $tablasFiltro = "";
}
/*=====  End of Defino  SQL para FilTros Personalizados  ======*/
/*fin  filtros personalizados*/

/*=========================================
=            Filtros de Marcas            =
=========================================*/
if($marca != 0){
    $marcaTodas = explode("-", $marca);
    $marcasFinal = "";
    $auxM = 1;
    foreach($marcaTodas as $valMarca){ 
        if($auxM == 1){
            if($valMarca != "" and is_numeric($valMarca)){
                $marcasFinal .= $valMarca;
                $auxM = $auxM + 1;
            }
            } else {
                if($valMarca != "" and is_numeric($valMarca)){
                    $marcasFinal .= ",".$valMarca;
                    $auxM = $auxM + 1;
            }
        }          
    }
    $whereMarca = " and p.marca_id IN ($marcasFinal)";
    $nm = consulta_bd("nombre, id","marcas","id IN ($marcasFinal)","");
    foreach ($nm as $key => $val) {
        $breadcrum .= "<p>".$val[0]." <a href='javascript:void(0)'><b class='quitarFiltro' id_marca='".$val[1]."'>x</b></a></p>";
    }
} else {
    $whereMarca;
}
/*=====  End of Filtros de Marcas  ======*/ 

/*=====================================
=            Filtro Oferta            =
=====================================*/
    if($oferta == 1){
		$whereOferta = " and pd.descuento > 0 and pd.descuento < pd.precio";
	} else {
		$whereOferta;
	}
/*=====  End of Filtro Oferta  ======*/

/*====================================
=            filtro Orden            =
====================================*/
	if($orden === "valor-desc"){
		$orderSql = ' valorMenor desc';
		$nombreOrden = "Mayor precio";
	} else if($orden === "valor-asc"){
		$orderSql = ' valorMenor asc';
		$nombreOrden = "Menor precio";
	} else {
		$orderSql = "posicionProducto asc";
		$nombreOrden = "Más relevantes";
	}
/*=====  End of filtro Orden  ======*/

/*======================================================
=            Defino Consultas SQL Generales            =
======================================================*/
/*consulta para mostrar filtros dinamicos segun productos existentes*/
$productosFiltros = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.id, if((select posicion from posicion_productos where producto_id = p.id and subcategoria_id = $id) IS NOT NULL, (select posicion from posicion_productos where producto_id = p.id and subcategoria_id = $id), 999999) as posicionProducto, pd.precio, pd.descuento", "productos p, productos_detalles pd, lineas_productos lp, subcategorias sc, marcas m", "p.id = pd.producto_id AND lp.producto_id = p.id AND sc.id = lp.subcategoria_id AND sc.id = $id AND p.marca_id = m.id AND p.publicado = 1 and pd.precio > 0  group by p.id", "");
/*consulta para mostrar filtros dinamicos segun productos existentes*/




$productosPaginador = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.id, if((select posicion from posicion_productos where producto_id = p.id and subcategoria_id = $id) IS NOT NULL, (select posicion from posicion_productos where producto_id = p.id and subcategoria_id = $id), 999999) as posicionProducto, pd.precio, pd.descuento", "productos p, productos_detalles pd, lineas_productos lp, subcategorias sc, marcas m $tablasFiltro", "p.id = pd.producto_id AND lp.producto_id = p.id AND sc.id = lp.subcategoria_id AND sc.id = $id AND p.marca_id = m.id AND p.publicado = 1 and pd.precio > 0 $whereOferta $whereMarca $whereFiltros group by p.id", "");
$total = mysqli_affected_rows($conexion);

$pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 7; 
	$rutaRetorno = "subcategorias/$id/$nombreSubCategoria/$marca/$orden/$oferta";
    $pages->paginate($rutaRetorno);
    
$productos = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, if((select posicion from posicion_productos where producto_id = p.id and subcategoria_id = $id) IS NOT NULL, (select posicion from posicion_productos where producto_id = p.id and subcategoria_id = $id), 999999) as posicionProducto, m.nombre, pd.id, p.descripcion_breve, pd.stock, pd.stock_reserva", "productos p, productos_detalles pd, lineas_productos lp, subcategorias sc, marcas m $tablasFiltro", "p.id = pd.producto_id AND lp.producto_id = p.id AND sc.id = lp.subcategoria_id AND sc.id = $id AND p.marca_id = m.id AND p.publicado = 1 and pd.precio > 0 $whereOferta $whereMarca $whereFiltros group by p.id", "$orderSql $pages->limit");
$cant_productos = mysqli_affected_rows($conexion);

?>


  <div class="cont100 contBreadCrumbs">
        <div class="cont100Centro">
        	<ul class="breadcrumb">
              <li><a href="home">Home</a></li>
              <li class="">
                  <a href="lineas/<?= $lineas[0][0]."/".url_amigables($lineas[0][1]);?>"><?= $lineas[0][1]; ?></a>
              </li>
              <li class="">
                  <a href="categorias/<?= $lineas[0][2]."/".url_amigables($lineas[0][3]); ?>"><?= $lineas[0][3]; ?></a>
              </li>
              <li class="active"><?= $lineas[0][5]; ?></li>
            </ul>
        </div>
    </div><!--Fin breadcrumbs -->



        
    <div class="cont100 fondoGris">
        <div class="cont100Centro">
            <h1 class="nomCategoria"><?= $lineas[0][5]; ?><span class="cantProdCat"> &nbsp;&nbsp;&nbsp;&nbsp;<span class="tituloSpan">Mostrando:</span><?= $cant_productos; ?></span></h1>
            <?php $url = "subcategorias/$id/$nombreSubCategoria";?>
            <input id="url" type="hidden" value="<?= $url ?>">
            <input id="orden" type="hidden" value="<?= $orden ?>">
            <input id="ofertas" type="hidden" value="<?= $oferta ?>">
            <input id="variablesFiltros" type="hidden" value="<?= $urlFiltrosCT ?>">

            <div class="contFIltrosMovil">
                <div class="filtrosOrden">
                    <div class="contenedorFiltro">
                        <span class="contNomFiltroActual">
                            <span><i class="fas fa-chevron-down"></i></span>
                            <span class="filtroActual"><?= $nombreOrden ?></span>
                        </span>
                        <ul>
                            <li class="<?php if($orden === 0){ echo 'filtroSeleccionado';} ?>">
                                <a href="subcategorias/<?= $id."/".$nombreSubCategoria.$varSiguientes; ?>">Ordenar por relevancia</a>
                            </li>
                            <li class="<?php if($orden === "valor-desc"){ echo 'filtroSeleccionado';} ?>">
                                <a href="subcategorias/<?= $id."/$nombreSubCategoria/".$marca."/valor-desc/$oferta".$varSiguientes; ?>">Precio Mayor a Menor</a>
                            </li>
                            <li class="<?php if($orden === "valor-asc"){ echo 'filtroSeleccionado';} ?>">
                                <a href="subcategorias/<?= $id."/$nombreSubCategoria/$marca/valor-asc/$oferta".$varSiguientes; ?>">Precio Menor a Mayor</a>
                            </li>
                        </ul>
                    </div><!--Fin contenedorFiltro -->
                </div><!--fin filtros-->
            
            
                
                <a href="javascript:void(0)" class="btnFiltrosMovil">
                    Filtros <i class="material-icons">keyboard_arrow_down</i>
                </a>
            </div><!--fin contFIltrosMovil -->
        </div>
    </div>
  
    
    <div class="cont100 marginTop20">
        <div class="cont100Centro">
            
    <!--filtros responsive-->
    <div class="fondoLateralFiltro">
        <div class="lateralFiltro">
            <!--filtros escritorio-->
            <div class="filtrosDesktop">
                <?php
                 if ($breadcrum != "") {
                    echo "
                    <span class='tituloFiltroBreadcrumb'>Filtrado por:</span>
                    <div class='contBreadcrumb'>".$breadcrum."
                    <a class='borrarFiltroBreadcrumb' href='subcategorias/".$id."/".$nombreSubCategoria."'><h4>Borrar filtros</h4></a>
                    </div>";
                }
                /*=====================================
                =            Filtro Marcas            =
                =====================================*/ 
                $pm = consulta_bd("p.marca_id", "productos p, productos_detalles pd, lineas_productos lp, subcategorias sc, marcas m", "p.id = pd.producto_id AND lp.producto_id = p.id AND sc.id = lp.subcategoria_id AND lp.subcategoria_id = $id AND p.marca_id = m.id AND p.publicado = 1 group by p.id", "");
                /*arreglo marcas activas*/
                $idsMarcas = array();
                for($i=0; $i<sizeof($pm); $i++) {
                    if (in_array($pm[$i][0], $idsMarcas)) {
                        //echo "Existe mac";
                    } else {
                        array_push($idsMarcas, $pm[$i][0]);
                    }
                }
                $contador = 0;
                $idsMarcas2 = "";
                foreach ($idsMarcas as $valor) {
                    if($contador == 0){
                        $idsMarcas2 .= $valor;
                    } else {
                        $idsMarcas2 .= " ,".$valor; 
                    }

                    $contador = $contador + 1;
                }
                /*Fin arreglo marcas activas*/
                if($idsMarcas2 != ""){
                    $marcas = consulta_bd("id, nombre","marcas","id > 0 and id IN($idsMarcas2)","id asc");    
                }
                ?>                    
                <!--marcas-->
                    <div class="filtros">
                    <div class="contenedorFiltroLateral">
                    <span class="tituloFiltro">Marcas</span>
                        <ul class="listadoMarcas">
                            <?php 
                            $chequeo = "";
                            for($i=0; $i<sizeof($marcas); $i++) { 
                                if($marca != 0){
                                    $marcaCheq = explode("-", $marca);
                                    foreach($marcaCheq as $valMarca){
                                        // echo $valMarca."<br>";
                                        // echo $marcas[$i][0]."<br>";
                                        if ($valMarca == $marcas[$i][0]) {
                                            $chequeo = "checked";
                                            break 1;
                                        }else {
                                            $chequeo = "";
                                        }
                                    }                        
                                }
                            ?>
                            <label><input class="checkMarcas" type="checkbox" id="<?= $marcas[$i][0]?>" value="<?= $marcas[$i][0]?>" <?= $chequeo?>><?= $marcas[$i][1]?></label><br> 
                            <?php } ?>

                        </ul>
                    </div><!--Fin contenedorFiltroLateral -->
                </div><!--fin filtros-->
                <!-- FIN de filtro MArcas -->
                
                
                <?php
                    /*=============================================================
                    =            Filtros Personalizados con Selectores            =
                    =============================================================*/
                    $contSelectores;
                    $idsProd = array();
                    for($i=0; $i<sizeof($productosFiltros); $i++) {
                        if (in_array($productosFiltros[$i][0], $idsProd)) {
                            //echo "Existe";
                        } else {
                            array_push($idsProd, $productosFiltros[$i][0]);
                        }
                    }
                    $contadorProd = 0;
                    $idsProd2 = "";
                    foreach ($idsProd as $valor) {
                        if($contadorProd == 0){
                            $idsProd2 .= $valor;
                        } else {
                           $idsProd2 .= " ,".$valor; 
                        }

                        $contadorProd = $contadorProd + 1;
                    }
                    /*Fin arreglo marcas activas*/

                    if($idsProd2 != ""){
                        $filtros = consulta_bd("distinct(f.id), f.nombre","filtros f, filtros_productos fp","f.id = fp.filtro_id and fp.producto_id IN($idsProd2)","");
                    }
                    ?>                                                       
                    <?php for($i=0; $i<sizeof($filtros); $i++) { ?> 

                    <!--filtros personalizados-->
                     <div class="filtros">
                        <div class="contenedorFiltroLateral">
                        <span class="tituloFiltro"><?= $filtros[$i][1]; ?></span> 
                            <ul class="listadoMarcas"> 
                                <?php 
                                $valoresFiltros = consulta_bd("distinct(vf.id), vf.nombre,fp.valores_filtro_id","valores_filtros vf, filtros_productos fp","vf.id = fp.valores_filtro_id and fp.valores_filtro_id > 0 and fp.filtro_id = ".$filtros[$i][0]." and fp.producto_id IN($idsProd2)","");
                                for($j=0; $j<sizeof($valoresFiltros); $j++) {
                                    $conjFiltro = "filtro".$filtros[$i][0];
                                    $chequeo2 = "";
                                    if(isset($_GET[$conjFiltro]) and $_GET[$conjFiltro] != 0){
                                        //existe y debo modificarla
                                        foreach($_GET as $key=>$val){
                                            /*diferencio de las variables que vienen por htaccess*/
                                            if(!in_array($key, $variablesGetExcluidas)){
                                                
                                                if($key == $conjFiltro){
                                                    if($val != 0 and $val > 0){
                                                        $valoresFiltroActual = explode("-",$val);
                                                        if(in_array($valoresFiltros[$j][0], $valoresFiltroActual)){
                                                            //ya existe
                                                            $chequeo2 = "checked";
                                                        } 
                                                }
                                            }                                 }
                                            /*diferencio de las variables que vienen por htaccess*/
                                        }
                                        //fin existe y debo modificar
                                    }
                                     
                                    // echo "<option ".$selectorELegido1." fil=".$conjFiltro."=".$valoresFiltros[$j][2]." value=''>".$valoresFiltros[$j][1]."</option>";
                                    echo'<label><input class="checkFiltroEsp" type="checkbox" id="<?= $marcas[$i][0]?>" value="'.$conjFiltro."=".$valoresFiltros[$j][2].'" '.$chequeo2.' >'.$valoresFiltros[$j][1].'</label><br>'; 
                                } ?>
                            </ul>
                        </div><!--Fin contenedorFiltroLateral -->
                    </div><!--fin filtros personalizados-->
                    <?php } ?>
                    <!--End of Filtros Personalizados con Selectores  -->
                
                
                
                
                
                
                
                
                
                
                
                
                
                     
                    
                <a href='<?= "subcategorias/$id/".url_amigables($lineas[0][5])."/$marca/$orden/1".$varSiguientes; ?>' class="ofertasLateral">ver solo Ofertas</a>
                <a href='<?= "subcategorias/$id/".url_amigables($lineas[0][5]); ?>' class="ofertasLateral">Quitar filtros</a>
                
                
                
            </div>
            <!--fin filtros escritorio-->
        </div><!--Fin lateral Filtro-->
    </div><!--fin fondoLateralFiltro-->
    <!--Fin filtros responsive-->
       
            
            
<!--Contenedor grillas -->
	   <div class="contGrillasProductos">
        	<?php for($i=0; $i<sizeof($productos); $i++){ ?>
                <div class="grilla">
                    <a href="#" class="like" rel="<?= $productos[$i][6]; ?>">
                        <i class="<?= (guardadoParaDespues($productos[$i][6])) ? 'fas':'far';?> fa-heart"></i>
                    </a>
                    <?php if(($productos[$i][8] - $productos[$i][9]) <= 0){?> 
                    <div class="cintaSinStockGrilla">
                        <span class="textoCintaSinStock">Este producto se encuentra sin stock</span>
                    </div>
                    <?php } ?>
                    <?php if(ultimasUnidades($productos[$i][6])){ ?>
                    <div class="etq-ultimas">ultimas unidades</div>
                    <?php } ?>
                    <?= porcentajeDescuento($productos[$i][0]); ?>

                    <?php if ($is_cyber AND is_cyber_product($productos[$i][0])): ?>
                        <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                    <?php endif ?>

                    <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="imgGrilla">
                        <img src="<?= imagen("imagenes/productos/", $productos[$i][2]);?>" width="100%">
                    </a>

                    <a class="btnFicha" href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>">Ver ficha</a>

                    <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="valorGrilla">
                    <?php if ($is_cyber AND is_cyber_product($productos[$i][0])):
                        $precios = get_cyber_price($productos[$i][6]); ?>
                            <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                            <span class="antes">$<?= number_format($precios['precio'],0,",",".") ?></span>

                    <?php else: ?>
                        <?php if(tieneDescuento($productos[$i][6])){ ?>
                            <span class='ahorroValor'><?= ahorras($productos[$i][6]); ?></span>
                            <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>    
                            <span class="antes">$<?= number_format(getPrecioNormal($productos[$i][6]),0,",",".") ?></span>

                        <?php }else{ ?>
                            <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                            <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>

                        <?php } ?>
                    <?php endif ?>
                    </a>
                    <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="nombreGrilla"><?= $productos[$i][1]; ?></a>
                    <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="descripcionGrilla"><?= strip_tags(preview($productos[$i][11], 130)); ?></a>
                </div><!--Fin Grilla -->
            
           <?php } ?>
            
           
           <div class="cont100 paginadorGrid">
                <div class="cont100Centro paginador">
                    <?= $pages->display_pages(); ?>
                </div>
            </div><!--Fin paginador -->
           
        </div>
    <!--Fin contenedor grillas -->
            
            
            
        </div> <!--fin cont centro-->
    </div><!--Fin contGeneral -->
    

