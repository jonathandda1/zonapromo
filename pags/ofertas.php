<?php 
require_once 'paginador/paginator.class.php';

/*============================================
=            Recibo variables get            =
============================================*/
$page = (isset($_GET['page'])) ? mysqli_real_escape_string($conexion, $_GET['page']) : 0;
$ipp = (isset($_GET['ipp'])) ? mysqli_real_escape_string($conexion, $_GET['ipp']) : 4;
$marca = (isset($_GET['marca'])) ? mysqli_real_escape_string($conexion, $_GET['marca']) : 0;
$orden = (isset($_GET['orden'])) ? mysqli_real_escape_string($conexion, $_GET['orden']) : 0;
$ancho = (isset($_GET['anchos'])) ? mysqli_real_escape_string($conexion, $_GET['anchos']) : 0;
$perfil = (isset($_GET['perfil'])) ? mysqli_real_escape_string($conexion, $_GET['perfil']) : 0;
$aro = (isset($_GET['aro'])) ? mysqli_real_escape_string($conexion, $_GET['aro']) : 0;

/*=====  End of Recibo variables get  ======*/

$breadcrum = "";

/*==============================================
=            Filtros Personalizados            =
==============================================*/
// Nota: La varible a es para retirar los breadcrum
$variablesGetExcluidas = array("page", "ipp", "marca", "orden", "op","a", "anchos", "perfil","aro");
$valoresTodos = "";
$contValoresTodos = 1;
foreach($_GET as $key=>$val){
    
    if(!in_array($key, $variablesGetExcluidas)){
        // and is_numeric($val)
        if($val != ""){
            if($contValoresTodos == 1){
            $valoresTodos .= $val;
            }else {
                $valoresTodos .= "-".$val;
            }
            $contValoresTodos = $contValoresTodos + 1;
        }
        
        
    }
}
$valoresTodos = explode("-", $valoresTodos);
$valoresTodosFinales = "";
$c2 = 1;
foreach($valoresTodos as $val){
    if($c2 == 1){
        if($val != "" and is_numeric($val)){
            $valoresTodosFinales .= $val;
            $c2 = $c2 + 1;
        }
    } else {
        if($val != "" and is_numeric($val)){
            $valoresTodosFinales .= ",".$val;
            $c2 = $c2 + 1;
        }
    }
    
}

/*lo ocupo para completar las url de los filtros que pasan por htaccess*/
$urlNeutra = $_SERVER['REQUEST_URI'];
$urlArregloParaMarcas = explode("?",$urlNeutra);
if($urlArregloParaMarcas[1] != ""){
    $varSiguientes = "?".$urlArregloParaMarcas[1];
} else {
    $varSiguientes = "";
}
/*lo ocupo para completar las url de los filtros que pasan por htaccess*/

/*=============================================================
=            Defino SQL para FilTros Personalizados            =
=============================================================*/
if($valoresTodosFinales != ""){
    $whereFiltros = " and p.id = fp.producto_id and fp.valores_filtro_id IN($valoresTodosFinales)";
    $tablasFiltro = ", valores_filtros vf, filtros_productos fp";
    $nv = consulta_bd("vf.nombre, f.id","filtros f, productos p, valores_filtros vf, filtros_productos fp","fp.valores_filtro_id IN($valoresTodosFinales) and fp.valores_filtro_id = vf.id and f.id = fp.filtro_id GROUP BY fp.valores_filtro_id","");
        foreach ($nv as $key => $val) {
            $breadcrum .= "<p>".$val[0]." <a href='".str_replace("filtro".$val[1]."=","a=",$urlNeutra)."'><b>x</b></a></p>";
        }
} else {
    $whereFiltros = "";
    $tablasFiltro = "";
}
/*=====  End of Defino  SQL para FilTros Personalizados  ======*/
/*fin  filtros personalizados*/

/*==========================================
=            Filtros de Medidas            =
==========================================*/
// NOTA : Solo se debe activar para la linea 1 (Neumaticos)
if($ancho != 0){
    $whereAncho = " and p.ancho_id = $ancho";
    $na = consulta_bd("nombre","anchos","id = $ancho","");
    // str_replace("anchos=","a=",$urlNeutra);
    $breadcrum .= "<p>Ancho: ".$na[0][0]." <a href='".str_replace("anchos=","a=",$urlNeutra)."'><b>x</b></a></p>";
} else {
    $whereAncho;
}
if($perfil != 0){
    $wherePerfil = " and p.neumatico_perfil_id = $perfil";
    $np = consulta_bd("nombre","neumatico_perfiles","id = $perfil","");
    $breadcrum .= "<p>Perfil: ".$np[0][0]." <a href='".str_replace("perfil=","a=",$urlNeutra)."'><b>x</b></a></p>";
} else {
    $wherePerfil;
}
if($aro != 0){
    $whereAro = " and p.aro_id = $aro";
    $nar = consulta_bd("nombre","aros","id = $aro","");
    $breadcrum .= "<p>Aro: ".$nar[0][0]." <a href='".str_replace("aro=","a=",$urlNeutra)."'><b>x</b></a></p>";
} else {
    $whereAro;
}
/*=====  End of Filtros de Medidas  ======*/

/*=========================================
=            Filtros de Marcas            =
=========================================*/

if($marca != 0){
    $marcaTodas = explode("-", $marca);
    $marcasFinal = "";
    $auxM = 1;
    foreach($marcaTodas as $valMarca){ 
        if($auxM == 1){
            if($valMarca != "" and is_numeric($valMarca)){
                $marcasFinal .= $valMarca;
                $auxM = $auxM + 1;
            }
            } else {
                if($valMarca != "" and is_numeric($valMarca)){
                    $marcasFinal .= ",".$valMarca;
                    $auxM = $auxM + 1;
            }
        }          
    }
    $whereMarca = " and p.marca_id IN ($marcasFinal)";
    $nm = consulta_bd("nombre, id","marcas","id IN ($marcasFinal)","");
    foreach ($nm as $key => $val) {
        $breadcrum .= "<p>".$val[0]." <a href='javascript:void(0)'><b class='quitarFiltro' id_marca='".$val[1]."'>x</b></a></p>";
    }
} else {
    $whereMarca;
}
/*=====  End of Filtros de Marcas  ======*/  

/*=====================================
=            Filtro Oferta            =
=====================================*/
//Nota: aquí se ejecuta siempre
$whereOferta = " and pd.descuento > 0 and pd.descuento < pd.precio";
/*=====  End of Filtro Oferta  ======*/	

/*====================================
=            filtro Orden            =
====================================*/
if($orden === "valor-desc"){
	$orderSql = ' valorMenor desc';
	$nombreOrden = "Mayor precio";
} else if($orden === "valor-asc"){
	$orderSql = ' valorMenor asc';
	$nombreOrden = "Menor precio";
} else {
	$orderSql = " p.fecha_creacion desc";
	$nombreOrden = "Más relevantes";
}
/*=====  End of filtro Orden  ======*/


/*======================================================
=            Defino Consultas SQL Generales            =
======================================================*/

/*consulta para mostrar filtros dinamicos segun productos existentes*/
$productosFiltros = consulta_bd("p.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor", "productos p, productos_detalles pd, marcas m", "p.id = pd.producto_id AND p.marca_id = m.id AND p.publicado = 1  $whereOferta group by p.id ", "$orderSql");
/*consulta para mostrar filtros dinamicos segun productos existentes*/

$productosPaginador = consulta_bd("p.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor", "productos p, productos_detalles pd, marcas m $tablasFiltro", "p.id = pd.producto_id AND p.marca_id = m.id AND p.publicado = 1  $whereMarca $whereOferta $whereFiltros $whereAncho $wherePerfil $whereAro group by p.id ", "$orderSql");
$total = mysqli_affected_rows($conexion);
    $pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 7; 
	$rutaRetorno = "ofertas/$marca/$orden";
    $pages->paginate($rutaRetorno);
    
$productos = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, p.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, pd.id, m.nombre,lp.linea_id", "productos p, productos_detalles pd, marcas m, lineas_productos lp $tablasFiltro", "lp.producto_id = p.id and p.id = pd.producto_id AND p.marca_id = m.id AND p.publicado = 1 $whereOferta $whereMarca $whereFiltros $whereAncho $wherePerfil $whereAro group by p.id", "$orderSql $pages->limit");
$cant_productos = mysqli_affected_rows($conexion);

// decidimos si mostramos filtros Medidas o no
$contLineasB = 0;
for($i=0; $i<sizeof($productos); $i++){
    if ($productos[$i][8] == 1) {
        $contLineasB = 1;
    }
}  

/*=====  End of Defino Consultas SQL Generales  ======*/
?>


<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
    	<ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Ofertas</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->


<div class="cont100 fondoGris">
    <div class="cont100Centro">
        <h1 class="nomCategoria">Ofertas<span class="cantProdCat"> &nbsp;&nbsp;&nbsp;&nbsp;<!--<span class="tituloSpan">Mostrando:</span><?= $cantPF; ?></span>--></h1>
        <?php $url = "ofertas";?>
        <input id="url" type="hidden" value="<?= $url ?>">
        <input id="orden" type="hidden" value="<?= $orden ?>">
        
        <div class="contFIltrosMovil">
            <div class="filtrosOrden">
            <div class="contenedorFiltro">
                <span class="contNomFiltroActual">
                    <span><i class="fas fa-chevron-down"></i></span>
                    <span class="filtroActual"><?= $nombreOrden ?></span>
                </span>
                <ul>
                    <li class="<?php if($orden === 0){ echo 'filtroSeleccionado';} ?>">
                        <a href="ofertas">Ordenar por relevancia</a>
                    </li>
                    <li class="<?php if($orden === "valor-desc"){ echo 'filtroSeleccionado';} ?>">
                        <a href="ofertas<?= "/".$marca."/valor-desc".$varSiguientes; ?>">Precio Mayor a Menor</a>
                    </li>
                    <li class="<?php if($orden === "valor-asc"){ echo 'filtroSeleccionado';} ?>">
                        <a href="ofertas<?= "/$marca/valor-asc".$varSiguientes; ?>">Precio Menor a Mayor</a>
                    </li>
                </ul>
            </div><!--Fin contenedorFiltro -->
            </div><!--fin filtros-->         
                <a href="javascript:void(0)" class="btnFiltrosMovil">
                    Filtros <i class="material-icons">keyboard_arrow_down</i>
                </a>
        </div><!--fin contFIltrosMovil -->            
    </div>
</div>
  
  
<div class="cont100 fondoGris">
    <div class="cont100Centro">
        <div class="fondoLateralFiltro">
            <div class="lateralFiltro">
                <!--filtros escritorio-->
                <div class="filtrosDesktop">
                    <?php
                    if ($breadcrum != "") {
                        echo "
                        <span class='tituloFiltroBreadcrumb'>Filtrado por:</span>
                        <div class='contBreadcrumb'>".$breadcrum."
                        <a class='borrarFiltroBreadcrumb' href='ofertas'><h4>Borrar filtros</h4></a>
                        </div>";
                    }

                    /*=======================================
                    =            FILTROS MEDIDAS            =
                    =======================================*/
                    if ($contLineasB > 0) {  
                    $tablasFiltroMedidas = ", productos_detalles pd, lineas_productos lp";
                    $whereFiltrosMedidas = "$whereOferta AND lp.producto_id = p.id and p.id = pd.producto_id";                  
                    // Consulta de Ancho
                    $pAncho = consulta_bd("p.ancho_id", "productos p, productos_detalles pd, anchos a", "p.id = pd.producto_id AND p.ancho_id = a.id AND p.publicado = 1 $whereOferta group by p.id", "");
                    /*arreglo Anchos activos*/
                    $idsAncho = array();
                    for($i=0; $i<sizeof($pAncho); $i++) {
                        if (in_array($pAncho[$i][0], $idsAncho)) {
                            //echo "Existe mac";
                        } else {
                            array_push($idsAncho, $pAncho[$i][0]);
                        }
                    }
                    $contador = 0;
                    $idsAncho2 = "";
                    foreach ($idsAncho as $valor) {
                        if($contador == 0){
                            $idsAncho2 .= $valor;
                        } else {
                           $idsAncho2 .= " ,".$valor; 
                        }

                        $contador = $contador + 1;
                    }
                    /*Fin arreglo Anchos activos*/
                    if($idsAncho2 != ""){
                        $anchos = consulta_bd("id, nombre","anchos","id > 0 and id IN($idsAncho2)","id asc");    
                    }

                    // Consulta de Perfil
                    $pPerfil = consulta_bd("p.neumatico_perfil_id", "productos p, productos_detalles pd, neumatico_perfiles a", "p.id = pd.producto_id AND p.neumatico_perfil_id = a.id AND p.publicado = 1 $whereOferta group by p.id", "");
                    /*arreglo marcas activas*/
                    $idsPerfil = array();
                    for($i=0; $i<sizeof($pPerfil); $i++) {
                        if (in_array($pPerfil[$i][0], $idsPerfil)) {
                            //echo "Existe mac";
                        } else {
                            array_push($idsPerfil, $pPerfil[$i][0]);
                        }
                    }
                    $contador = 0;
                    $idsPerfil2 = "";
                    foreach ($idsPerfil as $valor) {
                        if($contador == 0){
                            $idsPerfil2 .= $valor;
                        } else {
                           $idsPerfil2 .= " ,".$valor; 
                        }

                        $contador = $contador + 1;
                    }
                    /*Fin arreglo marcas activas*/
                    if($idsPerfil2 != ""){
                        $perfiles = consulta_bd("id, nombre","neumatico_perfiles","id > 0 and id IN($idsPerfil2)","id asc");    
                    }
                    $pAro = consulta_bd("p.aro_id", "productos p, productos_detalles pd, aros a", "p.id = pd.producto_id AND p.aro_id = a.id AND p.publicado = 1 $whereOferta group by p.id", "");
                    /*arreglo marcas activas*/
                    $idsAros = array();
                    for($i=0; $i<sizeof($pAro); $i++) {
                        if (in_array($pAro[$i][0], $idsAros)) {
                            //echo "Existe mac";
                        } else {
                            array_push($idsAros, $pAro[$i][0]);
                        }
                    }
                    $contador = 0;
                    $idsAros2 = "";
                    foreach ($idsAros as $valor) {
                        if($contador == 0){
                            $idsAros2 .= $valor;
                        } else {
                           $idsAros2 .= " ,".$valor; 
                        }

                        $contador = $contador + 1;
                    }
                    /*Fin arreglo marcas activas*/
                    if($idsAros2 != ""){
                        $aros = consulta_bd("id, nombre","aros","id > 0 and id IN($idsAros2)","id asc");    
                    }
                    ?>
                    <!--Medidas-->
                     <div class="filtros">
                        <div class="contenedorFiltroLateral">
                            <!-- Inputs de envio ajax -->
                            <input id="tablasFiltroMedidas" type="hidden" value="<?= $tablasFiltroMedidas?>">
                            <input id="whereFiltrosMedidas" type="hidden" value="<?= $whereFiltrosMedidas?>">
                            <!-- Fin Inputs de envio ajax -->
                            <a href="#" class="accordion-titulo">Medidas<span class="toggle-icon"></span></a>
                            <div class="accordion-content">
                                <select id="ancho">
                                    <option value="">Ancho</option>
                                    <?php 
                                    for($i=0; $i<sizeof($anchos); $i++) {
                                    if($ancho == $anchos[$i][0]){               
                                        echo "<option selected='selected' id_ancho='".$anchos[$i][0]."' value='".$anchos[$i][0]."'>".$anchos[$i][1]."</option>";
                                    } else {
                                        echo "<option id_ancho='".$anchos[$i][0]."' value='".$anchos[$i][0]."'>".$anchos[$i][1]."</option>"; 
                                    }
                                 } ?>
                                </select>
                                <select id="perfilN">
                                    <option value="">Perfil</option>
                                    <?php 
                                    for($i=0; $i<sizeof($perfiles); $i++) {
                                    if($perfil == $perfiles[$i][0]){               
                                        echo "<option  selected='selected' id_perfil='".$perfiles[$i][0]."' value='".$perfiles[$i][0]."'>".$perfiles[$i][1]."</option>";
                                    } else {
                                        echo "<option id_perfil='".$perfiles[$i][0]."' value='".$perfiles[$i][0]."'>".$perfiles[$i][1]."</option>"; 
                                    } 
                                 } ?>
                                </select>
                                <select id="aro">
                                    <option value="">Aro</option>
                                    <?php 
                                    for($i=0; $i<sizeof($aros); $i++) {
                                    if($aro == $aros[$i][0]){               
                                        echo "<option  selected='selected' id_aro='".$aros[$i][0]."' value='".$aros[$i][0]."'>".$aros[$i][1]."</option>";
                                    } else {
                                        echo "<option id_aro='".$aros[$i][0]."' value='".$aros[$i][0]."'>".$aros[$i][1]."</option>"; 
                                    } 
                                 } ?>
                                </select>
                            </div><!-- Fin de acordion -->
                        </div><!--Fin contenedorFiltroLateral -->
                    </div><!--fin filtros-->
                
                   <?php } 
                    /*=====  End of FILTROS MEDIDAS  ======*/?>

                    <?php 
                    /*=====================================
                    =            Filtro Marcas            =
                    =====================================*/                            
                    $pm = consulta_bd("p.marca_id", "productos p, productos_detalles pd, marcas m", "p.id = pd.producto_id AND p.marca_id = m.id AND p.publicado = 1 group by p.id", "");
                    /*arreglo marcas activas*/
                    $idsMarcas = array();
                    for($i=0; $i<sizeof($pm); $i++) {
                        if (in_array($pm[$i][0], $idsMarcas)) {
                            //echo "Existe mac";
                        } else {
                            array_push($idsMarcas, $pm[$i][0]);
                        }
                    }
                    $contador = 0;
                    $idsMarcas2 = "";
                    foreach ($idsMarcas as $valor) {
                        if($contador == 0){
                            $idsMarcas2 .= $valor;
                        } else {
                           $idsMarcas2 .= " ,".$valor; 
                        }

                        $contador = $contador + 1;
                    }
                    /*Fin arreglo marcas activas*/
                    if($idsMarcas2 != ""){
                        $marcas = consulta_bd("id, nombre","marcas","id > 0 and id IN($idsMarcas2)","id asc");    
                    }
                    ?> 
                            
                    <!--marcas-->
                     <div class="filtros">
                        <div class="contenedorFiltroLateral">
                            <a href="#" class="accordion-titulo">Marcas<span class="toggle-icon"></span></a>
                            <div class="accordion-content">
                            <ul class="listadoMarcas">
                                <?php 
                                $chequeo = "";
                                for($i=0; $i<sizeof($marcas); $i++) { 
                                    if($marca != 0){
                                        $marcaCheq = explode("-", $marca);
                                        foreach($marcaCheq as $valMarca){
                                            // echo $valMarca."<br>";
                                            // echo $marcas[$i][0]."<br>";
                                            if ($valMarca == $marcas[$i][0]) {
                                                $chequeo = "checked";
                                                break 1;
                                            }else {
                                                $chequeo = "";
                                            }
                                        }                        
                                    }
                                ?>
                                <label><input class="checkMarcas" type="checkbox" id="<?= $marcas[$i][0]?>" value="<?= $marcas[$i][0]?>" <?= $chequeo?>><?= $marcas[$i][1]?></label><br> 
                                <?php } ?>

                            </ul>
                            </div><!--Fin Acordion -->
                        </div><!--Fin contenedorFiltroLateral -->
                    </div><!--fin filtros-->
                    <!-- FIN de filtro MArcas -->
        
                    
                    <?php
                    /*==============================================
                    =            Filtros Personalizados            =
                    ==============================================*/
                    $contSelectores;
                    $idsProd = array();
                    for($i=0; $i<sizeof($productosFiltros); $i++) {
                        if (in_array($productosFiltros[$i][0], $idsProd)) {
                            //echo "Existe";
                        } else {
                            array_push($idsProd, $productosFiltros[$i][0]);
                        }
                    }
                    $contadorProd = 0;
                    $idsProd2 = "";
                    foreach ($idsProd as $valor) {
                        if($contadorProd == 0){
                            $idsProd2 .= $valor;
                        } else {
                           $idsProd2 .= " ,".$valor; 
                        }

                        $contadorProd = $contadorProd + 1;
                    }
                    /*Fin arreglo marcas activas*/

                    if($idsProd2 != ""){
                        $filtros2 = consulta_bd("distinct(f.id), f.nombre , f.titulo","filtros f, filtros_productos fp","f.id = fp.filtro_id and fp.producto_id IN($idsProd2) GROUP BY  3","");
                    }
                    ?>
                    <?php 
                    foreach ($filtros2 as $value) {
                    $tit = $value[2];
                    $filtros = consulta_bd("distinct(f.id), f.nombre , f.titulo","filtros f, filtros_productos fp","f.id = fp.filtro_id and f.titulo = '$tit'","");
                    $contSelectores += count($filtros);
                    ?>

                    <!--filtros personalizados-->
                     <div class="filtros">
                        <div class="contenedorFiltroLateral">
                            <a href="#" class="accordion-titulo"><?= $value[2]; ?><span class="toggle-icon"></span></a>
                            <div class="accordion-content">                                       
                            <?php for($i=0; $i<sizeof($filtros); $i++) { ?>                 
                    
                                <select class="selectorFiltrosEspeciales">
                                    <option value=""><?= $filtros[$i][1]; ?></option>
                                <?php 
                                $valoresFiltros = consulta_bd("distinct(vf.id), vf.nombre,fp.valores_filtro_id","valores_filtros vf, filtros_productos fp","vf.id = fp.valores_filtro_id and fp.valores_filtro_id > 0 and fp.filtro_id = ".$filtros[$i][0]." and fp.producto_id IN($idsProd2)","");
                                for($j=0; $j<sizeof($valoresFiltros); $j++) {
                                    $conjFiltro = "filtro".$filtros[$i][0];
                                    $selectorELegido1 = "";
                                    if(isset($_GET[$conjFiltro]) and $_GET[$conjFiltro] != 0){
                                        //existe y debo modificarla
                                        foreach($_GET as $key=>$val){
                                            /*diferencio de las variables que vienen por htaccess*/
                                            if(!in_array($key, $variablesGetExcluidas)){
                                                
                                                if($key == $conjFiltro){
                                                    if($val != 0 and $val > 0){
                                                        $valoresFiltroActual = explode("-",$val);
                                                        if(in_array($valoresFiltros[$j][0], $valoresFiltroActual)){
                                                            //ya existe
                                                            $selectorELegido1 = "selected='selected'";
                                                        } 
                                                }
                                            }                                 }
                                            /*diferencio de las variables que vienen por htaccess*/
                                        }
                                        //fin existe y debo modificar
                                    }
                                     
                                    echo "<option ".$selectorELegido1." fil=".$conjFiltro."=".$valoresFiltros[$j][2]." value=''>".$valoresFiltros[$j][1]."</option>";
                                } ?>
                            </select>
                            <?php } ?> 
                            </div> <!--Fin de Acordion -->
                        </div><!--Fin contenedorFiltroLateral -->
                    </div><!--fin filtros personalizados-->
                    <?php } ?> 
                    <input id="contSelectores" type="hidden" value="<?= $contSelectores ?>">
                    <!-- FINAL DE filtros personalizados-->        
    
                <!-- Botones de accion -->
                <!-- <a id="aplicarFiltrosLineas" href='javascript:void(0)' class="ofertasLateral">Aplicar Filtros</a> -->
                <a href='<?= "ofertas"; ?>' class="ofertasLateral">Quitar filtros</a>
                <!-- FINAL de Botones de accion -->        
        
        </div><!--fin filtros escritorio-->
    </div><!--Fin lateral Filtro-->
</div><!--fin fondoLateralFiltro -->     
            
            
            
        	
                  
               
            
            
           <!--Contenedor grillas -->
	       <div class="contGrillasProductos">
            <?php 
            $banner = consulta_bd("banner, url","banner_ofertas","publicado = 1 and banner <> ''","id desc");
            if(count($banner) > 0){?>
                <a href="<?= $banner[0][1]; ?>" class="bannerOfertas">
                    <img src="<?= imagen("imagenes/banner_ofertas/",$banner[0][0]);?>" width="100%">
                </a>
            <?php } ?>
               
            <?php for($i=0; $i<sizeof($productos); $i++){ ?>
               
                  <div class="grilla">
                        <a href="#" class="like" rel="<?= $productos[$i][6]; ?>">
                        <i class="<?= (guardadoParaDespues($productos[$i][6])) ? 'fas':'far';?> fa-heart"></i>
                        </a>
                        <?php if(ultimasUnidades($productos[$i][6])){ ?>
                        <div class="etq-ultimas">ultimas unidades</div>
                        <?php } ?>
                        

                        <?php if ($is_cyber AND is_cyber_product($productos[$i][0])): ?>
                            <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                        <?php endif ?>
                        <?= porcentajeDescuento($productos[$i][0]); ?>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="imgGrilla">
                            <img src="<?= imagen("imagenes/productos/", $productos[$i][2]);?>" width="100%">
                        </a>
                        <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="marcaGrilla"><?= nombreMarca($productos[$i][0]);?></a>
                        
                        <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="nombreGrilla"><?= $productos[$i][1]; ?></a>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="valorGrilla">
                        <?php if ($is_cyber AND is_cyber_product($productos[$i][0])):
                            $precios = get_cyber_price($productos[$i][6]); ?>
                                <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                                <span class="antes"> $<?= number_format($precios['precio'],0,",",".") ?></span>
                            
                        <?php else: ?>
                            <?php if(tieneDescuento($productos[$i][6])){ ?>
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>    
                                <span class="antes"> $<?= number_format(getPrecioNormal($productos[$i][6]),0,",",".") ?></span>
                                
                            <?php }else{ ?>
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                                <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                
                            <?php } ?>
                        <?php endif ?>
                        </a>
                        
                        <a class="btnFicha" href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>"><span class="botonDetalle">Ver Ficha</span></a>
                        <div class="overlay"><a href="javascript:void(0)" rel="<?php echo $productos[$i][6]; ?>" id="btnAgregarFixed"><button class="botonGrilla">AGREGAR AL CARRO</button></a></div>
                        <input type="hidden" value="1" id="cantidad">
                    </div><!--Fin Grilla -->
            
            <?php } ?>
            
               <div class="cont100 paginadorGrid">
                    <div class="paginador">
                        <?= $pages->display_pages(); ?>
                    </div>
                </div><!--Fin paginador -->
        </div>
    <!--Fin contenedor grillas -->
            
            
            
        </div> <!--fin cont centro-->
    </div><!--Fin contGeneral -->
    