<?php
include("../admin/conf.php");
$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
$usuario = consulta_bd("nombre, rut, telefono, email, activo","clientes","id = $id","");
 


?>
<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.uniform.standalone.js"></script>
<script type="text/javascript" src="js/jquery.Rut.js"></script>
<script type="text/javascript" src="js/jquery.label_better.js"></script>
<script type="text/javascript">
	$(function() {
		$(":checkbox").uniform();
        $("#Rut2").Rut({
           on_error: function(){ 
                        //Swal.fire(type:'error', text:'');
                        Swal.fire('Error','Rut incorrecto','error'); 
                        $("#Rut2").addClass("campo_vacio");
                        $("#Rut2").val("");
                    },
           format_on: 'keyup',
           on_success: function(){ $("#Rut2").removeClass("campo_vacio");} 
        });
        
        $('#activo').click(function(){
            if( $('#activo').prop('checked') ) {
                $('#activo').val("1");
                $('#activo').attr("checked","checked");
            } else {
                $('#activo').val("0");
                $('#activo').removeAttr("checked");
            }
        });
        
		$("#actualizarUsuario").click(function(){
			var nombre = $("#nombre2").val();
			var rut = $("#Rut2").val();
			var telefono = $("#telefono2").val();
			var email = $("#email2").val();
			var activo = $("#activo").val();
			
			if(nombre != "" && rut != "" && telefono != "" && email != ""){
				if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				Swal.fire('','Email incorrecto','error'); 
				} else {
                    $("#formActualizaUsuario").submit();
					}
                
				} else {
					console.log("Faltan campos por completar.");
					Swal.fire({
					  icon: 'error',
					  text: 'Faltan campos por completar.'
					});
                    
                    if(nombre != ""){
                        $("#nombre2").removeClass("campo_vacio");
                    } else {
                        $("#nombre2").addClass("campo_vacio");
                        console.log("falta nombre");
                    }
                    
                    if(rut != ""){
                        $("#Rut2").removeClass("campo_vacio");
                    } else {
                        console.log("falta rut");
                        $("#Rut2").addClass("campo_vacio");
                    }
                    if(telefono != ""){
                        $("#telefono2").removeClass("campo_vacio");
                    } else {
                        console.log("falta telefono");
                        $("#telefono2").addClass("campo_vacio");
                    }
                    
                    if(email != ""){
                        $("#email2").removeClass("campo_vacio");
                    } else {
                        console.log("falta email");
                        $("#email2").addClass("campo_vacio");
                    }
				}
			});
			
        /*EFECTO INPUT*/
        $(".label_better").label_better({
          easing: "bounce"
        });
        /*FIN EFECTO INPUT*/
			
	}); 
</script>

<div class="contNuevaDireccion" id="contNuevaDireccion">

    <div class="cont100 direcciones">
        <form method="post" action="ajax/actualizarUsuario.php" id="formActualizaUsuario" class="bl_form">
            <input type="hidden" name="id" value="<?= $id; ?>">
            <h2>Editar usuario</h2>
            <div class="cont100" style="margin: 0 0 20px 0;">
                <div class="ancho50Centro">
                    <input type="checkbox" name="activo" id="activo" value="<?= $usuario[0][4];?>" class="" <?php if($usuario[0][4] == 1){echo 'checked="checked"';}?>>
                    <label class="nombreCheck">Activo</label>
                </div>
            </div>
            <div class="cont100">
                <div class="ancho50Centro">
                    <input class="campoGrande2 label_better" type="text" name="nombre" id="nombre2" value="<?= $usuario[0][0];?>" data-new-placeholder="Nombre" placeholder="Nombre" />
                </div>
                <div class="ancho50Centro">
                    <input class="campoGrande2 label_better" type="text" name="rut" id="Rut2" value="<?= $usuario[0][1];?>" data-new-placeholder="Rut" placeholder="Rut" />
                </div>
            </div>
            <div class="cont100">
                <div class="ancho50Centro">
                  <input class="campoGrande2 label_better" type="text" name="telefono" id="telefono2" value="<?= $usuario[0][2];?>" data-new-placeholder="Teléfono" placeholder="Teléfono" />
                </div>
                <div class="ancho50Centro">
                    <input class="campoGrande2 label_better" type="text" name="email" id="email2" value="<?= $usuario[0][3];?>" data-new-placeholder="Email" placeholder="Email" />
                </div>
              </div> 
               
             
            <div class="cont100">
                <div class="ancho50Centro">
                    <input class="campoGrande2 label_better" type="text" name="clave" id="clave" value="" data-new-placeholder="Clave" placeholder="Clave" />
                </div>
                <div class="ancho50Centro">
                    <input class="campoGrande2 label_better" type="text" name="clave2" id="clave2" value="" data-new-placeholder="Repetir clave" placeholder="Repetir clave" />
                </div>
            </div>
            <div class="cont100">
                <a class="guardarDireccion" id="actualizarUsuario" href="javascript:void(0)">Guardar cambios</a>
            </div>
            
        </form>
    </div>
</div>
            