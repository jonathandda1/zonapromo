 <div class="cont100 contBreadCrumbs" style="margin-bottom:0;">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Encuentranos</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->

<?php $sucursales = consulta_bd("s.id, s.nombre, s.direccion, s.comuna_id, s.link_mapa, c.nombre, s.imagen, s.horarios","sucursales s, comunas c","s.publicado = 1 and s.destacada = 1 and s.comuna_id = c.id","s.posicion asc"); ?> 
<div class="cont100 encuentranosCont fondoGris">
    <div class="cont100Centro">
        <h3 class="tituloEncuentranos">ENCUÉNTRANOS</h3>
        <div class="cont100">
        
            <div class="ancho60 mapaEncuentranos">
            	<div class="map-responsive">
                	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3331.045578488568!2d-70.59865578483335!3d-33.395975080789476!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662cf356d6705e3%3A0xb25d6b37ec1eb058!2sObispo%20Sierra%202819%2C%20Vitacura%2C%20Regi%C3%B3n%20Metropolitana!5e0!3m2!1ses!2scl!4v1625243110280!5m2!1ses!2scl" width="650" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div><!--fin grillaSucursales -->

            <div class="ancho40 infoEncuentranos">
            	<h2>UBICACIÓN</h2>
            	<P>Obispo Sierra 2819, Vitacura, R.M.</P>
            	<h2>HORARIO DE ATENCIÓN</h2>
                <div class="seccionEncuentranos">
                	<p>Lunes a Viernes</p>
                    <p>de 08:00 a 18:30 horas</p>
                	<p>Sábados </p> 
                    <p>de 09:30 a 13:30 horas</p>
                	<br>
                </div>
            	<p>Telefono: <a href="tel:+56223649582">(+56) 22 364 95 82</a></p> 
            	<a target="_blank" href="https://goo.gl/maps/kfctc8yhkAaywuFh8"><button class="btnNosotros">VER GOOGLE MAPS</button></a>            
            </div><!--fin grillaSucursales -->
        </div>
    </div>
</div>

<?php include 'includes/sobreFooter.php'; ?>