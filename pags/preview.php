<?php 

if(!$_SESSION[Provit_admin]){
		echo '<script>location.href ="404";</script>';
	}

	$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
	$nombre = (isset($_GET['nombre'])) ? mysqli_real_escape_string($conexion, $_GET['nombre']) : 0;
    $producto = consulta_bd("distinct(p.id), 
							p.costo_instalacion,
							p.tabla_datos, 
							p.id,
							p.id, 
							p.id, 
							p.instalacion, 
							p.nombre,
							pd.sku,
							p.descripcion, 
							p.ficha_tecnica, 
							pd.precio,
							pd.descuento,
							pd.id,
							pd.stock, 
							pd.stock_reserva, 
                            pd.venta_minima, 
                            p.descripcion_seo, 
                            m.nombre, 
                            pd.precio_cantidad,
                            p.descripcion_tecnica,
                            p.especificaciones,
                            p.garantia",
	"productos p, productos_detalles pd, marcas m",
	"m.id = p.marca_id and p.id = $id and pd.producto_id = p.id",
	"");
    $cantProd = mysqli_affected_rows($conexion);
	//die("$cantProd");

    //cuando no hay poroducto dirijo al 404
    if($cantProd < 1){
        echo '<script>location.href ="404";</script>';
    }

    $galeria = consulta_bd("archivo","img_productos","producto_id = $id","posicion asc");
?>

<script type="text/javascript">
    $(function() {
        $("#spinner").spinner({
			max: <?php echo $producto[0][14] - $producto[0][15] ?>,
			min: <?= $producto[0][16]; ?>,
			step: <?= $producto[0][16]; ?>
        });
		
    });
</script>

<div class="cont100">
    <div class="cont100Centro">
        <?= breadcrumbs($id);?> 
    </div>
</div><!--Fin breadcrumbs -->
    


<div class="contValoresFixed">
	<div class="valoresFixed cont100Centro">
    	<div class="cont50">
        	<div class="contImgFixedFicha">
            	<img src="<?= imagen("imagenes/productos/",$galeria[0][0]);?>" width="100%" />
            </div>
            <div class="infoFixedFicha">
            	<span><strong><?= $producto[0][7]; ?></strong></span>
                <span class="codigo">SKU:<?php echo $producto[0][8]; ?></span>
            </div>
        </div><!--Fin cont50-->
        
        <div class="cont50 floatRight">
        	<a href="javascript:void(0)" rel="<?php echo $producto[0][13]; ?>" class="btnAgregar btnAgregarFixed" id="btnAgregarFixed">
               AGREGAR AL CARRO
            </a> 
        	<div class="contPrecioFixed">
                <p class="precio-ficha">
                    <?php if(tieneDescuento($producto[0][13])){ ?>
                        <span class="precioNormal">Antes: $<?php echo number_format(getPrecioNormal($producto[0][13]),0,",","."); ?></span>
                    <?php } ?>
                    <span class="precioOfertaFicha"><strong>$<?php echo number_format(getPrecio($producto[0][13]),0,",","."); ?></strong></span>
                </p>
            </div>
        </div><!--Fin cont50-->
    </div><!--Fin valoresFixed -->
</div><!--Fin contValoresFixed -->









<div class="cont100">
    <div class="cont100Centro">
        
        <div class="cont33 galeriaImagenes">
            <?php if(ofertaTiempo($producto[0][13])){ ?>
                <div class="countdown" rel="<?= ofertaTiempoHasta($producto[0][13]); ?>"></div>
            <?php } ?>
        	<div class="galeria cont100">
            	<div class="imgPrincipal cont100">

                    <?php if ($is_cyber and is_cyber_product($id)): ?>
                        <div class="img-cyber"><img src="img/iconcyber.png" alt="Cyber"></div>
                    <?php endif ?>
                    
                    <a href="<?= imagen("imagenes/productos/", $galeria[0][0]);?>" class="fancybox cont100" data-fancybox="gallery">
                        <img src="<?= imagen("imagenes/productos/", $galeria[0][0]);?>" width="100%" id="img-grande">
                    </a>
                </div>
                <div class="thumbsGaleriaFicha cont100">
                	<?php for($i=1; $i<sizeof($galeria); $i++){ ?>
                    <a href="<?= imagen("imagenes/productos/", $galeria[$i][0]);?>" data-fancybox="gallery" rel="<?= imagen("imagenes/productos/", $galeria[$i][0]);?>" class="thumbs-galeria img-thumbnail fancybox">
                        <img src="<?= imagen("imagenes/productos/", $galeria[$i][0]);?>" width="100%" />
                    </a>
                    <?php } ?>
                </div>
                	
               
            </div><!-- /Fin Galeria-->
        </div><!--Fin galeriaImagenes-->
        
        
        
        <div class="cont33 infoFichaProducto">
        	<div class="infoProducto">
                <?php if(ultimasUnidades($producto[0][13])){ ?><div class="etq-ultimas">ultimas unidades</div><?php } ?>
                <?php if(($producto[0][14] - $producto[0][15]) > 0){?> 
                <span class="conStock">Stock disponible</span>
                <?php } else { ?>
                <span class="sinStock">Sin stock</span>
                <?php } ?> 
                <h2><?= $producto[0][7]; ?></h2>
                <span class="codigo">SKU:<?php echo $producto[0][8]; ?></span>
                
                <div class="cont100">
                    <p class="precio-ficha">
                        <?php if ($is_cyber and is_cyber_product($id)): 
                            $precios = get_cyber_price($producto[0][13]);
                            $precio_final = $precios['precio_cyber']; ?>
                            <span class="precioNormal">Normal: $<?= number_format($precios['precio'],0,",","."); ?></span>
                            <span class="precioOfertaFicha">$<?= number_format($precios['precio_cyber'],0,",","."); ?></span>
                        <?php else: 
                            $precio_final = getPrecio($producto[0][13]); ?>
                            <?php if(tieneDescuento($producto[0][13])){ ?>
                                <span class="precioNormal">Normal: $<?= number_format(getPrecioNormal($producto[0][13]),0,",","."); ?></span>
                            <?php } ?>
                            <span class="precioOfertaFicha">$<?= number_format(getPrecio($producto[0][13]),0,",","."); ?></span>

                        <?php endif; ?>
                    </p>

                    <?php if($producto[0][10] != ""){ ?> 
                    <div class="contDescargarFicha">
                        <a href="docs/productos/<?= $producto[0][10]; ?>"><span><i class="fas fa-download"></i></span> <span>Descargar ficha técnica</span> </a>
                    </div>
                    <?php } ?> 
                </div>
            
                
                <?php if($producto[0][2] != ""){?> 
                <div class="tablaTecnica">
                    <?= $producto[0][2]; ?>
                </div><!--Fin tablaTecnica -->
                <?php } ?> 
        
            </div>
        </div><!--FIn infoProducto -->
       
        
        
        
        
        
        
         <div class="cont33 infoFichaProducto">
        	   
            <div class="infoProducto">
             	<div class="filaAcciones cont100 filaCantidadAgregar">
                	<div class="cantidad" rel="<?= $producto[0][13]; ?>">
                        <select name="cantidad" id="cantidad">
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                        <!--<input id="spinner" name="cantidad" value="<?= $producto[0][16]; ?>" readonly/>-->
                    </div>
                    <a href="javascript:void(0)" rel="<?= $producto[0][13]; ?>" class="btnAgregar" id="btn_agregarCarro">
                       AGREGAR AL CARRO
                    </a> 
                    
                    <?php if(isset($_COOKIE['usuario_id'])){?>
                         <a href="#" class="btn-lista-ficha" rel="<?= $producto[0][13]; ?>">
                            <i class="<?= (guardadoParaDespues($producto[0][13])) ? 'fas':'far';?> fa-heart"></i>
                        </a>
                         
                </div><!--Fin filaAcciones -->
                
                
                
                <?php if($producto[0][6] == 1){ ?> 
                <div class="filaAcciones cont100">
                    <h4 class="tituloInstalacion">Incluir instalación</h4>
                    <div class="cont50">
                        <div class="contInstalacion">
                            <input type="checkbox" name="instalacion" value="<?= $producto[0][13]; ?>">
                        </div>
                        <div class="contInstalacion">
                            <label><strong>Si</strong> ($<?= number_format($producto[0][1],0,",",".");?> c/u)</label>
                        </div>
                        
                    </div>
                </div>
                <?php } ?> 
				
                <?php  if(opciones("cotizador") == 1){ 
				//aca debe ir la regla para poder cotizar 
				//Solo en el caso que el cliente lo necesite
				?>	
                <div class="filaAcciones cont100">
                	<a href="javascript:void(0)" rel="<?= $producto[0][13]; ?>" class="btnAgregar" id="btn_agregarCotizacion">
                       <i class="fas fa-plus"></i> AGREGAR A COTIZACIÓN
                    </a> 
                </div>    
                <?php } ?>
                
                
                

                <?php if(!tieneDescuento($producto[0][13])){ 
                    if($producto[0][19]){
                ?>
                    <?php $precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","productos_detall_id = ".$producto[0][13],""); ?>

                    <?php if($precios_cantidad){ ?>
                        <div class="filaAcciones cont100 contPrecioCantidad">
                            <!--<p>Descuentos por cantidad</p>-->
                            <?php foreach($precios_cantidad as $pc){ ?>
                            <div class="preciocant"> 
                               <span class="pc1">Si compras</span>
                               <span class="pc2">Más de <?= $pc[1]; ?></span>
                               <span class="pc3">$ <?= number_format((getPrecio($producto[0][13])*((100 - $pc[2]) / 100)),0,",","."); ?></span>
                                
                               <span class="pc4">x unidad</span>
                                <!--<?= $pc[0]; ?> <span> - <?= number_format($pc[2],0,',','.'); ?>%</span>-->
                            </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php }
                } ?>

                
				
                
                <div class="">
                    <h4 class="tituloInstalacion">Medios de pago</h4>
                    <img src="<?= imagen("img/", "mediosPago.jpg"); ?>" width="100%">
                </div>	
                    
                
             </div>
        	
        </div><!--FIn infoProducto -->
       
        
        
        
        
    </div>
</div><!--Fin contenido documento-->



<div class="cont100">
    <div class="cont100Centro">
        <div class="contFilasAccordeon">
            <?php if($producto[0][9] != ''){ ?>
            <div class="filaAccordeon">
                <a class="tituloFilaAccordeon" href="javascript:void(0)">DESCRIPCIÓN<i class="material-icons">add</i></a>
                <div class="contDescricionesFicha">
                    <?php echo $producto[0][9]; ?>
                </div>
            </div><!--fin filaAccordeon-->
            <?php } ?>
            
            <?php if($producto[0][20] != ''){ ?>
            <div class="filaAccordeon">
                <a class="tituloFilaAccordeon" href="javascript:void(0)">FICHA TÉCNICA<i class="material-icons">add</i></a>
                <div class="contDescricionesFicha">
                    <?php echo $producto[0][20]; ?>
                </div>
            </div><!--fin filaAccordeon-->
            <?php } ?>
            
            <?php if($producto[0][21] != ''){ ?>
            <div class="filaAccordeon">
                <a class="tituloFilaAccordeon" href="javascript:void(0)">ESPECIFICACIONES<i class="material-icons">add</i></a>
                <div class="contDescricionesFicha">
                    <?php echo $producto[0][21]; ?>
                </div>
            </div><!--fin filaAccordeon-->
            <?php } ?>
            
            <?php if($producto[0][22] != ''){ ?>
            <div class="filaAccordeon">
                <a class="tituloFilaAccordeon" href="javascript:void(0)">GARANTIA<i class="material-icons">add</i></a>
                <div class="contDescricionesFicha">
                    <?php echo $producto[0][22]; ?>
                </div>
            </div><!--fin filaAccordeon-->
            <?php } ?>
            
        </div>
        
            
    </div>
</div><!--Fin contenido documento-->


<div class="cont100">
    <div class="cont100Centro">
        <h3 class="titulosFicha">Garantia</h3>
        <div class="cont100 contGarantias">
            <div class="selloGarantia"><img src="<?= imagen("img/", "garantia_1.jpg"); ?>"></div>
            <div class="selloGarantia"><img src="<?= imagen("img/", "garantia_2.jpg"); ?>"></div>
            <div class="selloGarantia"><img src="<?= imagen("img/", "garantia_3.jpg"); ?>"></div>
        </div>
    </div>
</div><!--Fin contenido documento-->





<?php 
    //si existe un producto por el id muestro relacionados
	//los relacionados se muestran por la relacion en el administrador y se  completa la cantidad indicada con otros relacionados 
	//de la misma subcategoria
if($cantProd > 0){ 
	$cantidadRelacionados = 4;
	$relacionadosAdmin = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id","productos p, productos_detalles pd, productos_relacionados pr","pr.producto_id = $id and pr.producto_relacionado = p.id and p.id = pd.producto_id and p.publicado = 1 group by(p.id)","p.id desc limit $cantidadRelacionados");
	$cantRelacionadosAdmin = mysqli_affected_rows($conexion);
	
    
	$relacionadosAutomaticos = $cantidadRelacionados - $cantRelacionadosAdmin;
	$relacionados = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id","productos p, lineas l, categorias c, lineas_productos cp, productos_detalles pd, subcategorias sc","l.id=c.linea_id and c.id = sc.categoria_id and sc.id = cp.subcategoria_id and cp.producto_id = p.id and p.id = pd.producto_id and p.publicado = 1 and cp.subcategoria_id = ".$producto[0][6]." and p.id <> $id  group by(p.id)","rand() limit $relacionadosAutomaticos");
	$cantRelacionados = mysqli_affected_rows($conexion);	
?>

    <!--Productos relacionados -->
    <div class="cont100">
        <div class="cont100Centro grillasFicha">
        	<h3 class="titulosFicha">Otras personas también vieron estos productos</h3>
            
            <?php for($i=0; $i<sizeof($relacionadosAdmin); $i++){ ?>
                <div class="grilla">
                    <a href="#" class="like" rel="<?= $relacionadosAdmin[$i][6]; ?>">
                        <i class="<?= (guardadoParaDespues($relacionadosAdmin[$i][6])) ? 'fas':'far';?> fa-heart"></i>
                    </a>
                    <?= porcentajeDescuento($relacionadosAdmin[$i][0]); ?>
                    <?php if(ultimasUnidades($relacionadosAdmin[$i][0])){ ?><div class="etq-ultimas">ultimas unidades</div><?php } ?>
                    
                    <?php if ($is_cyber AND is_cyber_product($relacionadosAdmin[$i][0])): ?>
                        <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                    <?php endif ?>
                    
                	<a href="ficha/<?= $relacionadosAdmin[$i][0]; ?>/<?= url_amigables($relacionadosAdmin[$i][1]); ?>" class="imgGrilla">
                        <img src="<?= imagen("imagenes/productos/", $relacionadosAdmin[$i][2]);?>" width="100%">
                        <?= dctoCantidad($relacionadosAdmin[$i][6]); ?>
                    </a>
                    
                    <a href="ficha/<?= $relacionadosAdmin[$i][0]; ?>/<?= url_amigables($relacionadosAdmin[$i][1]); ?>" class="nombreGrilla"><?= $relacionadosAdmin[$i][1]; ?></a>
                    
                    <a href="ficha/<?= $relacionadosAdmin[$i][0]; ?>/<?= url_amigables($relacionadosAdmin[$i][1]); ?>" class="valorGrilla">
                    <?php if ($is_cyber AND is_cyber_product($relacionadosAdmin[$i][0])):
                        $precios = get_cyber_price($relacionadosAdmin[$i][6]); ?>
                        <span class="antes">Antes $<?= number_format($precios['precio'],0,",",".") ?></span>
                        <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                    <?php else: ?>
                        <?php if(tieneDescuento($relacionadosAdmin[$i][6])){ ?>
                            <span class="antes">Antes $<?= number_format(getPrecioNormal($relacionadosAdmin[$i][6]),0,",",".") ?></span>
                            <span class='conDescuento'>$<?= number_format(getPrecio($relacionadosAdmin[$i][6]),0,",",".") ?></span>
                        <?php }else{ ?>
                            <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class='conDescuento'>$<?= number_format(getPrecio($relacionadosAdmin[$i][6]),0,",",".") ?></span>
                        <?php } ?>
                    <?php endif ?>
                    </a>
                    
                    <a href="ficha/<?= $relacionadosAdmin[$i][0]."/".url_amigables($relacionadosAdmin[$i][1]); ?>" class="btnGrilla">VER FICHA</a>
                    
               <!--     <a href="ficha/<?= $relacionadosAdmin[$i][0]; ?>/<?= url_amigables($relacionadosAdmin[$i][1]); ?>" class="valorGrilla">
                        <?php if(tieneDescuento($relacionadosAdmin[$i][6])){ ?>
                            <span class="antes">$<?= number_format(getPrecioNormal($relacionadosAdmin[$i][6]),0,",",".") ?></span>
                            <span> - </span>
                            <span class='conDescuento'>$<?= number_format(getPrecio($relacionadosAdmin[$i][6]),0,",",".") ?></span>
                        <?php }else{ ?>
                            $<?= number_format(getPrecio($relacionadosAdmin[$i][6]),0,",",".") ?>
                        <?php } ?>
                    </a>
               -->
            </div><!--Fin Grilla -->
            <?php } ?>
            
            
            <?php for($i=0; $i<sizeof($relacionados); $i++){ ?>
            <div class="grilla">
                <?php if(isset($_COOKIE['usuario_id'])){
                    if(guardadoParaDespues($relacionados[$i][6])){ ?>
                        <a href="#" class="like" rel="<?= $relacionados[$i][6]; ?>">
                        <i class="<?= (guardadoParaDespues($relacionados[$i][6])) ? 'fas':'far';?> fa-heart"></i>
                        </a>
                        <?php
                     } ?> 
                    <?= porcentajeDescuento($relacionados[$i][0]); ?>
                    <?php if(ultimasUnidades($relacionados[$i][0])){ ?><div class="etq-ultimas">ultimas unidades</div><?php } ?>
                    
                    <?php if ($is_cyber AND is_cyber_product($relacionados[$i][0])): ?>
                        <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                    <?php endif ?>
                    
                	<a href="ficha/<?= $relacionados[$i][0]; ?>/<?= url_amigables($relacionados[$i][1]); ?>" class="imgGrilla">
                        <img src="<?= imagen("imagenes/productos/", $relacionados[$i][2]);?>" width="100%">
                        <?= dctoCantidad($relacionados[$i][6]); ?>
                    </a>
                    
                    <a href="ficha/<?= $relacionados[$i][0]; ?>/<?= url_amigables($relacionados[$i][1]); ?>" class="nombreGrilla"><?= $relacionados[$i][1]; ?></a>
                    
                    <a href="ficha/<?= $relacionados[$i][0]; ?>/<?= url_amigables($relacionados[$i][1]); ?>" class="valorGrilla">
                    <?php if ($is_cyber AND is_cyber_product($relacionados[$i][0])):
                        $precios = get_cyber_price($relacionados[$i][6]); ?>
                        <span class="antes">Antes $<?= number_format($precios['precio'],0,",",".") ?></span>
                        <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                    <?php else: ?>
                        <?php if(tieneDescuento($relacionados[$i][6])){ ?>
                            <span class="antes">Antes $<?= number_format(getPrecioNormal($relacionados[$i][6]),0,",",".") ?></span>
                            <span class='conDescuento'>$<?= number_format(getPrecio($relacionados[$i][6]),0,",",".") ?></span>
                        <?php }else{ ?>
                            <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span class='conDescuento'>$<?= number_format(getPrecio($relacionados[$i][6]),0,",",".") ?></span>
                        <?php } ?>
                    <?php endif ?>
                    </a>
                    
                    <a href="ficha/<?= $relacionados[$i][0]."/".url_amigables($relacionados[$i][1]); ?>" class="btnGrilla">VER FICHA</a>
                    
               
            </div><!--Fin Grilla -->
            <?php } ?>
            
        </div>
    </div><!--Productos relacionados -->

<?php } //fin condicion relacionados ?>






<?php $banners = consulta_bd("imagen, link", "banner_home", "publicado = 1", "posicion asc");?> 
<div class="cont100">
    <div class="cont100Centro">
        <h3 class="tituloDestacadoHome">Promociones</h3>
        <div class="bannerHome cont100">
            <?php for($i=0; $i<sizeof($banners); $i++) { ?> 
            <a href="<?= $banners[$i][0]; ?>">
                <img src="<?= imagen("imagenes/banner_home/", $banners[$i][0]); ?>">
            </a>
            <?php } ?> 
        </div>
    </div>
</div><!--FIn cont100 -->






 <div class="cont100">   
	<div class="cont100Centro">
        <div class="cont100 ultimosVistos grillasFicha">
            <?= vistosRecientemente("grilla", 4); ?>
            
            <div class="cont100">
            	<a href="ultimos-vistos">Ver historial del navegador</a>
            </div>
        </div>
        <!--Productos relacionados -->
       



    </div><!--Fin centroDocumentos -->
</div><!--FIn cont100 -->












<div>
    <div itemtype="http://schema.org/Product" itemscope>
        <meta itemprop="mpn" content="<?= $producto[0][8] ?>" />
        <meta itemprop="name" content="<?= $producto[0][7] ?>" />
        <?php if (is_array($galeria)): ?>
            <?php for($i=0; $i<sizeof($galeria); $i++){ ?>
                <link itemprop="image" href="imagenes/productos/<?= $galeria[$i][0] ?>" />
            <?php } ?>
        <?php endif ?>
        <meta itemprop="description" content="<?= strip_tags($producto[0][17]) ?>" />
        
        <div itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
            <link itemprop="url" href="<?= $url_base ?>ficha/<?= $id ?>/<?= $nombre ?>" />
            <?php if ($producto[0][14] > 0): ?>
            <meta itemprop="availability" content="https://schema.org/InStock" />
            <?php else: ?>
            <meta itemprop="availability" content="https://schema.org/OutOfStock" />
            <?php endif ?>
            <meta itemprop="priceCurrency" content="CLP" />
            <meta itemprop="itemCondition" content="https://schema.org/NewCondition" />
            <meta itemprop="price" content="<?= $precio_final ?>" />
        </div>
        <meta itemprop="sku" content="<?= $producto[0][8] ?>" />
        <div itemprop="brand" itemtype="http://schema.org/Thing" itemscope>
            <meta itemprop="name" content="<?= $producto[0][18] ?>" />
        </div>
    </div> 
</div>
