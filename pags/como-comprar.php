

<div class="cont100 fondoServicio">
    <div class="cont100Centro">
        
        <div class="tituloContacto">¿Cómo comprar?</div>
        <div class="cont100">
            
            <div class="grillaComoComprar">
                <div class="contImgComoComprar">
                    <img src="<?= imagen("img/", "comoComprar1.jpg"); ?>" width="100%">
                </div><!--fin contImgComoComprar-->
                <p>Vitrinea y luego selecciona el producto que quieras comprar y agrégalo al carro de compras.</p>
                <p>Puedes revisar tu carro de compras las veces que quieras, para agregar o quitar productos.</p>
            </div><!--grillaComoComprar-->
            
            <div class="grillaComoComprar">
                <div class="contImgComoComprar">
                    <img src="<?= imagen("img/", "comoComprar2.jpg"); ?>" width="100%">
                </div><!--fin contImgComoComprar-->
                <p>Encontrarás el resumen de tu compra donde estarán él o los productos agregados al carro.</p>
            </div><!--grillaComoComprar-->
            
            <div class="grillaComoComprar">
                <div class="contImgComoComprar">
                    <img src="<?= imagen("img/", "comoComprar3.jpg"); ?>" width="100%">
                </div><!--fin contImgComoComprar-->
                <p>Debes ingresar tu correo electrónico para luego completar tus datos, seleccionar el documento de compra (boleta o factura), dirección de envío o retiro en tienda; según sea tu elección.</p>
                <p>Una vez que presiones el botón comprar irás al sistema seguro de pago Webpay, Mercado Pago o Transferencia.</p>
            </div><!--grillaComoComprar-->
            
        </div>
    </div>
</div>