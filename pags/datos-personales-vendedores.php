<?php 
	if(!isset($_COOKIE['Vendedor_id'])){
		echo 'location.href = "home?error=Debes iniciar sesión en la sección para vendedores en el footer de la pagina"';
	}
    $empresasAsociadas = consulta_bd("ev.id, e.rut","empresas_vendedores ev, empresas e","ev.vendedor_id = $vendedorID AND e.id = ev.empresa_id",""); 
?>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Datos personales</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro">
        
        <?php 
            include("pags/menuVendedores.php"); 
                if (count($empresasAsociadas) <= 0) { 
        ?>
                    <script>
                        $(".disableSinEmpresa").css("display","none");
                    </script>
        <?php   }     ?>
        <?php $cliente = consulta_bd("id, nombre, apellido, rut, telefono, email","vendedores","id = ".$_COOKIE['Vendedor_id'],""); ?>   
        <section class="contCuentaVendedor">
            <article class="explicaVendedor">
                <span>Hola, <?= $vendedorNombre;?></span>
                <p>Aquí encontrarás tu datos personales, podrás agregar direcciones, revisar tus pedidos, seguir tus compras y revisar todo lo relacionado con tu cuenta en Zona Promo.</p>
            </article>
        </section>
        <section class="datosEditarVendedor">
            <form action="ajax/actualizarDatosVendedor.php" method="post" id="formularioActualizarVendedor" class="bl_form">
                <div class="cont100 FilaDatosUsuario">
                    <div class="cont100">
                        <label for="nombre">Nombre</label>
                        <input type="text" id="nombreVendedor" name="nombreVendedor" class="campoGrandeDisable" value="<?= $vendedorNombre ?>" placeholder="Nombre" data-new-placeholder="Nombre" readonly/>
                    </div>
                    <div class="cont100">
                        <label for="apellidoVendedor">Apellidos</label>
                        <input type="text" id="apellidoVendedor" name="apellidoVendedor" class="campoGrandeDisable" readonly value="<?= $vendedorApellido; ?>" placeholder="Apellidos" data-new-placeholder="Apellidos"/>
                    </div>
                    <div class="cont100">
                        <label for="telefono">Teléfono</label>
                        <input type="text" id="telefonoVendedor" name="telefonoVendedor" class="campoGrandeDisable" readonly value="<?= $vendedorTelefono; ?>" placeholder="Teléfono" data-new-placeholder="Teléfono" maxlength="11" />
                    </div>
                    <div class="cont100">
                        <label for="email">Email</label>
                        <input type="text" id="emailVendedor" name="emailVendedor" class="campoGrandeDisable" readonly value="<?= $vendedorEmail; ?>" placeholder="Email" data-new-placeholder="Email" />
                    </div>
                    <div class="cont100">
                        <label for="rut">Rut</label>
                        <input type="text" id="rutVendedor" name="rutVendedor" class="campoGrandeDisable" readonly value="<?= $vendedorRut; ?>" placeholder="Rut" data-new-placeholder="Rut"/>
                    </div>
                </div>
                <div class="titulosCuenta">
                    <span>Cambiar Contraseña</span>
                </div><!--fin titulosCuenta-->
                
                <div class="cont100 FilaDatosUsuario">
                    <div class="cont100">
                        <label for="password">Contaseña</label>
                        <input type="password" id="claveVendedor" name="claveVendedor" class="campoGrande2" placeholder="Ingrese su clave" data-new-placeholder="Clave"/>
                    </div>
                    <div class="cont100">
                        <label for="clave2">Repita su clave</label>
                        <input type="password" id="clave2Vendedor" name="clave2Vendedor" class="campoGrande2" placeholder="Repita su clave" data-new-placeholder="Repita su clave" />
                    </div>    
                </div>                
                <div class="cont100">
                    <a href="javascript:void(0)" id="btnActualizarVendedor" style="float:left; margin-left:10px;" class="btnFormRegistro">Guardar cambios</a>
                </div>                
            </form>
        </section>

    </div>
</div>
