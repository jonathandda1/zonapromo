 <?php $marcas = consulta_bd("id, nombre, imagen", "marcas", "publicado = 1", "posicion asc");?> 

  <div class="cont100">
        <div class="cont100Centro">
        	<ul class="breadcrumb">
              <li><a href="home">Home</a></li>
              <li class="active">Marcas</li>
            </ul>
        </div>
    </div><!--Fin breadcrumbs -->

        
  
  
    
    <div class="cont100">
        <div class="cont100Centro">
            <div class="contMarcas">
                <div class="cont100">
                    <h3 class="tituloDestacadoHome">Nuestras marcas</h3>
                </div>
            	<?php for($i=0; $i<sizeof($marcas); $i++) { ?> 
                <a class="marca" href="marca/<?= $marcas[$i][0]; ?>/<?= url_amigables($marcas[$i][1]); ?>" >
                    <img src="<?= imagen("imagenes/marcas/", $marcas[$i][2]); ?>">
                </a>
                <?php } ?> 
            </div>
        </div> <!--fin cont centro-->
    </div><!--Fin contGeneral -->
    

