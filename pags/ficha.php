<?php
$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
$nombre = (isset($_GET['nombre'])) ? mysqli_real_escape_string($conexion, $_GET['nombre']) : 0;
$producto = consulta_bd(
                            "distinct(p.id), 
							p.costo_instalacion,
							p.especificaciones, 
							p.datos_tecnicos,
							p.descripcion_breve, 
							p.id, 
							p.instalacion, 
							p.nombre,
							pd.sku,
							p.descripcion, 
							p.ficha_tecnica, 
							pd.precio,
							pd.descuento,
							pd.id,
							pd.stock, 
							pd.stock_reserva, 
                            pd.venta_minima, 
                            p.descripcion_seo, 
                            m.nombre, 
                            pd.precio_cantidad, 
                            pd.imagen1, 
                            pd.imagen2, 
                            pd.imagen3, 
                            pd.imagen4, 
                            pd.imagen5, 
                            p.thumbs, 
                            m.logo_fichas, 
                            p.api, 
                            p.video, 
                            pd.cotizacion_minima",
    "productos p, productos_detalles pd, marcas m",
    "p.id = $id and pd.producto_id = p.id and p.publicado = 1 and m.id = p.marca_id GROUP BY p.id",
    ""
);
$cantProd = mysqli_affected_rows($conexion);

//cuando no hay poroducto dirijo al 404
if ($cantProd < 1) {
    echo '<script>location.href ="404";</script>';
}

$galeria = consulta_bd("archivo", "img_productos", "producto_id = $id", "posicion asc");

$ventaM2 = $producto[0][26];
$metrosCaja = $producto[0][27];
?>

<script type="text/javascript">
    $(function() {
        $("#spinner").spinner({
            max: <?= $producto[0][14] - $producto[0][15] ?>,
            min: <?= $producto[0][16]; ?>,
            step: <?= $producto[0][16]; ?>
        });

    });
</script>
<style>
    div>div.selector {
        background: #fff;
        height: 45px;
        border-radius: 15px;
    }
</style>

<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <?= breadcrumbs($id); ?>
    </div>
</div>
<!--Fin breadcrumbs -->



<div class="contValoresFixed">
    <div class="valoresFixed cont100Centro">
        <div class="">
            <div class="contImgFixedFicha">
                <img src="<?= imagen("imagenes/productos/", $producto[0][25]); ?>" width="100%" />
            </div>
            <div class="infoFixedFicha">
                <span><strong><?= $producto[0][7]; ?></strong></span>
                <span class="codigo">SKU:<?= $producto[0][8]; ?></span>
            </div>
        </div>
        <!--Fin cont50-->

        <div class="floatRight">
            <a href="javascript:void(0)" rel="<?php echo $producto[0][13]; ?>" class="btnAgregar btnAgregarFixed" id="btnAgregarFixed">
                Cotizar
            </a>
            <div class="contPrecioFixed">
                <p class="precio-ficha">
                    <?php if (tieneDescuento($producto[0][13])) { ?>
                        <span class="precioNormal"> $<?php echo number_format(getPrecioNormal($producto[0][13]), 0, ",", "."); ?></span>
                    <?php } ?>
                    <span class="precioOfertaFicha"><strong>$<?php echo number_format(getPrecio($producto[0][13]), 0, ",", "."); ?></strong></span>
                </p>

            </div>
        </div>
        <!--Fin cont50-->
    </div>
    <!--Fin valoresFixed -->
</div>
<!--Fin contValoresFixed -->










<div class="cont100">
    <div class="contFichaImagenes">
        <div class="galeriaImagenes">
            <?php if (ofertaTiempo($producto[0][13])) { ?>
                <div class="countdown" rel="<?= ofertaTiempoHasta($producto[0][13]); ?>"></div>
            <?php } ?>

            <div class="galeria">

                <?php
                    $imagenes = consulta_bd("pd.id, pd.imagen1, pd.imagen2, pd.imagen3, pd.imagen4, pd.imagen5", "productos p, productos_detalles pd","p.id = $id and pd.producto_id = p.id and p.publicado = 1 GROUP BY pd.id","");
                    $cantImagenes = count($imagenes);
                    if ($cantImagenes > 0) {
                ?>
                <div class="thumbsGaleriaFicha">
                    <?php if ($producto[0][28] != "") { ?>
                        <a href="https://www.youtube.com/watch?v=<?= $producto[0][28]; ?>" data-fancybox class="thumbs-galeria img-thumbnail fancybox">
                            <img src="https://img.youtube.com/vi/<?= $producto[0][28]; ?>/mqdefault.jpg" height="100%" style="width:auto;" />
                        </a>
                    <?php } ?>

                    <?php foreach ($imagenes as $value) { ?>

                        <?php if ($value[1] != "") { ?>
                            <a href="<?= imagen("imagenes/productos_detalles/", $value[1]); ?>" data-fancybox="gallery" rel="<?= imagen("imagenes/productos_detalles/", $value[1]); ?>" class="thumbs-galeria img-thumbnail fancybox">
                                <img src="<?= imagen("imagenes/productos_detalles/", $value[1]); ?>" width="100%" />
                            </a>
                        <?php } ?>


                        <?php if ($value[2] != "") { ?>
                            <a href="<?= imagen("imagenes/productos_detalles/", $value[2]); ?>" data-fancybox="gallery" rel="<?= imagen("imagenes/productos_detalles/", $value[2]); ?>" class="thumbs-galeria img-thumbnail fancybox">
                                <img src="<?= imagen("imagenes/productos_detalles/", $value[2]); ?>" width="100%" />
                            </a>
                        <?php } ?>

                        <?php if ($value[3] != "") { ?>
                            <a href="<?= imagen("imagenes/productos_detalles/", $value[3]); ?>" data-fancybox="gallery" rel="<?= imagen("imagenes/productos_detalles/", $value[3]); ?>" class="thumbs-galeria img-thumbnail fancybox">
                                <img src="<?= imagen("imagenes/productos_detalles/", $value[3]); ?>" width="100%" />
                            </a>
                        <?php } ?>


                        <?php if ($value[4] != "") { ?>
                            <a href="<?= imagen("imagenes/productos_detalles/", $value[4]); ?>" data-fancybox="gallery" rel="<?= imagen("imagenes/productos_detalles/", $value[4]); ?>" class="thumbs-galeria img-thumbnail fancybox">
                                <img src="<?= imagen("imagenes/productos_detalles/", $value[4]); ?>" width="100%" />
                            </a>
                        <?php } ?>

                        <?php if ($value[5] != "") { ?>
                            <a href="<?= imagen("imagenes/productos_detalles/", $value[5]); ?>" data-fancybox="gallery" rel="<?= imagen("imagenes/productos_detalles/", $value[5]); ?>" class="thumbs-galeria img-thumbnail fancybox">
                                <img src="<?= imagen("imagenes/productos_detalles/", $value[5]); ?>" width="100%" />
                            </a>
                        <?php } ?>

                    <?php }//Cierre de foreach ?>

                </div>
                
                <?php } //Fin de cantImagenes?>

                <div class="imgPrincipal">

                    <?php if ($is_cyber and is_cyber_product($id)) : ?>
                        <div class="img-cyber"><img src="img/iconcyber.png" alt="Cyber"></div>
                    <?php endif ?>
                    <?php if (($producto[0][14] - $producto[0][15]) <= 0) { ?>
                        <!-- <div class="cintaSinStock">
                            <span class="textoCintaSinStock">Este producto se encuentra sin stock, consulte disponibilidad</span>
                        </div> -->
                    <?php } ?>
                    <div class="" id="contDesc">
                        <?= porcentajeDescuento($producto[0][0]); ?>
                    </div>
                    <div id="imagenesFicha">
                        <a id="enlaceImagen" href="<?= imagen("imagenes/productos_detalles/", $producto[0][20]); ?>" class="fancybox cont100" data-fancybox="gallery">
                            <?php
                                    $imagenFicha1 = "sin-imagen.jpg";
                                    if ($producto[0][20] != "" && $producto[0][20] != null) {
                                        $imagenFicha1 = $producto[0][20];
                                    }
                                ?>
                            <img src="<?= imagen("imagenes/productos_detalles/", $imagenFicha1); ?>" width="100%" id="img-grande">
                        </a>
                    </div>
                    <!--fin imagenesFicha -->

                </div>

            </div><!-- /Fin Galeria-->
        </div>
        <!--Fin galeriaImagenes-->



        <div class="infoFichaProducto">
            <div class="infoProducto">
                <?php if (ultimasUnidades($producto[0][13])) { ?>
                    <div class="etq-ultimas">ultimas unidades</div>
                <?php } ?>

                <!-- <div class="filaPreTitulo">
                    <?php if (($producto[0][14] - $producto[0][15]) > 0) { ?> 
                    <span class="conStock">Stock disponible</span>
                    <?php } else { ?>
                    <span class="sinStock">Sin stock</span>
                    <?php } ?> 
                </div>fin filaPreTitulo -->

                <!-- <h5 class="marcaFicha"><strong><?= $producto[0][18]; ?></strong> </h5> -->

                <h2 class="prodFicha"><?= $producto[0][7]; ?></h2>
                <div class="filaPreTitulo">
                    <span class="codigo">Código: <?php echo $producto[0][8]; ?></span>
                </div>
                <!-- <?php if ($producto[0][29] == 0) { ?>
                    <div class="cont100">
                        <p class="precio-ficha">
                            <?php if ($is_cyber and is_cyber_product($id)) :
                                $precios = get_cyber_price($producto[0][13]);
                                $precio_final = $precios['precio_cyber']; ?>

                                <span class="precioOfertaFicha">$<?= number_format($precios['precio_cyber'], 0, ",", "."); ?></span>
                                <span class="precioNormal"> $<?= number_format($precios['precio'], 0, ",", "."); ?></span>
                            <?php else :
                                $precio_final = getPrecio($producto[0][13]); ?>
                                <span class="precioOfertaFicha">$<?= number_format(getPrecio($producto[0][13]), 0, ",", "."); ?></span>
                                <?php if (tieneDescuento($producto[0][13])) { ?>
                                    <span class="precioNormal"> $<?= number_format(getPrecioNormal($producto[0][13]), 0, ",", "."); ?></span>
                                <?php } ?>

                            <?php endif; ?>
                        </p>
                    </div>
                <?php } ?> -->
                <?php if ($producto[0][4] != "") { ?>
                    <div class="filaAcciones descripcionesLateral">
                        <h3>Descripción</h3>
                        <div class="contDescripcionLateral">
                            <?= $producto[0][4]; ?>
                        </div>
                    </div>
                <?php } ?>

                <?php
                    $color = consulta_bd("pd.sku, pd.id, pd.color_producto_id, pd.imagen1, pd.imagen2, pd.imagen3, pd.imagen4, pd.imagen5, cotizacion_minima", "productos p, productos_detalles pd","p.id = $id and pd.producto_id = p.id and p.publicado = 1 AND pd.color_producto_id != 0 AND pd.color_producto_id IS NOT NULL GROUP BY pd.id","");
                    $cantColor = count($color);
                    if ($cantColor > 0) {
                        $existeColor = 0;
                        for ($i = 0; $i < sizeof($color); $i++) {
                           if ($color[$i][2] != "") {
                                $existeColor++;
                           } 
                        }
                        if ($existeColor > 0) {
                 ?>
                    <div class="contLabelSelect">
                        <label for="color-producto">Color Producto</label>
                        <select name="colorProducto" id="color-producto">
                        <option imagen="<?= $producto[0][20] ?>" value="">Colores</option>
                        <?php for ($i = 0; $i < sizeof($color); $i++) { 
                            $idColor = $color[$i][2];
                            $nombreColor = consulta_bd("c.nombre", "color_productos c", "c.id= $idColor", "");
                            if ($color[$i][3] != ""):
                                $imagenCambiar = $color[$i][3];
                            elseif ($color[$i][4] != ""):
                                $imagenCambiar = $color[$i][4];
                            elseif ($color[$i][5] != ""):
                                $imagenCambiar = $color[$i][5];
                            elseif ($color[$i][6] != ""):
                                $imagenCambiar = $color[$i][6];
                            elseif ($color[$i][7] != ""):
                                $imagenCambiar = $color[$i][7];
                            else:
                                $imagenCambiar == $producto[0][20];
                            endif;
                        ?>
                            <option imagen="<?= $imagenCambiar ?>" idProd="<?= $color[$i][1] ?>" sku="<?= $color[$i][0] ?>" cotMin="<?= $color[$i][8] ?>" value="<?= $color[$i][2] ?>"><?= $nombreColor[0][0] ?></option>
                        <?php } ?> 
                        </select>
                    </div>
                <?php 
                        }//fin de $existeColor
                     }//Fin de cantColor
                 ?> 
                
                <?php
                    $logo = consulta_bd("l.id,l.nombre", "logo l", "", "l.nombre asc");
                    if (count($logo) > 0) {                    
                ?>
                <style>
                    /*the container must be positioned relative:*/
                    .contLabelSelect {
                    position: relative;
                    /* font-family: Arial; */
                    }

                    .contLabelSelect select {
                    display: none; /*hide original SELECT element:*/
                    }

                    .select-selected {
                    background-color: #fff;
                    display: block;
                    white-space: nowrap;
                    cursor: pointer;
                    background: url(css/selector.png) right center no-repeat;
                    }

                    /*style the arrow inside the select element:*/
                    .select-selected:after {
                    /* position: absolute;
                    content: "";
                    top: 28px;
                    right: 10px;
                    width: 0;
                    height: 0;
                    border: 6px solid transparent;
                    border-color: #000 transparent transparent transparent; */
                    }

                    /*point the arrow upwards when the select box is open (active):*/
                    .select-selected.select-arrow-active:after {
                    /* border-color: transparent transparent #fff transparent;
                    top: 7px; */
                    }

                    /*style the items (options), including the selected item:*/
                    .select-selected {
                    color: #9b9a99;
                    width: 100%;
                    border-radius: 15px;
                    margin: 10px 0;
                    height: 45px;
                    line-height: 25px;
                    padding: 8px 16px;
                    border: 1px solid rgb(204,203,203);
                    cursor: pointer;
                    user-select: none;
                    }
                    .select-items div {
                    color: #9b9a99;
                    width: 100%;
                    margin: 0;
                    height: 45px;
                    line-height: 25px;
                    padding: 8px 16px;
                    cursor: pointer;
                    user-select: none;
                    }

                    /*style items (options):*/
                    .select-items {
                        position: absolute;
                        background-color: #fff;
                        top: 64%;
                        right: 0;
                        z-index: 99;
                        width: 100%;
                        max-width: 210px;
                        margin: 0;
                        border: 1px solid #f1f1f1;
                        max-height: 250px;
                        overflow-y: scroll;
                    }

                    .select-items::-webkit-scrollbar {
                        width: 7px;
                    }
                    .select-items::-webkit-scrollbar-thumb {
                        background: #9b9a99;
                        border-radius: 5px;
                    }

                    /*hide the items when the select box is closed:*/
                    .select-hide {
                    display: none;
                    }

                    .select-items div:hover, .same-as-selected {
                    background-color: #f1f1f1;
                    }
                    </style>
                     <div class="contLabelSelect">
                        <label for="logo-producto">Logo</label>
                        <select name="logoProducto" id="logo-producto">
                        <?php for ($i = 0; $i < sizeof($logo); $i++) { ?>
                            <option value="<?= $logo[$i][0] ?>"><?= $logo[$i][1] ?></option>
                            <?php } ?> 
                            </select>
                        </div>
                <?php } ?> 
                <script>
                    var x, i, j, l, ll, selElmnt, a, b, c;
                    /*look for any elements with the class "contLabelSelect":*/
                    x = document.getElementsByClassName("contLabelSelect");
                    l = x.length;
                    for (i = 0; i < l; i++) {
                    selElmnt = x[i].getElementsByTagName("select")[0];
                    ll = selElmnt.length;
                    /*for each element, create a new DIV that will act as the selected item:*/
                    a = document.createElement("DIV");
                    a.setAttribute("class", "select-selected");
                    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
                    x[i].appendChild(a);
                    /*for each element, create a new DIV that will contain the option list:*/
                    b = document.createElement("DIV");
                    b.setAttribute("class", "select-items select-hide");
                    for (j = 1; j < ll; j++) {
                        /*for each option in the original select element,
                        create a new DIV that will act as an option item:*/
                        c = document.createElement("DIV");
                        c.innerHTML = selElmnt.options[j].innerHTML;
                        c.addEventListener("click", function(e) {
                            /*when an item is clicked, update the original select box,
                            and the selected item:*/
                            var y, i, k, s, h, sl, yl;
                            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                            sl = s.length;
                            h = this.parentNode.previousSibling;
                            for (i = 0; i < sl; i++) {
                            if (s.options[i].innerHTML == this.innerHTML) {
                                s.selectedIndex = i;
                                h.innerHTML = this.innerHTML;
                                y = this.parentNode.getElementsByClassName("same-as-selected");
                                yl = y.length;
                                for (k = 0; k < yl; k++) {
                                y[k].removeAttribute("class");
                                }
                                this.setAttribute("class", "same-as-selected");
                                break;
                            }
                            }
                            h.click();
                            let idProd = $('option:selected', '#color-producto').attr("idProd");
                            let sku = $('option:selected', '#color-producto').attr("sku");
                            let cotizacionMinima = $('option:selected', '#color-producto').attr("cotMin");
                            let imagen = $('option:selected', '#color-producto').attr("imagen");
                            // alert(imagen);
                            if (imagen == "") {
                                imagen = "sin-imagen.jpg";
                            }
                            if (idProd == null || sku == null) {
                                // location.reload();
                            }else{
                            // alert(idProd);
                            let verificacion = imagen.indexOf("http") > -1;
                            if (verificacion) {
                                $("#img-grande").attr("src",imagen);
                                $("#enlaceImagen").attr("href",imagen);
                                
                            } else {
                                $("#img-grande").attr("src","imagenes/productos_detalles/"+imagen);
                                $("#enlaceImagen").attr("href","imagenes/productos_detalles/"+imagen);
                            }
                            $('#btn_agregarCarro').attr('rel', idProd);
                            $('#btnAgregarFixed').attr('rel', idProd);
                            $('.codigo').html("Código: "+sku);
                            $('#cotMin').html("Cotización mínima de " +cotizacionMinima+  "productos*");
                            }
                        });
                        b.appendChild(c);
                    }
                    x[i].appendChild(b);
                    a.addEventListener("click", function(e) {
                        /*when the select box is clicked, close any other select boxes,
                        and open/close the current select box:*/
                        e.stopPropagation();
                        closeAllSelect(this);
                        this.nextSibling.classList.toggle("select-hide");
                        this.classList.toggle("select-arrow-active");
                        });
                    }
                    function closeAllSelect(elmnt) {
                    /*a function that will close all select boxes in the document,
                    except the current select box:*/
                    var x, y, i, xl, yl, arrNo = [];
                    x = document.getElementsByClassName("select-items");
                    y = document.getElementsByClassName("select-selected");
                    xl = x.length;
                    yl = y.length;
                    for (i = 0; i < yl; i++) {
                        if (elmnt == y[i]) {
                        arrNo.push(i)
                        } else {
                        y[i].classList.remove("select-arrow-active");
                        }
                    }
                    for (i = 0; i < xl; i++) {
                        if (arrNo.indexOf(i)) {
                        x[i].classList.add("select-hide");
                        }
                    }
                    }
                    /*if the user clicks anywhere outside the select box,
                    then close all select boxes:*/
                    document.addEventListener("click", closeAllSelect);
                </script>
            </div>
            
            <?php
                // echo "hola".$producto[0][29];
                if ($producto[0][29] != "" && $producto[0][29] != null) {
            ?>
                <div class="contCantMinimaCotizar">
                    <p id="cotMin">Cotización mínima de <?= $producto[0][29] ?> productos*</p>
                </div>
            <?php
                }
            ?>

            <div class="infoProducto">

                <?php if ($producto[0][10] != "") { ?>
                    <div class="contDescargarFicha">
                        <a href="docs/productos/<?= $producto[0][10]; ?>"> <span>Descargar ficha técnica</span><span><i class="fas fa-download"></i></span></a>
                    </div>
                <?php } ?>
               
                <div class="filaAcciones cont100 filaCantidadAgregar">
                        <div class="cantidad" rel="<?= $producto[0][13]; ?>">
                            <select name="cantidad" id="cantidad" class="selectCantidad">
                                <option value="1" selected>01</option>
                                <option value="2">02</option>
                                <option value="3">03</option>
                                <option value="4">04</option>
                                <option value="5">05</option>
                                <option value="6">06</option>
                                <option value="7">07</option>
                                <option value="8">08</option>
                                <option value="9">09</option>
                                <option value="10">10</option>
                                <option value="11">Más de 10</option>
                            </select>
                        </div> 
                        <a href="javascript:void(0)" rel="<?= $producto[0][13]; ?>" class="btnAgregar" id="btn_agregarCarro">
                            Cotizar
                        </a>
                    <a href="#" class="btn-lista-ficha" rel="<?= $producto[0][13]; ?>">
                        <i class="<?= (guardadoParaDespues($producto[0][13])) ? 'fas' : 'far'; ?> fa-heart"></i>
                    </a>
                </div>
                <!--Fin filaAcciones -->



                <?php if (!tieneDescuento($producto[0][13])) {
                    if ($producto[0][19]) {
                ?>
                        <?php $precios_cantidad = consulta_bd("nombre, rango, descuento", "precios_cantidades", "productos_detall_id = " . $producto[0][13], ""); ?>

                        <?php if ($precios_cantidad) { ?>
                            <div class="filaAcciones cont100 contPrecioCantidad">
                                <!--<p>Descuentos por cantidad</p>-->
                                <?php foreach ($precios_cantidad as $pc) { ?>
                                    <div class="preciocant">
                                        <span class="pc1">Si compras</span>
                                        <span class="pc2">Más de <?= $pc[1]; ?></span>
                                        <span class="pc3">$ <?= number_format((getPrecio($producto[0][13]) * ((100 - $pc[2]) / 100)), 0, ",", "."); ?></span>

                                        <span class="pc4">x unidad</span>
                                        <!--<?= $pc[0]; ?> <span> - <?= number_format($pc[2], 0, ',', '.'); ?>%</span>-->
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                <?php }
                } ?>







            </div>

        </div>
        <!--FIn infoProducto -->





    </div>
</div>
<!--Fin contenido documento-->

<?php if ($producto[0][9] != '' || $producto[0][2] != "" || $producto[0][3] != "") { ?>
    <div class="cont100 conTabDes">
        <div class="cont100Centro">
            <div class="contDescripciones">
                <div class="cont100 titulosMenuTabs">
                    <?php if ($producto[0][9] != '') { ?>
                        <a href="javascript:void(0)" class="tabActivo" rel="tabDescripcion">Descripción</a>
                    <?php } ?>
                    <?php if ($producto[0][2] != "") { ?>
                        <a href="javascript:void(0)" rel="tabEspecificaciones">Especificaciones</a>
                    <?php } ?>

                    <?php if ($producto[0][3] != "") { ?>
                        <a href="javascript:void(0)" rel="tabFichaTecnica">Ficha técnica</a>
                    <?php } ?>
                </div>


                <?php if ($producto[0][9] != '') { ?>
                    <div class="contDescripcion tabsGenerico" id="tabDescripcion" style="display:block;">
                        <?= $producto[0][9]; ?>
                    </div>
                    <!--fin descripcion -->
                <?php } ?>
                <?php if ($producto[0][2] != "") { ?>
                    <div class="tablaTecnica tabsGenerico" id="tabEspecificaciones">
                        <?= $producto[0][2]; ?>
                    </div>
                    <!--Fin tabEspecificaciones-->
                <?php } ?>

                <?php if ($producto[0][3] != '') { ?>
                    <div class="tablaTecnica tabsGenerico" id="tabFichaTecnica">
                        <?= $producto[0][3]; ?>
                    </div>
                    <!--Fin tablaTecnica Ficha tecnica-->
                <?php } ?>


            </div>
            <!--fin contDescripciones -->


        </div>
    </div>
<?php } ?>



<div class="cont100 grillaNormal">
    <div class="cont100Centro">
        <h3 class="tituloDestacadoHome">Clientes que vieron este producto también vieron</h3>
        <div class="cont100 ultimosVistos grillasFicha">
            <?= vistosRecientemente("grilla", 5); ?>
        </div>
        <!--Productos relacionados -->
    </div>
    <!--Fin centroDocumentos -->
</div>
<!--FIn cont100 -->

<div class="cont100 fondoGris grillaResponsive grillaResponsiveficha">
    <div class="cont100Centro">
        <div class="cont100" style="background: #fff;">
            <?= vistosRecientemente("lista", 4); ?>
        </div>
        <!--Productos relacionados -->
    </div>
    <!--Fin centroDocumentos -->
</div>
<!--FIn cont100 -->


<!-- Banner3 -->
<?php 
    $banners3 = consulta_bd("nombre,titulo_banner1,banner1, link1,titulo_banner2,banner2, link2,titulo_banner3,banner3, link3 ,titulo_banner4,banner4, link4,titulo_banner5,banner5, link5,titulo_banner6,banner6, link6,titulo_banner7,banner7, link7", "banner_home3", "publicado = 1", "id asc");
    if(count($banners3) > 0){
?>
<div class="cont100">
    <section class="contBanner3">
        <h3 class="tituloDestacadoHome tituloBanner3"><?= $banners3[0][0]; ?></h3>
        <a href="<?= $banners3[0][3]; ?>" class="banner3-1">
            <img src="imagenes/banner_home3/<?=$banners3[0][2]?>" alt="<?= $banners3[0][1]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][1]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][6]; ?>" class="banner3-2">
            <img src="imagenes/banner_home3/<?=$banners3[0][5]?>" alt="<?= $banners3[0][4]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][4]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][9]; ?>" class="banner3-3">
            <img src="imagenes/banner_home3/<?=$banners3[0][8]?>" alt="<?= $banners3[0][7]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][7]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][12]; ?>" class="banner3-4">
            <img src="imagenes/banner_home3/<?=$banners3[0][11]?>" alt="<?= $banners3[0][10]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][10]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][15]; ?>" class="banner3-5">
            <img src="imagenes/banner_home3/<?=$banners3[0][14]?>" alt="<?= $banners3[0][13]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][13]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][18]; ?>" class="banner3-6">
            <img src="imagenes/banner_home3/<?=$banners3[0][17]?>" alt="<?= $banners3[0][16]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][16]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][21]; ?>" class="banner3-7">
            <img src="imagenes/banner_home3/<?=$banners3[0][20]?>" alt="<?= $banners3[0][19]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][19]; ?></span></h6>
        </a>
    </section>
</div>
<?php } ?>
<!--Fin de Banner3-->


<div>
    <div itemtype="http://schema.org/Product" itemscope>
        <meta itemprop="mpn" content="<?= $producto[0][8] ?>" />
        <meta itemprop="name" content="<?= $producto[0][7] ?>" />
        <?php if (is_array($galeria)) : ?>
            <?php for ($i = 0; $i < sizeof($galeria); $i++) { ?>
                <link itemprop="image" href="imagenes/productos/<?= $galeria[$i][0] ?>" />
            <?php } ?>
        <?php endif ?>
        <meta itemprop="description" content="<?= strip_tags($producto[0][17]) ?>" />

        <div itemprop="offers" itemtype="http://schema.org/Offer" itemscope>
            <link itemprop="url" href="<?= $url_base ?>ficha/<?= $id ?>/<?= $nombre ?>" />
            <?php if ($producto[0][14] > 0) : ?>
                <meta itemprop="availability" content="https://schema.org/InStock" />
            <?php else : ?>
                <meta itemprop="availability" content="https://schema.org/OutOfStock" />
            <?php endif ?>
            <meta itemprop="priceCurrency" content="CLP" />
            <meta itemprop="itemCondition" content="https://schema.org/NewCondition" />
            <meta itemprop="price" content="<?= $precio_final ?>" />
        </div>
        <meta itemprop="sku" content="<?= $producto[0][8] ?>" />
        <div itemprop="brand" itemtype="http://schema.org/Thing" itemscope>
            <meta itemprop="name" content="<?= $producto[0][18] ?>" />
        </div>
    </div>
</div>