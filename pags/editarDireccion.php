<?php
include("../admin/conf.php");
$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
$direccion = consulta_bd("nombre, region_id, comuna_id, calle, numero","clientes_direcciones","id = $id","");

$regiones = consulta_bd("id, nombre","regiones","id <> 16","posicion asc");
 
$comuna = $direccion[0][2];
//$comuna_id = consulta_bd("ciudade_id","comunas","id = $comuna","nombre asc"); 
$region_id = $direccion[0][1]; 

$comunas = consulta_bd("id, nombre","comunas","region_id = $region_id","id asc"); 

//$localidades = consulta_bd("id, nombre","localidades","comuna_id = $comuna","id asc"); 
//$localidad_id = $direccion[0][4];
?>
<!-- JavaScript -->
<script type="text/javascript" src="js/jquery.uniform.standalone.js"></script>
<script type="text/javascript" src="js/jquery.label_better.js"></script>
<script type="text/javascript">
	$(function() {
		$("select").uniform();
		
		$("#region2").change(function() {
			var region_id = $(this).val();
			$("#comuna2").load("tienda/comunas.php?region_id=" + region_id);
			$("#uniform-comuna2 span").html("Seleccione comuna");
		});
		$("#actualizarDireccion").click(function(){
			var nombre = $("#nombre2").val();
			var region = $("#region2").val();
			var comuna = $("#comuna2").val();
			var calle = $("#calle2").val();
			//var numero = $("#numero2").val();
			if(nombre != "" && region != "" && comuna != "" && calle != ""){
				$("#formActualizaDireccion").submit();
				} else {
					console.log("Faltan campos por completar.");
					Swal.fire({
					  icon: 'error',
					  text: 'Faltan campos por completar.'
					});
				}
			});
			
        /*EFECTO INPUT*/
        $(".label_better").label_better({
          easing: "bounce"
        });
        /*FIN EFECTO INPUT*/
			
	}); 
</script>

<div class="contNuevaDireccion" id="contNuevaDireccion">

    <div class="cont100 direcciones">
        <form method="post" action="ajax/actualizarDireccion.php" id="formActualizaDireccion" class="bl_form">
            <input type="hidden" name="id" value="<?= $id; ?>">
            <h2>Editar dirección</h2>
            <div class="cont100">
                <input class="campoGrande2 label_better" type="text" name="nombre" id="nombre2" value="<?= $direccion[0][0];?>" data-new-placeholder="Nombre asignado" placeholder="Nombre asignado" />
            </div>
            <div class="cont100">
                <div class="col1-50">
                    <select name="region_id" id="region2">
                        <?php for($i=0; $i<sizeof($regiones); $i++) {?>
                        <option <?php if($regiones[$i][0] == $direccion[0][1]){echo "selected";}?> value="<?= $regiones[$i][0];?>"><?= $regiones[$i][1];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col2-50">
                   <select name="comuna_id" id="comuna2">
                        <?php for($i=0; $i<sizeof($comunas); $i++){ ?>
                        <option <?php if($comunas[$i][0] == $comuna){echo "selected";} ?> value="<?= $comunas[$i][0]; ?>"><?= $comunas[$i][1]; ?></option>
                        <?php } ?>
                    </select>
                </div>
                
              </div> 
               
             
            <div class="cont100">
                <input class="campoGrande2 label_better" type="text" name="calle" value="<?= $direccion[0][3];?>" id="calle2" data-new-placeholder="Calle" placeholder="Calle" />
            </div>
            <div class="cont100">
                <a class="guardarDireccion" id="actualizarDireccion" href="javascript:void(0)">Guardar cambios</a>
            </div>
            
        </form>
    </div>
</div>
            