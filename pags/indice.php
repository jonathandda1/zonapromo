<?php 

  

?> 


<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Índice</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->




<div class="cont100 contTituloPrincipal">
    <div class="cont100Centro ">
        <h1 class="nomCategoria">Índice de productos</h1>
    </div>
</div>
    

<div class="cont100">
    <div class="cont100Centro">
        <a class="masP" href="javascript:void(0)">LOS MAS POPULARES</a>
        <div class="contLetras">
            <div class="cont100">
            <?php 
                for($j=65; $j<=90; $j++) {  
                    
                    $letra = chr($j); 

                    $lineasIndice = consulta_bd("id, nombre","lineas","publicado = 1 and nombre like '$letra%'","id asc");
                    $catIndice = consulta_bd("id, nombre","categorias","publicada = 1 and nombre like '$letra%'","id asc");
                    $subcatIndice = consulta_bd("id, nombre","subcategorias","publicada = 1 and nombre like '$letra%'","id asc");
                    if(count($lineasIndice) > 0 || count($catIndice) > 0 || count($subcatIndice) > 0){
                        echo "<a class='letraMenu' href='indice#$letra'>$letra</a>";

                    } else {
                        echo "<a class='letraMenu letraMenuDesactivada' href='javascript:void(0)'>$letra</a>";
                    }
                ?> 
       
        <?php } ?>  
                
            
            </div>
            
            <div class="cont100">
            <?php 
                for($j=0; $j<=9; $j++) {  
                    $lineasIndice = consulta_bd("id, nombre","lineas","publicado = 1 and nombre like '$j%'","id asc");
                    $catIndice = consulta_bd("id, nombre","categorias","publicada = 1 and nombre like '$j%'","id asc");
                    $subcatIndice = consulta_bd("id, nombre","subcategorias","publicada = 1 and nombre like '$j%'","id asc");
                    if(count($lineasIndice) > 0 || count($catIndice) > 0 || count($subcatIndice) > 0){
                        echo "<a class='letraMenu' href='indice#numero$j'>$j</a>";

                    } else {
                        echo "<a class='letraMenu letraMenuDesactivada' href='javascript:void(0)'>$j</a>";
                    }
                }
            ?>
            </div>
        </div>
        
        
    </div>
</div>
    

<div class="cont100">
    <div class="cont100Centro">
        <h2 class='tituloLetra'>Los más buscados</h2>
        <?php 
         $catIndice = consulta_bd("id, nombre","categorias","publicada = 1 ","rand() limit 12");
            for($i=0; $i<sizeof($catIndice); $i++) {
                echo '<a href="categorias/'.$catIndice[$i][0].'/'.url_amigables($catIndice[$i][1]).'" class="botonLetra">'.$catIndice[$i][1].'</a>';
            }
            
        ?> 
    </div>
</div>


<div class="cont100">
    <div class="cont100Centro">
        <?php 
        for($j=65; $j<=90; $j++) {  
            $letra = chr($j); 
            
            $lineasIndice = consulta_bd("id, nombre","lineas","publicado = 1 and nombre like '$letra%'","id asc");
            $catIndice = consulta_bd("id, nombre","categorias","publicada = 1 and nombre like '$letra%'","id asc");
            $subcatIndice = consulta_bd("id, nombre","subcategorias","publicada = 1 and nombre like '$letra%'","id asc");
            if(count($lineasIndice) > 0 || count($catIndice) > 0 || count($subcatIndice) > 0){
            
                echo "<h2 class='tituloLetra' id='".$letra."'>".$letra."</h2>";
                for($i=0; $i<sizeof($lineasIndice); $i++) {
                    echo '<a href="lineas/'.$lineasIndice[$i][0].'/'.url_amigables($lineasIndice[$i][1]).'" class="botonLetra">'.$lineasIndice[$i][1].'</a>';
                }
                for($i=0; $i<sizeof($catIndice); $i++) {
                    echo '<a href="categorias/'.$catIndice[$i][0].'/'.url_amigables($catIndice[$i][1]).'" class="botonLetra">'.$catIndice[$i][1].'</a>';
                }
                for($i=0; $i<sizeof($subcatIndice); $i++) {
                    echo '<a href="subcategorias/'.$subcatIndice[$i][0].'/'.url_amigables($subcatIndice[$i][1]).'" class="botonLetra">'.$subcatIndice[$i][1].'</a>';
                }
            }
        ?> 
       
        <?php }  ?> 
        
        
        
    </div>
</div>




<div class="cont100">
    <div class="cont100Centro">
        <?php 
        for($j=0; $j<=9; $j++) {  
            $lineasIndice = consulta_bd("id, nombre","lineas","publicado = 1 and nombre like '$j%'","id asc");
            $catIndice = consulta_bd("id, nombre","categorias","publicada = 1 and nombre like '$j%'","id asc");
            $subcatIndice = consulta_bd("id, nombre","subcategorias","publicada = 1 and nombre like '$j%'","id asc");
            
                if(count($lineasIndice) > 0 || count($catIndice) > 0 || count($subcatIndice) > 0){
                   echo "<h2 class='tituloLetra' id='numero".$j."'>".$j."</h2>";
                for($i=0; $i<sizeof($lineasIndice); $i++) {
                    echo '<a href="lineas/'.$lineasIndice[$i][0].'/'.url_amigables($lineasIndice[$i][1]).'" class="botonLetra">'.$lineasIndice[$i][1].'</a>';
                }
                for($i=0; $i<sizeof($catIndice); $i++) {
                    echo '<a href="categorias/'.$catIndice[$i][0].'/'.url_amigables($catIndice[$i][1]).'" class="botonLetra">'.$catIndice[$i][1].'</a>';
                }
                for($i=0; $i<sizeof($subcatIndice); $i++) {
                    echo '<a href="subcategorias/'.$subcatIndice[$i][0].'/'.url_amigables($subcatIndice[$i][1]).'" class="botonLetra">'.$subcatIndice[$i][1].'</a>';
                } 
            }
            
        ?> 
       
        <?php }  ?> 
        
        
        
    </div>
</div>
