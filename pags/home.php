<?php
$popup = consulta_bd('activo', 'active_popup', '', '');  
$nombre_cliente = opciones('nombre_cliente');
$nombre_popup = 'POPUP'.strtoupper(str_replace(' ', '', $nombre_cliente));
if ($popup[0][0] == 1 AND !isset($_COOKIE[$nombre_popup])) { ?>
    <script>
        setTimeout(function(){
            $('.bg-popup').fadeIn();
            $('.popup-news').fadeIn();
        }, 2000)
    </script>
<?php }
?>
<div class="bg-popup" data-cliente="<?= $nombre_popup ?>">
    <div class="popup-news">
        <div class="contenido">
            <div class="close"></div>
            <h3 style="text-align: center;">ACÁ EL LOGO</h3>
            <p>¡Suscríbete a nuestro newsletter y <br>entérate primero de nuestras ofertas, novedades y más!</p>
            <div class="datos">
                <input type="text" name="nombre" placeholder="Nombre" id="nombre-suscribe">
                <input type="email" name="email" placeholder="Email" id="email-suscribe">
                <a href="#" id="suscribe-newsletter">SUSCRIBIRME</a>
                <h3 style="text-align: center;">LOGO PEQUEÑO</h3>
            </div>
        </div>
    </div>
</div>

<!-- Slider -->
<div class="cont100">
    <?php 
        $slider = consulta_bd("link, imagen, imagen_tablet, imagen_movil, titulo1, titulo2, subtitulo","slider","publicado = 1","posicion ASC");
    ?>
    <div class="slider desktop">
        <div class="slides cycle-slideshow" data-cycle-fx=fade data-cycle-timeout=4000 data-cycle-slides="> a" data-cycle-prev="#prev" data-cycle-next="#next" data-cycle-pager=".example-pager">
            <?php foreach($slider as $sl){ ?>
                <a <?php if($sl[0]) echo 'href="'.$sl[0].'"'?> class="slide">
                    <img src="<?= imagen("imagenes/slider/", $sl[1]);?>" width="100%">
                    <div class="cycle-overlay"><?= $sl[4];?></div>
                    <div class="cycle-overlay2"><?= $sl[5];?></div>
                    <div class="cycle-overlaySub"><?= $sl[6];?></div>
                    <div class="example-pager"></div>
                </a>
            <?php } ?>            
        </div>        
    </div>

    <div class="slider tablet">
        
        <div class="slides cycle-slideshow" data-cycle-fx=fade data-cycle-timeout=4000 data-cycle-slides="> a" data-cycle-prev="#prev" data-cycle-next="#next" data-cycle-pager=".example-pager2">
            <?php foreach($slider as $sl){ ?>
                <a <?php if($sl[0]) echo 'href="'.$sl[0].'"'?> class="slide">
                    <img src="<?= imagen("imagenes/slider/", $sl[2]); ?>" width="100%">
                    <div class="cycle-overlay3"><?= $sl[4];?></div>
                    <div class="cycle-overlay4"><?= $sl[5];?></div>
                    <div class="cycle-overlaySub"><?= $sl[6];?></div>
                    <div class="example-pager2"></div>
                </a>
            <?php } ?>
        </div>
    </div>

    <div class="slider movil">
        <div class="slides cycle-slideshow" data-cycle-fx=fade data-cycle-timeout=4000 data-cycle-slides="> a" data-cycle-prev="#prev" data-cycle-next="#next" data-cycle-pager=".movilSlider">
            <?php foreach($slider as $sl){ ?>
                <a <?php if($sl[0]) echo 'href="'.$sl[0].'"'?> class="slide">
                    <img src="<?= imagen("imagenes/slider/", $sl[3]); ?>" width="100%">
                    <div class="cycle-overlay5"><?= $sl[4];?></div>
                    <div class="cycle-overlay6"><?= $sl[5];?></div>
                    <div class="cycle-overlaySub"><?= $sl[6];?></div>
                    <div class="movilSlider"></div>
                </a>
            <?php } ?>
        </div>
    </div>
</div><!--FIn cont100 -->

<!-- Banner1 -->
<?php 
    $banners1 = consulta_bd("titulo, link, banner1", "banner_home1", "publicado = 1", "id asc");
    if(count($banners1) > 0){
?>
<div class="cont100">  
    <div class="contServiciosSlider">
        <?php for($i=0; $i<sizeof($banners1); $i++){ ?>
        <a href="<?= $banners1[$i][1]; ?>" class="serviciosSlider">
            <div class="imgServiciosSlider">
                <img src="imagenes/banner_home1/<?= $banners1[$i][2]; ?>" alt="">
            </div>
            <div class="textServiciosSlider">
                <h6><?= $banners1[$i][0]; ?></h6>
            </div>
        </a>
        <?php } ?>      
    </div>
</div>
<?php } ?>
<!--Fin de Banner1-->

<!-- Banner2 -->
<?php 
    $banners2 = consulta_bd("banner1, link1, banner2, link2", "banner_home2", "publicado = 1", "id asc");
    if(count($banners2) > 0){
?>
<div class="cont100">  
    <div class="contBanner2">
        <a href="<?= $banners2[0][1]; ?>" class="bannerHome2">
            <img src="imagenes/banner_home2/<?= $banners2[0][0]; ?>" class="imagenBanner2-1">
        </a>
        <a href="<?= $banners2[0][3]; ?>" class="bannerHome2">
            <img src="imagenes/banner_home2/<?= $banners2[0][2]; ?>" class="imagenBanner2-2">
        </a>
    </div>
</div>
<?php } ?>

<!--Fin de Banner2-->

<!-- Carrusel de productos 1 -->
<?php    
$prd = consulta_bd("titulo, productos", "destacados_home", "id = 1", "");
$cantPRD = mysqli_affected_rows($conexion);
if($cantPRD > 0){
    $skuPRD = trim(strip_tags($prd[0][1]));
    $skuSeparados = explode(",", $skuPRD);
    
    $skuPD = "";
    $x = 0;
    foreach($skuSeparados as $key) {    
        if($x == 0){
            $skuPD .= "'".trim($skuSeparados[$x])."'";
        } else {
            if(trim($skuSeparados[$x] != "")){
                $skuPD .= ",'".trim($skuSeparados[$x])."'";
                
            }
        }
        $x++;
    }
    
}

if(count($skuSeparados) > 0){
    $productos = consulta_bd("distinct(p.id), p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id, p.descripcion_breve, pd.stock, pd.stock_reserva", "productos p, productos_detalles pd", "p.publicado = 1 and p.id = pd.producto_id and pd.sku IN ($skuPD) group by p.id", "FIELD(pd.sku, $skuPD)");
    $cantPRD = mysqli_affected_rows($conexion);
}
if($cantPRD > 0){
?>
<div class="cont100">   
	<div class="cont100Centro grillasHome">
        <h3 class="tituloDestacadoHome"><?= $prd[0][0]; ?></h3>
            <div class="owl-carousel owl-theme owl-carouselProductos">
            <?php for($i=0; $i<sizeof($productos); $i++){ ?>
                <div class="item">
                    <div class="grilla">
                        <a href="#" class="like" rel="<?= $productos[$i][0]; ?>">
                        <i class="<?= (guardadoParaDespues($productos[$i][0])) ? 'fas':'far';?> fa-heart"></i>
                        </a>
                        <?php if(($productos[$i][8] - $productos[$i][9]) <= 0){?> 
                        <div class="cintaSinStockGrilla">
                            <span class="textoCintaSinStock">Este producto se encuentra sin stock</span>
                        </div>
                        <?php } ?>

                        <?= porcentajeDescuento($productos[$i][0]); ?>
                        
                        <?php if(ultimasUnidades($productos[$i][6])){ ?>
                        <div class="etq-ultimas">ultimas unidades</div>
                        <?php } ?>
                        

                        <?php if ($is_cyber AND is_cyber_product($productos[$i][0])): ?>
                            <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                        <?php endif ?>

                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="imgGrilla">
                            <?php
                                $imagenCarrucel1 = "sin-imagen.webp";
                                if ($productos[$i][2] != "" && $productos[$i][2] != null) {
                                    $imagenCarrucel1 = $productos[$i][2];
                                }
                            ?>
                            <img src="<?= imagen("imagenes/productos/", $imagenCarrucel1);?>" width="100%">
                        </a>
                        <a class="btnFicha" href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>"><span class="botonDetalle">Ver Ficha</span></a>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="valorGrilla">
                        <?php if ($is_cyber AND is_cyber_product($productos[$i][0])):
                            $precios = get_cyber_price($productos[$i][6]); ?>
                                <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                                <span class="antes">$<?= number_format($precios['precio'],0,",",".") ?></span>
                            
                        <?php else: ?>
                            <?php if(tieneDescuento($productos[$i][6])){ ?>
                                <span class='ahorroValor'><?= ahorras($productos[$i][6]); ?></span> 
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>    
                                <span class="antes">$<?= number_format(getPrecioNormal($productos[$i][6]),0,",",".") ?></span>
                                
                            <?php }else{ ?>
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                                <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                
                            <?php } ?>
                        <?php endif ?>
                        </a>                        
                        <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="nombreGrilla"><?= $productos[$i][1]; ?></a>
                        <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="descripcionGrilla"><?= strip_tags(preview($productos[$i][7], 130)); ?></a>
                    </div><!--Fin Grilla -->
                </div><!--Fin item -->
            <?php } ?>
            </div><!--Fin carousel -->    
    </div>
</div>
<?php } ?>
<!--Fin de Carrusel de productos 1 -->

<!-- Banner3 -->
<?php 
    $banners3 = consulta_bd("nombre,titulo_banner1,banner1, link1,titulo_banner2,banner2, link2,titulo_banner3,banner3, link3 ,titulo_banner4,banner4, link4,titulo_banner5,banner5, link5,titulo_banner6,banner6, link6,titulo_banner7,banner7, link7", "banner_home3", "publicado = 1", "id asc");
    if(count($banners3) > 0){
?>
<div class="cont100">
    <section class="contBanner3">
        <h3 class="tituloDestacadoHome tituloBanner3"><?= $banners3[0][0]; ?></h3>
        <a href="<?= $banners3[0][3]; ?>" class="banner3-1">
            <img src="imagenes/banner_home3/<?=$banners3[0][2]?>" alt="<?= $banners3[0][1]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][1]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][6]; ?>" class="banner3-2">
            <img src="imagenes/banner_home3/<?=$banners3[0][5]?>" alt="<?= $banners3[0][4]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][4]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][9]; ?>" class="banner3-3">
            <img src="imagenes/banner_home3/<?=$banners3[0][8]?>" alt="<?= $banners3[0][7]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][7]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][12]; ?>" class="banner3-4">
            <img src="imagenes/banner_home3/<?=$banners3[0][11]?>" alt="<?= $banners3[0][10]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][10]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][15]; ?>" class="banner3-5">
            <img src="imagenes/banner_home3/<?=$banners3[0][14]?>" alt="<?= $banners3[0][13]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][13]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][18]; ?>" class="banner3-6">
            <img src="imagenes/banner_home3/<?=$banners3[0][17]?>" alt="<?= $banners3[0][16]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][16]; ?></span></h6>
        </a>
        <a href="<?= $banners3[0][21]; ?>" class="banner3-7">
            <img src="imagenes/banner_home3/<?=$banners3[0][20]?>" alt="<?= $banners3[0][19]; ?>">
            <h6 class="tituloBanner3Img"><span><?= $banners3[0][19]; ?></span></h6>
        </a>
    </section>
</div>
<?php } ?>
<!--Fin de Banner3-->


<!-- Carrusel de productos 2 -->
<?php    
$prd2 = consulta_bd("titulo, productos", "destacados_home", "id = 2", "");
$cantPRD2 = mysqli_affected_rows($conexion);
if($cantPRD2 > 0){
    $skuPRD2 = trim(strip_tags($prd2[0][1]));
    $skuSeparados2 = explode(",", $skuPRD2);
    
    $skuPD2 = "";
    $x = 0;
    foreach($skuSeparados2 as $key) {    
        if($x == 0){
            $skuPD2 .= "'".trim($skuSeparados2[$x])."'";
        } else {
            if(trim($skuSeparados2[$x] != "")){
                $skuPD2 .= ",'".trim($skuSeparados2[$x])."'";
            }
        }
        $x++;
    }
    
}

if(count($skuSeparados2) > 0){
    $productos2 = consulta_bd("distinct(p.id), p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id, pd.sku", "productos p, productos_detalles pd", "p.publicado = 1 and p.id = pd.producto_id and pd.sku IN ($skuPD2) group by p.id", "FIELD(pd.sku, $skuPD2)");
    $cantPRD2 = mysqli_affected_rows($conexion);
}
if($cantPRD2 > 0){
?>
<div class="cont100">   
	<div class="cont100Centro grillasHome">
        <h3 class="tituloDestacadoHome"><?= $prd2[0][0]; ?></h3>
            <div class="owl-carousel owl-theme owl-carouselProductos">
            <?php for($i=0; $i<sizeof($productos2); $i++){ ?>
                <div class="item">
                    <div class="grilla">
                        <a href="#" class="like" rel="<?= $productos2[$i][0]; ?>">
                        <i class="<?= (guardadoParaDespues($productos2[$i][0])) ? 'fas':'far';?> fa-heart"></i>
                        </a>
                        <?php if(($productos2[$i][8] - $productos2[$i][9]) <= 0){?> 
                        <div class="cintaSinStockGrilla">
                            <span class="textoCintaSinStock">Este producto se encuentra sin stock</span>
                        </div>
                        <?php } ?>

                        <?= porcentajeDescuento($productos2[$i][0]); ?>
                        
                        <?php if(ultimasUnidades($productos2[$i][6])){ ?>
                        <div class="etq-ultimas">ultimas unidades</div>
                        <?php } ?>
                        

                        <?php if ($is_cyber AND is_cyber_product($productos2[$i][0])): ?>
                            <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                        <?php endif ?>

                        <a href="ficha/<?= $productos2[$i][0]; ?>/<?= url_amigables($productos2[$i][1]); ?>" class="imgGrilla">
                            <?php
                                $imagenCarrucel2 = "sin-imagen.webp";
                                if ($productos2[$i][2] != "" && $productos2[$i][2] != null) {
                                    $imagenCarrucel2 = $productos2[$i][2];
                                }
                            ?>
                            <img src="<?= imagen("imagenes/productos/", $imagenCarrucel2);?>" width="100%">
                        </a>
                        <a class="btnFicha" href="ficha/<?= $productos2[$i][0]."/".url_amigables($productos2[$i][1]); ?>"><span class="botonDetalle">Ver Ficha</span></a>
                        <a href="ficha/<?= $productos2[$i][0]; ?>/<?= url_amigables($productos2[$i][1]); ?>" class="valorGrilla">
                        <?php if ($is_cyber AND is_cyber_product($productos2[$i][0])):
                            $precios = get_cyber_price($productos2[$i][6]); ?>
                                <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                                <span class="antes">$<?= number_format($precios['precio'],0,",",".") ?></span>
                            
                        <?php else: ?>
                            <?php if(tieneDescuento($productos2[$i][6])){ ?>
                                <span class='ahorroValor'><?= ahorras($productos2[$i][6]); ?></span> 
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos2[$i][6]),0,",",".") ?></span>    
                                <span class="antes">$<?= number_format(getPrecioNormal($productos2[$i][6]),0,",",".") ?></span>
                                
                            <?php }else{ ?>
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos2[$i][6]),0,",",".") ?></span>
                                <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                
                            <?php } ?>
                        <?php endif ?>
                        </a>                        
                        <a href="ficha/<?= $productos2[$i][0]."/".url_amigables($productos2[$i][1]); ?>" class="nombreGrilla"><?= $productos2[$i][1]; ?></a>
                        <a href="ficha/<?= $productos2[$i][0]."/".url_amigables($productos2[$i][1]); ?>" class="descripcionGrilla"><?= strip_tags(preview($productos2[$i][7], 130)); ?></a>
                    </div><!--Fin Grilla -->
                </div><!--Fin item -->
            <?php } ?>
            </div><!--Fin carousel -->    
    </div>
</div>
<?php } ?>
<!--Fin de Carrusel de productos 2 -->
