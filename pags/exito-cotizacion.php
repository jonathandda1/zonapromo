<script>
    <?php
    if (opciones("cotizador") != 1) {
        echo 'location.href = "404"';
    }
        //conf
        include('admin/conf.php');
        // Include functions
        require_once('admin/includes/tienda/cart/inc/functions.inc.php');
        $oc = (isset($_GET['oc'])) ? mysqli_real_escape_string($conexion, $_GET['oc']) : 0;
        $datosCotizacion = consulta_bd("c.nombre, c.apellido, .c.telefono, c.email","cotizaciones c","c.oc= '$oc'","");
    ?>
</script>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
            <li><a href="home">Home</a></li>
            <li class="active">Cotizaciona exitosa</li>
        </ul>
    </div>
</div>
<!--Fin breadcrumbs -->


<div class="cont100">
    <section class="cont100Centro">
        <article class="contCotExito">

            <h2 class="titulosCotExito">Tu cotización fue enviada con éxito</h2>
            <div class="mensajeCotExito">
                <p>¡Muchas gracias por cotizar en ZonaPromo!</p>
                <p>Enviaremos a tu email la confirmación de la cotización</p>
                <p>Su número de orden es: <span><?= $oc ?></span></p>
            </div>
            <div class="contBotonCot">
                <a class="btnCotExito" href="home">Volver al sitio</a>
            </div>
            <div class="infoCotExito">
                <h2>Información personal</h2>
                <p>Nombre: <?= $datosCotizacion[0][0]." ".$datosCotizacion[0][1]  ?></p>
                <p>Teléfono: <?= $datosCotizacion[0][2]  ?></p>
                <p>Email: <?= $datosCotizacion[0][3]  ?></p>
            </div>

        </article>
    </section>
    <!--fin cont100Centro-->
</div>