<?php 
require_once 'paginador/paginator.class.php';
    $page = (isset($_GET[page])) ? mysqli_real_escape_string($conexion, $_GET[page]) : 0;
	$ipp = (isset($_GET[ipp])) ? mysqli_real_escape_string($conexion, $_GET[ipp]) : 4;

    $destacada = consulta_bd("e.titulo, e.portada, e.bajada, a.nombre, a.thumbs, e.id", "entradas e, autores a", "e.autor_id = a.id and e.publicada = 1 and e.destacada = 1", "e.id desc LIMIT 1");


    $categorias = consulta_bd("id, nombre","catblog","publicada = 1"," nombre asc");




    $UltimasEnt = consulta_bd("e.titulo, e.portada, e.bajada, a.nombre, a.thumbs, e.id", "entradas e, autores a", "e.autor_id = a.id and e.publicada = 1", "e.fecha_creacion desc LIMIT 3");


    $ent = consulta_bd("e.titulo, e.portada, e.bajada, a.nombre, a.thumbs, e.id", "entradas e, autores a", "e.autor_id = a.id and e.publicada = 1", "e.fecha_creacion desc");
    $total = mysqli_affected_rows($conexion);

    $pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 6; 
	$rutaRetorno = "blog";
    $pages->paginate($rutaRetorno);


    $entradas = consulta_bd("e.titulo, e.portada, e.bajada, a.nombre, a.thumbs, e.id", "entradas e, autores a", "e.autor_id = a.id and e.publicada = 1", "e.fecha_creacion desc $pages->limit");

    
?> 

<div class="cont100">
    <div class="cont100Centro">
        <?php 
         $autores = consulta_bd("titulo, nombre, thumbs, banner, descripcion", "autores", "destacado = 1", "id desc");
        if(count($autores) > 0){
        ?> 
         <div class="contFondoBlogger" style="background-image:url(imagenes/autores/<?= $autores[0][3]; ?>);">
            <h3><?= $autores[0][1]; ?></h3>
            <div class="decripcionBlogger"><?= $autores[0][4]; ?></div>
        </div>
       <?php } ?>
        
        
        <div class="cont100 contMenuBlog">
            <?php for($i=0; $i<sizeof($categorias); $i++) { ?> 
            <a href="categorias_blog/<?= $categorias[$i][0];?>/<?= url_amigables($categorias[$i][1]);?>"><?= $categorias[$i][1];?></a>
            <?php } ?> 
        </div>
            
        
        <div class="fila1Blog cont100">
            <div class="grillaDestacadaBlog">
                <div class="imgGrillaDestacada">
                    <img src="imagenes/entradas/<?= $destacada[0][1]; ?>" width="100%">
                </div>
                <div class="infoGrillaDestacado">
                    <div class="headEntrada">
                        <div class="thumbsAutor" style="background-image: url(imagenes/autores/<?= $destacada[0][4]; ?>);"></div>
                        <div class="tituloEntradaGrilla"><?= $destacada[0][0]; ?></div>
                    </div>
                    
                    <div class="contDatosGrillaEntrada">
                        <div class="bajadaEntradaGrilla">
                            <?= $destacada[0][2]; ?>
                        </div>

                        <a href="entradas/<?= $destacada[0][5]; ?>/<?= url_amigables($destacada[0][0]); ?>" class="btnGrillaBlog">ver artículo</a>
                    </div>
                </div>
            </div><!--fin grillaDestacadaBlog -->
            
            <div class="columnaUltimasEntradas">
                <h3>Últimas entradas</h3>
                <?php for($i=0; $i<sizeof($UltimasEnt); $i++) { ?> 
                <div class="grillaUltimasEntradas">
                    <div class="imgUltimasEntradas">
                        <img src="imagenes/entradas/<?= $UltimasEnt[$i][1]; ?>" width="100%">
                    </div>
                    <div class="contTinfoUltimasEntradas">
                        <div class="tituloUltimasEntradas"><?= $UltimasEnt[$i][0]; ?></div>
                        <a href="entradas/<?= $UltimasEnt[$i][5]; ?>/<?= url_amigables($UltimasEnt[$i][0]); ?>" class="btnGrillaBlog">ver artículo</a>
                    </div><!--fin contTinfoUltimasEntradas-->
                </div>
                <?php } ?> 
            </div><!--fin columnaUltimasEntradas -->
        </div><!--fila1Blog-->
        
        
        
        <div class="cont100 contGrillasBlog">
            <?php for($i=0; $i<sizeof($entradas); $i++) { ?> 
                <div class="grillaEntrada">
                    <div class="headEntrada">
                        <div class="thumbsAutor" style="background-image: url(imagenes/autores/<?= $entradas[$i][4]; ?>);"></div>
                        <div class="tituloEntradaGrilla"><?= $entradas[$i][0]; ?></div>
                    </div>
                    <div class="imgPortadaGrilla">
                        <img src="imagenes/entradas/<?= $entradas[$i][1]; ?>" width="100%">
                    </div>
                    <div class="contDatosGrillaEntrada">
                        <div class="bajadaEntradaGrilla">
                            <?= $entradas[$i][2]; ?>
                        </div>

                        <a href="entradas/<?= $entradas[$i][5]; ?>/<?= url_amigables($entradas[$i][0]); ?>" class="btnGrillaBlog">ver artículo</a>
                    </div>
                </div>
            <?php } ?> 
        </div><!--fin contGrillasBlog -->
        
        
        <div class="cont100">
            <div class="cont100Centro paginador">
                <?= $pages->display_pages(); ?>
            </div>
        </div><!--Fin paginador -->
        
    </div>
</div>
