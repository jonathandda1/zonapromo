<script>
<?php 
	if(opciones("cotizador") != 1){
		echo 'location.href = "404"';
	}
?>
</script>

<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Carro de cotización</li>
        </ul>
    </div>
</div>


<div class="cont100">
    <form method="post" action="tienda/procesarCotizacion.php" id="formCotizacion">
        <section class="contCarroCotizar">     
            <article class="contCarroCotizacionFinal">
                <?= ShowCartCotizar(); ?>
            </article><!-- fin contCarro-->
            
            <article class="contTotalesCotizacion">
                <div class="tituloColumnas"> <h3>Detalle de cotización</h3></div>
                <?php 
                if(isset($_COOKIE['usuario_id'])){
                    $usuario_id = $_COOKIE['usuario_id'];
                    $usuario = consulta_bd("nombre, apellido, telefono, email, rut,nombre_empresa, rut_empresa","clientes","id = $usuario_id","");
                    $nombre = $usuario[0][0];
                    $apellidos = $usuario[0][1];
                    $telefono = $usuario[0][2];
                    $email = $usuario[0][3];
                    $rut = $usuario[0][4];
                    $empresa = $usuario[0][5];
                    $rutEmpresa = $usuario[0][6];
                    
                    } else {
                        $usuario_id = 0;
                        $nombre = "";
                        $apellidos = "";
                        $telefono = "";
                        $email = "";
                        $rut = "";
                        $empresa = "";
                        $rutEmpresa = 0;
                    }
                ?>
                    <!--<label for="nombre">Nombre</label>-->
                    <!-- <input type="text" id="nombre" name="nombre" class="campoGrande2 label_better" value="" placeholder="Nombre" data-new-placeholder="Nombre"/> -->
                    <input class="inputCotizar label_better" type="text" name="nombreCotizar" value="<?= $nombre; ?>" id="nombreCotizar" placeholder="Nombre" data-new-placeholder="Nombre"/>
                    
                    <!--<label for="apellido">Apellidos</label>-->
                    <input class="inputCotizar label_better" type="text" name="apellidoCotizar" value="<?= $apellidos; ?>" id="apellidoCotizar" placeholder="Apellidos" data-new-placeholder="Apellidos" />

                    <!--<label for="apellido">Apellidos</label>-->
                    <input class="inputCotizar label_better" type="text" name="rutCotizar" value="<?= $rut; ?>" id="rutCotizar" placeholder="Rut" data-new-placeholder="Rut" />
                    
                    <!--<label for="telefono">Teléfono</label>-->
                    <input class="inputCotizar label_better" type="text" name="telefonoCotizar" value="<?= $telefono; ?>" id="telefonoCotizar" placeholder="Teléfono" data-new-placeholder="Teléfono" />
                    
                    <!--<label for="email">Email</label>-->
                    <input class="inputCotizar label_better" type="text" name="emailCotizar" value="<?= $email; ?>" id="emailCotizar" placeholder="Email" data-new-placeholder="E-mail" />
                    
                    <!--<label for="rut">Rut</label>-->
                    <div style="position: relative;">
                        <input class="inputCotizar label_better" type="text" name="empresaCotizar" value="<?= $empresa; ?>" id="empresaCotizar" placeholder="Empresa" data-new-placeholder="Empresa" />
                        <div class="lb_wrap" id="empresasList" style="position: relative;">
                        </div>
                    </div>
                    <!--<label for="apellido">Apellidos</label>-->
                    <input class="inputCotizar" type="hidden" name="rutEmpresaCotizar" value="<?= $rutEmpresa; ?>" id="rutEmpresaCotizar" placeholder="Rut Empresa" data-new-placeholder="Rut Empresa" />
                    
                    <!--<label for="comentarios">Comentarios</label>-->
                    <textarea class="textareaPasoFinal label_better"  name="comentariosCotizar" placeholder="Mensaje" data-new-placeholder="Mensaje"></textarea>
                    <?php if(qty_cotizar() > 0){ ?>
                    <a href="javascript:void(0)" class="btnTerminarCarro" id="btnCotizacion">Enviar cotización</a>
                    <?php } ?>            
            </article><!-- Fin contTotales-->
        
	    </section>
    </form>
</div>

            
            

<?php /*?><script type="text/javascript">
<?php
    
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}  
	$cart = $_COOKIE[cart_alfa_cm];  
    $items = explode(',',$cart);
    foreach ($items as $item) {
        $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
    }
    
    foreach ($contents as $prd_id=>$qty) {
        $filas = consulta_bd("pd.id, pd.sku, pd.nombre, m.nombre, pd.precio, pd.descuento","productos p, productos_detalles pd, marcas m","p.id = pd.producto_id and pd.id = ".$prd_id." and p.marca_id = m.id","");
	
		$idProductoInterior = $filas[0][0];
		if($filas[0][5] != '' and $filas[0][5] > 0){
			$valor = $filas[0][5];
		} else {
			$valor = $filas[0][4];
		}
        ?>
        
            ga('ec:addProduct', {
              'id': '<?php echo $filas[0][0]; ?>',
              'name': '<?php echo $filas[0][2]; ?>',
              'brand': '<?php echo $filas[0][3]; ?>',
              'price': '<?php echo $valor; ?>',
              'quantity': <?php echo $qty; ?>
            });
			console.log("<?php echo $filas[0][0]; ?>,<?php echo $filas[0][2]; ?>,<?php echo $filas[0][3]; ?>,<?php echo $valor; ?>,<?php echo $qty; ?>");
        
        <?php
    }
?>
</script>
<script type="text/javascript">
	ga('ec:setAction','checkout', {'step': 1});
</script>
<?php */?>
