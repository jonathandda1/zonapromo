
 <div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Despachos</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->



<div class="cont100">
    <div class="contCentroPagFijas">
        <div class="tituloContacto">Despachos</div>
        <div class="contTxtParrafo">
            
            <p>Toda entrega será efectuada de Lunes a Viernes en horarios de 9:30 a 18:00 horas en la dirección indicada al momento de realizar la compra, y en un plazo de 4 días desde haber hecho efectiva la compra.</p>

            <p>Por compras inferiores a $100.000 pesos usted podrá retirar en tienda sin hacer fila, directamente en empaque dando el número de su boleta o factura al encargado dependiente. Los despachos a Regiones y Provincias serán a cargo del cliente y por regla general lo haremos a través de Transportes STARKEN a OFICINAS de este mismo. En caso de que necesite aportar un transporte distinto por favor ingresarlo como comentario a la hora de realizar su compra, de lo contrario de pleno efecto haremos efectivo el envío por el transporte antes individualizado.</p>

            <p>En caso de tener alguna complicación con su envío, favor escribir a nuestro correo electrónico portal@zonapromo.cl</p>
            <p></p>

            
            
            
        </div>
    </div>
</div>