<?php 
	include("../admin/conf.php");
	include("../funciones.php");
	require_once('../admin/includes/tienda/cart/inc/functions.inc.php');
?>
<div class="fondoPopUp">
    <div class="contPop">
        <a href="javascript:cerrar()" class="cerrar"><i class="material-icons">close</i></a>
        <div id="contShowCartPopUp">
        <?= ShowCartPopUp();?>
        </div>
        
    </div>

</div>
<?php mysqli_close($conexion);?>
<script>
    $(function(){
       $(".fondoPopUp").click(function(){
           $(".fondoPopUp").remove();
           return false;
       }); 
        
       $(".fondoPopUp .contPop").click(function(event){
           event.stopPropagation();
       });
    });
</script>