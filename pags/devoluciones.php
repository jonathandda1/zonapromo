<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Devoluciones</li>
        </ul>
    </div>
</div>





<div class="cont100 fondoServicio">
    <div class="cont100Centro contCentrarFlex">
        <article class="contNosotros">
            <div class="tituloContacto">Devoluciones</div>
            <div class="contTextosFijos">
                <h4></h4>
                <p>Zona Promo mantendrá siempre tus datos aislados de terceras empresas o personas que puedan hacer usos de esta información para otros fines. Se garantiza confidencialidad, privacidad y seguridad en la manipulación y uso de cualquier información personal de los clientes inscritos en el sitio web.</p>
                <p></p>
                <p>Actualmente en todo el mundo, la manera más eficaz de resguardar la seguridad de las compras en línea es vía certificados SSL. Estos certificados son los encargados de cifrar o codificar los datos de las transacciones, impidiendo que estas sean intervenidas y descifradas por terceros. Un certificado SSL presente y activo en un sitio web, entrega a los compradores la tranquilidad necesaria para saber que sus datos no serán obtenidos por una tercera persona.</p>
                <p></p>
                <p>Zona Promo velará energéticamente por el uso, la manutención y mejora de los estándares de seguridad y privacidad hacia sus clientes registrados en el sitio web. Se mantendrán los estándares requeridos, tanto en infraestructura, herramientas y dotación para cumplir con este compromiso de resguardo de toda información otorgada en los formularios de registros.</p>
                <p></p>
                <p>Las informaciones requeridas en el formulario de registro tiene por finalidad el identificar a los clientes, mantenerlos informados, solucionar reclamos, revisar pedidos, consultar por despachos o recibir consultas y sugerencias. Esto permite tener un contacto más cercano, rápido y detallado que ayuda a un accionar mas rápido y eficiente en cualquiera de las situaciones.</p>
                <p></p>
                
            </div>
        </article>
    </div>
</div>