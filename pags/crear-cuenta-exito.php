<?php 
	$id = (isset($_POST[cliente_id])) ? mysqli_real_escape_string($conexion, $_POST[cliente_id]) : 0;
	$cliente = consulta_bd("id, nombre, apellido, rut, telefono, email","clientes","id = $id and clave is NULL",""); 
	$cantEmail = mysqli_affected_rows($conexion);
	
	if($cantEmail < 1){ 
?>
<script type="text/javascript">
	location.href ="404";
</script>
<?php } ?>
<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Crear Cuenta</li>
        </ul>
    </div>
</div>



<div class="cont100 marginBotton">
    <div class="cont100Centro identificacionCentrado">
        <div class="acceso1 titulo2">Crear cuenta</div>
        <div class="acceso2 subtitulo2">puedes crear una cuenta con nosotros para una futura compra, revisar el estado de tus despachos y revisar tus boletas</div>
        
        
        
        <form action="ajax/registroDesdeExito.php" method="post" class="formularioLogin bl_form" id="formularioRegistro">
            <div class="cont50Identificacion">
                <div class="acceso3 cont100">
                   <input type="text" id="nombre" name="nombre" class="campoGrande2 label_better" value="<?= $cliente[0][1]; ?>" placeholder="Nombre" data-new-placeholder="Nombre" />
                   <input type="text" id="rut" name="rut" class="campoGrande2 label_better" value="<?= $cliente[0][3]; ?>" placeholder="Rut" data-new-placeholder="Rut" />
                   <input type="password" id="clave" name="clave" class="campoGrande2 label_better" placeholder="Ingrese su clave" data-new-placeholder="Clave" />
                   <div style="clear:both"></div>
               </div>
            </div><!--iniciar sesion -->
            
            <div class="cont50Identificacion">
                <div class="acceso3 cont100">
                   <input type="text" id="telefono" name="telefono" class="campoGrande2 label_better" value="<?= $cliente[0][4]; ?>" placeholder="Teléfono" data-new-placeholder="Teléfono" />
                   <input type="text" id="email" name="email" class="campoGrande2 label_better" value="<?= $cliente[0][5]; ?>" placeholder="Email" data-new-placeholder="Email" />
                   <input type="password" id="clave2" name="clave2" class="campoGrande2 label_better" placeholder="Repita su clave" data-new-placeholder="Repetir clave" />
                   <div style="clear:both"></div>
               </div>
            </div><!--iniciar sesion -->
            <a href="javascript:void(0)" id="btnRegistro" class="btnFormRegistro">Registrarse</a>
        </form>
        
               
    </div>
</div>