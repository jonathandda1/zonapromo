<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Registro</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        <div class="acceso1 titulo2">REGÍSTRATE</div>
        <div class="acceso2 subtitulo2">Tu cuenta para todo lo relacionado con Zona Promo</div>
        <form action="ajax/registro.php" method="post" class="formularioLogin bl_form" id="formularioRegistro">
            
            <div class="cont100" style="display:none;">
                <div class="contRadios">
                    <div class="radioRegistro">
                        <input type="radio" name="tipo_registro" value="persona" id="radioPersona" checked="checked"> 
                        <label>Persona natural</label>
                    </div>
                    <div class="radioRegistro">
                        <input type="radio" name="tipo_registro" value="empresa" id="radioEmpresa"> 
                        <label>Empresa</label>
                    </div>
                </div>
                
            </div>
            <div class="cont50Identificacion">
                <div class="acceso3 cont100">
                   <input type="text" id="nombre" name="nombre" class="campoGrande2 label_better" placeholder="Ingrese su Nombre" data-new-placeholder="Nombre" />
                   <input type="text" id="rut" name="rut" class="campoGrande2 label_better" placeholder="Ingrese su Rut" data-new-placeholder="Rut"/>
                    
                    <div class="camposOcultos">
                        <input type="text" id="codigo_empresa" name="codigo_empresa" class="campoGrande2 label_better" placeholder="Ingrese Código de empresa" data-new-placeholder="Código empresa"/>
                    </div>
                    <!--<div class="camposOcultos">
                        <input type="text" id="Rut_empresa" name="rut_empresa" class="campoGrande2 label_better" placeholder="Ingrese Rut de empresa" data-new-placeholder="Rut Empresa"/>
                    </div>-->
                     
                    <input type="password" id="clave" name="clave" class="campoGrande2 label_better" placeholder="Ingrese su clave" data-new-placeholder="Clave" />
                   <div style="clear:both"></div>
               </div>
            </div><!--iniciar sesion -->
            
            <div class="cont50Identificacion">
                <div class="acceso3 cont100">
                   <!--<input type="text" id="apellido" name="apellido" class="campoGrande2 label_better" placeholder="Ingrese su Apellido" data-new-placeholder="Apellido" />-->
                    <input type="text" id="telefono" name="telefono" class="campoGrande2 label_better" placeholder="Ingrese su Teléfono" data-new-placeholder="Teléfono" minlength="9" maxlength="11" />
                   <input type="text" id="email" name="email" class="campoGrande2 label_better" placeholder="Ingrese su Email" data-new-placeholder="Email" />
                    
                    <div class="camposOcultos">
                        <input type="text" id="nombre_empresa" name="nombre_empresa" class="campoGrande2 label_better" placeholder="Ingrese nombre de empresa" data-new-placeholder="Nombre Empresa"/>
                    </div>
                    
                    
                   <input type="password" id="clave2" name="clave2" class="campoGrande2 label_better" placeholder="Repita su clave" data-new-placeholder="Repita su clave"/>
                   <div style="clear:both"></div>
               </div>
            </div><!--iniciar sesion -->            
            <a href="javascript:void(0)" id="btnRegistro" class="btnFormRegistro">Registrarse</a>
            
            <p>Al registrarse, aceptas la <a href="politicas-de-privacidad" class="btnParrafo">Política de privacidad</a> <br> y los <a href="terminos-y-condiciones" class="btnParrafo">términos de uso</a> de Zona Promo</p>
            <p>¿Ya estas registrado?  <a href="inicio-sesion" class="btnParrafo">Inicia sesión</a></p>
        </form>
        
               
    </div>
</div>
