<div class="menuVendedores">
    <div class="primeraColMenu">
        <h4>Mi cuenta</h4>
        <h5><?= $vendedorEmail;?></h5>
    </div>
    <a class="<?php if($op == "mi-cuenta-vendedores"){echo "current";}?>" href="mi-cuenta-vendedores">Menú principal</a>   
    <a class="<?php if($op == "datos-personales-vendedores"){echo "current";}?>" href="datos-personales-vendedores">Datos Vendedora</a>
    <a class="disableSinEmpresa <?php if($op == "clientes-vendedor"){echo "current";}?>" href="clientes-vendedor">Clientes</a>
    <a class="disableSinEmpresa <?php if($op == "cotizaciones-vendedor"){echo "current";}?>" href="cotizaciones-vendedor">Cotizaciones</a>
    <a href="ajax/cerrar-sesion-vendedor.php">Cerrar sesión</a>
</div>