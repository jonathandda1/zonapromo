
 <div class="cont100 contBreadCrumbs" style="margin-bottom:0;">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Error 404</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->



<div class="cont100">
    <section class="cont100Centro centro404">
        <article class="">
            <img src="img/404.jpg">
        </article>
        <article class="">
            <p><b>NO PODEMOS ENCONTRAR LA PÁGINA QUE ESTAS BUSCANDO.</b></p>
        </article>
        <article class="">
            <p>Te recomendamos intentar nuevamente o volver al Home de nuestro sitio.</p>
        </article>
        <article class="">
            <!-- Buscador -->
            <div class="contBuscador404">
                <div class="buscador buscadorEscritorio">
                    <form id="formBuscador" method="get" action="busquedas" style="float:none;">
                        <input type="text" name="busqueda" class="campo_buscador" placeholder="<?php if(isset($_GET['buscar'])){echo $_GET['buscar'];} else {echo 'Buscar productos';}?>">

                        <a href="javascript:void(0)" id="cerrarBuscadorHeader"><i class="material-icons">close</i></a>
                        <input class="btn_search" type="submit" value=" " name="b">

                    </form>
                </div>
            </div>
        </article>

        <div class="cont100 grillaNormal">   
            <div class="cont100Centro">
                <div class="cont100 ultimosVistos grillasFicha">
                    <?= vistosRecientemente("grilla", 4); ?>
                </div>
                <!--Productos relacionados -->
            </div><!--Fin centroDocumentos -->
        </div><!--FIn cont100 -->

        <div class="cont100 fondoGris grillaResponsive grillaResponsiveficha">   
            <div class="cont100Centro">
                <div class="cont100" style="background: #fff;">
                    <?= vistosRecientemente("lista", 4); ?>
                </div>
                <!--Productos relacionados -->
            </div><!--Fin centroDocumentos -->
        </div><!--FIn cont100 -->
    </section>
</div>


