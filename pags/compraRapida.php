<?php

if(qty_pro() == 0){
    echo '<script>parent.location = "home";</script>';
}

    $email = trim($_SESSION["correo"]);
	$clientes = consulta_bd("id, id, nombre, rut, telefono","clientes","email = '$email'","");
	$cant = mysqli_affected_rows($conexion);
	if($cant > 0){
        $nombre =  $clientes[0][2];
		$rut = $clientes[0][3];
		$telefono = $clientes[0][4];
		$idCliente = $clientes[0][0];
		
		$direccion = consulta_bd("id, region_id, comuna_id, calle, numero, localidad_id","clientes_direcciones","cliente_id = ".$clientes[0][0],"");
		$cantDirecciones = mysqli_affected_rows($conexion);
		if($cantDirecciones > 0){
			$region_id = $direccion[0][1];
			$comuna_id = $direccion[0][2];
			$localidad_id = $direccion[0][5];
			$calle = $direccion[0][3];
			$numero = $direccion[0][4];
		} else {
			$region_id = 0;
			$comuna_id = 0;
			$localidad_id = 0;
			$calle = '';
			$numero = '';
		}
	} else {
		$nombre =  "";
		$rut = "";
		$telefono = "";
		$region_id = 0;
		$comuna_id = 0;
		
?>
	<!--<script type="text/javascript">
    $(function(){
		$(".campoPasoFinal").removeAttr("disabled");
		$(".contRut").fadeIn(10);
		$(".tipoBlanco").removeClass("tipoBlanco");
	});
    </script>-->
    <?php
	}

?>
<?php 
    if(isset($_COOKIE['usuario_id'])){
         $maestro = consulta_bd("rut_empresa, giro, razon_social, direccion_facturacion, nombre_empresa","clientes","id = $usuarioID","");
            
            /*$dFact = consulta_bd("cd.nombre, cd.calle, c.nombre","clientes_direcciones cd, comunas c","c.id = cd.comuna_id and cliente_id = $usuarioID","");*/
            
            $rut_empresa = $maestro[0][0];
            $giro = $maestro[0][1];
            $razon_social = $maestro[0][2];
            //$direccion_facturacion = $maestro[0][3];
            $direccion_facturacion = $maestro[0][3];
            $nombre_empresa = $maestro[0][4];
    } else {
        $rut_empresa = "";
        $giro = "";
        $razon_social = "";
        $direccion_facturacion = "";
    }
    ?>

<?php 
if(!isset($_COOKIE['usuario_id']) || isset($_POST['carro-fracaso'])){

    if(isset($_POST[recuperar_datos])){
        $email = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : 0;
        $nombre = (isset($_POST['nombre'])) ? mysqli_real_escape_string($conexion, $_POST['nombre']) : 0;
        $telefono = (isset($_POST['telefono'])) ? mysqli_real_escape_string($conexion, $_POST['telefono']) : 0;

        $rut = (isset($_POST['rut'])) ? mysqli_real_escape_string($conexion, $_POST['rut']) : 0;
        
        $envio = (isset($_POST['envio'])) ? mysqli_real_escape_string($conexion, $_POST['envio']) : 0; 
        $region = (is_numeric($_POST['region'])) ? mysqli_real_escape_string($conexion, $_POST['region']) : 0;
        $comuna = (is_numeric($_POST['comuna'])) ? mysqli_real_escape_string($conexion, $_POST['comuna']) : 0;
        $direccion = (isset($_POST['direccion'])) ? mysqli_real_escape_string($conexion, $_POST['direccion']) : 0; 

        $factura = (isset($_POST['factura'])) ? mysqli_real_escape_string($conexion, $_POST['factura']) : 0;

        $idSucursal = (isset($_POST['idSucursal'])) ? mysqli_real_escape_string($conexion, $_POST['idSucursal']) : 0; 
        $nombre_retiro = (isset($_POST['nombre_retiro'])) ? mysqli_real_escape_string($conexion, $_POST['nombre_retiro']) : 0; 
        $telefono_retiro = (isset($_POST['telefono_retiro'])) ? mysqli_real_escape_string($conexion, $_POST['telefono_retiro']) : 0; 

        $rut_factura = (isset($_POST['rut_factura'])) ? mysqli_real_escape_string($conexion, $_POST['rut_factura']) : 0; 
        // echo $rut_factura;
        $giro_factura = (isset($_POST['giro_factura'])) ? mysqli_real_escape_string($conexion, $_POST['giro_factura']) : 0; 
        $razon_social = (isset($_POST['razon_social'])) ? mysqli_real_escape_string($conexion, $_POST['razon_social']) : 0; 
        $direccion_factura = (isset($_POST['direccion_factura'])) ? mysqli_real_escape_string($conexion, $_POST['direccion_factura']) : 0; 
        
        $comentarios_envio = (isset($_POST['comentarios_envio'])) ? mysqli_real_escape_string($conexion, $_POST['comentarios_envio']) : 0; 
        
    ?>

<form method="post" action="mi-carro" id="formMiCarro">
    <input type="hidden" name="recuperar_datos" value="1">    
    <input type="hidden" name="email" value="<?= $email; ?>"> 
    <input type="hidden" name="nombre" value="<?= $nombre; ?>"> 
    <input type="hidden" name="telefono" value="<?= $telefono; ?>"> 
    <input type="hidden" name="rut" value="<?= $rut; ?>"> 
    <input type="hidden" name="envio" value="<?= $envio; ?>"> <!--direccionCliente-->
    <input type="hidden" name="region" value="<?= $region; ?>">
    <input type="hidden" name="comuna" value="<?= $comuna; ?>"> 
    <input type="hidden" name="direccion" value="<?= $direccion; ?>"> 

    <input type="hidden" name="idSucursal" value="<?= $idSucursal; ?>"> <!--id de la sucursar de retiro-->
    <input type="hidden" name="nombre_retiro" value="<?= $nombre_retiro; ?>"> 
    <input type="hidden" name="telefono_retiro" value="<?= $telefono_retiro; ?>"> 

    <input type="hidden" name="factura" value="<?= $factura; ?>"> <!--si o no-->
    <input type="hidden" name="rut_factura" value="<?= $rut_factura; ?>"> 
    <input type="hidden" name="giro_factura" value="<?= $giro_factura; ?>"> 
    <input type="hidden" name="razon_social" value="<?= $razon_social; ?>"> 
    <input type="hidden" name="direccion_factura" value="<?= $direccion_factura; ?>">
    <input type="hidden" name="comentarios_envio" value="<?= $comentarios_envio; ?>">
</form>
    <script type="text/javascript">
        $( window ).on( "load", function() {
            
            // console.log("se ejecuto");
            envios = "<?= $envio; ?>";
            if (envios == "retiroEnTienda") {
                $("#tienda1").click();
                $("#nombre_retiro").val("<?= $nombre_retiro; ?>");
                $("#telefono_retiro").val("<?= $telefono_retiro; ?>");
            }
           $("#email").val("<?= $email; ?>");
           $("#btnValidarMail").trigger("click");
            
            $("#nombre").val("<?= $nombre; ?>");
            $("#telefono").val("<?= $telefono; ?>"); 
            $("#direccion").val("<?= $direccion; ?>");
            $("#rut").val("<?= $rut; ?>");
            $("#region").val("<?= $region; ?>").click();
            setTimeout(update, 1000);
            function update(){
                $("#comuna").val("<?= $comuna; ?>").click();
            }
            
            <?php if($factura == 'si'){ ?>
                $("#facturaSi").trigger("click");
                $("#facturaSi").click();
                $("#RutFactura").val("<?= $rut_factura; ?>"); 
                $("#razonSocial").val("<?= $razon_social; ?>"); 
                $("#giro").val("<?= $giro_factura; ?>"); 
                $("#direccionFactura").val("<?= $direccion_factura; ?>"); 
            <?php } ?>
             
            $(".btnTerminarCarro").fadeIn(100);
        });
    </script>
    <?php  }
}
?>
<?php if(!isset($_COOKIE['usuario_id'])){ ?>
<div id="inline1" class="inicioSesionCarroPopUp" style="width:400px;display: none;">
    <h3>Inicie Sesión</h3>
    <form method="post" id="formLoginCompraRapida" action="ajax/login.php">
        <input type="text" name="usuarioLogin" value="<?php echo $email ?>" class="inputInicioSesion" id="usuarioLogin" placeholder="Usuario" />
        <input type="password" name="passLogin" value="" id="passLogin" class="inputInicioSesion" placeholder="Ingrese Contraseña" />
        <input type="hidden" name="origen" value="carro">
        <a href="javascript:void(0)" id="btnIngresarCompraRapida">Ingresar</a>
    </form>
    <a href="javascript:void(0)" id="solicitarClave" rel="<?php echo $idCliente ?>">Solicitar una contraseña</a>
    
    <a href="registro" class="registroPopUp">REGÍSTRATE</a>
       
</div> 
<?php } ?>

<?php
    if($envio == "despachoDomicilio") {
        $retiroEnTienda = 0;
    } else {
        $retiroEnTienda = 1;
    }
    // echo "email:".$email."<br>nombre:".$nombre."<br>tele:".$telefono."<br>rut:".$rut."<br>envio:".$envio."<br>region:".$region."<br>comuna:".$comuna."<br>dire:".$direccion."<br>factura:".$factura."<br>idSucursa:".$idSucursal."<br>nombre_retiro:".$nombre_retiro."<br>tele_retiro:".$telefono_retiro."<br>rut_factura:".$rut_factura."<br>Giro:".$giro_factura."<br>razon_social:".$razon_social."<br>direccion_factura:".$direccion_factura."<br>comentarios:".$comentarios_envio;

    ?>





<form method="post" action="resumen" id="formCompra" class="bl_form">
    <!-- datosForm-->    
    <div class="cont100Centro contCompraRapida">
        <div class="carroDerechaMovil">
            <a class="filaCarroMovil">
                <i class="material-icons">local_mall</i> 
                <span class="textoCarroMovil">Mostrar resumen del pedido</span> 
                <i class="material-icons">keyboard_arrow_down</i>

                <span class="totalesMovil">$<?= number_format(resumenCompraShortTotales($comuna_id,$retiroEnTienda, $idSucursal),0,",",".");?></span>
            </a>
            <div class="contOcultoCarroResponsive">
                <div id="contDatosCartVariables2">
                    <div class="contCartCompraRapida">
                      <?= resumenCompraShort($comuna_id,$retiroEnTienda,$idSucursal); ?>
                    </div>
                </div>
                <?php if(qty_pro() > 0){?>
                <a href="javascript:void(0)" class="btnTerminarCarro" id="comprarMovil" />Siguiente</a>
                <?php } ?>
            </div><!--fin contOcultoCarroResponsive-->
        </div><!--resumen-->

        <div class="carroContIzquierda">
            <div class="contDatosPasoEnvio">
                <a href="home" class="contLogoCarro">
                    <img src="img/logoCarro.jpg" alt="Imagen de logo">
                </a>
                <!-- Primera seccion -->
                <div class="cont100">
                    <div class="cont50 contTituloResponsive">
                        <h2>Información de contacto</h2>
                       
                    </div>
                    <div class="cont50 contOpcionFactura">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?>
                        <h4>¿Ya tienes una cuenta? <a id="modificarDatos" class="fancybox" href="#inline1" rel="<?php echo $idCliente; ?>">inicia sesión</a></h4>
                        <?php } ?>
                    </div>
                    <?php //if(isset($_COOKIE['usuario_id']) and $usuarioTipo == 'empresa'){ ?>
                            <!--<input type="radio" name="factura" value="si" id="facturaSi" checked="checked" style="display:none;" />-->

                    <?php //} else { ?>
                    <!-- <div class="cont50 contOpcionFactura">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?>
                        <div class="parcheBlanco"></div>
                        <?php } ?> 
                        <div class="opcionFactura" >
                            <input type="radio" name="factura" value="no" id="facturaNo" checked="checked" />
                            <span class="tituloRadio">Boleta</span>

                            <input type="radio" name="factura" value="si" id="facturaSi" />
                            <span class="tituloRadio">Factura</span>
                        </div>  
                    </div> -->
                    <?php //} ?>
                </div><!--fin cont100-->

                <!-- Fin Primera seccion --> 
                <!-- Datos personales   -->
                <div class="contDatosPersonales">
                    <?php if(isset($_COOKIE['usuario_id'])){ ?>
                    <div class="cont100">
                        <label for="email">Correo electronico</label>
                        <?php if(tipoClienteActivo() == 2 || tipoClienteActivo() == 3 ){ ?>
                        <input class="campoGrande2" type="text" name="email" id="email" value="<?= $usuarioEmail;?>" data-new-placeholder="Correo electrónico" placeholder="Correo electrónico" readonly />
                        <?php } else { ?>
                        <input class="campoGrande2" type="text" name="email" id="email" value="<?= $usuarioEmail;?>" data-new-placeholder="Correo electrónico" placeholder="Correo electrónico" />
                        <?php } ?> 
                    </div>
                    <?php } else { ?>
                    <div class="cont100">
                        <label for="email">Correo electrónico</label>
                        <div class="contCorreoCarro">
                            <input class="campoGrande2" type="text" name="email" id="email" value="" data-new-placeholder="Correo electrónico" placeholder="Correo electrónico" />
                        </div>
                        <div class="contBtnValidarMail">
                            <a class="btnValidarMail" id="btnValidarMail" href="javascript:void(0)">Validar</a>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="opcionFactura" >
                    <?php if(!isset($_COOKIE['usuario_id'])){ ?>
                        <div class="parcheBlanco"></div>
                    <?php } ?> 
                        <h2>Elige un tipo de despacho</h2>
                        <div class="contDespacho">
                            <?php
                            $checked1= 'checked="checked"';
                            $checked2= '';
                            if ($envio == "retiroEnTienda") {
                                $checked2= 'checked="checked"';
                                $checked1= '';
                            }
                            ?> 
                            <input id="tienda2" type="radio" name="envio" value="despachoDomicilio" <?= $checked1 ?> />
                            <span class="tituloRadio">Despacho a domicilio</span>
                        </div>
                        <div class="contDespacho">
                            <input id="tienda1" type="radio" name="envio" value="retiroEnTienda"  <?= $checked2 ?>/>
                            <span class="tituloRadio">Retiro en tienda</span>
                        </div>
                    </div>  


                    <div class="contInfoDespacho cont100 contInfoDespacho2">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?>
                        <div class="parcheBlanco"></div>
                        <?php } ?>

                        <div class="cont100">
                            <label for="nombre">Nombre y apellido</label>
                            <?php if(tipoClienteActivo() == 2 || tipoClienteActivo() == 3 ){ ?>
                            <input type="text" name="nombre" id="nombre" value="<?php echo $usuarioNombre; ?>" class="campoGrande2" data-new-placeholder="Nombre y apellido" placeholder="Nombre y apellido" readonly />
                            <?php } else { ?>
                            <input type="text" name="nombre" id="nombre" value="<?php echo $usuarioNombre; ?>" class="campoGrande2" data-new-placeholder="Nombre y apellido" placeholder="Nombre y apellido" />
                            <?php } ?>                            
                        </div>

                        <div class="cont100">
                            <div class="cont50-1">
                                <label for="telefono">Teléfono</label>
                                <?php if(tipoClienteActivo() == 2 || tipoClienteActivo() == 3 ){ ?>
                                <input type="text" name="telefono" id="telefono" value="<?php echo $usuarioTelefono; ?>" class="campoGrande2" data-new-placeholder="Teléfono" placeholder="Teléfono" minlength="9" maxlength="11" readonly />
                                <?php } else { ?>
                                <input type="text" name="telefono" id="telefono" minlength="9" maxlength="11" value="<?php echo $usuarioTelefono; ?>" class="campoGrande2" data-new-placeholder="Teléfono" placeholder="Teléfono" />
                                <?php } ?> 
                  
                            </div>
                            
                            <div class="cont50-2">
                                <label for="rut">Rut</label>
                                <?php if(tipoClienteActivo() == 2 || tipoClienteActivo() == 3 ){ ?>
                                <input type="text" name="rut" id="rut" value="<?php echo $usuarioRut; ?>" class="campoGrande2" data-new-placeholder="Rut" placeholder="Rut" readonly />
                                <?php } else { ?>
                                <input type="text" name="rut" id="rut" value="<?php echo $usuarioRut; ?>" class="campoGrande2" data-new-placeholder="Rut" placeholder="Rut" />
                                <?php } ?>              
                            </div>
                            
                        </div><!--datos cliente-->
                    </div>

                    <!-- Fin Datos personales   -->

                    <div class="cont100 contOpcionFactura">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?>
                        <div class="parcheBlanco"></div>
                        <?php } ?> 
                        <div class="opcionFactura" >
                            <input type="radio" name="factura" value="si" id="facturaSi" />
                            <span class="tituloRadio">Necesito Factura</span>
                            <div class="ocultar">
                            <input type="radio" name="factura" value="no" id="facturaNo" checked="checked" />
                            <span class="tituloRadio">Boleta</span>
                            </div>

                        </div>  
                    </div>

                    <!-- Datos de facturacion -->
                    <?php /*if(isset($_COOKIE['usuario_id']) and $usuarioTipo == 'empresa'){ echo "style='display:block;'";}*/ ?>
                    <div class="contDatosFactura">
                            <h2>Datos de facturación</h2>
                            <div class="cont100">
                                <div class="cont50-1">
                                    <label for="RutFactura">Rut empresa</label>
                                    <input type="text" name="rut_factura" value="<?= $rut_empresa; ?>" id="RutFactura" class="campoGrande2" data-new-placeholder="Rut" placeholder="Rut" />
                                </div>

                                <div class="cont50-2">
                                    <label for="giro">Giro</label>
                                    <input type="text" name="giro_factura" value="<?= $giro; ?>" id="giro" class="campoGrande2" data-new-placeholder="Giro" placeholder="Giro" />
                                </div>
                            </div>

                            <div class="cont100">
                                <div class="cont50-1">
                                    <label for="razonSocial">Razón social</label>
                                    <input type="text" name="razon_social" value="<?= $razon_social; ?>" id="razonSocial" class="campoGrande2" data-new-placeholder="Razón social" placeholder="Razón social" />
                                </div>
                                <div class="cont50-2">
                                    <label for="direccionFactura">Dirección facturación</label>
                                    <input type="text" name="direccion_factura" id="direccionFactura" value="<?= $direccion_facturacion; ?>" class="campoGrande2" data-new-placeholder="Dirección facturación" placeholder="Dirección facturación" />
                                </div>
                            </div>                            
                    </div><!--Fin contDatosFactura -->

                    <!-- Fin Datos de facturacion -->
                    <div class="caluga-retiro">
                        <div class="tituloSucursalesDespacho">Puedes retirar tus productos en:</div>
                        <?php $sucursales = consulta_bd("s.id, s.nombre, s.direccion, s.comuna_id, s.latitud, s.longitud, c.nombre as nombre_comuna, r.nombre as nombre_region, s.link_mapa","sucursales s, comunas c, regiones r","s.retiro_en_tienda = 1 and s.comuna_id = c.id and c.region_id = r.id","s.posicion asc");
                        for($i=0; $i<sizeof($sucursales); $i++) {
                        ?> 
                        <div class="filaSucursalCarro">
                            <div class="contRadioDireccion">
                                <input type="radio" name="idSucursal" id="sucursal<?= $sucursales[$i][0]; ?>" value="<?= $sucursales[$i][0]; ?>" <?php if($i == 0){echo 'checked="checked"';}?> />
                            </div>
                                <div class="contNombreSucursal">
                                <!--<span class="nombreSucursal"><?= $sucursales[$i][1]; ?></span>-->
                                <span class="direccionSucursal"><?= $sucursales[$i][2]; ?>, <?= $sucursales[$i][6]; ?>, <?= $sucursales[$i][7]; ?></span>
                            </div>
                            <div class="contLinkMapa">
                                <?php if($sucursales[$i][8] != ""){?>
                                <a class="mapaSucursal" data-fancybox data-type="iframe"  data-src="pags/mapaSucursales.php?id=<?= $sucursales[$i][0]; ?>" href="javascript:void(0)">Ver mapa</a>
                                <?php } ?>
                            </div> <!--fin contLinkMapa-->
                        </div>
                        <?php } ?>
                    </div>
                    <!-- Direcciones  -->

                    <?php if(isset($_COOKIE['usuario_id'])){ ?>
                    <div class="cont100">
                    <!--<div class="direccionesGuardadas">-->
                    <?php 
                        if($usuarioParent == 0){
                            $direcciones = consulta_bd("id, nombre","clientes_direcciones","cliente_id = ".$_COOKIE['usuario_id'],"id desc");
                        } else{
                            $direcciones = consulta_bd("id, nombre","clientes_direcciones","cliente_id = $usuarioParent","id desc");
                        }

                    ?>
                        <label class="labelDirecciones" for="direcciones">Tus direcciones guardadas 
                            <a class="masDir" id="nuevaDireccion" href="#contNuevaDireccion">
                                <i class="fas fa-plus-circle"></i> Agregar dirección
                            </a>
                        </label>
                        <div class="compraSelect2">
                        <select name="direcciones" id="direcciones">
                            <option>Seleccione Dirección</option>
                            <?php for($i=0; $i<sizeof($direcciones); $i++){ ?>
                            <option value="<?= $direcciones[$i][0]; ?>"><?= $direcciones[$i][1]; ?></option>
                            <?php } ?>
                        </select>
                        </div>
                        <!--</div>-->
                    </div>
                    
                     
                    <?php } ?>

                    <?php $regiones = consulta_bd("id, nombre","regiones","id <> 16","id desc");?>
                   
                   <div class="contDireccion">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?>
                            <div class="parcheBlanco"></div>
                        <?php } ?> 
                        <!--al ser cuenta de empresa oculto esto para que no puedan ingresar direcciones nuevas, solo puedan usar las que vienen disponibles-->
                        <h2>Dirección de envio</h2>
                        <div class="cont100">
                            <label for="direccion">Dirección y número</label>
                            <input type="text" name="direccion" id="direccion" value="<?php if($calle != ''){ echo $calle.','.$numero;}; ?>" class="campoGrande2" data-new-placeholder="Dirección" placeholder="Dirección" />
                        </div>
                        <div class="cont100">
                            <div class="cont50-1">
                                <div class="compraSelect">
                                <label for="region">Región</label>
                                <select name="region" id="region">
                                    <option>Seleccione region</option>
                                    <?php for($i=0; $i<sizeof($regiones); $i++){ ?>
                                    <option <?php if($regiones[$i][0] == $region_id){echo 'selected="selected"';} ?> value="<?php echo $regiones[$i][0]; ?>"><?php echo $regiones[$i][1]; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>

                            <?php $comunas = consulta_bd("c.id, c.nombre","comunas c","c.region_id = $region_id","c.nombre asc");
                            ?>
                            <div class="cont50-2">
                                <div class="compraSelect">
                                <label for="comuna">Comuna</label>
                                <select name="comuna" id="comuna">
                                    <option>Seleccione Comuna</option>
                                    <?php for($i=0; $i<sizeof($comunas); $i++){ ?>
                                    <option <?php if($comunas[$i][0] == $comuna_id){echo 'selected="selected"';} ?> value="<?php echo $comunas[$i][0]; ?>"><?php echo $comunas[$i][1]; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Mensajes de despacho -->
                        <div class="mensajeDespachoCarro">
                            <div class="cont100">
                                <strong>Region Metropolitana:</strong> <?= opciones('mensaje_despacho_santiago');?>
                            </div>
                            <div class="cont100">
                                <strong>Otras Regiones:</strong> <?= opciones('mensaje_despacho_regiones');?>
                            </div>
                        </div>

                        <!-- Fin Mensajes de despacho -->

                    </div>
                    <!-- fIN Direcciones  -->

                    <!-- Fin de empresa oculta -->

                    

                    <!-- Quien retira? -->

                    <div class="contCamposRetiro" style="display: none;">
                        <h2>¿Quién retira?</h2>
                        <h4><?= opciones('horario_retiro'); ?></h4>
                        <div class="mensajeDespachoCarro">
                            <?= opciones('mensaje_retiro'); ?>        
                        </div>
                        <?php  //if(isset($_COOKIE['usuario_id'])){ ?> 
                        <div class="cont100 btnRetiro">
                            <a href="javascript:void(0)" class="" id="yoRetiro">Yo retirare</a>
                            <a href="javascript:void(0)" class="" id="otraPersona">Otra persona</a>
                        </div>
                        <?php //} ?>

                        <div class="cont100">
                            <div class="cont50-1">
                                <input type="text" class="campoGrande2 label_better" name="nombre_retiro" value="" placeholder="Nombre y Apellido" data-new-placeholder="Nombre y Apellido" id="nombre_retiro">
                            </div>
                            <div class="cont50-2">
                                <input type="text" class="campoGrande2 label_better" name="telefono_retiro" value="" data-new-placeholder="Teléfono" placeholder="Teléfono" id="telefono_retiro">
                            </div>
                        </div>
                    </div>

                    <!--Fin Quien retira? -->

                    <div class="contFinalCompra">
                        <?php if(!isset($_COOKIE['usuario_id'])){ ?>
                            <div class="parcheBlanco"></div>
                        <?php } ?>
                        <!-- Dejar Comentarios -->
                        <div class="cont100">
                            <h2 class="tituloDespachos">Dejar comentarios</h2>
                            <textarea name="comentarios_envio" class="form-control comentarios-envio" rows="3" placeholder="Ej: Dejar envío con el conserje."><?php if($comentarios_envio != 0 || $comentarios_envio != null){echo $comentarios_envio; }; ?></textarea>
                        </div><!--fin comentarios-->

                        <!-- Fin Dejar Comentarios -->

                        <div class="contPaso3Movil" id="retiroDespacho">
                            <div class="cont100">
                                <a class="btnVerMiCarro" href="javascript:void(0)"><i class="fas fa-angle-left"></i> Ver mi carro</a>
                            </div>
                            <div class="cont100 menuTerminos">
                                <a href="como-comprar">¿Cómo comprar?</a>
                                <a href="politicas-de-privacidad">Políticas de privacidad</a>
                                <a href="terminos-y-condiciones">Términos y condiciones</a>
                            </div>
                        </div> 
                    </div>

                </div><!--fin colForm1-->
            </div><!--fin contDatosPasoEnvio-->
        </div> <!--Fin datosForm-->

        <div class="carroContDerecha">
            <?php /*?>
             <div class="cont100 contCodigoDescuento">
                <label for="codigo_descuento">Código de descuento</label>
                    <?php if($_SESSION["descuento"]){
                            $codigo = $_SESSION["descuento"];
                            descuentoBy($codigo);
                            $estadoInput = 'disabled="disabled"';
                            $icono = '<i class="fas fa-check"></i>';
                    } else {
                        $estadoInput = '';
                    } ?>
                    <div class="codigoOK"><?php echo $icono; ?></div>
                    <input type="text" name="codigo_descuento" class="descuento campoPasoFinal" value="<?php echo $codigo; ?>" placeholder="Ingresa el código" <?php echo $estadoInput ?>>
                    <button type="button" class="aplicarDescuento">APLICAR</button>
            </div>
            <?php */ ?>
                    
            <div id="contDatosCartVariables">
                <div class="contCartCompraRapida">
                  <?= resumenCompraShort($comuna_id, $retiroEnTienda,$idSucursal); ?>
                </div>
            </div>

            <?php if(qty_pro() > 0){?>
                <?php if(!isset($_COOKIE['usuario_id'])){ ?>
                <a href="javascript:void(0)" class="btnTerminarCarro" id="comprar" style="display:none;"/>Siguiente</a>
                <?php } else { ?>
                <a href="javascript:void(0)" class="btnTerminarCarro" id="comprar"/>Siguiente</a>
                <?php } ?>
            <?php } ?>
        </div><!--resumen-->


        <input type="checkbox" name="terminos" id="terminos" value="1" checked="checked" style="display:none;" />
    </div>
</form>   






<?php if(tipoClienteActivo() == 1){ ?>
    <div style="display:none;">
        <div class="contNuevaDireccion" id="contNuevaDireccion">
            <div class="cont100 direcciones">
            <?php $regiones = consulta_bd("id, nombre","regiones"," id <> 16","posicion asc"); ?>            
                <form method="post" action="ajax/nuevaDireccion.php" id="formNuevaDireccion" class="bl_form">
                    <input type="hidden" name="usuario_idPopUp" value="<?= $_COOKIE['usuario_id']; ?>" />
                    <h2>Agregar nueva dirección</h2>
                    <div class="cont100">
                        <div class="cont100">
                            <input class="campoGrande2 label_better" type="text" name="nombre_PopUp" id="nombre_PopUp" value="" data-new-placeholder="Nombre" placeholder="Nombre"  />
                        </div>

                    </div>
                    <div class="cont100">
                        <div class="col1-50">
                            <!--<label>Región</label>-->
                            <select name="region_idPopUp" id="region_PopUp">
                                <option>Seleccione región</option>
                                <?php for($i=0; $i<sizeof($regiones); $i++) {?>
                                <option value="<?= $regiones[$i][0];?>"><?= $regiones[$i][1];?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col2-50">
                            <!--<label>Comuna</label>-->
                            <select name="comuna_idPopUp" id="comuna_PopUp">
                                <option value="">Seleccione Comuna</option>
                            </select>
                        </div>

                      </div> 


                    <div class="cont100">
                        <input class="campoGrande2 label_better" type="text" name="calle_PopUp" value="" id="calle_PopUp" data-new-placeholder="Dirección" placeholder="Dirección" />
                    </div>
                    <div class="cont100">
                        <input type="hidden" name="origen" value="envio-y-pago">
                        <a class="guardarDireccion" id="guardarDireccionCompraRapida" href="javascript:void(0)">Guardar dirección</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!--Fin direcciones-->
    <?php } ?>
            