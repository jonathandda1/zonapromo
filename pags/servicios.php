
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
    	<ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Servicios</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->
<div class="cont100">
    <div class="cont100Centro">
    <h1 class="tituloServicios">Servicio Técnico</h1>
        <section class="contServicios">
            <article class="servContenido">               
                <div class="imagenServicios">
                    <img src="img/servicio-tecnico1.jpg" alt="Imagen de Servicio tecnico">          
                </div>
                <p>Pola magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                <p>Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste nat.</p>
            </article>
            <article class="servFormulario">
                <div class="contServicioForm">
                    <form action="ajax/formularios.php" method="post" class="formularios">
                        <p class="tituloFormSlider">Deja tus datos y nos pondremos en contacto</p>
                        <hr class="hrForm">
                        <div class="centroFormSlider">
                            <input type="text" id="nombre" name="nombre" class="campoGrande2 label_better" value="" placeholder="Nombre" data-new-placeholder="Nombre"/>                                
                            <input type="text" id="email" name="email" class="campoGrande2 label_better" value="<?= $usuarioCookie; ?>" placeholder="E-mail" data-new-placeholder="Correo"/>
                            <input type="number" id="telefono" name="telefono" class="campoGrande2 label_better" value="" placeholder="Teléfono"/>
                            <textarea id="mensaje" name="mensaje" class="textTareaForm label_better" value="" placeholder="Mensaje" data-new-placeholder="Mensaje"></textarea>
                            <input type="hidden" name="tabla" id="tabla" value="formulario_servicioTecnico">
                            <button type="button" class="btnForm">Enviar</button>
                        </div>
                    </form>
                </div>
            </article>
        </section>
    </div>
</div>

        