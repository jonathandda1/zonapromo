<script>
<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo 'location.href = "inicio-sesion"';
	}
    
?>
</script>

<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Datos empresa</li>
        </ul>
    </div>
</div>


<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            
            
<?php 
    if($usuario[perfilCliente] == 3){
        $disabled = 'disabled';
        $cliente = consulta_bd("id, nombre_empresa, razon_social, rut_empresa, giro, direccion_facturacion, email_facturacion, telefono_facturacion","clientes","id = $usuarioParent","");
    } else {
        $disabled = '';
        $cliente = consulta_bd("id, nombre_empresa, razon_social, rut_empresa, giro, direccion_facturacion, email_facturacion, telefono_facturacion","clientes", "id = $usuarioID", "");
    }
     ?>        	
            <div class=" datosCliente">
                <div class="titulosCuenta">
                    <span>Datos empresa</span>
            <p>Podrás ver tus datos de facturación, modificarlos o ingresarlos.</p>
                </div><!--fin titulosCuenta-->
                
            	<form action="ajax/actualizarDatos.php?retorno=1" method="post" id="formularioActualizar" class="bl_form">
                    
                    <div class="cont100">
                       <input type="text" id="empresa" name="nombre_empresa" class="campoGrande2 label_better" value="<?= $cliente[0][1]; ?>" placeholder="Nombre empresa" data-new-placeholder="Empresa" <?= $disabled; ?> />
                   </div>
                    
                    <div class="cont50Form">
                        
                        <div class="cont100">
                           <input type="text" id="razon_social" name="razon_social" class="campoGrande2 label_better" value="<?= $cliente[0][2]; ?>" placeholder="Razón social" data-new-placeholder="Razón social" <?= $disabled; ?>/>
                       </div>
                        
                       <div class="cont100">
                           <input type="text" id="giro" name="giro" class="campoGrande2 label_better" value="<?= $cliente[0][4]; ?>" placeholder="Giro" data-new-placeholder="Giro" <?= $disabled; ?>/>
                       </div>
                       
                        <div class="cont100">
                           <input type="text" id="email_facturacion" name="email_facturacion" class="campoGrande2 label_better" value="<?= $cliente[0][6]; ?>" placeholder="Email" data-new-placeholder="Email" <?= $disabled; ?>/>
                       </div>
                       
                       <div style="clear:both"></div>
                       
                    </div><!--iniciar sesion -->
                    
                    <div class="cont50Form">
                        <div class="cont100">
                           <input type="text" id="Rut_empresa" name="rut_empresa" class="campoGrande2 label_better" value="<?= $cliente[0][3]; ?>" placeholder="Rut" data-new-placeholder="Rut" <?= $disabled; ?>/>
                       </div>
                        <div class="cont100">
                           <input type="text" id="direccion_facturacion" name="direccion_facturacion" class="campoGrande2 label_better" value="<?= $cliente[0][5]; ?>" placeholder="Direccion facturación" data-new-placeholder="Direccion facturación" <?= $disabled; ?> />
                       </div>
                       <div class="cont100">
                           <input type="text" id="telefono_facturacion" name="telefono_facturacion" class="campoGrande2 label_better" value="<?= $cliente[0][7]; ?>" placeholder="Teléfono facturación" data-new-placeholder="Teléfono facturación" <?= $disabled; ?> />
                       </div>
                       
                       <div style="clear:both"></div>
                       
                    </div><!--iniciar sesion -->
                    <?php if($usuario[perfilCliente] != 3){ ?>
                    <div class="cont100">
                    	<a href="javascript:void(0)" id="btnActualizarEmpresa" style="float:left; margin-left:10px;" class="btnFormRegistro">ACTUALIZAR DATOS</a>
                    </div>
                    <?php } ?>
                </form>
                
     
            </div>
            
            <?php include("pags/tipsMiCuenta.php");?> 
        
        </div><!--fin contenidoMiCuenta-->
        
      
               
    </div>
</div>
