
 <div class="cont100 contBreadCrumbs" style="margin-bottom:0;">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Conviértete en distribuidor regional</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->



<div class="cont100">
    <div class="contCentroPagFijas">
        <div class="contTxtParrafo">
            
            <div class="cont100 contFormServ">
                <div class="tituloContacto">Conviértete en distribuidor regional</div>
                <p>Completa nuestro formulario y conviértete en un distribuidor regional de Zona Promo.</p>
                <form method="post" action="mailsenderDistribuidor.php" class="bl_form" id="formContacto">
                    <input type="text" id="nombre" name="nombre" class="campoGrande2 label_better" value="" placeholder="Nombre y Apellido" data-new-placeholder="Nombre y Apellido"/>
                
                    <div class="cont100">
                        <div class="col1-50">
                            <input type="text" id="email" name="email" class="campoGrande2 label_better" value="" placeholder="Email" data-new-placeholder="Email"/>
                        </div>
                        
                        <div class="col2-50">
                            <input type="text" id="telefono" name="telefono" class="campoGrande2 label_better" value="" placeholder="Telefono" data-new-placeholder="Telefono"/>
                        </div>
                    </div>
                    
                    
                    <div class="cont100">
                        <textarea id="mensaje" name="mensaje" class="campoGrande3 label_better" value="" placeholder="Mensaje" data-new-placeholder="Mensaje"></textarea>
                    </div>
                    
                    <div class="cont100" style="text-align:center;">
                        <a href="javascript:void(0)" class="btnFormContacto" id="btnFormContacto">ENVIAR</a>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>


