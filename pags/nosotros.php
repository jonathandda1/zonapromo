
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
    	<ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Somos</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->
<div class="cont100">
    <section class="contNosotros">
        <h1 class="tituloNosotros">¿Quienes somos?</h1>
        <article class="nos1">
            <p>Somos una empresa líder en el mercado nacional especializada en regalos corporativos, tenemos muchos años de experiencia, lo que nos permite entregar un servicio integral a las empresas que buscan tener un socio comercial con experiencia en merchandising.</p>

            <div class="contIconosNosotros">
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110501_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>PRODUCTOS PERZONALIZADOS</h6>
                    </div>
                </a>
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110502_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>INNOVACIÓN</h6>
                    </div>
                </a>
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110539_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>EQUIPO CREATIVO</h6>
                    </div>
                </a>
                
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110504_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>ASESORÍA DE DISEÑO</h6>
                    </div>
                </a>
            </div>
            
        </article>
        <article class="nos2">
            <img src="img/nosotros1.jpg" alt="Imagen de Nosotros">
        </article>
        <article class="nos3">
            <p>Tenemos importación directa de productos, por lo que tenemos un amplio stock de regalos promocionales. Sabemos lo importante que es para cada empresa tener una buena plataforma de marketing, que logre cautivar a sus clientes y/o colaboradores. Por lo que estamos en constante búsqueda de productos innovadores que logren potenciar y cautivar a su público objetivo a través de un novedoso regalo corporativo. Entregamos el producto final con logo corporativo a lo largo de todo nuestro país.</p>
            <p>Contamos con bodega propia, la que nos permite dar un mejor servicio y; a su vez entregar una solución logística a distintas empresas, nos preocupamos de llevar sus productos hasta sus propias bodegas o dependencias.</p>
        </article>
        <article class="nos4">
            <p>Constamos con socios comerciales en diferentes partes del mundo, especialmente en Oriente, lo que nos permite ofrecer la fabricación e importación de cientos de productos con altos estándares de calidad garantizados. Desarrollamos productos exclusivos, potenciamos sus campañas publicitarias a través de un producto final único donde destaca por sobre todo su imagen corporativa.</p>
            <br>
            <p>También hacemos servicios adicionales de bodegaje, mecanizados, paletizado, picking y/o packaging y distribución a regiones; según requerimiento de cada cliente.</p>
        </article>
        <article class="nos5">
            <img src="img/nosotros2.jpg" alt="Imagen de Nosotros">
        </article>
        <article class="nos6">
        <div class="contIconosNosotros">
                
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110505_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>BODEGAS PROPIAS</h6>
                    </div>
                </a>
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110506_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>DESPACHO A TODO CHILE</h6>
                    </div>
                </a>
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110507_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>CALIDAD</h6>
                    </div>
                </a>
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110508_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>OFICINA EN CHINA</h6>
                    </div>
                </a>
            </div>
            <p>Tenemos un equipo multidisciplinario profesional y especializado que nos permite entregar un servicio íntegro para responder a las necesidades de cada cliente. Diseño de productos, asesoría publicitaria, control de calidad, valores convenientes, mecanizados especiales, kit de productos, entre otros ; es lo que puedes encontrar en Zona Promo.</p>  
        </article>
        <h2 class="subtituloNos subNos1">Nuestros clientes</h2>
        <article class="nos7">
            <img src="img/nosotros3.jpg" alt="Imagen de Nosotros">
        </article>
        <article class="nos8">
            <p>Buscamos desarrollar con nuestros clientes una relación comercial que perdure a través del tiempo. Contamos con una amplia cartera de clientes de diversos rubros tales como: empresas de retail, minería, laboratorios, automotriz, agencias, bancos, aseguradoras, cajas de compensación, sindicatos, pequeñas, medianas y grandes empresas del país, quienes han depositado su confianza en nuestra experiencia y equipo profesional que está detrás de nuestro trabajo.</p>    
        </article>
        <h2 class="subtituloNos subNos2">Misión</h2>
        <article class="nos9">
            <img src="img/nosotros1.png" alt="Imagen de Nosotros">
        </article>
        <article class="nos10">
            <p>Nuestra principal misión es convertirnos en la mejor inversión de nuestro cliente, en base a un compromiso profesional. No ofrecemos sólo productos, también buscamos entregar soluciones integradoras, eficientes y distintivas que reflejen identidad, calidad e innovación en tu marca. </p>
            <div class="contIconosNosotros">
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110509_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>MECANIZADO DE PRODUCTOS</h6>
                    </div>
                </a>
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110510_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>PALETIZADO DE PRODUCTOS</h6>
                    </div>
                </a>
                <a href="javascript:void(0)" class="itemIconoNosotros">
                    <div class="imgIconoNosotros">
                        <img src="imagenes/nosotros/3008110511_banner1.png" alt="Imagen de icono de nosotros">
                    </div>
                    <div class="textIconoNosotros">
                        <h6>PICKING Y/O PACKAGING</h6>
                    </div>
                </a>
                
            </div>
            <h2 class="subtituloNos">Objetivo</h2>
            <p>Nuestro principal objetivo es generar y crear soluciones integrales en comunicación, publicidad y marketing y para lograrlo, nos adecuamos a las necesidades del cliente y de esta manera logramos el éxito en cada trabajo que desarrollamos, con el profesionalismo y seriedad que nos caracteriza.</p>
            <br>
            <p>Damos soluciones a los requerimientos de nuestros clientes, mediante una exhaustiva asesoría, eso se logra en base a la experiencia que tenemos.</p>
            <br>
            <p>Construir una relación integra, de confianza y a largo plazo; estableciendo un compromiso real con nuestros clientes.</p>
            <br>
            <p>Ejecutar con precisión y excelencia los requerimientos de nuestros clientes.</p>
        </article>
        <h2 class="subtituloNos subNos3">Nuestros valores</h2>
        <article class="nos11">
            <img src="img/nosotros2.png" alt="Imagen de Nosotros">
        </article>
        <article class="nos12">
            <li>Respeto: hacia nuestros colaboradores y clientes.</li> 
            <li>Responsabilidad y Compromiso: construimos una relación de confianza, estableciendo un compromiso real con nuestros clientes.</li> 
            <li>Trabajo en Equipo: colaboramos y sumamos esfuerzos, logrando multiplicar nuestros logros.</li> 
            <li>Efectividad: ejecutamos con precisión y excelencia los requerimientos de nuestros clientes.</li> 
            <li>Pasión:  entregamos nuestro mayor esfuerzo como equipo para superar los objetivos impuestos.</li>
            <li>Orientación al cliente: construimos relaciones a largo plazo pues nuestros clientes son lo más importante para nosotros.</li>
        </article>

        <article class="nos13">   
            
        </article>
        
    </section>
</div>

        