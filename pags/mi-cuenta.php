<script>
<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo 'location.href = "inicio-sesion"';
	}
?>
</script>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Mi cuenta</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            
<?php $cliente = consulta_bd("id, nombre, apellido, rut, telefono, email","clientes","id = ".$_COOKIE['usuario_id'],""); ?>        	
            <div class="datosCliente">
            	<div class="titulosCuenta">
                    <span>Hola, <?= $cliente[0][1];?></span>
                    <p>Aquí encontrarás tus datos personales, los datos de tu empresa y podrás revisar tus cotizaciones y todo lo relacionado con tu cuenta del sitio web.</p>
                </div><!--fin titulosCuenta-->
                
                <div class="cont100">
                    <h5 class="subtituloMiCuenta">Ten siempre presente</h5>
                    
                    <div class="contAccsesoMiCuenta">
                        <a href="datos-personales" class="btnAccesoDirecto">
                            <span class="iconoDashboard">
                               <i class="material-icons">person_outline</i> 
                            </span>
                            <span class="nombreDashboard">Actualiza tus datos</span>
                        </a><!--btnAccesoDirecto -->
                        
                        <a href="mi-empresa" class="btnAccesoDirecto">
                            <span class="iconoDashboard">
                               <i class="material-icons">business</i> 
                            </span>
                            <span class="nombreDashboard">Actualiza datos de empresa</span>
                        </a><!--btnAccesoDirecto -->
                        
                        
                        <a href="productos-guardados" class="btnAccesoDirecto">
                            <span class="iconoDashboard">
                               <i class="material-icons">favorite_border</i> 
                            </span>
                            <span class="nombreDashboard">Crear lista de deseos</span>
                        </a><!--btnAccesoDirecto -->
                        
                        
                        <a href="mis-cotizaciones" class="btnAccesoDirecto">
                            <span class="iconoDashboard">
                               <i class="material-icons">sticky_note_2</i> 
                            </span>
                            <span class="nombreDashboard">Cotizaciones</span>
                        </a><!--btnAccesoDirecto -->
                    </div>
                    
                </div>
                
            </div> <!--fin columna datos-->
            
           <?php include("pags/tipsMiCuenta.php"); ?>
        
        </div><!--fin contenidoMiCuenta-->
        
      
               
    </div>
</div>