
 <div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Cambios y devoluciones</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->



<div class="cont100">
    <div class="contCentroPagFijas">
        <div class="tituloContacto">Cambios y devoluciones</div>
        <div class="contTxtParrafo">
            <p>La Política de cambio/devolución de Zona Promo está sujeta a la Ley 19.496 sobre los Derechos del Consumidor, y guardan relación sólo para lo que tiene que ver con las compras por Internet, sin guardar relación con nuestras sucursales.</p>

            <p>Derecho de Retracto y Cambio en los Productos: Si no estás satisfecho con la compra realizada, podrás optar por la devolución del producto en un plazo de 30 días corridos, contados desde la recepción del producto. La devolución deberá practicarse en las dependencias de nuestra Casa Matriz de San Diego 820, comuna de Santiago en horarios de lunes a viernes entre 9:30 y 14:00 horas, y de 15:30 a 19:00 horas respectivamente, para los clientes que se encuentren en la Región Metropolitana; o enviadas por transporte a esta misma dirección para los clientes de regiones. Deberá acompañarse siempre el ticket de la boleta o factura al momento de producirse la devolución de un producto, y contar éste con todos sus accesorios, embalajes o regalos promocionales, según corresponda el caso.</p>

            <p>Garantía Legal: Si cualquiera de nuestros productos presentare alguna falla de fabricación dentro de los primeros 90 días desde recibido el producto, el cliente podrá optar por la reparación, cambio o devolución del dinero. En caso del cliente optar por el cambio y encontrarse el producto con quiebre de stock, deberá únicamente optar por la reparación o la devolución del dinero.</p>

            <p>Devolución del Dinero: El dinero de toda compra será restituido en la proporción que indique la nota de crédito de la venta realizada mediante un cheque extendido a la persona, para que lo cobre en alguna de las dependencias del Banco Santander.</p>

            <p>No obstante, si su compra fue realizada a través de una tarjeta de crédito, la devolución será por medio de la reversa de la transacción al titular de la tarjeta. Para ello te solicitaremos los datos de tu tarjeta, y esta operación la realizará Transbank dependiendo los plazos de esta entidad, siendo generalmente entre 7 y 10 días hábiles. Si la transferencia hubiese sido hecha por una tarjeta de débito, la devolución del dinero se realizará a través de una transferencia electrónica bancaria, por lo que te solicitaremos los datos para realizar la transferencia, en un plazo de 48 horas.</p>


            <p>Las devoluciones serán autorizadas después de ser sometidas a un control por parte de nuestro personal encargado, por lo que al momento de exhibir la mercadería devuelta deberá ésta contar con todos sus embalajes y accesorios originales.</p>
        </div>
    </div>
</div>