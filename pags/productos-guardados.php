<script>
<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo 'location.href = "inicio-sesion"';
	}
?>
</script>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Productos guardados</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            <div class="datosCliente">
                <div class="titulosCuenta">
                    <span>Lista de deseos</span>
                    <p>Guarda tus productos favoritos en tu lista de deseos, para que puedas encontrarlos rápidamente cada vez que quieras</p>
                </div>
                
                <div id="contProductosGuardados">
                    <?= saveForLater(); ?>
                </div>
            </div><!-- fin datosCliente -->
			
            <?php include("pags/tipsMiCuenta.php"); ?>
            
        </div><!--fin contenidoMiCuenta-->
    
    </div>
</div>