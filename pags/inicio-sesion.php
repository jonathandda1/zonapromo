<?php 
	if(isset($_COOKIE['usuario_id'])){
		echo 'location.href = "mi-cuenta"';
	}
?>
<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Inicio de sesión</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        <div class="acceso1 titulo2">BIENVENIDO</div>
        <div class="acceso2 subtitulo2">Tu cuenta para todo lo relacionado con Zona Promo</div>
        
        <div class="cont50Identificacion">
        	<div class="acceso3 cont100">
            	<?php 
				if(isset($_COOKIE['nombreUsuario']) and isset($_COOKIE['claveUsuario'])){
					$checked = 'checked="checked"';
					$usuarioCookie = $_COOKIE['nombreUsuario'];
					$claveCookie = base64_decode($_COOKIE['claveUsuario']);
					} else {
						$checked = '';
						}
					?>
               <form action="ajax/login.php" method="post" class="formularioLogin bl_form" id="formularioLogin">
                   <!--<label for="email">Correo</label>-->
                   <input type="text" id="email" name="email" class="campoGrande2 label_better" value="<?= $usuarioCookie; ?>" placeholder="Ingrese su correo" data-new-placeholder="Correo"/>
                   <!--<label for="clave">Contraseña</label>-->
                   <input type="password" id="clave" name="clave" class="campoGrande2 label_better" value="<?= $claveCookie; ?>" placeholder="Ingrese su Clave" data-new-placeholder="Contraseña" />
                   <input type="hidden" name="origen" value="inicio-sesion" />
                   <div class="cont100">
                       <a href="recuperar-clave" class="btnRecuperarClave">¿Olvidaste la contraseña?</a>
                   </div>
                   <div class="cont100">
                        <input type="checkbox" name="recordarCuenta" <?= $checked; ?> /> <span>Recordar mis datos</span>
                   </div>
                   
                   
                   <a href="javascript:void(0)" id="inicioSesion" class="btnFormCompraRapida">Iniciar sesión</a>
                   <a href="registro" id="" class="btnFormCompraRapida2">Regístrate</a>
                   <div style="clear:both"></div>
                   <div class="cont100">
                        <p>Al iniciar sesión, aceptas la <a href="politicas-de-privacidad" class="btnParrafo">Política de privacidad</a> <br> y los <a href="terminos-y-condiciones" class="btnParrafo">términos de uso</a> de Zona Promo</p>
                   </div>
               </form>
           </div>
        </div><!--iniciar sesion -->
        
               
    </div>
</div>
