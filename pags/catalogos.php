<?php 
require_once 'paginador/paginator.class.php';

$page = (isset($_GET['page'])) ? mysqli_real_escape_string($conexion, $_GET['page']) : 0;
$ipp = (isset($_GET['ipp'])) ? mysqli_real_escape_string($conexion, $_GET['ipp']) : 4;

$catalogos = consulta_bd("titulo, descripcion, thumbs, archivo","catalogos","publicado = 1 and archivo IS NOT NULL","fecha desc");
$total = count($catalogos);
    $pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 12; 
	$rutaRetorno = "catalogos";
    $pages->paginate($rutaRetorno);

$catalogos = consulta_bd("titulo, descripcion, thumbs, archivo","catalogos","publicado = 1 and archivo IS NOT NULL","fecha desc $pages->limit");

?> 


<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Catálogos</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->   

<div class="cont100">
    <section class="contCatalogos">
        <div class="tituloPrincipalCatalogo">Catálogos</div>
        <article class="contGrillasCatalogo">
        <?php for($i=0; $i<sizeof($catalogos); $i++) { 
        ?>    
            <div class="grillaCatalogo">
                <div class="imgCatalogo">
                    <img src="imagenes/catalogos/<?= $catalogos[$i][2]?>" width="100%">
                </div>
                <div class="tituloCatalogo"><?= $catalogos[$i][0]?></div>
                <!-- <div class="bajadaCatalogo">
                    <?= preview($catalogos[$i][1], 170); ?>
                </div> -->
                <a class="btnCatalogo" href="docs/catalogos/<?= $catalogos[$i][3]?>" target="_blank">DESCARGAR</a>
            </div>
        <?php } ?>
        </article>
    </section>
</div>

<div class="cont100 paginadorGrid">
    <div class="paginador">
        <?= $pages->display_pages(); ?>
    </div>
</div><!--Fin paginador -->