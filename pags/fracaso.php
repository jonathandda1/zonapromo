<?php
    $oc = (isset($_GET['oc'])) ? mysqli_real_escape_string($conexion, $_GET['oc']) : 0;
    
    $PA = consulta_bd("id, estado_id, oc","pedidos","oc = '$oc'","");
	$cant = mysqli_affected_rows($conexion);
	if($oc == 'anulada'){
        update_bd("pedidos","estado_id = 3","oc='$oc'");
    } else if ($cant == 0){
		echo '<script>parent.location = "'.$url_base.'404";</script>';
	} else {
		update_bd("pedidos","estado_id = 3","oc='$oc' and payment_type_code IS NULL");
	}
    $volverALCarro = consulta_bd("id, estado_id, oc, nombre, rut, direccion, comuna, region, comentarios_envio, email, telefono, rut_factura, razon_social, direccion_factura, nombre_retiro, telefono_retiro, giro, factura, sucursal_id, retiro_en_tienda","pedidos","oc = '$oc'","");
    $nombre = $volverALCarro[0][3];
    $rut = $volverALCarro[0][4];
    $direccion = $volverALCarro[0][5];
    $comuna = $volverALCarro[0][6];
    $region = $volverALCarro[0][7];
    $comentarios_envio = $volverALCarro[0][8];
    $email = $volverALCarro[0][9];
    $telefono = $volverALCarro[0][10];
    $rut_factura = $volverALCarro[0][11];
    $razon_social = $volverALCarro[0][12];
    $direccion_factura = $volverALCarro[0][13];
    $nombre_retiro = $volverALCarro[0][14];
    $telefono_retiro = $volverALCarro[0][15];
    $giro = $volverALCarro[0][16];
    $factura = $volverALCarro[0][17];
    $idSucursal = $volverALCarro[0][18];
    $envio = $volverALCarro[0][19];
    $direccionCoRe = consulta_bd("id, region_id","comunas","nombre='$comuna'","");
    $comuna = $direccionCoRe[0][0];
    $region = $direccionCoRe[0][1];
    if ($factura == 1) {
        $factura = "si";
    }else{
        $factura= "no";
    }
    if ($envio == "Retiro en Tienda") {
        $envio = "retiroEnTienda";
    }else{
        $envio = "despachoDomicilio";
    }

    // echo "email:".$email."<br>nombre:".$nombre."<br>tele:".$telefono."<br>rut:".$rut."<br>envio:".$envio."<br>region:".$region."<br>comuna:".$comuna."<br>dire:".$direccion."<br>factura:".$factura."<br>idSucursa:".$idSucursal."<br>nombre_retiro:".$nombre_retiro."<br>tele_retiro:".$telefono_retiro."<br>rut_factura:".$rut_factura."<br>Giro:".$giro."<br>razon_social:".$razon_social."<br>direccion_factura:".$direccion_factura."<br>comentarios:".$comentarios_envio; 
?>

<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Fallo en Compra</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->
    
<div class="cont100">
    <div class="contFracaso">
        <div class="iconoFracaso"><img src="img/icono_fracaso.png" alt="Imagen de Fracaso"></div>
        <h2 class="tituloExito" style="position:relative;">Algo salió mal</h2><!--fin titulos_interiores -->

	<div class="top-identificacion2" style="margin-bottom:20px;">
        <div class="mensajePrincipal">Transacción Rechazada <span style="font-size:16px; color:#1f80d6; font-family: 'Raleway-Regular'; "><?= $oc; ?></span><br />
            Las posibles causas de este rechazo son:
        </div>
        
            <div class="txtExito"><img src="<?= imagen("img/","bullets.png");?>"> Error en el ingreso de los datos de su tarjeta de crédito o Debito (fecha y/o código de seguridad).</div>
            <div class="txtExito"><img src="<?= imagen("img/","bullets.png");?>"> Su tarjeta de crédito o debito no cuenta con el cupo necesario para cancelar la compra.</div>
            <div class="txtExito"><img src="<?= imagen("img/","bullets.png");?>"> Tarjeta aún no habilitada en el sistema financiero. </div>
            
        </div>
        
        <form method="post" action="envio-y-pago" id="formMiCarro2">
            <input type="hidden" name="carro-fracaso" value="1">
            <input type="hidden" name="recuperar_datos" value="1">    
            <input type="hidden" name="email" value="<?= $email; ?>"> 
            <input type="hidden" name="nombre" value="<?= $nombre; ?>"> 
            <input type="hidden" name="telefono" value="<?= $telefono; ?>"> 
            <input type="hidden" name="rut" value="<?= $rut; ?>"> 
            <input type="hidden" name="envio" value="<?= $envio; ?>"> 
            <!--direccionCliente-->
            <input type="hidden" name="region" value="<?= $region; ?>">
            <input type="hidden" name="comuna" value="<?= $comuna; ?>"> 
            <input type="hidden" name="direccion" value="<?= $direccion; ?>"> 

            <input type="hidden" name="idSucursal" value="<?= $idSucursal; ?>"> 
            <!--id de la sucursar de retiro-->
            <input type="hidden" name="nombre_retiro" value="<?= $nombre_retiro; ?>"> 
            <input type="hidden" name="telefono_retiro" value="<?= $telefono_retiro; ?>"> 
            <input type="hidden" name="factura" value="<?= $factura; ?>">
            <!--si o no-->
            <input type="hidden" name="rut_factura" value="<?= $rut_factura; ?>"> 
            <input type="hidden" name="giro_factura" value="<?= $giro; ?>"> 
            <input type="hidden" name="razon_social" value="<?= $razon_social; ?>"> 
            <input type="hidden" name="direccion_factura" value="<?= $direccion_factura; ?>">
            <input type="hidden" name="comentarios_envio" value="<?= $comentarios_envio; ?>">
        
            <div class="cont100 contBtnFracaso">
                <a id="volverSitio" href="home">VOLVER AL SITIO</a>
                <a id="volverAlCarro" href="javascript:void(0)">VOLVER A MI CARRO</a>
            </div>
        
        </form>
        
    </div>
</div><!--Fin mensaje --> 

<!--
<script type="text/javascript">
	ga('ec:setAction','checkout', {'step': 4});
</script>-->