<div class="bannerFicha cont100"></div>
<script>
<?php 
    $oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
    if(!isset($_COOKIE['usuario_id'])){
        echo 'location.href = "inicio-sesion"';
    }
?> 
</script>

<?php 
    $pedidos = consulta_bd("
    id,
    oc,
    total_pagado, 
    fecha_creacion, 
    medio_de_pago, 
    retiro_en_tienda, 
    direccion, 
    comuna, 
    region, 
    total, 
    valor_despacho, 
    total_pagado, 
    accounting_date, 
    card_number, 
    card_expiration_date, 
    authorization_code, 
    payment_type_code, 
    shares_number, 
    amount, 
    transaction_date, 
    mp_payment_type, 
    mp_payment_method, 
    mp_auth_code, 
    mp_paid_amount, 
    mp_card_number, 
    mp_id_paid, 
    mp_cuotas, 
    mp_valor_cuotas, 
    mp_transaction_date, 
    descuento, costo_instalacion, estado_id, 
    estados_pedido_id, medio_de_pago, email, id_envio","pedidos","estado_id IN (2,4,6,7) and oc = '$oc' and cliente_id = ".$_COOKIE['usuario_id'],"fecha_creacion desc");
    
    $medio_de_pago = ($pedidos[0][4] == 'webpay') ? 'tbk' : 'mp';
    $num_cuotas    = ($pedidos[0][4] == 'webpay') ? $pedidos[0][17] : $pedidos[0][26];
    $tipo_pago     = ($pedidos[0][4] == 'webpay') ? $pedidos[0][16] : $pedidos[0][20];
    $tipo_pago = tipo_pago($tipo_pago,$num_cuotas,$medio_de_pago);

    $fecha = ($pedidos[0][4] == 'webpay') ? date('d/m/Y', time($pedidos[0][19])) : date('d/m/Y', time($pedidos[0][28]));
        $hora_pago = ($pedidos[0][4] == 'webpay') ? date('H:s:i', time($pedidos[0][19])) : date('H:s:i', time($pedidos[0][28]));

    if($pedidos[0][31] == 4){
        $total_pagado = $pedidos[0][2];
    } else {
        if($pedidos[0][4] == 'webpay'){
            $total_pagado = $pedidos[0][18];
        } else if($pedidos[0][4] == 'mercadopago'){
            $total_pagado = $pedidos[0][23];
        } else {
            $total_pagado = $pedidos[0][2];
        } 
        //$total_pagado = ($pedidos[0][4] == 'webpay') ? $pedidos[0][18] : $pedidos[0][23];
    }
    
    
?>

<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li><a href="mis-pedidos">Mis Pedidos</a></li>
          <li class="active"><?= $oc; ?></li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            <div class="datosCliente">
                <div class="titulosCuenta">
                    <span>Detalle pedido: <?= $oc; ?></span>
                    <p>Podrás ver tus pedidos, historial de compra y editar tus datos personales y de envío.</p>
                </div>
                 <?php for($i=0; $i<sizeof($pedidos); $i++){ ?>
                    <div class="detallePedido">
                            <div class="encabezado">
                                <div class="ancho40">
                                    <span class="cont100"><strong>Fecha del pedido</strong></span>
                                    <span class="cont100 fechaPedido"><?= formato_fecha(substr($pedidos[$i][3], 0, 10), "esp"); ?></span>
                                </div>
                                <?php if($pedidos[0][31] == 4){ ?>
                                <div class="ancho25">
                                    <span class="cont100"><strong>Total por pagar</strong></span>
                                    <span class="cont100 fechaPedido">$<?= number_format($pedidos[$i][2],0,",",".");?></span>
                                </div>
                                <?php } else { ?>
                                <div class="ancho25">
                                    <span class="cont100"><strong>Total pagado</strong></span>
                                    <span class="cont100 fechaPedido">$<?= number_format($pedidos[$i][2],0,",",".");?></span>
                                </div>
                                <?php } ?>
                                <div class="floatRight ancho35">
                                    <span class="cont100"><strong>ORDEN: <?= $pedidos[$i][1]?></strong></span>
                                </div>
                            </div><!--fin encabezado -->

                            <!-- Datos pedido /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// ///////////////////-->
                            <div class="cont100 contDatosPago">
                                <div class="ancho50">
                                    <h3><?= $pedidos[0][5] ?></h3>
                                    <div class="cont100">
                                        <div class="cont100"><?= $pedidos[0][6]."<br />".$pedidos[0][7].", ".$pedidos[0][8]; ?></div>
                                    </div>
                                </div>

                                <div class="ancho50">
                                    <h3>Pago</h3>
                                    <div class="cont100">
                                        <div class="cont100"><span class="nvalor">Medio de pago:</span> <?= ucwords($pedidos[0][4]) ?></div>
                                        <?php if($pedidos[0][4] == "webpay"){ ?>
                                            <div class="cont100">
                                                <span class="nvalor">Tipo de transacción:</span> Venta
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">N° de Cuotas:</span> <?= $tipo_pago[cuota]; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Tipo de cuota:</span> <?= $tipo_pago[tipo_cuota]; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Tarjeta terminada en:</span>**** **** **** <?= $pedidos[0][13]; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Tipo de pago:</span> <?= $tipo_pago['tipo_pago']; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Fecha y hora de transacción:</span> <?= $fecha; ?> | <?= $hora_pago; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Valor pagado:</span> $<?= number_format($pedidos[0][18], 0, ',', '.'); ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Código de autorización:</span> <?= $pedidos[0][15]; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Tipo de moneda:</span>CLP / PESO Chileno
                                            </div>
                                        <?php } else { ?> 
                                            <div class="cont100">
                                                <span class="nvalor">Tipo de transacción:</span> Venta
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">N° de Cuotas:</span> <?= $tipo_pago[cuota]; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Tipo de cuota:</span> <?= $tipo_pago[tipo_cuota]; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Tarjeta terminada en:</span>**** **** **** <?= $pedidos[0][24]; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Tipo de pago:</span> <?= $tipo_pago['tipo_pago']; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Fecha y hora de transacción:</span> <?= $fecha; ?> | <?= $hora_pago; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Valor pagado:</span> $<?= number_format($pedidos[0][23], 0, ',', '.'); ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Código de autorización:</span> <?= $pedidos[0][22]; ?>
                                            </div>
                                            <div class="cont100">
                                                <span class="nvalor">Tipo de moneda:</span>CLP / PESO Chileno
                                            </div>
                                        <?php } ?>
                                </div>
                                <h3>Totales</h3>
                                <div class="cont100">
                                    <div class="cont50"><span class="nvalor"><strong>Subtotal</strong></span></div>
                                        <div class="cont50 textAlignRight">$<?= number_format($pedidos[0][9],0,",","."); ?></div>
                                    </div>
                                    <?php if($pedidos[0][29] > 0){?>
                                    <div class="cont100">
                                        <div class="cont50"><span class="nvalor"><strong>Descuentos</strong></span></div>
                                        <div class="cont50 textAlignRight">$<?= number_format($pedidos[0][29],0,",","."); ?></div>
                                    </div>
                                    <?php } ?>

                                    

                                    <div class="cont100">
                                        <div class="cont50"><span class="nvalor"><strong>Envio</strong></span></div>
                                        <div class="cont50 textAlignRight">$<?= number_format($pedidos[0][10],0,",","."); ?></div>
                                    </div>

                                   <?php if($pedidos[0][31] == 4){ ?>
                                        <div class="cont100">
                                            <div class="cont50"><span class="nvalor"><strong>Por cancelar</strong></span></div>
                                            <div class="cont50 textAlignRight totalDetallePedido">$<?= number_format($total_pagado,0,",","."); ?></div>
                                        </div>

                                    <?php } else { ?>
                                        <div class="cont100">
                                            <div class="cont50"><span class="nvalor"><strong>Total cancelado</strong></span></div>
                                            <div class="cont50 textAlignRight totalDetallePedido">$<?= number_format($total_pagado,0,",","."); ?></div>
                                        </div>
                                    <?php }?> 
                                    </div>
                            </div><!--Datos pedido /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// /////////////////// ///////////////////-->


                            <div class="cont100 contPedidosDetalle">
                                <div class="ancho70Pedido">
                                    <?php $estadoPedido = consulta_bd("nombre","estados_pedidos","id = ".$pedidos[$i][32],""); ?>
                                    <h3 class="subtituloPedidoGuardado">Medio de pago: <?= $pedidos[$i][33]?></h3>
                                    <!--<h3 class="tituloPedidoGuardado">Estado: <?= $estadoPedido[0][0]; ?></h3>-->

                                    <?php 
                                    $idPedido = $pedidos[$i][0];
                                    /*=================================================
                                    =            Datos para volver a pedir       =
                                    =================================================*/
                                    // Nota: Se envian separados por coma y en el proceso se de hacer el pedido se debe desglosar
                                    $idPedidoConsulta = $idPedido;
                                    $productosPedidos = consulta_bd("pedido_id, productos_detalle_id, cantidad","productos_pedidos","pedido_id = $idPedidoConsulta","");
                                    $aux = 0;
                                    foreach ($productosPedidos as $producto) {
                                        if ($aux == 0) {
                                            $prodId = $producto[1];
                                            $prodCant = $producto[2];
                                            $aux = 1;
                                        }else{
                                            $prodId .= ",".$producto[1];
                                            $prodCant .= ",".$producto[2];
                                        }
                                    }
                                    /*=====  End of Datos para volver a pedir===*/
                                    $detalles = consulta_bd("pd.id, p.nombre, pd.sku, pp.cantidad, pp.precio_unitario, p.thumbs, pd.descuento, pd.precio, pd.publicado, p.id","productos_detalles pd, productos_pedidos pp, productos p","pp.pedido_id = $idPedido and pp.productos_detalle_id = pd.id and pd.producto_id = p.id","");
                                    for($j=0; $j<sizeof($detalles); $j++){
                                        ?>
                                    <div class="cont100 filaProductoComprado">
                                        <a href="ficha/<?= $detalles[$j][9]; ?>/<?= url_amigables($detalles[$j][1]); ?>" target="_blank" class="thumbsProducto">
                                            <img src="<?= imagen("imagenes/productos/", $detalles[$j][5]); ?>" width="100%" />
                                        </a>
                                        <div class="infoProdDetalle">
                                            <span class="nombreProdDetalle"><strong><?= $detalles[$j][1]; ?></strong></span>
                                            <span>SKU: <?= $detalles[$j][2]; ?></span>
                                            <span>Cantidad: <?= $detalles[$j][3]; ?></span>
                                            <span class="azul"><strong>Valor pagado: $<?= number_format($detalles[$j][4],0,",","."); ?></strong></span>
                                            
                                        </div>
                                    </div><!--Fin producto -->
                                    <?php } ?>
                                </div>
                                <div class="contBotones">
                                    <a class="btnVolverAPedir" idProductos="<?= $prodId ?>" cantProductos = "<?= $prodCant ?>" href="javascript:void(0)">Volver a comprar</a>
                                    <?php if ($pedidos[0][5] == 0 and $pedidos[0][35] != ""){?>
                                    <a class="btnPedidos" href="https://api.enviame.io/s2/companies/2940/deliveries/<?= $pedidos[0][35]; ?>/tracking" data-fancybox data-type="iframe" >Seguimiento despacho</a>
                                    <?php } ?>
                                    
                                </div>
                            </div>
                        </div>
                    <?php } ?>
            <!--fin cont100 -->
            
            </div><!--fin datosCliente -->
            
            <div class="tipsMiCuenta">
                <h3>Necesitas ayuda</h3>
                <p>¡Escríbenos un whatsapp! estaremos para ayudarte ante cualquier duda.</p>
                
                <a href="whatsapp://send?phone=+56979674400" class="whatsAppCuenta">
                    <span class="iconoWhatsApp"><i class="fab fa-whatsapp"></i></span>
                    <span class="numeroWhatsApp">+56 9 7967 4400</span>
                </a>
                
                <h3>Horario de atención</h3>
                <p>Lunes a Viernes de 09:00 a 21:00<br>
                    Sábado y Domingo de 10:00 a 17:00</p>
            </div><!--fin tipsMiCuenta -->
        
       </div><!--fin contenidoMiCuenta-->
    
    </div>
</div>
