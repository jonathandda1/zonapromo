<?php 
require_once 'paginador/paginator.class.php';

    $page = (isset($_GET['page'])) ? mysqli_real_escape_string($conexion, $_GET['page']) : 0;
	$ipp = (isset($_GET['ipp'])) ? mysqli_real_escape_string($conexion, $_GET['ipp']) : 4;
	
	
    
    $marca = (is_numeric($_GET['marca'])) ? mysqli_real_escape_string($conexion, $_GET['marca']) : 0;
    $oferta = (is_numeric($_GET['oferta'])) ? mysqli_real_escape_string($conexion, $_GET['oferta']) : 0;
	$orden = (isset($_GET['orden'])) ? mysqli_real_escape_string($conexion, $_GET['orden']) : 0;
	






/*filtros personalizados*/
$variablesGetExcluidas = array("page", "ipp", "marca", "orden", "op", "oferta");
$valoresTodos = "";
$contValoresTodos = 1;
foreach($_GET as $key=>$val){
    
    if(!in_array($key, $variablesGetExcluidas)){
        // and is_numeric($val)
        if($val != ""){
            if($contValoresTodos == 1){
            $valoresTodos .= $val;
            }else {
                $valoresTodos .= "-".$val;
            }
            $contValoresTodos = $contValoresTodos + 1;
        }
        
        
    }
}
$valoresTodos = explode("-", $valoresTodos);
$valoresTodosFinales = "";
$c2 = 1;
foreach($valoresTodos as $val){
    if($c2 == 1){
        if($val != "" and is_numeric($val)){
            $valoresTodosFinales .= $val;
            $c2 = $c2 + 1;
        }
    } else {
        if($val != "" and is_numeric($val)){
            $valoresTodosFinales .= ",".$val;
            $c2 = $c2 + 1;
        }
    }
    
}

if($valoresTodosFinales != ""){
    $whereFiltros = " and p.id = fp.producto_id and fp.valores_filtro_id IN($valoresTodosFinales)";
    $tablasFiltro = ", valores_filtros vf, filtros_productos fp";
} else {
    $whereFiltros = "";
    $tablasFiltro = "";
}

/*lo ocupo para completar las url de los filtros que pasan por htaccess*/
$urlNeutra = $_SERVER['REQUEST_URI'];
$urlArregloParaMarcas = explode("?",$urlNeutra);
if($urlArregloParaMarcas[1] != ""){
    $varSiguientes = "?".$urlArregloParaMarcas[1];
} else {
    $varSiguientes = "";
}
/*lo ocupo para completar las url de los filtros que pasan por htaccess*/
/*fin  filtros personalizados*/






	if($marca != 0){
		$whereMarca = " and p.marca_id = $marca";
        $nc = consulta_bd("nombre","marcas","id = $marca","");
        $nombreMarca = "Marca: ".$nc[0][0];
	} else {
		$whereMarca;
        $nombreMarca = "Marca";
	}
    
    
  


   if($oferta == 1){
		$whereOferta = " and pd.descuento > 0 and pd.descuento < pd.precio";
	} else {
		$whereOferta;
	}
	
    $whereNuevosProductos = " and p.producto_nuevo = 1";
        
        
        
	if($orden === "valor-desc"){
		$orderSql = ' valorMenor desc';
		$nombreOrden = "Mayor precio";
	} else if($orden === "valor-asc"){
		$orderSql = ' valorMenor asc';
		$nombreOrden = "Menor precio";
	} else {
		$orderSql = " p.fecha_creacion desc";
		$nombreOrden = "Más relevantes";
	}


/*consulta para mostrar filtros dinamicos segun productos existentes*/
$productosFiltros = consulta_bd("p.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor", "productos p, productos_detalles pd, marcas m", "p.id = pd.producto_id AND p.marca_id = m.id AND p.publicado = 1  $whereOferta $whereNuevosProductos group by p.id ", "$orderSql");
/*consulta para mostrar filtros dinamicos segun productos existentes*/



$productosPaginador = consulta_bd("p.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor", "productos p, productos_detalles pd, marcas m $tablasFiltro", "p.id = pd.producto_id AND p.marca_id = m.id AND p.publicado = 1  $whereMarca $whereOferta $whereFiltros $whereNuevosProductos group by p.id ", "$orderSql");
$total = mysqli_affected_rows($conexion);

    $pages = new Paginator;
    $pages->items_total = $total;
    $pages->mid_range = 7; 
	$rutaRetorno = "nuevos-productos/$marca/$orden/$oferta";
    $pages->paginate($rutaRetorno);
    

$productos = consulta_bd("p.id, p.nombre, p.thumbs, p.marca_id, p.id, MIN(if(pd.descuento > 0, pd.descuento, pd.precio)) as valorMenor, pd.id, m.nombre", "productos p, productos_detalles pd, marcas m $tablasFiltro", "p.id = pd.producto_id AND p.marca_id = m.id AND p.publicado = 1 $whereOferta $whereMarca $whereFiltros $whereNuevosProductos group by p.id", "$orderSql $pages->limit");
$cant_productos = mysqli_affected_rows($conexion);   





?>


  <div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Nuevos productos</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->


<div class="cont100 fondoNuevosProductos">
    <div class="cont100Centro">
        <h1 class="nombreNuevos">Nuevos productos</h1>
    </div><!--Fin cont100Centro -->
</div>

<div class="cont100">
    <div class="cont100Centro">
        <!--
            <h1 class="nomCategoria">Nuevos Productos<span class="cantProdCat"> &nbsp;&nbsp;&nbsp;&nbsp;</h1>
        -->

        <div class="contFIltrosMovil">
            <div class="filtrosOrden">
            <div class="contenedorFiltro">
                <span class="contNomFiltroActual">
                    <span><i class="fas fa-chevron-down"></i></span>
                    <span class="filtroActual"><?= $nombreOrden ?></span>
                </span>
                <ul>
                    <li class="<?php if($orden === 0){ echo 'filtroSeleccionado';} ?>">
                        <a href="nuevos-productos">Ordenar por relevancia</a>
                    </li>
                    <li class="<?php if($orden === "valor-desc"){ echo 'filtroSeleccionado';} ?>">
                        <a href="nuevos-productos<?= "/".$marca."/valor-desc/$oferta".$varSiguientes; ?>">Precio Mayor a Menor</a>
                    </li>
                    <li class="<?php if($orden === "valor-asc"){ echo 'filtroSeleccionado';} ?>">
                        <a href="nuevos-productos<?= "/$marca/valor-asc/$oferta".$varSiguientes; ?>">Precio Menor a Mayor</a>
                    </li>
                </ul>
            </div><!--Fin contenedorFiltro -->
        </div><!--fin filtros-->



            <a href="javascript:void(0)" class="btnFiltrosMovil">
                Filtros <i class="material-icons">keyboard_arrow_down</i>
            </a>
        </div><!--fin contFIltrosMovil -->

    </div>
</div>


<div class="cont100">
    <div class="cont100Centro">


         <?php 
        $pm = consulta_bd("p.marca_id", "productos p, productos_detalles pd, marcas m", "p.id = pd.producto_id AND p.marca_id = m.id AND p.publicado = 1 group by p.id", "");
        /*arreglo marcas activas*/
        $idsMarcas = array();
        for($i=0; $i<sizeof($pm); $i++) {
            if (in_array($pm[$i][0], $idsMarcas)) {
                //echo "Existe mac";
            } else {
                array_push($idsMarcas, $pm[$i][0]);
            }
        }
        $contador = 0;
        $idsMarcas2 = "";
        foreach ($idsMarcas as $valor) {
            if($contador == 0){
                $idsMarcas2 .= $valor;
            } else {
               $idsMarcas2 .= " ,".$valor; 
            }

            $contador = $contador + 1;
        }
        /*Fin arreglo marcas activas*/
        if($idsMarcas2 != ""){
            $marcas = consulta_bd("id, nombre","marcas","id > 0 and id IN($idsMarcas2)","id asc");    
        }
        ?> 

    <div class="fondoLateralFiltro">
        <div class="lateralFiltro">
            <!--filtros escritorio-->
            <div class="filtrosDesktop">



                 <!--marcas-->
                 <div class="filtros">
                    <div class="contenedorFiltroLateral">
                        <span class="tituloFiltro">Marcas</span>
                        <ul class="listadoMarcas">
                            <?php for($i=0; $i<sizeof($marcas); $i++) { 

                            $classActiva = "";
                                if($marca == $marcas[$i][0]){ 
                                    $classActiva = 'activo';
                                    $urlMarcas = "nuevos-productos/0/$orden/$oferta".$varSiguientes;
                                } else {
                                    $classActiva = '';
                                    $urlMarcas = "nuevos-productos/".$marcas[$i][0]."/$orden/$oferta".$varSiguientes;
                                }
                            ?> 
                            <li class="">
                                <a class="<?= $classActiva; ?>" href="<?= $urlMarcas; ?>"><?= $marcas[$i][1]?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div><!--Fin contenedorFiltroLateral -->
                </div><!--fin filtros-->




                <!--filtros personalizados-->
              <?php   
                    $idsProd = array();
                    for($i=0; $i<sizeof($productosFiltros); $i++) {
                        if (in_array($productosFiltros[$i][0], $idsProd)) {
                            //echo "Existe";
                        } else {
                            array_push($idsProd, $productosFiltros[$i][0]);
                        }
                    }
                    $contadorProd = 0;
                    $idsProd2 = "";
                    foreach ($idsProd as $valor) {
                        if($contadorProd == 0){
                            $idsProd2 .= $valor;
                        } else {
                           $idsProd2 .= " ,".$valor; 
                        }

                        $contadorProd = $contadorProd + 1;
                    }
                    /*Fin arreglo marcas activas*/

                    if($idsProd2 != ""){
                        $filtros = consulta_bd("distinct(f.id), f.nombre","filtros f, filtros_productos fp","f.id = fp.filtro_id and fp.producto_id IN($idsProd2)","");
                    }
                ?>                                        
                <?php for($i=0; $i<sizeof($filtros); $i++) { ?>                 
                <!--filtros personalizados-->
                 <div class="filtros">
                    <div class="contenedorFiltroLateral">
                        <span class="tituloFiltro"><?= $filtros[$i][1]; ?></span>
                        <ul class="listadoMarcas">
                            <?php 
                            $valoresFiltros = consulta_bd("distinct(vf.id), vf.nombre","valores_filtros vf, filtros_productos fp","vf.id = fp.valores_filtro_id and fp.valores_filtro_id > 0 and fp.filtro_id = ".$filtros[$i][0]." and fp.producto_id IN($idsProd2)","");
                            for($j=0; $j<sizeof($valoresFiltros); $j++) {
                                $conjFiltro = "filtro".$filtros[$i][0];

                                //echo "<h1>".$conjFiltro."</h1>";

                                if(isset($_GET[$conjFiltro]) and $_GET[$conjFiltro] != 0){
                                    //existe y debo modificarla
                                    $activo = 0;
                                    $urlFiltro = "nuevos-productos/$marca/$orden/$oferta?";


                                    $contGral = 1;
                                    foreach($_GET as $key=>$val){
                                        /*diferencio de las variables que vienen por htaccess*/
                                        if(!in_array($key, $variablesGetExcluidas)){

                                            if($key == $conjFiltro){
                                                if($val != 0 and $val > 0){
                                                    $valoresFiltroActual = explode("-",$val);
                                                    if(in_array($valoresFiltros[$j][0], $valoresFiltroActual)){
                                                        //ya existe
                                                        $valorAConsultar = $valoresFiltros[$j][0];
                                                        $clave = array_search("$valorAConsultar", $valoresFiltroActual);
                                                        unset($valoresFiltroActual["$clave"]);
                                                        $activo = 1;
                                                    } else {
                                                        array_push($valoresFiltroActual, $valoresFiltros[$j][0]);
                                                        //lo agrego a la cadena
                                                        $activo = 0;
                                                    }
                                                    $valoresFiltroActual2 = "";
                                                    $c = 1;
                                                    foreach($valoresFiltroActual as $key2=>$val2){
                                                        if($c == 1){
                                                            $valoresFiltroActual2 .= "$val2";
                                                        } else {
                                                            $valoresFiltroActual2 .= "-$val2";
                                                        }
                                                        $c = $c + 1;
                                                    }
                                                    if($contGral == 1){
                                                        $urlFiltro .= "$key=$valoresFiltroActual2";
                                                    } else {
                                                        $urlFiltro .= "&$key=$valoresFiltroActual2";
                                                    }
                                                    $contGral = $contGral + 1;
                                                } else {/*no hago nada*/}
                                            } else {
                                                /*no hago nada*/
                                                if($contGral == 1){
                                                    $urlFiltro .= "$key=$val";
                                                } else {
                                                    $urlFiltro .= "&$key=$val";
                                                }
                                                $contGral = $contGral + 1;
                                            }
                                        }
                                        /*diferencio de las variables que vienen por htaccess*/

                                    }
                                    //fin existe y debo modificar
                                } else {
                                    //no existe y debo agregarla
                                    $urlNeutra = $_SERVER['REQUEST_URI'];
                                    $urlArreglo = explode("?",$urlNeutra);
                                    if($urlArreglo[1] == ""){
                                        $urlFiltro = $_SERVER['REQUEST_URI']."?$conjFiltro=".$valoresFiltros[$j][0];
                                    } else {
                                        $urlFiltro = $_SERVER['REQUEST_URI']."&$conjFiltro=".$valoresFiltros[$j][0];
                                    }

                                }


                            ?> 
                            <li class="">
                                <a class="<?php if($activo == 1){ echo 'activo';} ?>" href="<?= $urlFiltro; ?>"><?= $valoresFiltros[$j][1]?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div><!--Fin contenedorFiltroLateral -->
                </div><!--fin filtros personalizados-->
                <?php } ?> 




                <a href="nuevos-productos/<?= "$marca/$orden/$oferta".$varSiguientes?>" class="ofertasLateral">ver solo Ofertas</a>
                <a href='nuevos-productos' class="ofertasLateral">Quitar filtros</a>



            </div>
            <!--fin filtros escritorio-->

        </div><!--Fin lateral Filtro-->
    </div><!--fin fondoLateralFiltro -->


       <!--Contenedor grillas -->
       <div class="contGrillasProductos">


        <?php for($i=0; $i<sizeof($productos); $i++){ ?>

              <div class="grilla">
                    <a href="#" class="like" rel="<?= $productos[$i][6]; ?>">
                        <i class="<?= (guardadoParaDespues($productos[$i][6])) ? 'fas':'far';?> fa-heart"></i>
                    </a>
                    <?php if(ultimasUnidades($productos[$i][6])){ ?>
                    <div class="etq-ultimas">ultimas unidades</div>
                    <?php } ?>


                    <?php if ($is_cyber AND is_cyber_product($productos[$i][0])): ?>
                        <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                    <?php endif ?>

                    <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="imgGrilla">
                        <?= porcentajeDescuento($productos[$i][0]); ?>
                        <img src="<?= imagen("imagenes/productos/", $productos[$i][2]);?>" width="100%">
                    </a>
                    <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="marcaGrilla"><?= nombreMarca($productos[$i][0]);?></a>

                    <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="nombreGrilla"><?= $productos[$i][1]; ?></a>
                    <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="valorGrilla">
                    <?php if ($is_cyber AND is_cyber_product($productos[$i][0])):
                        $precios = get_cyber_price($productos[$i][6]); ?>
                            <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                            <span class="antes">Antes: $<?= number_format($precios['precio'],0,",",".") ?></span>

                    <?php else: ?>
                        <?php if(tieneDescuento($productos[$i][6])){ ?>
                            <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>    
                            <span class="antes">Antes: $<?= number_format(getPrecioNormal($productos[$i][6]),0,",",".") ?></span>

                        <?php }else{ ?>
                            <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                            <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>

                        <?php } ?>
                    <?php endif ?>
                    </a>

                    <a class="btnFicha" href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>">Ver ficha</a>
                </div><!--Fin Grilla -->

        <?php } ?>

           <div class="cont100 paginadorGrid">
                <div class="paginador">
                    <?= $pages->display_pages(); ?>
                </div>
            </div><!--Fin paginador -->
    </div>
<!--Fin contenedor grillas -->



    </div> <!--fin cont centro-->
</div><!--Fin contGeneral -->





	
    
    
    
    

