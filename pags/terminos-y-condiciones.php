    
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Términos y condiciones</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->

<div class="cont100">
    <div class="cont100Centro">
        <h1 class="tituloServicios">Términos y condiciones</h1>        
        <section class="contTerminos">
            <article class="contTextosFijosMenu">
                <h5>Indice</h5>
                <?php if ($is_cyber): ?>
                <a href="terminos-y-condiciones#puntoCyber">Cybermonday.</a>
                <?php endif ?>
                
                <a href="terminos-y-condiciones#punto1">1. Información general</a>
                <a href="terminos-y-condiciones#punto2">2. Antecedentes generales</a>
                <a href="terminos-y-condiciones#punto3">3. Contenido del sitio</a>
                <a href="terminos-y-condiciones#punto4">4. Modificaciones al servicio y precios</a>
                <a href="terminos-y-condiciones#punto5">5. Productos o servicios</a>
                <a href="terminos-y-condiciones#punto6">6. Fallas que NO aplican garantía y/o exclusiones generales</a>
                <a href="terminos-y-condiciones#punto7">7. Políticas de compra</a>
                <a href="terminos-y-condiciones#punto8">8. Ofertas y promociones</a>
                <a href="terminos-y-condiciones#punto9">9. Forma de pago</a>            
                <a href="terminos-y-condiciones#punto10">10. Entrega y despacho de los productos </a>
                <a href="terminos-y-condiciones#punto11">11. Garantía</a>
                <a href="terminos-y-condiciones#punto12">12. Cambios y devoluciones</a>
            </article>
            
            <article class="contTextosFijos">
                
                <?php if ($is_cyber): ?>
                <h4 id="puntoCyber">Cybermonday 2020</h4>
                <p>Para descargar el listado de productos asociados al cybermonday 2019 has click <a style="color:#009; font-weight:700;" href="<?= $url_base; ?>listadoCyber.xlsx" target="_blank">aqui</a></p>
                <p></p>
                <?php endif ?>
                
                <h4 id="punto1">1. Información general</h4>
                <p>Gracias por visitar nuestro sitio web. Por favor lea con cuidado los Términos y Condiciones de Uso contenidos en este documento, ya que cualquier uso de este sitio web constituye tu aceptación a los mismos.</p>

                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                <h4 id="punto2">2. Antecedentes generales</h4>
                <p>Este sitio web es operado y de propiedad de ZONA PROMO SPA. El acceso a es gratuito y solo está condicionado a la aceptación y cumplimiento de estos Términos y Condiciones de Uso. Zona Promo ofrece este sitio web, incluyendo toda la información, herramientas y servicios disponibles para ti, el usuario está condicionado a la aceptación de todos los términos, condiciones, políticas y notificaciones aquí establecidas.</p>

                <p>Al visitar nuestro sitio y/o comprar algo de nosotros, participas en nuestro “Servicio” y aceptas los siguientes términos y condiciones, incluidos todos los términos y condiciones adicionales y las políticas a las que se hace referencia en el presente documentos y/o disponible a través de hipervínculos. Estas Condiciones de Servicio se aplican a todos los usuarios del sitio, incluyendo sin limitación a usuarios que sean navegadores, proveedores, clientes, comerciantes, y/o colaboradores de contenido.</p>

                <p>Por favor lea estos Términos de Servicio cuidadosamente antes de acceder o utilizar nuestro sitio web. Al acceder o utilizar cualquier parte del sitio, estás aceptando los Términos de Servicio. Si no estás de acuerdo con todos los términos y condiciones de este acuerdo, entonces sugerimos no acceder a la página web ni usar cualquiera de los servicios.  Si los Términos de Servicio son considerados una oferta, la aceptación está expresamente limitada a estos Términos de Servicio.</p>

                <p>Cualquier función nueva o herramienta que se añadan a la tienda actual, también estarán sujetas a los Términos de Servicio Puedes revisar la versión actualizada de estos términos, en cualquier momento en esta página. Nos reservamos el derecho de actualizar, cambiar o reemplazar cualquier parte de los términos de Servicio mediante la ubicación de actualizaciones y/o cambios en nuestro sitio web. Es tú responsabilidad chequear esta página periódicamente para verificar cambios. Tú uso continuo o el acceso al sitio web después de la publicación de cualquier cambio constituye la aceptación de dichos cambios.</p>
                
                <p>Al utilizar este sitio, declaras que tienes al menos la mayoría de edad en tu estado o provincia de residencia, o que tienes la mayoría de edad en tu estado o provincia de residencia y que nos has dado tu consentimiento para permitir que cualquiera de tus dependientes menores use este sitio.</p>

                <p>Cualquier compra o transacción que se efectúe en forma no presencial, vía electrónica, Online, portal web, desde otro sitio, o telefónica, no se podrá ejercer derecho de retracto de parte del cliente o consumidor, en conformidad al Art. 3º bis, letra b, Ley Nº 19496.  En estos casos, la empresa no estará obligada ni aceptará cambiar el producto por no cumplir con las expectativas personal del cliente o ser de su agrado, una vez adquirido éste.</p>

                <p>No se admitirán devoluciones de productos, equipos, accesorios, ni suministros, los cuales sólo en caso de presentar fallas de fábrica podrán ser cambiados por uno idéntico, conforme con su garantía.</p>
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                <h4 id="punto3">3. Contenido del sitio</h4>

                <p>Los productos y servicios, las funcionalidades y las promociones que ZONA PROMO describe en este sitio están ofrecidas únicamente para el territorio nacional de la República de Chile.</p>

                <p>El material de este sitio es provisto solo para información referencia o general y no debe confiarse en ella o en sus fotos ni utilizarse éstas como la única base para la toma de decisiones sin consultar o requerir, primeramente, información más precisa, completa u oportuna en caso de dudas. Cualquier dependencia en la materia de este sitio es bajo su propio riesgo. </p>

                <p>Este sitio puede contener cierta información histórica. La información histórica, no es necesariamente actual y es provista únicamente par tu referencia. Nos reservamos el derecho de modificar los contenidos de este sitio en cualquier momento, pero no tenemos obligación de actualizar cualquier información en nuestro sitio. Aceptas que es tu responsabilidad de monitorear los cambios en la página.</p>

                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                
                <h4 id="punto4">4. Modificaciones al servicio y precios</h4>
                <p>Los precios de nuestros productos pueden estar sujetos a cambio sin previo aviso.
                Nos reservamos el derecho de modificar o discontinuar el Servicio (o cualquier parte del contenido) en cualquier momento, ni la compañía será responsable por cualquier eventual modificación al contenido de la web, cambio de precio, suspensión o discontinuidad del Servicio.</p>

                <p>Los precios y productos publicados en sitio venta web son válidos y sujetos solo para ventas no presenciales en sitio web. Todos los productos, precios, ofertas y promociones publicados en el portal web, son exclusivos de la Web y no necesariamente se encontrarán disponibles en los locales o tiendas.</p>
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                
                <h4 id="punto5">5. Productos o servicios</h4>
                <p>Ciertos productos pueden estar disponibles exclusivamente en línea a través del sitio web; pueden tener cantidades limitadas y estar sujetas a devolución o cambio de acuerdo a nuestra política de devolución solamente. Cada producto tiene un pedido mínimo indicado en el sitio.</p>

                <p>Hemos hecho el esfuerzo de mostrar los colores y las imágenes de nuestros productos con la mayor precisión de colores posibles.  No podemos garantizar que el monitor de su computadora muestre los colores de manera exacta.</p>

                <p>Nos reservamos el derecho, pero no estamos obligados para limitar las ventas de nuestros productos o servicios a cualquier persona, región geográfica o jurisdicción. Podemos ejercer este derecho basados en cada caso. Nos reservamos el derecho de limitar las cantidades de los productos o servicios que ofrecemos. Todas las descripciones de productos o precios de los mismos, están sujetos a cambios en cualquier momento sin previo aviso, a nuestra sola discreción.  Nos reservamos el derecho de discontinuar cualquier producto en cualquier momento.</p>

                <p>No garantizamos que la calidad de los productos, información u otro material comprado u obtenido por usted cumpla con tus expectativas, o que cualquier error en el Servicio será corregido.</p>
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                
                <h4 id="punto6">6. Fallas que NO aplican garantía y/o exclusiones generales</h4>
                <p>En general al aceptar las presentes condiciones de compra no cubre los daños ocasionados al producto por un hecho imputable al consumidor, la instalación no apropiada a la red de energía o reparación no autorizada por descuido, maltratos, mal uso o abuso, falta de mantenimiento indicada por el fabricante posteriores la entrega, daños derivados del transporte después de la compra, desconocimiento y/o desobediencia a las instrucciones de uso recomendados por el fabricante. En caso que el artefacto o producto haya sido dañado sin responsabilidad de ZONA PROMO.  Asimismo, la empresa no cubrirá daños producidos por robo, vandalismo, terremoto, congelamiento, oxidación corrosión, contaminación de fluidos, líquidos o combustibles, y cualquier situación de fuerza mayor o caso fortuito.</p>

                <p>Las imágenes ligadas a los productos son de carácter referencial y no representan necesariamente un factor decidor al momento de su compra.</p>
                
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                <h4 id="punto7">7. Políticas de compra</h4>
                <p>Cómo comprar online:  ZONA PROMO ofrece la posibilidad de cotizar en línea para lo cual deberá iniciar sesión www.zonapromo.cl, seleccionar el o los productos que se quiere cotizar y agregarlos al carro de cotización y seguir las instrucciones. Luego de recibir la cotización puede finalizar el pedido con su ejecutiva de ventas.</p>

                <p>Cómo comprar telefónicamente: ZONA PROMO ofrece la posibilidad de cotizar por teléfono llamando al +56 2 22193484. Para estos efectos, se solicitará el Rut y otros datos del cliente, se revisará la existencia de stock y se preguntará al cliente si necesita cotización formal.</p>

                <p>Respaldo: Toda compra realizada en ZONA PROMO, estará debidamente documentada  con orden de compra y pago asociado y, formalizada con la entrega del producto de compra.</p>

                <p>Stock: Las compras están sujetas a la disponibilidad de stock publicado para cada producto en la Web.</p>

                <p>Formación del Consentimiento y Validación de la compra: La validación de la compra comprenderá una revisión de los siguientes elementos: a) Disponibilidad de los fondos en la cuenta corriente de ZONA PROMO y/o validación por la entidad que corresponda según sea el medio de pago utilizado por el cliente; b) Verificación de los datos personales suministrados por el cliente a ZONA PROMO.</p>

                <p>En caso de ventas online, la validación será informada al cliente mediante la factura respectiva, la que será envida a la dirección de correo electrónico que haya registrado el cliente, o por cualquier medio de comunicación que garantice el debido y oportuno conocimiento del cliente, el que se le indicará previamente.</p>

                <p>Como una mediad de protección a la seguridad de las transacciones, ZONA PROMO, podrá dejar sin efecto la compra en los siguientes casos:</p>

                <p>1)	Cuando producto del proceso de validación resulte que uno o más de los datos suministrados por el cliente son erróneos, incompletos o no coinciden con los registros informados por el cliente previamente.</p>

                <p>2)	Cuando no se hubiese podido contactar al cliente dentro de las 72 horas siguientes a la compra. Las transacciones realizadas están sujetas a una validación adicional efectuada por un tercero que podría sumar hasta 48 horas hábiles al proceso habitual de validación.</p>
                
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                
                
                <h4 id="punto8">8. Ofertas y promociones</h4>
                <p>.ZONA PROMO podrá modificar las informaciones dadas en el sitio web www.zonapromo.cl, incluyendo las referidas a productos, servicios, precios, disponibilidad de stock y condiciones; en cualquier momento y sin previo aviso, hasta recibir una aceptación del cliente. Esto, sin perjuicio de ofertas para las cuales se haya ofrecido un plazo y/o stock determinado, en cuyo caso ZONA PROMO cumplirá las condiciones ofrecidas.</p>
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                
                <h4 id="punto9">9. Forma de pago</h4>
                <p>Las compras realizadas a través de nuestra página web, se pagarán mediante transferencia bancaria y una vez que los fondos estén disponibles en la cuenta corriente de ZONA PROMO, se enviará una confirmación escrita al cliente con el documento de venta asociado y el hecho da cuenta de la formación de consentimiento; o bien si es cliente que cuenta con aprobación se puede gestionar con crédito a 30 días, previa evaluación de cliente y con Orden de Compra.</p>
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                
                <h4 id="punto10">10. Entrega y despacho de los productos </h4>
                <p>Cada producto tiene su tiempo de entrega a definir según stock, producción de logo y envío.  Cada ejecutiva entregará los plazos de entrega requeridos en cada producción de productos en stock o bien de plazos de productos que requieran importación directa.</p>
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                
                <h4 id="punto11">11. Garantía</h4>
                <p>Productos nuevos: Todo producto nuevo cuenta con garantía legal los tres primeros meses.</p>
                <p>Garantía Legal conforme lo dispone la Ley del Consumidor, todo producto nuevo cuenta con garantía legal la cual rige para los primeros tres meses desde la recepción del mismo y procede sólo en casos de fallas de fabricación o cuando el producto no cumpla con las características ofrecidas.</p>

                <p> Productos de segunda selección o liquidación: Conforme a la Ley, estos productos no tienen garantía legal y sólo tendrán garantía de funcionamiento indicado expresamente al momento de la compra. Estos productos son debidamente informados y publicados como de segunda selección.</p>

                <p>Plazos: Los plazos de garantía legal o del fabricante (si fuese el caso), se cuentan desde la fecha de compra del producto. Lo anterior, a menos que el despacho sea en forma diferida y en dicho caso el plazo se cuenta desde la recepción del producto.</p>

                <p>Gastos: Los gastos o costos de despacho en que incurra el cliente para hacer efectiva una garantía, será de su propio cargo.  En el caso de garantía legal (3 meses) de un producto comprado vía internet y con despacho a domicilio y sí efectivamente presenta una falla cubierta por la garantía, se cubrirá sólo el costo derivado del traslado del producto dentro del territorio nacional Cualquier otro gasto o costo en que incurra el cliente para hacer efectiva una garantía será de su propio cargo (daño emergente, lucro cesante, u otros).</p>

                <p>Productos adquiridos por promoción: En caso de que por cualquier causa se proceda a la devolución de dinero por un producto y junto con éste, el cliente hubiere adquirido onerosa o gratuitamente otro producto por una promoción asociad, no se procederá a efectuar el cambio o devolución a menos que también se devuelva el producto comprado o recibido en virtud de esa promoción. El producto deberá devolverse nuevo, sellado, sin uso y en perfecto estado el que será constatado por ZONA PROMO. En caso de que el cliente no tenga el producto adquirido en promoción o éste se encuentre usado o abierto, deberá pagar el precio del mismo antes de obtener la devolución del producto principal.</p>

                <p>Garantías Legales:</p>

                <p>En caso de que el producto no cuente con las características técnicas informadas, si estuviera dañado o incompleto, podrá ser cambiado previa revisión por parte de ZONA PROMO.  Si presentara fallas o defectos dentro de los 3 meses siguientes a la fecha en que éste fue recibido, puede optarse a restitución, cambio o devolución de la cantidad pagada, siempre que el producto no se hubiese deteriorado por un hecho imputable al consumidor.</p>
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
                
                
                
                <h4 id="punto12">12. Cambios y devoluciones</h4>
                <p>Si el producto comprado presenta fallas o no cuenta con las características técnicas informadas, este puede ser cambiado hasta 10 días después de la fecha en que este haya sido recibido, previa revisión por parte de ZONA PROMO, a fin de verificar el motivo de la falla, ya sea por uso incorrecto del artículo o desperfecto de fábrica. Posterior a esto puede acudir a oficinas de la empresa, o si fue despachado, la empresa puede ir a buscarlo al mismo lugar, pagando sólo el valor del transporte que será el mismo valor de el de despacho.</p>

                <p>Para poder hacer cambio o devolución es necesario que el producto esté sin uso, con todos sus accesorios y embalajes originales.</p>

                <p>Para cambio o devolución deberá presentarse la boleta, guía de despacho, ticket de cambio, u otro comprobante de compra.</p>
                
                <p>En caso de devolución de dinero la empresa realizará un abono en el medio de pago que haya utilizado en un período no superior a 72 horas de haberse aceptado la devolución, cuestión que será informada a través del correo electrónico que se hubiere registrado.</p>
                <p><a href="terminos-y-condiciones#punto1" class="subirTerminos">Subir</a></p>
            </article>
        </section>
    </div>
</div>
