
<?php $sucursales = consulta_bd("s.id, s.nombre, s.direccion, s.comuna_id, s.link_mapa, c.nombre, s.imagen, s.horarios","sucursales s, comunas c","s.publicado = 1 and s.destacada = 1 and s.comuna_id = c.id","s.posicion asc"); ?> 
<div class="cont100 sucursalesInterior">
    <div class="cont100Centro">
        <h3 class="tituloDestacadoHome">Nuestras sucursales</h3>
        <div class="cont100">
        
            <?php for($i=0; $i<sizeof($sucursales); $i++) {?> 
            <div class="grillaSucursalesInterior">
                <div class="imgSucursalInterior">
                    <img src="imagenes/sucursales/<?= $sucursales[$i][6]; ?>">
                </div>
                <div class="datosGrillaSucursalInterior">
                    <div class="nombreDireccion">
                        <?= $sucursales[$i][1]; ?>
                    </div>
                    <div class="direccionSucursalGrilla">
                        <i class="fas fa-map-marker-alt"></i> <span><?= $sucursales[$i][2]; ?>, <?= $sucursales[$i][5]; ?></span>
                    </div>
                    
                    <?php if($sucursales[$i][7] != ""){ ?> 
                    <div class="direccionSucursalGrilla">
                        Horarios:<br>
                        <?= $sucursales[$i][7]; ?>
                    </div>
                    <?php }?> 
                    
                    <?php if($sucursales[$i][4] != ""){?> 
                    <a href="javascript:void(0)" data-fancybox data-type="iframe" data-src="pags/mapaSucursales.php?id=<?= $sucursales[$i][0]; ?>" class="btnGrillaSucursal">LINK EN GOOGLE MAPS</a>
                    <?php } ?>
                    
                </div>
                
            </div><!--fin grillaSucursales -->
            <?php } ?> 
        </div>
    </div>
</div>