
 <div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Formas de pago</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->



<div class="cont100">
    <div class="contCentroPagFijas">
        <div class="tituloContacto">Formas de pago</div>
        <div class="contTxtParrafo">
            
            <p><strong>1-</strong> Tarjetas de crédito (Visa, Master Card, Magna, Diners Club, American Express) o débito (Red Compra) a través de Web Pay. El portal Web Pay de pago es un servicio seguro y confidencial de Transbank. Los datos de tú tarjeta sólo serán enviados a Transbank, nosotros no tenemos acceso a estos.</p>

            <p><strong>2-</strong> Cupones de Descuento: Luego de haber seleccionado el/los productos que quieres comprar y haberlo agregado al carro de compras te encontrarás con el resumen de tú compra. Es en este momento deberás activar tú código de descuento ingresando el código en el espacio indicado. El descuento se aplica sobre precios vigentes por lo que una vez usados, el precio queda reflejado inmediatamente en el total.</p>
            <p></p>

            
            
            
        </div>
    </div>
</div>