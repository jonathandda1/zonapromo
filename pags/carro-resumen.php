<?php 

$email = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : 0;
$nombre = (isset($_POST['nombre'])) ? mysqli_real_escape_string($conexion, $_POST['nombre']) : 0;
$telefono = (isset($_POST['telefono'])) ? mysqli_real_escape_string($conexion, $_POST['telefono']) : 0;
$rut = (isset($_POST['rut'])) ? mysqli_real_escape_string($conexion, $_POST['rut']) : 0;

$envio = (isset($_POST['envio'])) ? mysqli_real_escape_string($conexion, $_POST['envio']) : 0; 
$region = (is_numeric($_POST['region'])) ? mysqli_real_escape_string($conexion, $_POST['region']) : 0;
$comuna = (is_numeric($_POST['comuna'])) ? mysqli_real_escape_string($conexion, $_POST['comuna']) : 0;
$direccion = (isset($_POST['direccion'])) ? mysqli_real_escape_string($conexion, $_POST['direccion']) : 0; 

$factura = (isset($_POST['factura'])) ? mysqli_real_escape_string($conexion, $_POST['factura']) : 0; 

$idSucursal = (isset($_POST['idSucursal'])) ? mysqli_real_escape_string($conexion, $_POST['idSucursal']) : 0; 
$nombre_retiro = (isset($_POST['nombre_retiro'])) ? mysqli_real_escape_string($conexion, $_POST['nombre_retiro']) : 0; 
$telefono_retiro = (isset($_POST['telefono_retiro'])) ? mysqli_real_escape_string($conexion, $_POST['telefono_retiro']) : 0; 

$rut_factura = (isset($_POST['rut_factura'])) ? mysqli_real_escape_string($conexion, $_POST['rut_factura']) : 0; 
$giro_factura = (isset($_POST['giro_factura'])) ? mysqli_real_escape_string($conexion, $_POST['giro_factura']) : 0; 
$razon_social = (isset($_POST['razon_social'])) ? mysqli_real_escape_string($conexion, $_POST['razon_social']) : 0; 
$direccion_factura = (isset($_POST['direccion_factura'])) ? mysqli_real_escape_string($conexion, $_POST['direccion_factura']) : 0; 

$comentarios_envio = (isset($_POST['comentarios_envio'])) ? mysqli_real_escape_string($conexion, $_POST['comentarios_envio']) : 0;

if ($email === 0){
    echo '<script>parent.location = "envio-y-pago";</script>';
}
if(qty_pro() == 0){
    echo '<script>parent.location = "home";</script>';
}

 // echo "email:".$email."<br>nombre:".$nombre."<br>tele:".$telefono."<br>rut:".$rut."<br>envio:".$envio."<br>region:".$region."<br>comuna:".$comuna."<br>dire:".$direccion."<br>factura:".$factura."<br>idSucursa:".$idSucursal."<br>nombre_retiro:".$nombre_retiro."<br>tele_retiro:".$telefono_retiro."<br>rut_factura:".$rut_factura."<br>Giro:".$giro_factura."<br>razon_social:".$razon_social."<br>direccion_factura:".$direccion_factura."<br>comentarios:".$comentarios_envio;
$nomComuna = consulta_bd("nombre", "comunas", "id = $comuna", "");
?>

<?php if(!isset($_COOKIE['usuario_id'])){ ?>                      
<form method="post" action="envio-y-pago" id="volverForm">
    <input type="hidden" name="recuperar_datos" value="1">    
    <input type="hidden" name="email" value="<?= $email; ?>"> 
    <input type="hidden" name="nombre" value="<?= $nombre; ?>"> 
    <input type="hidden" name="rut" value="<?= $rut; ?>">
    <input type="hidden" name="telefono" value="<?= $telefono; ?>"> 
    <input type="hidden" name="envio" value="<?= $envio; ?>"> <!--direccionCliente-->
    <input type="hidden" name="region" value="<?= $region; ?>">
    <input type="hidden" name="comuna" value="<?= $comuna; ?>"> 
    <input type="hidden" name="direccion" value="<?= $direccion; ?>"> 

    <input type="hidden" name="idSucursal" value="<?= $idSucursal; ?>"> <!--id de la sucursar de retiro-->
    <input type="hidden" name="nombre_retiro" value="<?= $nombre_retiro; ?>"> 
    <input type="hidden" name="telefono_retiro" value="<?= $telefono_retiro; ?>"> 

    <input type="hidden" name="factura" value="<?= $factura; ?>"> <!--si o no-->
    <input type="hidden" name="rut_factura" value="<?= $rut_factura; ?>"> 
    <input type="hidden" name="giro_factura" value="<?= $giro_factura; ?>"> 
    <input type="hidden" name="razon_social" value="<?= $razon_social; ?>"> 
    <input type="hidden" name="direccion_factura" value="<?= $direccion_factura; ?>">
    <input type="hidden" name="comentarios_envio" value="<?= $comentarios_envio; ?>">
    
    
</form>
<?php }else{?>
<form method="post" action="envio-y-pago" id="volverForm">
</form>
<?php } ?>
<!--<div id="cambioPrecio"></div>-->

<div class="cont100 contBreadCrumbs" style="margin-bottom:0;">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Mi carro</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->


<div class="cont100 paddingTop">
    <div class="cont100Centro">
        <h2 class="tituloMiCarro2">Mi carro</h2>
		<div class="contCarro">
            <div id="contFilasCarro">
                <?= ShowCart(); ?>
            </div>
            <div id="contProductosGuardados">
                <?= saveForLater(); ?>
            </div>
        </div><!-- fin contCarro-->
        
        <div class="contTotales">
            <div id="contValoresResumen">
                <h2 class="tituloMiCarro">Resumen carro de compras</h2>
                <?= resumenCompra(); ?>
            </div><!--fin contValoresResumen-->
            
            <div class="contUltimosVistos">
            	<?= vistosRecientemente("lista","4"); ?>
            </div><!--fin contUltimosVistos -->
            
        </div><!-- Fin contTotales-->

	</div>
</div>

<?php if($_GET['stock']){ ?>
    <script type="text/javascript">
        Swal.fire("","Uno de los productos en el carro, ya no cuenta con stock","warning");
    </script>
<?php } ?>
            


<?php if(qty_pro() > 0){ ?>
<script type="text/javascript">
<?php
    if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}  
	$cart = $_COOKIE['cart_alfa_cm'];  
	    $items = explode(',',$cart);
	foreach ($items as $item) {
        $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
    }
    foreach ($contents as $prd_id=>$qty) {
        $filas = consulta_bd("pd.id, pd.sku, pd.nombre, p.nombre, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id = pd.producto_id and pd.id = $prd_id","");
	
		$idProductoInterior = $filas[0][0];
		if($filas[0][5] != '' and $filas[0][5] > 0){
			$valor = $filas[0][5];
		} else {
			$valor = $filas[0][4];
		}
        ?>
        ga('ec:addProduct', {'id': '<?= $filas[0][0]; ?>','name': '<?= $filas[0][2]; ?>','brand': 'no informada','price': '<?= $valor; ?>','quantity': <?= $qty; ?>});
		<?php
    }
?>
</script>
<script type="text/javascript">ga('ec:setAction','checkout', {'step': 1});</script>
<?php } //valido que el carro tenga productos para mostrar el analytics ?>


<div class="fixedResponsive">
	<?= resumenCompra(); ?>
</div>
