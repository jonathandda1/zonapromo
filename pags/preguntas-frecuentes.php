
 <div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Preguntas frecuentes</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->



<div class="cont100">
    <div class="contCentroPagFijas">
        <div class="tituloContacto">Preguntas frecuentes</div>
        <div class="contTxtParrafo">
            <p><strong>1-</strong> ¿Cómo comprar en www.zonapromo.cl?: Es muy simple, sólo debes seleccionar el o los productos que quiere comprar, agregarlo al carro, ingresar tus datos, elegir medio de pago y método de despacho. ¡Listo!</p>

            <p><strong>2-</strong> ¿Todos los productos tienen stock?: Es difícil que compres un producto que no tenga stock, pero en el caso que sucediera nos contactaremos contigo para ofrecerte otras alternativas de productos o reversar la compra.</p>

            <p><strong>3-</strong> No puedo realizar mi compra ¿cómo lo hago?: Si tienes algún problema o consulta en el proceso de compra, puedes contactarnos a portal@zonapromo.cl o llamarnos al +562-26984785</p>
            <p></p>
            
            
            <h4>Pagos</h4>
            <p><strong>1-</strong> ¿Qué formas de pago tienen?: Puedes pagar con tarjeta de débito o crédito emitidas en Chile.</p>
            <p></p>

            
            
            
            <h4>Despachos y Retiros</h4>
            <p><strong>1-</strong> ¿Cuándo llegará mi pedido ?: En Santiago y la Región Metropolitana llegarán los pedidos en un plazo de entre dos y cuatro días hábiles, mientras que para Regiones y Provincias este plazo será entre cinco y ocho días hábiles, con la excepción de las Regiones de Aysén y Punta Arenas en donde el plazo máximo se amplía hasta 15 días hábiles desde el momento que se hizo efectiva la compra. Recuerda que estos despachos son realizados por STARKEN, por lo que los plazos no pueden ser garantizados en un 100%.</p>

            <p><strong>2-</strong> Mi compra no llegó en la fecha indicada. ¿Qué hago?: Escríbenos un e-mail a portal@zonapromo.cl o llamanos al +562-26984785 indicándonos tu orden de compra para poder revisar lo que ocurrió y darte una respuesta.</p>

            <p><strong>3-</strong> ¿Puedo cambiar la fecha o dirección de despacho una vez realizada la compra ?: Una vez realizada la compra, no es posible hacer cambios de fecha ni dirección.</p>

            <p><strong>4-</strong> ¿Qué hago si no estoy todos los días en la dirección de entrega ?: Es imprescindible que nos entregues una dirección en que puedan recibir los productos de lunes a viernes entre las 09 y 18:30 hrs. No podemos entregarte una hora específica, ya que la empresa de transporte lo hace en los horarios indicados. Si tú no estás, puedes dejar a alguien encargado para su recepción.</p>

            <p><strong>5-</strong> La primera entrega fue fallida (no había nadie, el encargado rechazo la entrega etc.). ¿Qué puedo hacer?: Siempre hacemos un segundo intento de entrega que es sin costo para ti, sin embargo, si nuevamente no pueden recibirlo, el pedido llegara de vuelta a nuestras oficinas. En esta situación nos contactemos contigo para programar un tercer envío el cual tendrá un costo para tí.</p>

            <p><strong>6-</strong> Compré con retiro en tienda, ¿cuándo puedo ir a buscarlo?: Te llegará un e-mail que confirma que ya puedes retirar tú compra. Posterior a éste tienes 72 hrs. para su retiro. Lo puedes hacer cualquier hora mientras la tienda esté abierta. Si por alguna razón no pudiste ir en este plazo, escríbenos a portal@zonapromo.cl o llámanos al +562-26984785.</p>
            <p></p>
            
            
            
            
            <h4>Cambios y devoluciones</h4>
            
            
            <p><strong>1-</strong> ¿Puedo cambiar o pedir la devolución de dinero ?: Si no estás conforme con lo que compraste, tienes 10 días desde la recepción del producto para la devolución del dinero. Si tú opción es cambiarlo, tienes un tiempo de 30 días desde la fecha de recepción del producto. Es importante que los productos estén en buenas condiciones y en su embalaje original.</p>

            <p><strong>2-</strong> ¿Cómo y cuándo me devuelven el dinero?: Si tu compra fue realizada a través de una tarjeta de crédito, la devolución de dinero será por medio de la reversa de la transacción al titular de la tarjeta. Para esto, te solicitaremos lo datos de tu tarjeta. Esta operación es realizada por Transbank, por lo que, los plazos dependen de esta entidad, siendo generalmente entre 7 y 10 días hábiles. En el caso que la compra fuera realizada con tarjeta de débito, la devolución de dinero se realizará a través de una transferencia electrónica bancaria. Para esto, te solicitaremos los datos de transferencia, una vez recibida la información se realizará la operación en un plazo de 3 días hábiles.</p>

            <p><strong>3-</strong> ¿Cuántos días tengo para cambiar o devolver mi producto que retiré en tienda?: 30 días para cambio y 10 para devolución, ambos desde la recepción del producto. Se puede hacer en cualquier tienda Zona Promo ya que los productos adquiridos por compra online – retiro en tienda, no están sujetos a cambio ni devoluciones con servicio de retiro a domicilio. Recuerda que para hacer una devolución o cambio necesitas tener la boleta, factura o ticket de cambio y en caso de devolución esta debe ser solicitada por el titular de la compra.</p>
        </div>
    </div>
</div>