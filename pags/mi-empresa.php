<script>
<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo 'location.href = "inicio-sesion"';
	}
?>
</script>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Datos personales</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            
<?php $cliente = consulta_bd("id, nombre_empresa, rut_empresa, giro, direccion_facturacion","clientes","id = ".$_COOKIE['usuario_id'],""); ?>        	
            <div class="datosCliente">
            	<div class="titulosCuenta">
                    <span>Datos empresa</span>
                    <p>Puedes modificar tus datos de empresa para realizar compras más fácilmente con factura.</p>
                </div><!--fin titulosCuenta-->
                
                <form action="ajax/actualizarEmpresaMiCuenta.php" method="post" id="formularioActualizarEmpresaMiCuenta" class="bl_form">
                    <div class="cont100 FilaDatosUsuario">
                        <div class="cont50Form">
                            <div class="cont100">
                               <!-- <label for="nombre">Nombre empresa</label> -->
                               <input type="text" id="nombreEmpresa" name="nombreEmpresa" class="campoGrande2 label_better" value="<?= $cliente[0][1]; ?>" placeholder="Nombre empresa" data-new-placeholder="Nombre empresa"/>
                           </div>
                           <div class="cont100">
                               <!-- <label for="giro">Giro</label> -->
                               <input type="text" id="giro" name="giro" class="campoGrande2 label_better" value="<?= $cliente[0][3]; ?>" placeholder="Giro" data-new-placeholder="Giro" />
                           </div>                           
                           <div style="clear:both"></div>
                        </div><!--iniciar sesion -->

                        <div class="cont50Form">
                            <div class="cont100">
                               <!-- <label for="rutEmpresa">Rut</label> -->
                               <input type="text" id="rutEmpresa" name="rutEmpresa" class="campoGrande2 label_better" value="<?= $cliente[0][2]; ?>" placeholder="Rut empresa" data-new-placeholder="Rut empresa"/>
                           </div>
                           <div class="cont100">
                               <!-- <label for="direccionFiscal">Direccion fiscal</label> -->
                               <input type="text" id="direccionFiscal" name="direccionFiscal" class="campoGrande2 label_better" value="<?= $cliente[0][4]; ?>" placeholder="Direccion fiscal" data-new-placeholder="Direccion fiscal"/>
                           </div>
                            <div style="clear:both"></div>
                        </div><!--iniciar sesion -->
                    </div>

                    <div class="cont100">
                    	<a href="javascript:void(0)" id="btnActualizarEmpresaMiCuenta" class="btnActualizarMicuenta">Guardar cambios</a>
                    </div>
                    
                </form>
                
            </div> <!--fin columna datos-->
            
            <?php include("pags/tipsMiCuenta.php");?> 
        
        </div><!--fin contenidoMiCuenta-->
        
      
               
    </div>
</div>
