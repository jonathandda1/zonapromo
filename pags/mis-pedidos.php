<script>
<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo 'location.href = "inicio-sesion"';
	}
?>
</script>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Mis Pedidos</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            <div class="datosCliente">
                <div class="titulosCuenta">
                    <span>Mis Pedidos</span>
                    <p>Revisa el historial de tus pedidos, repite tus últimas compras y sigue tu pedido.</p>
                </div>
                <?php 
                if($usuario['perfilCliente'] == 2){

                    $esclavo = consulta_bd("id","clientes","parent = ".$_COOKIE['usuario_id'],"");
                    $cantParent = mysqli_affected_rows($conexion);
                    if($cantParent > 0){
                        $ids;
                        for($i=0; $i<sizeof($esclavo); $i++) {
                            $ids .= $esclavo[$i][0].", ";
                        }
                        $ids .= $_COOKIE['usuario_id'];
                    } else {
                        $ids = $_COOKIE['usuario_id'];
                    }

                    $pedidos = consulta_bd("distinct(p.id), p.oc, p.total_pagado, p.fecha_creacion, p.cliente_id, p.estado_id, p.medio_de_pago, p.estados_pedido_id, p.retiro_en_tienda, p.email, p.id_envio","pedidos p, clientes c","p.estado_id IN (2,4,6,7) and p.cliente_id IN ($ids)","p.fecha_creacion desc");
                } else {
                    $pedidos = consulta_bd("id,oc,total_pagado, fecha_creacion, cliente_id, estado_id, medio_de_pago, estados_pedido_id, retiro_en_tienda, email, id_envio","pedidos","estado_id IN (2,4,6,7) and cliente_id =  ".$_COOKIE['usuario_id'],"fecha_creacion desc");
                }        
                ?>        

                    <?php 
                    for($i=0; $i<sizeof($pedidos); $i++){ 
                    /*=================================================
                    =            Datos para volver a pedir       =
                    =================================================*/
                    // Nota: Se envian separados por coma y en el proceso se de hacer el pedido se debe desglosar
                    $idPedidoConsulta = $pedidos[$i][0];
                    $productosPedidos = consulta_bd("pedido_id, productos_detalle_id, cantidad","productos_pedidos","pedido_id = $idPedidoConsulta","");
                    $aux = 0;
                    foreach ($productosPedidos as $producto) {
                        if ($aux == 0) {
                            $prodId = $producto[1];
                            $prodCant = $producto[2];
                            $aux = 1;
                        }else{
                            $prodId .= ",".$producto[1];
                            $prodCant .= ",".$producto[2];
                        }
                    }
                    /*=====  End of Datos para volver a pedir===*/
                    ?>

                    <div class="detallePedido">
                            <div class="encabezado">
                                <?php if($usuario['perfilCliente'] == 2){ 
                                   $esclavo2 = consulta_bd("nombre","clientes","id = ".$pedidos[$i][4],"");
                                ?> 
                                <div class="cont100">
                                    <h3 class="tituloOrdenPerfil">Orden realizada por <strong><?= $esclavo2[0][0]; ?></strong></h3>
                                </div>
                                <?php } ?>
                                <div class="ancho40">
                                    <span class="cont100"><strong>Fecha del pedido</strong></span>
                                    <span class="cont100 fechaPedido"><?= formato_fecha(substr($pedidos[$i][3], 0, 10), "esp"); ?></span>
                                </div>
                                <?php if($pedidos[0][4]){?>
                                <div class="ancho25">
                                    <span class="cont100"><strong>Total por pagar</strong></span>
                                    <span class="cont100 fechaPedido">$<?= number_format($pedidos[$i][2],0,",",".");?></span>
                                </div>
                                <?php } else { ?>
                                <div class="ancho25">
                                    <span class="cont100"><strong>Total pagado</strong></span>
                                    <span class="cont100 fechaPedido">$<?= number_format($pedidos[$i][2],0,",",".");?></span>
                                </div>
                                <?php } ?>

                                <div class="floatRight ancho35">
                                    <span class="cont100"><strong>ORDEN: <?= $pedidos[$i][1]?></strong></span>
                                    <a class="btnDetalles" href="detalle-pedido/<?= $pedidos[$i][1]?>">Ver Ficha</a>
                                </div>
                            </div><!--fin encabezado -->

                            <div class="cont100 contenidoPedidos">
                                <div class="ancho70Pedido">
                                    <?php 
                                    $idPedido = $pedidos[$i][0];
                                    $detalles = consulta_bd("pd.id, p.nombre, pd.sku, pp.cantidad, pp.precio_unitario, p.thumbs, pd.descuento, pd.precio, pd.publicado, p.id","productos_detalles pd, productos_pedidos pp, productos p","pp.pedido_id = $idPedido and pp.productos_detalle_id = pd.id and pd.producto_id = p.id","");
                                    for($j=0; $j<sizeof($detalles); $j++){
                                        ?>
                                    <div class="cont100 filaProductoComprado">
                                        <a href="ficha/<?= $detalles[$j][9]; ?>/<?= url_amigables($detalles[$j][1]); ?>" target="_blank" class="thumbsProducto">
                                            <img src="<?= imagen("imagenes/productos/", $detalles[$j][5]);?>" width="100%" />
                                        </a>
                                        <div class="infoProdDetalle">
                                            <span class="nombreProdDetalle"><strong><?= $detalles[$j][1]; ?></strong></span>
                                            <span>Cantidad: <?= $detalles[$j][3]; ?></span>
                                            <span class="azul"><strong>Valor pagado: $<?= number_format($detalles[$j][4],0,",","."); ?></strong></span>
                                            
                                        </div>
                                    </div><!--Fin producto -->
                                    <?php } ?>
                                </div>
                                <div class="contBotones">
                                    <a class="btnVolverAPedir" idProductos="<?= $prodId ?>" cantProductos = "<?= $prodCant ?>" href="javascript:void(0)">Volver a comprar</a>
                                    <a class="btnPedidos" href="detalle-pedido/<?= $pedidos[$i][1]?>">Ver detalle del pedido</a>
                                    
                                    <?php if ($pedidos[$i][8] == 0 and $pedidos[$i][10] != ""){?>
                                    <a class="btnPedidos" href="https://api.enviame.io/s2/companies/2940/deliveries/<?= $pedidos[$i][10]; ?>/tracking" data-fancybox data-type="iframe" >Seguimiento despacho</a>
                                    <?php } ?> 
                                
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                        <!--fin cont100 -->
            </div><!-- fin datosCliente -->
            
              <?php include("pags/tipsMiCuenta.php"); ?>
    
    </div>
</div>