<?php
	$oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
	$PA = consulta_bd("id, estado_id, oc, medio_de_pago","pedidos","oc = '$oc'","");
	$cant = mysqli_affected_rows($conexion);
    
    $method = (isset($_GET[method])) ? mysql_real_escape_string($_GET[method]) : 'tbk';
	
	if($cant > 0 and ($PA[0][1] != 2 and $PA[0][1] != 4)){
		echo '<script>parent.location = "'.$url_base.'fracaso?oc='.$oc.'";</script>';
	} else if($oc=="" || $cant == 0){
	    echo '<script>parent.location = "'.$url_base.'404";</script>';
 	}else{

    

		$campos = "id,nombre,direccion,email,telefono,";//0-4
		$campos.= "regalo,total,valor_despacho,";//5-8
    
		$campos.= "comuna,region,fecha,estado_id,";//15-18
		$campos.= "factura, direccion_factura, giro, email_factura, rut_factura, fecha_modificacion, region,ciudad,estado_id, rut, retiro_en_tienda";//19-30

		$pedido = consulta_bd($campos,"pedidos","oc = '$oc'","");

	 	//detalle comprador
    $direccion_cliente = $pedido[0][2];
    $comuna_cliente    = $pedido[0][9];
    $region_cliente    = $pedido[0][10];
    $ciudad_cliente    = $pedido[0][17];
		$localidad_cliente    = $pedido[0][29];

    //detalle pedido
    $pedido_id 	   = $pedido[0][0];
    $total_pedido 	   = $pedido[0][6];
    $total_despacho   = $pedido[0][7];
    $fecha 		   = $pedido[0][10];
    $hora_pago		   = date('H:s:i', time($pedido[0][8]));

    //detalle transbank
    $num_tarjeta 	   = $pedido[0][10];
    $num_cuotas 	   = $pedido[0][11];
    $cod_aurizacion   = $pedido[0][12];
    $tipo_pago 	   = $pedido[0][13];

    //USER
    $id_usuario	   = $pedido[0][14];
    //$info = info_user($id_usuario);

    //validar si esta pagado
    $validador = $pedido[0][18];
    $contador_validador = count($validador);

    //total
    $total_productos = $pedido[0][6];
    $total_pagado = number_format($pedido[0][9],0,",",".");

    //funcion tipo pago
    $tipo_pago = tipo_pago($tipo_pago,$num_cuotas, $method);

    //detalle productos
    $detalle_productos = consulta_bd("productos_detalle_id, cantidad, precio_unitario","productos_pedidos","pedido_id= ".$pedido[0][0],"");
 }
?>


<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Compra exitosa</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->


<div class="cont100">
    <div class="cont100Centro">
        <div class="ctndr100">

        <h2 class="tituloExito">Transacción exitosa</h2><!--fin titulos_interiores -->




        <div class="top-identificacion">
            <div class="txtExito">¡Muchas gracias por comprar en <?= opciones('nombre_cliente') ?>! Su número de orden de compra es:</div>
            <div class="ocExito"><?= $oc; ?></div>
        </div>
		
        <div class="cont100 contBotonesCarro">
            <!--<a href="tienda/voucher/voucherCompra.php?oc=<?php echo $oc; ?>" target="_blank">VER BOUCHER DE COMPRA</a>-->
            <a href="<?php echo $url_base; ?>home">SEGUIR EN EL SITIO</a>
        </div>
		
            
		
		
	<div class="infoClienteExito">
    	<div class="ancho33">
              <div class="t-paso4">INFORMACIÓN PERSONAL</div>
              <div class="cont100">
                  <div class="t2-paso4">Nombre:</div>
                  <div class="dato-paso4"><?= $pedido[0][1]; ?></div>
              </div>
              <div class="cont100">
                  <div class="t2-paso4">Teléfono:</div>
                  <div class="dato-paso4"><?= $pedido[0][4]; ?></div>
              </div>
              
              <div class="cont100">
                  <div class="t2-paso4">Email:</div>
                  <div class="dato-paso4"><?= $pedido[0][3]; ?></div>
              </div>
              
    
              
    
              
        </div><!--fin ancho33 -->
        
        <div class="ancho33">
              <div class="t-paso4">INFORMACIÓN DE ENVÍO</div>
              <div class="t2-paso4">Dirección;</div>
              <div class="dato-paso4" style="margin-bottom:15px;">
                <?php echo $direccion_cliente ?>, <?php echo $comuna_cliente ?> 
              </div>
              <div class="dato-paso4">
                <?php echo $region_cliente ?>
              </div>
		</div><!--fin ancho33-->
        
        <?php if ($pedido[0][19] == 'boleta'){ ?>
        <div class="ancho33">
            <div class="t-paso4">INFORMACIÓN DE FACTURACIÓN</div>
            <div class="t2-paso4">Dirección;</div>
            <div class="dato-paso4" style="margin-bottom:15px;"><?php echo $direccion_cliente ?>, <?php echo $localidad_cliente; ?></div>
            <div class="dato-paso4"><?php echo $comuna_cliente ?>, <?php echo $region_cliente ?>,  <?php echo $ciudad_cliente ?></div>
        </div><!--FIn ancho33-->
        <?php } else {?>

        <div class="ancho33">
          <div class="t-paso4">INFORMACIÓN DE FACTURACIÓN</div>
          
          <div class="cont100">
              <div class="t2-paso4">Nombre:</div>
              <div class="dato-paso4"><?= $pedido[0][1]; ?></div>
          </div>
          <div class="cont100">
              <div class="t2-paso4">Direccion:</div>
              <div class="dato-paso4"><?= $pedido[0][20]; ?>, <?= $pedido[0][24]; ?></div>
          </div>
          <div class="cont100">
              <div class="t2-paso4">Rut:</div>
              <div class="dato-paso4"><?= $pedido[0][23] ?></div>
          </div>
         </div><!--Fin ancho33-->
        <?php } ?>
        
    </div><!--Fin cont70-->

        



        <div class="ancho100 infoClienteExito2 contInfoPago">
        	
              <div class="t-paso4">INFORMACIÓN DE PAGO</div>
    
              <div class="ancho25">
                <div class="cont100">
                    <div class="t2-paso4">Tipo de transacción:</div>
                    <div class="dato-paso4">Venta</div>
                </div>
                <div class="cont100">
                    <div class="t2-paso4">Plazo</div>
                    <div class="dato-paso4"><?= $tipo_pago[cuota]; ?></div>
                </div>
    
               
    		</div><!-- ancho25 -->
    
            
            <div class="ancho25">
            	<div class="cont100">
                  <div class="t2-paso4">Tipo de pago:</div>
                  <div class="dato-paso4">Nota de crédito</div>
                </div>
    
                <div class="cont100">
                  <div class="t2-paso4">Fecha transacción:</div>
                  <div class="dato-paso4"><?= $fecha; ?></div>
                </div>
              </div>
    
              <div class="ancho25">
                <div class="cont100">
                  <div class="t2-paso4">Valor por pagar:</div>
                  <div class="dato-paso4">$<?= number_format($total_pedido,0,",",".") ?></div>
                </div>
            </div> <!--Fin ancho25-->
    
              <div class="ancho25">
                <div class="cont100">
                  <div class="t2-paso4">Nombre/URL Comercio:</div>
                  <div class="dato-paso4"><?= opciones('nombre_cliente') ?> | <?= opciones('dominio') ?></div>
                </div>
    
              </div>
           
        </div><!--Fin cont70-->

			<!--<div class="cont100">
                <a class="btnTerminos" href="terminos-y-condiciones" target="blank">VER TÉRMINOS Y CONDICIONES</a>
            </div>--><!-- cont-datos -->

			<!--<div class="cont100 resumenProductos">
            	<?= showCartExito($oc); ?>
            </div>-->

		</div>
    </div><!--fin cont100Centro-->
</div><!--Fin breadcrumbs -->


<script type="text/javascript">  
<?php
for ($i=0; $i <sizeof($detalle_productos) ; $i++) {
		$pro_id = $detalle_productos[$i][0]; 
		$details_pro = consulta_bd("nombre,producto_id,id","productos_detalles","id = $pro_id","");
		$total_item = round($detalle_productos[$i][2])*$detalle_productos[$i][1];
		
		$marca = consulta_bd("m.nombre","productos p, marcas m","p.id = ".$details_pro[0][1]." and p.marca_id = m.id","");
		$categoria = consulta_bd("sc.nombre","lineas_productos cp, categorias c, subcategorias sc","cp.producto_id=".$details_pro[0][1]." and cp.categoria_id = c.id and sc.id = cp.subcategoria_id and cp.subcategoria_id <> ''","");
		
?>							
    ga('ec:addProduct', {
      'id': '<?php echo $details_pro[0][2]; ?>',
      'name': '<?php echo $details_pro[0][0]; ?>',
      'category': '<?php echo $categoria[0][0] ?>',
      'brand': '<?php echo $marca[0][0] ?>',
      'variant': '<?php echo $details_pro[$i][0]; ?>',
      'price': <?php echo round($detalle_productos[$i][2]); ?>,
      'quantity': <?php echo $detalle_productos[$i][1]; ?>
    });
<?php } ?>

// Transaction level information is provided via an actionFieldObject.
ga('ec:setAction', 'purchase', {
  'id': '<?php echo $oc; ?>',
  'affiliation': '<?= opciones("nombre_cliente");?>',
  'revenue': <?php echo round($pedido[0][6]); ?>,
  'tax': <?php echo round(($pedido[0][6]/1.19) * 0.19); ?>,
  'shipping': <?php echo $total_despacho; ?>
});


//ga('ec:setAction','checkout', {'step': 4});
</script>
