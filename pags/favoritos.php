<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Productos guardados</li>
        </ul>
    </div>
</div>

<?php   
$fvr = json_decode($_COOKIE[listaDeseos], true);
$cant_productos = count($fvr);
if(count($fvr) > 0){ ?>
    <div class="cont100">
        <div class="cont100Centro">               
            <h1 class="nomCategoria">Productos guardados<span class="cantProdCat"> &nbsp;&nbsp;&nbsp;&nbsp;<span class="tituloSpan">Resultados </span> (<?= $cant_productos; ?>)</span></h1>
        </div>
    </div><!--Fin contenido del sitio -->

    <?php
        $nombreLinea2 = "CATEGORÍAS";
        $lineasCategorias = consulta_bd("id, nombre", "lineas", "publicado = 1", "posicion asc");
        $subcategoriasCat = consulta_bd("id, nombre", "categorias", "publicada = 1", "posicion ASC, nombre asc");
    ?>
    <div class="cont100 fondoGris">
        <section class="contFiltrosExtras">
            <article class="filtrosIzquierda">
                <div class="divCategorias">
                    <ul class="menuGrilla">
                        <li><a href="javascript:void(0)">
                            <span><?= $nombreLinea2 ?></span></a>
                                <i><img src="img/icono_flecha_abajo.png" alt="img de flecha"></i>
                                <ul>
                                    <?php
                                    /*====================================
                                    =            CARGA LINEAS            =
                                    ====================================*/
                                    foreach ($lineasCategorias as $lid) { ?>
                                        <li>
                                            <a href="lineas/<?= $lid[0]; ?>/<?= url_amigables($lid[1]); ?>"><?= $lid[1]; ?></a>
                                        </li>
                                    <?php  } ?>
                                </ul>
                        </li>
                    </ul>
                </div>
                <div class="divCategorias">
                    <ul class="menuGrilla">
                        <li><a href="javascript:void(0)">
                            <span>SUBCATEGORÍAS</span></a>
                                <i><img src="img/icono_flecha_abajo.png" alt="img de flecha"></i>
                                <ul>
                                    <?php
                                    /*====================================
                                    =            CARGA Subcategorias            =
                                    ====================================*/
                                    foreach ($subcategoriasCat as $ca) { ?>
                                        <li class="categoriasLI">
                                            <a class="categoriasA" href="categorias/<?= $ca[0]; ?>/<?= url_amigables($ca[1]); ?>"><?= $ca[1] ?></a>
                                        </li>
                                    <?php  } ?>
                                </ul>
                        </li>
                    </ul>
                </div>
            </article>
            <article>
            </article>
        </section>
    </div>
<?php
}else{
    echo'
    <div class="cont100">
        <div class="cont100Centro">
            <h1 class="nomCategoria2">Aún no tienes productos agregados a tus favoritos</h1>
            <aside class="imgProdGuardadoVacio">
                <img src="img/producto_guardado_vacio.jpg" alt="Imgen de corazon para agregar">
            </aside>
        </div>
        <!--Fin cont100Centro -->
    </div>';

}
?>
        
<?php   
$fvr = json_decode($_COOKIE[listaDeseos], true);
if(count($fvr) > 0){
    
    $skuPD2 = "(";
    $ids = "";
    $x = 0;
    foreach($fvr as $key) {    
        if($x == 0){
            $skuPD2 .= trim($fvr[$x][id]);
            $ids .= trim($fvr[$x][id]);
        } else {
            if(trim($fvr[$x][id] != "")){
                $skuPD2 .= ",".trim($fvr[$x][id]);
                $ids .= ",".trim($fvr[$x][id]);
            }
        }
        $x++;
    }
    $skuPD2 .= ")";
    
    
}      
if(count($fvr) > 0){
    $productos = consulta_bd("distinct(p.id), p.nombre, p.thumbs, p.marca_id, pd.precio, pd.descuento, pd.id", "productos p, productos_detalles pd", "p.publicado = 1 and p.id = pd.producto_id and pd.id IN $skuPD2 group by p.id", "p.id");
    $cantPRD2 = mysqli_affected_rows($conexion);
}
if($cantPRD2 > 0){
?>
<div class="cont100">   
	<div class="cont100Centro">
        <?php if(!isset($_COOKIE['usuario_id'])){ ?>
            <h4 class="favIniciaSesion">¿Ya tienes una cuenta? <a id="modificarDatos" class="fancybox" href="#inline1" rel="<?php echo $idCliente; ?>"><span class="solicitarAqui">Inicia sesión aquí</span></a></h4>
                <div class="contGrillasFav">
        <?php } ?>
        
            <?php for($i=0; $i<sizeof($productos); $i++){ ?>
            
                    <div class="grilla" id="guardado_<?= $productos[$i][6]; ?>">
                        <a href="#" class="like" rel="<?= $productos[$i][6]; ?>">
                        <i class="<?= (guardadoParaDespues($productos[$i][6])) ? 'fas':'far';?> fa-heart"></i>
                        </a>
                        <?php if(ultimasUnidades($productos[$i][6])){ ?>
                        <div class="etq-ultimas">ultimas unidades</div>
                        <?php } ?>
                        <?php if(ofertaTiempo($productos[$i][6])){ ?>
                        <div class="countdown" rel="<?= ofertaTiempoHasta($productos[$i][6]); ?>"></div>
                        <?php } ?>

                        <?php if ($is_cyber AND is_cyber_product($productos[$i][0])): ?>
                            <div class="img-cyber"><img src="img/iconcyber.png" alt=""></div>
                        <?php endif ?>
                        <?= porcentajeDescuento($productos[$i][0]); ?>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="imgGrilla">
                            <img src="<?= imagen("imagenes/productos/", $productos[$i][2]);?>" width="100%">
                        </a>
                        
                        <a href="ficha/<?= $productos[$i][0]."/".url_amigables($productos[$i][1]); ?>" class="nombreGrilla"><?= $productos[$i][1]; ?></a>
                        <a href="ficha/<?= $productos[$i][0]; ?>/<?= url_amigables($productos[$i][1]); ?>" class="valorGrilla">
                        <?php if ($is_cyber AND is_cyber_product($productos[$i][0])):
                            $precios = get_cyber_price($productos[$i][6]); ?>
                            
                            <span class='conDescuento'>$<?= number_format($precios['precio_cyber'],0,",",".") ?></span>
                            <span class="antes"> $<?= number_format($precios['precio'],0,",",".") ?></span>
                        <?php else: ?>
                            <?php if(tieneDescuento($productos[$i][6])){ ?>
                                
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                                <span class="antes"> $<?= number_format(getPrecioNormal($productos[$i][6]),0,",",".") ?></span>
                            <?php }else{ ?>
                                <span class="antes"></span>
                                <span class='conDescuento'>$<?= number_format(getPrecio($productos[$i][6]),0,",",".") ?></span>
                            <?php } ?>
                        <?php endif ?>
                        </a>                        
                    </div><!--Fin Grilla -->
            <?php } ?>        
         
        </div><!-- Fin de ContFavoritos -->
    </div>
</div>
<?php }else{  ?>
    <div class="cont100 grillaNormal">   
        <div class="cont100Centro">
            <h4 class="tituloVistosRecien">Te recomendamos</h4>
            <div class="cont100 ultimosVistos">
                <?= vistosRecientemente("grilla", 5); ?>
            </div>
            <!--Productos relacionados -->
        </div><!--Fin centroDocumentos -->
    </div><!--FIn cont100 -->

    <div class="cont100 fondoGris grillaResponsive grillaResponsiveficha">   
        <div class="cont100Centro">
            <h4 class="tituloVistosRecien">Te recomendamos</h4>
            <div class="cont100" style="background: #fff;">
                <?= vistosRecientemente("lista", 4); ?>
            </div>
            <!--Productos relacionados -->
        </div><!--Fin centroDocumentos -->
    </div><!--FIn cont100 -->
<?php }  ?>
<?php if(!isset($_COOKIE['usuario_id'])){ ?>
<div id="inline1" class="inicioSesionCarroPopUp" style="width:400px;display: none;">
    <h3>Inicie Sesión</h3>
    <form method="post" id="formLoginCompraRapida" action="ajax/login.php">
        <input type="text" name="usuarioLogin" value="<?php echo $email ?>" class="inputInicioSesion" id="usuarioLogin" placeholder="Usuario" />
        <input type="password" name="passLogin" value="" id="passLogin" class="inputInicioSesion" placeholder="Ingrese Contraseña" />
        <input type="hidden" name="origen" value="favoritos">
        <a href="javascript:void(0)" id="btnIngresarCompraRapida">Ingresar</a>
    </form>
    <a href="recuperar-clave" class="solicitarAqui2">¿Olvidaste tu clave?</a>
    
    <a href="registro" class="registroPopUp">REGÍSTRATE</a>
       
</div> 
<?php } ?>


   