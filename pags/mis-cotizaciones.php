<script>
<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo 'location.href = "inicio-sesion"';
	}
?>
</script>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Mis Pedidos</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            <div class="datosCliente">
                <div class="titulosCuenta">
                    <span>Cotizaciones</span>
                    <p>Puedes revisar el historial de tus cotizaciones y repetirlas.</p>
                </div>
                <?php 
                    $cotizaciones = consulta_bd("id,oc, estado_id , cliente_id,email, fecha_creacion","cotizaciones","estado_id IN (1,2,3) and cliente_id =  ".$_COOKIE['usuario_id'],"fecha_creacion desc");
                ?>        

                    <?php 
                    for($i=0; $i<sizeof($cotizaciones); $i++){ 
                    /*=================================================
                    =            Datos para volver a pedir       =
                    =================================================*/
                    // Nota: Se envian separados por coma y en el proceso se de hacer el pedido se debe desglosar
                    $idPedidoConsulta = $cotizaciones[$i][0];
                    $productosPedidos = consulta_bd("cotizacion_id, productos_detalle_id, cantidad, id_color, id_logo","productos_cotizaciones","cotizacion_id = $idPedidoConsulta AND condicion_boton != 'sin stock'","");
                    $aux = 0;
                    foreach ($productosPedidos as $producto) {
                        if ($producto[3] != 0 && $producto[3] != "") {
                            $colorId = $producto[3];
                        }else{
                            $colorId = 0;  
                        }
                        if ($producto[4] != 0 && $producto[4] != "") {
                            $logoId = $producto[4];
                        }else{
                            $logoId = 0;  
                        }
                        // echo $colorId."-";
                        if ($aux == 0) {
                            $prodId = $producto[1];
                            $prodCant = $producto[2];
                            $prodColor = $colorId;
                            $prodLogo = $logoId;
                            $aux = 1;
                        }else{
                            $prodId .= ",".$producto[1];
                            $prodCant .= ",".$producto[2];
                            $prodColor .= ",".$colorId;
                            $prodLogo .= ",".$logoId;
                        }
                    }
                    /*=====  End of Datos para volver a pedir===*/
                    ?>

                    <div class="detallePedido">
                            <div class="encabezado">
                                <div class="ancho40">
                                    <span class="cont100"><strong>Fecha del pedido</strong></span>
                                    <span class="cont100 fechaPedido"><?= formato_fecha(substr($cotizaciones[$i][5], 0, 10), "esp"); ?></span>
                                </div>
                                <div class="floatRight ancho50">
                                    <span class="cont100"><strong>ORDEN <strong></span>
                                    <span class="cont100"><strong class="ocCot"><?= $cotizaciones[$i][1]?></strong></span>
                                </div>
                            </div><!--fin encabezado -->

                            <div class="cont100 contenidoPedidos">
                                <div class="ancho70Pedido">
                                    <?php 
                                    $idPedido = $cotizaciones[$i][0];
                                    $detalles = consulta_bd("pd.id, p.nombre, pd.sku, pp.cantidad, pp.precio_unitario, p.thumbs, pd.descuento, pd.precio, pd.publicado, p.id, pp.color, pp.logo","productos_detalles pd, productos_cotizaciones pp, productos p","pp.cotizacion_id = $idPedido and pp.productos_detalle_id = pd.id and pd.producto_id = p.id AND pp.condicion_boton != 'sin stock'","");
                                    for($j=0; $j<sizeof($detalles); $j++){
                                        ?>
                                    <div class="cont100 filaProductoComprado">
                                        <a href="ficha/<?= $detalles[$j][9]; ?>/<?= url_amigables($detalles[$j][1]); ?>" target="_blank" class="thumbsProducto">
                                            <img src="<?= imagen("imagenes/productos/", $detalles[$j][5]);?>" width="100%" />
                                        </a>
                                        <div class="infoProdDetalle">
                                            <span class="nombreProdDetalle"><strong><?= $detalles[$j][1]; ?></strong></span>
                                            <span class="cantidadProdDetalle"><strong>Cantidad:</strong> <?= $detalles[$j][3]; ?></span>
                                            <?php if($detalles[$j][10] != "" && $detalles[$j][10] != null){ ?>
                                            <div class="colorDetalle">
                                                <a href="ficha/<?= $detalles[$j][9]; ?>/<?= url_amigables($detalles[$j][1]); ?>">
                                                <span><strong>Color impresión: </strong> <?= $detalles[$j][10]?></span>
                                                </a>
                                            </div> 
                                            <?php } ?> 
                                            <?php if($detalles[$j][11] != "" && $detalles[$j][11] != null){ ?>
                                            <div class="logoDetalle">
                                                <a href="ficha/<?= $detalles[$j][9]; ?>/<?= url_amigables($detalles[$j][1]); ?>">
                                                <span><strong>Cant. Color: </strong> <?= $detalles[$j][11]?></span>
                                                </a>
                                            </div> 
                                            <?php } ?>                                          
                                        </div>
                                    </div><!--Fin producto -->
                                    <?php } ?>
                                </div>
                                <div class="contBotones">
                                    <a class="btnPedidos volverCotizar"  idlogos="<?= $prodLogo ?>" idColores="<?= $prodColor ?>" idProductos="<?= $prodId ?>" cantProductos = "<?= $prodCant ?>" href="javascript:void(0)">Volver a cotizar</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                        <!--fin cont100 -->
            </div><!-- fin datosCliente -->
            
              <?php include("pags/tipsMiCuenta.php"); ?>
    
    </div>
</div>