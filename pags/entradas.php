<?php 
    $id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
    $titulo = (isset($_GET['nombre'])) ? mysqli_real_escape_string($conexion, $_GET['nombre']) : 0;

    $entrada = consulta_bd("e.titulo, e.portada, e.bajada, a.nombre, a.thumbs, e.id, e.desarrollo", "entradas e, autores a", "e.autor_id = a.id and e.publicada = 1 and e.id = $id", "");


    $categorias = consulta_bd("id, nombre","catblog","publicada = 1"," nombre asc");
    $UltimasEnt = consulta_bd("e.titulo, e.portada, e.bajada, a.nombre, a.thumbs, e.id", "entradas e, autores a", "e.autor_id = a.id and e.publicada = 1", "e.fecha_creacion desc LIMIT 3");


    
?> 

<div class="cont100">
    <div class="cont100Centro">
        <?php 
         $autores = consulta_bd("titulo, nombre, thumbs, banner, descripcion", "autores", "destacado = 1", "id desc");
        if(count($autores) > 0){
        ?> 
         <div class="contFondoBlogger" style="background-image:url(imagenes/autores/<?= $autores[0][3]; ?>);">
            <h3><?= $autores[0][1]; ?></h3>
            <div class="decripcionBlogger"><?= $autores[0][4]; ?></div>
        </div>
       <?php } ?>
        
        <div class="cont100 contMenuBlog">
            <?php for($i=0; $i<sizeof($categorias); $i++) { ?> 
            <a href="categorias_blog/<?= $categorias[$i][0];?>/<?= url_amigables($categorias[$i][1]);?>"><?= $categorias[$i][1];?></a>
            <?php } ?> 
        </div>
            
        
        <div class="fila1Blog cont100">
            <div class="grillaDestacadaBlog">
                <img src="imagenes/entradas/<?= $entrada[0][1]; ?>" width="100%">
            </div><!--fin grillaDestacadaBlog -->
            
            <div class="columnaUltimasEntradas">
                <h3>Últimas entradas</h3>
                <?php for($i=0; $i<sizeof($UltimasEnt); $i++) { ?> 
                <div class="grillaUltimasEntradas">
                    <div class="imgUltimasEntradas">
                        <img src="imagenes/entradas/<?= $UltimasEnt[$i][1]; ?>" width="100%">
                    </div>
                    <div class="contTinfoUltimasEntradas">
                        <div class="tituloUltimasEntradas"><?= $UltimasEnt[$i][0]; ?></div>
                        <a href="entradas/<?= $UltimasEnt[$i][5]; ?>/<?= url_amigables($UltimasEnt[$i][0]); ?>" class="btnGrillaBlog">ver artículo</a>
                    </div><!--fin contTinfoUltimasEntradas-->
                </div>
                <?php } ?> 
            </div><!--fin columnaUltimasEntradas -->
        </div><!--fila1Blog-->
        
        
        
        <div class="cont100 desarrolloEntrada">
            <h2><?= $entrada[0][0]; ?></h2>
            <?= $entrada[0][6]; ?>
        </div><!--fin contGrillasBlog -->
        
        
        
        
    </div>
</div>
