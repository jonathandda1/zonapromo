
 <div class="cont100 contBreadCrumbs" style="margin-bottom:0;">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Contacto</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->



<div class="cont100 fondoServicio">
    <div class="contCentroPagFijas">
        <div class="contTxtParrafo">
            <div class="tituloContacto">Contáctanos</div>
                <!-- <p class="subtituloContacto">Deja tus datos y nos pondremos en contacto contigo.</p> -->
            <div class="cont100 contFormServ">
                <form method="post" action="mailsenderContacto.php" class="bl_form" id="formContacto">
                    <!-- <label for="nombre">Nombre y Apellido</label> -->
                    <input type="text" id="nombre" name="nombre" class="campoGrande2 label_better" value="" placeholder="Nombre y Apellido" data-new-placeholder="Nombre y Apellido"/>
                    <div class="cont100">
                        <!-- <label for="email">Email</label> -->
                        <input type="text" id="email" name="email" class="campoGrande2 label_better " value="" placeholder="Email" data-new-placeholder="Email"/>
                    </div>
                    <div class="cont100">
                        <!-- <label for="telefono">Teléfono</label> -->
                        <input type="text" id="telefono" maxlength="11" minlength="9" name="telefono" class="campoGrande2 label_better" value="" placeholder="Teléfono" data-new-placeholder="Telefono"/>
                    </div>
                    <div class="cont100">
                        <!-- <label for="mensaje">Mensaje</label> -->
                        <textarea id="mensaje" name="mensaje" class="campoGrande3 label_better" value="" placeholder="Mensaje" data-new-placeholder="Mensaje"></textarea>
                    </div>
                    
                    <div class="cont100" style="text-align:center;">
                        <a href="javascript:void(0)" class="btnFormContacto" id="btnFormContacto">Enviar</a>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
