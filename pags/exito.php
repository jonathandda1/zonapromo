<?php
	$oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;
	$PA = consulta_bd("id, estado_id, oc, medio_de_pago","pedidos","oc = '$oc'","");
	$cant = mysqli_affected_rows($conexion);
    $method = (isset($_GET[method])) ? mysql_real_escape_string($_GET[method]) : 'tbk';
	
	if($cant > 0 and ($PA[0][1] != 2 and $PA[0][1] != 4)){
		echo '<script>parent.location = "'.$url_base.'fracaso?oc='.$oc.'";</script>';
	} else if($oc=="" || $cant == 0){
	    echo '<script>parent.location = "'.$url_base.'404";</script>';
 	}else{

    $method = ($PA[0][3] == 'webpay') ? 'tbk' : 'mp';

		$campos = "id,nombre,direccion,email,telefono,";//0-4
		$campos.= "regalo,total,valor_despacho,";//5-8
    if ($method == 'tbk') {
      $campos.= "transaction_date, amount, card_number,shares_number,authorization_code,payment_type_code,cliente_id,";//9-14
    }else{
      $campos.= "mp_transaction_date, mp_paid_amount, mp_card_number, mp_cuotas, mp_auth_code, mp_payment_type,cliente_id,";
    }
		$campos.= "comuna,region,ciudad,estado_id,";//15-18
		$campos.= "factura, direccion_factura, giro, email_factura, rut_factura, fecha_modificacion, region,ciudad,estado_id, rut,retiro_en_tienda, medio_de_pago";//19-30

		$pedido = consulta_bd($campos,"pedidos","oc = '$oc'","");

	 	//detalle comprador
    $direccion_cliente = $pedido[0][2];
    $comuna_cliente    = $pedido[0][15];
    $region_cliente    = $pedido[0][16];
    $ciudad_cliente    = $pedido[0][17];
		$localidad_cliente    = $pedido[0][29];

    //detalle pedido
    $pedido_id 	   = $pedido[0][0];
    $total_pedido 	   = $pedido[0][6];
    $total_despacho   = $pedido[0][7];
     date_default_timezone_set('America/Santiago');
    $fecha 		   = date("Y-m-d", strtotime(str_replace('/', '-', $pedido[0][8])));
    $hora_pago		   = date('H:i:s', strtotime(str_replace('/', '-', $pedido[0][8])));

    //detalle transbank
    $num_tarjeta 	   = $pedido[0][10];
    $num_cuotas 	   = $pedido[0][11];
    $cod_aurizacion   = $pedido[0][12];
    $tipo_pago 	   = $pedido[0][13];

    //USER
    $id_usuario	   = $pedido[0][14];
    //$info = info_user($id_usuario);

    //validar si esta pagado
    $validador = $pedido[0][18];
    $contador_validador = count($validador);

    //total
    $total_productos = $pedido[0][6];
    $total_pagado = number_format($pedido[0][9],0,",",".");

    //funcion tipo pago
    $tipo_pago = tipo_pago($tipo_pago,$num_cuotas, $method);
    //detalle productos
    $detalle_productos = consulta_bd("productos_detalle_id, cantidad, precio_unitario","productos_pedidos","pedido_id= ".$pedido[0][0],"");
 }
?>

<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Éxito en Compra</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->


<div class="cont100">
    <div class="cont100Centro">
        <div class="centroExito">
            <div class="iconoExito"><img src="img/icono_exito.png" alt="Imagen de éxito"></div>
            <h2 class="tituloExito">Transacción exitosa</h2><!--fin titulos_interiores -->
            <div class="top-identificacion">
                <div class="txtExito">¡Muchas gracias por comprar en <?= opciones('nombre_cliente') ?>! <br>Su número de orden de compra es: <span class="ocExito"><?= $oc; ?></span></div>
                
            </div>

            <div class="cont100 contBotonesCarro">
                <a href="tienda/voucher/voucherCompra.php?oc=<?php echo $oc; ?>" target="_blank">Ver voucher de compra</a>
                <!--<a href="<?php echo $url_base; ?>home">Seguir en el sitio</a>-->
            </div>
		
          	<div class="infoClienteExito">
            	<div class="filaExito">
                <div class="t-paso4"><i class="far fa-user"></i>Información personal</div>
                <div class="cont100">
                    <div class="t2-paso4">Nombre:</div>
                    <div class="dato-paso4"><?= $pedido[0][1]; ?></div>
                </div>
                <div class="cont100">
                    <div class="t2-paso4">Teléfono:</div>
                    <div class="dato-paso4"><?= $pedido[0][4]; ?></div>
                </div>
                <div class="cont100">
                    <div class="t2-paso4">Rut:</div>
                    <div class="dato-paso4"><?= $pedido[0][28]; ?></div>
                </div>
                <div class="cont100">
                    <div class="t2-paso4">Email:</div>
                    <div class="dato-paso4"><?= $pedido[0][3]; ?></div>
                </div>              
              </div><!--fin ancho33 -->
        
              <div class="filaExito">
                  <div class="t-paso4"><i class="fas fa-truck-moving"></i>Información de envío</div>
                  <div class="t2-paso4">Dirección:</div>
                  <div class="dato-paso4" style="margin-bottom:15px;">
                    <?php echo $direccion_cliente ?>
                  </div>
                  <div class="dato-paso4">
                    <?= $comuna_cliente ?>, <?= $region_cliente ?>
                  </div>
      		    </div><!--fin ancho33-->
        
              <?php if ($pedido[0][19] == 1){ ?>
                <div class="filaExito">
                  <div class="t-paso4"><i class="fas fa-search-dollar"></i>Información de facturación</div>
                  
                  <div class="cont100">
                      <div class="t2-paso4">Nombre:</div>
                      <div class="dato-paso4"><?= $pedido[0][1]; ?></div>
                  </div>
                  <div class="cont100">
                      <div class="t2-paso4">Direccion:</div>
                      <div class="dato-paso4"><?= $pedido[0][20]; ?>, <?= $pedido[0][24]; ?></div>
                  </div>
                  <div class="cont100">
                      <div class="t2-paso4">Rut:</div>
                      <div class="dato-paso4"><?= $pedido[0][28]; ?></div>
                  </div>
                 </div><!--Fin ancho33-->
              <?php } ?>
              <div class="t-paso4"><i class="fas fa-money-check"></i>Información de pago</div>
    
              <div class="filaExito">
                <div class="cont100">
                    <div class="t2-paso4">Tipo de transacción:</div>
                    <div class="dato-paso4"><?= $pedido[0][30]; ?></div>
                </div>
                <div class="cont100">
                    <div class="t2-paso4">N° de Cuotas:</div>
                    <div class="dato-paso4"><?= $tipo_pago[cuota]; ?></div>
                </div>
    
                <div class="cont100">
                    <div class="t2-paso4">Tipo de cuota:</div>
                    <div class="dato-paso4"><?= $tipo_pago[tipo_cuota]; ?></div>
                </div>

                <div class="cont100">
                    <div class="t2-paso4">Tarjeta terminada en:</div>
                    <div class="dato-paso4">**** **** **** <?php echo $num_tarjeta; ?></div>
                </div>
                
                <div class="cont100">
                  <div class="t2-paso4">Tipo de pago:</div>
                  <div class="dato-paso4"><?= $tipo_pago['tipo_pago']; ?></div>
                </div>
    
                <div class="cont100">
                  <div class="t2-paso4">Fecha y hora de transacción:</div>
                  <div class="dato-paso4"><?= $fecha; ?> | <?= $hora_pago; ?></div>
                </div>
              </div>
    
              <div class="filaExito">
                <div class="cont100">
                  <div class="t2-paso4">Valor pagado:</div>
                  <div class="dato-paso4">$<?= $total_pagado; ?></div>
                </div>
    
                <div class="cont100">
                  <div class="t2-paso4">Código de autorización de transacción:</div>
                  <div class="dato-paso4"><?= $cod_aurizacion; ?></div>
                </div>
    
                <div class="cont100">
                  <div class="t2-paso4">Tipo de moneda:</div>
                  <div class="dato-paso4">CLP / PESO Chileno</div>
                </div>
              </div> <!--Fin ancho25-->
    
              <div class="filaExito">
                <div class="cont100">
                  <div class="t2-paso4">Nombre/URL Comercio:</div>
                  <div class="dato-paso4"><?= opciones('nombre_cliente') ?> | <?= opciones('dominio') ?></div>
                </div>
              </div>
            </div><!--Fin cont70-->
            <?php if(!$_COOKIE[usuario_id]){
              $email = $pedido[0][3];
              $cliente = consulta_bd("id","clientes","email = '$email' and clave is NULL","");
              $cantEmail = mysqli_affected_rows($conexion);
              if($cantEmail > 0){ ?>
                <div class="cont100 contBotonesCarro crearCuentaDesdeExito">
                  <h2>¡CREA TU CUENTA EN UN SOLO CLICK!</h2>
                  <p>Crea tu cuenta en un solo click y podrás revisar el estado de tus despachos,</p>
                  <p>historial de compras, productos guardados en tu dashboard y mucho más.</p>
                  <form method="post" id="crearCuentaDesdeExito" action="crear-cuenta-exito">
                                <input type="hidden" name="cliente_id" value="<?= $cliente[0][0]; ?>" />
                                  <input class="btnCuentaExito" type="submit" name="crearCuenta" value="CREAR CUENTA" />
                              </form>
                          </div>
            <?php }
              
            }?>
       
		  </div><!--fin centroExito-->
    </div><!--fin cont100Centro-->
</div><!--Fin breadcrumbs -->

<script type="text/javascript">  
<?php
for ($i=0; $i <sizeof($detalle_productos) ; $i++) {
		$pro_id = $detalle_productos[$i][0]; 
		$details_pro = consulta_bd("nombre,producto_id,id","productos_detalles","id = $pro_id","");
		$total_item = round($detalle_productos[$i][2])*$detalle_productos[$i][1];
		
		$marca = consulta_bd("m.nombre","productos p, marcas m","p.id = ".$details_pro[0][1]." and p.marca_id = m.id","");
		$categoria = consulta_bd("sc.nombre","lineas_productos cp, categorias c, subcategorias sc","cp.producto_id=".$details_pro[0][1]." and cp.categoria_id = c.id and sc.id = cp.subcategoria_id and cp.subcategoria_id <> ''","");
		
?>							
    ga('ec:addProduct', {
      'id': '<?php echo $details_pro[0][2]; ?>',
      'name': '<?php echo $details_pro[0][0]; ?>',
      'category': '<?php echo $categoria[0][0] ?>',
      'brand': '<?php echo $marca[0][0] ?>',
      'variant': '<?php echo $details_pro[$i][0]; ?>',
      'price': <?php echo round($detalle_productos[$i][2]); ?>,
      'quantity': <?php echo $detalle_productos[$i][1]; ?>
    });
<?php } ?>

// Transaction level information is provided via an actionFieldObject.
ga('ec:setAction', 'purchase', {
  'id': '<?php echo $oc; ?>',
  'affiliation': '<?= opciones("nombre_cliente");?>',
  'revenue': <?php echo round($pedido[0][6]); ?>,
  'tax': <?php echo round(($pedido[0][6]/1.19) * 0.19); ?>,
  'shipping': <?php echo $total_despacho; ?>
});


//ga('ec:setAction','checkout', {'step': 4});
</script>
