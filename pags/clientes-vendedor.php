<script>
<?php 
	if(!isset($_COOKIE['Vendedor_id'])){
		echo 'location.href = "home?error=Debes iniciar sesión en la sección para vendedores en el footer de la pagina"';
	}
    $idEjecutiva = $_COOKIE['Vendedor_id'];
    $ejecutiva = consulta_bd("nombre, apellido,perfil","vendedores","id = '$idEjecutiva'","");
    $rutEmpresa = (isset($_GET['rutEmpresa'])) ? mysqli_real_escape_string($conexion, $_GET['rutEmpresa']) : 0;
    $rutUsuario = (isset($_GET['usuario'])) ? mysqli_real_escape_string($conexion, $_GET['usuario']) : 0;
    $url = "clientes-vendedor/".$rutEmpresa;
?>
</script>
<style>
    a:focus, a:hover {
        text-decoration: none;
    }
    .breadcrumb{
        margin-bottom: 5px;
        background-color: unset;
    }
    .breadcrumb span.active{
        color: #009be5;
    }
    .dataTables_length > label{
        max-width: 70px;
    }
    div.dataTables_wrapper div.dataTables_filter input {
        width: 300px;
        border: solid 1px #009be5;
        border-radius: 15px;
        -moz-border-radius: 15px;
        -webkit-border-radius: 15px;
        height: 40px;
        font-family: "Raleway-Medium";
        font-size: 14px;
    }
    .dataTables_length div.selector{
        border: solid 1px #009be5;
    }
    @media (max-width: 1050px) {
        div.dataTables_wrapper div.dataTables_filter input {
            width: unset;
        }
    }
    @media (max-width: 500px) {
        div.dataTables_wrapper div.dataTables_filter label{
            display: flex;
            flex-flow: wrap;
            justify-content: center;
            align-items: center;
        }
    }
    #example {
        border: none;
    }
    #example td {
        border: none;
        height: 40px;
        font-family: "Raleway-Medium";
        font-size: 13px;
        color: #888888;
        line-height: 40px;
    }
    #example th {
        border: none;
        height: 40px;
        font-family: "Raleway-SemiBold";
        font-size: 14px;
        color: #555555;
        line-height: 40px;
    }
    .page-item.active .page-link{
        background-color: #009be5;
        border-color: #009be5;
    }
    table.dataTable.dtr-inline.collapsed>tbody>tr>td.dtr-control:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th.dtr-control:before {
        background-color:#009be5;
    }
    .swal2-popup {
    font-size: 1.6rem !important;
    }
</style>

<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <span class="active">Clientes</span>
        </ul>
    </div>
</div>

<div class="cont100">
    <div class="cont100Centro">
        
        <?php include("pags/menuVendedores.php"); ?>

        <?php if ($rutEmpresa != 0) { ?>
            <section class="cont100 cotizacionesDetalle">
                <article>
                    <h1 class="tituloEmpresas">Empresa</h1>
                </article>
                <article class="contInputsTablas">
                    <?php
                        $empresa = consulta_bd("nombre, rut","empresas","rut = '$rutEmpresa'","");
                    ?>
                    <div class="contInputTables">
                        <!-- <label for="nombre">Nombre</label> -->
                        <input type="text" id="nombreEmpresa" name="nombreEmpresa" class="campoGrandeDisable label_better" value="<?= $empresa[0][0]; ?>" placeholder="Nombre" data-new-placeholder="Nombre" disabled/>
                    </div>
                    <div class="contInputTables">
                        <!-- <label for="nombre">Nombre</label> -->
                        <input type="text" id="rutEmpresa" name="rutEmpresa" class="campoGrandeDisable label_better" value="<?= $empresa[0][1]; ?>" placeholder="Rut" data-new-placeholder="Rut" disabled/>
                    </div>
                    
                    <div class="contInputTables">
                        <input type="hidden" name="url" id="url" value="<?= $url ?>">
                        <label for="usuarioSelect">Usuario</label>
                        <select class="selectorFiltrosEspeciales selectUsuarios" id="usuarioSelect">
                                <option value="all">Todos</option>
                            <?php
                            $usuariosEmpresas = consulta_bd("distinct(rut),rut_empresa, nombre, apellido","cotizaciones","rut_empresa = '$rutEmpresa'","rut_empresa ASC");
                            // if ($rutUsuario != 0 && $rutUsuario != "all") {
                            //     $usuariosEmpresas = consulta_bd("distinct(rut), rut_empresa, nombre, apellido","cotizaciones","rut_empresa = '$rutEmpresa' AND rut = '$rutUsuario'","nombre ASC");
                            // }
                            $cantidadUsuario = count($usuariosEmpresas);
                            foreach ($usuariosEmpresas as $value) {
                                $selectorELegido1 = "";      
                                if ($rutUsuario == $value[0]) {
                                    $selectorELegido1 = "selected='selected'";
                                }                         
                                echo "<option ".$selectorELegido1." value='".$value[0]."'>".$value[2]." ".$value[3]."</option>";
                            } ?>
                        </select>
                    </div>
                </article>
                <article class="botonesTablas">
                    <div class="botonTabla">
                        <a class="fancybox" href="#modalNuevaCotizacion"><button type="button" class="btn-nuevaCotizacion">Nueva cotización</button></a>
                        </div>
                    <div id="modalNuevaCotizacion" class="inicioSesionCarroPopUp" style="width:440px;display: none;">
                        <h3>Nueva cotización</h3>
                        <h5>Elije la empresa y el usuario</h5>
                        <?php
                        $usuarios = consulta_bd("nombre, apellido, rut", "clientes", "rut_empresa = '$rutEmpresa'", "nombre asc");
                        $usuariosCont = mysqli_affected_rows($conexion);
                        ?>
                        <form id="formNuevaCotizacion" action="ajax/ajaxNuevaCotizacion.php" method="post">
                            <div class="contSelectEmpresaUsuario">
                                <label for="nombre">Nombre de empresa</label>
                                <input type="text" id="nombreEmpresa" name="nombreEmpresa" class="inptEmpresaPopup" value="<?= $empresa[0][0]; ?>" placeholder="Nombre" data-new-placeholder="Nombre" readonly/>
                                <input type="hidden" name="rutEmpresa" id="rutEmpresa" value="<?= $rutEmpresa; ?>">
                                <input type="hidden" name="idVendedor" id="idVendedor" value="<?= $idEjecutiva; ?>">
                            </div>
                            <div class="contSelectEmpresaUsuario">
                                    <select name="selectUsuariosEmpresas" id="selectUsuariosEmpresas" class="selectSearch">
                                    <option value="">Usuarios</option>
                                    <?php for($i=0; $i<sizeof($usuarios); $i++){ ?>
                                    <option value="<?= $usuarios[$i][2] ?>"><?= $usuarios[$i][0]." ".$usuarios[$i][1] ?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <input type="hidden" id="origenNuevaCotizacion" value="cliente-vendedor">
                            <button type="button" class="btn-nuevaCotizacion" id="nuevaCotizacion">Generar Cotización</button>
                        </form>
                    </div>
                </article>
            </section>

            <section class="cont100">               
                <div class="cont100 FilaDatosUsuario" >
                    <table  id="example" class="table table-striped table-bordered tablas responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>Número de COT</th>
                                <th>Fecha cotización</th>
                                <th>Estado</th>
                                <th>Cant.</th>
                                <th>Monto Neto</th>
                                <th>Usuarios</th>
                                <th>Ejecutiva</th>
                                <th>Fecha estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            $cotizaciones = consulta_bd("oc, estado_id, nombre, apellido, cant_productos, fecha_creacion, fecha_modificacion, total, vendedor_id","cotizaciones","rut_empresa = '$rutEmpresa'","oc ASC");
                            if ($rutUsuario != 0 && $rutUsuario != "all") {
                                $cotizaciones = consulta_bd("oc, estado_id, nombre, apellido, cant_productos, fecha_creacion, fecha_modificacion, total,vendedor_id","cotizaciones","rut_empresa = '$rutEmpresa' AND rut = '$rutUsuario'","oc ASC");
                            }
                            foreach ($cotizaciones as $value) {
                                echo '
                                <tr>
                                    <td>'.$value[0].'</td>
                                    <td>'.$value[5].'</td>';
                                    $idEstado = $value[1];
                                    $estados = consulta_bd("nombre","estados","id = '$idEstado'","");
                                echo'
                                    <td>'.$estados[0][0].'</td>';

                                echo'
                                    <td>'.$value[4].'</td>
                                    <td>'.$value[7].'</td>
                                    <td>'.$value[2]." ". $value[3].'</td>';

                                    $idVendedor = $value[8];
                                    $vendedor = consulta_bd("nombre, apellido","vendedores","id = '$idVendedor'","");                                
                                echo'
                                    <td>'.$vendedor[0][0].' '.$vendedor[0][1].'</td>
                                    <td>'.$value[6].'</td>
                                    <td>
                                        <div>
                                        <a href="cotizaciones-vendedor/'.$value[0].'"><button class="btnDetalleTablas" rutEmpresa="">Detalle</button></a>            
                                        </div>
                                    </td>
                                </tr>';
                              }
                            ?>
                        </tbody>
                    </table>    
                </div>     
            </section>            
        <?php } else { ?>

            <section class="cont100">
                <h1 class="tituloEmpresas">Empresas</h1>
            </section>

            <section class="cont100">               
                <div class="cont100 FilaDatosUsuario" >
                    <table  id="example" class="table table-striped table-bordered tablas responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nombre Empresa</th>
                                <th>Rut Empresa</th>
                                <th>Cantidad de usuarios</th>
                                <th>Cotizaciones totales</th>
                                <th>Cotizaciones completadas</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($ejecutiva[0][2] == 1) {
                                $empresas = consulta_bd("nombre, rut","empresas","publicada = 1","nombre ASC");
                            }else{
                                $empresasAsociadas = consulta_bd("ev.id, e.rut","empresas_vendedores ev, empresas e","ev.vendedor_id = $idEjecutiva AND e.id = ev.empresa_id","");
                                $aux = 1;
                                $rutsEmpresas = "";
                                foreach($empresasAsociadas as $valRutEmpresa){ 
                                    if($aux == 1){
                                        if($valRutEmpresa != ""){
                                            $rutsEmpresas .= "'".$valRutEmpresa[1]."'";
                                            $aux = $aux + 1;
                                        }
                                        } else {
                                            if($valRutEmpresa != ""){
                                                $rutsEmpresas .= ",'".$valRutEmpresa[1]."'";
                                                $aux = $aux + 1;
                                            }
                                        }
                                }
                                $empresas = consulta_bd("nombre, rut","empresas","publicada = 1 AND rut IN($rutsEmpresas)","nombre ASC");
                            }
                            foreach ($empresas as $value) {
                                echo '
                                <tr>
                                    <td>'.$value[0].'</td>
                                    <td>'.$value[1].'</td>';
                                    $rutEmpresa = $value[1];
                                    $empresasCotizacionClientes = consulta_bd("COUNT(DISTINCT rut)","cotizaciones","rut_empresa = '$rutEmpresa'","rut_empresa ASC");
                                    echo'
                                        <td>'.$empresasCotizacionClientes[0][0].'</td>';
                                    $empresasCotizacionTotal = consulta_bd("rut_empresa","cotizaciones","rut_empresa = '$rutEmpresa'","rut_empresa ASC");
                                    $cantidadCotizacionTotal = count($empresasCotizacionTotal);
                                    echo'
                                        <td>'.$cantidadCotizacionTotal.'</td>';
                                    $empresasCotizacionCompletada = consulta_bd("rut_empresa","cotizaciones","rut_empresa = '$rutEmpresa' AND estado_id = 2","rut_empresa ASC");
                                    $cantidadCotizacionCompletada = count($empresasCotizacionCompletada);
                                    echo'
                                        <td>'.$cantidadCotizacionCompletada.'</td>
                                        <td>
                                            <div>
                                            <a href="clientes-vendedor/'.$value[1].'"><button class="btnDetalleTablas" rutEmpresa="">Detalle</button></a>            
                                            </div>
                                        </td>
                                </tr>';
                              }
                            ?>
                        </tbody>
                    </table>    
                </div>     
        </section>

        <?php } ?>

    </div>
</div>
<script>
    /*====================================================
=    LLAMANDO AL PLUGIN DATATABLE            =
====================================================*/

$(document).ready(function() {
    $('.tablas').DataTable({

    	"language":{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar por cualquier campo:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
                }
           });
} );
/*===== fIN DEL LLAMANDO AL PLUGIN DATATABLE  ======*/
$('#usuarioSelect').on("change", function(){
    let usuario = $("#usuarioSelect").val();
    let urlOriginal = $("#url").val();
    let url = urlOriginal+'?usuario='+usuario;
    window.location.href = url;    
});
</script>