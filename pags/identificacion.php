<?php if(qty_pro() > 0){ ?>
<?php } else { ?>
	<script>location.href = "404";</script>
<?php } ?>

<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li><a href="mi-carro">Mi carro</a></li>
          <li class="active">identificación</li>
        </ul>
    </div>
</div>

<div class="cont100">
    <div class="cont100Centro">
        <div class="filaPasos cont100">
        	<a class="" href="mi-carro">
            	<span class="paso">1</span>
                <span class="nomPaso">Mi Carro</span>
            </a>
            <a class="activo" href="javascript:void(0)">
            	<span class="paso">2</span>
                <span class="nomPaso">Identifícate</span>
            </a>
            <a class="" href="javascript:void(0)">
            	<span class="paso">3</span>
                <span class="nomPaso">Envío y Pago</span>
            </a>
        </div><!--Fin filaPasos -->
    </div>
</div>

<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        <div class="acceso1 titulo2">Identificación</div>
        <div class="acceso2 subtitulo2">Ingresa tu correo para realizar tu compra de manera rápida, fácil y segura</div>
        
        
        <div class="contBotonesIdentificacionResponsive">
        	<a href="javascript:void(0);" rel="inicioSesion">Inicio sesión</a>
            <a href="javascript:void(0);" rel="compraRapida">Compra rápida</a>
        </div>
        
        
        <div id="inicioSesion" class="cont50Identificacion">
        	<h3>Iniciar sesión</h3>
        	<div class="acceso3 cont100">
            	<?php 
				if(isset($_COOKIE[nombreUsuario]) and isset($_COOKIE[claveUsuario])){
					$checked = 'checked="checked"';
					$usuarioCookie = $_COOKIE[nombreUsuario];
					$claveCookie = base64_decode($_COOKIE[claveUsuario]);
					} else {
						$checked = '';
						}
					?>
               <form action="ajax/login.php" method="post" class="formInicioSesion">
                   <input type="text" id="email" name="email" class="campoGrande" value="<?= $usuarioCookie; ?>" placeholder="Ingrese su correo" />
                   <input type="password" id="clave" name="clave" class="campoGrande" value="<?= $claveCookie; ?>" placeholder="Ingrese su correo" />
                   <input type="hidden" name="origen" value="carro" />
                   <div class="cont100">
                   	<input type="checkbox" name="recordarCuenta" <?= $checked; ?> /> <span>Recordar mis datos</span>
                   </div>
                   <a href="javascript:void(0)" id="inicioSesionIdentificacion" class="btnFormCompraRapida">Iniciar sesión</a>
                   <div style="clear:both"></div>
               </form>
               	<div class="linea"></div>
                <a class="btnRegistro" href="registro">Crear cuenta</a>
           </div>
        </div><!--iniciar sesion -->
        
        
        <div id="compraRapida" class="cont50Identificacion">
        	<h3>Compra rapida</h3>
        	<div class="acceso3 cont100">
               <form action="<?php echo $url_base; ?>envio-y-pago" method="post" class="formCompraRapida1">
                   <input type="text" id="emailRapido" name="emailRapido" class="campoGrande center-block" placeholder="Ingrese su correo" />
                   <a href="javascript:void(0)" id="validarCorreoCompraRapida" class="btnFormCompraRapida">Validar correo</a>
                   <div style="clear:both"></div>
               </form>
           </div>
        </div><!--compra rápida -->
    </div>
</div>





<script type="text/javascript">
<?php
    if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}  
	$cart = $_COOKIE['cart_alfa_cm'];  
    $items = explode(',',$cart);
	foreach ($items as $item) {
        $contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
    }
    
	foreach ($contents as $prd_id=>$qty) {
        $filas = consulta_bd("pd.id, pd.sku, pd.nombre, p.nombre, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id = pd.producto_id and pd.id = $prd_id","");
	
		$idProductoInterior = $filas[0][0];
		if($filas[0][5] != '' and $filas[0][5] > 0){
			$valor = $filas[0][5];
		} else {
			$valor = $filas[0][4];
		}
        ?>
        ga('ec:addProduct', {'id': '<?= $filas[0][0]; ?>','name': '<?= $filas[0][2]; ?>','brand': 'no informada','price': '<?= $valor; ?>','quantity': <?= $qty; ?>});
		<?php
    }
?>
</script>
<script type="text/javascript">ga('ec:setAction','checkout', {'step': 2});</script>