<script>
<?php 
	if(!isset($_COOKIE['usuario_id']) or $usuario[perfilCliente] != 2){
		echo 'location.href = "inicio-sesion"';
	}
?>
</script>
<div class="cont100">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Agregar usuarios</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        <div class="titulosCuenta">
            <span>Agregar usuarios</span>
            <p>Podrás ver tus pedidos, historial de compra y editar tus datos personales y de envío.</p>
        </div>
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
             	
        
            <div class="cont100 datosCliente">
                
                 <?php 
        $usuariosCreados = consulta_bd("id, nombre, email, activo","clientes","tipo_registro = 'empresa' and parent = $usuarioID",""); 
        $cantCreados = mysqli_affected_rows($conexion);
            if($cantCreados > 0){
        ?> 
            	<h3 class="subtitulo">Usuarios agregados</h3>
                
                <div class="cont100 contUsuariosRegistrados">
                    <?php for($i=0; $i<sizeof($usuariosCreados); $i++) { 
                    if($usuariosCreados[$i][3] == 1){
                        $estadoUsuario = "activo";
                    } else {
                        $estadoUsuario = "inactivo";
                    }
                    ?>
                    <div class="filaUsuariosAgregados" id="esclavo_<?= $usuariosCreados[$i][0];?>">
                        
                        <div class="estadoUsuario <?= $estadoUsuario; ?>"></div>
                             
                        <span class="nombreUsuarioRegistrado"><strong><?= $usuariosCreados[$i][1]; ?></strong></span>
                        <span class="correoUsuarioRegistrado"><?= $usuariosCreados[$i][2]; ?></span>
                        
                        <div class="botonesEditar">
                            <a data-fancybox data-type="ajax" data-src="pags/editarUsuario.php?id=<?= $usuariosCreados[$i][0];?>" href="javascript:;" class="editarDireccion fancybox.ajax">Editar</a>
                            <a href="javascript:void(0)" class="btnEliminaUsuario" rel="<?= $usuariosCreados[$i][0];?>">Eliminar</a>
                        </div>
                    </div><!--fin filaUsuariosAgregados -->
                    <?php } ?> 
                        
                </div>
                <?php } else { ?>
                <div class="cont100 conSinUauarios">
                    <h2>Aún no has agregado usuarios.</h2>
                    <p>Los usuarios que designes podrán comprar a través de PROVIT, <br>ver el historial de compra, los pedidos activos, productos guardados, etc...</p>
                    <a href="agregar-usuarios#subtitulo" id="" class="btnFormRegistro">CREAR USUARIO</a>
                </div>
                <?php } ?>
                
                
                <h3 id="subtitulo" class="subtitulo">Agregar nuevo usuario</h3>
                <form action="ajax/registro.php?origen=1" method="post" class="formularioLogin bl_form" id="formularioRegistro">

                    <div class="cont100">
                        <input type="hidden" name="tipo_registro" value="empresa" >
                        <input type="hidden" name="parent" value="<?= $usuarioID; ?>"> 
                                
                    </div>
                    <div class="cont50Identificacion">
                        <div class="acceso3 cont100">
                           <input type="text" id="nombre" name="nombre" class="campoGrande2 label_better" placeholder="Ingrese su Nombre" data-new-placeholder="Nombre" />
                           <input type="text" id="rut" name="rut" class="campoGrande2 label_better" placeholder="Ingrese su Rut" data-new-placeholder="Rut"/>

                           <input type="password" id="clave" name="clave" class="campoGrande2 label_better" placeholder="Ingrese su clave" data-new-placeholder="Clave" />
                           <div style="clear:both"></div>
                       </div>
                    </div><!--iniciar sesion -->

                    <div class="cont50Identificacion">
                        <div class="acceso3 cont100">
                           <input type="text" id="telefono" name="telefono" class="campoGrande2 label_better" placeholder="Ingrese su Teléfono" data-new-placeholder="Teléfono" />
                           <input type="text" id="email" name="email" class="campoGrande2 label_better" placeholder="Ingrese su Email" data-new-placeholder="Email" />

                           <input type="password" id="clave2" name="clave2" class="campoGrande2 label_better" placeholder="Repita su clave" data-new-placeholder="Repita su clave"/>
                           <div style="clear:both"></div>
                       </div>
                    </div><!--iniciar sesion -->

                    <a href="javascript:void(0)" id="btnRegistro" class="btnFormRegistro">CREAR USUARIO</a>
                </form>
            
            </div>
            
            <div class="cont100 direcciones">
            </div>
        
        </div><!--fin contenidoMiCuenta-->
        
      
               
    </div>
</div>
