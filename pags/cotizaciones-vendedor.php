<?php 
	if(!isset($_COOKIE['Vendedor_id'])){
		echo 'location.href = "home?error=Debes iniciar sesión en la sección para vendedores en el footer de la pagina"';
	}
    $idEjecutiva = $_COOKIE['Vendedor_id'];
    $ejecutiva = consulta_bd("nombre, apellido, perfil","vendedores","id = '$idEjecutiva'","");
    $ocCot = (isset($_GET['cot'])) ? mysqli_real_escape_string($conexion, $_GET['cot']) : 0;
    $rutUsuario = (isset($_GET['usuario'])) ? mysqli_real_escape_string($conexion, $_GET['usuario']) : 0;
    $url = "clientes-vendedor/".$ocCot;
    $empresasAsociadas = consulta_bd("ev.id, e.rut","empresas_vendedores ev, empresas e","ev.vendedor_id = $idEjecutiva AND e.id = ev.empresa_id","");
    $aux = 1;
    $rutsEmpresas = "";
    foreach($empresasAsociadas as $valRutEmpresa){ 
        if($aux == 1){
            if($valRutEmpresa != ""){
                $rutsEmpresas .= "'".$valRutEmpresa[1]."'";
                $aux = $aux + 1;
            }
            } else {
                if($valRutEmpresa != ""){
                    $rutsEmpresas .= ",'".$valRutEmpresa[1]."'";
                    $aux = $aux + 1;
                }
            }
    }
?>
<style>
    nav li {
     line-height: 48px;
    }
    a:focus, a:hover {
        text-decoration: none;
    }
    .breadcrumb{
        margin-bottom: 5px;
        background-color: unset;
    }
    .breadcrumb span.active{
        color: #009be5;
    }
    .dataTables_length > label{
        max-width: 70px;
    }
    div.dataTables_wrapper div.dataTables_filter input {
        width: 300px;
        border: solid 1px #009be5;
        border-radius: 15px;
        -moz-border-radius: 15px;
        -webkit-border-radius: 15px;
        height: 40px;
        font-family: "Raleway-Medium";
        font-size: 14px;
    }
    .dataTables_length div.selector{
        border: solid 1px #009be5;
    }
    @media (max-width: 1050px) {
        div.dataTables_wrapper div.dataTables_filter input {
            width: unset;
        }
    }
    @media (max-width: 500px) {
        div.dataTables_wrapper div.dataTables_filter label{
            display: flex;
            flex-flow: wrap;
            justify-content: center;
            align-items: center;
        }
    }
    #example, #cotizarProducto {
        border: none;
    }
    #example td, #cotizarProducto td {
        border: none;
        height: 50px;
        font-family: "Raleway-Medium";
        font-size: 13px;
        color: #888888;
        line-height: 40px;
    }
    #example th, #cotizarProducto th {
        border: none;
        height: 40px;
        font-family: "Raleway-SemiBold";
        font-size: 14px;
        color: #555555;
        line-height: 14px;
    }
    .page-item.active .page-link{
        background-color: #009be5;
        border-color: #009be5;
    }
    .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
        background-color:#009be5;
    }
    .table-striped>tbody>tr:nth-of-type(odd){
        background-color: #f4f4f4;
    }
    .swal2-popup {
        font-size: 1.6rem !important;
        }
</style>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <span class="active">Cotizaciones</span>
        </ul>
    </div>
</div>

<div class="cont100">
    <div class="cont100Centro">
        
        <?php include("pags/menuVendedores.php"); ?>

        <?php
        if ($ocCot !== 0) { ?>
            <section class="cont100 cotizacionesDetalle">
                <article>
                    <h1 class="tituloEmpresas">Detalle cotizaciones</h1>
                </article>
                <article class="contInputsTablas">
                    <?php
                        $cotizacionDetalle = consulta_bd("oc, estado_id, nombre, apellido, cant_productos, fecha_creacion, fecha_modificacion,cliente_id,empresa,id,vendedor_id,total,total_unitario, nota_especial","cotizaciones","oc = '$ocCot'","");
                    ?>
                    <input type="hidden" name="estadoIdCotizacion" id="estadoIdCotizacion" value="<?= $cotizacionDetalle[0][1] ?>">
                    <div class="contInputTables">
                        <!-- <label for="nombre">Nombre</label> -->
                        <input type="text" id="oc" name="oc" class="campoGrandeDisable label_better" value="<?= $cotizacionDetalle[0][0]; ?>" placeholder="Cot" data-new-placeholder="Cot" disabled/>
                    </div>
                    <div class="contInputTables">
                        <!-- <label for="nombre">Nombre</label> -->
                        <input type="text" id="fecha" name="fecha" class="campoGrandeDisable label_better" value="<?= $cotizacionDetalle[0][5]; ?>" placeholder="Fecha" data-new-placeholder="Fecha" disabled/>
                    </div>

                    <div class="contInputTables">
                        <input type="hidden" name="url" id="url" value="<?= $url ?>">
                        <label for="usuarioSelect">Estado</label>
                        <select style="width: 80%;" class="selectorFiltrosEspeciales selectUsuarios" id="estadosSelect">
                            <?php
                            $idEstado = $cotizacionDetalle[0][1];
                            $estadosSelect = consulta_bd("id, nombre","estados","","");
                            foreach ($estadosSelect as $value) {
                                $selectorELegido1 = "";       
                                if ($value[0] == $idEstado) {
                                    $selectorELegido1 = "selected='selected'";
                                }                         
                                echo "<option ".$selectorELegido1." value='".$value[0]."'>".$value[1]."</option>";
                            } ?>
                        </select>
                    </div>
                    <div class="contInputTables">
                        <!-- <label for="nombre">Nombre</label> -->
                        <input type="text" id="usuario" name="usuario" class="campoGrandeDisable label_better" value="<?= $cotizacionDetalle[0][2]." ".$cotizacionDetalle[0][3]; ?>" placeholder="Usuario" data-new-placeholder="Usuario" disabled/>
                    </div>
                    <div class="contInputTables">
                        <!-- <label for="nombre">Nombre</label> -->
                        <input type="text" id="cantidadProductos" name="cantidadProductos" class="campoGrandeDisable label_better" value="<?= $cotizacionDetalle[0][4] ?>" placeholder="Cantidad productos" data-new-placeholder="Cantidad productos" disabled/>
                    </div>
                    <div class="contInputTables">
                        <!-- <label for="nombre">Nombre</label> -->
                        <input type="text" id="montoNeto" name="montoNeto" class="campoGrandeDisable label_better" value="<?= $cotizacionDetalle[0][11] ?>" placeholder="Monto" data-new-placeholder="Monto" disabled/>
                    </div>
                    <div class="contInputTables">
                        <!-- <label for="nombre">Nombre</label> -->
                        <input type="text" id="empresa" name="empresa" class="campoGrandeDisable label_better" value="<?= $cotizacionDetalle[0][8] ?>" placeholder="Empresa" data-new-placeholder="Empresa" disabled/>
                    </div>
                    <input type="hidden" name="idCotizacion" id="idCotizacion" value="<?= $cotizacionDetalle[0][9] ?>">
                </article>

                <article class="botonesTablas">
                <?php if ($cotizacionDetalle[0][1] == 1) {?>
                    <div class="botonTabla">
                        <a class="fancybox" href="#modalAgregarProducto"><button type="button" class="btn-nuevaCotizacion">Agregar Producto</button></a>
                    </div>
                    <div style="display: none;;">
                        <a class="fancybox" id="botonSustituir" href="#modalSustituirProducto"><button class="btnDetalleTablas">Sustituto</button></a>            
                    </div>
                    <div id="modalAgregarProducto" class="inicioSesionCarroPopUp buscadorDeProductos" style="width:640px;display: none;">
                        <h3>Producto nuevo</h3>
                        <h5>Busca el producto que quieres agregar</h5>
                        <h5>con su número de Sku o por nombre.</h5>
                            <!-- Buscador -->
                        <div class="contDelBuscador">
                            <div class="buscadorProductoCotizar">
                                <!-- <form id="formBuscadorProductoCotizar" method="post" action="ajax/busquedaProductos.php" style="float:none;"> -->
                                    <input type="text" name="busqueda" id="busquedaCotizar" class="campo_buscador" placeholder="Buscar productos">
                                    <input type="hidden" name="accion" value="buscar">
                                    <input class="btn_search" type="button" value="" name="b">
                                <!-- </form> -->
                            </div>
                            <div class="botonTabla">
                                <button type="button" para="agregar" idCotizacion="<?= $cotizacionDetalle[0][9];?>" id="buscarProductosCotizar" class="btn-nuevaCotizacion">Buscar</button>
                            </div>
                        </div>
                        
                        <div class="contProductosBuscados">
                            
                        </div>
                    <?php } ?>
                    <?php if ($cotizacionDetalle[0][1] == 2) {?>
                        <div style="display: none;;">
                            <a class="fancybox" id="botonSustituir" href="#modalSustituirProducto"><button class="btnDetalleTablas">Sustituto</button></a>            
                        </div>
                        <div class="botonTabla">
                            <a href="tienda/voucher/PdfCotizacion.php?oc=<?php echo $ocCot; ?>" target="_blank"><button type="button" class="btn-nuevaCotizacion btn-nuevaCotizacion-gris">Descargar PDF</button></a>
                            <!--<a href="<?php echo $url_base; ?>home">Seguir en el sitio</a>-->
                        </div>
                        <div class="botonTabla">
                            <a href="tienda/voucher/PdfCotizacion.php?oc=<?php echo $ocCot; ?>&envio=cliente"><button type="button" class="btn-nuevaCotizacion btn-nuevaCotizacion-gris">Enviar a Cliente</button></a>
                            <!--<a href="<?php echo $url_base; ?>home">Seguir en el sitio</a>-->
                        </div>
                    <?php } ?>
                </article>
            </section>

            <section class="cont100">               
                <div class="cont100 FilaDatosUsuario table-responsive" >
                    <table id="cotizarProducto" class="table table-striped table-bordered responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>SKU</th>
                                <th>Foto</th>
                                <th>Nombre</th>
                                <th>Logo</th>
                                <th>Cant.</th>
                                <th>Costo Prod.</th>
                                <th>Costo logo</th>
                                <th>Margen utilidad</th>
                                <th>Precio Unitario Neto</th>
                                <th>Precio Final Neto</th>
                                <th>Último precio Enviado</th>
                                <th>Utilidad Total</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>    
                </div>
                <div id="modalSustituirProducto" class="inicioSesionCarroPopUp buscadorDeProductos" style="width:640px;display: none;">
                    <h3>Sustituir Producto</h3>
                    <h5>Realiza la sustitución del producto buscandolo</h5>
                    <h5>con su número de Sku o por nombre.</h5>
                        <!-- Buscador -->
                    <div class="contDelBuscador">
                        <div class="buscadorProductoCotizar">
                            <!-- <form id="formBuscadorProductoCotizar" method="post" action="ajax/busquedaProductos.php" style="float:none;"> -->
                                <input type="text" name="busqueda" id="busquedaCotizar2" class="campo_buscador" placeholder="Buscar productos">
                                <input type="hidden" name="accion" value="buscar">
                                <input type="hidden" name="idProd" id="idProdEnvio" value="">
                                <input class="btn_search" type="button" value="" name="b">
                            <!-- </form> -->
                        </div>
                        <div class="botonTabla">
                            <button type="button" para="sustituir" idCotizacion="<?= $cotizacionDetalle[0][9];?>" id="buscarProductosCotizar2" class="btn-nuevaCotizacion">Buscar</button>
                        </div>
                    </div>
                    
                    <div class="contProductosBuscadosSustituir">
                        
                    </div>
                </div>    
            </section>

            <section class="cont100 contInfoTablas">
                <article class="calugasInfo1">
                        <h1>Nota especial</h1>
                        <div class="contenedorCalugaInfo1">
                            <textarea class="notaEspecial" name="notaEspecial" id="notaEspecial" rows="6" cols="25" oc="<?= $ocCot ?>"><?= $cotizacionDetalle[0][13] ?></textarea>
                        </div>
                        <h1>Datos para emitir Orden de Compra</h1>
                        <div class="contenedorCalugaInfo1">
                            <textarea class="notaEspecial" name="datosOrden" id="datosOrden" rows="6" cols="25" campo="datos_orden" idVendedor="<?= $idEjecutiva ?>"><?= trim(strip_tags($datosOrden)); ?></textarea>
                        </div>
                        <h1>Forma de pago</h1>
                        <div class="contenedorCalugaInfo1">
                             <textarea class="notaEspecial" name="formaPago" id="formaPago" rows="6" cols="25" campo="forma_pago" idVendedor="<?= $idEjecutiva ?>"><?= trim(strip_tags($formaPago)); ?></textarea>
                        </div>
                        <h1>Datos de banco</h1>
                        <div class="contenedorCalugaInfo1">
                            <textarea class="notaEspecial" name="datosBanco" id="datosBanco" rows="6" cols="25" campo="datos_banco" idVendedor="<?= $idEjecutiva ?>"><?= trim(strip_tags($datosBanco)); ?></textarea>
                        </div>
                </article>
                <article class="calugasInfo2">
                    <div class="contenedorCalugaInfo2">
                        <p class="precioNumero">$<?= number_format($cotizacionDetalle[0][11],0,",","."); ?></p>
                        <p class="precioReferencia">Total Neto</p>
                    </div>
                    <div class="contenedorCalugaInfo2">
                        <p class="precioNumero">$<?= number_format($cotizacionDetalle[0][12],0,",","."); ?></p>
                        <p class="precioReferencia">Utilidad Total</p>
                    </div>
                </article>
            </section>            
        <?php } else { ?>

            <section class="cont100">
                <article>
                    <h1 class="tituloEmpresas">Cotizaciones</h1>
                </article>
                <article class="botonesTablas">
                    <div class="botonTabla">
                        <a class="fancybox" href="#modalNuevaCotizacion"><button type="button" class="btn-nuevaCotizacion">Nueva cotización</button></a>
                    </div>
                    <div id="modalNuevaCotizacion" class="inicioSesionCarroPopUp" style="width:440px;display: none;">
                        <h3>Nueva cotización</h3>
                        <h5>Elije la empresa y el usuario</h5>
                        <?php
                        if ($ejecutiva[0][2] == 1) {
                            $empresa = consulta_bd("nombre, rut", "empresas", "publicada = 1", "nombre asc");
                        }else{
                            $empresa = consulta_bd("nombre, rut", "empresas", "publicada = 1 AND rut IN($rutsEmpresas)", "nombre asc");
                        }
                        $empresaCont = mysqli_affected_rows($conexion);
                        ?>
                        <form id="formNuevaCotizacion" action="ajax/ajaxNuevaCotizacion.php" method="post">
                            <div class="contSelectEmpresaUsuario">
                                <select name="rutEmpresa" id="rutEmpresa" class="selectSearch">
                                    <option value="">Empresas</option>
                                    <?php for($i=0; $i<sizeof($empresa); $i++){ ?>
                                    <option value="<?= $empresa[$i][1] ?>"><?= $empresa[$i][0] ?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="contSelectEmpresaUsuario">
                                <select name="selectUsuariosEmpresas" id="selectUsuariosEmpresas" class="selectSearch">
                                    <option value="">Usuarios</option>
                                </select>
                            </div>
                            <input type="hidden" id="origenNuevaCotizacion" value="cotizacion-vendedor">
                            <input type="hidden" name="idVendedor" id="idVendedor" value="<?= $idEjecutiva; ?>">
                            <button type="button" class="btn-nuevaCotizacion" id="nuevaCotizacion">Generar Cotización</button>
                        </form>
                    </div>
                </article>
            </section>

            <section class="cont100">               
                <div class="cont100 FilaDatosUsuario table-responsive" >
                    <input type="hidden" id="urlBase" value="<?= $url_base ?>">
                    <table id="example" class="table table-striped table-bordered tablas responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>Número de COT</th>
                                <th>Fecha cotización</th>
                                <th>Estado</th>
                                <th>Cant</th>
                                <th>Monto neto</th>
                                <th>Empresa</th>
                                <th>Usuarios</th>
                                <th>Fecha estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($ejecutiva[0][2] == 1) {
                                $cotizaciones = consulta_bd("oc, estado_id, nombre, apellido, cant_productos, fecha_creacion, fecha_modificacion,empresa,total","cotizaciones","asociada = 1","oc ASC");
                            }else{
                                $cotizaciones = consulta_bd("oc, estado_id, nombre, apellido, cant_productos, fecha_creacion, fecha_modificacion,empresa,total","cotizaciones","asociada = 1 AND rut_empresa IN($rutsEmpresas)","oc DESC");
                                $cotizacionesNOasociadas = consulta_bd("oc, estado_id, nombre, apellido, cant_productos, fecha_creacion, fecha_modificacion,empresa,total, rut_empresa","cotizaciones","asociada = 0","oc DESC");
                            }
                            foreach ($cotizacionesNOasociadas as $value) {
                                echo '
                                <tr class="trCotizacion">
                                    <td>'.$value[0].'</td>
                                    <td>'.$value[5].'</td>';
                                    $idEstado = $value[1];
                                    $estados = consulta_bd("nombre","estados","id = '$idEstado'","");
                                echo'
                                    <td>'.$estados[0][0].'</td>';

                                echo'<td>'.$value[4].'</td>
                                    <td>'.$value[8].'</td>
                                    <td>'.$value[7].'</td>
                                    <td>'.$value[2]." ".$value[3].'</td>
                                    <td>'.$value[6].'</td>';
                                    echo'
                                        <td>
                                            <div>
                                            <button class="btnDetalleTablas btnSinStockTablas" id="sinVendedor" oc="'.$value[0].'" 
                                            idVendedor="'.$idEjecutiva.'" rutEmpresa="'.$value[9].'">Sin Vendedor</button>            
                                            </div>
                                        </td>
                                </tr>';
                              }
                            foreach ($cotizaciones as $value) {
                                echo '
                                <tr class="trCotizacion">
                                    <td>'.$value[0].'</td>
                                    <td>'.$value[5].'</td>';
                                    $idEstado = $value[1];
                                    $estados = consulta_bd("nombre","estados","id = '$idEstado'","");
                                echo'
                                    <td>'.$estados[0][0].'</td>';

                                echo'<td>'.$value[4].'</td>
                                    <td>'.$value[8].'</td>
                                    <td>'.$value[7].'</td>
                                    <td>'.$value[2]." ".$value[3].'</td>
                                    <td>'.$value[6].'</td>';
                                    echo'
                                        <td>
                                            <div>
                                            <a href="cotizaciones-vendedor/'.$value[0].'"><button class="btnDetalleTablas" cot="">Detalle</button></a>            
                                            </div>
                                        </td>
                                </tr>';
                              }
                            ?>
                        </tbody>
                    </table>    
                </div> 
        </section>

        <?php } ?>

    </div>
</div>
<script>
    /*====================================================
=    LLAMANDO AL PLUGIN DATATABLE            =
====================================================*/

$(document).ready(function() {
    $('.tablas').DataTable({
        "order": [[ 1, "desc" ]],
    	"language":{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar por cualquier campo:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
                }
           });
           var idCotizacion = $('#idCotizacion').val();
            var dataTable = $('#cotizarProducto').DataTable({
                "processing": true,
                "serverSide": true,
                "language":{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar por sku/nombre/logo:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
                },
                "order":[],
                "ajax":{
                    url:"ajax/productoCotizacion.php",
                    data: { idCotizacion:idCotizacion },
                    type:"POST",
                    // success: function(data){
                    //     console.log(data)
                    // },
                },
                createdRow:function(row, data, rowIndex)
                {
                    var tdDisable = "";
                    let sku = data[1];
                    // console.log(data[1]+"<br>"+idCotizacion);
                    $.ajax({
                        url: "ajax/consultaStockTabla.php",
                        method: "post",
                        'async': false,
                        data:{ idCotizacion:idCotizacion, sku:sku },
                        success: function(resp){
                            if (resp == "sin") {
                                tdDisable = "tdDisable";
                            }
                        }
                    });
                    $.each($('td', row), function(colIndex){
                        if(colIndex == 1)
                        {
                            $(this).attr('data-name', 'codigo');
                            $(this).attr('class', 'codigo '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 2)
                        {
                            $(this).attr('data-name', 'thumbs');
                            $(this).attr('class', 'thumbs '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 3)
                        {
                            $(this).attr('data-name', 'nombre');
                            $(this).attr('class', 'nombre '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 4)
                        {
                            $(this).attr('data-name', 'logo');
                            $(this).attr('class', 'logo '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 5)
                        {
                            $(this).attr('data-name', 'cantidad');
                            $(this).attr('class', 'cantidad '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 6)
                        {
                            $(this).attr('data-name', 'costo_producto');
                            $(this).attr('class', 'costo_producto '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                            $(this).attr('data-pk2', data[1]);
                        }
                        if(colIndex == 7)
                        {
                            $(this).attr('data-name', 'costo_logo');
                            $(this).attr('class', 'costo_logo '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 8)
                        {
                            $(this).attr('data-name', 'margen_utilidad');
                            $(this).attr('class', 'margen_utilidad '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 9)
                        {
                            $(this).attr('data-name', 'precio_unitario_neto');
                            $(this).attr('class', 'precio_unitario_neto '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 10)
                        {
                            $(this).attr('data-name', 'precio_final_neto');
                            $(this).attr('class', 'precio_final_neto '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 11)
                        {
                            $(this).attr('data-name', 'ultimo_precio_enviado');
                            $(this).attr('class', 'ultimo_precio_enviado '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 12)
                        {
                            $(this).attr('data-name', 'utilidad_total');
                            $(this).attr('class', 'utilidad_total '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                        if(colIndex == 13)
                        {
                            $(this).attr('data-name', 'botones');
                            $(this).attr('class', 'botones '+tdDisable);
                            $(this).attr('data-type', 'text');
                            $(this).attr('data-pk', data[0]);
                        }
                    });
                }
            });
            $('#cotizarProducto').on( 'click', 'tbody tr', function (e) {
                var idCotProd = $(this).find(".prod-cotizacion").attr("idprodcotizar");
            } );
            let estadoId = $('#estadoIdCotizacion').val();
            if(estadoId == 1 || estadoId == 2){
                $('#cotizarProducto').editable({
                    container:'body',
                    selector:'td.costo_producto',
                    url:'ajax/actualizarTablas.php',
                    params: function(params) {
                        //originally params contain pk, name and value
                        params.pk2 = $(this).attr("data-pk2");
                        return params;
                    },
                    title:'Costo Producto',
                    type:'POST',
                    validate:function(value){
                        if($.trim(value) == '')
                        {
                            return 'Este campo es requerido';
                        }
                    },
                    success: function(r){
                        window.location.reload();
                    }
                });
                $('#cotizarProducto').editable({
                    container:'body',
                    selector:'td.cantidad',
                    url:'ajax/actualizarTablas.php',
                    title:'Cantidad',
                    type:'POST',
                    validate:function(value){
                        if($.trim(value) == '')
                        {
                            return 'Este campo es requerido';
                        }
                    },
                    success: function(r){
                        window.location.reload();
                    }
                });
                $('#cotizarProducto').editable({
                    container:'body',
                    selector:'td.costo_logo',
                    url:'ajax/actualizarTablas.php',
                    title:'Costo Logo',
                    type:'POST',
                    validate:function(value){
                        if($.trim(value) == '')
                        {
                            return 'Este campo es requerido';
                        }
                    },
                    success: function(r){
                        window.location.reload();
                    }
                });
                $('#cotizarProducto').editable({
                    container:'body',
                    selector:'td.margen_utilidad',
                    url:'ajax/actualizarTablas.php',
                    title:'Margen de utilidad',
                    type:'POST',
                    validate:function(value){
                        if($.trim(value) == '')
                        {
                            return 'Este campo es requerido';
                        }
                    },
                    success: function(r){
                        window.location.reload();
                    }
                });
            }
 
            // $.ajax({
            //     url: "ajax/productoCotizacion.php",
            //     method: "post",
            //     data: { idCotizacion:idCotizacion },
            //     success: function(resp){
            //         console.log(resp);
            //     }
            // });
} );
/*===== fIN DEL LLAMANDO AL PLUGIN DATATABLE  ======*/


$('#usuarioSelect').on("change", function(){
    let usuario = $("#usuarioSelect").val();
    let urlOriginal = $("#url").val();
    let url = urlOriginal+'?usuario='+usuario;
    window.location.href = url;    
});
</script>

<?php 
  if(isset($_GET['filtro'])){
    $filtro = $_GET['filtro'];
    echo "
    <script>
    $(document).ready( function() {
        $('.FilaDatosUsuario').children('#example_wrapper').children().children().children('#example_filter').children('label').children('input').val('".$filtro."').keyup();
    });
    </script>";
}
?>