
 <div class="cont100 contBreadCrumbs" style="margin-bottom:0;">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Éxito Contacto</li>
        </ul>
    </div>
</div><!--Fin breadcrumbs -->



<div class="cont100">
    <div class="contCentroPagFijas">
        <div class="contTxtParrafo">
        <div class="tituloContacto">Contáctanos</div>
            
            <div class="cont100 contFormServ" style="margin-top:0;">
                <div class="cont100" style="text-align:center;">
                    <img class="imgContactoExito" src="img/imgExitoContacto.png">
                </div>
                <div class="textoContactoEnviado">Su mensaje ha sido enviado con éxito<br><br>¡Muchas gracias!</div>
                <br>
                <p style="text-align:center;">En breve recibira una respuesta.</p>
                <div class="cont100" style="text-align:center;">
                    <a href="home" class="btnFormServ2" id="">Ir al home</a>
                </div>
            </div>
        </div>
    </div>
</div>
