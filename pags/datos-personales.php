<script>
<?php 
	if(!isset($_COOKIE['usuario_id'])){
		echo 'location.href = "inicio-sesion"';
	}
?>
</script>
<div class="cont100 contBreadCrumbs">
    <div class="cont100Centro">
        <ul class="breadcrumb">
          <li><a href="home">Home</a></li>
          <li class="active">Datos personales</li>
        </ul>
    </div>
</div>



<div class="cont100">
    <div class="cont100Centro identificacionCentrado">
        
        
        <?php include("pags/menuMiCuenta.php"); ?>
        
        <div class="contenidoMiCuenta">
            
<?php $cliente = consulta_bd("id, nombre, apellido, rut, telefono, email, dia, mes, ano","clientes","id = ".$_COOKIE['usuario_id'],""); ?>        	
            <div class="datosCliente">
            	<div class="titulosCuenta">
                    <span>Datos personales</span>
                    <p>Modifica tus datos personales a continuación para que tu cuenta esté actualizada.</p>
                </div><!--fin titulosCuenta-->
                
                <form action="ajax/actualizarDatos.php" method="post" id="formularioActualizar" class="bl_form">
                    <div class="cont100 FilaDatosUsuario">
                        <div class="cont100">
                            <!-- <label for="nombre">Nombre</label> -->
                            <input type="text" id="nombre" name="nombre" class="campoGrande2 label_better" value="<?= $cliente[0][1]; ?>" placeholder="Nombre" data-new-placeholder="Nombre"/>
                        </div>
                        <div class="cont100">
                            <!-- <label for="apellido">Apellidos</label> -->
                            <input type="text" id="apellido" name="apellido" class="campoGrande2 label_better" value="<?= $cliente[0][2]; ?>" placeholder="Apellidos" data-new-placeholder="Apellidos"/>
                        </div>
                        <div class="cont100">
                            <!-- <label for="rut">Rut</label> -->
                            <input type="text" id="rut" name="rut" class="campoGrande2 label_better" value="<?= $cliente[0][3]; ?>" placeholder="Rut" data-new-placeholder="Rut"/>
                        </div>
                        <div style="clear:both"></div>

                        <div class="cont100">
                            <!-- <label for="telefono">Teléfono</label> -->
                            <input type="text" id="telefono" name="telefono" class="campoGrande2 label_better" value="<?= $cliente[0][4]; ?>" placeholder="Teléfono" data-new-placeholder="Teléfono" />
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    
                    
                    
                    <div class="titulosCuenta">
                        <span>Fecha de nacimiento</span>
                        <p>Recopilamos la fecha de nacimiento. ¡Te haremos un regalo en tu cumpleaños!</p>
                    </div><!--fin titulosCuenta-->
                    
                     
                    <div class="cont100 FilaDatosUsuario">
                        <div class="cumple">
                            <label for="dia">Día</label>
                            <select name="dia" id="dia">
                                <option value="">Dia</option>
                                <?php 
                                $final = 32;
                                $inicio = 1;
                                for($i=$inicio; $i<$final; $i++) {
                                 ?> 
                                <option value="<?= $i; ?>" <?php if($i == $cliente[0][6]){echo "selected";}?>><?= $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="cumple">
                            <label for="mes">Mes</label>
                            <select name="mes" id="mes">
                                <option>Mes</option>
                                <option value="Enero" <?php if($cliente[0][7] == "Enero"){echo "selected";}?>>Enero</option>
                                <option value="Febrero" <?php if($cliente[0][7] == "Febrero"){echo "selected";}?>>Febrero</option>
                                <option value="Marzo" <?php if($cliente[0][7] == "Marzo"){echo "selected";}?>>Marzo</option>
                                <option value="Abril" <?php if($cliente[0][7] == "Abril"){echo "selected";}?>>Abril</option>
                                <option value="Mayo" <?php if($cliente[0][7] == "Mayo"){echo "selected";}?>>Mayo</option>
                                <option value="Junio" <?php if($cliente[0][7] == "Junio"){echo "selected";}?>>Junio</option>
                                <option value="Julio" <?php if($cliente[0][7] == "Julio"){echo "selected";}?>>Julio</option>
                                <option value="Agosto" <?php if($cliente[0][7] == "Agosto"){echo "selected";}?>>Agosto</option>
                                <option value="Septiembre" <?php if($cliente[0][7] == "Septiembre"){echo "selected";}?>>Septiembre</option>
                                <option value="Octubre" <?php if($cliente[0][7] == "Octubre"){echo "selected";}?>>Octubre</option>
                                <option value="Noviembre" <?php if($cliente[0][7] == "Noviembre"){echo "selected";}?>>Noviembre</option>
                                <option value="Diciembre" <?php if($cliente[0][7] == "Diciembre"){echo "selected";}?>>Diciembre</option>
                            </select>
                        </div>
                        <div class="cumple">
                            <label for="ano">Año</label>
                            <select name="ano" id="ano">
                                <option>Año</option>
                                <?php 
                                $actual = date(Y);
                                $actual = $actual - 10;
                                $inicio = $actual - 90;
                                for($i=$inicio; $i<$actual; $i++) {
                                 ?> 
                                <option value="<?= $i; ?>" <?php if($cliente[0][8] == $i){echo "selected";}?>><?= $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                   
                    
                    
                    
                    
                    <div class="titulosCuenta">
                        <span>Datos de acceso</span>
                        <p>Puedes modificar tus datos de acceso cuando desees.</p>
                    </div><!--fin titulosCuenta-->
                    
                    <div class="cont100 FilaDatosUsuario">
                        <div class="cont100">
                           <!-- <label for="email">Email</label> -->
                           <input type="text" id="email" name="email" class="campoGrande2 label_better" value="<?= $cliente[0][5]; ?>" placeholder="Email" data-new-placeholder="Email" />
                       </div>
                        <div class="cont100">
                           <!-- <label for="password">Contaseña</label> -->
                           <input type="password" id="clave" name="clave" class="campoGrande2 label_better" placeholder="Ingrese su clave" data-new-placeholder="Clave"/>
                       </div>
                       <div class="cont100">
                           <!-- <label for="clave2">Repita su clave</label> -->
                           <input type="password" id="clave2" name="clave2" class="campoGrande2 label_better" placeholder="Repita su clave" data-new-placeholder="Repita su clave" />
                       </div>    
                    </div>
                    
                    
                    
                    <div class="cont100">
                    	<a href="javascript:void(0)" id="btnActualizar" style="margin-right:16px;" class="btnActualizarMicuenta">Guardar cambios</a>
                    </div>
                    
                </form>
                
            </div> <!--fin columna datos-->
            
            <?php include("pags/tipsMiCuenta.php");?> 
        
        </div><!--fin contenidoMiCuenta-->
        
      
               
    </div>
</div>
