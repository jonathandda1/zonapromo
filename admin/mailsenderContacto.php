<?php 
header('Content-type: text/html; charset=utf-8');
include("admin/conf.php");
include 'Mailin.php';
/********************************************************\
|  Alfastudio Ltda - Mail sender V0.5			 		 |
|  Fecha Modificacion: 16/06/2011		                 |
|  Todos los derechos reservados © Alfastudio Ltda 2011  |
|  Prohibida su copia parcial o total, 					 |
|  venta, comercializacion o distribucion				 |
|  https://www.moldeable.com/                             |
\********************************************************/

//******************Cambiar sólo estos valores*************************//



$nombre_sitio = opciones("nombre_cliente");
$nombre_corto = opciones("dominio");
$noreply = "no-reply@".$nombre_corto;
$url_sitio = "https://".$nombre_corto;
$correo_venta = opciones("correo_venta");
$color_logo = opciones("color_logo");
	
$logo = opciones("logo_mail");
$campo_oculto = 'validacion';
$para = 'htorres@moldeable.com';
$asunto = "Contacto $nombre_sitio";
$ruta_retorno = 'contacto';
$ruta_retorno_exito = 'exito-contacto';
$campo_mensaje = 'mensaje';
$obligatorios = "nombre, email, $campo_mensaje";
$lang = (isset($_GET[lang])) ? "ing": "esp";



$file_field = ""; //Nombre del campo del archivo adjunto, dejar vacÃ­o si no se va a usar

$from = "noreply@zonapromo.cl";
$save_in_db = true;
if ($save_in_db)
$insert = insert_entry("validacion", "contactos");

//Campos del formulario correspondiente a nombre e email.
$nombre_cl = $_POST['nombre'];
$email_cl = $_POST['email'];

//******************Comienzo del código, no modificar*********************//

if ($_POST[$campo_oculto] == ''){
	$vars = get_post('');
	$obligatorios = explode(',', $obligatorios);
    
    
	foreach($obligatorios as $o)
	{
		$campo = trim($o);
		if ($_POST["$campo"] === '')
		{
            $error = ($lang == 'esp') ? "Los campos marcados con * son necesarios." : "* indicates requiered fields.";
			header("location:$ruta_retorno?msje=$error");
            die("");
            
		}
        
	}
    
	
    
	$msg2 = '';
    $msg2 .= '
            <html>
                <head>
                <link rel="preconnect" href="https://fonts.gstatic.com">
                <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@400;600&display=swap" rel="stylesheet">
                <title>'.$nombre_sitio.'</title>
                <style type="text/css">
                        p, ul, a { 
                            color:#333; 
                            font-family: "Titillium Web", sans-serif;
                            font-weight:400;
                        }
                        strong{
                            font-weight:600;
                        }
                        a {
                            color:#5f5f5f;
                        }
                    </style>
                </head>
                <body style="background:#fff;">
                    <div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; font-family: Titillium Web, sans-serif; padding-bottom: 20px;">

                            <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
                                <tr>
                                    <th align="left" width="50%">
                                        <p>
                                            <a href="'.$url_sitio.'">
                                                <img src="'.$logo.'" alt="'.$logo.'" border="0" width="155"/>
                                            </a>
                                        </p>
                                    </th>
                                </tr>
                            </table>
                            <br/>

                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top">
                                            <p style="font-size:36px; color: '.$color_logo.'; margin-bottom:0;"><strong>Hola '.$nombre_sitio.':</strong></p>
                                            <p>Se ha enviado una consulta a trav&eacute;s del formulario web Servicio tecnico. <br />Los datos de la persona que contacta son:<br /></p>
                                            <ul>';
                                                    foreach($_POST as $key=>$val)
                                                    {
                                                        if ($key != 'enviar' AND $key != $campo_mensaje AND $key != '')
                                                        {
                                                            $nombreVal = ucwords($key);
                                                            $msg2 .= "<li style='min-height:30px;'>".$nombreVal.": ".htmlentities($val,ENT_QUOTES,"UTF-8")."</li>";
                                                        }
                                                    }
                                            $msg2 .= '</ul>
                                            <p>El cliente ha dejado el siguiente mensaje: <br /><br />
                                                <em>'.htmlentities($_POST[$campo_mensaje],ENT_QUOTES,"UTF-8").'</em>
                                            </p>

                                            <p>Muchas gracias<br /> Atte,</p>
                                            <p><strong>Equipo de '.$nombre_sitio.'</strong></p>
                                        </td>
                                    </tr>
                                </table>
                                <br/>

                                
                    </div>

                    <div style="width:86%; padding-left:7%; padding-right:7%; font-family: Titillium Web, sans-serif; padding-bottom: 20px; padding-top: 20px; background-color:#0b3d8c; color:#fff;">
                        <p style="color:#fff; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                        <p style="color:#fff; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                        <p>
                            <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                            </a>
                            <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                            </a>
                        </p>

                        <p>
                            <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/preguntas-frecuentes">Preguntas Frecuentes</a>
                            <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                        </p>
                    </div>
                </body>
            </html>';
	
        
    
    
    
            $asuntoRespuesta = "Contacto $nombre_sitio";
            $msg3 = '
            <html>
                <head>
                <link rel="preconnect" href="https://fonts.gstatic.com">
                <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@400;600&display=swap" rel="stylesheet">
                <title>'.$nombre_sitio.'</title>
                <style type="text/css">
                        p, ul, a { 
                            color:#333; 
                            font-family: "Titillium Web", sans-serif;
                            font-weight:400;
                        }
                        strong{
                            font-weight:600;
                        }
                        a {
                            color:#333;
                        }
                    </style>
                </head>
                <body style="background:#fff;">
                    <div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; font-family: Titillium Web, sans-serif; padding-bottom: 20px;">

                            <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
                                <tr>
                                    <th align="left" width="50%">
                                        <p>
                                            <a href="'.$url_sitio.'">
                                                <img src="'.$logo.'" alt="'.$logo.'" border="0" width="155"/>
                                            </a>
                                        </p>
                                    </th>
                                </tr>
                            </table>
                            <br/>

                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top">
                                            <p style="font-size:36px; color: '.$color_logo.'; margin-bottom:0;"><strong>Hola '.$nombre_cl.':</strong></p>
                                            <p>Recibimos tu consulta, nuestro equipo respondera tu duda lo antes posible<br /></p>


                                            <p>Muchas gracias por contactarnos<br /> Atte,</p>
                                            <p><strong>Equipo de '.$nombre_sitio.'</strong></p>
                                        </td>
                                    </tr>
                                </table>
                                <br/>

                               
                    </div>

                    <div style="width:86%; padding-left:7%; padding-right:7%; font-family: Titillium Web, sans-serif; padding-bottom: 20px; padding-top: 20px; background-color:#0b3d8c; color:#fff;">
                        <p style="color:#fff; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                        <p style="color:#fff; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                        <p>
                            <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                            </a>
                            <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                            </a>
                        </p>

                        <p>
                            <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/preguntas-frecuentes">Preguntas Frecuentes</a>
                            <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                        </p>
                    </div>
                </body>
            </html>';
    
    
    
    
           
    
    
            $mailin = new Mailin('PORTAL@ELCARPINTERO.CL', '098dZTSqEIUMBhYF');
            $mailin->
                addTo('jonathandda@gmail.com','Portal')->
                setFrom('portal@zonapromo.cl', 'Zona Promo')->
                setSubject("$asunto")->
                setText("$msg2")->
                    setHtml("$msg2");
            $res = $mailin->send();
            $res2 = json_decode($res);

            if ($res2->{'result'} == true) {
                $error = ($lang == 'esp') ?  "Su mensaje fue enviado. Muchas gracias por contactarnos." : "Your message has been sent, we'll answer you asap. Thank You.";
				
                //envio copia al cliente
                $mailinCliente = new Mailin('PORTAL@ELCARPINTERO.CL', '098dZTSqEIUMBhYF');
                $mailinCliente->
                    addTo("$email_cl", "$nombre_cl")->
                    setFrom('portal@zonapromo.cl', 'Zona Promo')->
                    setSubject("$asuntoRespuesta")->
                    setText("$msg3")->
                        setHtml("$msg3");
                $resCliente = $mailinCliente->send();
                $res2Cliente = json_decode($resCliente);
                //fin envio copia al cliente
                
                
                header("Location: $ruta_retorno_exito?a=1");
				die("1");
            } else {

               $error = ($lang == 'esp') ? "Error enviando el correo, por favor inténtelo nuevamente." : "We had some dificulties sending the email, please try again later.";
				header("Location: $ruta_retorno?msje=$error");
				die("2");
            }
    
    
    
		
			
	} else {
		//si no existe retorno e indico que no esta en los registros
		$error = ($lang == 'esp') ? "Error al enviar el mensaje" : "Error sending the email.";
		header("Location: $ruta_retorno?msje=$error");
		die("3");
	}

?>