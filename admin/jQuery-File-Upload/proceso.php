<?php

//include("../conf.php");

$id = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
$tabla = (isset($_GET['tabla'])) ? mysqli_real_escape_string($conexion, $_GET['tabla']) : 0;
$id_tabla = singular($tabla)."_id";	
/*$options = array(
    'delete_type' => 'POST',
    'db_host' => $host,
    'db_user' => $user,
    'db_pass' => $pass,
    'db_name' => $base,
    'db_table' => "img_$tabla",
	'db_tabla_id' => "$id_tabla"
	
);*/



/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

$options = array (
				'delete_type' => 'POST',
				'db_host' => $host,
				'db_user' => $user,
				'db_pass' => $pass,
				'db_name' => $base,
				'db_table' => "img_$tabla",
				'db_id' => $id,
				'db_tabla_id' => "$id_tabla",
				'upload_dir' => '../../imagenes/'.$tabla.'/',
				'upload_url' => '../../imagenes/'.$tabla.'/');

error_reporting(E_ALL | E_STRICT);
require('UploadHandler.php');
//$upload_handler = new UploadHandler($options);





class CustomUploadHandler extends UploadHandler {

    protected function initialize() {
    	$this->db = new mysqli(
    		$this->options['db_host'],
    		$this->options['db_user'],
    		$this->options['db_pass'],
    		$this->options['db_name'],
			$this->options['db_table'],
    		$this->options['db_id'],
			$this->options['db_tabla_id']
    	);
        parent::initialize();
        $this->db->close();
    }

    protected function handle_form_data($file, $index) {
    	$file->title = @$_REQUEST['title'][$index];
    	$file->description = @$_REQUEST['description'][$index];
    }

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
            $index = null, $content_range = null) {
        $file = parent::handle_file_upload(
        	$uploaded_file, $name, $size, $type, $error, $index, $content_range
        );
        if (empty($file->error)) {
			$sql = 'INSERT INTO `'.$this->options['db_table']
				.'` (`archivo`, `size`, `type`, `title`, `description`, `'.$this->options['db_tabla_id']
				.'`)'
				.' VALUES (?, ?, ?, ?, ?, ?)';
	        $query = $this->db->prepare($sql);
	        $query->bind_param(
	        	'sisss',
	        	$file->name,
	        	$file->size,
	        	$file->type,
	        	$file->title,
	        	$file->description,
				$file->db_id
	        );
	        $query->execute();
	        $file->id = $this->db->insert_id;
        }
        return $file;
    }

    protected function set_additional_file_properties($file) {
        parent::set_additional_file_properties($file);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        	$sql = 'SELECT `id`, `type`, `title`, `description` FROM `'
        		.$this->options['db_table'].'` WHERE `'.$this->options['db_tabla_id'].'`=?';
        	$query = $this->db->prepare($sql);
 	        $query->bind_param('s', $file->db_id);
	        $query->execute();
	        $query->bind_result(
	        	$id,
	        	$type,
	        	$title,
	        	$description
	        );
	        while ($query->fetch()) {
	        	$file->id = $id;
        		$file->type = $type;
        		$file->title = $title;
        		$file->description = $description;
    		}
        }
    }

    public function delete($print_response = true) {
        $response = parent::delete(false);
        foreach ($response as $name => $deleted) {
        	if ($deleted) {
	        	$sql = 'DELETE FROM `'
	        		.$this->options['db_table'].'` WHERE `name`=?';
	        	$query = $this->db->prepare($sql);
	 	        $query->bind_param('s', $name);
		        $query->execute();
        	}
        } 
        return $this->generate_response($response, $print_response);
    }

}

$upload_handler = new CustomUploadHandler($options);