<?php

/********************************************************\
|  Moldeable CMS - Excel generator.			             |
|  Fecha Modificación: 16/06/2011		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

date_default_timezone_set('America/Santiago');
include('../conf.php');
mysqli_query ($conexion, "SET NAMES 'latin1'");


header("Content-type: application/octet-stream");
header("Content: charset=UTF-8");
header("Content-Disposition: attachment; filename=excel_valores_listas.xls"); 
header("Pragma: no-cache" );
header("Expires: 0"); 

// $hoy = date("d/m/Y");

$productos = consulta_bd("id, lista_id, sku, valor_bruto","listas_productos", "","id desc");
?>
<table width="700" border="1">
        <thead>
            <tr>
            	<th><h3>id</h3></th>
                <th><h3>Lista_id</h3></th>
                <th><h3>SKU</h3></th>
                <th><h3>Valor bruto</h3></th>
            </tr>
        </thead>
        <tbody>
        	<?php for($i=0; $i<sizeof($productos); $i++) {?>
			<tr>
            	<td><?= trim($productos[$i][0]); ?></td>
                <td><?= trim($productos[$i][1]); ?></td>
                <td><?= trim($productos[$i][2]); ?></td>
                <td><?= (trim($productos[$i][3]) != '') ? $productos[$i][3] : 0 ?></td>
                
            </tr>
            <?php } ?>
        </tbody>

</table>