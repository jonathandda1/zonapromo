<?php 
/********************************************************\
|  Libraría V0.88b2 - Fecha Modificación: 17/04/2014	     |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  	        		 |
|  https://www.moldeable.com/                             |
\********************************************************/

date_default_timezone_set('America/Santiago');



	//FUNCIONES SITIO WEB
	function get_env()
	{
		if (!isset($_SESSION['env'])) {
			$env = get_option('env');
			return ($env == '') ? 'desarrollo' : $env;
		} else {
			return $_SESSION['env'];
		}
	}
	
	function check_install()
	{
		if (file_exists('install.php'))
		{
			die(header("location:install.php"));
		}
		return false;
	}

	function prevnext($id, $tabla, $galeria_id) {
		$filas = consulta_bd("id","img_$tabla","galeria_id = $galeria_id","id ASC");
		$i = 0;
		while($i <= (sizeof($filas)-1))
		{	
			$id_actual = $filas[$i][0];
			if ($id_actual == $id)
			{
				$return[0] = $filas[$i-1][0]; //prev
				$return[1] = $filas[$i+1][0]; //next
				break;
			}
			$i++;
		}
		return $return;
	}
	
	function check_admin_loged($hash, $user)
	{
		$conexion = $GLOBALS["conexion"];
		$is = consulta_bd("*", "administradores", "id = '$user' AND session_hash = '$hash'", "");
		$cant = mysqli_affected_rows($conexion);
		if ($cant==1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function get_nombre_sitio($nombre)
	{
		$largo = strlen($nombre);
		
		$buscar = '_';
		if (strrpos($nombre, $buscar))
		{
			$nombre_real = ucwords(strtolower(substr(strrchr($nombre, '_'), 1)));
		}
		else
		{
			$nombre_real = ucwords($nombre);
		}
		return $nombre_real;
	}
	
	function get_table_id($table)
	{
		$q =  consulta_bd("id","tablas","nombre = '$table'","");
		$id = $q[0][0];
		return $id;
	}
	
	function get_table_name($id)
	{
		$q =  consulta_bd_por_id("display","tablas","", $id, "");
		$nombre_real = $q['display'];
		return $nombre_real;
	}
	
	function is_md5($var) { 
		return preg_match('/^[A-Fa-f0-9]{32}$/',$var); 
	}
	
	function formato_moneda($numero, $moneda, $echo){
		if ($numero != NULL)
		{
			$longitud = strlen($numero);
			$punto = substr($numero, -1,1);
			$punto2 = substr($numero, 0,1);
			$separador = ".";
			if($punto == "."){
				$numero = substr($numero, 0,$longitud-1);
				$longitud = strlen($numero);
			}
			if($punto2 == "."){
				$numero = "0".$numero;
				$longitud = strlen($numero);
			}
			$num_entero = strpos ($numero, $separador);
			$centavos = substr ($numero, ($num_entero));
			$l_cent = strlen($centavos);
			if($l_cent == 2){$centavos = $centavos."0";}
			elseif($l_cent == 3){$centavos = $centavos;}
			elseif($l_cent > 3){$centavos = substr($centavos, 0,3);}
			$entero = substr($numero, -$longitud,$longitud-$l_cent);
			if(!$num_entero){
				$num_entero = $longitud;
				$centavos = ".00";
				$entero = substr($numero, -$longitud,$longitud);
			}
			
			$start = floor($num_entero/3);
			$res = $num_entero-($start*3);
			if($res == 0){$coma = $start-1; $init = 0;}else{$coma = $start; $init = 3-$res;}
			$d= $init; $i = 0; $c = $coma;
			
			while($i <= $num_entero){
				if($d == 3 && $c > 0){$d = 0; $sep = "."; $c = $c-1;}else{$sep = "";}
				$final .=  $sep.$entero[$i];
				$i = $i+1; // todos los digitos
				$d = $d+1; // poner las comas
			}
		}
		else
		{
			$final = 0;
		}
		if($moneda == "pesos")  {$moneda = "$";
			if ($echo)
			{
				echo $moneda." ".$final;
			}
			else
			{
				return $moneda." ".$final;
			}
			
		}
		elseif($moneda == "dolares"){$moneda = "USD";
			if ($echo)
			{
				echo $moneda." ".$final.$centavos;
			}
			else
			{
				return $moneda." ".$final.$centavos;
			}
		}
		elseif($moneda == "euros")  {$moneda = "EUR";
			if ($echo)
			{
				echo $final.$centavos." ".$moneda;
			}
			else
			{
				return $final.$centavos." ".$moneda;
			}
		}
		else
		{
			if ($echo)
			{
				echo $final;
			}
			else
			{
				return $final;
			}
		}
	}
	
	function select($tabla, $columnas, $cod, $nombre, $tab_index, $where, $submit, $selected, $bloqueado){
		$conexion = $GLOBALS['conexion'];
		if ($cod != '')
		{
			$columnas = "$cod,$columnas"; 
		}
		
		$sql = "SELECT $columnas FROM $tabla";
		if ($where != '')
		{
			$sql .= " $where";
		}
		$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion)."<br /><br />$sql");;
		
		$mostrar = '<select class="select" '.$bloqueado.' name="'.$nombre.'" id="'.$nombre.'" ';
		if ($tab_index != '')
		{
			$mostrar .= 'tabindex="'.$tab_index.'"';
		}
		
		if ($submit == '1')
		{
			$mostrar .= 'onchange="cambiaSelect(this.value, this.name)">';
		}
		elseif ($submit == '2')
		{
			$mostrar .= 'onchange="submit();"';
		}
		else
		{
			$mostrar .= '>';
		}
		
		$mostrar .= '<option value="0">Seleccione...</option>';
		while ($res = mysqli_fetch_array($run))
		{
			$mostrar .= '<option value="'.$res[0].'"';
			if ($res[0] == $selected)
			{
				$mostrar .= ' selected="selected"';
			}
			
			if ($res[1])
			{
				$opcion = $res[1];
			}
			else
			{
				$opcion = $res[0];
			}
			
			$mostrar .= '>'.ucwords($opcion).'</option>';
		}
		$mostrar .= '</select>';
		
		echo "$mostrar";
	}
	
	
	function checkbox($valor, $activo, $id){
		if ($valor == '1')
		{
			$checked = 'checked="checked"';
		}
		else
		{
			$checked = '';
		}
		
		if ($activo == '1')
		{
			$disabled = '';
		}
		elseif ($activo == '0')
		{
			$disabled = 'disabled="disabled"';
		}
		if ($id != '')
		{
			$name = "checkbox$id";
		}
		else
		{
			$name = "checkbox";
		}
		
		$mostrar = '<label>
	  					<input name="'."$name".'" type="checkbox" id="checkbox" '.$checked.' '.$disabled.' value="1"/>
					</label>';
					
		echo "$mostrar";
	}
	
	function preview($cadena, $numerocaracteres)  
	{   
		$numeropalabrascadena = count(explode (" ", $cadena));   
		$cadenadividida = explode (" ", $cadena);  
		$cadenamostrar = "";  
		$cadenaaux = "";  
		$cadenacompleta = "si";  
		for ($i=0;$i<=$numeropalabrascadena;$i++)  
		{  
		   $cadenaaux = $cadenaaux.@$cadenadividida[$i]." ";  
		   if (strlen($cadenaaux)<= $numerocaracteres)  
		   {  
		      $cadenamostrar = $cadenamostrar.@$cadenadividida[$i]." ";  
		   }  
		   else  
		   {  
		      $cadenacompleta = "no";  
		   }  
		}  
		if ($cadenacompleta=="no")  
		{  
		   $cadenamostrar .= " ...";  
		}  
		return  $cadenamostrar;  
	} 
	
	function comprobar_mail($email)
	{ 
	    $mail_correcto = 0; 
	    if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
	       if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {  
	          if (substr_count($email,".")>= 1){ 
	             $term_dom = substr(strrchr ($email, '.'),1); 
	             if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){  
	                $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
	                $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
	                if ($caracter_ult != "@" && $caracter_ult != "."){ 
	                   $mail_correcto = 1; 
	                } 
	             } 
	          } 
	       } 
	    } 
	    if ($mail_correcto) 
	       return 1; 
	    else 
	       return 0; 
	} 
	
	function consulta_bd($campos, $table, $conditions, $orden) 
	{
		$conexion = $GLOBALS['conexion'];
		if ($campos == '*')
		{
			$sql0 = "SHOW columns FROM $table";
			$run0 = mysqli_query($conexion, $sql0);
			$cant_columnas = mysqli_affected_rows($conexion);
		}
		else
		{
			$columnas = explode(",",$campos);
			$cant_columnas = count($columnas);
		}
		
		$sql = "SELECT $campos FROM $table";
		if ($conditions != '')
		{
			$sql .= " WHERE $conditions";
		}
		if ($orden != '')
		{
			$sql .= " ORDER BY $orden";
		}
		
		if (!mysqli_query($conexion, $sql)){
			//" . mysqli_error($conexion).";
		  die("Error description: " . mysqli_error($conexion)."<br /><br />$sql");
		}
  
  		$run = mysqli_query($conexion, $sql);
		$i = 0;
		while($line = mysqli_fetch_array($run))
		{
			$j = 0;
			while($j <= $cant_columnas)
			{
				$resulset[$i][$j] = stripslashes($line[$j]);
				$j++;	
			}
			$i++;
		}
		return $resulset;
	}
	
	
	function consulta_bd_por_id($campos, $table, $conditions, $id) 
	{	
        $conexion = $GLOBALS['conexion'];        
		if (is_numeric((int)$id))
		{
			$sql = "SELECT $campos FROM $table";
			if ($conditions != '')
			{
				$sql .= " WHERE $conditions AND id = '$id'";
			}
			else
			{
				$sql .= " WHERE id = '$id'";
			}
		}
		else
		{
			die('Error, identificador no válido');
		}
		
		$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion)."<br /><br />$sql");
		$line = mysqli_fetch_array($run);
	
		return $line;
	}
	
	function update_bd($table, $values, $conditions)
	{
		$conexion = $GLOBALS['conexion'];
		$sql = "UPDATE $table SET $values ";
		if ($conditions != '')
		{
			$sql .= "WHERE $conditions";
		}
		
		$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion)."<br /><br />$sql");;
		if ($run)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function insert_bd($table, $campos, $values)
	{
		$conexion = $GLOBALS['conexion'];
		$sql = "INSERT INTO $table ($campos) VALUES ($values)";
		$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion)."<br /><br />$sql");;
		if ($run)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function del_bd($table, $id)
	{
		//die("$table, $id");
		$conexion = $GLOBALS['conexion'];
		$sql = sprintf("DELETE FROM $table WHERE id = '%d'", mysqli_real_escape_string($conexion, $id));
		$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion)."<br /><br />$sql");
		if ($run)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function del_bd_generic($table, $column, $val)
	{
		$conexion = $GLOBALS['conexion'];
		$sql = "DELETE FROM $table WHERE $column = '$val'";
		$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion)."<br /><br />$sql");
		if ($run)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function show_columns($table, $exception)
	{
		$conexion = $GLOBALS['conexion'];
		$excepciones = explode(',',$exception);
		
		$sql = "SHOW columns FROM $table";
		$run = mysqli_query($conexion, $sql) or die(mysqli_error($conexion)."<br /><br />$sql");
		
		$i = 0;
		$z = 0;
		while($line = mysqli_fetch_array($run))
		{		
			$ok = true;
			foreach($excepciones as $val)
			{
				if (trim($val) == $line[0])
				{
					$ok = false;
				}
			}
			if ($ok)
			{
				$resulset[$z] = "$line[0]";
				$z++;
			}
			$i++;
			
		}
		return $resulset;
	}
	
	function num_hojas($page, $total, $limite_hoja, $op, $buscar)
	{
		if ($total > $limite_hoja)
		{
			$restantes = $total-$limite_hoja;
			$hojas = ceil($restantes/$limite_hoja);
	
			if ($page != 0)
			{
				$prev = $page;
				echo '<a href="index.php?op='.$op.'&page='.$prev.'&buscar='.$buscar.'" class="page">Anterior</a> ';
			}
			$j=0;
			while ($j <= $hojas)
			{
				$k = $j+1;
				echo '<a href="index.php?op='.$op.'&page='.$k.'&buscar='.$buscar.'" ';
				if ($page==$j)
				{
					echo 'class="page_selected">';
				}
				else
				{
					echo 'class="page">';
				}
				echo $k;
				echo '</a> ';
				$j++;
			}
			if ($page != $hojas)
			{
				$next = $page+2;
				echo '<a href="index.php?op='.$op.'&page='.$next.'&buscar='.$buscar.'" class="page" onclick="submit()">Siguiente</a> ';
			}
		}
	}
	
	function get_last($table)
	{
		$conexion = $GLOBALS['conexion'];
		$sql = "SELECT id FROM $table ORDER BY id DESC LIMIT 1";
		$run = mysqli_query($conexion, $sql);
		$res = mysqli_fetch_array($run);
		$last = $res[0];
		return $last;
	}
	
	function fecha_sql($fecha)
	{
		if (strtotime($fecha))
		{
			$fecha_cl = date('d/m/Y - H:i',strtotime($fecha));
		}
		else
		{
			$fecha_cl = date('d/m/Y - H:i');
		}
		return $fecha_cl;
	}
	
	function fecha_sql_nh($fecha)
	{
		if (strtotime($fecha))
		{
			$fecha_cl = date('d/m/Y',strtotime($fecha));
		}
		else
		{
			$fecha_cl = date('d/m/Y');
		}
		return $fecha_cl;
	}
	
	function fecha_hes($fecha, $formato, $lang)
	{
		$mes_o = date("m", strtotime($fecha));
		$dia = date("d", strtotime($fecha));
		$ano = date("Y", strtotime($fecha));
		$hora = date("H:i:s",strtotime($fecha));
		
		if ($lang == 'esp')
		{
			switch ($mes_o)
			{
				case '01':
					$mes = "Enero";
					break;
				case '02':
					$mes = "Febrero";
					break;
				case '03':
					$mes = "Marzo";
					break;
				case '04':
					$mes = "Abril";
					break;
				case '05':
					$mes = "Mayo";
					break;
				case '06':
					$mes = "Junio";
					break;
				case '07':
					$mes = "Julio";
					break;
				case '08':
					$mes = "Agosto";
					break;
				case '09':
					$mes = "Septiembre";
					break;
				case '10':
					$mes = "Octubre";
					break;
				case '11':
					$mes = "Noviembre";
					break;
				case '12':
					$mes = "Diciembre";
					break;
			}
			
			$sep = 'de';
			$sep2 = 'a las';
		}
		elseif ($lang == 'ing')
		{
			switch ($mes)
			{
				case '01':
					$mes = "January";
					break;
				case '02':
					$mes = "February";
					break;
				case '03':
					$mes = "March";
					break;
				case '04':
					$mes = "April";
					break;
				case '05':
					$mes = "May";
					break;
				case '06':
					$mes = "June";
					break;
				case '07':
					$mes = "July";
					break;
				case '08':
					$mes = "August";
					break;
				case '09':
					$mes = "September";
					break;
				case '10':
					$mes = "October";
					break;
				case '11':
					$mes = "November";
					break;
				case '12':
					$mes = "December";
					break;
			}
			
			$sep = '';
			$sep2 = 'at';
		}
		
		switch ($formato)
		{
			case 1:
				return "$dia $sep $mes $sep $ano"; 
				break;
			case 2:
				return "$mes $sep $ano";
				break;
			case 3:
				return "$dia/$mes_o/$ano $sep2";
				break;
			case 4:
				return "$dia/$mes_o/$ano $sep2 $hora";
				break;
		}
		
	}
	
	function diferencia_fechas($date1, $date2)
	{
		$date1_seconds = strtotime($date1);
		$date2_seconds = strtotime($date2);
		
		$dif_seg = $date2_seconds - $date1_seconds;
		
		if ($dif_seg < 60)
		{
			$return =  "hace $dif_seg segundos";
		}
		else
		{
			$dif_min = floor($dif_seg/60);
			if ($dif_min < 60)
			{
				if ($dif_min == 1)
				{
					$return =  "hace $dif_min minuto";
				}
				else
				{
					$return = "hace $dif_min minutos";
				}
				
			}
			else
			{
				$dif_hora = floor($dif_min/60);
				if ($dif_hora < 24)
				{
					if ($dif_hora == 1)
					{
						$return = "hace $dif_hora hora";
					}
					else
					{
						$return = "hace $dif_hora horas";
					}					
				}
				else
				{
					$dif_dias = floor($dif_hora/24);
					if ($dif_dias < 24)
					{
						if ($dif_dias == 1)
						{
							$hora_publicacion = date("H:m:s", $date1_seconds);
							$return = "ayer a las $hora_publicacion";
						}
						else
						{
							$return = "hace $dif_dias días";
						}
						
					}
					else
					{
						$date_1_b = date("m-d-Y", $date1_seconds);
						$return = "el $date_1_b";
					}
				}
			}
		}
		return $return;
	}
	
	function select_paises()
	{
		$return = '<select name="select_pais"><option value="">Seleccione...</option><option value="Africa">Africa</option><option value="Argentina">Argentina</option><option value="Australia">Australia</option><option value="Austria">Austria</option><option value="Belgium">Belgium</option><option value="Brazil">Brazil</option><option value="Bulgaria">Bulgaria</option><option value="Canada">Canada</option><option value="Caribbean">Caribbean</option><option value="Central America">Central America</option><option value="Chile" selected="selected">Chile</option><option value="China">China</option><option value="Colombia">Colombia</option><option value="Costa Rica">Costa Rica</option><option value="Croatia">Croatia</option><option value="Czech Republic">Czech Republic</option><option value="Denmark">Denmark</option><option value="Dominican Republic">Dominican Republic</option><option value="Estonia">Estonia</option><option value="Finland">Finland</option><option value="France">France</option><option value="Germany">Germany</option><option value="Greece">Greece</option><option value="Guatemala">Guatemala</option><option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option><option value="India">India</option><option value="Indonesia">Indonesia</option><option value="Ireland">Ireland</option><option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Japan">Japan</option><option value="Korea">Korea</option><option value="Latvia">Latvia</option><option value="Lithuania">Lithuania</option><option value="Malaysia">Malaysia</option><option value="Mexico">Mexico</option><option value="Middle East">Middle East</option><option value="Morocco">Morocco</option><option value="Netherlands">Netherlands</option><option value="New Zealand">New Zealand</option><option value="Norway">Norway</option><option value="Panama">Panama</option><option value="Peru">Peru</option><option value="Philippines">Philippines</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Puerto Rico">Puerto Rico</option><option value="Romania">Romania</option><option value="Russian Federation">Russian Federation</option><option value="Singapore">Singapore</option><option value="Slovakia">Slovakia</option><option value="Slovenia">Slovenia</option><option value="South Africa">South Africa</option><option value="Spain">Spain</option><option value="Sweden">Sweden</option><option value="Switzerland">Switzerland</option><option value="Taiwan">Taiwan</option><option value="Thailand">Thailand</option><option value="Turkey">Turkey</option><option value="Ukraine">Ukraine</option><option value="United Kingdom">United Kingdom</option><option value="United States">United States</option><option value="Venezuela">Venezuela</option><option value="Vietnam">Vietnam</option></select>';
		return $return;
	}
	
	function get_post($except)
	{
		$excepciones = explode(',',$except);
		$excepciones[] = "posicion_img";
		foreach ($_POST as $key=>$val)
		{
			$ok = true;
			foreach($excepciones as $no)
			{
				if ($key == trim($no))
				{
					$ok = false;
				}
			}
			if ($ok)
			{
				if (!is_array($val)) {
					$valurl = urlencode($val);
					$variables .= "&$key=$valurl";
				}
			}
		}
		return $variables;
	}

	function get_gets($except)
	{
		$excepciones = explode(',',$except);
		foreach ($_GET as $key=>$val)
		{
			$ok = true;
			foreach($excepciones as $no)
			{
				if ($key == trim($no))
				{
					$ok = false;
				}
			}
			if ($ok)
			{
				$valurl = urlencode($val);
				$variables .= "&$key=$valurl";
			}
		}
		return $variables;
	}
	
	function get_val_op($op)
	{
		$value = substr($op, 0, -1);
		return $value;
	}
	
	function get_sec_op($op)
	{
		$value = substr($op, -1, 1);
		return $value;
	}
	
	function singular($string)
	{
		$penultima =  substr($string, -2, 1);
		$antepenultima =  substr($string, -3, 1);
		if ($penultima == 'e')
		{
			if ($antepenultima == 't' OR $antepenultima == 'j' OR $antepenultima == 'v')
			{
				$singular = substr($string, 0, -1);
			}
			else
			{
				$singular = substr($string, 0, -2);
			}
		}
		else
		{
			$singular = substr($string, 0, -1);
		}
		return $singular;
	}
	

	function get_real_name($nombre)
	{
		$real_name = str_replace("_", " ", $nombre);
		$palabras = explode(" ",$real_name);
		if (count($palabras) > 1)
		{
			$i == 0;
			foreach ($palabras as $palabra)
			{
				$nombre_final .= ($i == 0) ? ucwords(ortografia($palabra)): " ".ortografia($palabra);
				$i++;
			}
		}
		else
		{
			$nombre_final = ucwords(ortografia($nombre));
		}

		return $nombre_final;
	}
	
	
	function ortografia($palabra)
	{
		$conexion = $GLOBALS['conexion'];
		//Palabras agudas terminadas en vocales a, e y o son verbos, por lo que se omiten las volcales de la regla.
		$regla_aguda = array('n','s','i','u');
		$regla_aguda_not = array('ay', 'ey', 'oy', 'uy', 'au', 'eu', 'ou', 'os');
		$regla_aguda_verbo = array('a', 'e', 'o');
		$regla_grave = array('b','c','d','f','g','h','j','k','l','m','p','q','r','t','v','w','x','y','z');
		$vocales = array('a', 'e', 'i', 'o', 'u');
		$vocales_acentos = array('á', 'é', 'í', 'ó', 'ú');
		$ultima_letra =  strtolower(substr($palabra, -1));
		$penultima_letra =  strtolower(substr($palabra, -2, 1));
		$largo_palabra = strlen($palabra);
             
        $palabras_bd = consulta_bd("busqueda","ortografia","","");
        $cant=mysqli_affected_rows($conexion);
        if($cant>0){
	        foreach($palabras_bd as $pal)
	        {
	            $exclusiones_bd[] = $pal[0];
	        }
        }
        else
        {
	        $exclusiones_bd[]="";
        }
        if (has_array($exclusiones_bd,$palabra))
        {
            $palabra_bd = consulta_bd("mostrar","ortografia","busqueda = '$palabra'","");
            return ($palabra_bd[0][0] != '') ? $palabra_bd[0][0]: $palabra;
        }
        else
        {
            if ($largo_palabra > 2) {

                    if (has_array($regla_aguda, $ultima_letra))
                    {
                            //Es Aguda
                            //Separo la última letra del resto
                            $resto_palabra = substr($palabra, 0, -2);
                            $dos_ultimas_letras =  strtolower(substr($palabra, -2));
                            $tres_ultimas_letras =  strtolower(substr($palabra, -3));
                            if ($ultima_letra == 's' and !has_array($vocales, $penultima_letra))
                            {

                            }
                            else
                            {
                                    //Veo si pasa la regla en que no lleva acento
                                    if (!has_array($regla_aguda_not, $ultima_letra) and $tres_ultimas_letras != 'gen')
                                    {
                                            //Reemplazo la consonante en las œltimas dos letras.
                                            $cambio_aguda = str_replace($vocales, $vocales_acentos, $dos_ultimas_letras);
                                            $palabra = $resto_palabra.$cambio_aguda;
                                    }
                            }
                            return $palabra;

                    }

                    if (has_array($regla_grave, $ultima_letra))
                    {
                            //Es grave
                            //Separo las dos primeras letras del resto
                            $resto_palabra = substr($palabra, 2);
                            $primeras_dos_letras = substr($palabra, 0, 2);
                            //Reemplazo la consonante en las primerass dos letras.
                            $cambio = str_replace($vocales, $volcales_acentos, $primeras_dos_letras);
                            $palabra = $primeras_dos_letras.$resto_palabra;
                            return $palabra;
                    }
                    else
                    {
                            //Esdrújula, cuento 4 letras hacia atrás y si el total de letras es par pongo acento en las siguientes dos, de lo contrario en las siguientes 3
                            if ($largo_palabra%2==0 AND $largo_palabra > 2)
                            {
                                    //Es par
                                    $ultimas_cuatro = strtolower(substr($palabra, -4));
                                    $sin_ultimas_cuatro = strtolower(substr($palabra, 0, -4));
                                    $siguientes_dos = strtolower(substr($sin_ultimas_cuatro, -2));
                                    $resto = strtolower(substr($palabra, 0,-6));
                                    $primera_letra = strtolower(substr($palabra, 0, 1));
                                    //Si siguientes_dos tiene l
                                    $cambio_esd = ((!has_array(array('l'), $siguientes_dos) AND !has_array($vocales, $primera_letra)) OR has_array($vocales, $primera_letra)) ? $siguientes_dos : str_replace($vocales, $vocales_acentos, $siguientes_dos);
                                    $palabra = $resto.$cambio_esd.$ultimas_cuatro;
                                    return $palabra;
                            }
                            else
                            {
                                    //Es impar
                                    return $palabra;
                            }				
                    }
            }
            else
            {
                    return $palabra;
            }                    
        }
	}
	
	function has_array($array, $string)
	{
		foreach ($array as $a)
		{
			if (is_numeric(strrpos($string,trim($a))))
			{
				$ok = true;
				break;
			}
			else
			{
				$ok = false;
			}
		}
		return $ok;
	}
	
	function plural($string)
	{
		$ultima =  substr($string, -1, 1);
		$vocales = array('a','e','i','o','u');
		foreach ($vocales as $v)
		{
			if ($v == $ultima)
			{
				$ter = 's';
				break;
			}
			else
			{
				$ter = 'es';
			}
		}
		$plural = "$string$ter";
		return $plural;
	}
	
	function obtener_tabla($val_op){
		$filas = consulta_bd("nombre","tablas","id = '$val_op'","");
		$nombre = $filas[0][0];
		return $nombre;
	}
	
	function obtener_tabla_id($tabla){
		$filas = consulta_bd("id","tablas","nombre = '$tabla'","");
		$id = $filas[0][0];
		return $id;
	}
	
	function comprobar_rut($sUsr) {
		
		$sUsr = str_replace('.', '', $sUsr);
	
		if (!preg_match("/(\d{7,8})-([\dK])/", strtoupper($sUsr), $aMatch)) 
		{
			return false;
		}
		
		$sRutBase = substr(strrev($aMatch[1]) , 0, 8 );
		$sCodigoVerificador = $aMatch[2];
		$iCont = 2;
		$iSuma = 0;
		for ($i = 0;$i<=strlen($sRutBase);$i++) 
		{
			if ($iCont>7) 
			{
				$iCont = 2;
			}
			$iSuma+= ($sRutBase{$i}) *$iCont;
			$iCont++;
		}
		$iDigito = 11-($iSuma%11);
		$sCaracter = substr("-123456789K0", $iDigito, 1);
		return ($sCaracter == $sCodigoVerificador);
	}
	
	function validar_rut($r, $dv)
	{ 
		$r = str_replace('.', '', trim($r));    
		$s = 1;    
		for($m = 0; $r != 0; $r/= 10) $s = ( $s+ $r%10 * (9-$m++%6) ) %11;    
		return (chr($s ? $s+47 : 75) == strtoupper($dv));
	}
		
	function is_dependent($name)
	{
		$buscar = "_id";
		if (strrpos($name, $buscar))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function column_is_related($column){
		$is_id = substr($column, -3, 3);
		if ($is_id == "_id")
		{
			$parent_name = substr($column, 0, -3);
			return $parent_name;
		}
		else
		{
			return false;
		}
	}
	
	function check_if_item_has_belongs_to($column)
	{
		$conexion = $GLOBALS['conexion'];
		$nombre_tabla_hijo = plural(substr($column, 0, -3));
		$id_tabla_hijo = consulta_bd("id","tablas","nombre = '$nombre_tabla_hijo'","");
		$id_tabla_hijo = $id_tabla_hijo[0][0];
		$tabla = consulta_bd("valor", "opciones_tablas", "nombre = 'belongs_to' AND tabla_id = '$id_tabla_hijo'", "");
		if (mysqli_affected_rows($conexion) > 0)
		{
			return $tabla[0][0];
		}
		else
		{
			return false;
		}
			
	}
	
	function del_file($file)
	{
		if (file_exists($file))
		{
			$unlink = unlink($file);
			if ($unlink)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function is_inside($buscados, $string)
	{
		$exclusiones = explode(",",$buscados);
		$is_id =  substr($string, -3, 3);
		if ($is_id != '_id')
		{
			foreach($exclusiones as $no)
			{
				if ($string==trim($no))
				{
					return true;
					break;
				}
				else
				{
					$ok = 1;
				}
			}
		}
		else
		{
			$ok = 1;
		}
		if ($ok)
		return false;
	}
	
	function eng_sql($string)
	{
		$newstring = stripslashes($string);
		$new = addslashes($newstring);
		return $new;
	}
	
	function insert_entry($exclusiones, $tabla) 
	{	
		$keys = "fecha_creacion";
		$vals = "NOW()";
		foreach($_POST as $key=>$val)
		{
			if (!strpos(" ".$exclusiones,$key))
			{
				$keys .= ", $key";
				$vals .= ", '$val'";
			}
		}
	
		$insert = insert_bd($tabla, $keys, $vals);
		if ($insert)
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function update_entry($exclusiones, $tabla, $conditions) 
	{	
		$values = "fecha_modificacion = NOW()";
		foreach($_POST as $key=>$val)
		{
			if (!strpos(" ".$exclusiones,$key))
			{
				$values .= ", $key = '$val'";
			}
		}
	
		$insert = update_bd($tabla, $values, $conditions);
		if ($insert)
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function get_option($val) {
		$res = consulta_bd("valor","opciones","nombre = '$val'","");
		return $res[0][0];
	}

	function column_exists($table, $column)
	{	
		$conexion = $GLOBALS['conexion'];	
		$sql = "SHOW columns FROM $table";
		$run = mysqli_query($conexion, $sql) or die(mysqli_error()."<br /><br />$sql");
		
		$i = 0;
		$z = 0;
		while($line = mysqli_fetch_array($run))
		{		
			$ok = false;
			if (trim($column) == $line[0])
			{
				$ok = true;
				break;
			}
			$i++;
		}
		return $ok;
	}
	
	function prepare_img($html) {
		$url_sitio = get_option('url_sitio');
		return str_replace('src="../', 'src="'.$url_sitio.'/', $html);
	}
	
	function get_permiso($tabla_id, $action, $perfil_admin)
	{
		switch ($action)
		{
			case 'a':
				//Listar
				$action = 1;
				break;
			case 'b':
				//Nuevo
				$action = 2;
				break;
			case 'c':
				//Editar
				$action = 3;
				break;
			case 'other':
				$action = 4;
				break;
		}
		$res = consulta_bd("permiso_id","tablas_perfiles","perfil_id = $perfil_admin AND tabla_id = $tabla_id","");
		$permiso = $res[0][0];
		
		if (($action == 3 OR $action == 2 OR $action == 4) AND $permiso == 3)
		{
			return true;
		}
		elseif ($action == 1 AND $permiso != 1 AND $permiso != NULL)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function is_boolean($column, $table)
	{
		$conexion = $GLOBALS['conexion'];
		$sql = "SHOW COLUMNS FROM $table WHERE field = '$column'";
		$run = mysqli_query($conexion, $sql);
		$res = mysqli_fetch_array($run);
		$type = $res[1];
		return ($type == 'int(1)') ? true:false;
	}
	
	function is_enum($column, $table)
	{
		$conexion = $GLOBALS['conexion'];
		$sql = "SHOW COLUMNS FROM $table WHERE field = '$column'";
		$run = mysqli_query($conexion, $sql);
		$res = mysqli_fetch_array($run);
		$type = $res[1];
		return (substr_count($type, "enum") != 0) ? true:false;
	}
	
	function enum_vals($column, $table) {
		$conexion = $GLOBALS['conexion'];
		$sql = "SHOW COLUMNS FROM $table WHERE field = '$column'";
		$run = mysqli_query($conexion, $sql);
		$res = mysqli_fetch_array($run);
		$type = $res[1];
		$buscar = array("enum","(",")", "'");
		$vals = str_replace($buscar, '', $type);
		return explode(',', $vals);
	}
	
	function img_tag($file, $w, $h)
	{
		$size=getimagesize($file);
		$width=$size[0];
		$height=$size[1];
		if ($width > $w)
		{
			$p = $w/$width;
			$x = $p*$width; 
			$y = $p*$height;
			
			if ($y > $h)
			{
				$p = $h/$y;
				$x = $p*$x; 
				$y = $p*$y;	
			}
			
		}
		elseif ($height > $h)
		{
			$p = $h/$height;
			$x = $p*$width; 
			$y = $p*$height;
			
			if ($x > $width)
			{
				$p = $h/$x;
				$x = $p*$x; 
				$y = $p*$y;
			}		
		}
		else
		{
			$x = $width;
			$y = $height;
		}
		return '<img src="'.$file.'" width="'.$x.'px" height="'.$y.'px" border="0">';
	}
	
	
	
	function is_date($string)
	{
		if (strlen($string)== 10 || strlen($string)== 19)
		{
			$dia = substr($string, 8, 2);
			$ano = substr($string, 0, 4);
			$mes = substr($string, 5, 2);
			if (is_numeric($dia) AND is_numeric($mes) AND is_numeric($ano))
			{
				return checkdate($mes, $dia, $ano);
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	function manage_files($col_archivos, $opcion, $tabla, $id)	
	{
		$dir = "../../imagenes/$tabla";
		$dir_docs = "../../docs/$tabla";
		
		if (sizeof($col_archivos) > 1)
		{
			
			foreach($col_archivos as $row)
			{
				$row = trim($row);
				$file = $_FILES[$row]['name'];
				if($file!=''){ // para el caso de un array donde se agrava una y no todas
					$file_ext = strtolower(substr(strrchr($_FILES[$row]['name'], '.'), 1));
				
					//Obtengo los tamaños de las imagenes (En caso de que sea img)
					if ($opcion[trim($row)] and $opcion[$row] != '')
					{
						$tamanos = explode(',', $opcion[trim($row)]);
						$width = $tamanos[0];
						$height = $tamanos[1];
					}
					else
					{
						$width = ($opcion['width'] != '') ? (int)$opcion['width']:0;
						$height = ($opcion['height']!='')? (int)$opcion['height']:0;
					}
			
					if ($width != 0 AND $height != 0)
					{
						if ($file_ext == 'jpg' OR $file_ext == 'gif' OR $file_ext == 'png')
						{						
							$width_thumb = (int)$opcion['width_thumb'];
							$height_thumb = (int)$opcion['height_thumb'];
							
							$thumb = ($width_thum != '') ? 1: 0;
								
							$img = add_img(trim($row), $dir, $tabla, $id, $width, $height, $thumb, $width_thumb, $height_thumb, false);
							$error .= $img['error'];
							$values .= $img['valores'];
						}
						else
						{
							$error .= "Formato de archivo incorrecto.&tipo=error";
						}
					}
					elseif($file_ext != '')
					{
						$extensions = "doc, docx, ppt, pptx, xls, xlsx, pdf, dwg, swf";
						$file = add_file($row, $dir_docs, $tabla, $id, $extensions);
						$error .= $file['error'];
						$values .= $file['valores'];
					}	
				}				
			}
		}
		else
		{
			
			$file = $_FILES[$col_archivos]['name'];
			$file_ext = strtolower(substr(strrchr($_FILES[$col_archivos]['name'], '.'), 1));
			
			if ($opcion[trim($row)] and $opcion[$row] != '')
			{
				$tamanos = explode(',', $opcion[trim($row)]);
				$width = $tamanos[0];
				$height = $tamanos[1];
			}
			else
			{
				$width = ($opcion['width'] != '') ? (int)$opcion['width']:0;
				$height = ($opcion['height']!='')? (int)$opcion['height']:0;
			}
			
			if ($width != 0 AND $height != 0)
			{
				
				if ($file_ext == 'jpg' OR $file_ext == 'gif' OR $file_ext == 'png')
				{	
					$width_thumb = (int)$opcion['width_thumb'];
					$height_thumb = (int)$opcion['height_thumb'];
					
					$thumb = ($width_thumb != '') ? 1: 0;
	
					$img = add_img($col_archivos, $dir, $tabla, $id, $width, $height, $thumb, $width_thumb, $height_thumb, false);
					$error .= $img['error'];
					$values .= $img['valores'];
				}
				else
				{
					$error .= "Formato de archivo incorrecto.&tipo=error";
				}	
			}
			elseif($file_ext != '')
			{
				$extensions = "doc, docx, ppt, pptx, xls, xlsx, pdf, dwg, swf";
				$file = add_file(trim($col_archivos), $dir_docs, $tabla, $id, $extensions);
				$error .= $file['error'];
				$values .= $file['valores'];
				$entro = $values;
			}
		}
		
		return array("error" => $error, "values" => $values); 
	}

	/**
	 * Thanks to ZeBadger for original example, and Davide Gualano for pointing me to it
	 * Original at http://it.php.net/manual/en/function.imagecreatefromgif.php#59787
	 **/
	function is_animated_gif( $filename )
	{
	    $raw = file_get_contents( $filename );
	
	    $offset = 0;
	    $frames = 0;
	    while ($frames < 2)
	    {
	        $where1 = strpos($raw, "\x00\x21\xF9\x04", $offset);
	        if ( $where1 === false )
	        {
	                break;
	        }
	        else
	        {
	                $offset = $where1 + 1;
	                $where2 = strpos( $raw, "\x00\x2C", $offset );
	                if ( $where2 === false )
	                {
	                        break;
	                }
	                else
	                {
	                        if ( $where1 + 8 == $where2 )
	                        {
	                                $frames ++;
	                        }
	                        $offset = $where2 + 1;
	                }
	        }
	    }
	
	    return $frames > 1;
	}
	
	function add_file($input_name, $dir, $tabla, $id, $extensions){
		$conexion = $GLOBALS['conexion'];
		if (!file_exists($dir."/"))
		{
			$mk = mkdir($dir);
			if (!$mk)
			$error = "Error al crear el directorio.&tipo=error";
		}
	
		if ($error =='')
		{
			if ($_FILES[$input_name]['name'])
			{
				if ($_FILES[$input_name]['size'] < 20000000)
				{
					$file_name =  str_replace(" ", "_", substr($_FILES[$input_name]['name'], 0, -4));
					$file_name = str_replace('.', '', $file_name);		//quita el doble "." al cargar un file de 4 letras ext
					$file_ext = strtolower(substr(strrchr($_FILES[$input_name]['name'], '.'), 1));					
					$extension = explode(',', $extensions);
					$cant_ex = sizeof($extension);
					$ok = false;
					foreach ($extension as $ex)
					{
						if ($file_ext == trim($ex))
						{
							$ok = true;
							break;
						}
					}
					
					if ($ok)
					{
						$date = date("dmhis");
						$archivo = $date.'_'.$file_name.'.'.$file_ext;
						$dir1 = "$dir/$archivo";
						$move = move_uploaded_file($_FILES[$input_name]['tmp_name'], $dir1);
						if ($move)
						{
							if ($id)
							{
								$img_q = "SELECT $input_name FROM $tabla WHERE id = '$id'";
								$run = mysqli_query($conexion, $img_q);
								$res = mysqli_fetch_array($run);
								$img = "$dir/".$res[0];
								if($res[0] != '')
								{
									unlink($img);
								}	
							}
							$values .= ", $input_name = '$archivo'";
						}
						else
						{
							$error = "Error al cargar el archivo, por favor inténtelo nuevamente.&tipo=error";
						}
					}
					else
					{
						$error = "Tipo de archivo incorrecto, debe ser $extensions y es $file_ext&tipo=notificacion";
					}
				}
				else
				{
					$error = "Tamaño del archivo debe ser menor a 20 MB &tipo=notificacion";
				}
				
			}
			else
			{
				$error = "Error al procesar el archivo, archivo no encontrado.&tipo=error";
			}
		}
		$return = array("error" => $error, "valores" => $values);
		return $return;
	}
	
	function add_img($input_name, $dir, $tabla, $id, $width, $height, $thumb, $width_thumb, $height_thumb, $bn){
		$conexion = $GLOBALS['conexion'];
		if (!file_exists($dir."/"))
		{
			$mk = mkdir($dir);
			if (!$mk)
			$error = "Error al crear el directorio.&tipo=error";
		}
		
		if ($thumb)
		{
			$dir_thumb = "$dir/thumbs/";
			if (!file_exists($dir_thumb))
			{
				$mk = mkdir($dir_thumb);
				if (!$mk)
				$error = "Error al crear el directorio.&tipo=error";
			}
		}
	
		if ($error == '')
		{
			if ($_FILES[$input_name]['name'])
			{
				if ($_FILES[$input_name]['size'] < 50000000)
				{
					$file_name =  substr($_FILES[$input_name]['name'], 0, -4);
					$file_name = str_replace(" ", "_", $file_name);
					$file_ext = strtolower(substr(strrchr($_FILES[$input_name]['name'], '.'), 1));
					if ($file_ext == 'jpg' OR $file_ext == 'png' OR $file_ext == 'gif'){
						
						$date = date("dmhis");
						$tmp_name = $date.'.'.$file_ext;
						$dir1 = "$dir/$tmp_name";
						$move = move_uploaded_file($_FILES[$input_name]['tmp_name'], $dir1);
						
						if ($move)
						{
							$animated_gif = is_animated_gif($dir1);
							if($animated_gif)
							{
								$archivo = $date.'_'.$file_name.'.'.$file_ext;
								$dir2 = "$dir/$archivo";
								$copy = rename($dir1, $dir2);
							} else {
								//Cambio el tamaño de la imagen (archivo, altura, ancho)
								$new_file = resize_foto($dir1, $height, $width, false);
								$archivo = $date.'_'.$file_name.'.'.$file_ext;
								$dir2 = "$dir/$archivo";
                                $copy = copy($new_file, $dir2);
								
                                if(opciones("usar_webp")){
                                   $archivoWebp = $date.'_'.$file_name.'.webp';
                                    $dirWebp = "$dir/$archivoWebp";
                                    $path_lib_webp = opciones("path_lib_webp");
                                    $salida = shell_exec("$path_lib_webp -q 80 $dir2 -o $dirWebp"); 
                                }
                                
                                
                                if(opciones("s3") == 1){
                                    die("test integracion s3");
                                }
                                
                                
                                //die("$salida");
								//Creo el thumb si es que se debe hacer
								if ($thumb)
								{
									$new_file_thumb = resize_foto($dir1, $height_thumb, $width_thumb, false);
									$archivo_thumb = $date.'_'.$file_name.'.'.$file_ext;
									$dir3 = "$dir_thumb/$archivo_thumb";
									$copy = copy($new_file_thumb, $dir3);
									
                                    if(opciones("usar_webp")){
                                        $archivoWebp_thumb = $date.'_'.$file_name.'.webp';
                                        $dirWebp2 = "$dir_thumb/$archivoWebp_thumb";
                                        $path_lib_webp = opciones("path_lib_webp");
                                        $salida = shell_exec("$path_lib_webp -q 80 $dir3 -o $dirWebp2"); 
                                    }

									//Paso el thumb a blanco y negro
									if ($bn)
									{
										$new_file_thumb_bn = resize_foto($dir1, $height_thumb, $width_thumb, true);
										$archivo_thumb_bn = $date.'_'.$file_name.'_bn.'.$file_ext;
										$dir4 = "$dir_thumb/$archivo_thumb_bn";
										$copy = copy($new_file_thumb_bn, $dir4);
									}
								}
							}
								
							if ($copy)
							{
								
								if (!$animated_gif)
								{
									unlink($new_file);
									unlink($dir1);
								}
								if ($id)
								{
									
									$img_q = "SELECT $input_name FROM $tabla WHERE id = '$id'";
									$run = mysqli_query($conexion, $img_q);
									$res = mysqli_fetch_array($run);
									$img1 = "$dir/".$res[0];
									$img2 = "$dir/thumbs/".$res[0];
									if ($res[0]!='')
									{
										if (file_exists($img1))
										unlink($img1);
										if (file_exists($img2))
										unlink($img2);
									}
								}
								$values .= ", $input_name = '$archivo'";
								
								
							}
							else
							{
								$size = getimagesize($dir);
								$error = "Error al copiar la imagen&tipo=error";
							}
						}
						else
						{
							$error = "Error al cargar la imágen, por favor inténtelo nuevamente y revise el tamaño de la imagen.&tipo=ventana";
						}
					}
					else
					{
						$error = "Tipo de archivo incorrecto, debe ser JPG, GIF o PNG.&tipo=error";
					}
				}
				else
				{
					$error = "Tamaño de la imagen debe ser menor a 5 Mb &tipo=notificacion";
				}
				
			}
		}
		$return = array("error" => $error, "valores" => $values);
		
		return $return;
	}
	
	//Cambia tamaño de foto
	function resize_foto($file, $altura, $ancho, $bn)
	{
		$size=getimagesize($file); 
		$width=$size[0]; 
		$height=$size[1];
		
		if ($width > $ancho)
		{
			$p = $ancho/$width;
			$x = $p*$width; 
			$y = $p*$height;
			
			if ($y > $altura)
			{
				$p = $altura/$y;
				$x = $p*$x; 
				$y = $p*$y;	
			}
			
		}
		elseif ($height > $altura)
		{
			$p = $altura/$height;
			$x = $p*$width; 
			$y = $p*$height;
			
			if ($x > $width)
			{
				$p = $altura/$x;
				$x = $p*$x; 
				$y = $p*$y;
			}		
		}
		else
		{
			$x = $width;
			$y = $height;
		}
		
		$ext = strtolower(substr(strrchr($file, '.'), 1));
		$date = date("dhis");
		$nombre = $date.'_resized.'.$ext;
		//$dir = "../../pics/noticias/$nombre";
		
		//crea la nueva imagen (Espacio)
		switch ($ext) { 
		case 'jpg':     // jpg
			$imagen_origen = imagecreatefromjpeg($file);
			if ($bn){
				imagefilter($imagen_origen, IMG_FILTER_GRAYSCALE);
				imagefilter($imagen_origen2, IMG_FILTER_GRAYSCALE);
			}
			
			
			$imagen_destino = imagecreatetruecolor($x, $y);
			$res = imagecopyresampled($imagen_destino, $imagen_origen, 0, 0, 0, 0, $x, $y, $width, $height);
			break;
				
				
		
			
		case 'png':     // png
		
			$imagen_destino = imagecreatetruecolor($x, $y);
			imagealphablending($imagen_destino, false);
			imagesavealpha($imagen_destino, true); 
			
			$imagen_origen = imagecreatefrompng($file);
			if ($bn)
			imagefilter($imagen_origen, IMG_FILTER_GRAYSCALE);
			
			imagealphablending($imagen_origen, true);
			
			$res = imagecopyresampled($imagen_destino, $imagen_origen, 0, 0, 0, 0, $x, $y, $width, $height);
			
			break;
			
		case 'gif':     // gif
		
			$imagen_origen = imagecreatefromgif($file);
			if ($bn)
			imagefilter($imagen_origen, IMG_FILTER_GRAYSCALE);
			
			$imagen_destino = imagecreatetruecolor($x, $y);
			$transcolor = imagecolortransparent($imagen_origen);
	
			if($transcolor!=-1)
			{
				$trnprt_color = imagecolorsforindex($imagen_origen, $transcolor);
				$trnprt_indx = imagecolorallocatealpha($imagen_destino, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue'], $trnprt_color['alpha']);
				imagefill($imagen_destino, 0, 0, $trnprt_indx);
				imagecolortransparent($imagen_destino, $trnprt_indx);
			}
			
			$res = imagecopyresampled($imagen_destino, $imagen_origen, 0, 0, 0, 0, $x, $y, $width, $height);
			break;
		}
	
		switch ($ext) { 
			case 'jpg':     // jpg
				$src = imagejpeg($imagen_destino, $nombre, 100);
                break;
			case 'png':     // png
				$src = imagepng($imagen_destino, $nombre, 9);
				break;
			case 'gif':     // gif
				$src = imagegif($imagen_destino, $nombre, 100);
				break;
			}
		/* return $nombre; */
		return $nombre;
	}
	
	function get_asociados($tabla, $asociados, $id)
	{
		$id_tabla = singular($tabla)."_id";
		$cantidad = consulta_bd("COUNT(*)", "$asociados", "$id_tabla = $id", "");
		return $cantidad[0][0];
	}

	//nos indica que extension tiene el archivo que estamos mostrando
	function extension($nombre_archivo){
		$nombre = explode(".", $nombre_archivo); 
		$extension = end($nombre);
		return $extension;
	}
	
	//extrae la letra del numero de la op en la ruta para mostrar los tips
	function extraer_numero($cadena){		
	    $numero = "";	    
	    for( $index = 0; $index < strlen($cadena); $index++ )
	    {
	        if(is_numeric($cadena[$index]) ){
	            $numero .= $cadena[$index];
	        }
	    }
		return $numero;
	}
	
	//Obtiene los valores economicos del servidor de moldeable
	function get_valor_economico($valor) {
		$url = "https://moldeable.com/valores_economicos/$valor.php";
		$lineas = file($url);
		return trim(strip_tags($lineas[0]));
	}
	
	
	//funcion para url_amigables	
	function url_amigables($url) {
	
		//Rememplazamos caracteres especiales latinos
		$find = array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
		$repl = array('a', 'e', 'i', 'o', 'u', 'n', 'A', 'E', 'I', 'O', 'U', 'N');
		$url = str_replace ($find, $repl, $url);
		
		// Tranformamos todo a minusculas
		$url = strtolower($url);
		
		// Añaadimos los guiones
		$find = array(' ', '&', '\r\n', '\n', '+'); 
		$url = str_replace ($find, '-', $url);
		
		// Eliminamos y Reemplazamos demás caracteres especiales
		$find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
		$repl = array('', '-', '');
		
		$url = preg_replace ($find, $repl, $url);
		
		return $url;
	}
	
	function formato_fecha($fecha, $idioma){
		$fechas = explode("-", $fecha);
		
		$ano = $fechas[0];
		$dia = $fechas[2];
		if($fechas[1] == '01'){
			if($idioma == 'esp'){
				$mes = 'Enero';
			} else if($idioma == 'eng'){
				$mes = 'January';
			}
		} else if($fechas[1] == '02'){
			if($idioma == 'esp'){
				$mes = 'Febrero';
			} else if($idioma == 'eng'){
				$mes = 'February';
			}
		} else if($fechas[1] == '03'){
			if($idioma == 'esp'){
				$mes = 'Marzo';
			} else if($idioma == 'eng'){
				$mes = 'March';
			}
		} else if($fechas[1] == '04'){
			if($idioma == 'esp'){
				$mes = 'Abril';
			} else if($idioma == 'eng'){
				$mes = 'April';
			}
		} else if($fechas[1] == '05'){
			if($idioma == 'esp'){
				$mes = 'Mayo';
			} else if($idioma == 'eng'){
				$mes = 'May';
			}
		} else if($fechas[1] == '06'){
			if($idioma == 'esp'){
				$mes = 'Junio';
			} else if($idioma == 'eng'){
				$mes = 'Jun';
			}
		} else if($fechas[1] == '07'){
			if($idioma == 'esp'){
				$mes = 'Julio';
			} else if($idioma == 'eng'){
				$mes = 'July';
			}
		} else if($fechas[1] == '08'){
			if($idioma == 'esp'){
				$mes = 'Agosto';
			} else if($idioma == 'eng'){
				$mes = 'August';
			}
		} else if($fechas[1] == '09'){
			if($idioma == 'esp'){
				$mes = 'Septiembre';
			} else if($idioma == 'eng'){
				$mes = 'September';
			}
		} else if($fechas[1] == '10'){
			if($idioma == 'esp'){
				$mes = 'Octubre';
			} else if($idioma == 'eng'){
				$mes = 'October';
			}
		} else if($fechas[1] == '11'){
			if($idioma == 'esp'){
				$mes = 'Noviembre';
			} else if($idioma == 'eng'){
				$mes = 'November';
			}
		} else if($fechas[1] == '12'){
			if($idioma == 'esp'){
				$mes = 'Diciembre';
			} else if($idioma == 'eng'){
				$mes = 'December';
			}
		}
		
		if($dia == ''){
			return '';
		} else {
			if($idioma== 'esp'){
				return $dia.' de '.$mes.' del '.$ano;
			}else{
				return $mes.' '.$dia.' '.$ano;
			}
			
		}
		
	}

	function dias_transcurridos($fecha_i,$fecha_f)
	{
		$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
		$dias 	= abs($dias); 
		//$dias = floor($dias);	
		if(strtotime($fecha_i)>strtotime($fecha_f))	{
			return '-'.$dias;
		} else {
			return $dias;
		}
		
	}
	
function previewDatos($dato){
	$datoCodificado = '';
	$caracteres = str_split($dato);
	
	 foreach ($caracteres as $key => $valor) {
		if($key > 3){
			if($valor == ' '){
				$datoF = ' ';
			} elseif($valor == '@'){
				$datoF = $valor;
			}else {
				$datoF = '*';
			}
		} else {
			$datoF = $valor;
		}
		$datoCodificado .= $datoF;
	} 
	return $datoCodificado;
}

function sumaDiasHabiles(){
        //Esta pequeña funcion me crea una fecha de entrega sin sabados ni domingos  
        $fechaInicial = date("Y-m-d"); //obtenemos la fecha de hoy, solo para usar como referencia al usuario  

        $MaxDias = 2; //Cantidad de dias maximo para el prestamo, este sera util para crear el for  


         //Creamos un for desde 0 hasta 3  
         for ($i=0; $i<$MaxDias; $i++){  
            //Acumulamos la cantidad de segundos que tiene un dia en cada vuelta del for  
            $Segundos = $Segundos + 86400;  
            //Obtenemos el dia de la fecha, aumentando el tiempo en N cantidad de dias, segun la vuelta en la que estemos  
            $caduca = date("D",time()+$Segundos);  

            //Comparamos si estamos en sabado o domingo, si es asi restamos una vuelta al for, para brincarnos el o los dias...  
            if ($caduca == "Sat"){  
                $i--;  
            } else if ($caduca == "Sun"){  
                $i--;  
            } else {  
                //Si no es sabado o domingo, y el for termina y nos muestra la nueva fecha  
                $FechaFinal = date("d-m-Y",time()+$Segundos);  
            }  
        }
        return $FechaFinal;
    }
	
	function obtenerURL(){
		$host= $_SERVER["HTTP_HOST"];
		$url= $_SERVER["REQUEST_URI"];
		return "https://" . $host . $url;
		}
	
	function opciones($campo){
		$opciones = consulta_bd("valor","opciones","nombre = '$campo'","");
		$valor = $opciones[0][0];
		return $valor;	
		}

    //funcion para determinar si soporta webp el navegador y entregar la imagen correcta
    function imagen($url, $file){
        if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false ) {
            $supported = true;//die("webp is supported!") ;
        } else {
            $supported = false;
            //die("webp is not supported!") ;
        }
		if (strlen(stristr($file,'http'))>0) {
			return $file;
		}

        $nombre_fichero = $url.$file;

        if ($file == "") {
            //echo "El fichero $nombre_fichero existe";
            if($supported){
                $imagen = "sinFoto.jpg";
            } else {
                $imagen = "sinFoto.jpg";
            }
            return $url.$imagen;
            die();
        }
		
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $nombre_base = basename($file, '.'.$extension); 
        $nombreWebP = $nombre_base.".webp"; 

        if($extension == "png"|| $extension == "PNG"){
            $imagen = $nombre_fichero;
            return $imagen;
        } 
        if($supported){

            if (file_exists($url.$nombreWebP)) {
                //echo "El fichero $nombre_fichero existe";
                $imagen = $url.$nombreWebP;
            } else {
                //echo "El fichero $nombre_fichero no existe";
                $imagen = $nombre_fichero;
            }
        } else {
            $imagen = $nombre_fichero;
        }
        return $imagen;

    }//fin funcion imagen

    
function consulta_bd2($campos, $table, $conditions, $orden, $arrCondiciones) {
		$conexion = $GLOBALS['conexion'];
		
		$columnas = explode(",",$campos);
		$cant_columnas = count($columnas);
				
		$sql = "SELECT $campos FROM $table";
		if ($conditions != ''){
			$sql .= " WHERE $conditions";
		}
		if ($orden != ''){
			$sql .= " ORDER BY $orden";
		}
		
		if($run = $conexion->prepare($sql)){
			/////
			$refArr = $arrCondiciones; //envio parametros de tipo campo y variables de condiciones como arreglo
			$ref    = new ReflectionClass('mysqli_stmt');
			$method = $ref->getMethod("bind_param");
			$method->invokeArgs($run,$refArr);
			/////
			$run->execute();

			$meta = $run->result_metadata();
			while ($field = $meta->fetch_field())
			{
				$params[] = &$row[$field->name];
			}
			call_user_func_array(array($run, 'bind_result'), $params);

			if (!$run){
				die("Error description: " . $run->error);
			}


			$i = 0;
			while ($run->fetch()) {
				$j = 0;
				foreach($row as $key => $val)
				{
					$c[$j] = $val;
					$j++;	
				}
				$result[] = $c;
				$i++;
			}

			$run->close();
		} else {
            printf("Errormessage: %s\n", $conexion->error);
            printf("consulta: %s\n", $sql);
        };
		
		return $result;
	}

function update_bd2($table, $values, $conditions, $arrCondiciones){
		$conexion = $GLOBALS['conexion'];
		$sql = "UPDATE $table SET $values WHERE $conditions";
		
		$stmt = $conexion->prepare($sql);
		$refArr = $arrCondiciones; //envio parametros de tipo campo y variables de condiciones como arreglo
		$ref    = new ReflectionClass('mysqli_stmt');
		$method = $ref->getMethod("bind_param");
		$method->invokeArgs($stmt,$refArr);
		$stmt->execute();
		if ($run) {
			return true;
		} else {
			return false;
		}
	}

function insert_bd2($table, $campos, $values, $arrCondiciones){
		$conexion = $GLOBALS['conexion'];
		$sql = "INSERT INTO $table ($campos) VALUES ($values)";
		// prepare and bind
		$stmt = $conexion->prepare($sql);
		
		$refArr = $arrCondiciones; //envio parametros de tipo campo y variables de condiciones como arreglo
		$ref    = new ReflectionClass('mysqli_stmt');
		$method = $ref->getMethod("bind_param");
		$method->invokeArgs($stmt,$refArr);
		$stmt->execute();
	
		if ($run){
			return true;
		} else {
			return false;
		}
}



?>