<?php

include_once("../../../admin/conf.php");
include_once("functions.php");

$vars = get_post('password, password2');
$active_user = $_SESSION['active_user'];

//Registro de usuario
if ($_POST[registro])
{
	if ($_POST[nombre] and $_POST[apellidos] and $_POST[email] and $_POST[direccion] and $_POST[comuna] and $_POST[ciudad] and $_POST[region] and $_POST[celular] and $_POST[password] and $_POST[password_2] and $_POST[acepto])	
	{
		extract($_POST);
		$action_op = (isset($_POST[action_op]) and $_POST[action_op] != '') ? $_POST[action_op] : 1;
		
		require_once('../bcrypt_class.php');
		$bcrypt = new Bcrypt(15);
		$password = $bcrypt->hash($_POST[password]);
		
		$campos = "nombre, apellidos, email, telefono, celular, direccion, comuna, ciudad, region, password, fecha_creacion";
		$values = "'$nombre', '$apellidos', '$email', '$telefono', '$celular', '$direccion', '$comuna', '$ciudad', '$region', '$password', NOW()";
		
		//Verifico que cliente no exista
		$existe = consulta_bd("count(*)","clientes","email = '$email'","");
		$cant = $existe[0][0];
		
		if ($cant == 0)
		{	
			if ($_POST[password] == $_POST[password_2])
			{
				$update = insert_bd('clientes', $campos, $values);
				if ($update)
				{
					$u = mysql_insert_id($conexion);
					$_SESSION['active_user'] = $u;
					$nombre_completo = "$nombre $apellidos";
					//Genero el mensaje
					$mensaje = get_option_tienda('mail_bienvenida');
					$mensaje = str_replace("@nombre_completo", $nombre_completo, $mensaje);
					$mensaje = str_replace("@username", $email, $mensaje);
					$mensaje = str_replace("@password", $_POST[password], $mensaje);
					
					$enviar_bienvenida = mail_bienvenida($email, $mensaje);
					header("location:../../../index.php?op=$action_op&p=1");
				}
				else
				{
					$error = "Error al crear el usuario&tipo=error";
					header("location:../../../index.php?op=registro&error=$error$vars");
				}
			}
			else
			{
				$error = "Las password no coinciden&tipo=error";
				header("location:../../../index.php?op=registro&error=$error$vars");
			}
		}
		else
		{
			$error = "Usuario ya existe.&tipo=notificacion";
			header("location:../../../index.php?op=registro&error=$error$vars");
		}
	}
	else
	{
		$error = "Todos los campos marcados con * son obligatorios.&tipo=notificacion";
		header("location:../../../index.php?op=registro&error=$error$vars");
	}
}


if ($_POST[login])
{
	$fail_op = ($_POST[fail_op]) ? $_POST[fail_op] : 'login';
	$success_op = ($_POST[success_op]) ? $_POST[success_op] : 'home';
	
	if ($_POST[email] and $_POST[password])
	{
		$email = mysqli_real_escape_string($conexion, $_POST[email]);
		
		$valida = consulta_bd("id, password","clientes","email = '$email' and active = 1","");
		$cant = mysqli_affected_rows($conexion);
		if ($cant == 1)
		{
			require_once('../bcrypt_class.php');
			$bcrypt = new Bcrypt(15);
			$user_id = $valida[0][0];
			$password_hash = $valida[0][1];
			
			if ($bcrypt->verify($_POST[password],$password_hash))
			{
				$new_pass = $bcrypt->hash($_POST[password]);
				$update = update_bd('clientes', "last_log_in = NOW(), password = '$new_pass'", "id = '$user_id'");
				$_SESSION['active_user'] = $valida[0][0];
				$_SESSION['region_id'] = $valida[0][1];
				$url = "../../../index.php?op=$success_op";
				header("location:$url");
			}
			else
			{
				$error = "E-mail o contraseña incorrectos.&tipo=error";
				header("location:../../../index.php?op=$fail_op&error=$error$vars");
			}
		}
		else
		{
			$error = "E-mail o contraseña incorrectos.&tipo=error";
			header("location:../../../index.php?op=$fail_op&error=$error$vars");
		}
	}
	else
	{
		$error = "Debe ingresar su Email y password&tipo=notificacion";
		header("location:../../../index.php?op=$fail_op&error=$error$vars");
	}
}

if ($_POST[login_d])
{
	
	$vars = $_POST[vars];
	
	if ($_POST[email] and $_POST[password])
	{
		$email = mysqli_real_escape_string($conexion, $_POST[email]);
		$pass = md5($_POST[password]);
		
		$valida = consulta_bd("id, region_id, password","clientes","email = '$email' and not_active = 0","");
		$cant = mysqli_affected_rows($conexion);
		if ($cant == 1)
		{
			require_once('../admin/includes/bcrypt_class.php');
			$bcrypt = new Bcrypt(15);
			$user_id = $valida[0][0];
			$password_hash = $valida[0][2];
			
			if ($bcrypt->verify($_POST[password],$password_hash))
			{
				$new_pass = $bcrypt->hash($_POST[password]);
				$update = update_bd('clientes', "last_log_in = NOW(), password = '$new_pass'", "id = '$user_id'");
				$_SESSION['active_user'] = $valida[0][0];
				$_SESSION['region_id'] = $valida[0][1];
				//header("location:../../../index.php?op=tc&p=1");
			}
			else
			{
				$error = "E-mail o contraseña incorrectos.&tipo=error";
				//header("location:../../../index.php?op=login&error=$error");
			}
		}
		else
		{
			$error = "Email o contraseña incorrectos.&tipo=error";
		}
	}
	else
	{
		$error = "Debe ingresar su Email y password&tipo=notificacion";
	}
	header("location:../../../index.php?$vars&error=$error");
}

if ($_POST[fgp])
{
	$dominio_tienda = get_option('dominio_tienda');
	$nombre_tienda = get_option('nombre_tienda');
	
	if ($_POST[email])
	{
		$email = $_POST[email];
		$ok = comprobar_mail($email);
		if ($ok)
		{
			$hash = md5(date("Ymds"));
			$update = update_bd("clientes","password_hash = '$hash'","email = '$email'");
			$cant = mysqli_affected_rows($conexion);
			if ($cant == 1)
			{
				$para = $email;
				//$asunto = html_entity_decode('Olvido de contrase&ntilde;a');
				$asunto="=?utf-8?B?".base64_encode("Olvido de contraseña")."?=";

				$header = "From: $nombre_tienda <no-reply@$dominio_tienda>\nReply-To:no-reply@$dominio_tienda\n";
				$header .= "X-Mailer:PHP/".phpversion()."\n";
				$header .= "Mime-Version: 1.0\n";
				$header .= "Content-Type: text/plain; charset=iso-8859-1\r\n";
				
				$msg .= "Estimado Cliente:";
				$msg .= "\n\n";
				$msg .= "Se ha solicitado un cambio de contraseña desde nuestro sitio web, si Ud. no ha solicitado este cambio no debe realizar ninguna acción.";
				$msg .= "\n\n";
				$msg .= "Para poder realizar el cambio de contraseña haga clic en el siguiente link o cópielo en su navegador:";
				$msg .= "\n\n";
				$msg .= "http://www.$dominio_tienda/index.php?op=fgp&hash=$hash&e=$email";
				$msg .= "\n\n";
				$msg .= "Muchas gracias.";
				$msg .= "\n\n";
				$msg .= "Equipo de $nombre_tienda.";
				$aviso = mail($para, $asunto, utf8_decode($msg) ,$header);
				if ($aviso)
				{
					$error = "Se le ha enviado un correo con las instrucciones para el cambio de contraseña.&tipo=ventana";
				}
				else
				{
					$error = "Error al generar el correo, por favor inténtelo nuevamente.&tipo=ventana";
				}
				
			}
			else
			{
				$error = "No se ha encontrado el correo ingresado en nuestros registros, por favor regístrese nuevamente.&tipo=ventana";
			}
		}
		else
		{
			$error = "Correo inválido&tipo=error";
		}
	}
	else
	{
		$error = "Debe ingresar su correo.&tipo=error";
	}
	header("location:../../../index.php?op=login&error=$error");
}


if ($_POST[update_pass])
{
	$hash = $_POST[hash];
	$email = $_POST[e];
	if($_POST[password] AND $_POST[password2] AND strlen($_POST[password])>=4)
	{
		if($_POST[password] == $_POST[password2])
		{
			$id_q = consulta_bd("id","clientes","password_hash = '$hash' AND email = '$email'","");
			$id = $id_q[0][0];
			
			if ($id)
			{
				require_once('../admin/includes/bcrypt_class.php');
				$bcrypt = new Bcrypt(15);
				$password = $bcrypt->hash($_POST[password]);
				
				$new_hash = md5(date("Ymds"));
				
				$update = update_bd("clientes", "password_hash = '$new_hash', password = '$password'", "id = '$id'");
				if($update)
				{
					$error = "Su password ha sido actualizada.&tipo=exito";
				}
				else
				{
					$error = "Error al actualizar su password, por favor inténtelo nuevamente.&tipo=ventana";
				}
				$_SESSION['active_user'] = $id;
			}
			else
			{
				$error = "Error al actualizar su password, por favor inténtelo nuevamente.&tipo=ventana";
			}
			header("location:../../../index.php?op=tc&p=1&error=$error");
		}
		else
		{
			$error = "Las contraseñas no coinciden&tipo=error";
			header("location:../../../index.php?op=fgp&error=$error&hash=$hash&e=$email");
		}
	}
	else
	{
		$error = "Debe ingresar una contraseña de al menos 4 caracteres.&tipo=notificacion";
		header("location:../../../index.php?op=fgp&error=$error&hash=$hash&e=$email");
	}
}

//User nav actions
if ($_POST[update_user])
{
	if ($_POST[nombre] and $_POST[apellidos] and $_POST[direccion] and $_POST[comuna] and $_POST[telefono2])	
	{
		$exclusiones = "id, update_user";
		$update = update_entry($exclusiones, 'clientes', "id=$active_user");
		if ($update)
		{
			$error = "Hemos actualizado sus datos, muchas gracias.&tipo=exito";
		}
		else
		{
			$error = "Error al actualizar los datos.&tipo=error";
		}
	}
	else
	{
		$error = "Todos los campos marcados con * son obligatorios.&tipo=notificacion";
	}
	header("location:../../../index.php?op=8a&error=$error$vars");
}

if ($_POST[change_pass])
{
	if($_POST[password] AND $_POST[password2] AND strlen($_POST[password])>=6)
	{
		if($_POST[password] == $_POST[password2])
		{
			$new_hash = md5(date("Ymds"));
			$password = md5($_POST[password]);
			$update = update_bd("clientes", "password_hash = '$new_hash', password = '$password'", "id = '$active_user'");
			if($update)
			{
				$error = "Su password ha sido actualizada.&tipo=exito";
			}
			else
			{
				$error = "Error al actualizar su password, por favor inténtelo nuevamente.&tipo=error";
			}
		}
		else
		{
			$error = "Las contraseñas no coinciden&tipo=error";
		}
	}
	else
	{
		$error = "Debe ingresar una contraseña de al menos 6 caracteres.&tipo=notificacion";
	}
	header("location:../../../index.php?op=8a&error=$error");
}

if ($_POST[guardar_otra])
{
	$insert = insert_entry('guardar_otra', "direcciones_clientes" );
	$id_dc = get_last('direcciones_clientes');
	echo ($insert) ? $id_dc : 0;
}

?>