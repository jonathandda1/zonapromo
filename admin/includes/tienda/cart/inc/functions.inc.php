<?php
//total de items agregados al carro de compra
function totalCart(){
	global $db;
	if(!isset($_COOKIE['cart_alfa_cm'])){
		setcookie("cart_alfa_cm", "$cart", time() + (365 * 24 * 60 * 60), "/");
		}
	$cart = $_COOKIE['cart_alfa_cm'];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$total = 0;
		foreach ($contents as $prd_id=>$qty) {
			$is_cyber = (opciones('cyber') == 1) ? true : false;
			$producto_madre = consulta_bd('p.id', "productos p join productos_detalles pd on pd.producto_id = p.id", "pd.id = $prd_id", '');
			if ($is_cyber AND is_cyber_product($producto_madre[0][0])) {
				$precios = get_cyber_price($prd_id);
				$total += $precios['precio_cyber']*$qty;
			}else{
				$total += getPrecio($prd_id)*$qty;
			}
		}
	}
	return round($total);
}

/*==============================================================
=            Calculo de los totales individualmente            =
==============================================================*/
function totalCartIndividual($id=0,$cant=0){
	$is_cyber = (opciones('cyber') == 1) ? true : false;
    $producto_individual = consulta_bd('p.id', "productos p join productos_detalles pd on pd.producto_id = p.id", "pd.id = $id", '');
    if ($is_cyber AND is_cyber_product($producto_individual[0][0])) {
        $precios = get_cyber_price($id);
        $totalIndividual = $precios['precio_cyber']*$cant;
    }else{
        $totalIndividual += getPrecio($id)*$cant;
    }
    return round($totalIndividual);
}
/*=====  End of Calculo de los totales individualmente  ======*/

function qty_wish(){
	if(!isset($_COOKIE[listaDeseos])){
		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[listaDeseos];
	$return = json_decode($cart);
	return count($return);
	//return $cart;
}


//cantidad de productos en el carro
function qty_pro(){
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart){
		$items = explode(',',$cart);
		return count($items);
	}
	else
	{
		return 0;
	}
}


// function qty_fav(){
//     global $db;
// 	//$listaDeseos = $_SESSION['listaDeseos'];
// 	if(!isset($_COOKIE[listaDeseos])){
// 		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
// 	}
// 	$listaDeseos = json_decode($_COOKIE[listaDeseos], true);
// 	if ($listaDeseos) {
// 		$itemDiferente = 0;
// 		for ($i=0; $i<sizeof($listaDeseos); $i++){
//          	$itemDiferente = $itemDiferente + 1;
// 		}
// 	} 
// 	return $itemDiferente;
// }

function ultimasUnidades($pid){
	$ultimas = get_option('ultimas_unidades');
	if($ultimas){
		$cant = get_option('cant_ultimas_unidades');
		$prd = consulta_bd("stock","productos_detalles","id = $pid","");
		if((int)$prd[0][0] <= (int)$cant){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function ofertaTiempo($id){
    $fechaActual = date("Y-m-d H:i:s");
    $oferta = consulta_bd("oferta_tiempo_activa, oferta_tiempo_hasta","productos_detalles","id = $id","");
	if($oferta){
		if($oferta[0][0] == 1 and $oferta[0][1] >= '$fechaActual'){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function ofertaTiempoHasta($id){
	$oferta = consulta_bd("oferta_tiempo_hasta","productos_detalles","id = $id","");
	return $oferta[0][0];
}
function ofertaTiempoDescuento($id){
	$oferta = consulta_bd("oferta_tiempo_descuento","productos_detalles","id = $id","");
	return $oferta[0][0];
}

function getPrecio($pd){
	$is_cyber 	= (opciones('cyber') == 1) ? true : false;
	$detalles 	= consulta_bd("pd.precio, pd.descuento, p.id, pd.precio_cyber","productos_detalles pd, productos p ","p.id = pd.producto_id AND pd.id = $pd","");
	$precio 	= $detalles[0][0];

    
	if ($is_cyber AND is_cyber_product($detalles[0][2])) {
		$descuento 	= $detalles[0][3];
	}else{
		$descuento 	= $detalles[0][1];
	}

	if(ofertaTiempo($pd)){
		if(ofertaTiempoDescuento($pd) > 0){
			$descuento = ofertaTiempoDescuento($pd);
		}
	}

	if($descuento AND $precio > $descuento){
		$precio_final = $descuento;
	}else{
		$precio_final = $precio;
	}

	return $precio_final;
}



function getPrecioNormal($pd){
	$detalles 	= consulta_bd("pd.precio","productos_detalles pd","pd.id = $pd","");
	return $detalles[0][0];
}

function tieneDescuento($pd){
	$detalles 	= consulta_bd("pd.precio, pd.descuento","productos_detalles pd","pd.id = $pd","");
	$precio 	= ($detalles[0][0]) ? $detalles[0][0] : 0;//$detalles[0][0];
	$descuento 	= ($detalles[0][1]) ? $detalles[0][1] : 0;//$detalles[0][1];
    
    if($descuento != NULL AND $precio > $descuento) return true;
	else return false;
}

function get_cyber_price($pd){
	$sql = consulta_bd("pd.precio, pd.precio_cyber", "productos_detalles pd", "pd.id = $pd", "");
	$out['precio'] = $sql[0][0];
	$out['precio_cyber'] = $sql[0][1];
	return $out;
}

function hasCategoria($li){
	$row = consulta_bd("id","categorias","linea_id = $li AND publicada = 1","");
	if($row) return true;
	else return false;
}
function hasSubCat($cat){
	if($cat){
		$row = consulta_bd("id","subcategorias","categoria_id = $cat AND publicada = 1","");
		return count($row[0][0]);
	}else{
		return 0;
	}
}


function ShowCart(){
	global $db;

	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.venta_minima","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id = $prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "sin-imagen.jpg";
			}
			
			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);

			if(!tieneDescuento($prd_id)){
               $pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
						$valor 			= $valorUnitario * $qty;
                        
                         //die("$valorUnitario");
					}
				}
			}

			$no_disponible = ($_GET['stock'] == $prd_id) ? 1 : 0;

			
			   
	$output[] .='
                
                <div class="filaProductosCarro" id="fila_carro_'.$prd_id.'">
                	<a href="javascript:void(0)" class="eliminarCart" onclick="eliminaItemCarro('.$prd_id.')"><i class="fas fa-times"></i></a>
                    <div class="imgFilaCart">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<img src="imagenes/productos/'.$thumbs.'" width="100%" />
						</a>
					</div>
					<div class="contCarroResumen1">
						<p class="tituloItemCarroResumen">Producto</p>
						<p class="nomProdCarroResumen">
							<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span>'.$producto[0][1].'</span>
							</a>
						</p>	
					</div>
					<div class="contCarroResumen1">
						<p class="tituloItemCarroResumen">Precio</p>
						<p class="">
							<div class="precioCarroResumen">
								<span class="unitariosCarro unitarioValor">$'.number_format($valorUnitario,0,",",".").'</span>
							</div>
						</p>	
					</div>
					<div class="contCarroResumen1">
						<p class="tituloItemCarroResumen">Cantidad</p>
						<div class="qtyCart">'.$qty.'</div>
						<p class="">
							<!--<div class="contSpinner">
								<div class="pull-left spinnerCarro" id="spinnerCarro_'.$prd_id.'">
									<input type="text" name="cant" class="campoCantCarroResumen" value="'.$qty.'" />
									<div class="contFlechas">
										<span class="mas" onclick="agregarElementoCarro('.$prd_id.', '.$producto[0][6].')"  rel="'.$prd_id.'"><i class="fas fa-chevron-up"></i></span>
										<span class="menos" onclick="quitarElementoCarro('.$prd_id.', '.$producto[0][6].')" rel="'.$prd_id.'"><i class="fas fa-chevron-down"></i></span>
									</div>
								</div>
							</div> -->
							<div class="box_spinnerCarro" id="spinnerCarro_'.$prd_id.'">
								<div class="sep_spin spin_action" data-action="quitar" data-where="ficha" onclick="quitarElementoCarro('.$prd_id.', '.$producto[0][6].')" rel="'.$prd_id.'">-</div>
								<div id="" class="sep_spin qty_spin">'.$qty.'</div>
								<div class="sep_spin spin_action" data-action="agregar" data-where="ficha" onclick="agregarElementoCarro('.$prd_id.', '.$producto[0][6].')"  rel="'.$prd_id.'">+</div>
							</div>
						</p>	
					</div>
					<div class="contCarroResumen1">
						<p class="tituloItemCarroResumen">Total item</p>
						<p>
							<div class="precioCarroResumen">
								<span class="subtotal">$'.number_format($valor,0,",",".").'</span>
							</div>
						</p>						
					</div>';

                    if($no_disponible){
                    	$output[] .='
							<div class="cantFila ancho10">
								<div class="sin-stock">Sin stock</div>
							</div>';
                    }

           $output[] .='                  
                </div><!-- fin filaProductos-->';
		}
			
	} else {
		$output[] = '<div class="carroVacio">
                        <div class="iconoCarroVacio"><img src="img/iconoCarro.png" /></div>
						<h2 class="tituloCarroVacio">Tu carro esta vacío</h2>
                        <a class="btnCarroVacio" href="home">Comenzar a comprar</a>
					</div>';
	}
	return join('',$output);
	
}

function ShowCartPopUp(){
	global $db;
	if(!isset($_COOKIE['cart_alfa_cm'])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	} 
	$cart = $_COOKIE['cart_alfa_cm'];
	if(!isset($_COOKIE['colorProd'])){
		setcookie('colorProd', "", time() + (365 * 24 * 60 * 60), "/");
	}
	$colorProducto = json_decode(stripslashes( $_COOKIE['colorProd']) , true);
	// var_dump($colorProducto);
	if(!isset($_COOKIE['cantColorProd'])){
		setcookie('cantColorProd', "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cantColorProducto = json_decode(stripslashes( $_COOKIE['cantColorProd']) , true);
	// var_dump($cantColorProducto);
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '<h3>Agregado al carro de cotización</h3>
        <div class="ancho100 floatLeft carroScroll scroll-rojo">';
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("pd.imagen1, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.venta_minima, pd.precio_cyber","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id = $prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "sin-imagen.jpg";
			}
			
			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
						$valor 			= $valorUnitario * $qty;
					}
				}
			}

			$no_disponible = ($_GET['stock'] == $prd_id) ? 1 : 0;

			
			   
	$output[] .='<div class="filaProductosPopUp" id="fila_carro_'.$prd_id.'">
                	<div class="imgFilaPopUp">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<img src="'.imagen("imagenes/productos_detalles/",$thumbs).'" width="100%" />
						</a>
					</div>
					<div class="contInfoShowCartPopUp">
						<div class="eliminarCartPopUp">
							<a href="javascript:void(0)" onclick="eliminaItemCarroPopUp('.$prd_id.')">
								<i class="material-icons">close</i>
							</a>
						</div>
						<div class="nombreFilaPopUp">
							
                            <a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span class="nombreMarca">'.nombreMarca($producto[0][4]).'</span>
							</a>
                            
                            <a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span>'.$producto[0][1].'</span>
							</a>
						</div>';
					$output[] .='<div class="skuGrilla">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<span>SKU: '.$producto[0][5].'</span>
						</a>
					</div>';
				
				$cantColorHtml = "";
						if (count($cantColorProducto) > 0) {
							if ($cantColorProducto[$prd_id] != 0) {
								$idCantColor = $cantColorProducto[$prd_id];
								$cantColor = consulta_bd("l.nombre", "logo l", "l.id = $idCantColor", "l.nombre asc");
								$cantColorHtml ='
								<div class="colorPopUp">
									<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
										<strong>Cant Colores: </strong> <span class="colorCarroPopup">'.$cantColor[0][0].'</span>
									</a>
								</div>';
							}
							$output[] .= $cantColorHtml;
						}   
						
				$colorHtml = "";
						if (count($colorProducto) > 0) {
							if ($colorProducto[$prd_id] != 0) {
								$idColor = $colorProducto[$prd_id];
								$color = consulta_bd("c.nombre", "color_productos c", "c.id = $idColor", "c.nombre asc");
								$colorHtml ='
								<div class="colorPopUp">
									<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
										<strong>Color impresión: </strong> <span class="colorCarroPopup">'.$color[0][0].'</span>
									</a>
								</div>';
							}
							$output[] .= $colorHtml;
						}   

            $output[] .='
						<div class="cont100">
						<div class="precioFila">';
						if ($is_cyber AND is_cyber_product($producto[0][4])){
							$precios = get_cyber_price($prd_id);
									
									
						$output[] .='
							<span class="conDescuento">C/U: $'.number_format($precios['precio_cyber'],0,",",".").'</span>
							<span class="antes">$'.number_format($precios['precio'],0,",",".").'</span>';
									} else {
										if($producto[0][3] > 0){ 
											
							$output[] .='
										<span class="conDescuento">C/U: $'.number_format(getPrecio($prd_id),0,",",".").'</span>
										<span class="antes">$'.number_format(getPrecioNormal($prd_id),0,",",".").'</span>';
										}else{
											$output[] .='
											<span class="conDescuento">C/U: $'.number_format(getPrecio($prd_id),0,",",".").'</span>
											<span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>';
										} 
									}
						$masde10i = 'style="display:none"';
							if($qty > 10) $masde10i = 'style="display:block"';
						$masde10s = 'style="display:block"';
							if($qty > 10) $masde10s = 'style="display:none"';

						$output[] .='</div> <!-- fin precioFila-->
							
						</div>
						<div class="cont100">
                            <div class="ancho60">
								<div class="cantPopUp">
									<label for="cantidadCarro" class="label">Cantidad: </label>
									<input  valId="'.$prd_id.'" type="text" name="cant" class="cantidadCarro" value="'.$qty.'" '.$masde10i.' />
									<div class="selectCantidadCarro" rel="'.$qty.'"  '.$masde10s.'>
												<select name="cant" valId="'.$prd_id.'" class="cantidadCarro sinBorde">';
													for($x = 1;$x < 11;$x++){
														if($x == $qty) $isQty = "selected";
														else $isQty = "";
														$output[] = '<option value="'.$x.'" '.$isQty.'>'.$x.'</option>';
													}

										$output[] = '<option value="11">más</option>
														</select>
									</div>
								</div>
                            </div> 
                            
                            <div class="ancho40">';
                                

                    $output[] .='<div class="precioFila totalIndividual">Subtotal: $'.number_format(totalCartIndividual($prd_id,$qty),0,",",".").'</div>';
                $output[] .='</div>
                        </div>
                        
                        
						
					</div><!-- fin contInfoShowCart-->
                    
                    
					
                    <div class="cantFila ancho10">';

                    if($no_disponible){
                    	$output[] .='<div class="sin-stock">Sin stock</div>';
                    }

           $output[] .='
                    </div>
                    
                </div><!-- fin filaProductos-->';
		}
        $output[] .='</div><!--Fin ancho50-->';
        
        // $output[] .='<div class="contDatosCompra">
        //     	<div class="cantArticulos floatLeft ancho100">Los costos de envio y de instalacion se calcularán previo al pago</div>
        //         <a href="envio-y-pago" class="btnPopUpComprar">Finalizar compra $<span id="totalCartPopUp">'.number_format(totalCart(),0,",",".").'</span></a>
        //         <a href="javascript:cerrar()" class="btnPopUpSeguirComprando">Seguir comprando</a>
        //     </div><!--fin contDatosCompra -->';
		$output[] .='<div class="contDatosCompra">
                <a href="carro-cotizacion" class="btnPopUpComprar">Cotizar</a>
                <a href="javascript:cerrar()" class="btnPopUpSeguirComprando">Seguir cotizando</a>
            </div><!--fin contDatosCompra -->';
        
	} else {
		$output[] = '<div class="carroVacio">
                        <div class="iconoCarroVacio"><img src="img/iconoCarro.png" /></div>
						<h2 class="tituloCarroVacio">Tu carro esta vacío</h2>
                        <a class="btnCarroVacio" href="home">Comenzar a comprar</a>
					</div>
                    <div class="sugeridosCarro">
                        '.vistosRecientemente("lista","3").'
                        <div class="contDatosCompra">
	                		<a href="javascript:cerrar()" class="btnPopUpSeguirComprando">Seguir comprando</a>
	            		</div><!--fin contDatosCompra -->
                    </div>
                    ';
                    
                        
        
	}
	return join('',$output);
	
}

//cantidad de productos en el carro
function qty_cotizar(){
	if(!isset($_COOKIE['cart_alfa_cm'])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cotizacion = $_COOKIE['cart_alfa_cm'];
	if ($cotizacion){
		$items = explode(',',$cotizacion);
		return count($items);
	}
	else
	{
		return 0;
	}
}

function ShowCartCotizar(){
	global $db;
	if(!isset($_COOKIE['cart_alfa_cm'])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	} 
	$cart = $_COOKIE['cart_alfa_cm'];
	if(!isset($_COOKIE['colorProd'])){
		setcookie('colorProd', "", time() + (365 * 24 * 60 * 60), "/");
	}
	$colorProducto = json_decode(stripslashes( $_COOKIE['colorProd']) , true);
	// var_dump($colorProducto);
	if(!isset($_COOKIE['cantColorProd'])){
		setcookie('cantColorProd', "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cantColorProducto = json_decode(stripslashes( $_COOKIE['cantColorProd']) , true);
	// var_dump($cantColorProducto);
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '<h3>Mi carro</h3>
        <div class="ancho100">';
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("pd.imagen1, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.venta_minima, pd.precio_cyber","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id = $prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "sin-imagen.jpg";
			}
			   
	$output[] .='<div class="filaProductosCotizar" id="fila_carro_'.$prd_id.'">
                	<div class="imgFilaPopUp">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<img src="'.imagen("imagenes/productos_detalles/",$thumbs).'" width="100%" />
						</a>
					</div>
					<div class="contInfoShowCartPopUp">
						<div class="nombreFilaPopUp">
							
                            <a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span class="nombreMarca">'.nombreMarca($producto[0][4]).'</span>
							</a>
                            
                            <a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span>'.$producto[0][1].'</span>
							</a>
						</div>';
					$output[] .='<div class="skuGrilla">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<span>SKU: '.$producto[0][5].'</span>
						</a>
					</div>';
				
				$cantColorHtml = "";
						if (count($cantColorProducto) > 0) {
							if ($cantColorProducto[$prd_id] != 0) {
								$idCantColor = $cantColorProducto[$prd_id];
								$cantColor = consulta_bd("l.nombre", "logo l", "l.id = $idCantColor", "l.nombre asc");
								$cantColorHtml ='
								<div class="colorPopUp">
									<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
										<strong>Cant Colores: </strong> <span class="colorCarroPopup">'.$cantColor[0][0].'</span>
									</a>
								</div>';
							}
							$output[] .= $cantColorHtml;
						}   
						
				$colorHtml = "";
						if (count($colorProducto) > 0) {
							if ($colorProducto[$prd_id] != 0) {
								$idColor = $colorProducto[$prd_id];
								$color = consulta_bd("c.nombre", "color_productos c", "c.id = $idColor", "c.nombre asc");
								$colorHtml ='
								<div class="colorPopUp">
									<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
										<strong>Color impresión: </strong> <span class="colorCarroPopup">'.$color[0][0].'</span>
									</a>
								</div>';
							}
							$output[] .= $colorHtml.'</div><!-- fin contInfoShowCart-->';
						}   
			$masde10i = 'style="display:none"';
				if($qty > 10) $masde10i = 'style="display:block"';
			$masde10s = 'style="display:block"';
				if($qty > 10) $masde10s = 'style="display:none"';
            $output[] .='<div class="contCantCotizar">
							<div class="cantCotizar">
								<input  valId="'.$prd_id.'" type="text" name="cant" class="cantidadCarro" value="'.$qty.'" '.$masde10i.' />
								<div class="selectCantidadCarro" rel="'.$qty.'"  '.$masde10s.'>
									<select name="cant" valId="'.$prd_id.'" class="cantidadCarro">';
										for($x = 1;$x < 11;$x++){
											if($x == $qty) $isQty = "selected";
											else $isQty = "";
											$output[] = '<option value="'.$x.'" '.$isQty.'>'.$x.'</option>';
										}

				            $output[] = '<option value="11">más</option>
				                            </select>
								</div> 
							</div>
							<div class="eliminarCotizar">
								<a href="javascript:void(0)" onclick="eliminaItemCarroPopUp('.$prd_id.')">Eliminar</a>
							</div>';
							
            $output[] .='</div><!-- fin contCantCotizar-->';
           $output[] .='
		   </div><!-- fin filaProductos-->';
		}
        $output[] .='</div><!--Fin ancho50-->';
        
	} else {
		$output[] = '<div class="carroVacio">
                        <div class="iconoCarroVacio"><img src="img/iconoCarro.png" /></div>
						<h2 class="tituloCarroVacio">Tu carro esta vacío</h2>
                        <a class="btnCarroVacio" href="home">Comenzar a comprar</a>
					</div>
                    <div class="sugeridosCarro">
                        '.vistosRecientemente("lista","3").'
                        <div class="contDatosCompra">
	                		<a href="javascript:cerrar()" class="btnPopUpSeguirComprando">Seguir comprando</a>
	            		</div><!--fin contDatosCompra -->
                    </div>
                    ';
                    
                        
        
	}
	return join('',$output);
	
}

function saveForLater(){
	global $db;
	//$listaDeseos = $_SESSION['listaDeseos'];
	if(!isset($_COOKIE['listaDeseos'])){
		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$listaDeseos = json_decode($_COOKIE['listaDeseos'], true);

	if ($listaDeseos) {
		$itemDiferente = 0;
		for ($i=0; $i<sizeof($listaDeseos); $i++){
         	$itemDiferente = $itemDiferente + 1;
		}
		$output[] = '<div class="mensajeGuardados">Tienes artículos guardados para comprar más tarde. Para comprar uno, o más, ahora, haga clic en Mover al carrito junto al artículo.</div>
		<div class="tituloGuardados">Articulos guardados para despues <span>('.$itemDiferente.')</span></div>';
		
		
		for ($i=0; $i<sizeof($listaDeseos); $i++){
         	$prd_id = $listaDeseos[$i]['id'];
			
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, p.descripcion","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id = $prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "sin-imagen.jpg";
			}
			
			$valor 			= getPrecio($prd_id) * $qty;
			$valorUnitario 	= getPrecio($prd_id);

			/*if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= $valorUnitario - ($valorUnitario * ($descuento / 100));
						$valor 			= $valorUnitario * $qty;
					}
				}
			}*/
			
			
			   
	$output[] .='<div class="filaProductos" id="fila_carro_guardado'.$prd_id.'">
                	<div class="imgFila">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<img src="'.imagen("imagenes/productos/", $thumbs).'" width="100%" />
						</a>
					</div>
                    
                    <a class="eliminarFilaGuardadosOculto" href="javascript:void(0)" onclick="eliminaFavorito('.$prd_id.')" rel="'.$prd_id.'"><i class="fas fa-times"></i></a>
                    
					<div class="contCarroGuardado">
						<div class="nombreFila">
							<a class="nombreProdGuardados" href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span>'.$producto[0][1].'</span>
							</a>
							<div class="breveDescripcion">
								'.preview($producto[0][5], 170).'
							</div>
						</div>
						<div class="botonesFilaGuardado">
							<a class="eliminarFilaGuardados" href="javascript:void(0)" onclick="eliminaFavorito('.$prd_id.')" rel="'.$prd_id.'">Eliminar</a>
							<span class="separadorGuardados"> | </span>
							<a class="moverAlCarroGuardados" href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">Ver ficha</a>
						</div>
					</div><!-- fin contCarroGuardado -->
                    <div class="precioFilaGuardado ancho20">
						<span>$'.number_format($valorUnitario,0,",",".").'</span>
					</div>
					
                </div><!-- fin filaProductos-->';
		}//fin for
		
		
		
		
		
		
			
			
	} 
	return join('',$output);
	}






function guardadoParaDespues($id){
	if(!isset($_COOKIE[listaDeseos])){
		setcookie("listaDeseos", "", time() + (365 * 24 * 60 * 60), "/");
	}
	//$listaDeseos = $_COOKIE[listaDeseos];
    $listaDeseos = json_decode($_COOKIE['listaDeseos'], true);
    if(is_array($listaDeseos)){
	   //echo "es un arreglo";
        $existe = 0;
        $existe2 = "es un arreglo";
        for ($i=0; $i<sizeof($listaDeseos); $i++){
            if ($listaDeseos[$i]['id'] === $id) {
               $result = "producto ya existe";
               $existe = 1;
             } else {
                 $existe = $existe;
                 $result = "producto aun no existe";
              }
            
        }/*fin for */
        
	} else {
	   $existe = 0;
       $existe2 = "no es un arreglo";
	}
    return $existe;
}



function resumenCompra(){
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		
		$i = 1;
		$total = 0;
		$cantArticulos = 0;
		foreach ($contents as $prd_id=>$qty) {
			//$sql = consulta_bd("pd.id, pd.precio, pd.descuento, p.id","productos_detalles pd join productos p on p.id = pd.producto_id","p.id = $prd_id","");

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$total 			+= $valorUnitario * $qty;
					}else{
						$total += getPrecio($prd_id) * $qty;
					}
				}else{
					$total += getPrecio($prd_id) * $qty;
				}
			}else{
				$total += getPrecio($prd_id) * $qty;	
			}
			
			$cantArticulos = $cantArticulos + $qty;
		}
		
	}
	$neto = $total/1.19;
	$iva = $total - $neto;
	$resumen = '<div class="ancho100">
					<h3><h class="azul">Subtotal</h> <h>('.$cantArticulos.' articulos):</h> <br><span>$'.number_format(round($total),0,",",".").'</span></h3>
					<div class="ancho100 filatoolTip">*El valor del despacho se definira siguiente paso.</div>
					<button type="submit" form="volverForm" class="btnCompletarCompra" id="btnCompletarCompra">PAGAR</button>
				</div>';
	$resumen .= '<div class="cont100carro1 fixedCarro1">
					<div class="cont100Centro centroFixedCarro1">
					<button type="submit" form="volverForm" class="btnCompletarCompra">PAGAR</button>
						<div class="valoresCarro1Fixed"><h class="azul">Subtotal</h><h>('.$cantArticulos.' articulos):</h> <span class="montoAPagarFixed"><strong >$'.number_format(round($total),0,",",".").'</strong></span></div>
					</div>
				</div>';
	if(qty_pro() > 0){
		return $resumen;
		} else {
		//return false;
		}
	
}



function vistosRecientemente($vista, $cantidad) {
	global $db;
	$output[] = "";
	$recientes = $_COOKIE['productosRecientes'];
	if ($recientes) {
		$items = explode(',',$recientes);
		$items = array_reverse($items);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		// $output[] = '<h4 class="tituloMiCarro">Vistos recientemente</h4>';
		$i = 0;
        $j = 0;
		foreach ($contents as $id=>$qty) {
			
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.id, pd.precio_cyber, m.nombre,p.descripcion_breve","productos p, productos_detalles pd, marcas m","p.id = $id and p.id = pd.producto_id and p.marca_id = m.id","");
			$cant = count($producto);
			
			if($cant > 0){
				$j = $j + 1;
				if($producto[0][0] != ''){
					$thumbs = $producto[0][0];
				} else {
					$thumbs = "sin-imagen.jpg";
				}
				
				$valorUnitario 	= getPrecio($producto[0][6]);
	
				
				//solo muestro los ultimos 3
				if($cantidad > 0){
					$cantidad = $cantidad;
					} else {
						$cantidad = 99999999999;
						}
				
				if($vista == "grilla"){
					$vista = "grilla";
					} else {
						$vista = "filaProductosVistos";
						}		
		
				if($i < $cantidad){	
						   
					$output[] .='<div class="'.$vista.'" >';
            		
            		$output[] .= '<a href="#" class="like" rel="'.$producto[0][6].'">
                    	<i class="';
                    $clase = (guardadoParaDespues($producto[0][6])) ? 'fas':'far'; 
                    $output[] .= ''.$clase.' fa-heart"></i>
                    </a>';       
                     
                    $output[] .= ''.porcentajeDescuento($producto[0][6]).'
								<div class="imgFila">								
									<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">									
                                        <img src="'.imagen('imagenes/productos/', $thumbs).'" width="100%" />
									</a>
								</div>
                                
								<div class="contInfoShowCart">

									<a class="btnFicha" href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'"><span class="botonDetalle">Ver Ficha</span></a>';
									

					$output[] .='   <div class="valorGrilla">';
                                        if ($is_cyber AND is_cyber_product($producto[0][0])){
                                            $precios = get_cyber_price($producto[0][6]);
					$output[] .='
									<span class="ahorroValor">'.ahorras($producto[0][4]).'</span>
									<span class="conDescuento">$'.number_format($producto[0][7],0,",",".").'</span>
									<span class="antes"> $'.number_format($producto[0][2],0,",",".").'</span>';
								} else {
									if($producto[0][3] > 0){ 
									$output[] .='
										<span class="ahorroValor">'.ahorras($producto[0][4]).'</span>
										<span class="conDescuento">$'.number_format($producto[0][3],0,",",".").'</span>
										<span class="antes"> $'.number_format($producto[0][2],0,",",".").'</span>';
									}else{
										$output[] .='<span class="conDescuento">$'.number_format($producto[0][2],0,",",".").'</span>
										<span class="antes">&nbsp;&nbsp;&nbsp;</span>';
										} 
								}
								
			
								$output[] .='
							</div>';
							
							$output[] .='<div class="nombreGrilla">
										<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
											<span>'.$producto[0][1].'</span>
										</a>
									</div>
									<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'" class="descripcionGrilla">'.strip_tags(preview($producto[0][9], 130)).'</a>
									
									';
							$output[] .='<div class="skuGrilla">
											<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
												<span>SKU: '.$producto[0][5].'</span>
											</a>
										</div>';
									
							$output[] .='
								</div><!-- fin contInfoShowCart-->
								
							</div><!-- fin filaProductos-->';
							$i = $i + 1;
							
						}
						
				} //fin condicion de cantidad
		}

        /*$cantVistos = count($items);
        if($cantVistos < $cantidad){
            $faltantes = $cantidad - $cantVistos;*/
        if($j < $cantidad){
            $faltantes = $cantidad - $j;
                
            $pf = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku, pd.id, pd.precio_cyber, m.nombre,p.descripcion_breve","productos p, productos_detalles pd, marcas m","p.id = pd.producto_id and p.marca_id = m.id and p.id","RAND() limit $faltantes");
            
            for($i=0; $i<sizeof($pf); $i++) {
			
                if($pf[$i][0] != ''){
                        $thumbs = $pf[$i][0];
                    } else {
                        $thumbs = "sin-imagen.jpg";
                    }

                    $valorUnitario 	= getPrecio($pf[$i][6]);

                    if($vista == "grilla"){
                        $vista = "grilla";
                        } else {
                            $vista = "filaProductosVistos";
                            }		

	                    $output[] .='<div class="'.$vista.'" >';
	                    $output[] .= '<a href="#" class="like" rel="'.$pf[$i][6].'">
	                    	<i class="';
	                    $clase = (guardadoParaDespues($pf[$i][6])) ? 'fas':'far'; 
	                    $output[] .= ''.$clase.' fa-heart"></i>
	                    </a>';

                        $output[] .= '
                        			'.porcentajeDescuento($pf[$i][6]).'
                                    <div class="imgFila">
                                        <a href="ficha/'.$pf[$i][4].'/'.url_amigables($pf[$i][1]).'">
                                            <img src="'.imagen('imagenes/productos/', $thumbs).'" width="100%" />
                                        </a>
                                    </div>

                                    <div class="contInfoShowCart">
									
									<a class="btnFicha" href="ficha/'.$pf[$i][4].'/'.url_amigables($pf[$i][1]).'"><span class="botonDetalle">Ver Ficha</span></a>
										
									<div class="valorGrilla">';
									if ($is_cyber AND is_cyber_product($pf[$i][0])){
										$precios = get_cyber_price($pf[$i][6]);
						$output[] .='
										<span class="ahorroValor">'.ahorras($pf[$i][4]).'</span>
										<span class="conDescuento">$'.number_format($pf[$i][7],0,",",".").'</span>
										<span class="antes"> $'.number_format($pf[$i][2],0,",",".").'</span>';
									} else {
										if($pf[$i][3] > 0){ 
										$output[] .='
											<span class="ahorroValor">'.ahorras($pf[$i][4]).'</span>
											<span class="conDescuento">$'.number_format($pf[$i][3],0,",",".").'</span>
											<span class="antes"> $'.number_format($pf[$i][2],0,",",".").'</span>';
										}else{
											$output[] .='<span class="conDescuento">$'.number_format($pf[$i][2],0,",",".").'</span>    
											<span class="antes">&nbsp;&nbsp;&nbsp;</span>';
										 } 
									}
									$output[] .='
                                        </div>
                                        <div class="nombreGrilla">
                                            <a href="ficha/'.$pf[$i][4].'/'.url_amigables($pf[$i][1]).'">
                                                <span>'.$pf[$i][1].'</span>
                                            </a>
                                        </div>
										<a href="ficha/'.$pf[$i][4].'/'.url_amigables($pf[$i][1]).'" class="descripcionGrilla">
											'.strip_tags(preview($pf[$i][9], 130)).'
										</a>
										
										';
                                    
								$output[] .='</div><!-- fin contInfoShowCart-->

                                </div><!-- fin filaProductos-->';
                                

                            

                    
            }
            
            
        }
	} 
	
	return join('',$output);
	
}



function dctoCantidad($id){
    global $db;
    $prd_id = $id;
    $dctoCantidad = 0;
    if(!tieneDescuento($prd_id)){
        $pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
        if($pd[0][0]){
            $dctoCantidad = 1;
        }
    }
    
    if($dctoCantidad){
        $htmlDctoCantidad = '<div class="etiquetaDctoCantidad">Dcto. por cantidad</div>';
    } else {
        $htmlDctoCantidad = '';
    }
    
    return $htmlDctoCantidad;
}



function resumenCompraShort($comuna_id, $retiro, $idSucursal=0) {
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$output[] .= '<div class="conFilasCarroEnvioYPago scroll-rojo">	';

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs, p.costo_instalacion","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			
            
            $valorUnitario 	= $productos[0][2];
            $valorUnitarioConDescuento 	= $productos[0][3];
            
            if($valorUnitarioConDescuento > 0 AND $valorUnitarioConDescuento < $valorUnitario){
                $precio_final 	= $valorUnitarioConDescuento * $qty;
            } else {
                $precio_final 	= $valorUnitario * $qty;
            }
            
          
			$precio_final = getPrecio($prd_id) * $qty;

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitarioConDescuento 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitarioConDescuento * $qty;
					}
				}
			}


			
		$output[] .= '<div class="filaResumen">
                        <a href="javascript:void(0)" class="eliminarCart2" onclick="eliminaItemCarro2('.$prd_id.')"><i class="fas fa-times"></i></a>
                        
                       <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="imgThumbsCartHeader">
                            <div class="qtyCart">'.$qty.'</div>
                              <img src="'.imagen("imagenes/productos/",$productos[0][4]).'" width="100%">
                       </a>
                       <div class="datosProductoCart3">
					   	   <div class="tituloProductoResumen">
							   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'">'.$productos[0][1].'</a>
                               <a href="mi-carro" class="edicarCarro">Editar</a>
						   </div>
						   
						   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="valorFilaHeader">';
                                
                            if ($is_cyber AND is_cyber_product($producto[0][0])){
                                    $precios = get_cyber_price($prd_id);
                                    
                                    
                           $output[] .='<span class="valorCarroAhora">C/U: $'.number_format($precios['precio_cyber'],0,",",".").'</span>
                           <span class="valorCarroAntes"> $'.number_format($precios['precio'],0,",",".").'</span>';
                                    } else {
                                        if(tieneDescuento($prd_id)){ 
                                            
                               $output[] .='<span class="valorCarroAhora">C/U: $'.number_format(getPrecio($prd_id),0,",",".").'</span> <span class="valorCarroAntes">  $'.number_format(getPrecioNormal($prd_id),0,",",".").'</span>';
                                        }else{
                                            $output[] .='
                                            <span class="valorCarroAhora">C/U: $'.number_format(getPrecio($prd_id),0,",",".").'</span> <span class="antes">&nbsp;&nbsp;&nbsp;&nbsp;</span>';

                                         } 
                                    }
              $output[] .= '
              				</a>
							  <div class="contCantidadResumen">
									<p>Cantidad</p>							  
								<div class="box_spinnerCarro" id="spinnerCarro_'.$prd_id.'">
									<div class="sep_spin spin_action" data-action="quitar" data-where="carroResumen" onclick="quitarElementoCarroPopUp('.$prd_id.', 1,1)" rel="'.$prd_id.'">-</div>
									<div id="" class="sep_spin qty_spin">'.$qty.'</div>
									<div class="sep_spin spin_action" data-action="agregar" data-where="carroResumen" onclick="agregarElementoCarroPopUp('.$prd_id.', 1,1)"  rel="'.$prd_id.'">+</div>
								</div>
							</div>
						</div>
					   
                    </div><!--filaResumen -->
					
					
			';
			$total += round($precio_final);
			$i++;
		}
        $output[] .= '</div>';
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
        
            if($retiro == 1){
                $despacho = 0;
            } else {
                $despacho = valorDespacho($comuna_id,$retiro,$idSucursal);
            }
			
		$output[] = '
					<div class="valores">
                        <div class="filaValor">';

		$output[] .='<div class="cont100 contCodigoDescuento">
		<label for="codigo_descuento">Código de descuento</label>';
		    if($_SESSION["descuento"]){
		            $codigo = $_SESSION["descuento"];
		            descuentoBy($codigo);
		            $estadoInput = 'disabled="disabled"';
		            $icono = '<i class="fas fa-check"></i>';
		    } else {
		        $estadoInput = '';
		    }
		$output[] .='      <div class="codigoOK"></div>
		    <input type="text" name="codigo_descuento" class="descuento campoPasoFinal" value="'.$codigo.'" placeholder="Ingresa el código"'.$estadoInput.'>
		    <button type="button" class="aplicarDescuento">APLICAR</button>
		</div>';
       
    	$output[] .='
    		<div class="montoValor">$'.number_format($total_neto,0,",",".").'</div>
							<div class="nombreValor">Sub total:</div>
						</div>
						
    					<div class="filaValor">
							<div class="montoValor">$'.number_format($iva,0,",",".").'</div>
							<div class="nombreValor">IVA:</div>
                        </div>';
                        
          if($retiro == 1 || $despacho > 0){
              $output[] .= '<div class="filaValor">
                                <div id="montoEnvios" class="montoValor">$'.number_format($despacho,0,",",".").'</div>
                                <div class="nombreValor">Envío:</div>
                            </div>';
          } else {
              $output[] .= '<div class="filaValor">
                                <div id="montoEnvios" class="montoValor">Por pagar</div>
                                <div class="nombreValor">Envío:</div>
                            </div>';
          }
          
    				$tiene_descuento = (isset($_SESSION['descuento'])) ? descuentoBy($_SESSION['descuento']) : 0;
					if($tiene_descuento == 1){
							
							$output[] = '
								<div class="filaValor">
									<div class="montoValor">$-'.number_format($_SESSION['val_descuento'],0,",",".").'</div>
									<div class="nombreValor">Descuento:</div> 
								</div>
										 ';
					}
					
            $output[] = '	
                        <div class="filaValor filaTotales">
                            <div class="montoTotal">$'.number_format(($total_neto+$iva+$despacho)-$_SESSION['val_descuento'],0,",",".").'</div>
                            <div class="nombreTotal">Total:</div>
                        </div>
						
               </div>';
        if ($retiro == 0) {
        	/*$output[] = '<div class="mensajesDespacho">Su tiempo estimado de envio es de 5 días hábiles.</div>';*/
        }
					
					
		
	} else {
		$output[] = '<div class="carroVacio">
                        <div class="iconoCarroVacio"><img src="img/iconoCarro.png" /></div>
						<h2 class="tituloCarroVacio">Tu carro esta vacío</h2>
                        <a class="btnCarroVacio" href="home">Comenzar a comprar</a>
					</div><br />';
	}
	return join('',$output);
	
}





function resumenCompraShortTotales($comuna_id, $retiro=0, $idSucursal=0) {
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		//$output[] .= '<div class="conFilasCarroEnvioYPago">	';

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs, p.costo_instalacion","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			
            
            $valorUnitario 	= $productos[0][2];
            $valorUnitarioConDescuento 	= $productos[0][3];
            
            if($valorUnitarioConDescuento > 0 AND $valorUnitarioConDescuento < $valorUnitario){
                $precio_final 	= $valorUnitarioConDescuento * $qty;
            } else {
                $precio_final 	= $valorUnitario * $qty;
            }
            
          
			$precio_final = getPrecio($prd_id) * $qty;

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitarioConDescuento 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitarioConDescuento * $qty;
					}
				}
			}

            $total += round($precio_final);
			$i++;
		}
        
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
			
		    
			if($retiro == 1){
               $despacho = 0;
            } else {
               $despacho = valorDespacho($comuna_id,$retiro,$idSucursal); 
            }		
					
            $totales2 = $total + $despacho + totalInstalacionCarro();
            
        		
					
		
	} else {
		
	}
	return $totales2;
	
}




//resumen de productos en paso identificacion y envio
function resumenCompraShort2($comuna_id, $retiro) {
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
			

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			
			/*
$valoresFinales = precioFinalConDescuentoGeneral($prd_id);
	
			if($valoresFinales[descuento] > 0){
				$precio_final = $valoresFinales[valorConDescuento]*$qty;
			} else {
				$precio_final = $valoresFinales[valorOriginal]*$qty;
			}
*/
			
			$precio_final = getPrecio($prd_id) *$qty;

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitario * $qty;
					}
				}
			}
			
			
		$output[] .= '<div class="filaResumen">
                       <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="imgThumbsCartHeader">
                              <img src="imagenes/productos/'.$productos[0][4].'" width="100%" />
                       </a>
					   <div class="datosProductoCart3">
						   <div class="tituloProductoResumen">
							   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'">'.$productos[0][1].'</a>
						   </div>
						   <div class="cantResumen">'.$qty.'</div>
						   <a href="ficha/'.$productos[0][0].'/'.url_amigables($productos[0][1]).'" class="valorFilaHeader text-right">$'.number_format($precio_final,0,",",".").'</a>
					   </div>
                    </div><!--filaResumen -->
					
					
			';
			$total += round($precio_final);
			$i++;
		}
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
		
        if($retiro == 1){
            $despacho = 0;
        } else {
            $despacho = valorDespacho($comuna_id);
        }	
        
		
		$output[] = '
					<div class="valores">
						<div class="filaValor">
							<div class="montoValor">$'.number_format($total_neto,0,",",".").'</div>
							<div class="nombreValor">Sub total:</div>
						</div>
						<div class="filaValor">
							<div class="montoValor">$'.number_format($iva,0,",",".").'</div>
							<div class="nombreValor">IVA:</div>
						</div>';
                        
            if($retiro == 0 and $despacho > 0){
                $output[] = '<div class="filaValor">
                                <div class="montoValor">$'.number_format($despacho,0,",",".").'</div>
                                <div class="nombreValor">Envío:</div>
                            </div>';
            } else {
                $output[] = '<div class="filaValor">
                                <div class="montoValor">Por pagar</div>
                                <div class="nombreValor">Envío:</div>
                            </div>';
            }
           
    				
					if(isset($_SESSION["descuento"])){
							$codigo = $_SESSION["descuento"];
							$descuento = consulta_bd("id, codigo, valor, porcentaje","codigo_descuento","codigo = '$codigo' and activo = 1","");
							if($descuento[0][3] > 0){
								$nPorcentaje = $descuento[0][3]/100;
								$descuentoProducto = round($nPorcentaje*$total);
							} else if($descuento[0][2] > 0){
								$descuentoProducto = $descuento[0][2];
							} else {
								$descuentoProducto = 0;
							}
							
							$output[] = '
								<div class="filaValor">
									<div class="montoValor">$-'.number_format($descuentoProducto,0,",",".").'</div>
									<div class="nombreValor">Descuento:</div> 
								</div>';
					}
					
		$output[] = '<div class="filaValor">
						<div class="montoValor">$'.number_format(($total_neto+$iva+$despacho)-$descuentoProducto,0,",",".").'</div>
						<div class="nombreValor">Total:</div>
                        
                    </div>
				</div>';
					
					
		
	} else {
		$output[] = '<div class="carroVacio">
                        <div class="iconoCarroVacio"><img src="img/iconoCarro.png" /></div>
						<h2 class="tituloCarroVacio">Tu carro esta vacío</h2>
                        <a class="btnCarroVacio" href="home">Comenzar a comprar</a>
					</div>';
	}
	return join('',$output);
	
}



function resumenTotales($comuna_id, $retiro,$idSucursal=0) {
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
			

		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("p.id, p.nombre, pd.precio, pd.descuento, p.thumbs","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			
			/*
            $valoresFinales = precioFinalConDescuentoGeneral($prd_id);
                        if($valoresFinales[descuento] > 0){
                            $precio_final = $valoresFinales[valorConDescuento]*$qty;
                        } else {
                            $precio_final = $valoresFinales[valorOriginal]*$qty;
                        }
*/



			$precio_final = getPrecio($prd_id) * $qty;

			if(!tieneDescuento($prd_id)){
				$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
				if($pd[0][0]){
					$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
					if($precios_cantidad){
						$pc = $precios_cantidad[0];
						$rango 			= $pc[1];
						$descuento 		= $pc[2];
						$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
						$precio_final 	= $valorUnitario * $qty;
					}
				}
			}


			
		
			$total += round($precio_final);
			$i++;
		}
			$total_neto = $total/1.19;
			$iva = $total_neto * 0.19;
        
            if($retiro == 1){
                $despacho = 0;
            } else {
                $despacho = valorDespacho($comuna_id,$retiro,$idSucursal);//3000;//$address[0][5];
            }
			
		$output[] = '
					<div class="valores">
                        <div class="filaValor">
							<div class="montoValor">$'.number_format($total_neto,0,",",".").'</div>
							<div class="nombreValor">Sub total:</div>
						</div>
						
    					<div class="filaValor">
							<div class="montoValor">$'.number_format($iva,0,",",".").'</div>
							<div class="nombreValor">IVA:</div>
                        </div>';
                        
           
        if($retiro == 1 || $despacho > 0){
            $output[] = '<div class="filaValor">
							<div class="montoValor">$'.number_format($despacho,0,",",".").'</div>
							<div class="nombreValor">Envío:</div>
                        </div>';
        } else {
            $output[] = '<div class="filaValor">
							<div class="montoValor">Por pagar</div>
							<div class="nombreValor">Envío:</div>
                        </div>';
        }
           
        
        
    				$tiene_descuento = (isset($_SESSION['descuento'])) ? descuentoBy($_SESSION['descuento']) : 0;
					if($tiene_descuento == 1){
							
							$output[] = '
								<div class="filaValor">
									<div class="montoValor">$-'.number_format($_SESSION['val_descuento'],0,",",".").'</div>
									<div class="nombreValor">Descuento:</div> 
								</div>
										 ';
					}
					
            $output[] = '	
                        <div class="filaValor filaTotales">
                            <div class="montoTotal">$'.number_format(($total_neto+$iva+$despacho)-$_SESSION['val_descuento'],0,",",".").'</div>
                            <div class="nombreTotal">TOTAL:</div>
                        </div>
						
               </div>';
        if ($retiro == 0) {
        	/*$output[] = '<div class="mensajesDespacho">Su tiempo estimado de envio es de 5 días hábiles.</div>';*/
        }
					
					
		
	} else {
		$output[] = '<div class="carroVacio">
                        <div class="iconoCarroVacio"><img src="img/iconoCarro.png" /></div>
						<h2 class="tituloCarroVacio">Tu carro esta vacío</h2>
                        <a class="btnCarroVacio" href="home">Comenzar a comprar</a>
					</div>';
	}
	return join('',$output);
	
}




//valor despacho, REGLAS POR DEFINIR SEGUN CADA CASO


function valorDespacho($comuna_id, $tipoRetiro=0, $idSucursal=0){
	global $db;
	if (!isset($_COOKIE[cart_alfa_cm])) {
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
	}
	$cart = $_COOKIE[cart_alfa_cm];

	if ($cart) {
		$items = explode(',', $cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}

		$valor_despacho = 0;

		/*foreach ($contents as $prd_id => $qty) {
			$productos = consulta_bd("peso, ancho, alto, largo", "productos_detalles", "id = $prd_id", "");

			$ancho = $productos[0][1];
			$alto = $productos[0][2];
			$largo = $productos[0][3];

			$volumen += round(((($ancho * $largo) * $alto) / 4000)) * $qty;

			$peso += $productos[0][0] * $qty;

			if ($volumen < 100 and $peso < 100) {
				$peso = ($peso > $volumen) ? $peso : $volumen;
			} elseif ($volumen > 100 and $peso < 100) {
				$peso = $peso;
			} elseif ($volumen < 100 and $peso > 100) {
				$peso = $peso;
			} elseif ($volumen > 100 and $peso > 100) {
				$peso = $volumen;
			}

			if ($comuna_id > 0) {
				
					$valores_despachos = consulta_bd('a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t', 'despachos_chx', "comuna_id = $comuna_id", '');

					if ($peso <= 1) {
						$envio = $valores_despachos[0][0];
					} elseif ($peso > 1 and $peso <= 1.5) {
						$envio = $valores_despachos[0][1];
					} elseif ($peso > 1.5 and $peso <= 2) {
						$envio = $valores_despachos[0][2];
					} elseif ($peso > 2 and $peso <= 3) {
						$envio = $valores_despachos[0][3];
					} elseif ($peso > 3 and $peso <= 4) {
						$envio = $valores_despachos[0][4];
					} elseif ($peso > 4 and $peso <= 5) {
						$envio = $valores_despachos[0][5];
					} elseif ($peso > 5 and $peso <= 6) {
						$envio = $valores_despachos[0][6];
					} elseif ($peso > 6 and $peso <= 7) {
						$envio = $valores_despachos[0][7];
					} elseif ($peso > 7 and $peso <= 8) {
						$envio = $valores_despachos[0][8];
					} elseif ($peso > 8 and $peso <= 9) {
						$envio = $valores_despachos[0][9];
					} elseif ($peso > 9 and $peso <= 10) {
						$envio = $valores_despachos[0][10];
					} elseif ($peso > 10 and $peso <= 20) {
						$envio = $valores_despachos[0][11];
					}
				
			} else {
				$envio = 0;
			}

			if (is_numeric($envio)) {
				if ($valor_despacho > $envio) {
					$despacho_final = $valor_despacho;
				} else {
					$despacho_final = $envio;
				}
			} else {
				return "x";
			}
		}*/
		if ($tipoRetiro == 0) {
			$VC = consulta_bd("valor_despacho","comunas","id = $comuna_id","");
        	return $VC[0][0]; 
		}
		if ($tipoRetiro == 2) {
			$VC = consulta_bd("valor_instalacion_domicilio","comunas","id = $comuna_id","");
        	return $VC[0][0]; 
		}
		if ($tipoRetiro == 3) {
			$VC = consulta_bd("valor_instalacion","sucursales","id = $idSucursal","");
        	return $VC[0][0];
		}
        
        
	} else {
       return 0; 
    }

	
}



function showCartExito($oc){
	$pedido = consulta_bd("id, oc, total, valor_despacho, total_pagado, descuento","pedidos","oc = '$oc'","");
	$productos_pedidos = consulta_bd("pp.cantidad, pp.precio_unitario, pp.precio_total, p.nombre, p.thumbs, p.id, pd.sku","productos_pedidos pp, productos p, productos_detalles pd","pp.productos_detalle_id = pd.id and pd.producto_id=p.id and pp.pedido_id = ".$pedido[0][0],"pp.id");
	
	$carro_exito = '<div class="contCarro">
						<h2>Productos Asociados</h2>
						<div class="cont100 filaTitulos">
							<div class="ancho50"><span style="float:left; margin-left:10px;">Producto</span></div>
							<div class="ancho20">Precio unitario</div>
							<div class="ancho10">Cantidad</div>
							<div class="ancho20">Total Item</div>
						</div>';
		for($i=0; $i<sizeof($productos_pedidos); $i++) {
			$carro_exito .= '<div class="filaProductos">
								<div class="imgFila ancho10">
									<a href="ficha/'.$productos_pedidos[$i][5].'/'.url_amigables($productos_pedidos[$i][3]).'">
										<img src="imagenes/productos/'.$productos_pedidos[$i][4].'" width="100%">
									</a>
								</div>
								<div class="contInfoShowCart">
									<div class="nombreFila">
										<a href="ficha/'.$productos_pedidos[$i][5].'/'.url_amigables($productos_pedidos[$i][3]).'">
											<span>'.$productos_pedidos[$i][3].'</span>
										</a>
									</div>
									<div class="skuShowCart">SKU: '.$productos_pedidos[$i][6].'</div>
								</div>
								
								<div class="precioFila ancho20">
									<span>$'.number_format($productos_pedidos[$i][1],0,",",".").'</span>
									<span class="unidadMovil">c/u</span>
								</div>
								<div class="cantFila ancho10">
									<span>'.$productos_pedidos[$i][0].'</span>
									<span class="unidadMovil">Unidades</span>
								</div>
								<div class="totalFila ancho20">
									<span>$'.number_format($productos_pedidos[$i][2],0,",",".").'</span>
								</div>
							</div>';
		}
		$carro_exito .= '</div>';
	//Fin ciclo
	$carro_exito .= '<div class="contTotalesExito">
						<div class="cont100 filaValoresExito">
							<span class="valorValor">$'.number_format($pedido[0][2],0,",",".").'</span>
							<span class="nomValor">Subtotal</span>
						</div>
						<div class="cont100 filaValoresExito">
							<span class="valorValor">$'.number_format($pedido[0][3],0,",",".").'</span>
							<span class="nomValor">Envío</span>
						</div>
                        <div class="cont100 filaValoresExito">
							<span class="valorValor">$'.number_format($pedido[0][5],0,",",".").'</span>
							<span class="nomValor">Descuento</span>
						</div>
						<div class="cont100 filaValoresExito filaTotal">
							<span class="valorValor">$'.number_format($pedido[0][4],0,",",".").'</span>
							<span class="nomValor">Total</span>
						</div>
					</div>';
	return $carro_exito;
}


//FUNCION PARA SABER TIPO DE PAGO DE WEB PAY Y NUMERO DE CUOTAS
function tipo_pago($tipo_pago,$num_cuotas,$method){
	if ($method == 'tbk') {
		switch ($tipo_pago){
	        case 'VN':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Sin cuotas";
	            $cuota = "00";
	            break;

	        case 'VC':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Cuotas Normales";
	            $cuota_valores = strlen($num_cuotas);
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas;
	            }else{
	                $cuota = $num_cuotas;
	            }                
	            break;
	        case 'NC':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Sin interés";
	            $cuota_valores = strlen($num_cuotas);
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas;
	            }else{
	                $cuota = $num_cuotas;
	            }
	            break;

	        case 'SI':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Sin interés";
	            $cuota_valores = strlen($num_cuotas);
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas;
	            }else{
	                $cuota = $num_cuotas;
	            }
	            break;

	        case 'CI':
	            $tipo_pago = "Crédito";
	            $tipo_cuota = "Cuotas Comercio";
	            $cuota_valores = strlen($num_cuotas);
	            $cuota_valores = $num_cuotas ." cuotas";
	            
	            if($cuota_valores==1){
	                $cuota="0".$num_cuotas ." cuotas";
	            }else{
	                $cuota = $num_cuotas ." cuotas";
	            }
	            break;

	        case 'VD':
	            $tipo_pago = "Débito";
	            $tipo_cuota = "Venta Débito";
	            $cuota = "00";
	            break;
	    }
	}else{
		switch ($tipo_pago) {
			case 'credit_card':
				$tipo_pago = "Crédito";
				$tipo_cuota = ($num_cuotas > 6) ? 'Cuotas normales' : 'Sin interés';
				$cuota_valores = strlen($num_cuotas);
				if ($cuota_valores == 1) {
					$cuota = "0".$num_cuotas;
				}else{
					$cuota = $num_cuotas;
				}
				break;
			
			case 'debit_card':
				$tipo_pago = "Débito";
				$tipo_cuota = 'Venta Débito';
				$cuota = "00";
			break;
		}
	}
    

    return array("tipo_pago" => $tipo_pago, "tipo_cuota" => $tipo_cuota, "cuota" => $cuota);
}

function cambioDePrecio(){
	$cambioPrecios = json_decode($_COOKIE[listaDeseos], true); 
	$cantidad = 0;
	$cadena = "";
	/*
return var_dump($cambioPrecios);
	die();
*/
	//if(!$cambioPrecios){
		for ($i=0; $i<sizeof($cambioPrecios); $i++){
			$id = $cambioPrecios[$i]['id'];
			$valor = $cambioPrecios[$i]['valor'];
				
			$precios = consulta_bd("pd.precio, pd.descuento, p.nombre","productos_detalles pd, productos p","p.id = pd.producto_id and pd.id = $id","");
			if($precios[0][1] > 0 and $precios[0][1] < $precios[0][0]){
				$precioAplicable = $precios[0][1];
				} else {
					$precioAplicable = $precios[0][0];
				}
			//imprimo valores que cambian
			if($valor > $precioAplicable){
				//bajo de precio
				$cantidad = $cantidad + 1;
				$cadena .='<div class="filaProductoPrecio">'.$precios[0][2].' ha bajado su precio de: <strong class="rojo">$'.number_format($valor,0,",",".").' a $'.number_format($precioAplicable,0,",",".").'</strong></div>';
				$cambioPrecios[$i]['valor'] = "$precioAplicable";
				} else if($valor < $precioAplicable){
					//subio de precio
					$cantidad = $cantidad + 1;
					$cadena .='<div class="filaProductoPrecio">'.$precios[0][2].' ha subido su precio de: <strong class="rojo">$'.number_format($precioAplicable,0,",",".").' a $'.number_format($valor,0,",",".").'</strong></div>';
					$cambioPrecios[$i]['valor'] = "$valor";
					} else {
						//se mantiene el precio
						}
				
				//
			}//Fin for
	
	
	
	//}
	
	 $resultado = '<div class="cont100">
						<div class="cont100Centro">
							
							<div class="contCambioPrecios">
								<h2><strong>'.$cantidad.'</strong> productos han cambiado de precio</h2>
								'.$cadena.'
							</div>
						</div>
					</div>';

		setcookie("listaDeseos", json_encode($cambioPrecios), time() + (365 * 24 * 60 * 60), "/");
	
	

		if($cantidad > 0){
		//solo si un producto cambio su precio respondo
		return $resultado;
		} else {
		
		}

	
}//fin function



/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */

/*////////////////////////ENVIAR COMPROBANTE AL COMPRADOR///////////////////////////////////////////////*/   

function enviarComprobanteCliente($oc){

    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
    $color_logo = opciones("color_logo");
    $msje_despacho = 'Su pedido fue recibido y será procesado para despacho.';
    $datos_cliente = consulta_bd("nombre,email,id,direccion, medio_de_pago, retiro_en_tienda, region","pedidos","oc = '$oc'","");
    
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
      
    $id_pedido = $datos_cliente[0][2];
    
    //$id_pedidoAdminitrador = $datos_cliente[0][4];  
    $medioPago = $datos_cliente[0][4];
      
    $detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack","productos_pedidos","pedido_id=$id_pedido and codigo_pack is NULL","");
    
    $detalle_pack = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, count(codigo_pack) as cantidad_articulos","productos_pedidos","pedido_id=$id_pedido and codigo_pack <> '' group by codigo_pack","");
    
    $despacho = $datos_cliente[0][3];
    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="padding: 10px 10px 10px 0px; border-bottom: 2px solid #ccc;">';
           
    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id = ".$detalle_pedido[$i][0],"");
                        $id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs,marca_id";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $id_marca = $productos[0][3];
                        $marca = consulta_bd("nombre","marcas","id='$id_marca'","");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td style="border-bottom: 2px solid #f1f1f3;" valign="top" align="left" width="23%">
                                                <p style="float:left; margin:3px 3px 3px 0; ">
                                                    <img style="width:100px; border: solid 1px #DDDCDC; height:100px;" src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].'" width="100" height="100"/>
                                                </p>
                                                
                                            </td>';
                        $tabla_compra .= ' 
                                            <td valign="top" align="left" width="27%" style="border-bottom: 2px solid #f1f1f3;color:#797979;">
                                                <p style="float:left; width:100%; margin:3px 3px 3px 0; font-weight: bold; color: #000; font-size:14px; font-family: Nunito Sans, sans-serif;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; color:#297edd; font-weight:900; font-size:16px; margin:0 0 5px 0;">$'.number_format($detalle_pedido[$i][2],0,",",".").'</p>
											</td>
                                            <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #f1f1f3;color:#797979;">
                                            <p style="text-align: center; color:#297edd; font-size:12px; float:left; width:100%; margin:10px 0 5px 0;">Cantidad</p>
                                            <p style="width:100%; margin: 10px 0 10px 0;">
                                                <div style="width:30%; float:left; text-align: center; line-height: 30px;">-</div>
                                                <div style="width:40%; height: 30px; max-width:30px; float:left; border: 1px solid #d7d7d7; border-radius: 50%; font-size: 12px; background-color: #1f80d6; color: #fff; line-height: 30px; text-align: center;">'.$detalle_pedido[$i][1].'</div>
                                                <div style="width:30%; float:left; text-align: center; line-height: 30px;">+</div>
                                            </p>
											</td>';
                        $tabla_compra .= '  <td valign="top" align="right" width="20%" style="border-bottom: 2px solid #f1f1f3;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">Subtotal</p>
                                                <p style="color:#297edd; float:left; width:100%; margin:0 0 5px 0;font-weight:900; font-size:15px; ">$'.number_format(($detalle_pedido[$i][2]*$detalle_pedido[$i][1]),0,",",".").'</p>			
											</td>'; //nombre producto
                        
                        $tabla_compra .= '</tr>';

                    }
                    
                    
                    for ($i=0; $i <sizeof($detalle_pack) ; $i++) {
                        $skuPack = $detalle_pack[$i][4];
                        
                        $cantProdPack = $detalle_pack[$i][5];
                        $pDP = consulta_bd("p.codigos, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id = pd.producto_id and pd.sku = '$skuPack'","");
                        $productosPorPack = explode(",", $pDP[0][0]);
                        $cantArreglo = count($productosPorPack);
                    
                        
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","sku='$skuPack'","");
                        $id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        
                        if($pDP[0][2] > 0){
                            $precio_unitario = $pDP[0][2];
                        } else {
                            $precio_unitario = $pDP[0][1];
                        }
                        //die("$cantProdPack");
                        $cantidad = $detalle_pack[$i][1];
                        $subtotalFila = $precio_unitario;

                        
                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
                                                <p style="float:left; width:100%; margin:10px 0 5px 0;">
                                                    <img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].' " width="90%"/>
                                                </p>
                                                
                                            </td>';
                        $tabla_compra .= '  <td  align="left" width="70%" >
                                                <p style="float:left; width:100%; margin:10px 0 5px 0;"><strong>'.$productos[0][1].'</strong></p>
                                                <p style="float:left; width:100%; margin:0 0 5px 0;">'.$skuPack.'</p>
                                                <p style="float:left; width:100%; margin:0 0 5px 0;">'.$cantidad.'</p>
                                                <p style="float:left; width:100%; margin:0 0 5px 0;">$'.number_format($subtotalFila,0,",",".").'</p>
                                            </td>'; //nombre producto
                        $tabla_compra .= '</tr>';

                    }
                    

                 

            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado, costo_instalacion","pedidos","oc = '$oc'","");
            
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Sub Total:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][4],0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

            if($totales[0][1] > 0){
                $tabla_compra .='<tr class="cart_total_price">';
                $tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
                $tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][1],0,",",".").'</span></td>';
                $tabla_compra .='</tr>';
            }
            
            
    
            if($totales[0][3] > 0){
                $tabla_compra .='     <tr class="cart_total_price">';
                $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Valor envío:</span></td>';
                $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][3],0,",",".").'</span></td>';
                $tabla_compra .='     </tr>'; 
            }
            
    
    
            

    
            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'; float:right;">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color: #297edd; font-size:16px; font-family: Nunito Sans, sans-serif; text-align:right;">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            // $tabla_compra .='<br /><br />';

    
    
    $msg2 = '
    <html>
        <head>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body>
    <!--[if mso]>
        <table style="border-top:4px solid #1b61ad;" align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="650">
    <![endif]-->

    <!--[if !mso]> <!---->
       <table border="0" style="border-top:4px solid #1b61ad; max-width: 650px; padding: 10px; margin:0 auto; border-collapse: collapse;">
    <!-- <![endif]-->
            <tr>
                <td style="background-color: #fff; padding: 0">
                <div style="color: #34495e; margin: 10px 20px 10px 20px; text-align: justify;font-family: sans-serif">
                    <a href="'.$url_sitio.'">
                        <img width="200" style="display:block; margin: 10px 20px 10px 0;" src="'.$logo.'" alt="'.$logo.'"/>
                    </a>
                </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #fff">
                    <div style="color: #34495e; margin: 20px 10px 10px 20px; text-align: justify;font-family: sans-serif">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="text-align: justify;">';
                            if($medioPago === "transferencia"){
                            //inicio medio de pago es transferencia
                            $msg2 .= '<tr>
                                            <th align="left" width="100%" style="color:#797979;"> 
                                                <p style="text-transform: uppercase;float:right;width:100%; font-size:20px;">Hola, '.$nombre_cliente.'</p>
                                                <p style="float:right;width:100%;margin: 0px;">Gracias por su compra con transferencia</p>
                                                <p style="float:right;width:100%;margin: 0 0 10px 0;">Su número de compra es:<strong style="color:#1f80d6; text-decoration: underline">'.$oc.'</strong></p>
                                                
                                                <p style="text-align: justify;">A continuación encontrarás los datos para que realices la transferencia antes de 48 horas, después de ese periodo se anulara el pedido.</p>

                                                <p style="text-align: justify;">Para asistencia sobre el pago o dudas del producto, por favor contáctenos al mail <strong style="color:#1f80d6; text-decoration: underline">ventas@zonapromo.cl</strong>, en horario de atención de tienda.</p>

                                                <p style="text-align: justify;">En el caso que el producto incluya despacho pagado o por pagar el o los productos serán enviados 24 horas después de haber realizado el pago, previa confirmación de la transacción. En el caso que la transacción se realice un día viernes, fin de semana o feriado el despacho se realizara en los primeros días hábiles siguientes.</p>

                                                <p style="text-align: justify;">
                                                Banco: Banco Santander / Banefe<br>
                                                Cuenta Corriente: 0234006318<br>
                                                Rut: 11.111.111-1<br>
                                                Nombre: Zona Promo <br>
                                                Email: <strong style="color:#1f80d6; text-decoration: underline">ventas@zonapromo.cl</strong></p>
                                            </th>
                                        </tr>';
                            //fin medio de pago es transferencia
                        } else if($medioPago == "notaVenta"){
                            //inicio medio de pago es Nota de venta
                            $msg2 .= '<tr>
                                            <th align="left" width="100%" style="color:#797979;"> 
                                                <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Hola, '.$nombre_cliente.'</p>
                                                <p style="float:right;width:100%;margin: 0px;">Gracias por su compra</p>
                                                <p style="float:right;width:100%;">Su número de compra es: <br />
                                                <strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                                
                                                <p style="float:right;width:100%;margin: 15px 0px;">
                                                    <a style="display:inline-block; color:#fff;padding:10px 20px 10px 20px; margin: 5px 10px; text-decoration:none; border-radius:5px; background-color:#2677d2;" href="'.$url_sitio.'/tienda/voucher/voucherCompra.php?oc='.$oc.'">
                                                        VER VOUCHER
                                                    </a>
                                                </p>
                                                <br><br>
                                                <p style="float:right;width:100%;margin: 0 0 10px 0;">Su compra fue realizada con nota de venta y tiene opcion de pago de XXX dias (Consultar dias con GGH).</p>
                                            </th>
                                        </tr>';
                            //fin medio de pago es Nota de venta
                        } else {
                            //inicio medio de pago es transbank o mercado pago
                            $msg2 .= '<tr>
                                        <th align="left" width="100%" style="color:#7b7c7d;"> 
                                            <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:30px;color: '.$color_logo.';"><strong>Hola, '.$nombre_cliente.'</strong></p>
                                            <p style="float:right;width:100%;margin: 0px; padding:10px 0; font-size: 20px; color:#7b7c7d;">Gracias por tu compra</p>
                                            <p style="float:right;width:100%;margin: 0px;">Su número de compra es: <strong style="color:#1f80d6; text-decoration: underline; font-size: 20px;">'.$oc.'</strong></p>
                                            
                                            <p style="float:right;width:100%;margin: 15px 0px;">
                                                <a style="display:inline-block; color:#fff;padding:15px 20px 15px 20px; margin: 5px 0 5px 0; text-decoration:none; border-radius:5px; background-color:#2677d2;" href="'.$url_sitio.'/boucher?oc='.$oc.'">
                                                    VER COMPROBANTE
                                                </a>
                                                
                                            </p>
                                        </th>
                                    </tr>';
                        //fin medio de pago es transbank o mercado pago
                        }
        $msg2 .= '        
                        </table>
                    </div>
                </td>
            </tr>';
                        
        $msg2 .= '
            <tr>
                <td style="background-color: #fff">
                    <div style="color: #34495e; margin: 20px 10px 10px 10px; text-align: justify;font-family: sans-serif">
                        <table style="border-bottom: 2px solid #ccc;font-family: Nunito Sans, sans-serif;padding:10px;font-weight:bold;" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #009be5; font-size:28px; font-family: Nunito Sans, sans-serif;">Productos comprados</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="left" style="margin:0;color:#7b7c7d; font-size:20px; font-family: Nunito Sans, sans-serif;">'.$msje_despacho.'Gracias.</p> 
                                    <p align="left" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong style="color:#7b7c7d;">Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
                        <br/>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="border-top:2px solid #1b61ad; background-color: #f5f6f7; color:#8b8b8b;">
                    <div style="color: #34495e; margin: 20px 10px 10px 10px; text-align: justify;font-family: sans-serif">
                        <p style="color:#8b8b8b;; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                                <p style="color:#8b8b8b;; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                                <p>
                                    <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                                    </a>
                                    <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                                    </a>
                                </p>

                                <p>
                                    <a style="color:#8b8b8b;; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                                    <a style="color:#8b8b8b;; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                                </p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>';

// echo $msg2;
	//return $msg2;

    //die();
    //save_in_mailchimp($oc, 'exito');
    //save_in_mailchimp($oc, 'todas_compras');
    
	
    
    $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
    $mailin->
        addTo($email_cliente, $nombre_cliente)->
        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        setSubject("$asunto")->
        setText("$msg2")->
            setHtml("$msg2");
    $res = $mailin->send();
    $res2 = json_decode($res);

    if ($res2->{'result'} == true) {
        return 1;
    } else {
        return 2;
    }
}

function enviarComprobanteAdmin($oc, $imprimir, $correoForzarNotificacion){

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
	
	
	$fontFamily = ($imprimir == 1) ? "font-family: Trebuchet MS, sans-serif;" : "font-family: Open Sans, sans-serif;";
    
    /*
$email_admin = 'ventas@moldeable.com';
	$email_admin = 'htorres@moldeable.com';
*/
	
    
    $msje_despacho = 'Su pedido sera procesado y despachado dentro de 24 horas.';
	$datos_cliente = consulta_bd("nombre,email,id,direccion","pedidos","oc = '$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
    $id_pedido = $datos_cliente[0][2];
    
    $datos_cliente = consulta_bd("nombre,
    email,
    id,
    direccion, 
    region,
    ciudad,
    comuna,
    direccion,
    telefono,
    rut,
    factura,
    direccion_factura,
    giro,
    email_factura,
    rut_factura,
    fecha_creacion,
    telefono,
    regalo,
	razon_social,
	payment_type_code,
	shares_number, 
	transaction_date, 
    comentarios_envio, 
    nombre_retiro,
    telefono_retiro, 
    rut_retiro, 
    nombre, 
    retiro_en_tienda, 
    medio_de_pago, 
    fecha, 
    cliente_id, 
    
    mp_payment_type, mp_payment_method, mp_auth_code, mp_card_number, mp_id_paid, mp_cuotas, mp_valor_cuotas, mp_transaction_date,sucursal_id","pedidos","oc = '$oc'","");
    $idSucursal = $datos_cliente[0][39];
    $sucursal = consulta_bd("nombre, direccion, horarios","sucursales","id=$idSucursal","");
    $nombre_cliente = $datos_cliente[0][0];
    
    $medioPago = $datos_cliente[0][28];
	
    $method = ($datos_cliente[0][28] == "webpay") ? "tbk" : "mpago";
	
    $tipo_pago = tipo_pago($datos_cliente[0][19], $datos_cliente[0][20], $method);
	
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, descuento","productos_pedidos","pedido_id = $id_pedido","");
    $despacho = $datos_cliente[0][3];
    
  
    $tabla_compra = '
                <table width="100%"  border="0" cellspacing="0" cellpadding="5" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc; '.$fontFamily.' color:#666; float:left;">
                    <thead>
                    <tr>
                        <th align="left" width="10%" style="border-bottom:2px solid #ccc;"></th>
                        <th align="left" width="40%" style="border-bottom:2px solid #ccc;">Producto</th>
                        <th align="center" width="10%" style="border-bottom:2px solid #ccc;">SKU</th>
                        
                        <th align="center" width="5%" style="border-bottom:2px solid #ccc;">qty</th>
                        <th align="right" width="12%" style="border-bottom:2px solid #ccc;">Precio <br>Unitario</th>
                        <th align="right" width="12%" style="border-bottom:2px solid #ccc;">Total item</th>
                    </tr>
                    </thead>
                <tbody>';
           
                    
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("producto_id, nombre","productos_detalles","id = ".$detalle_pedido[$i][0],"");
                        $id_prod = $pD[0][0];
                        $campos = "id, nombre,thumbs";
                        $tabla  = "productos";
                        $where  = "id = $id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;
                        
                        if($detalle_pedido[$i][4] != ""){
                            $tabla_compra .= '<tr bgcolor="#efefef">';
                            } else {
                            $tabla_compra .= '<tr>';
                            }
                        
                        
                        $tabla_compra .= '  <td style="border-bottom: 2px solid #ccc;">
                                                <img src="'.$url_sitio.'/imagenes/productos/'.$productos[0][2].'" width="100%"/>
                                            </td>';
                        $tabla_compra .= '  <td style="border-bottom: 2px solid #ccc;color:#797979; '.$fontFamily.'">'.$productos[0][1].'</td>'; //nombre producto
                        $tabla_compra .= '  <td align="center" style="border-bottom: 2px solid #ccc;color:#797979;'.$fontFamily.'">'.$detalle_pedido[$i][3].'</td>'; //codigo producto SKU
                        
                        
                        $tabla_compra .= '  <td align="center" style="border-bottom: 2px solid #ccc;color:#797979; '.$fontFamily.'">'.$detalle_pedido[$i][1].'</td>'; //cantidad
                        
                        if($detalle_pedido[$i][5] > 0){
                            $tabla_compra .= '  <td align="right" style="font-weight:bold; border-bottom: 2px solid #ccc; color:#3366ff; '.$fontFamily.'">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; //precio producto
                            $tabla_compra .= '  <td align="right" style="font-weight:bold; border-bottom: 2px solid #ccc; color:#e10613; '.$fontFamily.'">$'.number_format(($detalle_pedido[$i][2]*$detalle_pedido[$i][1]),0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';
                            } else {
                            $tabla_compra .= '  <td align="right" style="font-weight:bold; border-bottom: 2px solid #ccc; color:#3366ff; '.$fontFamily.'">$'.number_format($detalle_pedido[$i][2],0,",",".").'</td>'; 
                            $tabla_compra .= '  <td align="right" style="font-weight:bold; border-bottom: 2px solid #ccc; color:#e10613; '.$fontFamily.'">$'.number_format(($detalle_pedido[$i][2]*$detalle_pedido[$i][1]),0,",",".").'</td>'; //precio producto
                        $tabla_compra .= '</tr>';
                            }
                        

                    }

                 
            $tabla_compra .= '</tbody>';
            $tabla_compra .= '</table>';
            
$totales = consulta_bd("id, descuento, fecha_creacion, valor_despacho, total, total_pagado, costo_instalacion","pedidos","oc = '$oc'","");
            
            $tabla_compra .= '<table width="100%" style="border-bottom: 2px solid #ccc;font-family: Trebuchet MS, sans-serif;padding:10px;font-weight:bold;">';
            $tabla_compra .='   <tfoot>';

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">Total neto:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format($totales[0][4]/1.19,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';
            
            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999; float:right;">IVA:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999; text-align:right;">$'.number_format(($totales[0][4]/1.19)*0.19,0,",",".") .'</span></td>';
            $tabla_compra .='     </tr>';

            if($totales[0][1] > 0){
                $tabla_compra .='<tr class="cart_total_price">';
                $tabla_compra .='   <td align="right" width="90%"><span style="color:#999">Descuento:</span></td>';
                $tabla_compra .='   <td align="right" width="10%"><span style="color:#999">$'.number_format($totales[0][1],0,",",".").'</span></td>';
                $tabla_compra .='</tr>';
            }
            
            

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:#999">Valor envío:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#999">$'.number_format($totales[0][3],0,",",".").'</span></td>';
            $tabla_compra .='     </tr>';
    
            

            $tabla_compra .='     <tr class="cart_total_price">';
            $tabla_compra .='       <td align="right" width="85%"><span style="color:'.$color_logo.'">Total Pedido:</span></td>';
            $tabla_compra .='       <td align="right" width="15%"><span style="color:#e10613;">$'.number_format($totales[0][5],0,",",".").'</span></td>';        
            $tabla_compra .='     </tr>';

            $tabla_compra .='   </tfoot>';
            $tabla_compra .='</table>';

            $tabla_compra .='<br /><br />';
    

    
    $asunto = "Comprobante de compra,  OC N°".$oc."";
    $msg2 = "<html>";
    if($imprimir != 1){
        $msg2 .= '
            <head>
            <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
            <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap" rel="stylesheet">
            <title>'.$nombre_sitio.'</title>
            </head>
            <body>
        <!--[if mso]>
            <table style="border-top: 4px solid #1b61ad;" align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="650">
        <![endif]-->

        <!--[if !mso]> <!---->
           <table border="0" style="border-top: 4px solid #1b61ad; max-width: 650px; padding: 10px; margin:0 auto; border-collapse: collapse;">
        <!-- <![endif]-->';

    } else {
    $msg2 .= '<body style="background:#fff; float:left;">
                <table border="0" style="width: 98%; padding: 10px; margin:0 auto; border-collapse: collapse;">';
    }
            
    $msg2 .= '
    <tr>
    <td style="background-color: #fff; padding: 0">
        <div style="color: #34495e; margin: 10px 20px 10px 20px; text-align: justify;font-family: sans-serif">
            <a href="'.$url_sitio.'">
         <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:10px 10px 10px 10px;">
            <tr>
                <th align="left" width="50%">
                    <p>
                        <a href="'.$url_sitio.'+-" style="color:#8CC63F;">
                            <img width="200" style="display:block; margin: 10px 20px 10px 20px;" src="'.$logo.'" alt="'.$logo.'"/>
                        </a>
                    </p>
                </th>
                <th align="right" width="50%"> 
                    <p style="text-transform: uppercase;float:right;width:100%;margin: 10px 20px 0 0; '.$fontFamily.'">'.$nombre_cliente.'</p>
                    <p style="color:#797979;float:right;width:100%;margin: 10px 20px 0 0;line-height:20px; '.$fontFamily.'">Ha generado una compra web.</p>
                    <p style="color:#797979; float:right; width:100%; margin: 10px 20px 0 0; '.$fontFamily.'">Su número de compra es: <strong style="color:#009be5; text-decoration:underline; '.$fontFamily.'">'.$oc.'</strong></p>';
                
                if($imprimir != 1 && $medioPago != "transferencia"){
                    $msg2 .= '<p style="float:right;width:100%;margin: 10px 20px 0 0;">
                        <a style="text-decoration: none; float:right; border-radius:5px; background-color:#2471c8; padding:15px 20px 15px 20px; color:#fff;" href="'.$url_sitio.'/tienda/voucher/voucherCompra.php?oc='.$oc.'">
                            Ver voucher
                        </a>
                    </p>';
                    }

        $msg2 .= '</th>
                </tr>
            </table>
            </div>
        </td>
    <tr>';
                        
    if($medioPago == "transferencia"){
      $msg2 .= '
    <tr>
        <td style="background-color: #F9F9F9">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">  
            <table width="100%" cellpadding="20" style="background-color: #e10613;color: #fff;">
                <tr>
                    <td>Compra realizada con transferencia electronica, corroborar que se realizo esta antes de enviar los productos.</td>
                </tr>
            </table>
            </div>
        </td>
    <tr>';

    }  else if($medioPago == "notaVenta"){
    $msg2 .= '
    <tr>
        <td style="background-color: #F9F9F9">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
            <table width="100%" cellpadding="20" style="background-color: #3366ff;color: #fff;">
                <tr>
                    <td>Compra realizada con Nota de venta, corroborar el credito disponible y los dias maximos permitidos para el pago.</td>
                </tr>
            </table>
            </div>
        </td>
    <tr>';
    }
                                
    $msg2 .= '
    <tr>
        <td style="background-color: #fff">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px; float:left;">
                    <tr>
                        <td valign="top" width="50%">';
                        if($datos_cliente[0][27] == "Instalación a Domicilio"){
                                
                        $msg2.='<h3 style="'.$fontFamily.'">'.$datos_cliente[0][27].'</h3>
                            <h5 style="'.$fontFamily.'">Dirección de instalación</h5>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][3].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Region: </strong>'.$datos_cliente[0][4].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Comuna: </strong>'.$datos_cliente[0][6].'</li>
                                </ul>';
                            }else if ($datos_cliente[0][27] == "Despacho a Domicilio") {
                        $msg2.='<h3 style="'.$fontFamily.'">'.$datos_cliente[0][27].'</h3>
                            <h5 style="'.$fontFamily.'">Dirección de envio</h5>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][3].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Region: </strong>'.$datos_cliente[0][4].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Comuna: </strong>'.$datos_cliente[0][6].'</li>
                                </ul>';
                            }else if ($datos_cliente[0][27] == "Instalación en Taller") {
                        $msg2.='<h3 style="'.$fontFamily.'">'.$datos_cliente[0][27].'</h3>
                                <h5 style="'.$fontFamily.'">Datos de intalación</h5>                                
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Sucursal: </strong>'.$sucursal[0][0].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Dirección: </strong>'.$sucursal[0][1].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Horario: </strong>'.$sucursal[0][2].'</li>
                                </ul>';
                            }else {
                        $msg2.='<h3 style="'.$fontFamily.'">Datos retiro</h3>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][23].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Telefono: </strong>'.$datos_cliente[0][24].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Sucursal: </strong>'.$sucursal[0][0].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Dirección: </strong>'.$sucursal[0][1].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Horario: </strong>'.$sucursal[0][2].'</li>
                                </ul>';
                            }
                                 
                $msg2.='</td>
                        <td valign="top" width="50%">
                            <h3 style="'.$fontFamily.'">Datos usuario</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nombre: </strong>'.$datos_cliente[0][0].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Correo: </strong>'.$datos_cliente[0][1].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Teléfono: </strong>'.$datos_cliente[0][8].'</li>
                                </ul>
                            </p>
                        </td>
                    </tr>
                </table>
                </div>
        </td>
    <tr>';
    
    $msg2.='
    <tr>
        <td style="background-color: #fff">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
            <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px; float:left;">

            <tr>';
                if($datos_cliente[0][28] == "notaVenta"){
                   $urlLink = "";
                   $cliente = consulta_bd("nombre, tipo_registro, nombre_empresa, parent, email, telefono","clientes","id = ".$datos_cliente[0][30],"");
                    $usuarioTipo = $cliente[0][1];
                    $usuarioNombreEmpresa = $cliente[0][2];
                    $usuarioParent = $cliente[0][3];
                    
                    if($usuarioParent == 0){
                        $msnv = consulta_bd("mp.mensaje","clientes c, mensajes_pagos mp","mp.id = c.mensajes_pago_id and c.id = ".$datos_cliente[0][30],"");
                    } else {
                        $msnv = consulta_bd("mp.mensaje","clientes c, mensajes_pagos mp","mp.id = c.mensajes_pago_id and c.id = ".$usuarioParent,"");
                    }
                   
                        
                    $msg2.='
                        <td valign="top" width="50%" height="100">
                            <h3 style="'.$fontFamily.'">Datos Nota de venta</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Fecha: </strong>'.$datos_cliente[0][29].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Modo pago: </strong>'.$msnv[0][0].'</li>
                                </ul>
                            </p>
                        </td>';
                } else if($datos_cliente[0][28] == "mercadopago"){
                    $urlLink = "?op=231a";
                    $msg2.='
                        <td valign="top" width="50%" height="100">
                            <h3 style="'.$fontFamily.'">Datos Mercadopago</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de pago: </strong>'.$datos_cliente[0][31].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Método de pago: </strong>'.$datos_cliente[0][32].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Código de autorización: </strong>'.$datos_cliente[0][33].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Número tarjeta: </strong>**** **** **** '.$datos_cliente[0][34].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">ID Transacción: </strong> '.$datos_cliente[0][35].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Cuotas: </strong> '.$datos_cliente[0][36].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Valor cuotas: </strong> '.$datos_cliente[0][37].'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Fecha transacción: </strong> '.$datos_cliente[0][38].'</li>
                                </ul>
                            </p>
                        </td>';
                } else if($datos_cliente[0][28] == "transferencia"){
                    $urlLink = "?op=275a";
                    $msg2.='
                        <td valign="top" width="50%" height="100">
                            <h3 style="'.$fontFamily.'">Pago por transferencia</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de pago: </strong>Transferencia</li>                                    
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nota: </strong> Verifique el pago antes de hacer el envio</li>
                                </ul>
                            </p>
                        </td>';
                } else {
                	$urlLink = "?op=231a";
                    $msg2.='
                        <td valign="top" width="50%" height="100">
                            <h3 style="'.$fontFamily.'">Datos Transbank</h3>
                            <p>
                                <ul>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Orden de compra: </strong>'.$oc.'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de pago: </strong>'.$tipo_pago[tipo_pago].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Tipo de cuota: </strong>'.$tipo_pago[tipo_cuota].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Nº Cuotas: </strong>'.$tipo_pago[cuota].'</li>
                                    <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Fecha: </strong>'.$datos_cliente[0][21].'</li>
                                </ul>
                            </p>
                        </td>';
                }
                
                   
                $msg2.='<td valign="top" width="50%">';
                        if($datos_cliente[0][14] != ''){
                    $msg2.='<h3 style="'.$fontFamily.'">Datos de facturación</h3>
                            <ul>
                                <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Razon Social: </strong>'.$datos_cliente[0][18].'</li>
                                <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Direccion: </strong>'.$datos_cliente[0][11].'</li>
                                <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Giro: </strong>'.$datos_cliente[0][12].'</li>
                                <li style="'.$fontFamily.'"><strong style="color:'.$color_logo.';">Rut: </strong>'.$datos_cliente[0][14].'</li>
                            </ul>';
                        }
            $msg2.='</td>
                </tr>
            </table>
            </div> 
        </td>
    </tr>';
                
                           
    $msg2.='
    <tr>
        <td style="background-color: #fff">
            <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="float:left; margin-top:50px;">
                            <tr>
                                <td valign="top">';
                            $msg2.='<h3 style="'.$fontFamily.' margin-bottom:20px;">PRODUCTOS COMPRADOS</h3>
                                    <p>'.$tabla_compra.'</p>';
                         
                        //Muestro los datos solo si es para enviar por correo
                        if($imprimir != 1){
                            $msg2.='<p align="center" style="margin:0;color:#000; '.$fontFamily.'">Para ver el detalle de la compra y datos del cliente puede pinchar el siguiente <a href="'.$url_sitio.'/admin/index.php'.$urlLink.'">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999; '.$fontFamily.'">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;'.$fontFamily.'">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>';
                        }
                        //Muestro los datos solo si es para enviar por correo
                       $msg2.='</td>
                            </tr>
                        </table>
                        </div> 
                    </td>
                </tr>
				<tr style="border-top: 2px solid #1b61ad;>
                <td style="background-color: #f5f6f7">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                        <p style="color:#8b8b8b; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                                <p style="color:#8b8b8b; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                                <p>
                                    <a href="https://www.facebook.com/" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                                    </a>
                                    <a href="https://www.instagram.com/" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                                    </a>
                                </p>

                                <p>
                                    <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                                    <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                                </p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>';
	
	if($imprimir == 1){
		//Muestro el html para transformarlo en un pdf
		return $msg2;	
		
	} else if($correoForzarNotificacion != ""){
		//envio la notificacion al correo que me indican en la variable
		
        $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
        $mailin->
            addTo($correoForzarNotificacion, "Notificacion Venta")->
            setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
            setSubject("$asunto")->
            setText("$msg2")->
                setHtml("$msg2");
        $res = $mailin->send();
        $res2 = json_decode($res);

        if ($res2->{'result'} == true) {
            return 1;
        } else {
            return 2;
        }
        
		//envio la notificacion al correo que me indican en la variable
		
	} else {
		//si envio las otras 2 variables vacias ejecuto la funcion con normalidad
		
        $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
        if(opciones("correo_admin1") != ""){
            $mailin-> addTo(opciones("correo_admin1"), opciones("nombre_correo_admin1"));
        }
        if(opciones("correo_admin2") != ""){
            $mailin-> addTo(opciones("correo_admin2"), opciones("nombre_correo_admin2"));
        }
        if(opciones("correo_admin3") != ""){
            $mailin-> addTo(opciones("correo_admin3"), opciones("nombre_correo_admin3"));
        }
        
        
        
        $mailin->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
            setSubject("$asunto")->
            setText("$msg2")->
                setHtml("$msg2");
        $res = $mailin->send();
        $res2 = json_decode($res);

        if ($res2->{'result'} == true) {
            return 1;
        } else {
            return 2;
        }
		//si envio las otras 2 variables vacias ejecuto la funcion con normalidad
	}		
}




/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// 
FUNCTIONES COTIZACION*/


function totalCartCotizacion($cotizacion){
	global $db;
	if(!isset($_COOKIE[cotizacion])){
		setcookie("cotizacion", "$cotizacion", time() + (365 * 24 * 60 * 60), "/");
		}
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion) {
		$items = explode(',',$cotizacion);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$total = 0;
		foreach ($contents as $prd_id=>$qty) {
			$is_cyber = (opciones('cyber') == 1) ? true : false;
			$producto = consulta_bd("pd.id, pd.precio, pd.descuento, p.id","productos_detalles pd join productos p on p.id = pd.producto_id","pd.id = $prd_id","");
			if ($is_cyber AND is_cyber_product($producto[0][3])) {
				$precios = get_cyber_price($prd_id);
				$total += $precios['precio_cyber']*$qty;
			}else{
				if($producto[0][2] > 0){
					$total += $producto[0][2]*$qty;
				} else {
					$total += $producto[0][1]*$qty;
				}
			}
		}
	}
	return round($total);
}

//cantidad de productos en el carro
function qty_proCotizacion(){
	if(!isset($_COOKIE[cotizacion])){
		setcookie("cotizacion", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion){
		$items = explode(',',$cotizacion);
		return count($items);
	}
	else
	{
		return 0;
	}
}





function ShowCartCotizacion() {
	global $db;
	if(!isset($_COOKIE[cotizacion])){
		setcookie("cotizacion", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cotizacion = $_COOKIE[cotizacion];
	if ($cotizacion) {
		$items = explode(',',$cotizacion);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
		$i = 1;
		foreach ($contents as $prd_id=>$qty) {
			$producto = consulta_bd("p.thumbs, p.nombre, pd.precio, pd.descuento, p.id, pd.sku","productos p, productos_detalles pd","p.id=pd.producto_id and pd.id = $prd_id","");
			if($producto[0][0] != ''){
				$thumbs = $producto[0][0];
			} else {
				$thumbs = "sin-imagen.jpg";
			}

			$valor = getPrecio($prd_id)*$qty;
			$valorUnitario = getPrecio($prd_id);
			
			
			   
	$output[] .='<div class="filaProductos" id="fila_carro_'.$prd_id.'">
                	<div class="imgFila ancho10">
						<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
							<img src="imagenes/productos/'.$thumbs.'" width="100%" />
						</a>
					</div>
					<div class="contInfoShowCart">
						<div class="nombreFila">
							<a href="ficha/'.$producto[0][4].'/'.url_amigables($producto[0][1]).'">
								<span>'.$producto[0][1].'</span>
							</a>
						</div>
						<div class="skuShowCart">SKU: '.$producto[0][5].'</div>
						<div class="botoneraShowCart">
							<a href="javascript:void(0)" onclick="eliminaItemCarroCotizacion('.$prd_id.')">Eliminar</a>
						</div>
						
					</div><!-- fin contInfoShowCart-->
                    
                    
					<div class="precioFila ancho20"><span>$'.number_format($valorUnitario,0,",",".").'</span></div>
                    <div class="cantFila ancho10">
                    	<div class="pull-left spinnerCarro">
                        	<input type="text" name="cant" class="campoCantCarroResumen" value="'.$qty.'" />
                            <div class="contFlechas">
                                <span class="mas" onclick="agregarElementoCarroCotizacion('.$prd_id.')"  rel="'.$prd_id.'">▲</span>
                                <span class="menos" onclick="quitarElementoCarroCotizacion('.$prd_id.')" rel="'.$prd_id.'">▼</span>
                            </div>
                    	</div>
                    </div>
                    <div class="totalFila ancho20"><span>$'.number_format($valor,0,",",".").'</span></div>
                    
                </div><!-- fin filaProductos-->';
		}
			
	} else {
		$output[] = '<div class="carroVacio">
                        <div class="iconoCarroVacio"><img src="img/iconoCarro.png" /></div>
						<h2 class="tituloCarroVacio">Tu carro esta vacío</h2>
                        <a class="btnCarroVacio" href="home">Comenzar a comprar</a>
					</div>';
	}
	return join('',$output);
	
}


/*  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */

/*////////////////////////ENVIAR COMPROBANTE AL COMPRADOR///////////////////////////////////////////////*/   

  
  function enviarComprobanteClienteCotizacion($oc){

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
	$color_logo = opciones("color_logo");

	$datos_cliente = consulta_bd("nombre, email, id, apellido","cotizaciones","oc = '$oc'","");
    $nombre_cliente = $datos_cliente[0][0]." ".$datos_cliente[0][3];
    $email_cliente = $datos_cliente[0][1];  
    $id_cotizacion = $datos_cliente[0][2];
      
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack,color,logo","productos_cotizaciones","cotizacion_id=$id_cotizacion and codigo_pack is NULL AND condicion_boton != 'sin stock'","");
	$detalle_pack = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack, count(codigo_pack) as cantidad_articulos,color,logo","productos_cotizaciones","cotizacion_id=$id_cotizacion and codigo_pack <> '' group by codigo_pack","");
    
    $asunto = "Cotización N°".$oc."";
    
	$tabla_compra = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
                        $pD = consulta_bd("id, nombre","productos_detalles","id = ".$detalle_pedido[$i][0],"");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,imagen1,color_producto_id";
                        $tabla  = "productos_detalles";
                        $where  = "id = $id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;
						$imagen = '/imagenes/productos_detalles/'.$productos[0][2].'';
						if ($productos[0][2] == "" || $productos[0][2] == null) {
							$imagen = "/img/sin-imagen.jpg";
						}

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="left" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio.$imagen.' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td valign="top" align="left" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$detalle_pedido[$i][3].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;"><strong>Cant. Color:</strong>'.$detalle_pedido[$i][6].'</p>';
												if ($detalle_pedido[$i][5] != "") {
													$tabla_compra .= '<p style="float:left; width:100%; margin:0 0 5px 0;"><strong>Color impresion:</strong>'.$detalle_pedido[$i][5].'</p>';
												}
												
						$tabla_compra .= '</td>'; //nombre producto
                        
                        $tabla_compra .= '</tr>';

                    }
					
					
					for ($i=0; $i <sizeof($detalle_pack) ; $i++) {
						$skuPack = $detalle_pack[$i][4];
						
						$cantProdPack = $detalle_pack[$i][5];
                        $pDP = consulta_bd("p.codigos, pd.precio, pd.descuento","productos_detalles pd, productos p","p.id= pd. producto_id and pd.sku = '$skuPack'","");
						$productosPorPack = explode(",", $pDP[0][0]);
						$cantArreglo = count($productosPorPack);
					
						
						$pD = consulta_bd("producto_id, nombre","productos_detalles","sku='$skuPack'","");
						$id_prod = $pD[0][0];
                        $campos = "id, nombre,imagen1";
                        $tabla  = "productos_detalles";
                        $where  = "id=$id_prod";
                        $productos = consulta_bd($campos,$tabla,$where,"");
                        
						if($pDP[0][2] > 0){
							$precio_unitario = $pDP[0][2];
						} else {
							$precio_unitario = $pDP[0][1];
						}
						//die("$cantProdPack");
                        $cantidad = $detalle_pack[$i][1];
                        $subtotalFila = $precio_unitario;

						
                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td valign="top" align="center" width="30%" style="border-bottom: 2px solid #ccc;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">
													<img src="'.$url_sitio.'/imagenes/productos_detalles/'.$productos[0][2].' " width="90%"/>
												</p>
												
											</td>';
                        $tabla_compra .= '  <td  align="center" width="70%" style="border-bottom: 2px solid #ccc;color:#797979;">
												<p style="float:left; width:100%; margin:10px 0 5px 0;">'.$productos[0][1].'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$skuPack.'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;">'.$cantidad.'</p>
												<p style="float:left; width:100%; margin:0 0 5px 0;"><strong>Cant. Color:</strong>'.$detalle_pedido[$i][7].'</p>';
												if ($detalle_pedido[$i][6] != "") {
													$tabla_compra .= '<p style="float:left; width:100%; margin:0 0 5px 0;"><strong>Color impresion:</strong>'.$detalle_pedido[$i][6].'</p>';
												}
						$tabla_compra .= '		</td>'; //nombre producto
						$tabla_compra .= '</tr>';

                    }
					

                 

            $tabla_compra .= '</table>';
            

            $tabla_compra .='<br /><br />';

    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#fff;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="100%">
									<p style="margin:20px 0 30px 0;">
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/>
										</a>
									</p>
                                </th>
                            </tr>
							<tr>
								<th align="left" width="100%" style="color:#797979;"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:20px;">Hola '.$nombre_cliente.'</p>
                                    <p style="float:right;width:100%;margin: 0px;">Gracias por cotizar</p>
                                    <p style="float:right;width:100%;margin: 0px;">Su número de cotización es: <br />
<strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                </th>
							</tr>
                        </table>
                        <br/><br/>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #333;">PRODUCTOS COTIZADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0 0 20px 0;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	// return $msg2;
	$mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
    $mailin->
        addTo($email_cliente, $nombre_cliente)->
        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        setSubject("$asunto")->
        setText("$msg2")->
            setHtml("$msg2");
    $res = $mailin->send();
    $res2 = json_decode($res);

    if ($res2->{'result'} == true) {
        return 1;
    } else {
        return 2;
    }

}


function enviarComprobanteAdminCotizacion($oc){

    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
	
	$email_admin = 'ventas@moldeable.com';
	
    
    $datos_cliente = consulta_bd("nombre,email,id,apellido","cotizaciones","oc = '$oc'","");
    $nombre_cliente = $datos_cliente[0][0]." ".$datos_cliente[0][3];
    $email_cliente = $datos_cliente[0][1];
    $id_cotizacion = $datos_cliente[0][2];
    
    $datos_cliente = consulta_bd("nombre, apellido,
    email,
    id,
    telefono,
    rut,
    fecha_creacion,
    comentarios","cotizaciones","oc = '$oc'","");
    $nombre_cliente = $datos_cliente[0][0]." ".$datos_cliente[0][1];
    $nombre = $nombre_cliente;
	$email = $datos_cliente[0][2];
	$telefono = $datos_cliente[0][4];
	$rut = $datos_cliente[0][5];
    $comentarios = $datos_cliente[0][7];
	
	$detalle_pedido = consulta_bd("productos_detalle_id,cantidad,precio_unitario,codigo, codigo_pack","productos_cotizaciones","cotizacion_id=$id_cotizacion AND condicion_boton != 'sin stock'","");

    $tabla_compra = '
                <table width="100%" style="border-bottom: 2px solid #ccc;border-top: 2px solid #ccc;font-family: Trebuchet MS, sans-serif; color:#666;">
                    <tr>
                        <th align="center" width="16%">Imagen</th>
                        <th align="center" width="16%">Producto</th>
                        <th align="center" width="16%">SKU</th>
                        <th align="center" width="16%">Cantidad</th>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
           
                    for ($i=0; $i <sizeof($detalle_pedido) ; $i++) {
						$id_prod = $detalle_pedido[$i][0]; 
                        $productos = consulta_bd("id, nombre,imagen1,producto_id","productos_detalles","id = $id_prod","");
						$imagen = "imagenes/productos_detalles/".$productos[0][2]."";
						if ($productos[0][2] == "" || $productos[0][2] == null) {
							$imagen = "img/sin-imagen.jpg";
						}
                        $precio_unitario = $detalle_pedido[$i][2];
                        $cantidad = $detalle_pedido[$i][1];
                        $subtotal = $precio_unitario * $cantidad;

                        $tabla_compra .= '<tr>';
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;">
												<img src="'.$url_sitio.'/'.$imagen.'" width="100"/>
											</td>';
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$productos[0][1].'</td>'; //nombre producto
                        $tabla_compra .= '  <td  align="center" width="16%" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][3].'</td>'; //codigo producto
								
                        $tabla_compra .= '  <td  align="center" width="16%" align="right" style="border-bottom: 2px solid #ccc;color:#797979;">'.$detalle_pedido[$i][1].'</td>'; //cantidad
                        $tabla_compra .= '</tr>';

                    }

                 

            $tabla_compra .= '</table>';
			

            $tabla_compra .='<br /><br />';

    
    $asunto = "Cotización N°".$oc."";
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#fff;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="50%">
                                	<p>
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/>
										</a>
									</p>
                                </th>
                                <th align="right" width="50%"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px;line-height:20px;">'.$nombre_cliente.'</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">ha solicitado una cotización.</p>
                                    <p style="color:#797979;float:right;width:100%;margin: 0px;line-height:20px;">Su número de cotización es: <strong style="color:'.$color_logo.';">'.$oc.'</strong></p>
                                    
                                </th>
                            </tr>
                        </table>
                        <br/><br/>
                        
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top:solid 1px #ccc; padding-top:10px;">
                            <tr>';
                        $msg2.='
                                <td valign="top" width="50%">
                                    <h3>Datos usuario</h3>
                                    <p>
                                        <ul>
                                            <li><strong style="color:'.$color_logo.';">Nombre: </strong>'.$nombre.'</li>
                                            <li><strong style="color:'.$color_logo.';">Correo: </strong>'.$email.'</li>
                                            <li><strong style="color:'.$color_logo.';">Teléfono: </strong>'.$telefono.'</li>
                                            <li><strong style="color:'.$color_logo.';">Rut: </strong>'.$rut.'</li>
                                            <li><strong style="color:'.$color_logo.';">Comenatrios Cliente: </strong>'.$comentarios.'</li>
                                            
                                        </ul>
                                    </p>
                                    
    
                                </td>
                            </tr>
                        </table>
                        <br/>';
                        
                        
                        
                        $msg2.='
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #797979;">PRODUCTOS COTIZADOS</p>

                                    <p>'.$tabla_compra.'</p>';
                                    
                            $msg2.='<p align="center" style="margin:0;color:#000;">Para ver el detalle de la cotización y datos del cliente puede pinchar el siguiente <a href="'.$url_sitio.'/admin/index.php?op=250c&id='.$id_cotizacion.'">link</a></p> 
                                   
                                    <p align="center" style="margin:0;color:#999;">Gracias,</p>
                                    <p align="center" style="margin:0; margin-bottom:10px;color:#999;">Saludos cordiales, <strong>Equipo '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>';
	// return $msg2;
    $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
	if(opciones("correo_admin1") != ""){
		$mailin-> addTo(opciones("correo_admin1"), opciones("nombre_correo_admin1"));
	}
	if(opciones("correo_admin2") != ""){
		$mailin-> addTo(opciones("correo_admin2"), opciones("nombre_correo_admin2"));
	}
	if(opciones("correo_admin3") != ""){
		$mailin-> addTo(opciones("correo_admin3"), opciones("nombre_correo_admin3"));
	}
	
	$mailin->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
		setSubject("$asunto")->
		setText("$msg2")->
			setHtml("$msg2");
	$res = $mailin->send();
	$res2 = json_decode($res);

	if ($res2->{'result'} == true) {
		return 1;
	} else {
		return 2;
	}

}

function enviarCotizacionCliente($oc){

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");
	
	$datos_cliente = consulta_bd("nombre,email,id, apellido, total, nota_especial","cotizaciones","oc = '$oc'","");
    $totalNeto = $datos_cliente[0][4];
	$iva = $datos_cliente[0][4] * 0.19;
	$totalBruto = $datos_cliente[0][4] + $iva;
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
	$nota_cliente = $datos_cliente[0][5];

	$idEjecutiva = $_COOKIE['Vendedor_id'];
    $ejecutiva = consulta_bd("nombre, apellido, perfil, datos_orden, forma_pago, datos_banco","vendedores","id = '$idEjecutiva'","");
    $datosOrden = $ejecutiva[0][3];
    $formaPago = $ejecutiva[0][4];
    $datosBanco = $ejecutiva[0][5];
      
    $id_cotizacion = $datos_cliente[0][2];
	  
    $asunto = "Cotización N°".$oc."";

    $prodCot = consulta_bd("productos_detalle_id, codigo, logo, cantidad, precio_unitario_neto, precio_final_neto", "productos_cotizaciones", "cotizacion_id= '$id_cotizacion' AND condicion_boton != 'sin stock' ", "");
    $tabla_compra = "";
	foreach ($prodCot as $value) {
        $codigo = $value[1];
		$idProductoCot = $value[0];
        $prodDetalle = consulta_bd("imagen1, nombre", "productos_detalles", "id = $idProductoCot AND sku = '$codigo'", "");
		$imagen1 = $prodDetalle[0][0];
		if ($prodDetalle[0][0] == "") {
			$imagen1 = "sin-imagen.jpg";
		}
        $tabla_compra .= '
            <tr style="height:70px;">
                <td>'.$value[1].'</td>
                <td><img width="40" class="img-tabla" src="'.$url_sitio.'/imagenes/productos_detalles/'.$imagen1.'"></td>
                <td>'.$prodDetalle[0][1].'</td>
                <td>'.$value[2].'</td>
                <td>'.$value[3].'</td>
                <td>'.$value[4].'</td>
                <td>'.$value[5].'</td>
            </tr>';
    }

    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body style="background:#fff; border-top:solid 4px '.$color_logo.';">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; '.$color_logo.'; font-family: Open Sans, sans-serif;">
            
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:10px;margin-bottom:10px;">
                            <tr>
                                <th align="left" width="100%">
									<p style="margin:20px 0 30px 0;">
										<a href="'.$url_sitio.'" style="color:#8CC63F;">
											<img src="'.$logo.'" alt="'.$logo.'" border="0" width="300"/>
										</a>
									</p>
                                </th>
                            </tr>
							<tr>
								<th align="left" width="100%" style="color:#797979;"> 
                                    <p style="text-transform: uppercase;float:right;width:100%;margin: 0px; font-size:26px; font-weight:bold;">Hola, '.$nombre_cliente.'</p>
                                    <p style="font-weight:600; font-size:22px;float:right;width:100%;margin: 0px;">Aquí esta el detalle de su cotización</p>
                                    <p style="font-weight:100;font-size:20px; float:right;width:100%;margin: 0px;">Su número de cotización es: <strong style="color:'.$color_logo.'; text-decoration: underline;">'.$oc.'</strong></p>
                                </th>
							</tr>
                        </table>
                        <br/><br/>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   ';
                                    
                            $msg2.='<p style="color: #009be5; font-size: 22px; font-weight: bold;">PRODUCTOS COTIZADOS</p>

							<div class="cont100 FilaDatosUsuario table-responsive" >
								<table border="0" id="example" class="table table-striped table-bordered tablas responsive" style="width:100%">
									<thead>
										<tr style = "text-align: left;">
											<th>SKU producto</th>
											<th>Foto producto</th>
											<th>Nombre producto</th>
											<th>Tipo de logo</th>
											<th>Cant.</th>
											<th>Precio unitario</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
									'.$tabla_compra.'
									</tbody>
								</table>    
							</div>
							<br/><br/>
							<section class="cont100 contInfoTablas">
								<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
									<tr>
									<td align="center" valign="top" id="bodyCell">
									</td>
									<td align="right" valign="top" id="bodyCell2">
										<article class="calugasInfo2" style="width: 100%; text-align:right;">
											<div align="right" class="contenedorCalugaInfo2" style="border: 2px solid #009be5; padding: 15px 20px; max-width: 340px; float: right; width: 100%;" >
												<p class="precioReferencia" style= "font-size: 14px; color: #009be5; line-height: 14px; margin: 10px 0; float: left;">Valor total Neto</p>
												<p class="precioNumero">$'.number_format($totalNeto,0,",",".").'</p>

												<p class="precioReferencia" style= "font-size: 14px; color: #009be5; line-height: 14px; margin: 10px 0; float: left;">Iva(19%)</p>
												<p class="precioNumero">$'.number_format($iva,0,",",".").'</p>
												
												<p class="precioReferencia" style= "font-size: 14px; color: #009be5; line-height: 14px; margin: 10px 0; float: left;">Valor total bruto</p>
												<p class="precioNumero">$'.number_format($totalBruto,0,",",".").'</p>
											</div>
										</article>	
									</td>
									</tr>
								</table>
								
								<article style="width: 100%; max-width:560px;" class="calugasInfo1">
									<div style="width: 100% max-width:560px;">
										<h1 style="font-size: 14px; color: #009be5;">Nota especial</h1>
										<div class="contenedorCalugaInfo1" style="border: 2px solid #009be5; padding: 20px; min-height:100px; color: #888; font-size: 12px; line-height: 14px;">
										'.nl2br($nota_cliente).'
										</div>
										<h1 style="font-size: 14px; color: #009be5;">Datos para emitir Orden de Compra</h1>
										<div class="contenedorCalugaInfo1" style="border: 2px solid #009be5; padding: 20px;min-height:100px; color: #888; font-size: 12px; line-height: 14px;min-height:100px; color: #888; font-size: 12px; line-height: 14px;">
											'.nl2br($datosOrden).'
										</div>
										<h1 style="font-size: 14px; color: #009be5;">Forma de pago</h1>
										<div class="contenedorCalugaInfo1" style="border: 2px solid #009be5; padding: 20px;min-height:100px; color: #888; font-size: 12px; line-height: 14px;">
											'.nl2br($formaPago).'
										</div>
										<h1 style="font-size: 14px; color: #009be5;">Datos de banco</h1>
										<div class="contenedorCalugaInfo1" style="border: 2px solid #009be5; padding: 20px;min-height:100px; color: #888; font-size: 12px; line-height: 14px;">
											'.nl2br($datosBanco).'
										</div>
									</div>
								</article>
							</section>
							<br/><br/>';
                                    
					$msg2.='
						</td>
					</tr>
				</table>
            </div>
			<div style="width:86%; padding-left:7%; padding-right:7%; font-family: Titillium Web, sans-serif; padding-bottom: 20px; padding-top: 20px; background-color:#555555; color:#fff;">
				<p style="color:#fff; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
				<p style="color:#fff; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
				<p>
					<a href="https://www.facebook.com" style="margin-right:30px; ">
						<img src="'.$url_sitio.'/img/iconoFaceMail.png">
					</a>
					<a href="https://www.instagram.com" style="margin-right:30px; ">
						<img src="'.$url_sitio.'/img/iconoInstagramMail.png">
					</a>
				</p>

				<p>
					<a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/preguntas-frecuentes">Preguntas Frecuentes</a>
					<a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
				</p>
			</div>
        </body>
    </html>';
	// return $msg2;
	$mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
    $mailin->
        addTo($email_cliente, $nombre_cliente)->
        setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
        setSubject("$asunto")->
        setText("$msg2")->
            setHtml("$msg2");
    $res = $mailin->send();
    $res2 = json_decode($res);

    if ($res2->{'result'} == true) {
        return 1;
    } else {
        return 2;
    }

}

/* FIN FUNCIONES COTIZACION  //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// //////////////////////////// */



function get_precio($id, $tipo) {
	$filas = consulta_bd("precio","productos", "id = $id", "");
	$precio = $filas[0][0];
	$descuento = $filas[0][1];
	if ($tipo == 'normal')
	{
		$valor = $precio;
	}
	else if ($tipo == 'oferta')
	{
		if ($descuento != 0 and $descuento > 0)
		{
			$valor = round($precio*(1-$descuento/100));
		}
		else
		{
			$valor = $precio;
		}	
	}
	return $valor;
}

function writeMiniCart() {
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return count($items).' producto'.$s;
	}
	else
	{
		return ' Mis compras';
	}
}
function writeShoppingCart(){
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart){
		// Parse the cart session variable
		$items = explode(',',$cart);
		$s = (count($items) > 1) ? 's':'';
		return '<p>Ud tiene <strong>'.count($items).' producto'.$s.'</strong> en su carro de compras:</p>';
	}
}



function get_total_price(){
	$total = 0;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	
	$items = explode(',',$cart);
	$contents = array();
	foreach ($items as $item) {
		$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
	}
	
	$i = 1;
	foreach ($contents as $prd_id=>$qty) {

		$precio_final = getPrecio($prd_id) * $qty;

		if(!tieneDescuento($prd_id)){
			$pd = consulta_bd("precio_cantidad","productos_detalles","id = $prd_id","");
			if($pd[0][0]){
				$precios_cantidad = consulta_bd("nombre, rango, descuento","precios_cantidades","rango <= $qty AND productos_detall_id = $prd_id","rango DESC");
				if($precios_cantidad){
					$pc = $precios_cantidad[0];
					$rango 			= $pc[1];
					$descuento 		= $pc[2];
					$valorUnitario 	= getPrecio($prd_id) - (getPrecio($prd_id) * ($descuento / 100));
					$precio_final 	= $valorUnitario * $qty;
				}
			}
		}



		$total += round($precio_final);
		$i++;
	}
	
	return $total;
}

function generateToken($oc, $monto) {    
	// generar token de forma aleatoria (sin ninguna relación al monto, la relación solo está en la bd)
	$token = md5(uniqid(microtime(), true));
	$insert = insert_bd('checkout_token', 'token, monto, oc', "'$token', $monto, '$oc'");
	
	if ($insert == true) {
		return $token;
	}else{
		return false;
	}
}

function save_in_mailchimp($oc, $donde){
	$list = consulta_bd('id_lista', 'mailchimp', "short_name = '$donde'", '');
	$cliente = consulta_bd('nombre, email', 'pedidos', "oc = '$oc'", "");

	$apikey = opciones('key_mailchimp');

	$n_cliente = explode(' ', $cliente[0][0]);
	$nombre = $n_cliente[0];
	$apellido = $n_cliente[1];

	$data['email'] = $cliente[0][1];
  	$data['listId'] = $list[0][0];
  	$data['nombre'] = $nombre;
  	$data['apellido'] = $apellido;

  	$mcc = new MailChimpClient($apikey);

  	$mcc->subscribe($data);

  	save_all_mailchimp($cliente[0][1], $nombre, $apellido);
}

function save_all_mailchimp($email, $nombre, $apellido){
	$list = consulta_bd('id_lista', 'mailchimp', "short_name = 'todos_mail'", '');
	$apikey = opciones('key_mailchimp');

	$data['email'] = $email;
  	$data['listId'] = $exito_list[0][0];
  	$data['nombre'] = $nombre;
  	$data['apellido'] = $apellido;

  	$mcc = new MailChimpClient($apikey);

  	$mcc->subscribe($data);
}

function descuentoBy($cod){
	global $db;

	$session_correo = $_SESSION['correo'];

	/* Consultamos si el codigo de descuento es válido */
	$fecha = date('Ymd');
	$consulta = consulta_bd('id, valor, porcentaje, codigo, descuento_opcion_id,donde_el_descuento, cliente_id', 'codigo_descuento',"codigo = '{$cod}' COLLATE utf8_bin and activo = 1 and $fecha >= fecha_desde and $fecha <= fecha_hasta and (cantidad - usados > 0)", "");

	/* Si es válido, seguimos con el proceso de cálculo */
	if (is_array($consulta)) {

		if ($consulta[0][6] == 0) {
			$whatfor = $consulta[0][4]; // A qué se le asigna el descuento (Marca[1], Categorías[2], Subcategorías[3]).
			$id = $consulta[0][5]; // Nombre de la marca, categoría, subcategoría.
			$condition;
			if ($whatfor == 1) {
				$dev = consulta_bd('id, nombre', 'marcas', "", "");
				$tbl = 'marcas';
				$condition = 'and p.marca_id = ';
			}elseif($whatfor == 2){
				$dev = consulta_bd('id, nombre', 'categorias', "", "");
				$tbl = 'categorias';
				$condition = 'and cp.categoria_id = ';
			}else if($whatfor == 3){
				$dev = consulta_bd('id, nombre', 'subcategorias', "", "");
				$tbl = 'subcategorias';
				$condition = 'and cp.subcategoria_id = ';
			}else{
				$condition = '';
			}

			$id_dev = 0;
			for ($i=0; $i < sizeof($dev); $i++) { 
				$nom_compare = url_amigables($dev[$i][1]);
				if ($id == $dev[$i][0]) {
					$id_dev = $dev[$i][0];
				}
			}

			if ($condition != ''){
				$condition .= $id_dev;
			}

			/* Si el descuento es por porcentaje o por valor */
			$porcentaje = ($consulta[0][2] > 0) ? true : false;

			$cart = $_COOKIE['cart_alfa_cm'];
			if ($cart) {
				$items = explode(',',$cart);
				$contents = array();
				foreach ($items as $item) {
					$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
				}
				$output[] = '';

				$valorProducto = 0;
				$encontrados = 0;
				$no_descuento = 0;

				/* Recorremos los productos que tiene el carro */
				foreach ($contents as $prd_id=>$qty) {
					$productos = consulta_bd("p.id, pd.precio, pd.descuento","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
					
					$valorProducto = (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;
					$valorTotal += (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;

					/* Consulta que devuelve el id del producto si es que cumple la condición  */
					$productosDescuento = consulta_bd('p.id', 'productos p join lineas_productos cp on cp.producto_id = p.id', "p.id = {$productos[0][0]} {$condition} group by p.id","");

					if (is_array($productosDescuento)) { // Si el producto pertenece a la condición del descuento
						$encontrados += $qty;
						$valProductosEncontrados += $valorProducto;

						if ($porcentaje) {
							if ($consulta[0][2] > $valProductosEncontrados) {
								$calculoFinalValor = $valProductosEncontrados;
								$resultado = false;
							}else{
								$muestraDesc = number_format($valProductosEncontrados, 0, ',', '.') . ' - ' .$consulta[0][2].'%';
								$calculoFinalValor = ($valProductosEncontrados * $consulta[0][2] / 100);
							}
							
						}else{
							if ($consulta[0][1] > $valProductosEncontrados) {
								$calculoFinalValor = $valProductosEncontrados;
								$resultado = false;
							}else{
								$muestraDesc = number_format($valProductosEncontrados, 0, ',', '.') . ' - $' .$consulta[0][1];
								$calculoFinalValor = $consulta[0][1];
							}	
						}
					}else{ 
						$no_descuento += $valorProducto;
					}
				} // End foreach

				$total = $calculoFinalValor + $no_descuento;
					
			}
			if ($encontrados > 0) {
				$resultado = true;
			}else{
				$resultado = false;
			}	
		}else{
			// Si el código existe pero tiene a un usuario asociado
			$tieneAssoc = consulta_bd('cd.cliente_id, cd.codigo, c.email', 'codigo_descuento cd join clientes c on c.id = cd.cliente_id', "c.email = ".$_SESSION['correo'], '');

			if (is_array($tieneAssoc) > 0) {

				// Recorremos los descuentos que tiene asociado el cliente
				foreach ($tieneAssoc as $codigo) {

					if ($codigo[1] == $cod) {
						/* Si el descuento es por porcentaje o por valor */
						$porcentaje = ($consulta[0][2] > 0) ? true : false;

						// Trabajamos el carro activo
						$cart = $_COOKIE['cart_alfa_cm'];
						if ($cart) {
							$items = explode(',',$cart);
							$contents = array();
							foreach ($items as $item) {
								$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
							}
							$output[] = '';

							$valorProducto = 0;
							$encontrados = 0;
							$no_descuento = 0;

							$contador_ofertas = 0;
							$cont_general = 0;

							/* Recorremos los productos que tiene el carro */
							foreach ($contents as $prd_id=>$qty) {
								$productos = consulta_bd("p.id, pd.precio, pd.descuento","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");

								if ($productos[0][2] > 0) {
									$valorTotal += 0;
									$contador_ofertas++;
								}else{
									$valorTotal += (($productos[0][2] != 0) ? $productos[0][2] : $productos[0][1]) * $qty;
								}
								$cont_general++;
								
							} // End foreach carro

							if ($porcentaje) {
								$calculoFinalValor = ($valorTotal * $consulta[0][2] / 100);
							}else{
								$calculoFinalValor = $consulta[0][1];
							} // End if porcentaje

							if ($cont_general == 1) {
								if ($contador_ofertas > 0) {
									$resultado = false;
								}else{
									$resultado = true;
								}
							}else{
								$resultado = true;
							}
							// Si encuentra el código rompo el ciclo.
							break;
						}else{
							$resultado = false;
						}
					} // End if codigo == cod

				} // End foreach tieneAssoc

			}else{ // Else tieneAssoc

				$resultado = false;

			}
		}
		
	}else{ /* Else ---> if cont > 0 */
		$resultado = false;
	}/* End if cont > 0 */

	if ($resultado == true) {
		if (validarMontoDescuento($calculoFinalValor)) {
			$_SESSION['val_descuento'] = $calculoFinalValor;
			$_SESSION['descuento'] = $cod;
			return 1;
		}else{
			unset($_SESSION['descuento']);
			unset($_SESSION['val_descuento']);
			return 0;
		}	
	}else{
		unset($_SESSION['descuento']);
		unset($_SESSION['val_descuento']);
		return 0;
	}
}

function validarMontoDescuento($valor){
	$precio_carro = (int)get_total_price();

	if ($precio_carro < (int)$valor || $precio_carro == (int)$valor) {
		return false;
	}else{
		return true;
	}
}

/* Retorna si un producto pertenece al cyberday
Parámetro => id producto madre */
function is_cyber_product($id){
	$sql = consulta_bd('p.cyber, pd.precio_cyber','productos p, productos_detalles pd', "pd.producto_id = p.id and p.id = $id", '');

	if ($sql[0][0] == 1 AND $sql[0][1] > 0) {
		return true;
	}else{
		return false;
	}
}

// Retorna true si no encuentra al usuario en la tabla primera_compra.
// True: Enviar codigo, False: No enviar codigo
function enviarCodigo($oc){
	$usuario = consulta_bd('cliente_id', 'pedidos', "oc = '$oc'", '');
	$id_usuario = $usuario[0][0];

	$tieneDescuento = consulta_bd('cliente_id', 'first_buy', "cliente_id = $id_usuario", "");
	
	if (!is_array($tieneDescuento)) {
		return true;
	}else{
		return false;
	}

	if ($cont < 1) {
		return true;
	}else{
		return false;
	}
}

function enviarCodigoDescuento($oc, $notification){

	// Consulto si ya tiene un codigo de primera compra
	$consulta_pc = consulta_bd("codigo", "first_buy", "oc = '{$oc}'" , "");
	if ($consulta_pc[0][0] == '' OR $consulta_pc[0][0] == NULL) {
		// Si no tiene un código de descuento, se le crea uno (Notificación 1)
		$codigo_desc = explode("_", $oc);
		$codigo = "COD_".$codigo_desc[1];

		$hoy = date('Y-m-d'); // Fecha desde que se envía el código (HOY)

		update_bd("first_buy", "codigo = '{$codigo}', fecha = '{$hoy}'", "oc = '{$oc}'");

		$id_cliente = consulta_bd('c.id', 'clientes c join pedidos p on c.email = p.email', "p.oc = '$oc'", "");

		// +3 meses de duración
	    $nuevaFecha = strtotime('+3 month' , strtotime($hoy));

	    $hasta = date('Y-m-d', $nuevaFecha); // Fecha hasta

	    // y le asignamos un codigo de descuento 
		insert_bd('codigo_descuento', 'cliente_id, codigo, porcentaje, activo, oc, fecha_desde, fecha_hasta, cantidad, descuento_opcion_id, donde_el_descuento', "{$id_cliente[0][0]},'$codigo', 10, 1, '$oc', '$hoy', '$hasta', 1, 4, 'Primera compra'");

	}else{
		$codigo = $consulta_pc[0][0];
	}

	$nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio="https://".$nombre_corto;
    $logo = opciones("logo_mail");
    $correo_venta = opciones("correo_venta");
	$color_logo = opciones("color_logo");

	$datos_cliente = consulta_bd("nombre,email,id,direccion, cliente_id, cliente_id","pedidos","oc='$oc'","");
    $nombre_cliente = $datos_cliente[0][0];
    $email_cliente = $datos_cliente[0][1];
    $id_cliente = $datos_cliente[0][5];
	
    

    if ($notification == 1) {
    	$asunto = "Código descuento para tu próxima compra";
    	$body_message = '<p>Muchas gracias por su preferencia. Adjuntamos un código por un 10% de descuento. Con él su próxima compra en nuestro sitio web será más conveniente. Para acceder al descuento, usted simplemente debe ingresar en nuestro sitio web <a href="'.$nombre_corto.'">'.$nombre_sitio.'</a> y seguir los pasos que el sistema propone, al final del proceso de compra y antes de pagar ingrese el código indicado.<br />
									
			<br />Recuerda que el código tiene una vigencia de 3 meses a partir de hoy.</p>
			<h2><strong>Código de descuento: '.$codigo.'</strong></h2>';
    }else{
    	$asunto = "Recuerda que tienes un descuento para tu próxima compra";
    	$body_message = '<p>Recuerda que tienes un código de descuento por un 10% en tú próxima compra. Para acceder al descuento, usted simplemente debe ingresar en nuestro sitio web <a href="https://www.mercadojardin.cl">mercadojardin.cl</a> y seguir los pasos que el sistema propone, al final del proceso de compra y antes de pagar ingrese el código indicado.<br />

			<h2><strong>Código de descuento: '.$codigo.'</strong></h2>';
    }
	
    $msg2 = '
    <html>
        <head>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
		<style type="text/css">
				p, ul, a { 
					color:#666; 
					font-family: "Open Sans", sans-serif;
					background-color: #ffffff; 
					font-weight:300;
				}
				strong{
					font-weight:600;
				}
				a {
					color:#666;
				}
			</style>
        </head>
        <body style="background:#fff;">
			<div style="background:#fff; width:86%; margin-left:5%; padding-left:2%; padding-right:2%; border-top:solid 4px '.$color_logo.'; border-bottom:solid 4px '.$color_logo.'; font-family: Open Sans, sans-serif; padding-bottom: 20px;">
            
					<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top:20px;margin-bottom:10px;">
						<tr>
							<th align="left" width="50%">
								<p>
									<a href="'.$url_sitio.'" style="color:#8CC63F;">
										<img src="'.$logo.'" alt="'.$logo.'" border="0" width="200"/>
									</a>
								</p>
							</th>
						</tr>
					</table>
					<br/><br/>
                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top">
                                   <p><strong>Estimado '.$nombre_cliente.':</strong></p>'.
									$body_message
									.'<p></p>
									<p>Muchas gracias<br /> Atte,</p>
									<p><strong>Equipo de '.$nombre_sitio.'</strong></p>
                                </td>
                            </tr>
                        </table>
            </div>
        </body>
    </html>
	';

	$mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = opciones("Host");
	$mail->Port = opciones("Port");
	$mail->SMTPSecure = opciones("SMTPSecure");
	$mail->SMTPAuth = true;
	$mail->Username = opciones("Username");
	$mail->Password = opciones("Password");
	
    $mail->setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"));
    //Set an alternative reply-to address

    $mail->addAddress($email_cliente, $nombre_cliente);
    $mail->Subject = $asunto;
    $mail->msgHTML($msg2);
    $mail->AltBody = $msg2;
    $mail->CharSet = 'UTF-8';
    //send the message, check for errors
    if (!$mail->send()) {
        return "Mailer Error: " . $mail->ErrorInfo;
    } else {
        return true;
    }
}

function breadcrumbs($id){
    $nombreProducto = consulta_bd("nombre","productos","id = $id","");
    $breadcrumbs = consulta_bd("subcategoria_id, categoria_id, linea_id","lineas_productos","producto_id = $id and subcategoria_id <> ''","");
    
    $breadcrumbs2 = consulta_bd("categoria_id, linea_id","lineas_productos","producto_id = $id and subcategoria_id IS NULL and categoria_id <> ''","");
    
    $breadcrumbs3 = consulta_bd("linea_id","lineas_productos","producto_id = $id and subcategoria_id IS NULL and categoria_id IS NULL and linea_id <> ''","");
    
    
    $cant1 = count($breadcrumbs);//producto solo tiene asignado los 3 niveles
    $cant2 = count($breadcrumbs2);//producto solo tiene asignada linea y categoria
    $cant3 = count($breadcrumbs3);//producto solo tiene asignada linea y categoria
    
    
    
    if($cant1 > 0){
        $subcategoria_id = $breadcrumbs[0][0];
        $ruta = consulta_bd("l.id, l.nombre, c.id, c.nombre, sc.id, sc.nombre","lineas l, categorias c, subcategorias sc","l.id = c.linea_id and c.id = sc.categoria_id and sc.id = $subcategoria_id","");
        
  $html = "<ul class='breadcrumb'>
              <li><a href='home'>Home</a></li>
              <li><a href='lineas/".$ruta[0][0]."/".url_amigables($ruta[0][1])."'>".$ruta[0][1]."</a></li>
              <li><a href='categorias/".$ruta[0][2]."/".url_amigables($ruta[0][3])."'>".$ruta[0][3]."</a></li>
              <li><a href='subcategorias/".$ruta[0][4]."/".url_amigables($ruta[0][5])."'>".$ruta[0][5]."</a></li>
              <li class='active'>".$nombreProducto[0][0]."</li>
            </ul>";  
        
    } else if($cant2 > 0){
        $categoria_id = $breadcrumbs2[0][0];
        $ruta = consulta_bd("l.id, l.nombre, c.id, c.nombre","lineas l, categorias c","l.id = c.linea_id and c.id = $categoria_id","");
        
  $html = "<ul class='breadcrumb'>
              <li><a href='home'>Home</a></li>
              <li><a href='lineas/".$ruta[0][0]."/".url_amigables($ruta[0][1])."'>".$ruta[0][1]."</a></li>
              <li><a href='categorias/".$ruta[0][2]."/".url_amigables($ruta[0][3])."'>".$ruta[0][3]."</a></li>
              <li class='active'>".$nombreProducto[0][0]."</li>
            </ul>";  
        
    } else if($cant3 > 0){
        $linea_id = $breadcrumbs3[0][0];
        $ruta = consulta_bd("id, nombre","lineas","id = $linea_id","");
        
  $html = "<ul class='breadcrumb'>
              <li><a href='home'>Home</a></li>
              <li><a href='lineas/".$ruta[0][0]."/".url_amigables($ruta[0][1])."'>".$ruta[0][1]."</a></li>
              <li class='active'>".$nombreProducto[0][0]."</li>
            </ul>";  
        
    }
    
    return $html;
}


function iniciales($nombre){
    $iniciales = explode(" ", $nombre);
    $letra1 = substr($iniciales[0], 0, 1);
    $letra2 = substr($iniciales[1], 0, 1);
    return $letra1;
}
function tipoCliente($id){
    $tipoUsuario = consulta_bd("c.parent, c.tipo_registro, e.lista_id", "clientes c, empresas e", "c.id = $id and c.empresa_id = e.id", "");
    if(count($tipoUsuario) > 0){
        if($tipoUsuario[0][1] == 'persona' ){
           $perfilCliente =  1; //perfil cliente normal
           $idLista = $tipoUsuario[0][2]; //id de la lista de precios 
        } else if($tipoUsuario[0][1] == 'empresa' AND $tipoUsuario[0][0] == 0){
           $perfilCliente =  2; //perfil maestro empresa
           $idLista = $tipoUsuario[0][2]; //id de la lista de precios 
        } else if($tipoUsuario[0][1] == 'empresa' AND $tipoUsuario[0][0] > 0){
           $lista = consulta_bd("e.lista_id","clientes c, empresas e","c.id = ".$tipoUsuario[0][0]." and c.empresa_id = e.id","");
           $perfilCliente =  3; //perfil esclavo empresa
           $idLista = $lista[0][0]; //id de la lista de precios 
        }
    } else {
       $perfilCliente =  false;
       $idLista = 1; //lista generica
    }
    
    return array("perfilCliente" => $perfilCliente, "idLista" => $idLista);
}

function listaCliente(){
    if(isset($_COOKIE['usuario_id'])){
        /*$usuario = tipoCliente($_COOKIE['usuario_id']);
        return $usuario['idLista'];*/
        $listaID = consulta_bd("lista_id","clientes","id=".$_COOKIE['usuario_id'],"");
        return $listaID[0][0];
    } else {
        return 1;
    } 
}

function listaOferta(){
    return 2;
}
function listaCyber(){
    return 3;
}

function porcentajeDescuento($id){
    $conexion = $GLOBALS['conexion'];
    //$productos = consulta_bd("p.id, pd.precio, pd.descuento, pd.precio_cyber", "productos p, productos_detalles pd", "p.id = $id and pd.producto_id = p.id", "p.id");
    
    $productos = consulta_bd("p.id, pd.precio, pd.descuento, pd.precio_cyber, pd.id", "productos p, productos_detalles pd", "p.id = $id and pd.producto_id = p.id", "p.id");
    $cant_productos = mysqli_affected_rows($conexion);
    
    
    if(ofertaTiempo(1)){
        /*descuento por tiempo*/
		if(ofertaTiempoDescuento($productos[0][4]) > 0){
			//$descuento = ofertaTiempoDescuento($pd);
            $descuento = round(100 - ((ofertaTiempoDescuento($productos[0][4]) * 100) / $productos[0][1]));
            $htmlDescuento = "<div class='porcentajeDescuento'>-$descuento%</div>";
            
		} else {
            /*descuento normal*/
            if(opciones('cyber') == 1 and $productos[0][3] > 0){
                $descuento = round(100 - (($productos[0][3] * 100) / $productos[0][1]));
                $htmlDescuento = "<div class='porcentajeDescuento'>-$descuento%</div>";

            } else if($productos[0][2] > 0 and $productos[0][2] < $productos[0][1]){
                $descuento = round(100 - (($productos[0][2] * 100) / $productos[0][1]));
                $htmlDescuento = "<div class='porcentajeDescuento'>-$descuento%</div>";

            } else {
                $htmlDescuento = "";
            }
        }
	} else {
        /*descuento normal*/
        if(opciones('cyber') == 1 and $productos[0][3] > 0){
            $descuento = round(100 - (($productos[0][3] * 100) / $productos[0][1]));
            $htmlDescuento = "<div class='porcentajeDescuento'>-$descuento%</div>";

        } else if($productos[0][2] > 0 and $productos[0][2] < $productos[0][1]){
            $descuento = round(100 - (($productos[0][2] * 100) / $productos[0][1]));
            $htmlDescuento = "<div class='porcentajeDescuento'>-$descuento%</div>";

        } else {
            $htmlDescuento = "";
        }
    }
    
    
    
    return $htmlDescuento;
    
}

function ahorras($id){
    $conexion = $GLOBALS['conexion'];
    $productos = consulta_bd("p.id, pd.precio, pd.descuento, pd.precio_cyber", "productos p, productos_detalles pd", "p.id = $id and pd.producto_id = p.id", "p.id");
    
    $cant_productos = mysqli_affected_rows($conexion);
    $descuento = "";
    
    if(opciones('cyber') == 1 and $productos[0][3] > 0){
        $descuento = $productos[0][1] - $productos[0][3];
        $htmlDescuento = "<div class='valorAhorro'>Ahorras $".number_format($descuento,0,",",".")."</div>";
        
    } else if($productos[0][2] > 0 and $productos[0][2] < $productos[0][1]){
        $descuento = $productos[0][1] - $productos[0][2];;
        $htmlDescuento = "<div class='valorAhorro'>Ahorras $".number_format($descuento,0,",",".")."</div>";
        
    }
    
    if($descuento > 5000){
        return $htmlDescuento;
    } 
    
}


function totalInstalacionCarro(){
	global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		} 
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$output[] = '';
			
        $i = 1;
		$costoInstalacion = 0;
		foreach ($contents as $prd_id=>$qty) {
			$productos = consulta_bd("pd.id, p.nombre, p.costo_instalacion","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $prd_id","");
			
			//reviso si tiene costo de armado
			if($productos[0][2] > 0){
				if(opcionInstalacion($productos[0][0]) == 1){
					$costoInstalacion += ($productos[0][2] * $qty);
				
					} else {}
				}
		}
			
	} else {
		$costoInstalacion = 0;
	}
	return $costoInstalacion;
}


function opcionInstalacion($id){
    global $db;
	$arm = consulta_bd("p.costo_instalacion, pd.id","productos p, productos_detalles pd","p.id = pd.producto_id and pd.id = $id","");
	$idMadre = $arm[0][1];
	
	$instalacion = $_SESSION['instalacion'];
	$add = 0;
	$items = explode(',',$instalacion);
	foreach ($items as $item) {
		if ($idMadre == $item) {
			$add = $add + 1;
			} else {
				$add = $add;
			}
	}
					
	if($add > 0){
		$instalacion = 1;
		} else {
			$armado = 0;
		}
	return $instalacion;
}
function ppack($id){
    global $db;
    $pack = consulta_bd("id","productos","id = $id and producto_pack = 1","");
    if(count($pack) > 0){
        return true;
    } else {
        return false;
    }
}

function valorM2($metrosCaja, $valorCaja){
    $valorM2 = $valorCaja / $metrosCaja;
    return $valorM2;
}

function pesoCarro(){
    global $db;
	if(!isset($_COOKIE[cart_alfa_cm])){
		setcookie("cart_alfa_cm", "$cart", time() + (365 * 24 * 60 * 60), "/");
		}
	$cart = $_COOKIE[cart_alfa_cm];
	if ($cart) {
		$items = explode(',',$cart);
		$contents = array();
		foreach ($items as $item) {
			$contents[$item] = (isset($contents[$item])) ? $contents[$item] + 1 : 1;
		}
		$i = 1;
		$peso = 0;
		foreach ($contents as $prd_id=>$qty) {
			$producto_madre = consulta_bd('peso', "productos_detalles", "id = $prd_id", '');
			
            $peso += ($producto_madre[0][0] * $qty);
		}
	}
	return $peso;
}

function nombreMarca($id){
    $marca = consulta_bd("m.nombre","productos p, marcas m","m.id = p.marca_id and p.id = $id","");
    if(count($marca) > 0){
        return $marca[0][0];
    } else {
        return ;
    }
}



function tipoClienteActivo(){
    
    if(isset($_SESSION["usuario"])){
            $id = $_SESSION["usuario"];
            $tipoUsuario = consulta_bd("c.parent, c.tipo_registro, c.lista_id", "clientes c", "c.id = $id", "");
            if(count($tipoUsuario) > 0){
                if($tipoUsuario[0][1] == 'persona' ){
                   $tipo = 1;
                   //$perfilCliente =  1; //perfil cliente normal
                   //$idLista = $tipoUsuario[0][2]; //id de la lista de precios 
                } else if($tipoUsuario[0][1] == 'empresa' AND $tipoUsuario[0][0] == 0){
                   $tipo = 2;
                   //$perfilCliente =  2; //perfil maestro empresa
                   //$idLista = $tipoUsuario[0][2]; //id de la lista de precios 
                } else if($tipoUsuario[0][1] == 'empresa' AND $tipoUsuario[0][0] > 0){
                   $lista = consulta_bd("e.lista_id","clientes c, empresas e","c.id = ".$tipoUsuario[0][0]." and c.empresa_id = e.id","");
                   $tipo = 3;
                    //$perfilCliente =  3; //perfil esclavo empresa
                   //$idLista = $lista[0][0]; //id de la lista de precios 
                }
            } else {
               $tipo = 1;
               
            }
        } else {
            //cuanta normal
            $tipo = 1;
        }
    
    
    
    return $tipo;
}

    

?>