<?php

//$cart = $_SESSION['cart_alfa_cm'];
$cart = "";
if(!isset($_COOKIE["cart_alfa_cm"])){
	setcookie("cart_alfa_cm", "$cart", time() + (365 * 24 * 60 * 60), "/");
} else {
	$cart = $_COOKIE["cart_alfa_cm"];
}
if(isset($_COOKIE["colorProd"])){
	$colorArray = json_decode(stripslashes( $_COOKIE['colorProd']) , true);
}else{
	$colorArray = array();
}
if(isset($_COOKIE["cantColorProd"])){
	$cantColorArray = json_decode(stripslashes( $_COOKIE['cantColorProd']) , true);
}else{
	$cantColorArray = array();
}
$action = (isset($_GET['action'])) ? mysqli_real_escape_string($conexion, $_GET['action']) : 0;
$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;
$cantidad = (is_numeric($_GET['qty'])) ? mysqli_real_escape_string($conexion, $_GET['qty']) : 0;
$idProd = (isset($_GET['idPr'])) ? mysqli_real_escape_string($conexion, $_GET['idPr']) : 0;
$qtyProd = (isset($_GET['qtyPr'])) ? mysqli_real_escape_string($conexion, $_GET['qtyPr']) : 0;
$action = (isset($_GET['action'])) ? mysqli_real_escape_string($conexion, $_GET['action']) : 0;
$idColor = (isset($_GET['color'])) ? mysqli_real_escape_string($conexion, $_GET['color']) : 0;
$idCantColor = (isset($_GET['cantColor'])) ? mysqli_real_escape_string($conexion, $_GET['cantColor']) : 0;
// $colorArray[2]=4;
// setcookie('colorProd', json_encode($colorArray), time() + (365 * 24 * 60 * 60), "/");
// unset($colorArray[1]);
switch ($action) {	
	case 'add':
		//Reviso el stock del producto
		$colorArray[$id]=$idColor;
		$cantColorArray[$id]=$idCantColor;
		setcookie('colorProd', json_encode( $colorArray), time() + (365 * 24 * 60 * 60), "/");
		setcookie('cantColorProd', json_encode( $cantColorArray), time() + (365 * 24 * 60 * 60), "/");
		$stock = consulta_bd("id, stock, stock_reserva","productos_detalles","id=$id","");
		// if($cantidad <= ($stock[0][1] - $stock[0][2])){
			for($i=0;$i<$cantidad;$i++){
				if ($cart) {
					$cart .= ','.$id;
				} else {
					$cart .= $id;
				}
			}
		// }else{
		// 	if ($cart) {
		// 		$cart .= ','.$id;
		// 	} else {
		// 		$cart .= $id;
		// 	}
			
		// }
		break;

		case 'addPedido':
		//Reviso el stock del producto
		$idProductos = explode(",", $idProd);
		$qty = explode(",", $qtyProd);
		$cont = 0;
		foreach($idProductos as $idPr) {
			$stock = consulta_bd("id, stock, stock_reserva","productos_detalles","id=$idPr","");
			if($qty[$cont] <= ($stock[0][1] - $stock[0][2])){
				for($i=0;$i<$qty[$cont];$i++){
					if ($cart) {
						$cart .= ','.$idPr;
					} else {
						$cart .= $idPr;
					}
				}
			}else{
				if ($cart) {
					$cart .= ','.$idPr;
				} else {
					$cart .= $idPr;
				}
				
			}
			$cont++;
		}
        //die($cart);
		break;

		case 'addCotizar':
			//Reviso el stock del producto
			$idProductos = explode(",", $idProd);
			$qty = explode(",", $qtyProd);
			$color = explode(",", $idColor);
			$logo = explode(",", $idCantColor);
			for($j=0; $j<sizeof($idProductos); $j++){
				$colorArray[$idProductos[$j]]=$color[$j];
				$cantColorArray[$idProductos[$j]]=$logo[$j];
			}
			// $cantColorArray[$id]=$idCantColor;
			setcookie('colorProd', json_encode( $colorArray), time() + (365 * 24 * 60 * 60), "/");
			setcookie('cantColorProd', json_encode( $cantColorArray), time() + (365 * 24 * 60 * 60), "/");
			$cont = 0;
			foreach($idProductos as $idPr) {
				// $stock = consulta_bd("id, stock, stock_reserva","productos_detalles","id=$idPr","");
				// if($qty[$cont] <= ($stock[0][1] - $stock[0][2])){
					for($i=0;$i<$qty[$cont];$i++){
						if ($cart) {
							$cart .= ','.$idPr;
						} else {
							$cart .= $idPr;
						}
					}
				// }else{
				// 	if ($cart) {
				// 		$cart .= ','.$idPr;
				// 	} else {
				// 		$cart .= $idPr;
				// 	}
					
				// }
				$cont++;
			}
			//die($cart);
			break;

		case 'remove':
           if ($cart) {
			$items = explode(',',$cart);
			
			$i = 0;//$cantidad-1;
			$cont = 0;
			foreach ($items as $item) {
				if ($id == $item) {
					unset($items[$i]);
					$cont = $cont+1;
					if($cont == $cantidad){
						break;
					}
				}
				$i++;
			}
			$cart = implode(",", $items);
		}
        break;

	case 'delete':
		if ($colorArray) {
			unset($colorArray[$id]);
			setcookie('colorProd', json_encode( $colorArray), time() + (365 * 24 * 60 * 60), "/");
		}
		if ($cantColorArray) {
			unset($cantColorArray[$id]);
			setcookie('cantColorProd', json_encode( $cantColorArray), time() + (365 * 24 * 60 * 60), "/");
		}
		
		if ($cart) {
			$items = explode(',',$cart);
			$newcart = '';
			foreach ($items as $item) {
				if ($id != $item) {
					if ($newcart != '') {
						$newcart .= ','.$item;
					} else {
						$newcart = $item;
					}
				}
			}
			$cart = $newcart;
		}
		break;
	case 'update':
		if ($cart) {
			foreach ($_POST as $key=>$value) {
				if (stristr($key,'qty')) {
					$id = str_replace('qty','',$key);
					//Reviso el stock del producto
					$stock = consulta_bd_por_id("nombre, stock","productos","",mysqli_real_escape_string($conexion, $id));
					
					$items = ($newcart != '') ? explode(',',$newcart) : explode(',',$cart);
					$newcart = '';
					foreach ($items as $item) {
						if ($id != $item) {
							if ($newcart != '') {
								$newcart .= ','.$item;
							} else {
								$newcart = $item;
							}
						}
					}
					for ($i=1;$i<=$value;$i++) {
						if ($i <= $stock['stock'])
						{
							if ($newcart != '') {
								$newcart .= ','.$id;
							} else {
								$newcart = $id;
							}
						}
						else
						{
							$error = "No existe stock suficiente para ".$stock['nombre'].", sólo se ha considerado el stock disponible.&tipo=error";
							break;							
						}
					}	
				}
			}
		}
		$cart = $newcart;
		break;
		case 'update2':
		if ($cart) {
			$items = explode(',',$cart);
			$newcart = '';
			$aux = '';
			foreach ($items as $item) {
				if ($id != $item) {
					if ($newcart != '') {
						$newcart .= ','.$item;
					} else {
						$newcart = $item;
					}
				}else{
					if ($aux == '') {
						if ($newcart != '') {
							$newcart .= ','.$item;
						} else {
							$newcart = $item;
						}
						$aux = 1;
					}					
				}
			}
			$cart = $newcart;
			$cantidad = $cantidad -1; 
			$stock = consulta_bd("id, stock, stock_reserva","productos_detalles","id=$id","");
			// if($cantidad <= ($stock[0][1] - $stock[0][2])){
				for($i=0;$i<$cantidad;$i++){
					if ($cart) {
						$cart .= ','.$id;
					} else {
						$cart .= $id;
					}
				}
			// }else{
			// 	if ($cart) {
			// 		$cart .= ','.$id;
			// 	} else {
			// 		$cart .= $id;
			// 	}
				
			// }
		}
		break;

}
//$_SESSION["carroCompras"] = $cart;
setcookie("cart_alfa_cm", "$cart", time() + (365 * 24 * 60 * 60), "/");

	//echo showCart();
?>