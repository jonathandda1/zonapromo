<?php 

include('../conf.php');

$listas = consulta_bd('id, nombre, id_lista, short_name', 'mailchimp', '', 'id asc');

?>
<h2 class="tituloReporte">Lista de Mailchimp</h2>

<div class="cont-form">
	<form action="action/save_mailchimp.php" method="POST">
		<p>Ingresar api key</p>
		<div class="form-mchimp">
			<?php $api_key = opciones('key_mailchimp'); ?>
			<label for="">API Key</label>
			<input type="text" name="api_key" value="<?= $api_key ?>">
		</div>
		<p>Ingresar solo los id de las listas</p>
		<?php foreach ($listas as $lista): ?>
			<div class="form-mchimp">
				<label for="<?= $lista[0] ?>"><?= $lista[1] ?></label>
				<input type="text" name="<?= $lista[3] ?>" id="<?= $lista[0] ?>" value="<?= $lista[2] ?>">
			</div>
		<?php endforeach ?>
		<input type="submit" value="Guardar">
	</form>
</div>