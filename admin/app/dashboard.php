<?php
	include('../conf.php');
	$servicio = consulta_bd("valor","opciones","nombre = 'servicio'","");
	
	$tienda = "Ejemplo moldeable";//$_GET['tienda'];
	$service = "";
	$serv1 = ""; $serv2 = ""; $serv3 = ""; $serv4 = "";

	
	$totalVentas = totalVentas($servicio[0][0]);

	$tiendaName = "Ejemplo moldeable";//$result["nombre"];
	ksort($totalVentas['total_meses']);


$mesActual = date("n"); //5;//
$anoActual = date("Y"); //2019;//

$anoAnterior = $anoActual - 1;
$mesAnteriorActual = $mesActual - 1;
?>

<!--Grafico de barras -->
<script type="text/javascript">
	$(function () {
	    var ventas = new Highcharts.Chart({
	        chart: {
	            type: 'column',
	            backgroundColor: '#FAFAFA',
	            renderTo: 'grafico_ventas'
	        },
	        title: {
	            text: 'Valor ventas por mes y año'
	        },
	        xAxis: {
	            categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ]

	        },
			colors: ['#101DC2', '#10C2AA', '#1050CC', '#1777B6', '#10B2CC'],
	        yAxis: {
	            title: {
	                text: 'Total por mes'
	            }
	        },
			plotOptions: {
				column: {
					borderRadius: 5
				}
			},

	        series: [
	        	<?php foreach ($totalVentas['total_meses'] as $item => $value): ?>
	        		{ name: <?=$item?>,
	        		data: [
	        			<?php 
				        	for($i = 0; $i < 12; $i++):
				        		if ($value[$i] != null) {
				        			echo $value[$i] . ',';
				        		}else{
				        			echo 0 .',';
				        		}
				        		
				        	endfor;
				        ?>
	        		],
			        tooltip: {
			            valuePrefix: '$ '
			        }
			    },
	        	<?php endforeach ?>
	        ]
	    });

	
	});
</script>



<!--grafico torta / Categorias mas vendidas-->
<?php 
	$categorias = consulta_bd("pd.id, c.id, c.nombre, count(c.id) as cantidad","pedidos p, productos_pedidos pp, productos_detalles pd, lineas_productos lp,  categorias c","p.id = pp.pedido_id and pp.productos_detalle_id = pd.id and pd.producto_id = lp.producto_id and lp.categoria_id = c.id and p.estado_id = 2 GROUP BY c.nombre","cantidad DESC"); 
	$datosCat = "";
	$valoresOtros = 0;
	$nuevoValor = 0;
	for($i=0; $i<sizeof($categorias); $i++) {
		if($i < 4){
			$datosCat .=  "['".$categorias[$i][2]."', ".$categorias[$i][3]."],";
		} else {
			$nuevoValor = $nuevoValor + $categorias[$i][3];
		}
	}
	$datoNormal = substr(trim($datosCat), 0, -1);
	$datosCatOtros =  ",['Otros', ".$nuevoValor."]";
?>
<script type="text/javascript">
$(function () {
	 var ventas2 = new Highcharts.Chart({
    chart: {
        type: 'pie',
		plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        options3d: {
            enabled: true,
            alpha: 45
        },
        backgroundColor: '#FAFAFA',
        renderTo: 'masVendidas',
		marginRight: 10
    },
    title: {
        text: ''
    },
	tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true,
			size: 180
        }
    },
	colors: ['#101DC2', '#10C2AA', '#1050CC', '#1777B6', '#10B2CC'],
    series: [{
        name: 'Cantidad',
		colorByPoint: true,
        data: [
            <?= $datoNormal.$datosCatOtros; ?>
        ]
    }]
});
});
</script>



<!--Grafico de lineas -->
<script type="text/javascript">
$(function(){
	Highcharts.chart('graficoLinea', {

    title: {
        text: 'Total acumulado Diario mes actual/mes año anterior'
    },
	subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Totales en ventas'
        }
    },
	xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            day: '%e de %b'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2010
        }
    },
		<?php 
		//año acual
		$valores3 = consulta_bd("fecha, SUM(total), SUM(descuento), DATE_FORMAT(fecha, '%e') as dia","pedidos","estado_id IN (2,4,6,7) and MONTH(fecha) = $mesActual AND YEAR(fecha) = $anoActual GROUP BY fecha","");
		// Número de días del mes actual, de 28 a 31
		$diasMesActual = date("t");
		
		
		$datosFecha = array();
		for($i=0; $i<$diasMesActual; $i++) {
			array_push($datosFecha, 0);
		}
		for($i=0; $i<sizeof($valores3); $i++) {
			$dia = $valores3[$i][3] - 1;
			$datosFecha[$dia] = (int)$valores3[$i][1];
		}
		
		$cadenaMesActual = "";
		foreach ($datosFecha as $clave => $valor){
			$cadenaMesActual .= "$valor, ";
		}
		$valoresMesAcual = substr(trim($cadenaMesActual), 0, -1);
				
		
		$anoAnterior = $anoActual - 1; 	
		//año anterior
		$valores4 = consulta_bd("fecha, SUM(total), SUM(descuento), DATE_FORMAT(fecha, '%e') as dia","pedidos","estado_id IN (2,4,6,7) and MONTH(fecha) = $mesActual AND YEAR(fecha) = $anoAnterior GROUP BY fecha","");
		// Número de días del mes actual, de 28 a 31
		$diasMesActual2 = date("t");
		
		
		$datosFecha2 = array();
		for($i=0; $i<$diasMesActual2; $i++) {
			array_push($datosFecha2, 0);
		}
		for($i=0; $i<sizeof($valores4); $i++) {
			$dia2 = $valores4[$i][3] - 1;
			$datosFecha2[$dia] = (int)$valores4[$i][1];
		}
		
		$cadenaMesActual2 = "";
		foreach ($datosFecha2 as $clave => $valor){
			$cadenaMesActual2 .= "$valor, ";
		}
		$valoresMesAcual2 = substr(trim($cadenaMesActual2), 0, -1);
		?>
		
		
			
		
		colors: ['#101DC2', '#10C2AA', '#1050CC', '#1777B6', '#10B2CC'],
    	series: [{
        name: '<?= $anoActual - 1; ?>',
        data: [<?= $valoresMesAcual2; ?>],
		pointStart: Date.UTC(<?= $anoActual; ?>, <?= $mesActual - 1; ?>, 1),
        pointInterval: 24 * 3600 * 1000 
    }, {
        name: '<?= $anoActual; ?>',
        data: [<?= $valoresMesAcual ?>],
		pointStart: Date.UTC(<?= $anoActual; ?>, <?= $mesActual - 1; ?>, 1),//mes corresponde al numero de mes menos 1, aca esta diciembre y es el 11
        pointInterval: 24 * 3600 * 1000 
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
});
</script>



<?php 
$valores1 = consulta_bd("total, descuento, valor_despacho, cant_productos","pedidos","estado_id IN (2,4,6,7) and MONTH(fecha) = $mesActual AND YEAR(fecha) = $anoActual","");
				
$ventasMesActual=0;
$pedidosDelMes = 0;
$cantidadProductos = 0;
				
for($i=0; $i<sizeof($valores1); $i++) {
	$ventasMesActual= $ventasMesActual + ($valores1[$i][0] - $valores1[$i][1]);
	$pedidosDelMes =  $pedidosDelMes + 1;
	$cantidadProductos = $cantidadProductos + $valores1[$i][3];
}

$valoresMesAnterior = consulta_bd("total, descuento, valor_despacho, cant_productos","pedidos","estado_id IN (2,4,6,7) and MONTH(fecha) = $mesAnteriorActual AND YEAR(fecha) = $anoActual","");
$ventasMesActualAnterior=0;
for($i=0; $i<sizeof($valoresMesAnterior); $i++) {
	$ventasMesActualAnterior = $ventasMesActualAnterior + ($valoresMesAnterior[$i][0] - $valoresMesAnterior[$i][1]);
}


$valoresAnoAnterior = consulta_bd("total, descuento, valor_despacho, cant_productos","pedidos","estado_id IN (2,4,6,7) and MONTH(fecha) = $mesActual AND YEAR(fecha) = $anoAnterior","");
$ventasMesAnoaAnterior=0;			
for($i=0; $i<sizeof($valoresAnoAnterior); $i++) {
	$ventasMesAnoaAnterior = $ventasMesAnoaAnterior + ($valoresAnoAnterior[$i][0] - $valoresAnoAnterior[$i][1]);
}

?>
<div class="filaBloques">
	<h2 class="tituloDash">Datos mes actual</h2>
	<div class="bloqueInfo" style="background-color: #101DC2">
		<h3>Ventas del Mes</h3>
		<span>$<?= number_format($ventasMesActual,0,",","."); ?></span>
		
		<?php $valorMesAnterior = round(($ventasMesActual - $ventasMesActualAnterior)/$ventasMesActualAnterior*100,2); ?>
		<div class="ma">Mes anterior <?= number_format($valorMesAnterior,1,",","."); ?>%</div>
		
		<?php $valorMesAnoAnterior = round((($ventasMesActual - $ventasMesAnoaAnterior)/$ventasMesAnoaAnterior*100),1); ?>
		<div class="maa" rel="<?= $valorMesAnterior;?>">Mes año anterior <?= number_format($valorMesAnoAnterior,1,",","."); ?>%</div>
	</div>
	
	<div class="bloqueInfo" style="background-color: #1050CC;">
		<h3>Pedidos del mes</h3>
		<span><?= $pedidosDelMes; ?></span>
	</div>
	
	<div class="bloqueInfo" style="background-color: #1777B6;" rel="<?= $ventasMesActual;?>" data-rel="<?= $pedidosDelMes;?>">
		<h3>Valor promedio carro</h3>
		<span>$<?= number_format(((int)$ventasMesActual / (int)$pedidosDelMes),0,",","."); ?></span>
	</div>
	<div class="bloqueInfo" style="background-color: #10B2CC;">
		<h3>Cantidad Productos del mes</h3>
		<span><?= $cantidadProductos; ?></span>
	</div>
	<div class="bloqueInfo" style="background-color: #10C2AA;">
		<h3>Promedio productos carro</h3>
        <?php $valorX = $cantidadProductos / $pedidosDelMes; ?>
		<span><?= round($valorX, 2); ?> </span>
	</div>
	
</div>



<?php 
$valores2 = consulta_bd("id, oc, estado_id, medio_de_pago, fecha, total, retiro_en_tienda, descuento, valor_despacho, cant_productos, estados_pedido_id","pedidos","","id desc limit 10");

$masVendidos = consulta_bd("pp.codigo, count(pp.codigo) as cantidadCodigosIguales, prd.id, prd.nombre, p.estado_id, sum(pp.cantidad) as totalVendidos, p.oc","pedidos p, productos_pedidos pp, productos_detalles pd, productos prd","prd.id = pd.producto_id and pd.id = pp.productos_detalle_id and pp.pedido_id = p.id and p.estado_id IN (2,4,6,7) GROUP BY pp.codigo","totalVendidos desc limit 10");
?>

<div class="filaDashboard">
	<div class="columna100">
		<div class="contTabsDashboard">
			<div class="filaTitulos filaMenuTabs">
				<a class="activoTab" rel="pedidosRecientes" href="javascript:void(0)">Pedidos recientes</a>
				<a href="javascript:void(0)" rel="losMasVendidos">Los mas vendidos del mes</a>
			</div>
			<div class="tabActivoDashboard tabsDashboard" id="pedidosRecientes">
				<div class="filaTitulosInterior">
					<div class="col1">Orden de compra</div>
					<div class="col2">Estado</div>
					<div class="col3">Medio pago</div>
					<div class="col4">Fecha</div>
					<div class="col5">Valor</div>
					<div class="col6">Estado pedido</div>
					<div class="col7">Entrega</div>
					<div class="col8"></div>
					<!--crear nuevo estado para el pedido, entregado, en bodega, despachado, cerrado-->
					<!--crear nuevo estado para el retiro en tienda o despacho-->
				</div>
				<?php for($i=0; $i<sizeof($valores2); $i++){ 
					$estado = $valores2[$i][2];
					if($estado == 1){
						$est = "abandonado";
						$valorEstado = "Abandonado";
					} else if($estado == 2 || $estado == 6 || $estado == 7){
						$est = "pagado";
						$valorEstado = "Pagado";
					} else if($estado == 4 || $estado == 5){
						$est = "pago_pendiente";
						$valorEstado = "Pendiente p.";
					} if($estado == 3){
						$est = "rechazado";
						$valorEstado = "Rechazado";
					}
                    $estadoPedidoId = $valores2[$i][10];
                    $estadoPedido = consulta_bd("nombre","estados_pedidos","id = $estadoPedidoId","")
				?>
				<div class="filaDatos">
					<div class="col1"><?= $valores2[$i][1]; ?></div>
					<div class="col2"><span class="<?= $est; ?>"><?= $valorEstado; ?></span></div>
					<div class="col3"><?= $valores2[$i][3]; ?></div>
					<div class="col4"><?= $valores2[$i][4]; ?></div>
					<div class="col5">$<?= number_format(($valores2[$i][5] - $valores2[$i][7]),0,",","."); ?></div>
					<div class="col6"><?= $estadoPedido[0][0]; ?></div>
					<div class="col7"><?php if($valores2[$i][6] == 1){echo "Retiro en tienda";} else { echo "Despacho";}; ?></div>
					<div class="col8"><a href="index.php?op=231c&id=<?= $valores2[$i][0]; ?>" target="_blank"><i class="far fa-eye"></a></i></div>
				</div>
				<?php } ?>
				
			</div>
			
			
			<div class="tabActivoDashboard tabsDashboard" id="losMasVendidos" style="display: none;">
				<div class="filaTitulosInterior">
					<div class="col2" style="margin-left: 20px;">id</div>
					<div class="col1" style="width: 50%;">Nombre</div>
					<div class="col3">SKU</div>
					<div class="col6">Vendidos</div>
					<div class="col8"></div>
					<!--crear nuevo estado para el pedido, entregado, en bodega, despachado, cerrado-->
					<!--crear nuevo estado para el retiro en tienda o despacho-->
				</div>
				<?php for($i=0; $i<sizeof($masVendidos); $i++){ ?>
				<div class="filaDatos">
					
					<div class="col2" style="padding-left: 20px;"><span class=""><?= $masVendidos[$i][2]; ?></span></div>
					<div class="col1" style="width: 50%;"><?= $masVendidos[$i][3]; ?></div>
					<div class="col3"><?= $masVendidos[$i][0]; ?></div>
					
					<div class="col6"><?= $masVendidos[$i][5]; ?></div>
					<div class="col8"><a href="index.php?op=219c&id=<?= $masVendidos[$i][2]; ?>" target="_blank"><i class="far fa-eye"></a></i></div>
				</div>
				<?php } ?>
				
			</div>
		</div>
		
		
		
		
	</div>
	
</div><!--fin filaDashboard-->

<div class="filaDashboard dashboard2">
	<div class="ancho100" id="graficoLinea"></div>
</div>






<?php 
$valores5 = consulta_bd("total, descuento, valor_despacho, cant_productos","pedidos","estado_id IN (2,4,6,7) and YEAR(fecha) = $anoActual","");			
$ventasAnoActual=0;
$pedidosDelAno = 0;
$cantidadProductosAno = 0;
				
for($i=0; $i<sizeof($valores5); $i++) {
	$ventasAnoActual= $ventasAnoActual + ($valores5[$i][0] - $valores5[$i][1]);
	$pedidosDelAno =  $pedidosDelAno + 1;
	$cantidadProductosAno = $cantidadProductosAno + $valores5[$i][3];
}

?>
<div class="filaDashboard dashboard2">
	<h2 class="tituloDash">Reporte anual</h2>
	
	<div class="filaBloques">
		<div class="bloqueInfo" style="background-color: #101DC2">
			<h3>Ventas <?= $anoActual; ?></h3>
			<span>$<?= number_format($ventasAnoActual,0,",","."); ?></span>
			<div>Mes anterior +2.5%</div>
			<div>Mes año anterior +2.5%</div>
		</div>

		<div class="bloqueInfo" style="background-color: #1050CC;">
			<h3>Pedidos <?= $anoActual; ?></h3>
			<span><?= $pedidosDelAno; ?></span>
		</div>

		<div class="bloqueInfo" style="background-color: #1777B6;">
			<h3>Valor promedio carro</h3>
			<span>$<?= number_format(((int)$ventasAnoActual / (int)$pedidosDelAno),0,",","."); ?></span>
		</div>
		<div class="bloqueInfo" style="background-color: #10B2CC;">
			<h3>Cantidad Productos <?= $anoActual; ?></h3>
			<span><?= number_format($cantidadProductosAno,0,",","."); ?></span>
		</div>
		<div class="bloqueInfo" style="background-color: #10C2AA;">
			<h3>Promedio productos carro <?= $anoActual; ?></h3>
			<span><?= number_format(($cantidadProductosAno / $pedidosDelAno),0,",","."); ?></span>
		</div>

	</div>
	
	
	<div class="columnaIzquierda">
		<div class="tipsLaterales">
			<h5>Categorías mas vendidas</h5>
			<div class="contGrafico" id="masVendidas"></div><!--fin contGrafico-->
		</div>
		
		<!--<div class="tipsLaterales">
			<h5>Marcas mas vendidas</h5>
			<div class="contGrafico" id="marcasMasVendidas"></div>
		</div>-->
		
	</div><!--fin columnaIzquierda -->
	
	<div class="columnaDerecha">
		<div class="graficoDashboard">
			<div class="contenedor">
        
				<div class="total-por-anio">
					<h1 class="tituloTotalVemntas">Total Ventas</h1>
					<div class="box">
						<p><?= $anoActual ?></p>
						<p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i>
						<?= "$".number_format($ventasAnoActual,0,",","."); ?>
						</p>
					</div>
					<?php $valores6 = consulta_bd("SUM(total), SUM(descuento), valor_despacho, cant_productos","pedidos","estado_id IN (2,4,6,7) and YEAR(fecha) = $anoAnterior",""); ?>
					<div class="box">
						<p><?= $anoAnterior; ?></p>
						<p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i>
						<?php echo number_format(round($valores6[0][0] - $valores6[0][1]),0,",","."); ?>
						</p>
					</div>
					<?php 
					$anoAnteriorAlAnterior = $anoAnterior - 1;
					$valores7 = consulta_bd("SUM(total), SUM(descuento), valor_despacho, cant_productos","pedidos","estado_id IN (2,4,6,7) and YEAR(fecha) = $anoAnteriorAlAnterior",""); ?>
					<div class="box">
						<p><?= $anoAnteriorAlAnterior; ?></p>
						<p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i>
						<?php echo number_format(round($valores7[0][0] - $valores7[0][1]),0,",","."); ?>
						</p>
					</div>
					
				</div>
				<div class="div-g">
					<div id="grafico_ventas" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
				</div><!--fin selectores-->
				<div style="clear:both"></div>
			</div>
		</div><!--fin graficoDashboard-->
	</div><!--fin columnaDerecha-->
	
	
	
	
</div><!--fin filaDashboard-->

<div style="clear: both"></div>

<script type="text/javascript">
	$(function(){
		$(".filaMenuTabs a").click(function(){
			$(".filaMenuTabs a").removeClass("activoTab");
			var idActivar = $(this).attr("rel");
			$(this).addClass("activoTab");
			$(".tabsDashboard").fadeOut(100);
			$("#"+ idActivar).fadeIn(100);
		})
	});
</script>
