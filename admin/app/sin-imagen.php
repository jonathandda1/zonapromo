<div class="filaDashboard">
	<h2 class="tituloReporte">Productos sin Imagen</h2>
	<?php include('includes/menuLateralInterior.php');?>
	
	<?php 
	$resultado = consulta_bd("p.id, p.nombre, pd.sku, p.publicado, p.fecha_modificacion, p.thumbs","productos p, productos_detalles pd","p.id =pd.producto_id",""); 
	?>
	<div class="columnaDerecha">
		<div class="contTabsDashboard">
			<div class="tabActivoDashboard">
				<div class="filaTitulosInterior2">
					<div class="col1">Nombre</div>
					<div class="col2" style="width: 10%;">SKU</div>
					<div class="col3" style="width: 10%;">Estado</div>
					<div class="col4" style="width: 13%;">Modificación</div>
					<div class="col5" style="width: 10%;">Thumbs</div>
					<div class="col5" style="width: 10%;">Galería</div>
					<div class="col5"></div>
					
				</div>
				<?php for($i=0; $i<sizeof($resultado); $i++){ 
						
					if($resultado[$i][4] != ""){
						$fechaModificacion = substr($resultado[$i][4], 0, 10);
					} else {
						$fechaModificacion = "----";
					}
					
					$publicado = "";
					if($resultado[$i][3]){
						$publicado = '<i class="fas fa-check verde"></i>';
					} else {
						$publicado = '<i class="fas fa-times rojo"></i>';
					}
				
					if($resultado[$i][5] == ""){
						$thumbs = 0;
						$imgThumbs = '<i class="fas fa-times rojo"></i>';
						//muestro los datos y reviso si tiene imagenes de galeria
						$producto_id = $resultado[$i][0];
						$gal = consulta_bd("archivo","img_productos","producto_id = $producto_id ","");
						$cantGal = mysqli_affected_rows($conexion);
						if($cantGal == 0){
							$imgGal = 0;
							$galeria = '<i class="fas fa-times rojo"></i>';
						} else {
							//NO hago nada
							$imgGal = 1;
							$galeria = '<i class="fas fa-check verde"></i>';
						}
					?>
					<div class="filaDatos2">
						<div class="col1"><?= preview($resultado[$i][1], 35); ?></div>
						<div class="col2" style="width: 10%;"><?= $resultado[$i][2]; ?></div>
						<div class="col3" style="width: 10%;"><?= $publicado; ?></div>
						<div class="col4" style="width: 13%; min-height: 10px;"><?= $fechaModificacion; ?></div>
						<div class="col5" style="width: 10%;"><?= $imgThumbs; ?></div>
						<div class="col5" style="width: 10%;"><?= $galeria; ?></div>
						<div class="col5" style="float: right;"><a class="previewOjo" href="index.php?op=219c&id=<?= $resultado[$i][0]; ?>"><i class="far fa-eye"></i></a></div>
					</div>
					<?php } else {
						$thumbs = 1;
						$imgThumbs = '<i class="fas fa-check verde"></i>';
						$producto_id = $resultado[$i][0];
						$gal = consulta_bd("archivo","img_productos","producto_id = $producto_id ","");
						$cantGal = mysqli_affected_rows($conexion);
						if($cantGal == 0){
							$imgGal = 0;
							$galeria = '<i class="fas fa-times rojo"></i>';
					?>
				
				    <div class="filaDatos2">
						<div class="col1"><?= preview($resultado[$i][1], 35); ?></div>
						<div class="col2" style="width: 10%;"><?= $resultado[$i][2]; ?></div>
						<div class="col3" style="width: 10%;"><?= $publicado; ?></div>
						<div class="col4" style="width: 13%; min-height: 10px;"><?= $fechaModificacion; ?></div>
						<div class="col5" style="width: 10%;"><?= $imgThumbs; ?></div>
						<div class="col5" style="width: 10%;"><?= $galeria; ?></div>
						<div class="col5" style="float: right;"><a class="previewOjo" href="index.php?op=219c&id=<?= $resultado[$i][0]; ?>"><i class="far fa-eye"></i></a></div>
					</div>
					<?php
						} else {
							//NO hago nada
							$imgGal = 1;
						}
						} ?>
				
				
				<?php } ?>
				
				<!--<div class="fila">
					<a href="" target="_blank" class="descargarXLS">Descargar XLS</a>
				</div>
				-->
			</div>
		</div>
		
		
		
	
	</div>
	
</div><!--fin filaDashboard-->

<div style="clear: both"></div>