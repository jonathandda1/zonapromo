<div class="filaDashboard">
	<h2 class="tituloReporte">Productos más vendidos</h2>
	<?php include('includes/menuLateralInterior.php');?>
	
	<?php 
	//$resultado = consulta_bd("pd.producto_id, p.nombre, pd.sku, p.publicado, p.fecha_modificacion, pd.stock, pd.stock_reserva, pd.stock - pd.stock_reserva as stockreal","productos p, productos_detalles pd","p.id = pd.producto_id and (pd.stock - pd.stock_reserva < 1)",""); 
	
	$masVendidos = consulta_bd("pp.codigo, count(pp.codigo) as cantidadCodigosIguales, prd.id, prd.nombre, p.estado_id, sum(pp.cantidad) as totalVendidos, p.oc","pedidos p, productos_pedidos pp, productos_detalles pd, productos prd","prd.id = pd.producto_id and pd.id = pp.productos_detalle_id and pp.pedido_id = p.id and p.estado_id = 2 GROUP BY pp.codigo","totalVendidos desc limit 30");
	
	?>
	
																 
	<div class="columnaDerecha">
		<div class="contTabsDashboard">
			<div class="tabActivoDashboard">
				<div class="filaTitulosInterior" style="background-color: #10B2CC;">
					<div class="col2" style="margin-left: 20px;">id</div>
					<div class="col1" style="width: 50%;">Nombre</div>
					<div class="col3">SKU</div>
					<div class="col6">Vendidos</div>
					<div class="col8"></div>
					<!--crear nuevo estado para el pedido, entregado, en bodega, despachado, cerrado-->
					<!--crear nuevo estado para el retiro en tienda o despacho-->
				</div>
				<?php for($i=0; $i<sizeof($masVendidos); $i++){ ?>
				<div class="filaDatos">
					
					<div class="col2" style="padding-left: 20px;"><span class=""><?= $masVendidos[$i][2]; ?></span></div>
					<div class="col1" style="width: 50%;"><?= $masVendidos[$i][3]; ?></div>
					<div class="col3"><?= $masVendidos[$i][0]; ?></div>
					
					<div class="col6"><?= $masVendidos[$i][5]; ?></div>
					<div class="col8"><a href="index.php?op=219c&id=<?= $masVendidos[$i][2]; ?>" target="_blank"><i class="far fa-eye"></a></i></div>
				</div>
				<?php } ?>
				
				<!--<div class="fila">
					<a href="" target="_blank" class="descargarXLS">Descargar XLS</a>
				</div>-->
				
			</div>
		</div>
		
		
		
	
	</div>
	
</div><!--fin filaDashboard-->

<div style="clear: both"></div>