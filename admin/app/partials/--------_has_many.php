<?php

/********************************************************\
|  Moldeable CMS - Edición de contenido - Has Many       |
|  Fecha Modificación: 20/09/2012                        |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total                  	 |
|  http://www.moldeable.com/                             |
\********************************************************/

$tabla_original = $tabla;
$val_op_original = $val_op;

#Borro las opciones de la tabla original
$i = 0;
while($i <= (sizeof($opciones)-1))
{	
	$nombre = $opciones[$i][0];
	$opcion[$nombre] = '';
	$i++;
}
if(sizeof($opciones)>0)
extract($opcion);

$opcion = '';


$tabla = trim($tabla_hm);
$val_op_q = consulta_bd("id","tablas","nombre = '$tabla'","");
$val_op = $val_op_q[0][0];
//Obtengo las opciones de la tabla HM
$opciones = consulta_bd("nombre, valor","opciones_tablas","tabla_id = $val_op","");
$i = 0;
while($i <= (sizeof($opciones)-1))
{	
	$nombre = $opciones[$i][0];
	$opcion[$nombre] = $opciones[$i][1];
	$i++;
}
if(sizeof($opciones)>0)
extract($opcion);

?>

<div id="tab-<?php echo $t;?>">

	<!-- Hidden_fields -->
	<?php
		if ($hidden_fields) {
			$hidden_fields_array = json_decode($hidden_fields, true);
			foreach ($hidden_fields_array['hidden_fields'] as $val)
			{
				if ($val['valor'][0] == '$')
				{
					$resto_palabra = substr($val['valor'], 1);
					$valor =  $resto_palabra;
				}
				else
				{
					$valor = $val['valor'];
				}
			
				?>
					<input type="hidden" name="<?php echo $val['nombre'];?>" value="<?php echo $valor;?>" />
				<?php
			}
		}
	?>
	<!-- Fin Hidden_fields -->

	<div id="add_fields">
		<div id="has_many_fields_<?php echo $tabla;?>" style="display:none">
			<?php
			include('_has_many_fields.php');
			?>
			<input type="hidden" name="nested_fields_<?php echo $tabla;?>" id="nested_fields_<?php echo $tabla;?>" value="0" />
		</div>
		<div id="new_entry_<?php echo $tabla;?>" class="new_entry">
			<a href="javascript:void(0)" onclick="javascript:add_fields('<?php echo $tabla;?>');">
				<img src="pics/document_new.png" width="40" />
			</a>
		</div>
	</div>
	<!--<p>&nbsp;</p>-->
	<?php
	//Obtengo los elementos de la tabla para mostrarlos.
	$columnas_hm = show_columns($tabla, $exclusiones_list);
	$lista_columnas_hm = implode(",", $columnas_hm);
	$items = consulta_bd($lista_columnas_hm,"$tabla",singular($tabla_original)."_id = $id","id desc");
	if (sizeof($items) > 0) {
	?>
		<!--<h2><?php echo get_table_name($id_hm_tabla);?>:</h2>-->
        <h2><?php echo get_real_name($tabla); ?>:</h2>
		<table width="100%" border="0">
			<tr>
			<?php 
			$i = 0;
			$columna_autocompletar_items = array();
			$tabla_autocompletar_items = array();
			$id_col_autocompletar = array();
			if ($autocompletar){
				$autocompletar_array = explode(';',$autocompletar);
				foreach ($autocompletar_array as $autocompletar_item)
				{
					$datos_autocompletar = explode(',', $autocompletar_item);
					$tabla_autocompletar_items[$i] = trim($datos_autocompletar[0]);
					$columna_autocompletar_items[$i] = trim($datos_autocompletar[1]);
				}
			}			
			$i = 0;
			foreach ($columnas_hm as $col_hm) {
                $related[$i] = column_is_related($col_hm);
                if ($related[$i])
                {
					//titulo para productos relacionados o autocompletar
                    echo '<th align="left">'.get_real_name($related[$i]).'</th>';
                }
                else
                {
					//titulo para el resto de los campos
                    echo '<th align="left">'.get_real_name($col_hm).'</th>';
                }
				
				
				if ($col_hm == 'creado_por') 
				$id_creado_por_hm = $i; 
				
				if ($col_hm == 'fecha_creacion') 
				$id_fecha_creacion_hm = $i; 
				
				
				if (in_array($col_hm, $columna_autocompletar_items)) 
				$id_col_autocompletar[] .= $i; 
				
				$i++;
			} 
			?>
				<th width="20"></th>
                <th width="40"></th>
				<th width="20"></th>
			</tr>
			<?php
			$j = 0; 
			while($j <= (sizeof($items)-1))
			{
			?>
            <tr>
			<?php
				$i = 0;
				while($i <= sizeof($columnas_hm))
				{	
					if ($id_creado_por_hm === $i)
					{
						$user = consulta_bd_por_id("CONCAT(nombre, ' ', apellido_paterno) as nombre_completo","administradores","",$items[$j][$i]);	
						echo '<td>'.$user['nombre_completo'].'</td>';
					}
                    elseif ($related[$i]) {
                        $id = $items[$j][$i];
                        if ($id != null)
				        {
                            $val_related = consulta_bd_por_id("nombre", plural($related[$i]), '', $id);
                            $data = ($val_related['nombre'] == '') ? "&nbsp;": $val_related['nombre'];
                            
							echo '<td>'.ucwords($data).'</td>'; 
						}
                        else
                        {
                            echo '<td>&nbsp;</td>';
                        }
                    }
					elseif ($id_fecha_creacion_hm === $i) 
					{
						//Muestra la fecha de creacion
						echo '<td>'.fecha_sql($items[$j][$i]).'</td>';
					}
					elseif (stristr($archivos,chr($columnas_hm[$i])))
					{
						$extension = strtolower(substr(strrchr($items[$j][$i], '.'), 1));
						$file = $items[$j][$i];
						echo '<td><a href="../docs/'.$tabla.'/'.$items[$j][$i].'" target="_blank">'.$items[$j][$i].'</a></td>';
					}
					elseif (is_boolean(trim($columnas_hm[$i]), $tabla_hm))
					{
						$val_boolean = ($items[$j][$i] == 1) ? "Si": "No";
						//echo '<td>'.$val_boolean.'</td>';
                        if($val_boolean == Si){
                            $chequeado = 'checked="checked"';
                            $valor = 1;
                        } else {
                            $chequeado= '';
                            $valor = 0;
                        }
                        $idEntradaEnTablaOriginal = $id;
                        $nombreCampo = $columnas_hm[$i];
                        $idAEditar=$items[$j][0];
                        $nombre_Tabla= trim($tabla_hm);
                
                        echo '<td><input type="checkbox" name="check_nombreTabla_" value="'.$valor.'" '.$chequeado.' rel="'.$columnas_hm[$i].'-'.$idAEditar.'-'.$nombre_Tabla.'" class="check_" /><span class="'.$columnas_hm[$i].'-'.$idAEditar.'-'.$nombre_Tabla.'">'.$val_boolean.'</span></td>';
					}
					elseif(in_array($i, $id_col_autocompletar))
					{
						//Recorro los autocompletar
						foreach ($autocompletar_array as $autocompletar_item)
						{
							$datos_autocompletar = explode(',', $autocompletar_item);
							$tabla_autocompletar = trim($datos_autocompletar[0]);
							$columna_autocompletar = trim($datos_autocompletar[1]);
							
							if ($columnas_hm[$i] == $columna_autocompletar)
							{
								$val_autocompletar = consulta_bd_por_id("nombre","$tabla_autocompletar","",$items[$j][$i]);
								echo '<td>'.$val_autocompletar['nombre'].'</td>';
							}
						}
						
					}
					else
					{
						echo '<td style="position:relative;" class="td_editables">
								<span class="'.$columnas_hm[$i].'-'.$items[$j][0].'-'.$tabla.'">'.$items[$j][$i].'</span>
								<div class="form_editar_en_linea" style="display: none;">
									<input type="text" value="'.$items[$j][$i].'" name="'.$columnas_hm[$i].'-'.$items[$j][0].'-'.$tabla.'" id="" class="campo_editable_lista" >
									<a class="btn_editar_linea" href="javascript:void(0)">Guardar</a>
								</div>
							</td>';	
					}
					
					$i++;
				}
				?>
					<?php if ($opcion[archivos]) { ?>
					<td>
						<a href="#" target="_blank">
							<?php 
								if ($extension == 'pdf')	
								{
									$link = "index.php?op=pdfview&tabla=$tabla_hm&id=".$items[$j][0];
								}
								elseif ($extension == 'jpg' OR $extension == 'jpeg' OR $extension == 'gif' OR $extension == 'png')
								{
									$link = "../imagenes/$file";
								}
								else
								{
									$link = "../docs/$file";
								}
							?>
							<a href="<?php echo $link;?>" target="_blank" >
								<img src="pics/view.png" border="0" alt="Ver" title="Ver" width="20"/>
							</a>
						</a>
					</td>
					<?php } ?>
                    
                    <td>
						<?php if($tabla != 'productos_detalles'){ ; ?>
							<a href="app/edit_has_many.php?id=<?php echo $items[$j][0];?>&tabla=<?php echo trim($tabla_hm);?>&op=<?php echo $_GET[op];?>&idPadre=<?php echo $_GET[id]; ?>" class="ajax">
	                        	Editar
							</a>
						<?php }else{ ?>
							<a href="index.php?op=<?php echo $val_op;?>c&id=<?php echo $items[$j][0];?>">
	                        	Editar
							</a>
						<?php } ?>
					</td>
					<td>
						<?php
							$last_url = get_gets('');
						?>
						<a href="#" onclick="javascript:confirmar('<?php echo $items[$j][0];?>', 'del', '', '<?php echo trim($tabla_hm);?>', '', '<?php echo $last_url; ?>')">
							<img src="pics/delete.jpg" border="0" alt="Eliminar" title="Eliminar"/>
						</a>
					</td>
				</tr>
				<?php
				$j++;
			}
			?>
			<!--</tr>-->
		</table>
	<?php
	} else { echo "<p style='color:red'>No se han cargado ".get_real_name($hm)."</p>"; }
	?>
</div>


<?php
//Vuelta de valores.

$tabla = $tabla_original;
$val_op = $val_op_original;

//Obtengo las opciones de la tabla original
$opciones = consulta_bd("nombre, valor","opciones_tablas","tabla_id = $val_op","");
$i = 0;
while($i <= (sizeof($opciones)-1))
{	
	$nombre = $opciones[$i][0];
	$opcion[$nombre] = $opciones[$i][1];
	$i++;
}
if(sizeof($opciones)>0)
extract($opcion);


?>
