<?php 
include("../../conf.php");
$id = (is_numeric($_GET['id'])) ? mysqli_real_escape_string($conexion, $_GET['id']) : 0;

$va = consulta_bd("pd.id, pd.nombre, pd.sku, (SELECT valor_bruto FROM listas_productos WHERE sku = pd.sku and lista_id = $id) AS valor_bruto","productos_detalles pd","","id asc");


?>
<link href="../../css/style.css?v=3" rel="stylesheet" type="text/css" />
<link href="../../css/layout.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../css/tinytable.css" />
<form method="post" action="actualizacionListas.php?lista=<?= $id; ?>" id="formActualizarListas">
    <table width="100%" class="tinytable">
            <thead>
                <tr>
                    <th><h3>ID</h3></th>
                    <th><h3>Nombre</h3></th>
                    <th><h3>SKU</h3></th>
                    <th><h3>Valor bruto</h3></th>
                </tr>
            </thead>
            <tbody>
                <?php for($h=0; $h<sizeof($va); $h++) {?> 
                <tr>
                    <td><?= $va[$h][0];?></td>
                    <td><?= $va[$h][1];?></td>
                    <td><?= $va[$h][2];?></td>
                    <td><input type="text" name="valorBruto___<?= $va[$h][0];?>___<?= str_replace(' ', '*', $va[$h][2]);?>" value="<?php if($va[$h][3] > 0){echo $va[$h][3];} else {echo "0";}; ?>"></td>
                </tr>
                <?php } ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><input type="submit" name="enviar" value="Actualizar" class="btn_actualizarListas"></td>
                </tr>
            </tbody>
        </table>
</form>
