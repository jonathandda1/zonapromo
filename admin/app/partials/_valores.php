<?php  
    //echo get_val_op($_GET['op']);
    if(isset($_GET[id])){
        $va = consulta_bd("lp.id, pd.nombre, pd.sku, lp.valor_bruto","listas l, productos_detalles pd, listas_productos lp","l.id = lp.lista_id and lp.sku = pd.sku and lp.lista_id = $id","");
    }
    //CTX-617 T/58
?>

<div id="tab-valores">
    <a href="app/partials/modificar_valores.php?id=<?= $id;?>" class="informes btnModificarListasPrecios">modificar valores</a>
        <table width="100%" class="tinytable" id="table">
            <thead>
                <tr>
                    <th><h3>ID</h3></th>
                    <th><h3>Nombre</h3></th>
                    <th><h3>SKU</h3></th>
                    <th><h3>Valor bruto</h3></th>
                </tr>
            </thead>
            <tbody>
                <?php for($h=0; $h<sizeof($va); $h++) {?> 
                <tr <?php if($h%2==0){echo 'bgcolor="#ddd"';}?>>
                    <td><?= $va[$h][0];?></td>
                    <td><?= $va[$h][1];?></td>
                    <td><?= $va[$h][2];?></td>
                    <td><?= "$".number_format($va[$h][3],0,",","."); ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
    </div>