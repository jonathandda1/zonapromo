<?php
include('../../conf.php');
/********************************************************\
|  Moldeable CMS - Edición de contenido - Galerías       |
|  Fecha Modificación: 03/06/2013                        |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total                  	 |
|  http://www.moldeable.com/                             |
\********************************************************/

if ($galerias and isset($id))
{
	$val_op = get_val_op($op);
	$rel = (isset($rel1)) ? $rel1 : singular($tabla)."_id"; 
	$campos = ($texto_galerias) ? "archivo, id, posicion, texto, galeria" : "archivo, id, posicion,galeria";
	$campos .= ($multiple_galerias) ? ",galeria" : '';
	$filas = consulta_bd($campos,"img_$tabla","$rel = $id","posicion");
	$file = "../imagenes/$tabla";
	$cant = mysqli_affected_rows($conexion);
	?>
	<div id="tab-<?php echo $t; ?>">
	<?php
	if ($cant > 0)
	{
	?>
	<div id="imagenes">
	<table width="100%" border="0" cellpadding="5">
	<?php
		$i = 0;
		$j = 1;
		while($i <= (sizeof($filas)-1))
		{	
			if ($i%5==0) {
				echo "<tr width='100%'>";
			}
			$z = $i+1;
			$id_img = $filas[$i][1];
			$posicion = $filas[$i][2];
			$texto = ($texto_galerias) ? $filas[$i][3] : "";
			$galeria = ($multiple_galerias) ? $filas[$i][4] : '';
			echo '<td width="20%">
					<div class="galery_wrapper">';
				?>
               			<a style="color:#fff" class="img_galeria">
							<img src="pics/delete.png" border="0" class="eliminar_galeria" onclick="delete_img_gal('<?php echo $id ?>','<?php echo $id_img ?>', '<?php echo $tabla ?>', '<?php echo $val_op ?>')" width="30" height="30" />
							<img src=<?php echo $file; ?>/<?php echo $filas[$i][0]; ?> border="0" width="100%" />
						</a>
				<?php
					echo	'<br />Posición 
							<select name="posicion_img['.$id_img.']">';
							for ($counter = 1; $counter <= $cant; $counter++)
							{
								echo '<option value="'.$counter.'"';
								if ($posicion == $counter) echo 'selected="selected" ';
								echo '>'.$counter.'</option>';
							}
					echo '</select>';
					if ($multiple_galerias)
					{
						echo "<br />";
						$lista_galerias = explode(',',$multiple_galerias);
						if (sizeof($lista_galerias) > 1)
						{
							echo 'Galería: <select name="galeria_img['.$id_img.']" id="galeria">';
							foreach ($lista_galerias as $g) {
								echo '<option rel="'.$galeria.'" value="'.trim($g).'"';
								if (trim($g) == $galeria) echo 'selected="selected" ';
								echo '>';
								echo trim($g);
								echo '</option>';
							}
							echo '</select>';
						}
					}
					if ($texto_galerias)
					{
						echo '<br ><input type="text" name="texto_img['.$id_img.']" style="width:200px" value="'.$texto.'"/>';
					}
				    echo '</div>
				  </td>';
			$i++;
			$j++;
			if ($i%5==0) {
				echo "</tr>";
				$j=1;
			}
		}
		
		if ($j!=5)
		{
			while($j < 6)
			{
				echo "<td></td>";
				$j++;
			}
			echo "</tr>";
		}
		$t++;
	?>
	</table>
	</div>
	<p><strong>Nota:</strong> Haga clic sobre la imagen y luego sobre su cruz para eliminarla.<br /></p>
	<?php
	}
	else
	{
		echo "
		
		<div id='imagenes'>
			<p style='color:red;font-weight:bold'>No se han cargado imágenes</p>
		</div>";
		$t++;
	}
	?>
	</div>
	<?php
}
?>