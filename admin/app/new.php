<?php
/********************************************************\
|  Moldeable CMS - Creacion de entradas.				 |
|  Fecha Modificación: 25/06/2012				         |
|  Todos los derechos reservados © Moldeable S.A.  2012  |
|  Prohibida su copia parcial o total  			 		 |
|  http://www.moldeable.com/                             |
\********************************************************/

$val_op = get_val_op($op);
$tabla = obtener_tabla($val_op);

//Obtengo las opciones de la tabla
$opciones = consulta_bd("nombre, valor","opciones_tablas","tabla_id = $val_op","");
$i = 0;
while($i <= (sizeof($opciones)-1))
{	
	$nombre = $opciones[$i][0];
	$opcion[$nombre] = $opciones[$i][1];
	$i++;
}
extract($opcion);

if($create == '0' AND !isset($id))
die("No se permite la creación de entradas en la tabla $tabla.");

if($obligatorios == '')
die("Tabla $tabla no está correctamente configurada. <br /> Por favor contacte al administrador del sistema.");

if (strrpos($obligatorios, ","))
{
	$obligatorios = explode(',', $obligatorios);
}

?>

<form id="form" name="form" method="post" enctype="multipart/form-data" action="app/actions.php">
<div id="botonera">
	<div id="titulo"><h3>Agregar <?php echo get_real_name(singular(get_table_name($val_op)));?></h3></div>
	<div id="botones">
              <?php
                    if ($sm_table[0][0] != '')
                    {
                        $rid = $_GET[rid];
                        $rel="&r=$submenu_table&id=$rid";
                    } 
                ?>
		<button type="button" class="boton3 volver-edit" onclick="javascript:volver('index.php?op=<?php echo $val_op."a".$rel;?>')">Volver</button>
		<input type="submit" name="add_<?php echo $tabla;?>" id="add_<?php echo $tabla;?>" class="btn_siguiente" value="Siguiente"/>
		<input type="hidden" name="tabla" value="<?php echo $tabla;?>"/>
		<input type="hidden" name="op" value="<?php echo $op; ?>" />
        <input type="hidden" name="rid" value="<?php echo $rid; ?>" />
        
		<!-- Hidden_fields -->
		<?php
			if ($hidden_fields) {
				$hidden_fields_array = json_decode($hidden_fields, true);
				foreach ($hidden_fields_array['hidden_fields'] as $val)
				{
					if ($val['valor'][0] == '$')
					{
						$resto_palabra = substr($val['valor'], 1);
						$valor =  $$resto_palabra;
					}
					else
					{
						$valor = $val['valor'];
					}
				
					?>
						<input type="hidden" name="<?php echo $val['nombre'];?>" value="<?php echo $valor;?>" />
					<?php
				}
			}
		?>
		<!-- Fin Hidden_fields -->
        
	</div>
</div>



	<table width="100%" border="0">
    	<tr>
        	<td width="100"></td>
            <td></td>
        </tr>
    	<?php 
    		$columnas = show_columns($tabla, '');
    		$i = 0;
    		while($i <= (sizeof($columnas)-1))
    		{	
    			$column = ucwords(preg_replace('/_/', ' ', $columnas[$i]));
    			$column_id = $columnas[$i];
    			
    			if (sizeof($obligatorios) == 1)
    			{
	    			if ($columnas[$i] == trim($obligatorios))
	    			{
						$parent = column_is_related($columnas[$i]);
						if ($parent)
						{
							echo '<tr>';
							echo '<td>'.get_real_name(ucwords($parent)).'</td>';
							echo '<td>';
									select(plural($parent), 'nombre', 'id', $columnas[$i], '', '', '', $_GET[$parent."_id"]);
							echo '</td>';
							echo '</tr>';
						}
						else
						{
		    				echo '<tr>';
		    				echo '<td>'.$column.'</td>';
		    				echo '<td><input class="campo_texto" type="text" id="'.$column_id.'" name="'.$column_id.'" value="'.$_GET[$column_id].'"/></td>';
		    				echo '</tr>';
						}
	    			}
	    			elseif (sizeof($columnas)==$i)
	    			{
	    				echo "Error en el nombre del campo obligatorio.";
	    				break;
	    			}
    			}
    			else
    			{
    				if ($obligatorios != '')
    				{
		    			foreach($obligatorios as $o)
		    			{
			    			if ($columnas[$i] == trim($o))
			    			{
								$parent = column_is_related($columnas[$i]);
								if ($parent)
								{
									//Si el padre tiene una relación, el select debe ser dependiente (belongs_to) o se debe establecer una relación many_to_many
									//Consulto por la opción many_to_many del padre
									$parent_plural = plural($parent);
									$many_to_many_q = consulta_bd("ot.nombre, ot.valor","opciones_tablas ot, tablas t","t.nombre = '$parent_plural' AND ot.tabla_id = t.id AND (ot.nombre = 'many_to_many' OR ot.nombre = 'belongs_to')","");
									$j = 0;
									while($j <= (sizeof($many_to_many_q)-1))
									{	
										$nombre_p = $many_to_many_q[$j][0];
										$opcion_p[$nombre_p."_parent"] = $many_to_many_q[$j][1];
										$j++;
									}
									
									if (isset($belongs_to_parent))
									$belongs_to_parent = NULL;
									
									if ($opcion_p != NULL)
									extract($opcion_p);
									
									$opcion_p = NULL;
								}

								if ($belongs_to_parent != '' AND $parent)
								{
									$sel2 = $columnas[$i];
				    				echo '<tr>';
				    				echo '<td>'.ucwords(singular($belongs_to_parent)).'</td>';
				    				echo '<td>';
				    					select($belongs_to_parent, 'nombre', 'id', $belongs_to_parent, '', '', '', $_GET[$belongs_to_parent."_id"]);
				    				echo '</td>';
				    				echo '</tr>';
				    				
				    				echo '<tr>';
				    				echo '<td>'.ucwords($parent).'</td>';
				    				echo '<td>';
				    					select(plural($parent), 'nombre', 'id', $sel2, '', "WHERE nombre = NULL", '', $_GET[$parent."_id"]);
				    				echo '</td>';
				    				echo '</tr>';
				    				
				    				$btp = $belongs_to_parent;
								}
								elseif ($parent)
								{
									echo '<tr>';
									echo '<td>'.get_real_name(ucwords($parent)).'</td>';
									echo '<td>';
											select(plural($parent), 'nombre', 'id', $columnas[$i], '', '', '', $_GET[$parent."_id"]);
									echo '</td>';
									echo '</tr>';
								}
								else
								{
				    				echo '<tr>';
				    				echo '<td>'.$column.'</td>';
				    				echo '<td><input class="campo_texto" type="text" id="'.$column_id.'" name="'.$column_id.'" value="'.$_GET[$column_id].'"/></td>';
				    				echo '</tr>';
								}
			    			} 
		    			}
    				}
    			}
    			$i++;
    		}
    	?>
	</table>
</form>

<?php 
//Activación de los select dependientes en caso de que exista relación many to many.
if ($btp != NULL) {
?>
<script type="text/javascript">
	$(function(){		
		$("#form").relatedSelects({
			onChangeLoad: 'includes/dependent_selects_new.php',
			selects: ['<?php echo $btp; ?>', '<?php echo $sel2; ?>']
		});
	});
	</script>
<?php
}
?>