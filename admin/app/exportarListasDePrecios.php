<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');
include("conf.php");

require_once 'PHPExcel-1.8/Classes/PHPExcel.php';
require_once 'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$productos = consulta_bd("id, nombre","listas","","");
//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Nombre');

for($i=0; $i<sizeof($productos); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $productos[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $productos[$i][1]);
}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Listas');






// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();
// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(1);


//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Lista_id');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'SKU');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Valor bruto');

$PD = consulta_bd("id, lista_id, sku, valor_bruto","listas_productos","","");
for($i=0; $i<sizeof($PD); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $PD[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $PD[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $PD[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $PD[$i][3]);
}


// Rename 2nd sheet
$objPHPExcel->getActiveSheet()->setTitle('Valores listas');





// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Listas_de_precios.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>
