<?php 

/********************************************************\
|  Moldeable CMS - Envía mail de soporte.		         |
|  Fecha Modificación: 25/06/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/

?>
<div id="botonera">
	<div id="titulo"><h3>Insertar pedidos en SAP </h3></div>
	
</div>

<form name="formInsertSap" action="app/partials/ajaxInsertarEnSap.php" id="formInsertSap" class="formInsertSap" method="post">
    <label>Ingrese Orden de compra</label>
    <input type="text" name="oc" value="" class="campoOC campo_texto" id="campoOC">
    <a href="javascript:void(0)" class="btnOC">Insertar en SAP</a>
</form>
<p>Esta opcion solo envia los pedidos a sap, corroborar que esten cancelados antes de ejecutarlo, no envia a enviame.</p>
<script>
    $(function(){
        $(".btnOC").click(function(){
            var oc = $("#campoOC").val();
            if(oc != ""){
                //envio la oc
                $(".formInsertSap").submit();
            } else {
                //debe ingresar una oc para validar  
                alertify.success("debe ingresar una oc para validar ");
            }
        });
    })
</script>
 