<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');
ini_set('max_execution_time', 0);
ini_set('memory_limit', '600M');
include("conf.php");

require_once 'PHPExcel-1.8/Classes/PHPExcel.php';
require_once 'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$productos = consulta_bd("id, nombre, sku, icono, posicion, fecha_creacion","lineas","","");
//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Nombre');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'SKU');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Icono');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Posicion');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Fecha creacion');

for($i=0; $i<sizeof($productos); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $productos[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $productos[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $productos[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $productos[$i][3]);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$linea, $productos[$i][4]);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$linea, $productos[$i][5]);
}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Lineas');



// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();
// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(1);


//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ID Linea');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Nombre');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Posición');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Fecha modificacion');

$categorias = consulta_bd("id, linea_id, nombre, posicion, fecha_creacion","categorias","","");
for($i=0; $i<sizeof($categorias); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $categorias[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $categorias[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $categorias[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $categorias[$i][3]);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$linea, $categorias[$i][4]);
}


// Rename 2nd sheet
$objPHPExcel->getActiveSheet()->setTitle('Categorias');



// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();
// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(2);


//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'ID Producto');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Id Linea');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Id categoría');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Fecha modificacion');

$lineasProductos = consulta_bd("id, producto_id, linea_id, categoria_id, fecha_creacion","lineas_productos","","");
for($i=0; $i<sizeof($lineasProductos); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $lineasProductos[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $lineasProductos[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $lineasProductos[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $lineasProductos[$i][3]);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$linea, $lineasProductos[$i][4]);
}


// Rename 2nd sheet
$objPHPExcel->getActiveSheet()->setTitle('Lineas-Productos');





// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="lineas_categorias_productos.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>
