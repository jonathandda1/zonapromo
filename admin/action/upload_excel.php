<?php 
include('../conf.php');
require('../PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');

$file = $_FILES['excel']['name'];
$tipo = $_FILES['excel']['type'];

$que_carga = $_POST['tipo'];

if (trim($file) == '' || $file == null) {
	header('Location: '.$_SERVER['HTTP_REFERER'].'&error=Debes cargar un archivo&tipo=error');
}elseif($que_carga == 0) {
	header('Location: '.$_SERVER['HTTP_REFERER'].'&error=Debes seleccionar tipo de carga&tipo=error');
}elseif($que_carga == 1){
	$name_tipo = "stock_y_precio";
}elseif($que_carga == 2){
	$name_tipo = "productos";
}elseif($que_carga == 3){
	$name_tipo = "listas";
} elseif($que_carga == 4){
	$name_tipo = "valores_listas";
}

$archivo = "upload_".$name_tipo."_".date('dmYHis', time()).'-'.$file;
$destino = "../excel/".$archivo;

if (copy($_FILES['excel']['tmp_name'],$destino)){
	// echo "Archivo Cargado Con Éxito";
}else{
	$errors= error_get_last();
 	echo "Error Al Cargar el Archivo ";
 	var_dump($errors);
 	die();
}

if (file_exists($destino)) {
	if ($name_tipo == "stock_y_precio") {
		$obj = PHPEXCEL_IOFactory::load($destino);
		$obj->setActiveSheetIndex(0);
		$rows = $obj->setActiveSheetIndex(0)->getHighestRow();

		for ($i=2; $i <= $rows ; $i++) { 
			$sku = trim($obj->getActiveSheet()->getCell('A'.$i)->getCalculatedValue());
			$precio = $obj->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
			$descuento = $obj->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
			$stock = $obj->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();

			// Verifico si existe el sku para hacer el cambio
			$producto = consulta_bd('id', 'productos_detalles', "sku = '$sku'", '');
			if (is_array($producto)) {
				update_bd('productos_detalles', "precio = $precio, descuento = $descuento, stock = $stock", "id = {$producto[0][0]}");
			}
		};
	} else if ($name_tipo == "valores_listas") {
		$obj = PHPEXCEL_IOFactory::load($destino);
		$obj->setActiveSheetIndex(0);
		$rows = $obj->setActiveSheetIndex(0)->getHighestRow();

		for ($i=2; $i <= $rows ; $i++) { 
			$id = trim($obj->getActiveSheet()->getCell('A'.$i)->getCalculatedValue());
			$lista_id = $obj->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
			$sku = $obj->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
            $valor_bruto = $obj->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
            
            if($id != ""){
                //die("sadasdasd $lista_id    $valor_bruto");
                // Verifico si existe el sku para hacer el cambio
                $listas_productos = consulta_bd('id', 'listas_productos', "lista_id = $lista_id and sku = '$sku'", '');
                if (count($listas_productos) > 0) {
                    //existe el producto
                    update_bd('listas_productos', "valor_bruto = $valor_bruto", "lista_id = $lista_id and sku = '$sku'");
                } else {
                    insert_bd("listas_productos","lista_id, sku, valor_bruto","$lista_id, '$sku', $valor_bruto");
                    //No existe y lo ingreso
                }
            }
            
		};
        
        
	} else if ($name_tipo == "listas") {
		$obj = PHPEXCEL_IOFactory::load($destino);
		$obj->setActiveSheetIndex(0);
		$rows = $obj->setActiveSheetIndex(0)->getHighestRow();

		for ($i=2; $i <= $rows ; $i++) { 
			$id = trim($obj->getActiveSheet()->getCell('A'.$i)->getCalculatedValue());
			$nombre = $obj->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
			
            if($id != ""){
                //die("sadasdasd $lista_id    $valor_bruto");
                // Verifico si existe el sku para hacer el cambio
                $listas = consulta_bd('id', 'listas', "id = $id", '');
                if (count($listas) > 0) {
                    //existe el producto
                    update_bd('listas', "nombre = '$nombre'", "id = $id");
                } else {
                    insert_bd("listas","id, nombre","$id, '$nombre'");
                    //No existe y lo ingreso
                }
            }
            
		};
        
        
	} elseif($name_tipo == "productos"){
		// Tabla productos
		$obj_productos = PHPEXCEL_IOFactory::load($destino);
		$obj_productos->setActiveSheetIndex(0);
		$rows_prd = $obj_productos->setActiveSheetIndex(0)->getHighestRow();

		for ($i=2; $i <= $rows_prd; $i++) { 
		 	$id = $obj_productos->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
		 	$publicado = $obj_productos->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
		 	$cyber = $obj_productos->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
		 	$recomendado = $obj_productos->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
		 	$pack = $obj_productos->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
		 	$codigos = trim($obj_productos->getActiveSheet()->getCell('F'.$i)->getCalculatedValue());
		 	$nombre = $obj_productos->getActiveSheet()->getCell('G'.$i)->getCalculatedValue();
		 	$marca_id = $obj_productos->getActiveSheet()->getCell('H'.$i)->getCalculatedValue();
		 	$video = $obj_productos->getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
		 	$ficha_tecnica = $obj_productos->getActiveSheet()->getCell('J'.$i)->getCalculatedValue();
		 	$thumbs = $obj_productos->getActiveSheet()->getCell('K'.$i)->getCalculatedValue();
		 	$descripcion = $obj_productos->getActiveSheet()->getCell('L'.$i)->getCalculatedValue();
		 	$descripcion_seo = $obj_productos->getActiveSheet()->getCell('M'.$i)->getCalculatedValue();
		 	$keywords = $obj_productos->getActiveSheet()->getCell('N'.$i)->getCalculatedValue();

		 	$existe_in_db = consulta_bd('id', 'productos', "id = $id", '');
		 	if (is_array($existe_in_db)) {
		 		update_bd('productos', "id = $id, publicado = $publicado, cyber = $cyber, recomendado = $recomendado, pack = $pack, codigos = '$codigos', nombre = '$nombre', marca_id = $marca_id, video = '$video', ficha_tecnica = '$ficha_tecnica', thumbs = '$thumbs', descripcion = '$descripcion', descripcion_seo = '$descripcion_seo', keywords = '$keywords', fecha_modificacion = NOW()", "id = $id");
		 	}else{
		 		insert_bd('productos', "id,publicado, cyber, recomendado, pack , codigos, nombre, marca_id, video, ficha_tecnica, thumbs, descripcion, descripcion_seo, keywords, fecha_creacion", "$id, $publicado, $cyber, $recomendado, $pack, '$codigos', '$nombre', $marca_id, '$video', '$ficha_tecnica', '$thumbs', '$descripcion', '$descripcion_seo', '$keywords', NOW()");
		 	}
		} 

		// Tabla productos detalles
		$obj_productos_detalles = PHPEXCEL_IOFactory::load($destino);
		$obj_productos_detalles->setActiveSheetIndex(1);
		$rows_prd_d = $obj_productos_detalles->setActiveSheetIndex(1)->getHighestRow();

		for ($i=2; $i <= $rows_prd_d; $i++) { 
		 	$id = $obj_productos_detalles->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
		 	$producto_id = $obj_productos_detalles->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
		 	$publicado = $obj_productos_detalles->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
		 	$precio_cantidad = $obj_productos_detalles->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
		 	$nombre = $obj_productos_detalles->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
		 	$sku = $obj_productos_detalles->getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
		 	$tamaño_id = $obj_productos_detalles->getActiveSheet()->getCell('G'.$i)->getCalculatedValue();

		 	$ancho = $obj_productos_detalles->getActiveSheet()->getCell('H'.$i)->getCalculatedValue();
		 	if ($ancho == '' OR $ancho == NULL) {
		 		$ancho = 0;
		 	}

		 	$alto = $obj_productos_detalles->getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
		 	if ($alto == '' OR $alto == NULL) {
		 		$alto = 0;
		 	}

		 	$largo = $obj_productos_detalles->getActiveSheet()->getCell('J'.$i)->getCalculatedValue();
		 	if ($largo == '' OR $largo == NULL) {
		 		$largo = 0;
		 	}

		 	$precio = $obj_productos_detalles->getActiveSheet()->getCell('K'.$i)->getCalculatedValue();
		 	$descuento = $obj_productos_detalles->getActiveSheet()->getCell('L'.$i)->getCalculatedValue();
		 	$stock = $obj_productos_detalles->getActiveSheet()->getCell('M'.$i)->getCalculatedValue();
		 	$stock_reserva = $obj_productos_detalles->getActiveSheet()->getCell('N'.$i)->getCalculatedValue();
		 	$venta_minima = $obj_productos_detalles->getActiveSheet()->getCell('O'.$i)->getCalculatedValue();

		 	$existe_in_db = consulta_bd('id', 'productos_detalles', "id = $id", '');
		 	if (is_array($existe_in_db)) {
		 		update_bd('productos_detalles', "id = $id, producto_id = $producto_id, publicado = $publicado, precio_cantidad = $precio_cantidad, nombre = '$nombre', sku = '$sku', tamaño_id = $tamaño_id, ancho = $ancho, alto = $alto, largo = $largo, precio = $precio, descuento = $descuento, stock = $stock, stock_reserva = $stock_reserva, venta_minima = $venta_minima, fecha_modificacion = NOW()", "id = $id");
		 	}else{
		 		insert_bd('productos_detalles', "id, producto_id, publicado, precio_cantidad, nombre, sku, tamaño_id, ancho, alto, largo, precio, descuento, stock, stock_reserva, venta_minima, fecha_creacion", "$id, $producto_id, $publicado, $precio_cantidad, '$nombre', '$sku', $tamaño_id, $ancho, $alto, $largo, $precio, $descuento, $stock, $stock_reserva, $venta_minima, NOW()");
		 	}
		} 

		// Categorización
		$obj_cat = PHPEXCEL_IOFactory::load($destino);
		$obj_cat->setActiveSheetIndex(2);
		$rows_cat = $obj_cat->setActiveSheetIndex(2)->getHighestRow();

		for ($i=2; $i <= $rows_cat; $i++) { 
			$linea_id = $obj_cat->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
			$producto_id = $obj_cat->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
			$categoria_id = $obj_cat->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
			$subcategoria_id = $obj_cat->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();

			$existe_in_db = consulta_bd('producto_id', 'lineas_productos', "producto_id = $producto_id", '');
			if (is_array($existe_in_db)) {
				update_bd('lineas_productos', "linea_id = $linea_id, producto_id = $producto_id, categoria_id = $categoria_id, subcategoria_id = $subcategoria_id", "producto_id = $producto_id");
			}else{
				insert_bd('lineas_productos', "linea_id, producto_id, categoria_id, subcategoria_id", "$linea_id, $producto_id, $categoria_id, $subcategoria_id");
			}
		}
	}

	$date = date('d/m/Y', time());
	insert_bd('historial_upload_excel', 'fecha_subida, archivo, tipo', "'$date', '$archivo', '$name_tipo'");
}

header('Location: '.$_SERVER['HTTP_REFERER']);
