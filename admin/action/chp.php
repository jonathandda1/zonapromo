<?php
include ('../conf.php');
require_once('../includes/bcrypt_class.php');
if ($_POST[update_pass])
{
	$hash = mysqli_real_escape_string($conexion, $_POST[hash]);
	$email = mysqli_real_escape_string($conexion, $_POST[e]);
	if($_POST[password] AND $_POST[password2] AND strlen($_POST[password])>=6)
	{
		if($_POST[password] == $_POST[password2])
		{
			$id_q = consulta_bd("id","administradores","password_hash = '$hash' AND email = '$email'","");
			$id = $id_q[0][0];
			
			if ($id)
			{
				$bcrypt = new Bcrypt(15);
				
				$new_hash = md5(date("Ymds"));
				$password = $bcrypt->hash($_POST[password]);
				$update = update_bd("administradores", "password_hash = '$new_hash', password = '$password', active = 1", "id = '$id'");
				if($update)
				{
					$error = "Su password ha sido actualizada.&tipo=exito";
				}
				else
				{
					$error = "Error al actualizar su password, por favor inténtelo nuevamente.&tipo=error";
				}
			    /*Log*/
			    $log = insert_bd("system_logs","tabla, accion, fila, administrador_id,date","'administradores','Cambio de contraseña', '','$row', NOW()");
			    /*Fin Log*/
			}
			else
			{
				$error = "Error al actualizar su password, por favor inténtelo nuevamente.&tipo=error";
			}
			header("location:../adlogin.php?op=fgp&error=$error");
		}
		else
		{
			$error = "Las contraseñas no coinciden&tipo=notificacion";
			header("location:../adlogin.php?op=fgp&error=$error&hash=$hash&e=$email");
		}
	}
	else
	{
		$error = "Debe ingresar una contraseña de al menos 4 caracteres.&tipo=notificacion";
		header("location:../adlogin.php?op=fgp&error=$error&hash=$hash&e=$email");
	}
	header("location:../adlogin.php?error=$error");
}
?>