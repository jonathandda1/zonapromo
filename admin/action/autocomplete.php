<?php

/********************************************************\
|  Moldeable CMS - Autocompletar            		     |
|  Fecha Modificación: 20/09/2012		                 |
|  Todos los derechos reservados © Moldeable Ltda 2012   |
|  Prohibida su copia parcial o total  					 |
|  http://www.moldeable.com/                             |
\********************************************************/

include('../conf.php');
$term = mysqli_real_escape_string($conexion, $_GET[term]);
$tabla = mysqli_real_escape_string($conexion, $_GET[tabla]);

$filas = consulta_bd("id, nombre","$tabla","nombre LIKE '%$term%'","nombre");

$return = "[";
$i = 0;
while($i <= (sizeof($filas)-1))
{	
	$return .= '{"id": "'.$filas[$i][0].'", "label": "'.$filas[$i][1].'"}';
	if ($i != (sizeof($filas)-1))
	{
		$return .= ",";
	}
	$i++;
}
$return .= "]";

echo $return;

?>