// JavaScript Document
$(function(){
	$('.btn-next-news').on('click', function(){
		var template = $('.select-template:checked').attr('id');

		var val_template = $('.select-template:checked').val();
		var id_n = $(this).attr('data-n');
		var tpl_actual = $('.preview').attr('data-tpl');
		// console.log(template);
		// console.log(id_n);

		if (parseInt(tpl_actual) != parseInt(val_template)) {
			alertify.confirm(
				'Si continuas, se eliminarÃ¡ la informaciÃ³n de la plantilla anterior.', 
				function(e){ 
					if (e) {
						$.ajax({
							url: 'action/save_newsletter.php',
							type: 'post',
							dataType: 'json',
							data: { action: 'save_tpl', n_id: id_n, tpl_id: template }
						}).done(function(res){
							$('#paso-1').fadeOut( 300, function(){

								$('.preview').attr('data-tpl', val_template);
								$('.getcode').attr('data-tpl', val_template);

								$('#paso-2').fadeIn();
								if (template == 'template-1') {
									$('.template-1').fadeIn();
									$('.template-2').fadeOut();
								}else{
									$('.template-2').fadeIn();
									$('.template-1').fadeOut();
								}
								
							});
						}).fail(function(res){
							console.log(res);
						})
					}
				}
			);
		}else{
			$.ajax({
				url: 'action/save_newsletter.php',
				type: 'post',
				dataType: 'json',
				data: { action: 'save_tpl', n_id: id_n, tpl_id: template }
			}).done(function(res){
				$('#paso-1').fadeOut( 300, function(){

					$('#paso-2').fadeIn();
					if (template == 'template-1') {
						$('.template-1').fadeIn();
						$('.template-2').fadeOut();
					}else{
						$('.template-2').fadeIn();
						$('.template-1').fadeOut();
					}
					
				});
			}).fail(function(res){
				console.log(res);
			})
		}
		
	})

	$('.btn-reset-news').on('click', function(){
		$('#paso-2').fadeOut( 300, function(){
			$('#paso-1').fadeIn();
			$('.template-1').fadeOut();
			$('.template-2').fadeOut();
		});
	})

	$('.add-producto-principal').on('click', function(){
		$('.bg_popup_news').fadeIn();
		$('.content_pop_news').fadeIn();
	})

	$('.bg_popup_news').on('click', function(){
		$('.bg_popup_news').fadeOut();
		$('.content_pop_news').fadeOut();
	})

	$('.btn-search').on('click', function(){
		var palabra = $('.input-popup').val();

		if (palabra.trim() != '') {
			$('.input-popup').removeClass('red-error');
			$.ajax({
				url: 'action/product-news.php',
				type: 'post',
				dataType: 'json',
				data: { palabra: palabra }
			}).done(function(res){
				// console.log(res);
				if (res[0]['status'] == 'error') {
					$('.box-autocomplete').html('No se encontrÃ³ producto.');
				}else{

					$('.box-autocomplete').html('<div class="row-search">\
						<span class="nombre_prd">' + res[0]["nombre"] + '</span>\
						<a href="#" onclick="javascript:closeColorbox(this)" class="selec-product" data-id="' + res[0]["id"] + '">Seleccionar</a>\
					</div>');
				}
			}).fail(function(res){
				console.log(res);
			})
		}else{
			$('.input-popup').addClass('red-error');
		}
		
	})

	$('.validate-form').on('submit', function(){
		var template = $(this).attr('id'),
			error = 0;

		if (template == 'form_template1') {
			/*var nombre = $('input[name="nombre"]').val(),
				titulo = $('input[name="titulo"]').val(),
				link = $('input[name="link"]').val(),
				titulo_link = $('input[name="nombre_link"]').val(),
				imagen_banner = $('input[name="imagen_banner"]').val(),
				descripcion = $('.descripcion_t1').val(),
				banner_1 = $('input[name="banner_1"]').val(),
				banner_2 = $('input[name="banner_2"]').val(),
				grilla_1 = $('input[name="prdsGrillaUno"]').val(),
				grilla_2 = $('input[name="prdsGrillaDos"]').val();

			if (nombre.trim() != '' && titulo.trim() != '' && link.trim() != '' && titulo_link.trim() != '' && imagen_banner.trim() != '' && descripcion.trim() != '' && banner_1.trim() != '' && banner_2.trim() != '' && grilla_1.trim() != '' && grilla_2.trim() != '') {

			}else{
				alertify.error("Todos los campos son obligatorios.");
				error++;
			}*/

		}else{

		}

		if (error > 0) {
			return false;
		}else{
			return true;
		}
		
	})

	$('.preview').click(function(e){
		e.preventDefault();
		var template = $(this).attr('data-tpl');
		if($(this).hasClass('n2')){
			url = 'preview.php?n2&id=';
		}
		else {
			url = 'preview.php?id=';
		}
		url = url+$(this).attr('data-id')+'&template='+template;
		$.colorbox({
			href:url,
			maxHeight:'500px',
			top: '50px',
			width: '700px'
		});
	});

	$('.getcode').click(function(e){
		e.preventDefault();
		var template = $(this).attr('data-tpl');
		if($(this).hasClass('n2')){
			url = 'getcode.php?n2&id=';
		}
		else {
			url = 'getcode.php?id=';
		}
		url = url+$(this).attr('data-id')+'&template='+template;
		console.log(url);
		$.colorbox({
			href:url,
			height:'400px',
			maxHeight:'100%',
			width: '900px',
			top: '50px'
		});
	});
})

function closeColorbox(e){
	var producto_id = $(e).attr('data-id');

	$('.add-producto-principal').attr('data-id', producto_id);
	$('input[name="producto_principal"]').val(producto_id);
	$('.pp-seleccionado').html('Producto Seleccionado: ' + producto_id);

	$('.bg_popup_news').fadeOut();
	$('.content_pop_news').fadeOut();

}

function deleteNews(id){
	alertify.confirm(
	'Â¿EstÃ¡s seguro de eliminar este elemento?', 
	function(e){ 
		if (e) {
			$.ajax({
				url: 'action/delete_news.php',
				type: 'post',
				dataType: 'json',
				data: { id: id }
			}).done(function(res){
				location.reload();
			}).fail(function(res){
				console.log(res);
			})
		}
	});
}

function deleteImg(newsletter_id, columna, template){

	$.ajax({
		url: 'action/delete_img_news.php',
		type: 'post',
		dataType: 'json',
		data: { newsletter_id: newsletter_id, columna: columna, template: template }
	}).done(function(res){
		console.log(res);
		if (res == 'success') {
			location.reload();
		}else{
			alertify.error('Error al eliminar la imagen.');
		}
	}).fail(function(res){
		console.log(res);
	})

}