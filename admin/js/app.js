function popup(pagina, width, height) {
	var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width="+width+", height="+height+", top=85, left=140";
	window.open(pagina,"",opciones);
}

//Muestra Div Oculto
function mostrar(element){
	$(element).fadeIn("slow");
}

function ocultar(element){
	$(element).fadeOut("slow");
}

//Marca todos los checkbox de un formulario
function seleccionar_todo(){ 
   for (i=0;i<document.form.elements.length;i++) 
      if(document.form.elements[i].type == "checkbox"){
      	if (document.form.elements[i].checked==1)
      	{
      		document.form.elements[i].checked=0
      	}
      	else
      	{
      		document.form.elements[i].checked=1
      	}
      } 
}

function volver(pagina)
{
    window.open(pagina,'_self');
}

function confirmar(id, action, msg, tabla, rid, last_url){ 
	if (msg == '')
    {
            msg = "Está seguro que desea eliminar esta entrada?";
    }

    //var r = confirm(msg);
	alertify.set({ labels: { ok: "Eliminar", cancel: "Cancelar" } });
	alertify.confirm(msg, function (e) {
				if (e) {
					if (rid != '')
					{
						window.location="app/actions.php?"+action+"="+id+"&tabla="+tabla+"&rid="+rid;
					}
					else if (last_url != '')
					{
						window.location="app/actions.php?"+action+"="+id+"&tabla="+tabla+last_url;
					}
					else
					{
						window.location="app/actions.php?"+action+"="+id+"&tabla="+tabla;
					}
					alertify.success("Se ha eliminado la entrada");
				} else {
					alertify.error("Se ha conservado la entrada");
				}
			});
    if (r == true)
    { 
        /*if (rid != '')
        {
            window.location="app/actions.php?"+action+"="+id+"&tabla="+tabla+"&rid="+rid;
        }
        else if (last_url != '')
        {
	        window.location="app/actions.php?"+action+"="+id+"&tabla="+tabla+last_url;
        }
        else
        {
            window.location="app/actions.php?"+action+"="+id+"&tabla="+tabla;
        }*/
    }
    else
    {
    	//return false;
    }
    return false;
} 


function miles(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

//Muetsra globo para borrar imagenes
var last_id;
function show_delete_img_gal(id) {
	
	if (last_id)
	{
		$('#delete_'+last_id).fadeOut('fast');
	}
	if (last_id != id)
	{
		$('#delete_'+id).fadeIn('fast');
	}
	last_id = id;
}

function delete_img_gal(val_id, val_img_id, val_tabla, val_op) {
	$("#loader_consulta").ajaxStart(function(){
		reset();
			alertify.log("Realizando Modificacion");
		return false;
		//$("#loader_consulta").fadeIn(200);
	});
	
	$("#loader_consulta").ajaxStop(function(){
		//$(this).fadeOut(200);
		reset();
		alertify.success("Modificacion realizada con éxito");
		return false;
	});
	
	$.post('action/delete_img_gal.php', {id: val_id, img_id: val_img_id, tabla: val_tabla, val_op: val_op}, function(data) {
			$('#imagenes').empty();
			$('#imagenes').html(data);
	});
}


$("a[alt=Borrar]").click(function(){
	$(this).parent("div").find("#img_entry").css("display","block");
	//return;
});


$(function(){
	$('a[alt=Borrar]').hover(function(){
		$(this).find(".btn_borrar_imagen").css("display","block");
	}, function(){
		$(this).find(".btn_borrar_imagen").css("display","none");
	});
});


function delete_img(val_id,val_tabla, val_campo) {
	alertify.log("Eliminando imagen");
	$.post('action/delete_img.php', {id: val_id,tabla: val_tabla, campo: val_campo}, function(data) {
			$(".fila_imagen_" + val_campo).fadeOut(200);
			alertify.success("Imagen eliminada con éxito");
			$('.galery_wrapper').empty();
			$("."+val_campo).fadeIn(100);
	});
}

function reload_img_content(val_tabla, val_id, val_op, op) {
	//Cargador de ajax
	$("#loader_consulta").ajaxStart(function(){
		$("#loader_consulta").fadeIn(200);
	});
	
	$("#loader_consulta").ajaxStop(function(){
		$(this).fadeOut(200);
	});
	
	$.post('action/delete_img_gal.php', {id: val_id, tabla: val_tabla, val_op: val_op}, function(data) {
			$('#imagenes').empty();
			$('#imagenes').html(data);
	});
}

function delete_file(val_id, val_tabla, val_campo, val_archivo)
{
	//Cargador de ajax
	$("#loader_consulta").ajaxStart(function(){
		$("#loader_consulta").fadeIn(200);
	});
	
	$("#loader_consulta").ajaxStop(function(){
		$(this).fadeOut(200);
	});
	
	$.post('action/delete_file.php', {id: val_id, tabla: val_tabla, campo: val_campo, archivo: val_archivo}, function(data) {
			$('#file_name_for').empty();
			$('#file_name_for').html(data);
	});
}


function add_fields(id) {
	$('#nested_fields_'+id).val(1);
	$('#has_many_fields_'+id).show(500);
	$('#new_entry_'+id).hide();
}
function cerrar_bienvenida(){
	$(".lightbox").fadeOut(400);
}

$(function(){
	$('a.img_galeria').hover(function(){
		$(this).find(".eliminar_galeria").css("display","block");
	}, function(){
		$(this).find(".eliminar_galeria").css("display","none");
	});
});
