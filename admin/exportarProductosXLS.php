<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');
ini_set('max_execution_time', 0);
ini_set('memory_limit', '600M');
include("conf.php");

require_once 'PHPExcel-1.8/Classes/PHPExcel.php';
require_once 'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$productos = consulta_bd("id, codigo_producto_madre, nombre, marca_id, thumbs, fecha_creacion, fecha_modificacion, api, nombre_api, sku_api, descripcion_breve, descripcion, video","productos","","");
//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'SKU');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Nombre');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Marca ID');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Thumbs');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Fecha creacion');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Fecha modificacion');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Webservice');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Nombre Proveedor');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'SKU Proveedor');
$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Descripcion Breve');
$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Descripcion');
$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Video');

for($i=0; $i<sizeof($productos); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $productos[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $productos[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $productos[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $productos[$i][3]);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$linea, $productos[$i][4]);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$linea, $productos[$i][5]);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$linea, $productos[$i][6]);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$linea, $productos[$i][7]);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$linea, $productos[$i][8]);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$linea, $productos[$i][9]);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$linea, $productos[$i][10]);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$linea, $productos[$i][11]);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$linea, $productos[$i][12]);
}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Productos');






// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();
// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(1);


//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Producto_id');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ID Color');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Nombre');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'SKU');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Webservice');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Nombre Proveedor');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'SKU Proveedor');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Precio');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Fecha creacion');
$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Fecha modificacion');
$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Imagen 1');
$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Imagen 2');
$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Imagen 3');
$objPHPExcel->getActiveSheet()->setCellValue('O1', 'Imagen 4');
$objPHPExcel->getActiveSheet()->setCellValue('P1', 'Imagen 5');

$PD = consulta_bd("id, producto_id, color_producto_id, nombre, sku, api, nombre_api, sku_api, precio, fecha_creacion, fecha_modificacion, imagen1, imagen2, imagen3, imagen4, imagen5","productos_detalles","","");
for($i=0; $i<sizeof($PD); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $PD[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $PD[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $PD[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $PD[$i][3]);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$linea, $PD[$i][4]);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$linea, $PD[$i][5]);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$linea, $PD[$i][6]);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$linea, $PD[$i][7]);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$linea, $PD[$i][8]);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$linea, $PD[$i][9]);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$linea, $PD[$i][10]);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$linea, $PD[$i][11]);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$linea, $PD[$i][12]);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$linea, $PD[$i][13]);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$linea, $PD[$i][14]);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$linea, $PD[$i][15]);
}


// Rename 2nd sheet
$objPHPExcel->getActiveSheet()->setTitle('Productos detalles');





// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="productos.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>
