<?php

/********************************************************\
|  Moldeable CMS - Instalador.		            		 |
|  Fecha Modificación: 24/09/2012		                 |
|  Todos los derechos reservados © Moldeable S.A. 2012   |
|  Prohibida su copia parcial o total  			         |
|  http://www.moldeable.com/                             |
\********************************************************/
	
include('conf.php');


$die_start = "<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Error de instalaci&oacute;n:</span> <br /><br />";
$die_end = "</p>";

$c1 = "ALTER TABLE clientes_direcciones ENGINE=InnoDB;";
$c2 = "ALTER TABLE clientes ENGINE=InnoDB;";
$c3 = "ALTER TABLE regiones ENGINE=InnoDB;";
$c4 = "ALTER TABLE ciudades ENGINE=InnoDB;";
$c5 = "ALTER TABLE comunas ENGINE=InnoDB;";
$c6 = "ALTER TABLE lineas ENGINE=InnoDB;";
$c7 = "ALTER TABLE lineas_productos ENGINE=InnoDB;";
$c8 = "ALTER TABLE categorias ENGINE=InnoDB;";
$c9 = "ALTER TABLE subcategorias ENGINE=InnoDB;";
$c10 = "ALTER TABLE productos ENGINE=InnoDB;";
$c11 = "ALTER TABLE productos_detalles ENGINE=InnoDB;";
$c12 = "ALTER TABLE productos_pedidos ENGINE=InnoDB;";
$c13 = "ALTER TABLE pedidos ENGINE=InnoDB;";
$c14 = "ALTER TABLE cotizaciones ENGINE=InnoDB;";
$c15 = "ALTER TABLE productos_cotizaciones ENGINE=InnoDB;";


$run_c1 = mysqli_query($conexion, $c1);
if (!$run_c1){
	die($die_start."Error al cambiar formato tabla en c1: ".mysqli_error($conexion)."$die_end");
}

$run_c2 = mysqli_query($conexion, $c2);
if (!$run_c2){
	die($die_start."Error al cambiar formato tabla en c2: ".mysqli_error($conexion)."$die_end");
}

$run_c3 = mysqli_query($conexion, $c3);
if (!$run_c3){
	die($die_start."Error al cambiar formato tabla en c3: ".mysqli_error($conexion)."$die_end");
}

$run_c4 = mysqli_query($conexion, $c4);
if (!$run_c4){
	die($die_start."Error al cambiar formato tabla en c4: ".mysqli_error($conexion)."$die_end");
}

$run_c5 = mysqli_query($conexion, $c5);
if (!$run_c5){
	die($die_start."Error al cambiar formato tabla en c5: ".mysqli_error($conexion)."$die_end");
}

$run_c6 = mysqli_query($conexion, $c6);
if (!$run_c6){
	die($die_start."Error al cambiar formato tabla en c6: ".mysqli_error($conexion)."$die_end");
}

$run_c7 = mysqli_query($conexion, $c7);
if (!$run_c7){
	die($die_start."Error al cambiar formato tabla en c7: ".mysqli_error($conexion)."$die_end");
}

$run_c8 = mysqli_query($conexion, $c8);
if (!$run_c8){
	die($die_start."Error al cambiar formato tabla en c8: ".mysqli_error($conexion)."$die_end");
}

$run_c9 = mysqli_query($conexion, $c9);
if (!$run_c9){
	die($die_start."Error al cambiar formato tabla en c9: ".mysqli_error($conexion)."$die_end");
}

$run_c10 = mysqli_query($conexion, $c10);
if (!$run_c10){
	die($die_start."Error al cambiar formato tabla en c10: ".mysqli_error($conexion)."$die_end");
}

$run_c11 = mysqli_query($conexion, $c11);
if (!$run_c11){
	die($die_start."Error al cambiar formato tabla en c11: ".mysqli_error($conexion)."$die_end");
}

$run_c12 = mysqli_query($conexion, $c12);
if (!$run_c12){
	die($die_start."Error al cambiar formato tabla en c12: ".mysqli_error($conexion)."$die_end");
}

$run_c13 = mysqli_query($conexion, $c13);
if (!$run_c13){
	die($die_start."Error al cambiar formato tabla en c13: ".mysqli_error($conexion)."$die_end");
}

$run_c14 = mysqli_query($conexion, $c14);
if (!$run_c14){
	die($die_start."Error al cambiar formato tabla en c14: ".mysqli_error($conexion)."$die_end");
}

$run_c15 = mysqli_query($conexion, $c15);
if (!$run_c15){
	die($die_start."Error al cambiar formato tabla en c15: ".mysqli_error($conexion)."$die_end");
}







$r1 = 'ALTER TABLE clientes_direcciones
ADD FOREIGN KEY (cliente_id) REFERENCES clientes(id);';
$run_r1 = mysqli_query($conexion, $r1);
if (!$run_r1){
	die($die_start."Error al generar relaciones en R1: ".mysqli_error($conexion)."$die_end");
}



$r2 = 'ALTER TABLE clientes_direcciones
ADD FOREIGN KEY (region_id) REFERENCES regiones(id);';
$run_r2 = mysqli_query($conexion, $r2);
if (!$run_r2){
	die($die_start."Error al generar relaciones en R2: ".mysqli_error($conexion)."$die_end");
}


$r4 = 'ALTER TABLE clientes_direcciones
ADD FOREIGN KEY (ciudad_id) REFERENCES ciudades(id);';
$run_r4 = mysqli_query($conexion, $r4);
if (!$run_r4){
	die($die_start."Error al generar relaciones en R4: ".mysqli_error($conexion)."$die_end");
}


$r5 = 'ALTER TABLE clientes_direcciones
ADD FOREIGN KEY (comuna_id) REFERENCES comunas(id);';
$run_r5 = mysqli_query($conexion, $r5);
if (!$run_r5){
	die($die_start."Error al generar relaciones en R5: ".mysqli_error($conexion)."$die_end");
}


$r6 = 'ALTER TABLE lineas_productos
ADD FOREIGN KEY (linea_id) REFERENCES lineas(id);';
$run_r6 = mysqli_query($conexion, $r6);
if (!$run_r6){
	die($die_start."Error al generar relaciones en R6: ".mysqli_error($conexion)."$die_end");
}

$r7 = 'ALTER TABLE lineas_productos
ADD FOREIGN KEY (categoria_id) REFERENCES categorias(id);';
$run_r7 = mysqli_query($conexion, $r7);
if (!$run_r7){
	die($die_start."Error al generar relaciones en R7: ".mysqli_error($conexion)."$die_end");
}




$r8 = 'ALTER TABLE lineas_productos
ADD FOREIGN KEY (subcategoria_id) REFERENCES subcategorias(id);';
$run_r8 = mysqli_query($conexion, $r8);
if (!$run_r8){
	die($die_start."Error al generar relaciones en R8: ".mysqli_error($conexion)."$die_end");
}

$r9 = 'ALTER TABLE lineas_productos
ADD FOREIGN KEY (`producto_id`) REFERENCES `productos`(id);';
$run_r9 = mysqli_query($conexion, $r9);
if (!$run_r9){
	die($die_start."Error al generar relaciones en R9: ".mysqli_error($conexion)."$die_end");
}




$r10 = 'ALTER TABLE productos_detalles
ADD FOREIGN KEY (producto_id) REFERENCES productos(id);';
$run_r10 = mysqli_query($conexion, $r10);
if (!$run_r10){
	die($die_start."Error al generar relaciones en R10: ".mysqli_error($conexion)."$die_end");
}



$r11 = 'ALTER TABLE productos_pedidos
ADD FOREIGN KEY (productos_detalle_id) REFERENCES productos_detalles(id);';
$run_r11 = mysqli_query($conexion, $r11);
if (!$run_r11){
	die($die_start."Error al generar relaciones en R11: ".mysqli_error($conexion)."$die_end");
}

$r12 = 'ALTER TABLE productos_pedidos
ADD FOREIGN KEY (pedido_id) REFERENCES pedidos(id);';
$run_r12 = mysqli_query($conexion, $r12);
if (!$run_r12){
	die($die_start."Error al generar relaciones en R12: ".mysqli_error($conexion)."$die_end");
}


$r13 = 'ALTER TABLE productos_cotizaciones
ADD FOREIGN KEY (productos_detalle_id) REFERENCES productos_detalles(id);';
$run_r13 = mysqli_query($conexion, $r13);
if (!$run_r13){
	die($die_start."Error al generar relaciones en R13: ".mysqli_error($conexion)."$die_end");
}


$r14 = 'ALTER TABLE productos_cotizaciones
ADD FOREIGN KEY (cotizacion_id) REFERENCES cotizaciones(id);';
$run_r14 = mysqli_query($conexion, $r14);
if (!$run_r14){
	die($die_start."Error al generar relaciones en R14: ".mysqli_error($conexion)."$die_end");
}



$r15 = 'ALTER TABLE categorias
ADD FOREIGN KEY (linea_id) REFERENCES lineas(id);';
$run_r15 = mysqli_query($conexion, $r15);
if (!$run_r15){
	die($die_start."Error al generar relaciones en R15: ".mysqli_error($conexion)."$die_end");
}




$r16 = 'ALTER TABLE subcategorias
ADD FOREIGN KEY (categoria_id) REFERENCES categorias(id);';
$run_r16 = mysqli_query($conexion, $r16);
if (!$run_r16){
	die($die_start."Error al generar relaciones en R16: ".mysqli_error($conexion)."$die_end");
}





if (!$run_r1 OR !$run_r2 OR !$run_r4 OR !$run_r5 OR !$run_r6 OR !$run_r7 OR !$run_r8 OR !$run_r9 OR !$run_r10 OR !$run_r11 OR !!$run_r12 OR !$run_r13 OR !$run_r14 OR !$run_r15 OR !$run_r16){
	die($die_start."Por favor revise que la base de datos est&eacute; bien configurada".$die_end);
} else {
	echo "<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Success!!</span> <br /><br />Instalaci&oacute;n correcta, por favor elimine este archivo y cambie los permisos de escritura en el directorio ra&iacute;z.<br /><p style='font-family: verdana;'>Haga clic <a href='adlogin.php'>aqu&iacute;</a> para ir al panel de administraci&oacute;n.</p>";
}

?>