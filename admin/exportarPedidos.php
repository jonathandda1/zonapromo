<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');
include("conf.php");

require_once 'PHPExcel-1.8/Classes/PHPExcel.php';
require_once 'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$productos = consulta_bd("id, oc, medio_de_pago, nombre, rut, email, telefono, fecha, retiro_en_tienda, region, comuna, localidad, direccion, codigo_descuento, descuento, total, valor_despacho, total_pagado, cant_productos, giro, razon_social, direccion_factura, rut_factura, email_factura, authorization_code, mp_auth_code","pedidos","","id desc");
//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'OC');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Medio de pago');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Nombre');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'rut');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'email');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'telefono');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Fecha');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Retiro en tienda');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Region');
$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Comuna');
$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Localidad');
$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Direccion');
$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Codigo de descuento');
$objPHPExcel->getActiveSheet()->setCellValue('O1', 'Descuento');
$objPHPExcel->getActiveSheet()->setCellValue('P1', 'Total');
$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Valor despacho');
$objPHPExcel->getActiveSheet()->setCellValue('R1', 'Total pagado');
$objPHPExcel->getActiveSheet()->setCellValue('S1', 'Cantidad productos');
$objPHPExcel->getActiveSheet()->setCellValue('T1', 'Giro');
$objPHPExcel->getActiveSheet()->setCellValue('U1', 'Razon social');
$objPHPExcel->getActiveSheet()->setCellValue('V1', 'Direccion factura');
$objPHPExcel->getActiveSheet()->setCellValue('W1', 'Rut factura');
$objPHPExcel->getActiveSheet()->setCellValue('X1', 'Email factura');
$objPHPExcel->getActiveSheet()->setCellValue('Y1', 'Codigo autorizacion transbank');
$objPHPExcel->getActiveSheet()->setCellValue('Z1', 'Codigo autorizacion Mercado pago');

for($i=0; $i<sizeof($productos); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $productos[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $productos[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $productos[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $productos[$i][3]);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$linea, $productos[$i][4]);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$linea, $productos[$i][5]);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$linea, $productos[$i][6]);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$linea, $productos[$i][7]);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$linea, $productos[$i][8]);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$linea, $productos[$i][9]);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$linea, $productos[$i][10]);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$linea, $productos[$i][11]);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$linea, $productos[$i][12]);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$linea, $productos[$i][13]);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$linea, $productos[$i][14]);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$linea, $productos[$i][15]);
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$linea, $productos[$i][16]);
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$linea, $productos[$i][17]);
	$objPHPExcel->getActiveSheet()->setCellValue('S'.$linea, $productos[$i][18]);
	$objPHPExcel->getActiveSheet()->setCellValue('T'.$linea, $productos[$i][19]);
	$objPHPExcel->getActiveSheet()->setCellValue('U'.$linea, $productos[$i][20]);
	$objPHPExcel->getActiveSheet()->setCellValue('V'.$linea, $productos[$i][21]);
	$objPHPExcel->getActiveSheet()->setCellValue('W'.$linea, $productos[$i][22]);
	$objPHPExcel->getActiveSheet()->setCellValue('X'.$linea, $productos[$i][23]);
	$objPHPExcel->getActiveSheet()->setCellValue('Y'.$linea, $productos[$i][24]);
	$objPHPExcel->getActiveSheet()->setCellValue('Z'.$linea, $productos[$i][25]);
}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Pedidos');






// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();
// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(1);


//filas A son encabezados
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Pedido_id');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Productos_detale_id');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Cantidad');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Precio Unitario');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Descuento');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Precio total');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Codigo');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Codigo pack');

$PP = consulta_bd("id, pedido_id, productos_detalle_id, cantidad, precio_unitario, descuento, precio_total, codigo, codigo_pack","productos_pedidos","","");
for($i=0; $i<sizeof($productos); $i++) {
	$linea = $i + 2;
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$linea, $PP[$i][0]);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$linea, $PP[$i][1]);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$linea, $PP[$i][2]);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$linea, $PP[$i][3]);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$linea, $PP[$i][4]);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$linea, $PP[$i][5]);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$linea, $PP[$i][6]);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$linea, $PP[$i][7]);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$linea, $PP[$i][8]);
}


// Rename 2nd sheet
$objPHPExcel->getActiveSheet()->setTitle('Productos pedidos');





// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Pedidos.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>
