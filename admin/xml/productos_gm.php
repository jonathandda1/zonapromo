<?php
	include("../conf.php");
    //include("../admin/includes/tienda/cart/inc/functions.inc.php");
	header('Content-type: text/xml');
	//header("Content-Type: text/plain");

	$url_sitio 				= get_option('url_sitio');
	$tienda_facebook 		= get_option('tienda_facebook');
	$nombre_setFrom_mail	= get_option('nombre_setFrom_mail');
	$categoria_google 		= get_option('categoria_google');

	if($tienda_facebook){

		$select 	= "pd.id, pd.nombre, p.marca_id, pd.sku, p.descripcion";
		$from 		= "productos p, productos_detalles pd";
		$where 		= "p.id = pd.producto_id AND p.publicado = 1 AND pd.publicado = 1 GROUP BY pd.id";

		$productos 	= consulta_bd($select,$from,$where,"");

		echo '<?xml version="1.0"?>';
		echo '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">';
			echo '<channel>';
				echo '<title>'.$nombre_setFrom_mail.'</title>';
				echo '<link>'.$url_sitio.'</link>';
				echo '<description></description>';
                

				foreach($productos as $pr){
					$imagen 		= getIMGPrd($pr[0]);
					$description 	= strip_tags($pr[4]);

					if($imagen){
						echo '<item>';
							echo '<g:id>'.$pr[0].'</g:id>';
							echo '<g:availability>in stock</g:availability>';
							echo '<g:condition>new</g:condition>';
							echo '<g:description>'.$description.'</g:description>';
							echo '<g:image_link>'.$imagen.'</g:image_link>';
							echo '<g:link>'.$url_sitio.'/ficha/'.$pr[0].'/'.url_amigables($pr[1]).'</g:link>';
							echo '<g:title>'.$pr[1].'</g:title>';
							echo '<g:price>'.round(getPrecioNormal($pr[0])).' CLP</g:price>';
							echo '<g:sale_price>'.round(getPrecio($pr[0])).' CLP</g:sale_price>';
							echo '<g:mpn>'.$pr[3].'</g:mpn>';
							echo '<g:brand>'.getMarca($pr[2]).'</g:brand>';
							echo '<g:google_product_category>'.$categoria_google.'</g:google_product_category>';
						echo '</item>';
					}
				}


			echo '</channel>';
		echo '</rss>';

	}


	function getIMGPrd($pd){
		$url_sitio 	= get_option('url_sitio');
		$img 		= consulta_bd("p.thumbs","productos_detalles pd, productos p","p.id = pd.producto_id AND pd.id = $pd","");
		$src 		= $url_sitio.'/imagenes/productos/'.$img[0][0];

		if (getimagesize($src)){
			$return = $src;
		}else{
			$return = false;
		}
		return $return;
	}


	function getPrecio($pd){
		$row = consulta_bd("(select valor_bruto from listas_productos where lista_id = ".listaGeneral()." and productos_detalle_id = pd.id) as lista_normal, (select valor_bruto from listas_productos where lista_id = ".listaOferta()." and productos_detalle_id = pd.id) as lista_descuento","productos_detalles pd","pd.id = $pd","");
		if(hasDescuento($pd)){
			//$precio = ((100 - (double)$row[0][1]) / 100) * (int)$row[0][0];
            $precio = (int)$row[0][1];
		}else{
			$precio = (int)$row[0][0];
		}
		return $precio;//*1.19;
	}
/*
function getPrecio($pd){
		$row = consulta_bd("precio, descuento","productos_detalles","id = $pd","");
		if(hasDescuento($pd)){
			$precio = ((100 - (double)$row[0][1]) / 100) * (int)$row[0][0];
		}else{
			$precio = (int)$row[0][0];
		}
		return $precio*1.19;
	}
*/



function listaGeneral(){
   return 3;  
}

function listaOferta(){
    return 12;
}
function listaCyber(){
    return 13;
}

	function getMarca($id){
		$row = consulta_bd("nombre","marcas","id = $id","");
		return $row[0][0];
	}
	function hasDescuento($pd){
		$row = consulta_bd("(select valor_bruto from listas_productos where lista_id = ".listaOferta()." and productos_detalle_id = pd.id) as lista_descuento","productos_detalles pd","pd.id = $pd","");
		if((int)$row[0][0] > 0) return true;
		else return false;
	}
	function getPrecioNormal($pd){
		$row = consulta_bd("(select valor_bruto from listas_productos where lista_id = ".listaGeneral()." and productos_detalle_id = pd.id) as lista_normal","productos_detalles pd","pd.id = $pd","");
		return $row[0][0];//*1.19;
	}
?>