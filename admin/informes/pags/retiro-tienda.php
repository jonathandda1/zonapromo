<?php
include('../conex.php');
	require_once "../functions.php";
	$tienda = $_GET['tienda'];
	$service;
	$aContar;
            $serv1; $serv2; $serv3; $serv4;
	
    $select = "SELECT nombre, servicio FROM tienda WHERE url='$tienda'";
    $query = mysqli_query($conexion, $select);

    $result = mysqli_fetch_assoc($query);

    $tiendaName = $result["nombre"];

if ($tienda == 'todas'):

    $ventas = array('retiro' => array(0,0,0),
                              'despacho' => array(0,0,0));

    $productos = array('retiro' => array(0,0,0),
                              'despacho' => array(0,0,0));

    $pedidos =array('retiro' => array(0,0,0),
                              'despacho' => array(0,0,0));

    $totales = array('ped' => array(0,0,0,0,0,0), 
                               'prod' => array(0,0,0,0,0,0), 
                               'ventas' => array(0,0,0,0,0,0)
                        );
    $queryT = "SELECT nombre, servicio FROM tienda";
    $resultT = mysqli_query($conexion, $queryT);

    if (mysqli_num_rows($resultT) > 0):
            while ($row = mysqli_fetch_assoc($resultT)) {
                $auxPed = PedidosRetiroTienda($row["servicio"]);
                $auxProd = cantProductosRetiroTienda($row["servicio"]);
                $auxVen = VentasRetiroTienda($row["servicio"]);

                $auxVenT = totalVentas($row["servicio"]);
                $auxProdT = countProductos($row["servicio"]);
                $auxPedT = countPedidos($row["servicio"]);

                for($i = 0; $i < 12; $i++):
                    $ventas['retiro'][$i] += $auxVen['retiro'][$i];
                    $productos['retiro'][$i] += $auxProd['retiro'][$i];
                    $pedidos['retiro'][$i] += $auxPed['retiro'][$i];
                endfor;

                for($i = 0; $i < 12; $i++):
                    $ventas['despacho'][$i] += $auxVen['despacho'][$i];
                    $productos['despacho'][$i] += $auxProd['despacho'][$i];
                    $pedidos['despacho'][$i] += $auxPed['despacho'][$i];
                endfor;

                $totales['ped'][0] += $auxPed['t2016'][0];
                $totales['ped'][1] += $auxPed['t2017'][0];
                $totales['ped'][2] += $auxPed['t2018'][0];
                $totales['ped'][3] += $auxPedT['t2016'][0];
                $totales['ped'][4] += $auxPedT['t2017'][0];
                $totales['ped'][5] += $auxPedT['t2018'][0];

                $totales['prod'][0] += $auxProd['t2016'][0];
                $totales['prod'][1] += $auxProd['t2017'][0];
                $totales['prod'][2] += $auxProd['t2018'][0];
                $totales['prod'][3] += $auxProdT['t2016'][0];
                $totales['prod'][4] += $auxProdT['t2017'][0];
                $totales['prod'][5] += $auxProdT['t2018'][0];

                $totales['ventas'][0] += $auxVen['t2016'][0];
                $totales['ventas'][1] += $auxVen['t2017'][0];
                $totales['ventas'][2] += $auxVen['t2018'][0];
                $totales['ventas'][3] += $auxVenT['t2016'][0];
                $totales['ventas'][4] += $auxVenT['t2017'][0];
                $totales['ventas'][5] += $auxVenT['t2018'][0];
            };
            endif;
        $tiendaName = 'Todas';  
    // Variables que se ocupan para sacar las ventas segun retiro o despacho y también poder sacar el porcentaje
    /*$vAmoble = VentasRetiroTienda($serv1);
    $vForastero = ventasRetiroTienda($serv2);
    $vJardin = VentasRetiroTienda($serv3);
    $vHbt = ventasRetiroTienda($serv4);

    $vTotalesAmoble = totalVentas($serv1);
    $vTotalesForastero = totalVentas($serv2);
    $vTotalesJardin = totalVentas($serv3);
    $vTotalesHbt = totalVentas($serv4) ;

    // Variables que se ocupan para los pedidos por retiro y despacho, además de sacar el porcentaje
    $pedAmoble = PedidosRetiroTienda($serv1);
    $pedForastero = PedidosRetiroTienda($serv2);
    $pedJardin = PedidosRetiroTienda($serv3);
    $pedHbt = PedidosRetiroTienda($serv4);

    $pedTotalesAmoble = countPedidos($serv1);
    $pedTotalesForastero = countPedidos($serv2);
    $pedTotalesJardin = countPedidos($serv3);
    $pedTotalesHbt = countPedidos($serv4);

    //
    $prodAmoble = cantProductosRetiroTienda($serv1);
    $prodForastero = cantProductosRetiroTienda($serv2);
    $prodJardin = cantProductosRetiroTienda($serv3);
    $prodHbt = cantProductosRetiroTienda($serv4);

    $prodTotalesAmoble = countProductos($serv1);
    $prodTotalesForastero = countProductos($serv2);
    $prodTotalesJardin = countProductos($serv3);
    $prodTotalesHbt = countProductos($serv4);*/
else:
    $totalVentas = ventasRetiroTienda($result["servicio"]);
    $totalPedidos = PedidosRetiroTienda($result["servicio"]);
    $totalProductos = cantProductosRetiroTienda($result["servicio"]);

    $ventasTotales = totalVentas($result["servicio"]);
    $pedidosTotales = countPedidos($result["servicio"]);
    $prodTotales = countProductos($result["servicio"]);
endif;	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tienda <?php echo $tiendaName; ?> - Retiro en Tienda</title>
	<script src="../../js/jquery-1.8.0.js"></script>
	<script src="../../js/highcharts.js"></script>
	<script src="../../js/exporting.js"></script>

	<script type="text/javascript" src="../../js/jquery.uniform.js"></script>

	<link rel="stylesheet" href="../../css/themes/aristo/css/uniform.aristo.css" type="text/css" media="screen" charset="utf-8" />

	<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
    <link href="../../css/font-awesome.css" rel="stylesheet" type="text/css" />


	<script type="text/javascript">
	    

// Create the chart
$(function () {
        var ventas = new Highcharts.Chart({
            chart: {
                type: 'column',
                backgroundColor: '#FAFAFA',
                renderTo: 'container'
            },
            title: {
                text: 'Valor Ventas Retiro en tienda y Despacho'
            },
            xAxis: {
                categories: ['2016', '2017', '2018']

            },
            yAxis: {
                title: {
                    text: 'Total por mes'
                }
            },
            series: [{
                name: 'Retiro Tienda',
                data: [
                    <?php 
                        if($tienda == 'todas'):
                            for($i = 0; $i < 3; $i++):
                                $vTodasRetiro = $ventas['retiro'][$i];
                                echo "{$vTodasRetiro} ,";
                            endfor;
                        else:
                            for($i = 0; $i < 3; $i++):
                                echo "{$totalVentas['retiro'][$i]} ,";
                            endfor;
                        endif;
                    ?>
                ],
                tooltip: {
                    valuePrefix: '$ '
                }
                }, {
                name: 'Despacho',
                data: [
                    <?php 
                        if($tienda == 'todas'):
                            for($i = 0; $i < 3; $i++):
                                $vTodasDespacho = $ventas['despacho'][$i];
                                echo "{$vTodasDespacho} ,";
                            endfor;
                        else:
                            for($i = 0; $i < 3; $i++):
                                echo "{$totalVentas['despacho'][$i]} ,";
                            endfor;
                        endif;
                    ?>
                ],
                tooltip: {
                    valuePrefix: '$ '
                }
            }
            ]
        });

    
    });

$(function () {
         var pedidos = new Highcharts.Chart({
            chart: {
                type: 'column',
                backgroundColor: '#FAFAFA',
                renderTo: 'torta-pedidos'
            },
            title: {
                text: 'Pedidos Retiro en tienda y Despacho'
            },
            xAxis: {
                categories: ['2016', '2017', '2018']

            },
            yAxis: {
                title: {
                    text: 'Total por mes'
                }
            },
            series: [{
                name: 'Retiro Tienda',
                data: [
                    <?php 
                        if($tienda == 'todas'):
                            for($i = 0; $i < 3; $i++):
                                $pedTodasRetiro = $pedidos['retiro'][$i];
                                echo "{$pedTodasRetiro} ,";
                            endfor;
                        else:
                            for($i = 0; $i < 3; $i++):
                                echo "{$totalPedidos['retiro'][$i]} ,";
                            endfor;
                        endif;    
                     ?>
                ]
                }, {
                name: 'Despacho',
                data: [
                    <?php 
                        if($tienda == 'todas'):
                            for($i = 0; $i < 3; $i++):
                                $pedTodasRetiro = $pedidos['despacho'][$i];
                                echo "{$pedTodasRetiro} ,";
                            endfor;
                        else:
                            for($i = 0; $i < 3; $i++):
                                echo "{$totalPedidos['despacho'][$i]} ,";
                            endfor;
                        endif;    
                     ?>
                ]}
            ]
        });

    
    });
$(function () {
         var productos = new Highcharts.Chart({
            chart: {
                type: 'column',
                backgroundColor: '#FAFAFA',
                renderTo: 'torta-productos'
            },
            title: {
                text: 'Cantidad Productos Retiro en tienda y Despacho'
            },
            xAxis: {
                categories: ['2016', '2017', '2018' ]

            },
            yAxis: {
                title: {
                    text: 'Total por mes'
                }
            },
            series: [{
                name: 'Retiro Tienda',
                data: [
                    <?php 
                        if($tienda == 'todas'):
                            for($i = 0; $i < 3; $i++):
                                $prodTodasRetiro = $productos['retiro'][$i];
                                echo "{$prodTodasRetiro} ,";
                            endfor;
                        else:
                            for($i = 0; $i < 3; $i++):
                                echo "{$totalProductos['retiro'][$i]} ,";
                            endfor;
                        endif;                                           
                     ?>
                ]
                }, {
                name: 'Despacho',
                data: [
                    <?php 
                        if($tienda == 'todas'):
                            for($i = 0; $i < 3; $i++):
                                $prodTodasDespacho = $productos['despacho'][$i];
                                echo "{$prodTodasDespacho} ,";
                            endfor;
                        else:
                            for($i = 0; $i < 3; $i++):
                                echo "{$totalProductos['despacho'][$i]} ,";
                            endfor;
                        endif;                                           
                     ?>
                ]}
            ]
        });

    
    });
</script>
</head>
<body>
    <div class="flecha"><a href="home"><i class="fa fa-reply fa-3x" aria-hidden="true"></i></a></div>
	<div class="contenedor" style="margin-top:20px;">
        <?php include('../index.php'); ?>
        
        <div class="total-por-anio">
        	<h1>Total Ventas</h1>
        	<h2>Retito en Tienda</h2>
        	<div class="box">
                <p id="anio">Año</p> <p id="porc">% vs Total Ventas</p>
                <div class="clearfix"></div>
                <div class="colum">
                    <p>2016</p>
                    <p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i> 
                        <?php
                            if($tienda == 'todas'):
                                $v2016 = $totales['ventas'][0];
                                echo number_format($v2016);
                            else:
                                echo number_format($totalVentas['t2016'][0]);
                            endif;
                        ?>
                    </p>
                </div>
                <div class="colum">
                    <?php
                    if (isset($ventasTotales)):
                         if ($ventasTotales['t2016'][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($totalVentas['t2016'][0] * 100) / $ventasTotales['t2016'][0], 2) . '%</p>';
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    else:
                        $total2016 = $totales['ventas'][3];
                        if ($total2016 > 0) :
                                $promVe2016 = ($totales['ventas'][0] * 100) / $total2016;
                                echo '<p class="number porcentaje">' . round($promVe2016, 2) . '%</p>';
                            
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    endif;
                    ?>
                    
                </div>
        	</div>

        	<div class="box">
                <div class="colum">
                    <p>2017</p>
                    <p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i> 
                        <?php
                            if($tienda == 'todas'):
                                $v2017 = $totales['ventas'][1];
                                echo number_format($v2017);
                            else:
                                echo number_format($totalVentas['t2017'][0]);
                            endif;
                        ?>
                    </p>
                </div>
                <div class="colum">
                    <?php
                    if (isset($ventasTotales)):
                         if ($ventasTotales['t2017'][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($totalVentas['t2017'][0] * 100) / $ventasTotales['t2017'][0], 2) . '%</p>';
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    else:
                        $total2017 = $totales['ventas'][4];
                        if ($total2016 > 0) :
                                $promVe2017 = ($totales['ventas'][1] * 100) / $total2017;
                                echo '<p class="number porcentaje">' . round($promVe2017, 2) . '%</p>';
                            
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    endif;
                    ?>
                </div>
        	</div>

        	<div class="box">
                <div class="colum">
                    <p>2018</p>
                    <p class="number"><i class="fa fa-usd totales" aria-hidden="true"></i> 
                        <?php
                            if($tienda == 'todas'):
                                $v2018 = $totales['ventas'][2];
                                echo number_format($v2018);
                            else:
                                echo number_format($totalVentas['t2018'][0]);
                            endif;
                        ?>
                    </p>
                </div>
                <div class="colum">
                    <?php
                    if (isset($ventasTotales)):
                         if ($ventasTotales['t2018'][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($totalVentas['t2018'][0] * 100) / $ventasTotales['t2018'][0], 2) . '%</p>';
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    else:
                        $total2018 = $totales['ventas'][5];
                        if ($total2018 > 0) :
                                $promVe2018 = ($totales['ventas'][2] * 100) / $total2018;
                                echo '<p class="number porcentaje">' . round($promVe2018, 2) . '%</p>';
                            
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    endif;
                    ?>
                </div>
        	</div>
        </div>
        <div class="div-g">
            <div id="container"></div>
        </div>

        <div class="separator"></div>

        <div class="total-por-anio">
            <h1>Total Pedidos</h1>
            <h2>Retito en Tienda</h2>
            <div class="box">
                <p id="anio">Año</p> <p id="porc">% vs Total Pedidos</p>
                <div class="clearfix"></div>
                <div class="colum">
                    <p>2016</p>
                    <p class="number">
                        <?php
                            if($tienda == 'todas'):
                                $ped2016 = $totales['ped'][0];
                                echo number_format($ped2016);
                            else:
                                echo number_format($totalPedidos['t2016'][0]);
                            endif;
                        ?>
                    </p>
                </div>
                <div class="colum">
                    <?php
                    if (isset($pedidosTotales)):
                         if ($pedidosTotales['t2016'][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($totalPedidos['t2016'][0] * 100) / $pedidosTotales['t2016'][0], 2) . '%</p>';
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    else:
                        $pedTotal2016 = $totales['ped'][3];
                        if ($pedTotal2016 > 0) :
                                $promPed2016 = ($totales['ped'][0] * 100) / $pedTotal2016;
                                echo '<p class="number porcentaje">' . round($promPed2016, 2) . '%</p>';
                            
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    endif;
                    ?>
                </div>
            </div>

            <div class="box">
                <div class="colum">
                    <p>2017</p>
                    <p class="number">
                        <?php
                            if($tienda == 'todas'):
                                $ped2017 = $totales['ped'][1];
                                echo number_format($ped2017);
                            else:
                                echo number_format($totalPedidos['t2017'][0]);
                            endif;
                        ?>
                    </p>
                </div>
                <div class="colum">
                   <?php
                    if (isset($pedidosTotales)):
                         if ($pedidosTotales['t2017'][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($totalPedidos['t2017'][0] * 100) / $pedidosTotales['t2017'][0], 2) . '%</p>';
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    else:
                        $pedTotal2017 = $totales['ped'][4];
                        if ($pedTotal2017 > 0) :
                                $promPed2017 = ($totales['ped'][1] * 100) / $pedTotal2017;
                                echo '<p class="number porcentaje">' . round($promPed2017, 2) . '%</p>';
                            
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    endif;
                    ?>
                </div>
            </div>

            <div class="box">
                <div class="colum">
                    <p>2018</p>
                    <p class="number">
                        <?php
                            if($tienda == 'todas'):
                                $ped2018 = $totales['ped'][2];
                                echo number_format($ped2018);
                            else:
                                echo number_format($totalPedidos['t2018'][0]);
                            endif;
                        ?>
                    </p>
                </div>
                <div class="colum">
                   <?php
                    if (isset($pedidosTotales)):
                         if ($pedidosTotales['t2018'][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($totalPedidos['t2018'][0] * 100) / $pedidosTotales['t2018'][0], 2) . '%</p>';
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    else:
                        $pedTotal2018 = $totales['ped'][5];
                        if ($pedTotal2018 > 0) :
                                $promPed2018 = ($totales['ped'][2] * 100) / $pedTotal2018;
                                echo '<p class="number porcentaje">' . round($promPed2018, 2) . '%</p>';
                            
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="div-g">
            <div id="torta-pedidos"></div>
        </div>

        <div class="separator"></div>
        
        <!-- DIV PRODUCTOS -->
        <div class="total-por-anio">
            <h1>Total Productos</h1>
            <h2>Retito en Tienda</h2>
            <div class="box">
                <p id="anio">Año</p> <p id="porc">% vs Total Productos</p>
                <div class="clearfix"></div>
                <div class="colum">
                    <p>2016</p>
                    <p class="number">
                        <?php
                            if($tienda == 'todas'):
                                $prod2016 = $totales['prod'][0];
                                echo number_format($ped2016);
                            else:
                                echo number_format($totalProductos['t2016'][0]);
                            endif;
                        ?>
                    </p>
                </div>
                <div class="colum">
                   <?php
                    if (isset($prodTotales)):
                         if ($prodTotales['t2016'][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($totalProductos['t2016'][0] * 100) / $prodTotales['t2016'][0], 2) . '%</p>';
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    else:
                        $prodTotal2016 =$totales['prod'][3];
                        if ($prodTotal2016 > 0) :
                                $promProd2016 = ($totales['prod'][0] * 100) / $prodTotal2016;
                                echo '<p class="number porcentaje">' . round($promProd2016, 2) . '%</p>';
                            
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    endif;
                    ?>
                </div>
            </div>

            <div class="box">
                <div class="colum">
                    <p>2017</p>
                    <p class="number">
                        <?php
                            if($tienda == 'todas'):
                                $prod2017 = $totales['prod'][1];
                                echo number_format($prod2017);
                            else:
                                echo number_format($totalProductos['t2017'][0]);
                            endif;
                        ?>
                    </p>
                </div>
                <div class="colum">
                   <?php
                    if (isset($prodTotales)):
                         if ($prodTotales['t2017'][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($totalProductos['t2017'][0] * 100) / $prodTotales['t2017'][0], 2) . '%</p>';
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    else:
                        $prodTotal2017 = $totales['prod'][4];
                        if ($prodTotal2017 > 0) :
                                $promProd2017 = ($totales['prod'][1] * 100) / $prodTotal2017;
                                echo '<p class="number porcentaje">' . round($promProd2017, 2) . '%</p>';
                            
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    endif;
                    ?>
                </div>
            </div>

            <div class="box">
                <div class="colum">
                    <p>2018</p>
                    <p class="number">
                        <?php
                            if($tienda == 'todas'):
                                $prod2018 = $totales['prod'][2];
                                echo number_format($prod2018);
                            else:
                                echo number_format($totalProductos['t2018'][0]);
                            endif;
                        ?>
                    </p>
                </div>
                <div class="colum">
                   <?php
                    if (isset($prodTotales)):
                         if ($prodTotales['t2018'][0] > 0) :
                                echo '<p class="number porcentaje">' . round(($totalProductos['t2018'][0] * 100) / $prodTotales['t2018'][0], 2) . '%</p>';
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    else:
                        $prodTotal2018 = $totales['prod'][5];
                        if ($prodTotal2018 > 0) :
                                $promProd2018 = ($totales['prod'][2] * 100) / $prodTotal2018;
                                echo '<p class="number porcentaje">' . round($promProd2018, 2) . '%</p>';
                            
                        else:
                            echo '<p class="number porcentaje">0%</p>';
                        endif;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="div-g">
            <div id="torta-productos"></div>
        </div>
		
    </div>
</body>
</html>



