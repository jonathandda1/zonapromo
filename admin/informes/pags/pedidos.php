<?php
	include('../conex.php');
	require_once "../functions.php";
	$tienda = $_GET['tienda'];
	$service;
	$serv1; $serv2; $serv3; $serv4;

	$select = "SELECT nombre, servicio FROM tienda WHERE url='$tienda'";
	$query = mysqli_query($conexion, $select);

	$result = mysqli_fetch_assoc($query);

	// query todas
	
	if ($tienda == 'todas') :
		$cont2016 = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$cont2017 = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$cont2018 = array(0,0,0,0,0,0,0,0,0,0,0,0);

		$ventas2016 = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$ventas2017 = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$ventas2018 = array(0,0,0,0,0,0,0,0,0,0,0,0);

		$aux;
		$ventas;
		$totales = array(0,0,0,0,0,0);
		$queryT = "SELECT nombre, servicio FROM tienda";
		$resultT = mysqli_query($conexion, $queryT);

		if (mysqli_num_rows($resultT) > 0):
			while ($row = mysqli_fetch_assoc($resultT)) {
				$aux = countPedidos($row["servicio"]);
				$ventas = totalVentas($row["servicio"]);

				for($i = 0; $i < 12; $i++):
					$cont2016[$i] += $aux[2016][$i];
					$ventas2016[$i] += $ventas[2016][$i];
				endfor;

				for($i = 0; $i < 12; $i++):
					$cont2017[$i] += $aux[2017][$i];
					$ventas2017[$i] += $ventas[2017][$i];
				endfor;

				for($i = 0; $i < 12; $i++):
					$cont2018[$i] += $aux[2018][$i];
					$ventas2018[$i] += $ventas[2018][$i];
				endfor;

				$totales[0] += $aux['t2016'][0];
				$totales[1] += $aux['t2017'][0];
				$totales[2] += $aux['t2018'][0];
				$totales[3] += $ventas['t2016'][0];
				$totales[4] += $ventas['t2017'][0];
				$totales[5] += $ventas['t2018'][0];
			}
			
		endif;
		$tiendaName = 'Todas';

		/*$totalPed2016 = $pedAmoble['t2016'][0] + $pedForastero['t2016'][0] + $pedJardin['t2016'][0] + $pedHbt['t2016'][0];
		$totalPed2017 = $pedAmoble['t2017'][0] + $pedForastero['t2017'][0] + $pedJardin['t2017'][0] + $pedHbt['t2017'][0];
		$totalPed2018 = $pedAmoble['t2018'][0] + $pedForastero['t2018'][0] + $pedJardin['t2018'][0] + $pedHbt['t2018'][0];

		$ventasAmoble = totalVentas($serv1);
		$ventasForastero = totalVentas($serv2);
		$ventasJardin = totalVentas($serv3);
		$ventasHbt = totalVentas($serv4);*/

	else:
		$pedidos = countPedidos($result["servicio"]);
		$totalVentas = totalVentas($result["servicio"]);
		$tiendaName = $result["nombre"];
	endif;
	

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tienda <?php echo $tiendaName; ?> - Gráfico Pedidos</title>
	<script src="../../js/jquery-1.8.0.js"></script>
	<script src="../../js/highcharts.js"></script>
	<script src="../../js/exporting.js"></script>
	<script type="text/javascript" src="../../js/jquery.uniform.js"></script>

	<link rel="stylesheet" href="../../css/themes/aristo/css/uniform.aristo.css" type="text/css" media="screen" charset="utf-8" />

	<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
	<link href="../../css/font-awesome.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript">
	$(function () {
	    var pedidos = new Highcharts.chart({
			chart: {
		    zoomType: 'xy',
		    backgroundColor: '#FAFAFA',
		    renderTo: 'grafico_pedidos'
		    },
		    title: {
		        text: 'Cantidad Pedidos por mes Según Año'
		    },
		    xAxis: [{
		        categories:  ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		        crosshair: true
		    }],
		    yAxis: [{ // Primary yAxis
		        title: {
		            text: 'Valor Promedio por mes',
		            style: {
		                color: Highcharts.getOptions().colors[1]
		            }
		        }
		    }, { // Secondary yAxis
		        title: {
		        	text: 'Cantidad de Pedidos',
		            style: {
		                color: Highcharts.getOptions().colors[0]
		            }
		        },
		        opposite: true
		    }],
		    tooltip: {
		        shared: true
		    },
		    series: [{
		    	name: '2016',
		    	type: 'column',
		    	yAxis: 1,
			    data: [<?php 
		        		if ($tienda == 'todas') :
		        			for($i = 0; $i < 12; $i++):
		        				echo $cont2016[$i] . ',';
		        			endfor;
				else:
		        			for($i = 0; $i < 12; $i++):
		        				echo $pedidos['2016'][$i] . ',';
		        			endfor;
		        		endif;
			?>
		        ]
		    	},{
		    	name: '2017',
		    	type: 'column',
		    	yAxis: 1,
			    data: [
			<?php 
		        		if ($tienda == 'todas') :
		        			for($i = 0; $i < 12; $i++):
		        				echo $cont2017[$i] . ',';
		        			endfor;
				else:
		        			for($i = 0; $i < 12; $i++):
		        				echo $pedidos['2017'][$i] . ',';
		        			endfor;
		        		endif;
		        	?>
		        ]
		    	},{

			name: '2018',
			type: 'column',
			yAxis: 1,
			data: [
			<?php 
		        		if ($tienda == 'todas') :
		        			for($i = 0; $i < 12; $i++):
		        				echo $cont2018[$i] . ',';
		        			endfor;
				else:
		        			for($i = 0; $i < 12; $i++):
		        				echo $pedidos['2018'][$i] . ',';
		        			endfor;
		        		endif;
		        	?>
		        ]}, {
		       	name: 'Valor Promedio Pedidos 2016',
		        	type: 'spline',
		        	data: [
		        	<?php 
		        	if ($tienda == 'todas'):
		        		for($i = 0; $i < 12; $i++):
		        			$sumaPedidos = ($cont2016[$i]);
			        		if ($sumaPedidos > 0) :
			        			echo round( $ventas2016[$i] / $sumaPedidos) . ',';
			        		else:
			        			echo 0 . ',';
			        		endif;
		        		endfor;
		        	else:
		        	 	for($i = 0; $i < 12; $i++):
			        		if ($totalVentas['2016'][$i] == 0):
			        			echo 0 . ',';
			        		 else:
			        			echo round($totalVentas['2016'][$i] / $pedidos['2016'][$i]) . ',';
			        		endif;
		        		endfor;
		        endif; 
		        ?> ],
		        tooltip: {
		            valuePrefix: '$ '
		        }
		        },{
		       	name: 'Valor Promedio Pedidos 2017',
		        type: 'spline',
		        data: [
		        	<?php 
		        	if ($tienda == 'todas'):
		        		for($i = 0; $i < 12; $i++):
		        			$sumaPedidos = ($cont2017[$i]);
			        		if ($sumaPedidos > 0) :
			        			echo round( $ventas2017[$i] / $sumaPedidos) . ',';
			        		else:
			        			echo 0 . ',';
			        		endif;
		        		endfor;
		        	else:
		        	 	for($i = 0; $i < 12; $i++):
			        		if ($totalVentas['2017'][$i] == 0):
			        			echo 0 . ',';
			        		else:
			        			echo round($totalVentas['2017'][$i] / $pedidos['2017'][$i]) . ',';
			        		endif;
		        		endfor;
		        	endif; 
		        	?> 
		        	],
		        tooltip: {
		            valuePrefix: '$ '
		        }
		        },{
		       	name: 'Valor Promedio Pedidos 2018',
		        type: 'spline',
		        data: [
		        	<?php 
		        	if ($tienda == 'todas'):
		        		for($i = 0; $i < 12; $i++):
		        			$sumaPedidos = ($cont2018[$i]);
			        		if ($sumaPedidos > 0) :
			        			echo round( $ventas2018[$i] / $sumaPedidos) . ',';
			        		else:
			        			echo 0 . ',';
			        		endif;
		        		endfor;
		        	else:
		        	 	for($i = 0; $i < 12; $i++):
			        		if ($totalVentas['2018'][$i] == 0) :
			        			echo 0 . ',';
			        		else:
			        			echo round($totalVentas['2018'][$i] / $pedidos['2018'][$i]) . ',';
			        		endif;
		        		endfor;
		        	endif; 
		        	?> 
		        	],
		        tooltip: {
		            valuePrefix: '$ '
		        }
		    }]
		});
	});
</script>
</head>
<body>
	<div class="flecha"><a href="home"><i class="fa fa-reply fa-3x" aria-hidden="true"></i></a></div>
	<div class="contenedor">
    	 <?php include('../index.php'); ?>

        <div class="total-por-anio">
        	<h1>Total Pedidos</h1>
        	<h2>POR AÑO</h2>
        	<div class="box">
        		<p id="anio">Pedidos</p> <p id="porc">Valor promedio por pedido</p>
        		<div class="clearfix"></div>
        		<div class="colum">
	        		<p>2016</p>
	        		<p class="number"><?php 
	        		if ($tienda == 'todas'):
	        			echo $totales[0];
	        		else:
	        			echo $pedidos['t2016'][0];
	        		endif;
	        		?>
	        		</p>        			
        		</div>
        		<div class="colum">
	        		<p class="number porcentaje"><i class="fa fa-usd totales" aria-hidden="true"></i> <?php 

	        		if ($tienda == 'todas'):
	        			$result = ($totales[3]) / $totales[0];
	        			if ($result > 0):
	        				echo number_format(round($result));
	        			else:
	        				echo "0";
	        			endif;			
	        		else:
	        			if ($pedidos['t2016'][0] > 0):
	        				echo number_format(round($totalVentas['t2016'][0] / $pedidos['t2016'][0])); 
	        			else:
	        				echo "0";
	        			endif;        			
	        		endif;
	        		?>
	        		</p>      			
        		</div>


        	</div>

        	<div class="box">
        		<div class="colum">
	        		<p>2017</p>
		        		<p class="number"><?php
		        		if ($tienda == 'todas'):
		        		 	echo $totales[1];
		        		else:
		        			echo $pedidos['t2017'][0];
		        		endif;  
		        		?>
	        		</p>
        		</div>
        		<div class="colum">
	        		<p class="number porcentaje"><i class="fa fa-usd totales" aria-hidden="true"></i> <?php 
	        		if ($tienda == 'todas'):
	        			echo number_format(round($totales[4] / $totales[1]));
	        		else:
	        			echo number_format(round($totalVentas['t2017'][0] / $pedidos['t2017'][0])); 
	        		endif;
	        		?>
	        		</p>
        		</div>
        	</div>

        	<div class="box">
        		<div class="colum">
        			<p>2018</p>
	        		<p class="number"><?php 
	        		if ($tienda == 'todas'):
	        			echo $totales[2];
	        		else:
	        			echo $pedidos['t2018'][0]; 
	        		endif;
	        		?>
	        		</p>
        		</div>
        		<div class="colum">
	        		<p class="number porcentaje"><i class="fa fa-usd totales" aria-hidden="true"></i> <?php 
	        		if ($tienda == 'todas'):
	        			echo number_format(round($totales[5] / $totales[2]));
	        		else:
	        			echo number_format(round($totalVentas['t2018'][0] / $pedidos['t2018'][0])); 
	        		endif;
	        		?>
	        		</p>
        		</div>
        	</div>
        </div>
        <div class="div-g">
        	<div id="grafico_pedidos" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
            <!--<div id="container2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>-->
        </div><!--fin selectores-->

        
    </div>
</body>
</html>