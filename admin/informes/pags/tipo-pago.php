<?php
	include('../conex.php');
	require_once "../functions.php";
	$tienda = $_GET['tienda'];
	$service;

	$select = "SELECT nombre, servicio FROM tienda WHERE url='$tienda'";
	$query = mysqli_query($conexion, $select);

	$result = mysqli_fetch_assoc($query);

	if ($tienda == 'todas'):
		$ventas = array(
			2016 => array(0,0,0,0,0,0),
			2017 => array(0,0,0,0,0,0),
			2018 => array(0,0,0,0,0,0),
			'total' => array(0,0,0,0,0,0) 
		);

		$pedidos = array(
			2016 => array(0,0,0,0,0,0),
			2017 => array(0,0,0,0,0,0),
			2018 => array(0,0,0,0,0,0),
			'total' => array(0,0,0,0,0,0) 
		);

		$productos = array(
			2016 => array(0,0,0,0,0,0),
			2017 => array(0,0,0,0,0,0),
			2018 => array(0,0,0,0,0,0),
			'total' => array(0,0,0,0,0,0) 
		);

		$queryT = "SELECT nombre, servicio FROM tienda";
    	$resultT = mysqli_query($conexion, $queryT);

    	if (mysqli_num_rows($resultT) > 0):
    		while ($row = mysqli_fetch_assoc($resultT)) {
    			$auxVen = ventasTipoPago($row['servicio']);
    			$auxPed = pedidosTipoPago($row['servicio']);
    			$auxProd = productosTipoPago($row['servicio']);

    			for ($i=0; $i < 6; $i++) { 
    				$ventas[2016][$i] += $auxVen[2016][$i];
    				$pedidos[2016][$i] += $auxPed[2016][$i];
    				$productos[2016][$i] += $auxProd[2016][$i];

    				$ventas[2017][$i] += $auxVen[2017][$i];
    				$pedidos[2017][$i] += $auxPed[2017][$i];
    				$productos[2017][$i] += $auxProd[2017][$i];

    				$ventas[2018][$i] += $auxVen[2018][$i];
    				$pedidos[2018][$i] += $auxPed[2018][$i];
    				$productos[2018][$i] += $auxProd[2018][$i];

    				$ventas['total'][$i] += $auxVen['total'][$i];
    				$pedidos['total'][$i] += $auxPed['total'][$i];
    				$productos['total'][$i] += $auxProd['total'][$i];
    			}
    		}
    	endif;

    	$totalVentasT = 0;
    	$totalPedT = 0;
    	$totalProdT = 0;
    	$tiendaName = 'Todas';

	else:
		$totalVentas = ventasTipoPago($result['servicio']);
	   $totalPedidos = pedidosTipoPago($result['servicio']);
	   $totalProductos = productosTipoPago($result['servicio']);
	   $tiendaName = $result['nombre'];
	endif;

	

	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tienda <?php echo $tiendaName; ?> - Retiro en Tienda</title>
	<script src="../../js/jquery-1.8.0.js"></script>
	<script src="../../js/highcharts.js"></script>
	<script src="../../js/exporting.js"></script>
	<script src="../../js/myscript.js"></script>

	<script type="text/javascript" src="../../js/jquery.uniform.js"></script>

	<link rel="stylesheet" href="../../css/themes/aristo/css/uniform.aristo.css" type="text/css" media="screen" charset="utf-8" />

	<link href="../../css/estilos.css" rel="stylesheet" type="text/css" />
    <link href="../../css/font-awesome.css" rel="stylesheet" type="text/css" />


	<script type="text/javascript">
		// Create the chart
	$(function () {
	    var ventas = new Highcharts.Chart({
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        },
        backgroundColor: '#FAFAFA',
        renderTo: 'container'
    },
    title: {
        text: 'Valor Venta tipo pago'
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 45,
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Monto',
        data: [
        	<?php 
        		if ($tienda == 'todas'):
        			echo "['Ventas Debito', {$ventas['total'][0]}],
			            ['Ventas Normales', {$ventas['total'][1]}],
			            ['Ventas Cuotas', {$ventas['total'][2]}],
			            ['3 cuotas sin interés', {$ventas['total'][3]}],
			            ['2 cuotas sin interés', {$ventas['total'][4]}],
			            ['X cuotas sin interés', {$ventas['total'][5]}]";
        		else:
        			echo "['Ventas Debito', {$totalVentas['total'][0]}],
			            ['Ventas Normales', {$totalVentas['total'][1]}],
			            ['Ventas Cuotas', {$totalVentas['total'][2]}],
			            ['3 cuotas sin interés', {$totalVentas['total'][3]}],
			            ['2 cuotas sin interés', {$totalVentas['total'][4]}],
			            ['X cuotas sin interés', {$totalVentas['total'][5]}]";
         	endif;
        	?>
        ],
        tooltip: {
        		valuePrefix: '$ '
        }
    }]
});
});

$(function () {
	  var pedidos = new Highcharts.Chart({
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        },
        backgroundColor: '#FAFAFA',
        renderTo: 'torta-pedidos'
    },
    title: {
        text: 'Cantidad de Pedidos Según tipo pago'
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 45,
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Cantidad',
        data: [
            <?php 
        		if ($tienda == 'todas'):
        			echo "['Ventas Debito', {$pedidos['total'][0]}],
			            ['Ventas Normales', {$pedidos['total'][1]}],
			            ['Ventas Cuotas', {$pedidos['total'][2]}],
			            ['3 cuotas sin interés', {$pedidos['total'][3]}],
			            ['2 cuotas sin interés', {$pedidos['total'][4]}],
			            ['X cuotas sin interés', {$pedidos['total'][5]}]";
        		else:
        			echo "['Ventas Debito', {$totalPedidos['total'][0]}],
			            ['Ventas Normales', {$totalPedidos['total'][1]}],
			            ['Ventas Cuotas', {$totalPedidos['total'][2]}],
			            ['3 cuotas sin interés', {$totalPedidos['total'][3]}],
			            ['2 cuotas sin interés', {$totalPedidos['total'][4]}],
			            ['X cuotas sin interés', {$totalPedidos['total'][5]}]";
         	endif;
        	?>
        ]
    }]
});
});

$(function () {
	 var ventas = new Highcharts.Chart({
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        },
        backgroundColor: '#FAFAFA',
        renderTo: 'torta-productos'
    },
    title: {
        text: 'Cantidad de Productos Según tipo pago'
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 45,
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Cantidad',
        data: [
            <?php 
        		if ($tienda == 'todas'):
        			echo "['Ventas Debito', {$productos['total'][0]}],
			            ['Ventas Normales', {$productos['total'][1]}],
			            ['Ventas Cuotas', {$productos['total'][2]}],
			            ['3 cuotas sin interés', {$productos['total'][3]}],
			            ['2 cuotas sin interés', {$productos['total'][4]}],
			            ['X cuotas sin interés', {$productos['total'][5]}]";
        		else:
        			echo "['Ventas Debito', {$totalProductos['total'][0]}],
			            ['Ventas Normales', {$totalProductos['total'][1]}],
			            ['Ventas Cuotas', {$totalProductos['total'][2]}],
			            ['3 cuotas sin interés', {$totalProductos['total'][3]}],
			            ['2 cuotas sin interés', {$totalProductos['total'][4]}],
			            ['X cuotas sin interés', {$totalProductos['total'][5]}]";
         	endif;
        	?>
        ]
    }]
});
});
</script>
</head>
<body>
	<div class="flecha"><a href="../../home"><i class="fa fa-reply fa-3x" aria-hidden="true"></i></a></div>
	<div class="contenedor" style="margin-top:20px;">
        <?php include('../index.php'); ?>

		<!-- GRAFICO TORTA DE VENTAS -->
		<div class="totales-menu">
			<h2>Ventas Tipo Pago</h2>
			<h3>POR AÑOS</h3>
			<ul>
				<li class="nav-totales">2016 <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child">
						<li>Debito <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2016][0]) : number_format($totalVentas[2016][0]);
						?></p></li>
						<li>Normales <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2016][1]) : number_format($totalVentas[2016][1]);
						?></p></li>
						<li>Cuotas <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2016][2]) : number_format($totalVentas[2016][2]);
						?></p></li>
						<li>3 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2016][3]) : number_format($totalVentas[2016][3]);
						?></p></li>
						<li>2 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2016][4]) : number_format($totalVentas[2016][4]);
						?></p></li>
						<li>X C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2016][5]) : number_format($totalVentas[2016][5]);
						?></p></li>
					</ul>
				</li>
				<li class="nav-totales">2017 <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child">
						<li>Debito <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2017][0]) : number_format($totalVentas[2017][0]);
						?></p></li>
						<li>Normales <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2017][1]) : number_format($totalVentas[2017][1]);
						?></p></li>
						<li>Cuotas <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2017][2]) : number_format($totalVentas[2017][2]);
						?></p></li>
						<li>3 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2017][3]) : number_format($totalVentas[2017][3]);
						?></p></li>
						<li>2 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2017][4]) : number_format($totalVentas[2017][4]);
						?></p></li>
						<li>X C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2017][5]) : number_format($totalVentas[2017][5]);
						?></p></li>
					</ul>
				</li>
				<li class="nav-totales">2018 <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child">
						<li>Debito <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2018][0]) : number_format($totalVentas[2018][0]);
						?></p></li>
						<li>Normales <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2018][1]) : number_format($totalVentas[2018][1]);
						?></p></li>
						<li>Cuotas <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2018][2]) : number_format($totalVentas[2018][2]);
						?></p></li>
						<li>3 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2018][3]) : number_format($totalVentas[2018][3]);
						?></p></li>
						<li>2 C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2018][4]) : number_format($totalVentas[2018][4]);
						?></p></li>
						<li>X C/Sin Interés <p><i class="fa fa-usd" aria-hidden="true"></i> <?php echo ($tienda == 'todas') ? number_format($ventas[2018][5]) : number_format($totalVentas[2018][5]);
						?></p></li>
					</ul>
				</li>
			</ul>
		</div>
        <div class="div-g">
            <div id="container"></div>
        </div>

        <div class="separator"></div>
		
		<!-- GRAFICO TORTA DE PEDIDOS -->
        <div class="totales-menu">
			<h2>Pedidos Tipo Pago</h2>
			<h3>POR AÑOS</h3>
			<ul>
				<li class="nav-totales">2016 <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child" id="child-pedidos-2016">
						<li>Debito <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2016][0]) : number_format($totalPedidos[2016][0]);
						?></p></li>
						<li>Normales <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2016][1]) : number_format($totalPedidos[2016][1]);
						?></p></li>
						<li>Cuotas <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2016][2]) : number_format($totalPedidos[2016][2]);
						?></p></li>
						<li>3 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2016][3]) : number_format($totalPedidos[2016][3]);
						?></p></li>
						<li>2 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2016][4]) : number_format($totalPedidos[2016][4]);
						?></p></li>
						<li>X C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2016][5]) : number_format($totalPedidos[2016][5]);
						?></p></li>
					</ul>
				</li>
				<li class="nav-totales">2017 <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child" id="child-pedidos-2016">
						<li>Debito <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2017][0]) : number_format($totalPedidos[2017][0]);
						?></p></li>
						<li>Normales <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2017][1]) : number_format($totalPedidos[2017][1]);
						?></p></li>
						<li>Cuotas <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2017][2]) : number_format($totalPedidos[2017][2]);
						?></p></li>
						<li>3 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2017][3]) : number_format($totalPedidos[2017][3]);
						?></p></li>
						<li>2 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2017][4]) : number_format($totalPedidos[2017][4]);
						?></p></li>
						<li>X C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2017][5]) : number_format($totalPedidos[2017][5]);
						?></p></li>
					</ul>
				</li>
				<li class="nav-totales">2018 <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child" id="child-pedidos-2016">
						<li>Debito <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2018][0]) : number_format($totalPedidos[2018][0]);
						?></p></li>
						<li>Normales <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2018][1]) : number_format($totalPedidos[2018][1]);
						?></p></li>
						<li>Cuotas <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2018][2]) : number_format($totalPedidos[2018][2]);
						?></p></li>
						<li>3 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2018][3]) : number_format($totalPedidos[2018][3]);
						?></p></li>
						<li>2 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2018][4]) : number_format($totalPedidos[2018][4]);
						?></p></li>
						<li>X C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($pedidos[2018][5]) : number_format($totalPedidos[2018][5]);
						?></p></li>
					</ul>
				</li>
			</ul>
		</div>
        <div class="div-g">
            <div id="torta-pedidos"></div>
        </div>

        <div class="separator"></div>

        <!-- GRAFICO TORTA DE PRODUCTOS -->
        <div class="totales-menu">
			<h2>Productos Tipo Pago</h2>
			<h3>POR AÑOS</h3>
			<ul>
				<li class="nav-totales">2016 <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child" id="child-prod-2016">
						<li>Debito <p><?php echo ($tienda == 'todas') ? number_format($productos[2016][0]) : number_format($totalProductos[2016][0]);
						?></p></li>
						<li>Normales <p><?php echo ($tienda == 'todas') ? number_format($productos[2016][1]) : number_format($totalProductos[2016][1]);
						?></p></li>
						<li>Cuotas <p><?php echo ($tienda == 'todas') ? number_format($productos[2016][2]) : number_format($totalProductos[2016][2]);
						?></p></li>
						<li>3 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($productos[2016][3]) : number_format($totalProductos[2016][3]);
						?></p></li>
						<li>2 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($productos[2016][4]) : number_format($totalProductos[2016][4]);
						?></p></li>
						<li>X C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($productos[2016][5]) : number_format($totalProductos[2016][5]);
						?></p></li>
					</ul>
				</li>
				<li class="nav-totales"">2017 <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child" id="child-prod-2016">
						<li>Debito <p><?php echo ($tienda == 'todas') ? number_format($productos[2017][0]) : number_format($totalProductos[2017][0]);
						?></p></li>
						<li>Normales <p><?php echo ($tienda == 'todas') ? number_format($productos[2017][1]) : number_format($totalProductos[2017][1]);
						?></p></li>
						<li>Cuotas <p><?php echo ($tienda == 'todas') ? number_format($productos[2017][2]) : number_format($totalProductos[2017][2]);
						?></p></li>
						<li>3 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($productos[2017][3]) : number_format($totalProductos[2017][3]);
						?></p></li>
						<li>2 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($productos[2017][4]) : number_format($totalProductos[2017][4]);
						?></p></li>
						<li>X C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($productos[2017][5]) : number_format($totalProductos[2017][5]);
						?></p></li>
					</ul>
				</li>
				<li class="nav-totales"">2018 <i class="fa fa-sort-desc" aria-hidden="true"></i>
					<ul class="child" id="child-prod-2016">
						<li>Debito <p><?php echo ($tienda == 'todas') ? number_format($productos[2018][0]) : number_format($totalProductos[2018][0]);
						?></p></li>
						<li>Normales <p><?php echo ($tienda == 'todas') ? number_format($productos[2018][1]) : number_format($totalProductos[2018][1]);
						?></p></li>
						<li>Cuotas <p><?php echo ($tienda == 'todas') ? number_format($productos[2018][2]) : number_format($totalProductos[2018][2]);
						?></p></li>
						<li>3 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($productos[2018][3]) : number_format($totalProductos[2018][3]);
						?></p></li>
						<li>2 C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($productos[2018][4]) : number_format($totalProductos[2018][5]);
						?></p></li>
						<li>X C/Sin Interés <p><?php echo ($tienda == 'todas') ? number_format($productos[2018][5]) : number_format($totalProductos[2018][5]);
						?></p></li>
					</ul>
				</li>
			</ul>
		</div>
        <div class="div-g">
            <div id="torta-productos"></div>
        </div>
</body>
</html>