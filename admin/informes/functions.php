<?php 


function countPedidos ($service){
    // años
    $anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
	$file = file_get_contents($service);

	$xml = simplexml_load_string($file);

	// Variables contadoras de pedidos por años/mes
	$array = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

	$arraySalida = array (
		'total_meses' => array($anioAnteriorAlAnterior => array(), $anioAnterior => array(), $anioActual => array()),
		'totales' => array("t$anioAnteriorAlAnterior" => array(0), "t$anioAnterior" => array(0), "t$anioActual" => array(0))
 	);

	foreach ($xml->pedido as $pedido):
		$fecha = explode("-", $pedido->fecha);
		$año = $fecha[0];
		$taño = 't'.$año;

		for ($i = 0; $i < 12; $i++):
			if ($fecha[1] == $array[$i]):
				$arraySalida['total_meses'][$año][$i] += 1;
			endif;
		endfor;
		$arraySalida['totales'][$taño][0] += 1;
	endforeach;

	return $arraySalida;	
}

function totalVentas($service){
	$anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
    $file = file_get_contents($service);

	$xml = simplexml_load_string($file);

	// Variables contadoras de pedidos por años/mes
	$array = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

	$arraySalida = array (
		'total_meses' => array($anioAnteriorAlAnterior => array(), $anioAnterior => array(), $anioActual => array()),
		'totales' => array("t$anioAnteriorAlAnterior" => array(0), "t$anioAnterior" => array(0), "t$anioActual" => array(0))
 	);

	foreach ($xml->pedido as $pedido) :

		$fecha = explode("-", $pedido->fecha);
		$cant = $pedido->total_sin_despacho;
		$año = $fecha[0];
		$taño = 't'.$año;

		for ($i = 0; $i < 12; $i++) :
			if ($fecha[1] == $array[$i]) :
				$arraySalida['total_meses'][$año][$i] += $cant;
			endif;
		endfor;
		$arraySalida['totales'][$taño][0] += $cant;

	endforeach;

	return $arraySalida;
}

function countProductos($service){
	$anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
    $file = file_get_contents($service);
    $xml = simplexml_load_string($file);

	// Variables contadoras de pedidos por años/mes
	$array = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

	$arraySalida = array (
		'total_meses' => array($anioAnteriorAlAnterior => array(), $anioAnterior => array(), $anioActual => array()),
		'totales' => array("t$anioAnteriorAlAnterior" => array(0), "t$anioAnterior" => array(0), "t$anioActual" => array(0))
 	);

 	foreach ($xml->pedido as $pedido) :
		$fecha = explode("-", $pedido->fecha);
		$cant = $pedido->cant_productos;
		$año = $fecha[0];
		$taño = 't'.$año;

		for ($i = 0; $i < 12; $i++) :
			if ($fecha[1] == $array[$i]) :
				$arraySalida['total_meses'][$año][$i] += $cant;
			endif;
		endfor;
		$arraySalida['totales'][$taño][0] += $cant;
	endforeach;

	return $arraySalida;
}

function cantidadVentas($service){
	
    $anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
    $file = file_get_contents($service);
    $xml = simplexml_load_string($file);

	// Variables contadoras de pedidos por años/mes
	$array = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

 	$arraySalida = array (
 		"$anioAnteriorAlAnterior" => array(0),
 		"$anioAnterior" => array(0),
 		"$anioActual" => array(0)
 	);

 	foreach ($xml->pedido as $pedido) :
		$fecha = explode("-", $pedido->fecha);
		$cant = 0;
		if ($fecha[0] == "$anioAnteriorAlAnterior") :
			$arraySalida[$anioAnteriorAlAnterior][0] = $arraySalida[$anioAnteriorAlAnterior][0] + $cant;
		elseif ($fecha[0] == "$anioAnterior") :
			$arraySalida[$anioAnterior][0] = $arraySalida[$anioAnterior][0] + $cant;
		elseif ($fecha[0] == "$anioActual") :
			$arraySalida["t$anioActual"][0] = $arraySalida[$anioActual][0] + $cant;
		endif;
	endforeach;

	return $arraySalida;
}

function ventasRetiroTienda($service){
	
    $anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
    $file = file_get_contents($service);
    $xml = simplexml_load_string($file);

	// Variables contadoras de pedidos por años/mes
	$array = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

 	$arraySalida = array (
		'retiro' => array($anioAnteriorAlAnterior => array(0), $anioAnterior => array(0), $anioActual => array(0)), 
		'despacho' => array($anioAnteriorAlAnterior => array(0), $anioAnterior => array(0), $anioActual => array(0)),
		'totales' => array("t$anioAnteriorAlAnterior" => array(0), "t$anioAnterior" => array(0), "t$anioActual" => array(0))
 	);

 	foreach ($xml->pedido as $pedido) :
		$fecha = explode("-", $pedido->fecha);
		$cant = $pedido->total_sin_despacho;
		$retiro = $pedido->retiro_en_tienda;
		$año = $fecha[0];
		$taño = 't'.$año;

		if ($retiro == 1):
			$arraySalida['retiro'][$año][0] += $cant;
			$arraySalida['totales'][$taño][0] += $cant;
		else:
			$arraySalida['despacho'][$año][0] += $cant;
		endif;
	endforeach;

	return $arraySalida;
}

function pedidosRetiroTienda($service){
	
    $anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
    $file = file_get_contents($service);
    $xml = simplexml_load_string($file);
    // Variables contadoras de pedidos por años/mes
	$array = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

 	$arraySalida = array (
		'retiro' => array($anioAnteriorAlAnterior => array(0), $anioAnterior => array(0), $anioActual => array(0)), 
		'despacho' => array($anioAnteriorAlAnterior => array(0), $anioAnterior => array(0), $anioActual => array(0)),
		'totales' => array("t$anioAnteriorAlAnterior" => array(0), "t$anioAnterior" => array(0), "t$anioActual" => array(0))
 	);

 	foreach ($xml->pedido as $pedido) :
		$fecha = explode("-", $pedido->fecha);
		$retiro = $pedido->retiro_en_tienda;
		$año = $fecha[0];
		$taño = 't'.$año;

		if ($retiro == 1):
			$arraySalida['retiro'][$año][0] += 1;
			$arraySalida['totales'][$taño][0] += 1;
		else:
			$arraySalida['despacho'][$año][0] += 1;
		endif;
	endforeach;

	return $arraySalida;	
}

function cantProductosRetiroTienda($service){
	$anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
    $file = file_get_contents($service);
    $xml = simplexml_load_string($file);
    // Variables contadoras de pedidos por años/mes
	$array = array ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

 	$arraySalida = array (
		'retiro' => array($anioAnteriorAlAnterior => array(0), $anioAnterior => array(0), $anioActual => array(0)), 
		'despacho' => array($anioAnteriorAlAnterior => array(0), $anioAnterior => array(0), $anioActual019 => array(0)),
		'totales' => array("t$anioAnteriorAlAnterior" => array(0), "t$anioAnterior" => array(0), "t$anioActual" => array(0))
 	);

 	foreach ($xml->pedido as $pedido) :
		$fecha = explode("-", $pedido->fecha);
		$cant = $pedido->cant_productos;
		$retiro = $pedido->retiro_en_tienda;
		$año = $fecha[0];
		$taño = 't'.$año;

		if ($retiro == 1):
			$arraySalida['retiro'][$año][0] += $cant;
			$arraySalida['totales'][$taño][0] += $cant;
		else:
			$arraySalida['despacho'][$año][0] += $cant;
		endif;
	endforeach;

	return $arraySalida;
}

function ventasTipoPago($service){
	$anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
    $file = file_get_contents($service);
    $xml = simplexml_load_string($file);
    $arraySalida = array (
 		'total_meses' => array(
 			$anioAnteriorAlAnterior => array(0,0,0,0,0,0),
	 		$anioAnterior => array(0,0,0,0,0,0),
	 		$anioActual => array(0,0,0,0,0,0)
 		),
 		'total' => array(0,0,0,0,0,0)
 	);

 	foreach ($xml->pedido as $pedido):
 		$fecha = explode("-", $pedido->fecha);
 		$cant = $pedido->total_sin_despacho;
 		$tipoPago = $pedido->tipo_pago;
 		$anios = array(0 => "$anioAnteriorAlAnterior", 1 => "$anioAnterior", 2 => "$anioActual");

 		for($i = 0; $i < sizeof($anios); $i++):
 			$anio = $anios[$i];
	 		if($fecha[0] == $anio):
	 			if($tipoPago == 'VD'):
	 				$arraySalida['total_meses'][$anio][0] += $cant;
	 				$arraySalida['total'][0] += $cant;
	 			elseif($tipoPago == 'VN'):
	 				$arraySalida['total_meses'][$anio][1] += $cant;
	 				$arraySalida['total'][1] += $cant;
	 			elseif($tipoPago == 'VC'):
	 				$arraySalida['total_meses'][$anio][2] += $cant;
	 				$arraySalida['total'][2] += $cant;
	 			elseif($tipoPago == 'SI'):
	 				$arraySalida['total_meses'][$anio][3] += $cant;
	 				$arraySalida['total'][3] += $cant;
	 			elseif($tipoPago == 'S2'):
	 				$arraySalida['total_meses'][$anio][4] += $cant;
	 				$arraySalida['total'][4] += $cant;
	 			elseif($tipoPago == 'NC'):
	 				$arraySalida['total_meses'][$anio][5] += $cant;
	 				$arraySalida['total'][5] += $cant;
	 			endif;
	 		endif;
	 	endfor;
 	endforeach;

 	return $arraySalida;
}

function pedidosTipoPago($service){
    
    $anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
    $file = file_get_contents($service);

	$xml = simplexml_load_string($file);

 	$arraySalida = array (
 		'total_meses' => array(
 			$anioAnteriorAlAnterior => array(0,0,0,0,0,0),
	 		$anioAnterior => array(0,0,0,0,0,0),
	 		$anioActual => array(0,0,0,0,0,0)
 		),
 		'total' => array(0,0,0,0,0,0)
 	);

 	foreach ($xml->pedido as $pedido):
 		$fecha = explode("-", $pedido->fecha);
 		$tipoPago = $pedido->tipo_pago;
 		$anios = array(0 => "$anioAnteriorAlAnterior", 1 => "$anioAnterior", 2 => "$anioActual");

 		for($i = 0; $i < sizeof($anios); $i++):
 			$anio = $anios[$i];
	 		if($fecha[0] == $anio):
	 			if($tipoPago == 'VD'):
	 				$arraySalida['total_meses'][$anio][0] += 1;
	 				$arraySalida['total'][0] += 1;
	 			elseif($tipoPago == 'VN'):
	 				$arraySalida['total_meses'][$anio][1] += 1;
	 				$arraySalida['total'][1] += 1;
	 			elseif($tipoPago == 'VC'):
	 				$arraySalida['total_meses'][$anio][2] += 1;
	 				$arraySalida['total'][2] += 1;
	 			elseif($tipoPago == 'SI'):
	 				$arraySalida['total_meses'][$anio][3] += 1;
	 				$arraySalida['total'][3] += 1;
	 			elseif($tipoPago == 'S2'):
	 				$arraySalida['total_meses'][$anio][4] += 1;
	 				$arraySalida['total'][4] += 1;
	 			elseif($tipoPago == 'NC'):
	 				$arraySalida['total_meses'][$anio][5] += 1;
	 				$arraySalida['total'][5] += 1;
	 			endif;
	 		endif;
	 	endfor;
 	endforeach;

 	return $arraySalida;
}

function productosTipoPago($service){
	$anioActual = date('Y') ;
    $anioAnterior = date('Y', strtotime('-1 year')) ;
    $anioAnteriorAlAnterior = date('Y', strtotime('-2 year')) ;
    
    $file = file_get_contents($service);
    $xml = simplexml_load_string($file);

 	$arraySalida = array (
 		'total_meses' => array(
 			$anioAnteriorAlAnterior => array(0,0,0,0,0,0),
	 		$anioAnterior => array(0,0,0,0,0,0),
	 		$anioActual => array(0,0,0,0,0,0)
 		),
 		'total' => array(0,0,0,0,0,0)
 	);

 	foreach ($xml->pedido as $pedido):
 		$fecha = explode("-", $pedido->fecha);
 		$cant = $pedido->cant_productos;
 		$tipoPago = $pedido->tipo_pago;
 		$anios = array(0 => "$anioAnteriorAlAnterior", 1 => "$anioAnterior", 2 => "$anioActual");

 		for($i = 0; $i < sizeof($anios); $i++):
 			$anio = $anios[$i];
	 		if($fecha[0] == $anio):
	 			if($tipoPago == 'VD'):
	 				$arraySalida['total_meses'][$anio][0] += $cant;
	 				$arraySalida['total'][0] += $cant;
	 			elseif($tipoPago == 'VN'):
	 				$arraySalida['total_meses'][$anio][1] += $cant;
	 				$arraySalida['total'][1] += $cant;
	 			elseif($tipoPago == 'VC'):
	 				$arraySalida['total_meses'][$anio][2] += $cant;
	 				$arraySalida['total'][2] += $cant;
	 			elseif($tipoPago == 'SI'):
	 				$arraySalida['total_meses'][$anio][3] += $cant;
	 				$arraySalida['total'][3] += $cant;
	 			elseif($tipoPago == 'S2'):
	 				$arraySalida['total_meses'][$anio][4] += $cant;
	 				$arraySalida['total'][4] += $cant;
	 			elseif($tipoPago == 'NC'):
	 				$arraySalida['total_meses'][$anio][5] += $cant;
	 				$arraySalida['total'][5] += $cant;
	 			endif;
	 		endif;
	 	endfor;
 	endforeach;

 	return $arraySalida;
}