<?php 
header('Content-type: text/html; charset=utf-8');
include("admin/conf.php");
include 'Mailin.php';
/********************************************************\
|  Alfastudio Ltda - Mail sender V0.5			 		 |
|  Fecha Modificacion: 16/06/2011		                 |
|  Todos los derechos reservados © Alfastudio Ltda 2011  |
|  Prohibida su copia parcial o total, 					 |
|  venta, comercializacion o distribucion				 |
|  https://www.moldeable.com/                             |
\********************************************************/

//******************Cambiar sólo estos valores*************************//



$nombre_sitio = opciones("nombre_cliente");
$nombre_corto = opciones("dominio");
$noreply = "no-reply@".$nombre_corto;
$url_sitio = "https://".$nombre_corto;
$correo_venta = opciones("correo_venta");
$color_logo = opciones("color_logo");
	
$logo = opciones("logo_mail");
$campo_oculto = 'validacion';
$para = 'contacto@zonapromo.cl';
$asunto = "Contacto $nombre_sitio";
$ruta_retorno = 'contacto';
$ruta_retorno_exito = 'exito-contacto';
$campo_mensaje = 'mensaje';
$obligatorios = "nombre, email, $campo_mensaje";
$lang = (isset($_GET[lang])) ? "ing": "esp";

define('clave', '6LfZoiIeAAAAAPAgvAOU1YL5UxiHrU72BemCOzOi');
$token = (isset($_POST['token'])) ? mysqli_real_escape_string($conexion, $_POST['token']) : "";
$action = (isset($_POST['action'])) ? mysqli_real_escape_string($conexion, $_POST['action']) : "";


$cu = curl_init();
curl_setopt($cu, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
curl_setopt($cu, CURLOPT_POST, 1);
curl_setopt($cu, CURLOPT_POSTFIELDS, http_build_query(array('secret' => clave, 'response' => $token)));
curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($cu);
curl_close($cu);

$datos = json_decode($response, true);

// echo $token."<br>";
// print_r($datos);

if($datos['success'] == 1 && $datos['score'] >= 0.5){

$file_field = ""; //Nombre del campo del archivo adjunto, dejar vacÃ­o si no se va a usar

$from = "noreply@zonapromo.cl";
$save_in_db = true;
if ($save_in_db)
$insert = insert_entry("validacion, token, action", "contactos");

//Campos del formulario correspondiente a nombre e email.
$nombre_cl = $_POST['nombre'];
$email_cl = $_POST['email'];

//******************Comienzo del código, no modificar*********************//

if ($_POST[$campo_oculto] == ''){
	$vars = get_post('');
	$obligatorios = explode(',', $obligatorios);
    
    
	foreach($obligatorios as $o)
	{
		$campo = trim($o);
		if ($_POST["$campo"] === '')
		{
            $error = ($lang == 'esp') ? "Los campos marcados con * son necesarios." : "* indicates requiered fields.";
			header("location:$ruta_retorno?msje=$error");
            die("");
            
		}
        
	}
    
	
  
    $msg .='<html>
	<head><link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"></head>
	<body style="font-family: \'Lato\', sans-serif; color: #333;">
	<div style="background: #f9f9f9; width: 100%;">
		<div style="background: #fff; border-top: 2px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px; margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
			<p style="font-size: 20px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
			<p style="font-size: 16px; margin-top: 0;">Se ha enviado un nuevo mensaje de contacto a través de la página web de '.$nombre_sitio.'. <br>
			Los datos de la persona que contacta son:</p>
            <ul>';

                            foreach($_POST as $key=>$val)
                            {

                                if ($key != 'action' AND $key != 'token' AND $key != 'enviar' AND $key != $campo_mensaje AND $key != '')
                                {
                                    $nombreVal = ucwords($key);
                                    $msg .= "<li style='min-height:30px;'><strong>".$nombreVal.":</strong> ".htmlentities($val,ENT_QUOTES,"UTF-8")."</li>";
                                }
                            }
                            $msg .= '</ul>
                            <p align="justify" style="font-size:20px; color: #009be5;">El cliente ha dejado el siguiente mensaje: <br /><br />
                                <em>'.htmlentities($_POST[$campo_mensaje],ENT_QUOTES,"UTF-8").'</em>
                            </p>

                    <p style="font-size: 16px;">Rogamos contactarse lo antes posible con el cliente.<br><br>
                    Muchas gracias. <br>
                    Atte,<br>
                    <b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
                </div>
            </div>
        </body>
    </html>';
	        
    
    // echo $msg."<br><hr>";
    
    $asuntoRespuesta = "Contacto $nombre_sitio";
    $msg3 = '<html>
	<head><link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"></head>
	<body style="font-family: \'Lato\', sans-serif; color: #333;">
	<div style="background: #f1f1f1; width: 100%;">
		<div style="background: #fff; border-top: 3px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
            <h2 style="font-size: 26px; text-transform:uppercase; margin-bottom:5px;color:'.$color_logo.';">Hola, '.$nombre_cl.':</h2>
			<p style="font-size: 20px; margin-top: 0; color:'.$color_logo.';">Hemos recibido tu solicitud de contacto, pronto nos comunicaremos contigo.</p>
			<p style="font-size: 20px; color:'.$color_logo.';">
			Muchas gracias. <br>
			Atte,<br>
			<b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
		</div>
        <div style="background: #f5f6f7; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
            <p style="color:'.$color_logo.'; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
            <p>
                <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
            </p>
		</div>
        
	</div>
	</body>
	</html>';

            // echo $msg3."<br><hr>";
            // die();

    
            $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
                if(opciones("correo_admin1") != ""){
                    $mailin-> addTo(opciones("correo_admin1"), opciones("nombre_correo_admin1"));
                }
                if(opciones("correo_admin2") != ""){
                    $mailin-> addTo(opciones("correo_admin2"), opciones("nombre_correo_admin2"));
                }
                if(opciones("correo_admin3") != ""){
                    $mailin-> addTo(opciones("correo_admin3"), opciones("nombre_correo_admin3"));
                }
                $mailin-> setFrom('portal@zonapromo.cl', 'Zona Promo')->
                setSubject("$asunto")->
                setText("$msg")->
                    setHtml("$msg");
            $res = $mailin->send();
            $res2 = json_decode($res);

            if ($res2->{'result'} == true) {
                $error = ($lang == 'esp') ?  "Su mensaje fue enviado. Muchas gracias por contactarnos." : "Your message has been sent, we'll answer you asap. Thank You.";
				
                //envio copia al cliente
                $mailinCliente = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
                $mailinCliente->
                    addTo("$email_cl", "$nombre_cl")->
                    setFrom('portal@zonapromo.cl', 'Zona Promo')->
                    setSubject("$asuntoRespuesta")->
                    setText("$msg3")->
                        setHtml("$msg3");
                $resCliente = $mailinCliente->send();
                $res2Cliente = json_decode($resCliente);
                //fin envio copia al cliente
                
                
                header("Location: $ruta_retorno_exito?a=1");
				die("1");
            } else {

               $error = ($lang == 'esp') ? "Error enviando el correo, por favor inténtelo nuevamente." : "We had some dificulties sending the email, please try again later.";
				header("Location: $ruta_retorno?msje=$error");
				die("2");
            }
	
			
	} else {
		//si no existe retorno e indico que no esta en los registros
		$error = ($lang == 'esp') ? "Error al enviar el mensaje" : "Error sending the email.";
		header("Location: $ruta_retorno?msje=$error");
		die("3");
	}
}else{
    //si no existe retorno e indico que no esta en los registros
    $error = ($lang == 'esp') ? "Error al procesar tu solicitud" : "Error sending the email.";
    header("Location: $ruta_retorno?msje=$error");
    die("3");
}
?>