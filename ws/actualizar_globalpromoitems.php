<?php
    ini_set('max_execution_time', 0);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
	include('../admin/conf.php');

    
$curl = curl_init();
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);    
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt_array($curl, [
  CURLOPT_URL => "https://globalpromoitems.com/tienda/chile/index.php?controller=SBWebService&id=A&customer=134&token=a48d5fdc20",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
//   echo $response;
}
// die("hola");
    // $file = file_get_contents('https://globalpromoitems.com/tienda/chile/index.php?controller=SBWebService&id=A&customer=134&token=a48d5fdc20');
	$xml = new SimpleXMLElement($response);
    $cantidadProductosMadre = 0;
    $cantidadProductoshijo = 0;
	$actualizados = 0;
    $insertados = 0;
    foreach($xml->product as $producto){
        //     echo '<pre>';
        // print_r($producto);
        // echo '</pre>';
            $sku = $producto->ref;
            $nombre = $producto->name;
            $descripcionCorta = $producto->otherinfo;
            $descripcion = $producto->extendedinfo;
            $idCategoria = $producto->categories->category_ref_1;
            $nombreCategoria = $producto->categories->category_name_1;
            $video = $producto->link360;
            $thumbsProductoMadre = $producto->variants->variant->image500px;
            if ($nombreCategoria != "" &&  $idCategoria != "") {
                $consultaCategoria = consulta_bd("id, nombre","categorias_api","id = $idCategoria AND nombre = '$nombreCategoria'","");
                if ($consultaCategoria <= 0) {
                    $insertandoCategoria =  insert_bd("categorias_api","id, publicada, nombre, api","$idCategoria, 1, '$nombreCategoria','globalpromoitems'");
                }
            }
            $consultaProductoMadre = consulta_bd("nombre_api, descripcion_breve, descripcion, video, thumbs","productos","sku_api = '$sku'","");
            if ($consultaProductoMadre <= 0) {
                $insertandoProductoMadre =  insert_bd("productos","publicado, nombre_api, api, marca_id, thumbs, fecha_creacion, descripcion, descripcion_breve, sku_api, nombre, codigo_producto_madre","1, '$nombre','globalpromoitems', 0, '$thumbsProductoMadre', NOW(), '$descripcion', '$descripcionCorta', '$sku', '$nombre', '$sku'");
                $idProducto = mysqli_insert_id($conexion);
                $cantidadProductosMadre++;
                $insertados++;
                echo "Se inserto NUEVO PRODUCTO: nombre=".$nombre."  sku= ".$sku. " id =".$idProducto." <br>";
            }

            if ($nombre != $consultaProductoMadre[0][0] || $descripcionCorta != $consultaProductoMadre[0][1] || $descripcion != $consultaProductoMadre[0][2] || $video != $consultaProductoMadre[0][3] || $thumbsProductoMadre != $consultaProductoMadre[0][4]) {

                echo "Nombres: ". $nombre. ' = ' .$consultaProductoMadre[0][0] . "<br>".
                 "Descripcion Corta: ".$descripcionCorta. ' = ' .$consultaProductoMadre[0][1] ."<br>". 
                 "Descripciones: ".$descripcion. ' = ' .$consultaProductoMadre[0][2] ."<br>".
                 "Videos: ".$video. ' = ' .$consultaProductoMadre[0][3] ."<br>". 
                 "Thumbs: ".$thumbsProductoMadre. ' = ' .$consultaProductoMadre[0][4] ."<br>";

                $update = update_bd("productos","nombre_api = '$nombre', thumbs = '$thumbsProductoMadre', descripcion = '$descripcion', descripcion_breve = '$descripcionCorta', video = '$video'","sku_api = '$sku'");
                if($update === true){
                    echo "Se ACTUALIZO PRODUCTO: nombre= ".$nombre."  sku= ".$sku. "<br>";
                    $actualizados++;
                    $cantidadProductosMadre++;
                }
            }
            // echo 
            // $sku."<br>".
            // $nombre."<br>".
            // $descripcion."<br>".
            // $descripcionCorta."<br>".
            // $video ."<br>".
            // $idCategoria."<br>".
            // $nombreCategoria."<br>".
            // $thumbsProductoMadre."<br>";
            $detalles = $producto->variants;
            foreach ($detalles->variant as $detail) {
                $color = $detail->colour;
                $size = $detail->size;
                if ($color == "VER") { $color = "VERDE";} 
                elseif ($color == "MARR") {$color = "MARRON";}
                elseif ($color == "NARA") {$color = "NARANJA";}
                elseif ($color == "ROJ") {$color = "ROJO";}
                elseif ($color == "NATU") {$color = "NATURAL";}
                elseif ($color == "AMA") {$color = "AMARILLO";}
                elseif ($color == "GRI") {$color = "GRIS";}
                elseif ($color == "BLA") {$color = "BLANCO";}
                elseif ($color == "FUCSI") {$color = "FUCSIA";}
                elseif ($color == "NEG") {$color = "NEGRO";}
                elseif ($color == "TRANS") {$color = "TRANSPARENTE";}
                elseif ($color == "PLAT") {$color = "PLATA";}
                elseif ($color == "S/C") {$color = "Sin Color";}
                $consultaColor = consulta_bd("id,nombre","color_productos","nombre = '$color'","");
                if ($consultaColor > 0) {
                    $id_color = $consultaColor[0][0];
                }else{
                    $insertandoColor =  insert_bd("color_productos","nombre, fecha_creacion","'$color', NOW()");
                    $id_color = mysqli_insert_id($conexion);
                }
                
                $stock = $producto->palet_units;
                $imagenProductoHijo = $detail->image500px;
                $colorSKU = preg_replace("/ /", '-', $color);
                $skuHijo = $sku."-".$colorSKU."/".$size;
                // $skuHijo = $detail->refct;

                $consultaProductoHijo = consulta_bd("nombre_api, color_producto_id, stock, imagen1","productos_detalles","sku_api = '$skuHijo'","");

                if ($consultaProductoHijo <= 0) {
                    $insertandoProductoHijo =  insert_bd("productos_detalles","producto_id, color_producto_id, publicado, nombre_api,sku_api, api, cotizacion_minima, stock, venta_minima, fecha_creacion,imagen1, nombre, sku","'$idProducto', '$id_color', 1, '$nombre', '$skuHijo','globalpromoitems', 50, '$stock', 1, NOW(), '$imagenProductoHijo', '$nombre', '$skuHijo'");

                    echo "------------- Se inserto NUEVO PRODUCTO HIJO: nombre=".$nombre."  sku= ".$sku. " color =".$color." <br>";
                    $cantidadProductoshijo ++;
                    $insertados++;
                }

                if ($stock == "" || $stock == null) {
                    $stock = 0;
                }

                if ($nombre != $consultaProductoHijo[0][0] || $id_color != $consultaProductoHijo[0][1] || $stock != $consultaProductoHijo[0][2] || $imagenProductoHijo != $consultaProductoHijo[0][3]) {

                    echo "Nombres: ". $nombre. ' = ' .$consultaProductoHijo[0][0] . "<br>".
                    "Id color: ".$id_color. ' = ' .$consultaProductoHijo[0][1] ."<br>". 
                    "Stock: ".$stock. ' = ' .$consultaProductoHijo[0][2] ."<br>".
                    "Imagen1: ".$imagenProductoHijo. ' = ' .$consultaProductoHijo[0][3] ."<br>";

                    $update = update_bd("productos_detalles","nombre_api = '$nombre', color_producto_id = '$id_color', stock = '$stock', imagen1 = '$imagenProductoHijo'","sku_api = '$skuHijo'");
                    if($update === true){
                        echo "Se ACTUALIZO PRODUCTO HIJO: nombre= ".$nombre."  sku= ".$skuHijo. "<br>";
                        $actualizados++;
                        $cantidadProductoshijo ++;
                    }
                }
                
                // echo "Detalle <br>".     
                // $id_color."<br>".
                // $color."<br>".
                // $stock."<br>".
                // $imagenProductoHijo."<br>";

                // array_push($arrayColor, $color);
            }
        
        }
        // $resultado = array_unique($arrayColor);
        // print_r($resultado);
        echo '<h2>Productos madre Afectados: '.$cantidadProductosMadre.'</h2>
        <h2>Productos Hijos Afectados: '.$cantidadProductoshijo.'</h2>
        <h2>Productos Insertados: '.$insertados.'</h2>
        <h2>Productos Actualizados: '.$actualizados.'</h2>';
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Insertando Cdopromocionales</title>
		<meta charset="utf8">
	</head>
	<body>
	</body>
</html>