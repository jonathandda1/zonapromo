<?php 

include("../admin/conf.php");

$pedido = consulta_bd("retiro_en_tienda, fecha, total, cantidad_productos, payment_type_code, payment_type_code, factura, fecha_creacion, despacho","pedidos","estado_id IN (2,4,6,7)","id desc limit 100");
$cant = mysqli_affected_rows($conexion);
	
	/* output in necessary format */
header('Content-type: text/xml');

echo '<pedidos>';
    echo '<Columnas_afectadas>'.$cant.'</Columnas_afectadas>';
	for($i=0; $i<sizeof($pedido); $i++) {
		if($pedido[$i][4] != ""){$tipoPago = $pedido[$i][4];} else {$tipoPago = $pedido[$i][5];}
		
		echo '<pedido>';
			echo '<retiro_en_tienda>'.$pedido[$i][0].'</retiro_en_tienda>';
			echo '<fecha>'.$pedido[$i][1].'</fecha>';
			echo '<total_sin_despacho>'.$pedido[$i][2].'</total_sin_despacho>';
            echo '<valor_despacho>'.$pedido[$i][8].'</valor_despacho>';
			echo '<cant_productos>'.$pedido[$i][3].'</cant_productos>';
			echo '<tipo_pago>'.$tipoPago.'</tipo_pago>';
			echo '<factura>'.$pedido[$i][6].'</factura>';
			echo '<fecha_creacion>'.$pedido[$i][7].'</fecha_creacion>';
		echo '</pedido>';	
	}
echo '</pedidos>';

	/* disconnect from the db */
	@mysqli_close($conexion);
?>