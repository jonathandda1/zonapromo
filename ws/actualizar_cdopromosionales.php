<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
ini_set('max_execution_time', 0);
include('../admin/conf.php');
$data = file_get_contents("http://api.chile.cdopromocionales.com/v1/products?auth_token=4OyJFDM5YkYNmITY_7hYLQ");
$productos = json_decode($data, true);

$cantidadProductosMadre = 0;
$cantidadProductoshijo = 0;
$actualizados = 0;
$insertados = 0;

foreach ($productos as $producto) {
//     echo '<pre>';
// print_r($producto);
// echo '</pre>';
    $sku = $producto["code"];
    $nombre1 = $producto["name"];
    $descripcion1 = $producto["description"];
    $descripcion = preg_replace("/'/", '', $descripcion1);
    $nombre = preg_replace("/'/", '', $nombre1);
    $nombreCategoria = $producto["category"];
    $detalles = $producto["variants"];
    $thumbsProductoMadre = $producto["variants"][0]["picture"]["medium"];
    $consultaCategoria = consulta_bd("id","categorias_api","nombre = '$nombreCategoria'","");
    if ($consultaCategoria <= 0) {
        $insertandoCategoria =  insert_bd("categorias_api","publicada, nombre, api","1, '$nombreCategoria','cdopromosionales'");
    }

    $consultaProductoMadre = consulta_bd("nombre_api, descripcion, thumbs","productos","sku_api = '$sku'","");

    if ($consultaProductoMadre <= 0) {
        $insertandoProductoMadre =  insert_bd("productos","publicado, nombre_api, api, marca_id, thumbs, fecha_creacion, descripcion, sku_api, nombre, codigo_producto_madre","1, '$nombre','cdopromosionales', 0, '$thumbsProductoMadre', NOW(), '$descripcion', '$sku', '$nombre', '$sku'");
        $idProducto = mysqli_insert_id($conexion);
        $cantidadProductosMadre++;
        $insertados++;
        echo "Se inserto NUEVO PRODUCTO: nombre=".$nombre."  sku= ".$sku. " id =".$idProducto." <br>";
    }
    if ($nombre != $consultaProductoMadre[0][0] || $descripcion != $consultaProductoMadre[0][1] || $thumbsProductoMadre != $consultaProductoMadre[0][2]) {

        echo "Nombres: ". $nombre. ' = ' .$consultaProductoMadre[0][0] . "<br>". 
         "Descripciones: ".$descripcion. ' = ' .$consultaProductoMadre[0][1] ."<br>".
         "Thumbs: ".$thumbsProductoMadre. ' = ' .$consultaProductoMadre[0][2] ."<br>";

        $update = update_bd("productos","nombre_api = '$nombre', thumbs = '$thumbsProductoMadre', descripcion = '$descripcion'","sku_api = '$sku'");
        if($update === true){
            echo "Se ACTUALIZO PRODUCTO: nombre= ".$nombre."  sku= ".$sku. "<br>";
            $actualizados++;
            $cantidadProductosMadre++;
        }
    }
    // echo 
    // $sku."<br>".
    // $nombre."<br>".
    // $descripcion."<br>".
    // $nombreCategoria."<br>".
    // $thumbsProductoMadre."<br>";
    foreach ($detalles as $detail) {
        $color = $detail["color"];
        $stock = $detail["stock_available"];
        $imagenesProductoMadreChica = $detail["picture"]["small"];
        $imagenesProductoMadreGrande = $detail["picture"]["medium"];
        $imagenesProductoHijoChica = $detail["detail_picture"]["small"];
        $imagenesProductoHijoGrande = $detail["detail_picture"]["medium"];
        $consultaColor = consulta_bd("id,nombre","color_productos","nombre = '$color'","");
        $colorSKU = preg_replace("/ /", '-', $color);
        $skuHijo = $sku."-".$colorSKU;
        if ($consultaColor > 0) {
            $id_color = $consultaColor[0][0];
        }else{
            $insertandoColor =  insert_bd("color_productos","nombre, fecha_creacion","'$color', NOW()");
            $id_color = mysqli_insert_id($conexion);
        }
        $consultaProductoHijo = consulta_bd("nombre_api, color_producto_id, stock, imagen1, imagen2","productos_detalles","sku_api = '$skuHijo'","");
        if ($consultaProductoHijo <= 0) {
            // echo "Id del producto madre = ". $idProducto . "y el id color = ". $id_color ."<br>";
            $insertandoProductoHijo =  insert_bd("productos_detalles","producto_id, color_producto_id, publicado, nombre_api,sku_api, api, cotizacion_minima, stock, venta_minima, fecha_creacion, imagen1, imagen2, nombre, sku","'$idProducto', '$id_color', 1, '$nombre', '$skuHijo','cdopromosionales', 50, '$stock', 1, NOW(),'$imagenesProductoHijoGrande', '$imagenesProductoMadreGrande', '$nombre', '$skuHijo'");
            echo "------------- Se inserto NUEVO PRODUCTO HIJO: nombre=".$nombre."  sku= ".$sku. " color =".$color." <br>";
                    $cantidadProductoshijo ++;
                    $insertados++;
        }
        if ($stock == "" || $stock == null) {
            $stock = 0;
        }

        if ($nombre != $consultaProductoHijo[0][0] || $id_color != $consultaProductoHijo[0][1] || $stock != $consultaProductoHijo[0][2] || $imagenesProductoHijoGrande != $consultaProductoHijo[0][3] || $imagenesProductoMadreGrande != $consultaProductoHijo[0][4]) {

            echo "Nombres: ". $nombre. ' = ' .$consultaProductoHijo[0][0] . "<br>".
            "Id color: ".$id_color. ' = ' .$consultaProductoHijo[0][1] ."<br>". 
            "Stock: ".$stock. ' = ' .$consultaProductoHijo[0][2] ."<br>".
            "Imagen1: ".$imagenesProductoHijoGrande. ' = ' .$consultaProductoHijo[0][3] ."<br>".
            "Imagen2: ".$imagenesProductoMadreGrande. ' = ' .$consultaProductoHijo[0][4] ."<br>";

            $update = update_bd("productos_detalles","nombre_api = '$nombre', color_producto_id = '$id_color', stock = '$stock', imagen1 = '$imagenesProductoHijoGrande' , imagen2 = '$imagenesProductoMadreGrande'","sku_api = '$skuHijo'");
            if($update === true){
                echo "Se ACTUALIZO PRODUCTO HIJO: nombre= ".$nombre."  sku= ".$skuHijo. "<br>";
                $actualizados++;
                $cantidadProductoshijo ++;
            }
        }
        // echo "Detalle".     $color."<br>".
        // $stock."<br>".
        // $imagenesProductoMadreChica."<br>".
        // $imagenesProductoMadreGrande."<br>".
        // $imagenesProductoHijoChica."<br>".
        // $imagenesProductoHijoGrande."<br>";
    }

}

echo '<h2>Productos madre Afectados: '.$cantidadProductosMadre.'</h2>
        <h2>Productos Hijos Afectados: '.$cantidadProductoshijo.'</h2>
        <h2>Productos Insertados: '.$insertados.'</h2>
        <h2>Productos Actualizados: '.$actualizados.'</h2>';
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Actualizar Globalpromoitems</title>
		<meta charset="utf8">
	</head>
	<body>
	</body>
</html>