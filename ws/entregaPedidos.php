<?php
//include('../admin/conf.php');
error_reporting(E_ALL);
ini_set('display_errors', '0');
date_default_timezone_set('America/Santiago');

    $die_start = "<p style='font-family: verdana;'><span style='color:red;font-weight:bold;'>Error de base de datos:</span> <br /><br />";
	$die_end = "</p>";

	/*$host = 'localhost';
	$user = 'carpinte_mcltda';
	$pass = 'Molde3938';
	$base = 'houseofcars';
*/	

    $host = '127.0.0.1';
	//Usuario de la base de datos.
	$user = 'root';	
	//Paswrod del usuario.
	$pass = '';	
	//Base de datos a trabajar.
	$base = 'houseofcars';

    $conexion = mysqli_connect("$host", "$user", "$pass", "$base");
	if (!$conexion) {
		echo "Error: No se pudo establecer una conexion con la base de datos:." . PHP_EOL;
		echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
		exit;
	} else {
		//echo "Éxito: Se realizó una conexión apropiada a MySQL! La base de datos mi_bd es genial." . PHP_EOL;
		//echo "Información del host: " . mysqli_get_host_info($conexion) . PHP_EOL;
	}

	mysqli_query ($conexion, "SET NAMES 'utf8'");



include('../admin/includes/tienda/cart/inc/functions.inc.php');

$id = (is_numeric($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
$oc = (isset($_GET[oc])) ? mysqli_real_escape_string($conexion, $_GET[oc]) : 0;




/* grab the posts from the db */
if($oc != 0){
	//$query = "SELECT * FROM pedidos WHERE id > $id AND estado_id = 3 ORDER BY id desc";
    $query = "SELECT * FROM pedidos WHERE oc = '$oc' and estado_id IN(2,7)  ORDER BY id desc";
    $result = mysqli_query($conexion, $query) or die('Error query:  '.$query);
    
    $cant = mysqli_affected_rows($conexion);
    
} else if($id != 0){
	//$query = "SELECT * FROM pedidos WHERE id > $id AND estado_id = 3 ORDER BY id desc";
    $query = "SELECT * FROM pedidos WHERE id > $id and estado_id IN(2,7) ORDER BY id desc";
    $result = mysqli_query($conexion, $query) or die('Error query:  '.$query);
    $cant = mysqli_affected_rows($conexion);
    
}else{
    //$query = "SELECT * FROM pedidos WHERE estado_id = 3 ORDER BY id desc LIMIT 200";
    $query = "SELECT * FROM pedidos WHERE estado_id IN(2,7) ORDER BY id desc";
    $result = mysqli_query($conexion, $query) or die('Error query:  '.$query);
    $cant = mysqli_affected_rows($conexion);
}
//var_dump($result);


	/* create one master array of the records */
	$posts = array();
	if(mysqli_num_rows($result)) {
		while($post = mysqli_fetch_assoc($result)) {
			$posts[] = array('Pedido'=>$post);
		}
	}


	//$query = consulta_bd('medio_de_pago, payment_type_code, mp_payment_type, mp_payment_method, shares_number, mp_cuotas', 'pedidos', "oc = '$oc'", '');


	function tipo_pago_ws($tipo_pago,$method){
		if ($method == 'tbk') {
			if ($tipo_pago == 'VN' OR $tipo_pago == 'VC' OR $tipo_pago == 'SI' OR $tipo_pago == 'CI') {
				$return = 'credit_card';
			}elseif($tipo_pago == 'VD'){
				$return = 'debit_card';
			}
		}else{
			if ($tipo_pago == 'bank_transfer' OR $tipo_pago == 'debit_card') {
				$return = 'debit_card';
			}else{
				$return = 'credit_card';
			}
		}
	    return $return;
	}


	$row = mysqli_fetch_array($result);

	/* output in necessary format */

	header('Content-type: text/xml; charset=utf-8');
	echo '<?xml version="1.0" encoding="UTF-8"?>';
    echo '<pedidos>';

    echo '<Columnas_afectadas>'.$cant.'</Columnas_afectadas>';
		foreach($posts as $index => $post) {
			if(is_array($post)) {
				foreach($post as $key => $value) {
					echo '<'.$key.'>';
					if(is_array($value)) {
	
                        foreach($value as $tag => $val) {
							if($tag == 'giro'){
								if($val == ''){
									$val = 'Particular';
								}else{
									$val = $val;
								}
							}
                            if($tag == 'fecha_despacho'){
                                  echo '<fechaEntrega>'.$val.'</fechaEntrega>';
                                }
                            
							if($tag == 'estado_id'){
								if($val == 1){
									$estado = 'Pendiente';
								} else if($val == 2){
									$estado = 'Pagado';
								} else if($val == 3){
									$estado = 'Rechazado';
								} else {
                                    $estado = 'Sin estado';
                                }
                                
                                
				                echo '<'.$tag.'>'.utf8_encode($estado).'</'.$tag.'>';
							} else if($tag == 'cantidad_productos'){
								 $query2 = "SELECT productos_detalle_id, precio, precio_final, codigo, cantidad FROM productos_pedidos WHERE pedido_id =".$value['id']." ORDER BY id";
								$result2 = mysqli_query($conexion, $query2) or die('Error query:  '.$query2);
								$cant = mysqli_affected_rows($conexion); 
								echo '<productosComprados>';
									while ($row=mysqli_fetch_array($result2)) {
										echo '<producto>';
											echo '<skuComprado>'.utf8_encode($row['codigo']).'</skuComprado>';
											echo '<qty>'.$row['cantidad'].'</qty>';
											echo '<precioUnitario>'.round($row['precio']).'</precioUnitario>';
											echo '<precioTotal>'.round($row['precio_final']).'</precioTotal>';
											if (!is_null($row['codigo_pack'])) {
												echo '<codigoPack>'.$row['codigo_pack'].'</codigoPack>';
											}else{
												echo '<codigoPack/>';
											}
										echo '</producto>';
									}
								echo '</productosComprados>';
								
								
							}  else {
								if($val == ''){
									$val = 'vacio';
								} else {
									$val = html_entity_decode($val);
								}
								echo '<'.$tag.'>'.$val.'</'.$tag.'>';
							}
							
                        }
					}
					$method = ($query[0][0] == 'webpay') ? 'tbk' : 'mpago';
					$payment_code = ($query[0][0] == 'webpay') ? $query[0][1] : $query[0][2];
					$method_payment = ($query[0][0] == 'webpay') ? '' : $query[0][3];
					$cuotas = ($query[0][0] == 'webpay') ? $query[0][4] : $query[0][5];

					$payment_method_gl = tipo_pago_ws($payment_code, $method);

					echo '<tipo_pago>'. $payment_method_gl .'</tipo_pago>';
					echo '<metodo_pago>'. $method_payment .'</metodo_pago>';
					echo '<cuotas>'. $cuotas .'</cuotas>';
					echo '</'.$key.'>';
				}
                
			}
            
		}

		echo '</pedidos>';
	

	/* disconnect from the db */
	mysqli_close($conexion);
//}



 
?>


