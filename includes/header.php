<!-- Header -->
<header>
    <!-- Franja superior -->
    <div class="filaSuperiorHeader">
        <div class="contenedor_headerSuperior">
            <div class="izInfoHeader">
                <i class="iconoCel"><img src="img/icono_telefono.png" alt="Imagen de telefono"></i>
                <span><a href="tel:+56222193484">+56222193484</a></span>

            </div>
            <!--fin izInfoHeader -->

            <div class="deInfoHeader">
                <i class="iconoCorreo"><img src="img/icono_correo.png" alt="Imagen de Sobre de correo"></i>
                <a href="mailto:contacto@zonapromo.cl">contacto@zonapromo.cl</a>
            </div>
            <!--fin izInfoHeader -->

        </div>
    </div>
    <!--fin Franja superior -->

    <div class="segundaFilaHeader">
        <div class="contenedor_header">
            <section class="contLogo">
                <!-- Logo -->
                <picture class="logo">
                    <a href="home">
                        <img src="img/logo.png" alt="Imagen logo">
                    </a>
                </picture>
                <!--Fin logo-->
            </section>

            <!--Contenedor Menu y buscador-->
            <section class="contMenuBuscador">
                <div class="filaLogobuscador">
                    <div class="barraBuscador">
                        <!-- Buscador -->
                        <div class="buscador">
                            <form id="formBuscador" method="get" action="busquedas" style="float:none;">
                                <input type="text" name="busqueda" class="campo_buscador" placeholder="<?php if (isset($_GET['buscar'])) {
                                    echo $_GET['buscar'];
                                } else {
                                    echo 'Busca por categorías, productos, marcas y mucho más';
                                } ?>">
                                <a href="javascript:void(0)" id="cerrarBuscadorHeader"><i class="material-icons">close</i></a>
                                <input class="btn_search" type="submit" value=" " name="b">
                            </form>
                        </div>
                    </div>
                    <!-- Fin icono-header-right3-->
                    <aside class="iconosHeader">
                        <?php
                        if(isset($_COOKIE['usuario_id'])){
                            echo '
                            <a href="productos-guardados" class="icono-header-right2 contWish">
                            <div class="icon"><span class="material-icons">favorite_border</span></div>';
                            if(qty_wish()){
                                echo'<span class="cantWish">'.qty_wish().'</span></a>';
                            } 
                        }else{
                        echo '
                                <a href="favoritos" class="icono-header-right2 contWish">
                            <div class="icon"><span class="material-icons">favorite_border</span></div>';
                            if(qty_wish()){
                                echo'<span class="cantWish">'.qty_wish().'</span></a>';
                            } 
                        }
                        ?>
                        <a class="icono-header-right" id="elementosCarro"><span>
                            <!-- <i class="material-icons">shopping_cart</i> -->
                            <img src="img/shopping_cart_black_24dp.svg" alt="">
                            <!-- <i class="fas fa-shopping-basket"></i> -->
                            <?php if (qty_pro() > 0) { ?>
                                <span class="badge cantItems"><?= qty_pro(); ?></span>
                            <?php } ?>
                            </span>
                        </a>  
                        <?php
                        if(isset($_COOKIE['usuario_id'])){
                            echo '
                                <a class="icono-header-right" href="mi-cuenta">
                                    <i class="material-icons">person_outline</i>
                                </a>';
                        }else{
                            echo '
                            <a class="icono-header-right" href="inicio-sesion">
                                <i class="material-icons">person_outline</i>
                            </a>';
                        }
                        ?>                              
                    </aside>
                </div>
                <!--Fin fila logo2 -->
                <!-- Menu -->
                <?php
                $lineas = consulta_bd("id, nombre,icono", "lineas", "publicado = 1", "posicion asc");
                $lineasMenu = consulta_bd("id, nombre", "lineas", "publicado = 1 AND destacada_home = 1", "posicion asc limit 5");
                ?>
                <div class="filaLogo2">
                    <nav>
                        <div class="contBtnMenuResponsive">
                            <div class="hamburger">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                        <!--fin contBtnMenuResponsive-->
                        <ul class="menuFijo">
                        <li><a class="menuA" href="javascript:void(0)">
                            <i class="material-icons">menu</i>
                            <span>Menú Categorías</span></a>
                                <ul>
                                    <?php
                                    /*====================================
                                    =            CARGA LINEAS            =
                                    ====================================*/
                                    foreach ($lineas as $lid) { 
                                        $iconoLinea = $lid[2];
                                        if ($iconoLinea == "" || $iconoLinea == null) {
                                            $iconoLinea= "general.png";
                                        }
                                        ?>
                                        <li><a title="<?= $lid[1];?>" class="lineasA" href="lineas/<?= $lid[0]; ?>/<?= url_amigables($lid[1]); ?>"><i class="iconosLineas"><img src="imagenes/lineas/<?= $iconoLinea; ?>" alt="Icono de lineas"></i><?= preview($lid[1], 30);?></a>
                                            <?php $categoriasMenu = consulta_bd("id, nombre", "categorias", "publicada = 1 AND nombre != '' AND linea_id = $lid[0]", "posicion ASC, nombre asc"); ?>
                                            <?php if ($categoriasMenu) { ?>
                                                <ul class="categoriasUL">
                                                    <?php foreach ($categoriasMenu as $ca) {
                                                        /*===================================
                                                         =         Categorias            =
                                                         ===================================*/
                                                    ?>
                                                        <li class="categoriasLI">
                                                            <a class="categoriasA" href="categorias/<?= $ca[0]; ?>/<?= url_amigables($ca[1]); ?>"><?= $ca[1] ?></a>

                                                            <?php $subCategorias = consulta_bd("id, nombre", "subcategorias", "publicada = 1 AND nombre != '' AND categoria_id = $ca[0]", "posicion ASC, nombre asc"); ?>
                                                            <?php if ($subCategorias) {
                                                            ?>
                                                                <ul>
                                                                    <?php foreach ($subCategorias as $sub) {
                                                                        /*============================
                                                                         =     Subcategorias         =
                                                                         =============================*/
                                                                    ?>
                                                                        <li>
                                                                            <a class="subCategoriasA" href="subcategorias/<?= $sub[0]; ?>/<?= url_amigables($sub[1]); ?>"><?= $sub[1]; ?>

                                                                        </li>
                                                                    <?php }
                                                                    /*==End Subcategorias =*/
                                                                    ?>
                                                                </ul>
                                                            <?php } ?>
                                                        </li>
                                                    <?php }
                                                    /*=====  End Categorias  ======*/
                                                    ?>
                                                </ul>
                                            <?php } ?>
                                        </li>
                                    <?php }
                                    /*=====  End of CARGA LINEAS  ======*/
                                    ?>
                                </ul>
                            </li>
                            <?php
                                /*====================================
                                =       CARGA LINEAS DESTACADAS  =
                                ====================================*/
                                foreach ($lineasMenu as $liMe) { ?>
                                    <li title="<?= $liMe[1]; ?>" class="lineasDestacadas" ><a href="lineas/<?= $liMe[0]; ?>/<?= url_amigables($liMe[1]); ?>"><span><?= preview($liMe[1], 25);?></span></a></li>
                                <?php } ?>        
                        </ul><!-- fin de menuFijo -->
                    </nav>
                </div>
                <!--fin filaLogo2-->


                <!-- barra de buscar responsiva -->
                <div class="cont100 contBuscadorResponsive">
                    <div class="buscador buscadorResponsive">
                        <form method="get" action="busquedas" style="float:none;">
                            <input type="text" name="busqueda" class="campo_buscador" placeholder="<?php if (isset($_GET['buscar'])) {
                                                                                                        echo $_GET['buscar'];
                                                                                                    } else {
                                                                                                        echo '¿Qué estas buscando?';
                                                                                                    } ?>">

                            <a href="javascript:void(0)" id="cerrarBuscadorHeader"><i class="material-icons">close</i></a>
                            <input class="btn_search" type="submit" value=" " name="b">

                        </form>
                    </div>
                </div>
                <!--contBuscadorResponsive -->


                <div style="clear:both"></div>


                <!--menu responsive  ////////////////////////////////////////////////////////////////////////////////-->
                <div class="contMenu menuParaResponsive">
                    <ul class="menu">
                        <?php for ($i = 0; $i < sizeof($lineas); $i++) { ?>
                            <li>
                                <a href="lineas/<?= $lineas[$i][0] . "/" . url_amigables($lineas[$i][1]); ?>" class="img-menu">
                                    <span><?= $lineas[$i][1]; ?></span>
                                    <!--<span class="iconoMenu">
                                <i class="material-icons escritorio">keyboard_arrow_right</i>
                            </span>-->
                                </a>
                                <?php
                                $linea_id = $lineas[$i][0];
                                $categorias = consulta_bd("id, nombre", "categorias", "linea_id = $linea_id and publicada = 1", "id asc");
                                $catCat = mysqli_affected_rows($conexion);
                                if ($catCat > 0) {
                                ?>
                                    <i class="material-icons movil lineaIcono">keyboard_arrow_right</i>
                                    <ul class="submenuCategoria sub">
                                        <li class="volverMenuMovil volverLineas">
                                            <a href="javascript:void(0)">
                                                <i class="material-icons">keyboard_arrow_left</i>
                                                <span>Volver</span>
                                            </a>
                                        </li>
                                        <?php
                                        for ($j = 0; $j < sizeof($categorias); $j++) {
                                        ?>
                                            <li>
                                                <a href="categorias/<?php echo $categorias[$j][0]; ?>/<?php echo url_amigables($categorias[$j][1]); ?>"><?php echo $categorias[$j][1]; ?></a>
                                            </li>
                                        <?php } //fin ciclo categorias 
                                        ?>

                                    </ul>
                                <?php } //fin condicion de cantidad de categorias 
                                ?>
                            </li>
                        <?php } //Fin ciclo lineas 
                        ?>
                        <li><a href="nosotros"><span>Somos</span></a></li>
                        <!-- <li><a class="btnMenuOfertas" href="ofertas">Ofertas</a></li> -->
                        <!--<li>
                        <a class="btnSeguimiento" href="javascript:void(0)">
                            <span>Siguir pedido</span>
                        </a>
                    </li> -->
                    </ul>
                </div>
                <!--fin menu responsive ////////////////////////////////////////////////////////////////////////////////-->

                <div class="contMenuEscritorio">
                    <ul class="menuLineas">
                        <?php for ($i = 0; $i < sizeof($lineas); $i++) { ?>
                            <li rel="<?= $lineas[$i][0]; ?>" class="ItemMenuEscritorio">
                                <a href="lineas/<?= $lineas[$i][0] . "/" . url_amigables($lineas[$i][1]); ?>" class="img-menu">
                                    <span><?= $lineas[$i][1]; ?></span>
                                    <?php
                                    $linea_id = $lineas[$i][0];
                                    $categorias = consulta_bd("id, nombre", "categorias", "linea_id = $linea_id and publicada = 1", "id asc");
                                    $catCat = mysqli_affected_rows($conexion);
                                    if ($catCat > 0) {
                                    ?>
                                        <i class="material-icons lineaIcono">keyboard_arrow_right</i>
                                    <?php } ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <!--fin menu -->

                    <div class="contCategoriasEscritorio">
                        <?php $catGrilla = consulta_bd("id, nombre, thumbs, linea_id", "categorias", "publicada = 1", "posicion asc"); ?>
                        <h2 id="nombreLineaMenu"></h2>

                        <?php for ($i = 0; $i < sizeof($catGrilla); $i++) { ?>
                            <a rel="<?= $catGrilla[$i][3]; ?>" href="categorias/<?= $catGrilla[$i][0]; ?>/<?= url_amigables($catGrilla[$i][1]); ?>" class="grillaCategoriaMenu" style="display:none;">
                                <!-- <span class="imgCatMenu">
                                    <img src="imagenes/categorias/<?= $catGrilla[$i][2]; ?>">
                                </span> -->
                                <span class="nomCategoriaMenu"><?= $catGrilla[$i][1]; ?></span>
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <!--fin contMenuEscritorio-->
            </section>
            <!--fin contenedor Menu y buscador-->
        </div>
        <!--fin contenedor header-->
    </div>
    <!--fin Segunda Fila header-->

</header><!-- fin de header -->