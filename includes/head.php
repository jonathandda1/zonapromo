<!DOCTYPE html>
<html lang="es">
<head>
    <?php include("includes/metatags.php"); ?>
    <?php include("includes/titulos.php"); ?>
    
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=DM+Serif+Display:ital@0;1&display=swap" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    
    <script src="js/jquery.min.js"></script>
    <!-- Recaptcha de google -->
    <script src="https://www.google.com/recaptcha/api.js?render=6LfZoiIeAAAAALbEyRM_H7eXP3O2X86UJ06wA3lV"></script>

	<!-- Sweet alert -->
    <script type="text/javascript" src="js/sweetalert.js"></script>
    <link href="css/sweetalert2.min.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<?php 
	$base = file_get_contents('tienda/base_tienda.css');
	echo '<style type="text/css">';
	echo minify_css($base);
	echo '</style>';
	
	$estilos = file_get_contents('css/estilos.css');
	echo '<style type="text/css">';
	echo minify_css($estilos);
	echo '</style>';

    $base2 = file_get_contents('css/estilosjd.css');
	echo '<style type="text/css">';
	echo minify_css($base2);
	echo '</style>';
	
	$uniform = file_get_contents('css/agent.css');
	echo '<style type="text/css">';
	echo minify_css($uniform);
	echo '</style>';
	
	$baseResp = file_get_contents('tienda/base_responsive.css');
	echo '<style type="text/css">';
	echo minify_css($baseResp);
	echo '</style>';
?>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <script src="js/jquery.cycle2.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <script src="js/jquery.ui.core.min.js"></script>
    <script src="js/jquery.ui.widget.min.js"></script>
    <script src="js/jquery.ui.button.min.js"></script>
    <script src="js/jquery.ui.spinner.min.js"></script>
    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	//ga('create', 'UA-65024256-46', 'auto');
	ga('require', 'ec');
	ga('send', 'pageview');
	
	function productClick(sku, nombre_producto,categoria, variante, posicion, lista, marca ) {
	  ga('ec:addProduct', {
	    'id': sku,
	    'name': nombre_producto,
	    'category': categoria,
	    'brand': marca,
	    'variant': variante,
	    'position': posicion
	  });
	  ga('ec:setAction', 'click', {list: lista});
	  ga('send', 'event', 'UX', 'click', 'Results', {
	      hitCallback: function() {
	      }
	  });
	};
  
	function addToCart(sku, nombre_producto,categoria, variante, precio, cantidad, marca) {
	 ga('ec:addProduct', {
	    'id': sku,
	    'name': nombre_producto,
	    'category': categoria,
	    'brand': marca,
	    'variant': variante,
	    'price': precio,
	    'quantity': cantidad
	  });
	  ga('ec:setAction', 'add');
	  ga('send', 'event', 'UX', 'click', 'add to cart');     // Send data using an event.
	}
</script>

    <?php if ($op == 'clientes-vendedor') { ?>
    <!-- DataTable -->
    <!-- <link href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <?php }else if($op == 'cotizaciones-vendedor'){ ?>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>  
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.js"></script>
          
    <?php } ?>
    <?php if($op == 'home' || $op == 'ficha' || $op == '404' || $op == 'busqueda'){?> 
<!-- Owl Stylesheets -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    
    <!-- javascript -->
    <script src="js/owl.carousel.js"></script>

    <script>
    $(function(){
       $('.owl-carousel1').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
           dots: false,
            responsive:{
                0:{
                    items:2
                },
                500:{
                    items:3
                },
                700:{
                    items:4
                },
                900:{
                    items:6
                }
            }
        });
        
        $('.owl-bannerMarcas').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
           dots: false,
            responsive:{
                0:{
                    items:2
                },
                400:{
                    items:3
                },
                600:{
                    items:4
                },
                700:{
                    items:4
                },
                700:{
                    items:5
                },
                900:{
                    items:6
                },
                1050:{
                    items:8
                }
            }
        });

        $('.owl-banner1').owlCarousel({
            loop:true,
            margin:1,
            nav:true,
           dots: false,
            responsive:{
                0:{
                    items:1
                },
                500:{
                    items:1
                },
                700:{
                    items:1
                },
                900:{
                    items:1
                }
            }
        }); 
        
        $('.owl-carouselProductos').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                500:{
                    items:2
                },
                700:{
                    items:3
                },
                900:{
                    items:4
                },
                1100:{
                    items:5
                }
            }
        }); 
    });  
    </script>    
  <?php } ?>  
    

<script type="text/javascript" src="js/jquery.label_better.js"></script>
    
<!--agrego el chat en el caso que el cliente lo cargo en su pagina de configuracion -->
<?= opciones("chat"); ?>
</head>