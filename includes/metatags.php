<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <base href="<?= $url_base; ?>" />
    <LINK REL="SHORTCUT ICON" HREF="favicons/favicon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="faviconsapple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-touch-icon-114x114.png">
    
    <META NAME="Author" CONTENT="Moldeable S.A.">
    <META NAME="DC.Author" CONTENT="Moldeable S.A.">
    <META NAME="DC.Creator" CONTENT="Moldeable S.A.">
    <META NAME="Generator" CONTENT="Moldeable CMS">
    <META NAME="authoring.tool" CONTENT="Moldeable CMS">
    <META NAME="VW96.objecttype" CONTENT="Homepage">
    <META NAME="resource-type" CONTENT="Homepage">
    <META NAME="doc-type" CONTENT="Homepage">
    <META NAME="Classification" CONTENT="General">
    <META NAME="RATING" CONTENT="General">
    <META NAME="Distribution" CONTENT="Global">
    <META NAME="Language" CONTENT="Spanish">
    <META NAME="DC.Language" SCHEME="RFC1766" CONTENT="Spanish">
    <META NAME="Robots" CONTENT="index,follow">
    <META NAME="Revisit-after" CONTENT="15 days">
    <META NAME="CREDITS" CONTENT="Diseño y Prgramación: Moldeable S.A., www.moldeable.com">
    
	<?php if($op == "ficha"){ 
		$idProd = (isset($_GET[id])) ? mysqli_real_escape_string($conexion, $_GET[id]) : 0;
		$prod = consulta_bd("nombre, descripcion, thumbs, descripcion_seo, keywords","productos","id = $idProd","");
	?>
        <meta property="og:title" content="<?= htmlspecialchars($prod[0][0]); ?>" />
        <?php if($prod[0][3] != ""){ ?>
        <meta property="og:description" content="<?= htmlspecialchars($prod[0][3]); ?>" />
        <?php } else { ?>
        <meta property="og:description" content="<?= htmlspecialchars(strip_tags($prod[0][1])); ?>" />
        <?php } ?>
        <meta property="og:image" content="<?php echo $url_base; ?>imagenes/<?= $prod[0][3]; ?>" />
        <meta property="og:site_name" content="<?= opciones("url_sitio"); ?>" />
        <link rel="canonical" href="<?= obtenerURL(); ?>">
        
		<?php if($prod[0][4] != ""){ ?>
        <META NAME="Keywords" CONTENT="<?= $prod[0][4]; ?>">
		<?php } else { ?>
		<META NAME="Keywords" CONTENT="<?= opciones("seo_keywords"); ?>">
		<?php } ?>
        
    <?php } else { ?>
        <META NAME="Title" CONTENT="<?= opciones("seo_titulo"); ?>">
        <META NAME="DC.Title" CONTENT="<?= opciones("seo_titulo"); ?>">
        <META http-equiv="title" CONTENT="<?= opciones("seo_titulo"); ?>">
        <META NAME="DESCRIPTION" CONTENT="<?= opciones("seo_descripcion"); ?>">
        <META NAME="copyright" CONTENT="<?= opciones("nombre_cliente"); ?>">
        <META NAME="Keywords" CONTENT="<?= opciones("seo_keywords"); ?>">
        <META http-equiv="keywords" CONTENT="<?= opciones("seo_keywords"); ?>">
       
        <meta property="og:title" content="<?= opciones("seo_titulo"); ?>" />
        <meta property="og:description" content="<?= opciones("seo_descripcion"); ?>" />
        <meta property="og:image" content="<?= opciones("seo_url_imagen"); ?>" />
        <meta property="og:site_name" content="<?php echo $url_base; ?>" />
        <link rel="canonical" href="<?= obtenerURL(); ?>">
    <?php } ?>


    <!-- FAVICONS -->
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <!-- <link rel="manifest" href="favicons/site.webmanifest"> -->
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
