<?php /*if($op == 'home'){ ?>
<div class="cont100 contNews">
    <div class="cont100Centro">
        <div class="cont50">
            <span class="tituloNews1">Suscríbete a </span>
            <span class="tituloNews2">nuestro newsletter</span>
        </div>
        <div class="cont50">
            <form method="post" action="ajax/newsletter.php" id="formNewsletter">
                <label>email</label>
                <input type="text" name="newsletter" value="" class="campoNewsletter" id="newsletter" placeholder="Ingrese su correo" />
                <a href="javascript:void(0)" id="btnNewsletter">Suscríbete</a>
            </form>
        </div>
    </div>
</div>
<?php }*/ ?> 


<footer class="cont100 footer">
	<div class="contFooter">
        <div class="colFooter1">
            <img src="img/imgFooter.png" alt="Logo Foooter">
        </div>
       
       <div class="colFooter2">
            <h4 class="accordeonFooter">Contacto</h4>
			<div class="contColumnaOcultaMovil">
                <p>Email:</p>
                <a href="mailto:contacto@zonapromo.cl">contacto@zonapromo.cl</a>
                <p>Teléfono:</p>
                <a href="tel:+56222193484">+56222193484</a>           
            </div>
       </div> 
        
        
        <div class="colFooter3">  
        <h4 class="accordeonFooter">Soporte</h4>
           <div class="contColumnaOcultaMovil">
                <!-- <a href="como-comprar">¿Cómo comprar?</a> -->
                <a href="politicas-de-privacidad">Políticas de privacidad</a>
                <a href="terminos-y-condiciones">Terminos y condiciones</a>
                <!-- <a href="devoluciones">Devoluciones</a> -->
           </div>          
        </div><!--fin columna footer-->
        
        
        <div class="colFooter4">
            <h4 class="accordeonFooter">Empresa</h4>
            <div class="contColumnaOcultaMovil">
                <a href="nosotros">Nosotros</a>
                <a href="contacto">Contacto</a>
                <a href="catalogos">Catálogo</a>
                <?php if(!isset($_COOKIE['Vendedor_id'])){ ?>
                    <a class="fancybox" href="#inline2">Vendedoras</a>
                <?php } else { ?> 
                    <a href="mi-cuenta-vendedores">Vendedoras</a>
                <?php } ?>
                <!-- <a href="encuentranos">Encuéntranos</a> -->
                <!-- <a href="servicios">Servivio Técnico</a>
                <a href="arriendos">Arriendo</a> -->
            </div><!--contColumnaOculta-->
        </div><!--fin columna footer-->
        
                <!-- <label>email</label>
                <input type="text"  placeholder="Ingrese su correo" />
                <a href="javascript:void(0)" >Suscríbete</a>
            
         -->
        
		<div class="colFooter5">
        	<!-- <h4 class="accordeonFooter">Síguenos</h4>
                <div class="footerCompartir">
                    <a href="https://www.facebook.com/">
                        <img src="img/faceFooter.png" alt="Logo de facebook">
                    </a>
                    <a href="https://www.instagram.com/">
                        <img src="img/instaFooter.png" alt="Logo de Instagram">
                    </a>
                </div> -->
            <!-- <div class="cont100">
                <form method="post" id="formNewsletter">
                <div class="eviarNewsLetter">
                    <input name="newsletter" value="" id="newsletter" type="text" class="inputNews campoNewsletter" placeholder="Ingresa tu email..."><button type="button" id="btnNewsletter" class="botonEnviarNews">Enviar</button>
                </div>
                </form>
            </div> -->
        </div><!--fin columna footer-->
        
        
        
        
        
        <div class="logosResponsive">
            <span class="logoFooterCcs"><img src="img/iconoCCS.png"></span>
            <span class="logoFooterIso"><img src="img/iso.png"></span>
           <?php if ($is_cyber): ?>
                <span class="selloFooter">
                    <img src="tienda/selloEmpresaOficial.png" height="45" />
                </span>
                <span class="logoCyberFooter">
                    <img src="tienda/logoCyberFooter.png" width="100%"/>
                </span><br>
            <?php endif ?>
        </div><!--fin logosResponsive -->
        
    </div><!--fin contenedor_footer -->
</footer><!--FIn cont100 -->


<div class="cont100 finalFooter">   
    <section class="cont100Centro">
        <article class="contFinalFooter">
            <p>&copy; Todos los derechos reservados</p> 
            <a class="firma" href="https://moldeable.com" target="_blank">
                <img src="img/firma2.png">
            </a>
        </article>
    </section><!--fin contenedor_footer -->
    
    <div class="notificacionFooter">
        <div class="contMensaje" id="contMensaje">
            
        </div>
        <a class="btnAceptar" href="javascript:void(0)">Aceptar</a>
    </div>
    
</div>

<div id="inline3" class="inicioSesionCarroPopUp" style="width:440px;display: none;">
    <h3>¿Haz olvidado tu contraseña de vendedora?</h3>
    <h5>Ingresa tu email y te enviaremos una nueva contraseña.</h5>
    <form action="ajax/recuperar_clave.php" method="post" id="formRecuperarClave" class="bl_form">
        <input type="text" id="email" name="email" class="campoGrande2 label_better" placeholder="Ingrese su correo" data-new-placeholder="Ingrese su correo" />
        <input type="hidden" name="origenRecuperar" id="origenRecuperar" value="poPup">
        <a href="javascript:void(0)" id="recuperarClaveVendedora" class="btnFormCompraRapida">Solicitar clave</a>
    </form>
</div>

<?php if(!isset($_COOKIE['Vendedor_id'])){ ?>
<div id="inline2" class="inicioSesionCarroPopUp" style="width:440px;display: none;">
    <h3>Inicio sesión Vendedora</h3>
    <h5>Tu cuenta para todo lo relacionado con Zonapromo</h5>
    <form method="post" id="formLoginVendedora" action="ajax/loginVendedor.php">
        <input type="text" name="vendedoraEmail" value="<?php echo $email ?>" class="inputInicioSesion" id="vendedoraEmail" placeholder="Ingrese su correo" />
        <input type="password" name="vendedorClave" value="" id="vendedorClave" class="inputInicioSesion" placeholder="Ingrese Contraseña" />
        <input type="hidden" name="origen" value="vendedoras">
        <a href="javascript:void(0)" id="btnInicioVendedora">Iniciar sesión</a>
    </form>
    <a href="#inline3" class="solicitarAqui2 fancybox">¿Olvidaste tu clave?</a>
</div> 
<?php } ?>