<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version='2.0'
  xmlns:html='http://www.w3.org/TR/REC-html40'
  xmlns:sitemap='http://www.sitemaps.org/schemas/sitemap/0.9'
  xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Mapa del sitio XML</title>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>

  <style type='text/css'>
    @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i');

    body {
      font: 14px 'Open Sans', Helvetica, Arial, sans-serif;
      margin: 0;
    }

    a {
      color: #3498db;
      text-decoration: none;
    }

    h1 {
      margin: 0;
    }

    #description {
      background-color: #FFF;
      color: #333;
      padding: 30px 30px 20px;
    }

    #description a {
      color: #fff;
    }

    #content {
      padding: 10px 30px 30px;
      background: #fff;
    }

    a:hover {
      border-bottom: 1px solid;
    }

    th, td {
      font-size: 14px;
    }

    th {
      text-align: left;
      border-bottom: 1px solid #ccc;
    }

    th, td {
      padding: 10px 15px;
    }

    .odd {
      background-color: #F1F3F4;
    }

    #footer {
      margin: 20px 30px;
      font-size: 12px;
      color: #999;
    }

    #footer a {
      color: inherit;
    }

    #description a, #footer a {
      border-bottom: 1px solid;
    }

    #description a:hover, #footer a:hover {
      border-bottom: none;
    }

    img.thumbnail {
      max-height: 100px;
      max-width: 100px;
    }
  </style>
</head>
<body>
  <div id='description'>
    <h1>Mapa del sitio XML</h1>
    <a href="http://www.moldeable.com" target="blank" style="background:url(https://moldeable.com/img/logo-moldeable.png) no-repeat; width:200;height:31px;display:block" title="Moldeable S.A"></a>
  </div>
  <div id='content'>
    <!-- <xsl:value-of select="count(sitemap:urlset/sitemap:url)"/> -->
    <table>
      <tr>
        <th>#</th>
        <th>URL</th>
        <th>Última modificación</th>
        <th>Prioridad</th>
        <th>Período de cambios</th>
      </tr>
      <xsl:for-each select="sitemap:urlset/sitemap:url">
        <tr>
          <xsl:choose>
            <xsl:when test='position() mod 2 != 1'>
              <xsl:attribute name="class">odd</xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <td>
            <xsl:value-of select = "position()" />
          </td>
          <td>
            <xsl:variable name='itemURL'>
              <xsl:value-of select='sitemap:loc'/>
            </xsl:variable>
            <a href='{$itemURL}'>
              <xsl:value-of select='sitemap:loc'/>
            </a>
          </td>
          <td>
            <xsl:value-of select='sitemap:lastmod'/>
          </td>
          <td>
            <xsl:value-of select='sitemap:priority'/>
          </td>
          <td>
            <xsl:value-of select='sitemap:changefreq'/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </div>
  <div id='footer'>
    <p>Sitio web creado por <a target="blank" href="https://moldeable.com">Moldeable</a></p>
  </div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
