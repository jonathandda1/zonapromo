<header class="headerCarroInterior">
    <div class="contenedor_header">
        <section class="contLogo">
            <!-- Logo -->
            <picture class="logo">
                <a href="home">
                    <img src="img/logo.png" alt="Imagen logo">
                </a>
            </picture>
            <!--Fin logo-->
        </section>
        <section class="contTiltuloHeader2">
            <h1>¡Cotiza con nostros de manera</h1>
            <h1>rápida y fácil en un solo paso!</h1>
        </section>
    </div>
</header>