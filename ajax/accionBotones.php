<?php
    include("../admin/conf.php");
    /*================================================
    =            Recibimos Variables POST            =
    ================================================*/
    $idCotProd = (isset($_POST['idCotProd'])) ? mysqli_real_escape_string($conexion, $_POST['idCotProd']) : 0;
    $accion = (isset($_POST['accion'])) ? mysqli_real_escape_string($conexion, $_POST['accion']) : 0;
    $oc = (isset($_POST['oc'])) ? mysqli_real_escape_string($conexion, $_POST['oc']) : 0;
    $idVendedor = (isset($_POST['idVendedor'])) ? mysqli_real_escape_string($conexion, $_POST['idVendedor']) : 0;
    $rutEmpresa = (isset($_POST['rutEmpresa'])) ? mysqli_real_escape_string($conexion, $_POST['rutEmpresa']) : 0;
    /*=====  End of Recibimos Variables POST  ======*/
    if ($accion == "sinStock") {
        $cantidad = consulta_bd("cantidad, cotizacion_id, precio_unitario_neto, precio_final_neto","productos_cotizaciones","id='$idCotProd'","");
        $idCotizacion = $cantidad[0][1];
        $cantidadGeneral = consulta_bd("cant_productos,total_unitario, total","cotizaciones","id = '$idCotizacion'","");
        $precioUnitarioNeto = $cantidadGeneral[0][1] - $cantidad[0][2];
        $precioFinalNeto = $cantidadGeneral[0][2] - $cantidad[0][3];
        $cantActual = $cantidadGeneral[0][0] - $cantidad[0][0];
        $updateCantidad = update_bd("cotizaciones","cant_productos = $cantActual, total_unitario = '$precioUnitarioNeto', total = '$precioFinalNeto'","id=$idCotizacion");
        
        $update = update_bd("productos_cotizaciones","condicion_boton = 'sin stock', costo_producto = 0, costo_logo = 0, margen_utilidad = 0, precio_unitario_neto = 0, precio_final_neto = 0, utilidad_total = 0, cantidad = 0","id='$idCotProd'");
    }
    if ($accion == "conStock") {
        $cantidad = consulta_bd("cantidad, cotizacion_id, precio_unitario_neto, precio_final_neto","productos_cotizaciones","id='$idCotProd'","");
        $idCotizacion = $cantidad[0][1];
        $cantidadGeneral = consulta_bd("cant_productos,total_unitario, total","cotizaciones","id = '$idCotizacion'","");
        $precioUnitarioNeto = $cantidadGeneral[0][1] + $cantidad[0][2];
        $precioFinalNeto = $cantidadGeneral[0][2] + $cantidad[0][3];
        $cantActual = $cantidadGeneral[0][0] + $cantidad[0][0];
        $updateCantidad = update_bd("cotizaciones","cant_productos = $cantActual, total_unitario = '$precioUnitarioNeto', total = '$precioFinalNeto'","id=$idCotizacion");
        
        
        $update = update_bd("productos_cotizaciones","condicion_boton = ''","id='$idCotProd'");
    }
    if ($accion == "empresa_vendedor") {
        $empresa = consulta_bd("id","empresas","rut='$rutEmpresa'","");
        $idEmpresa = $empresa[0][0];
        $consultarAsociacion = consulta_bd("id","empresas_vendedores","empresa_id=$idEmpresa","");
        $existencia = count($consultarAsociacion);
        if ($existencia > 0) {
            die("existe");            
        }else{
            $asociarEmpresaVendedor = insert_bd("empresas_vendedores","vendedor_id, empresa_id,fecha_creacion","'$idVendedor','$idEmpresa', NOW()");
            $verificar_insert = mysqli_affected_rows($conexion);
            if ($verificar_insert > 0) {
                $updateCantidad = update_bd("cotizaciones","asociada = 1","oc='$oc'");
                die("ok");
            }else{
                die("error");
            }
        }   
    }
    /*===========================================================
    =            Creamos un array para leer por JSON            =
    ===========================================================*/
        // $row = array(
        // 'filaProducto' => join('',$output),
        // 'oc' => $oc
        // );
        // if (is_array($row)) {
        //     echo json_encode($row);
        // }
    /*=====  End of Creamos un array para leer por JSON  ======*/
