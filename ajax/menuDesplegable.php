<?php
    include("../admin/conf.php");
    $id = $_POST['id'];
    $lineas = consulta_bd("id, nombre, icono", "lineas", "publicado = 1 and id = $id", "posicion asc");
    $categoria = consulta_bd("id, nombre, thumbs, linea_id", "categorias", "linea_id = $id and publicada = 1", "posicion asc"); 
    $categoriaDestacada = consulta_bd("id, nombre, thumbs, linea_id", "categorias", "linea_id = $id and publicada = 1 and destacada_home = 1", "posicion asc"); 

    $catMostrar .= ' <div class="menuLineasCategorias" id="menuLineasCategorias">
                        <div class="textoMenu">'; 
    $catMostrar .=    '<h4 class="tituloLineasCat"><a href="lineas/'.$lineas[0][0].'/'.url_amigables($lineas[0][1]).'">'.$lineas[0][1].'</a></h4>';       
    
    for($i=0; $i<sizeof($categoria); $i++) {
        $catMostrar .=  '<p class="textoLineasCat"><a rel="'.$categoria[$i][3].'" href="categorias/'.$categoria[$i][0].'/'.url_amigables($categoria[$i][1]).'">'.$categoria[$i][1].'</a></p>';
    } 
    $catMostrar .=  '</div>
                        <div class="imagenesMenuLineas">';
    for($i=0; $i<sizeof($categoriaDestacada); $i++) { 
            
        $catMostrar .=  '<a class="imgMenuLinea" rel="'.$categoriaDestacada[$i][3].'" href="categorias/'.$categoriaDestacada[$i][0].'/'.url_amigables($categoriaDestacada[$i][1]).'">
                <span><img src="imagenes/categorias/'.$categoriaDestacada[$i][2].'" alt="Imagen de categoria"></span>';
        if (strlen($categoriaDestacada[$i][1])<=13) {
            $catMostrar .=  '<span class="imgTextMenuCat">'.$categoriaDestacada[$i][1].'</span>
            </a>'; 
        }else{
            $catMostrar .=  '<span class="imgTextMenuCat2">'.$categoriaDestacada[$i][1].'</span>
            </a>';
        }

    }
    $catMostrar .=  '</div>
                        </div><!--fin menu lineas Categorias-->';

    echo $catMostrar;
