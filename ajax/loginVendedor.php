<?php 
	include("../admin/conf.php");
	
	$correo = (isset($_POST['vendedoraEmail'])) ? mysqli_real_escape_string($conexion, $_POST['vendedoraEmail']) : 0;
	$clave = (isset($_POST['vendedorClave'])) ? mysqli_real_escape_string($conexion, $_POST['vendedorClave']) : 0;
	$recordarCuenta = (isset($_POST['recordarCuenta'])) ? mysqli_real_escape_string($conexion, $_POST['recordarCuenta']) : 0;
	$origen = (isset($_POST['origen'])) ? mysqli_real_escape_string($conexion, $_POST['origen']) : 0;
	$ruta_retorno = $url_base."home";
	$pass_hash =  md5($clave);
	$pass_encrypted = consulta_bd("password_salt", "vendedores", "email = '$correo'", "");
	$cantVendedores = mysqli_affected_rows($conexion);
	
	if($cantVendedores == 0){
		$error = "Clave y/o usuario invalido";
		header("location:$ruta_retorno?error=$error");
		die();
	}
	
	$password_final = hash('md5',$pass_hash.$pass_encrypted[0][0]);
	$enter  = consulta_bd("id, nombre", "vendedores", "clave = '$password_final' and email = '$correo'", "");
	$cant = mysqli_affected_rows($conexion);
	
	if(count($enter) > 0){
		//guardo los datos en una cookie y la contraseña codificada en base 64
		if($recordarCuenta == true){
			setcookie("nombreVendedor", "$correo", time() + (365 * 24 * 60 * 60), "/");
			setcookie("claveVendedor", base64_encode($clave), time() + (365 * 24 * 60 * 60), "/");
			} else {
			setcookie('nombreVendedor', null, -1, '/');
			setcookie('claveVendedor', null, -1, '/');
			}
		//guardo los datos en una cookie
		$_SESSION["Vendedor"] = $enter[0][0];
		setcookie("Vendedor_id", $enter[0][0], time() + (365 * 24 * 60 * 60), "/");
		header("Location: ../mi-cuenta-vendedores?a=1&msje=Bienvenido ".$enter[0][1]);
		die();
	} else {
		$error = "No fue posible acceder a su cuenta.";	
		header("Location: ../home?a=2&msje=$error");
		die();
	}	
?>
