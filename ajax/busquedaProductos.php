<?php
    include("../admin/conf.php");
    /*================================================
    =            Recibimos Variables POST            =
    ================================================*/
    $busqueda = (isset($_POST['busqueda'])) ? mysqli_real_escape_string($conexion, $_POST['busqueda']) : 0;
    $idCotizacion = (isset($_POST['idCotizacion'])) ? mysqli_real_escape_string($conexion, $_POST['idCotizacion']) : 0;
    $para = (isset($_POST['para'])) ? mysqli_real_escape_string($conexion, $_POST['para']) : 0;
    $idProd = (isset($_POST['idProd'])) ? mysqli_real_escape_string($conexion, $_POST['idProd']) : 0;
    /*=====  End of Recibimos Variables POST  ======*/
    $logo = consulta_bd("l.id,l.nombre", "logo l", "", "l.nombre asc");
    $logoSalida = "";
    if (count($logo) > 0) {
            $logoSalida .= '               
                        <div class="contLabelSelect">
                            <label for="logo-producto">Logo</label>
                            <select name="logoProducto" class="logo-producto">';
        for ($i = 0; $i < sizeof($logo); $i++) {
            $logoSalida .= '<option value="'.$logo[$i][0].'">'.$logo[$i][1].'</option>';
        }  
            $logoSalida .= '</select>
                        </div>';
    } 
    $producto = consulta_bd("pd.imagen1, pd.nombre,pd.id, pd.sku,color_producto_id","productos_detalles pd","pd.nombre like '%$busqueda%' or pd.sku like '%$busqueda%' group by(pd.id)","");
    $cantidadProductos = count($producto);
    if ($cantidadProductos <= 0) {
        $output[] = '<div class="contProductoBuscado">
            <h3>No se encontraron coincidencias</h3>
        </div>';
    }else{
        $output[] = '';
        foreach ($producto as $value) {
        
            if($value[0] != ''){
                $thumbs = $value[0];
            } else {
                $thumbs = "sin-imagen.jpg";
            }
                        
            $output[] .='<div class="contProductoBuscado">';
        
            $output[] .= '
                        <div class="contImgFila">								
                            <a href="ficha/'.$value[2].'/'.url_amigables($value[1]).'">	
                                <img src="'.imagen('imagenes/productos_detalles/', $thumbs).'" width="100%" />
                            </a>
                        </div>
                        <div class="contInfoAgregar">';                        
                    
            $output[] .='<div class="nombreGrilla">
                            <a href="ficha/'.$value[2].'/'.url_amigables($value[1]).'">
                                <span>'.$value[1].'</span>
                            </a>
                        </div>';
            $output[] .='<div class="skuGrilla">
                            <a href="ficha/'.$value[2].'/'.url_amigables($value[1]).'">
                                <span>SKU: '.$value[3].'</span>
                            </a>
                        </div>
                        <div class="cantidadAgregar">
                            <select name="cantidad" class="selectCantidadAgregar cantidadBuscados">
                                <option value="1" selected>01</option>
                                <option value="2">02</option>
                                <option value="3">03</option>
                                <option value="4">04</option>
                                <option value="5">05</option>
                                <option value="6">06</option>
                                <option value="7">07</option>
                                <option value="8">08</option>
                                <option value="9">09</option>
                                <option value="10">10</option>
                                <option value="11">Más de 10</option>
                            </select>
                        </div>';
            $output[] .='
                </div><!-- fin contInfoAgregar-->
            
                <div class="contLogoColor">';
            $output[] .= $logoSalida;
            $idColor = $value[4];
            $colorSalida = "";
            if ($idColor != "" && $idColor != null && $idColor != 0) {
            $color = consulta_bd("c.id,c.nombre", "color_productos c", "c.id= $idColor", "c.nombre asc");
            $colorSalida .='<div class="contLabelSelect">
                                <label for="color-producto">Color Producto</label>
                                <select name="colorProducto" class="color-producto">';

                                $colorSalida .= '<option value="'.$color[0][0].'">'.$color[0][1].'</option>'; 
            $colorSalida .= '   </select>
                            </div>';
            } 
            $output[] .= $colorSalida;
            $output[] .='
                </div><!-- fin contLogoColor-->';
                if ($para == "sustituir") {
                    $output[] .= '
                                <div class="contBotonAgregar">
                                    <button prodSustituir= "'.$idProd.'" idProducto="'.$value[2].'" idCotizacion="'.$idCotizacion.'" id="btnAgregarProductoTabla" para="sustituir" class="btn-nuevaCotizacion">Sustituir</button>
                                </div>
                            </div><!-- fin contProductoBuscado-->';
                    
                }else{
                    $output[] .= '
                                <div class="contBotonAgregar">
                                    <button idProducto="'.$value[2].'" idCotizacion="'.$idCotizacion.'" id="btnAgregarProductoTabla" para="agregar" class="btn-nuevaCotizacion">Agregar</button>
                                </div>
                            </div><!-- fin contProductoBuscado-->';
                } 
        }//Fin Foreach
    }//Fin del if $cantidadProductos
    // echo  join('',$output);
    // echo $a;
                
    /*===========================================================
    =            Creamos un array para leer por JSON            =
    ===========================================================*/
        $row = array(
        'filaProducto' => join('',$output),
        'para' => $para
        );
        if (is_array($row)) {
            echo json_encode($row);
        }
    /*=====  End of Creamos un array para leer por JSON  ======*/
