<?php
    include("../admin/conf.php");
    /*================================================
    =            Recibimos Variables POST            =
    ================================================*/
    $rut = (isset($_POST['rutEmpresa'])) ? mysqli_real_escape_string($conexion, $_POST['rutEmpresa']) : 0;
  /*=====  End of Recibimos Variables POST  ======*/
    // die($rut);


    if ($rut != 0) {
    
    /*===========================================================
    =            Creamos los option correspondientes            =
    ===========================================================*/    
    $usuarios = consulta_bd("nombre, apellido, rut", "clientes", "rut_empresa
     = '$rut'", "nombre asc");

    $usuariosMostrar = '<option value="">Usuarios</option>';

    foreach ($usuarios as $value) {
        $usuariosMostrar .= "<option value=".$value[2].">".$value[0]." ".$value[1]."</option>";
    }
    // echo $usuariosMostrar;
    /*=====  End of Creamos los option correspondientes  ======*/

    /*===========================================================
    =            Creamos un array para leer por JSON            =
    ===========================================================*/
        $row = array(
        'usuarios' => $usuariosMostrar
        );
        if (is_array($row)) {
            echo json_encode($row);
        }
    /*=====  End of Creamos un array para leer por JSON  ======*/

    }
