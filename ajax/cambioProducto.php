<?php 
include("../admin/conf.php");
include("../admin/includes/tienda/cart/inc/functions.inc.php");

$id = (is_numeric($_POST['id'])) ? mysqli_real_escape_string($conexion, $_POST['id']) : 0;
$color = (is_numeric($_POST['color'])) ? mysqli_real_escape_string($conexion, $_POST['color']) : 0;

$productosDetalles = consulta_bd("pd.id, pd.sku, (select valor_bruto from listas_productos where lista_id = ".listaCliente()." and sku = pd.sku) as lista_cliente, (select valor_bruto from listas_productos where lista_id = ".listaOferta()." and sku = pd.sku) as lista_descuento, (select valor_bruto from listas_productos where lista_id = ".listaCyber()." and sku = pd.sku) as lista_cyber, pd.producto_id, p.cyber, pd.imagen1, pd.imagen2, pd.imagen3, pd.imagen4, pd.imagen5, pd.talla_id","productos_detalles pd, productos p","p.id = pd. producto_id and pd.producto_id = $id and pd.color_id = $color","");


$tallBase = $productosDetalles[0][12];
$colorBase = $color;
$tallas = consulta_bd("pd.id, t.id, t.nombre","tallas t, productos_detalles pd, colores c","t.id = pd.talla_id and pd.color_id = c.id and c.id = $colorBase and pd.producto_id = $id and pd.publicado = 1","pd.id asc"); 

$htmlTallas = '<select name="talla" id="talla" class="selectTalla">';
/*listado de tallas disponibles para ese color*/
for($i=0; $i<sizeof($tallas); $i++){ 
    if($tallBase == $tallas[$i][1]){
        $selected = "selected";
    } else {
        $selected = "";
    }

    $htmlTallas .='<option value="'.$tallas[$i][1].'" '.$selected.' >'.$tallas[$i][2].'</option>';
    }
$htmlTallas .= '</select>';
/*listado de tallas disponibles para ese color*/





$galeria = '<div class="thumbsGaleriaFicha cont100">';
if($productosDetalles[0][7] != ""){ 
    $galeria .='<a href="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][7]).'" data-fancybox="gallery" rel="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][7]).'" class="thumbs-galeria img-thumbnail fancybox">
        <img src="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][7]).'" width="100%" />
    </a>';

    }

if($productosDetalles[0][8] != ""){ 
    $galeria .='<a href="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][8]).'" data-fancybox="gallery" rel="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][8]).'" class="thumbs-galeria img-thumbnail fancybox">
        <img src="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][8]).'" width="100%" />
    </a>';

    }

if($productosDetalles[0][9] != ""){ 
    $galeria .='<a href="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][9]).'" data-fancybox="gallery" rel="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][9]).'" class="thumbs-galeria img-thumbnail fancybox">
        <img src="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][9]).'" width="100%" />
    </a>';

    }

if($productosDetalles[0][10] != ""){ 
    $galeria .='<a href="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][10]).'" data-fancybox="gallery" rel="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][10]).'" class="thumbs-galeria img-thumbnail fancybox">
        <img src="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][10]).'" width="100%" />
    </a>';

    }

if($productosDetalles[0][11] != ""){ 
    $galeria .='<a href="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][11]).'" data-fancybox="gallery" rel="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][11]).'" class="thumbs-galeria img-thumbnail fancybox">
        <img src="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][11]).'" width="100%" />
    </a>';

    }


$galeria .='</div>';

$galeria .='<a href="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][7]).'" class="fancybox cont100" data-fancybox="gallery">
    <img src="'.imagen("imagenes/productos_detalles/", $productosDetalles[0][7]).'" width="100%" id="img-grande">
</a>';











$is_cyber = (opciones('cyber') == 1) ? true : false;
if($is_cyber){
    if($productosDetalles[0][6] == 1 and $productosDetalles[0][4] > 0){
        $precio = ' <span class="precioNormal">$'.number_format($productosDetalles[0][2],0,",",".").'</span>
                    <span class="precioInternet">Precio Cyber</span>
                    <span class="precioOfertaFicha">$'.number_format($productosDetalles[0][4],0,",",".").'</span>';
    } else {
        if($productosDetalles[0][3] > 0){
            $precio = '<span class="precioNormal">$'.number_format($productosDetalles[0][2],0,",",".").'</span>
                       <span class="precioOfertaFicha">$'.number_format($productosDetalles[0][3],0,",",".").'</span>';
        } else {
            $precio = '<span class="precioOfertaFicha">$'.number_format($productosDetalles[0][2],0,",",".").'</span>';
        }
    }
    
} else {
    if($productosDetalles[0][3] > 0){
        $precio = '<span class="precioNormal">$'.number_format($productosDetalles[0][2],0,",",".").'</span>
                   <span class="precioOfertaFicha">$'.number_format($productosDetalles[0][3],0,",",".").'</span>';
    } else {
        $precio = '<span class="precioOfertaFicha">$'.number_format($productosDetalles[0][2],0,",",".").'</span>';
    }
    
}


    if(opciones('cyber') == 1 and $productosDetalles[0][4] > 0){
        $descuento = round(100 - (($productosDetalles[0][4] * 100) / $productosDetalles[0][2]));
        $htmlDescuento = "<div class='porcentajeDescuento'>-$descuento%</div>";
        
    } else if($productosDetalles[0][3] > 0 and $productosDetalles[0][3] < $productosDetalles[0][2]){
        $descuento = round(100 - (($productosDetalles[0][3] * 100) / $productosDetalles[0][2]));
        $htmlDescuento = "<div class='porcentajeDescuento'>-$descuento%</div>";
        
    } else {
        $htmlDescuento = "";
    }
    

$producto = array(
	'precio' => $precio,
	'sku' => $productosDetalles[0][1],
    'porcentajeDescuento' => $htmlDescuento,
    'galeria' => $galeria,
    'idCambio' => $productosDetalles[0][0],
    'tallas' => $htmlTallas
	);
print json_encode($producto);
mysqli_close($conexion);
?>
