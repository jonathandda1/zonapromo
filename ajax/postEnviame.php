<?php 

include("../../admin/conf.php");

$pedido_id = $argv[1];

$pedido     = consulta_bd("oc, nombre, rut, direccion, comuna, email, telefono, total, carrier, comentarios_envio, medio_de_pago, id_envio, despacho_por_pagar","pedidos","id = $pedido_id","");

$nombre_comuna = trim($pedido[0][4]);

$region = consulta_bd('id, region_id', 'comunas', "nombre = '{$nombre_comuna}'", '');

$pasa = true;

/*if($region[0][1] == 13){
    if ($region[0][0] == 339 OR $region[0][0] == 332 OR $region[0][0] == 342 OR $region[0][0] == 345 OR $region[0][0] == 335 OR $region[0][0] == 346 OR $region[0][0] == 337 OR $region[0][0] == 334 OR $region[0][0] == 333 OR $region[0][0] == 331 OR $region[0][0] == 341 OR $region[0][0] == 336 OR $region[0][0] == 340) {
        $pasa = true;
    }else{
        $pasa = false;
    }
}*/

if ($pasa) {

    if ($pedido[0][12] == 0) { // Despacho por pagar no se envian a enviame.

        if (is_null($pedido[0][11]) OR trim($pedido[0][11]) == '') {
    
            $medio_de_pago = $pedido[0][10];

            $pr_pedidos = consulta_bd("productos_detalle_id, cantidad","productos_pedidos","pedido_id = $pedido_id","");
            $nombre_pr  = "";
            $peso_total = 0;
            $peso_total = 0;
            $largo      = 0;
            $altura     = 0;
            $ancho      = 0;
            $carrier = "CHX";//$pedido[0][8];

            foreach($pr_pedidos as $pr){
                $producto = consulta_bd("nombre, ancho, alto, largo, peso","productos_detalles","id = $pr[0]","");
                $nombre_producto_pro = preg_replace("/[\r\n|\n|\r]+/", " ", $producto[0][0]);
                if($nombre_pr){ 
                    $nombre_pr .= ", ".str_replace('"', '', $nombre_producto_pro); 
                }else{ 
                    $nombre_pr = str_replace('"', '', $nombre_producto_pro); 
                }
                $peso_total += $producto[0][4]*$pr[1];

                $largo      = $producto[0][3];
                $altura     = $producto[0][2];
                $ancho      = $producto[0][1];

                $volumen += (($ancho * $largo * $altura) / 4000) * $pr[1];
            }


            //DATOS ENVIO           
            $imported_id            = str_replace("OC_", "", $pedido[0][0]);    // ID DEL ENVIO
            $order_price            = $pedido[0][7];                            // PRECIO
            $n_packages             = 1;                                        // NUMERO DE PAQUETES
            $content_description    = substr($nombre_pr, 0, 200);                               // DESCRIPCION DEL CONTENIDO
            $type                   = 'delivery';
            $weight                 = $peso_total;               // PESO
            $volume                 = $volumen;             // VOLUMEN

            //DATOS RECEPTOR
            $name                   = $pedido[0][1];
            $phone                  = $pedido[0][6];
            $email                  = $pedido[0][5];

            //DIRECCIÖN DESPACHO
            $place                  = $pedido[0][4];
            $full_address           = preg_replace("/[\r\n|\n|\r]+/", " ", $pedido[0][3]);
            $comentario_cliente     = str_replace(
                array("\\", "¨", "º", "~",
                     "#", "@", "|", "!", "\"",
                     "·", "$", "%", "&", "/",
                     "(", ")", "?", "'", "¡",
                     "¿", "[", "^", "<code>", "]",
                     "+", "}", "{", "¨", "´",
                     ">", "< ", ";", ",", ":",
                     ".", "*", " "),
                ' ',
                $pedido[0][9]
            );
            $comentario_cliente = preg_replace("/[\r\n|\n|\r]+/", " ", $comentario_cliente);

            //BODEGA
            $warehouse_code         = '001';

            //CARRIER DE ENVIO
            $carrier_code           = $carrier;    // CODIGO DE CARRIER
            $tracking_number        = "";

            $variables = '{ "shipping_order" : {   
                    "imported_id" : "'.$imported_id.'",
                    "order_price" : "'.$order_price.'",
                    "n_packages"  : "'.$n_packages.'",
                    "content_description" : "'.$content_description.'",
                    "type" : "'.$type.'",
                    "weight" : "'.$weight.'",
                    "volume" : "'.$volume.'"
                },
                "shipping_destination" : {          
                    "customer" : { 
                        "name" : "'.$name.'",
                        "phone" : "'.$phone.'",
                        "email" : "'.$email.'"
                    },
                    "delivery_address" : { 
                        "home_address" : { 
                            "place" : "'.$place.'",
                            "full_address" : "'.$full_address.'",
                            "information" : "'.$comentario_cliente.'"
                        }
                    }
                },
                "shipping_origin" : {
                    "warehouse_code" : "'.$warehouse_code.'"
                },
                "carrier" : {
                    "carrier_code" : "'.$carrier_code.'",
                    "tracking_number": "'.$tracking_number.'"
                }
            }';


            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.enviame.io/api/s2/v2/companies/eltio/deliveries",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $variables,
                CURLOPT_HTTPHEADER => array(
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Content-Type: application/json",
                    "Postman-Token: 2f43d24c-4001-46c8-a020-be5541732528",
                    "api-key: 88695514147270616885835be96aae5b"
                ),
            ));

            

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if($err){
                echo "cURL Error #:" . $err;
            }
            
            // return json_encode($response, true);

            $resp           = json_decode($response);
            $identificador  = $resp->data->identifier;
            $estado_envio   = $resp->data->status->name;

            if ($medio_de_pago == 'transferencia') {
                $update = update_bd("pedidos","id_envio = '$identificador'","id = $pedido_id");
            }else{
                $update = update_bd("pedidos","id_envio = '$identificador', estado_envio = '$estado_envio'","id = $pedido_id");
            }

            $mensaje = "Pedido $pedido_id creado en envíame";
            $fechaHoy = date('d-m-Y H:i:s', time());
            insert_bd('cron_logs', 'cron, fecha', "'$mensaje', '$fechaHoy'");

        }
        
    }
    
}else{
    $medio_de_pago = $pedido[0][10];
    if ($medio_de_pago != 'transferencia') {
        $update = update_bd("pedidos","estado_envio = 'Validado'","id = $pedido_id");
    }

    $mensaje = "Pedido $pedido_id no genera enviame por condición.";
    $fechaHoy = date('d-m-Y H:i:s', time());
    insert_bd('cron_logs', 'cron, fecha', "'$mensaje', '$fechaHoy'");
}

?>