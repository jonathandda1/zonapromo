<?php
//conf
include('../admin/conf.php');

require_once('../PHPMailer/PHPMailerAutoload.php');
// Include functions
require_once('../admin/includes/tienda/cart/inc/functions.inc.php');
$rutEmpresa  = (isset($_POST['rutEmpresa'])) ? mysqli_real_escape_string($conexion, $_POST['rutEmpresa']) : 0;
$rutUsuario  = (isset($_POST['selectUsuariosEmpresas'])) ? mysqli_real_escape_string($conexion, $_POST['selectUsuariosEmpresas']) : 0;
$idVendedor  = (isset($_POST['idVendedor'])) ? mysqli_real_escape_string($conexion, $_POST['idVendedor']) : 0;
$origen  = (isset($_POST['origen'])) ? mysqli_real_escape_string($conexion, $_POST['origen']) : 0;

$empresaConsulta = consulta_bd("nombre","empresas","rut = '$rutEmpresa'","");
$empresa = $empresaConsulta[0][0];
$usuarioConsulta = consulta_bd("nombre, apellido, email, telefono, rut, id","clientes","rut = '$rutUsuario'","");
$nombre = $usuarioConsulta[0][0];
$apellido = $usuarioConsulta[0][1];
$email = $usuarioConsulta[0][2];
$telefono = $usuarioConsulta[0][3];
$idUsuario = $usuarioConsulta[0][5];
// die($empresa);
$date = date('Ymdhis');
$oc = "COT_$date";
$estado_pendiente = 1;

$campos = " oc, 
			fecha, 
			estado_id,
			cliente_id,
			vendedor_id, 
			empresa,
			rut_empresa,
			nombre,
			apellido,
			email,
			telefono,
			rut,
			fecha_creacion";
			$values = "'$oc', NOW(), $estado_pendiente, $idUsuario, $idVendedor,'$empresa', '$rutEmpresa', '$nombre', '$apellido', '$email', '$telefono', '$rutUsuario', NOW()";
	
    $generar_pedido = insert_bd("cotizaciones","$campos","$values");
	$cotizacion_id = mysqli_insert_id($conexion);
	header("Location: ../cotizaciones-vendedor/$oc");
?>
