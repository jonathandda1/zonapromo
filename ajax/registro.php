<?php
    header('Content-type: text/html; charset=utf-8');
    include("../admin/conf.php");
    include '../Mailin.php';
    $nombre_sitio = opciones("nombre_cliente");
    $nombre_corto = opciones("dominio");
    $noreply = "no-reply@".$nombre_corto;
    $url_sitio = "https://".$nombre_corto;
    $correo_venta = opciones("correo_venta");
    $color_logo = opciones("color_logo");
        
    $logo = opciones("logo_mail");
    $campo_oculto = 'validacion';
    $para = 'contacto@zonapromo.cl';
    $asunto = "Nuevo usuario en $nombre_sitio";
    $ruta_retorno = 'contacto';
    $ruta_retorno_exito = 'exito-contacto';
    $campo_mensaje = 'mensaje';
    $obligatorios = "nombre, email, $campo_mensaje";
    $lang = (isset($_GET[lang])) ? "ing": "esp";

	//include("../funciones.php");
	$nombre = (isset($_POST[nombre])) ? mysqli_real_escape_string($conexion, $_POST[nombre]) : 0;
	$apellido = (isset($_POST[apellido])) ? mysqli_real_escape_string($conexion, $_POST[apellido]) : 0;
	$correo = (isset($_POST[email])) ? mysqli_real_escape_string($conexion, $_POST[email]) : 0;
	$rut = (isset($_POST[rut])) ? mysqli_real_escape_string($conexion, $_POST[rut]) : 0;
	$pass = (isset($_POST[clave])) ? mysqli_real_escape_string($conexion, $_POST[clave]) : 0;

    $origen = (isset($_GET['origen'])) ? mysqli_real_escape_string($conexion, $_GET['origen']) : 0;

$tipo_registro = (isset($_POST['tipo_registro'])) ? mysqli_real_escape_string($conexion, $_POST['tipo_registro']) : 0;

$codigo_empresa = (isset($_POST['codigo_empresa'])) ? mysqli_real_escape_string($conexion, $_POST['codigo_empresa']) : 0;


if($tipo_registro == "empresa"){
    $empresa = consulta_bd("codigo, lista_id","empresas","codigo = '$codigo_empresa'","");
    if(count($empresa) == 0){
        $error = "Codigo de empresa no existe.";
        header("Location: ../registro?a=2&msje=$error");
        die();
    } else {
        if($empresa[0][1] > 0){
            $listaEmpresa = $empresa[0][1];
        } else {
            $listaEmpresa = 1;
        }
        
    }
} else {
    $listaEmpresa = 1;
}

	
	
	$pass_encrypted = md5($pass);
	$rand = rand(1000, 9999);
    $password_salt = md5($rand);
    $password = hash('md5',$pass_encrypted.$password_salt);
	
	$existe = consulta_bd("id","clientes","email = '$correo'","");
	$cant = mysqli_affected_rows($conexion);
	if($cant > 0){
        if($origen == 1){
            $error = "Correo ya se encuentra registrado";
            header("Location: ../agregar-usuarios?a=2&msje=$error");
            die();
        } else {
            $error = "Correo ya se encuentra registrado";
            header("Location: ../registro?a=2&msje=$error");
            die();
        }
		
	}
	
	if($origen == 1){
        $keys = "fecha_creacion, fecha_modificacion, activo, password_salt";
	    $vals = "NOW(), NOW(), 1, '$password_salt'";
    } else {
        
       $keys = "fecha_creacion, fecha_modificacion, activo, password_salt, lista_id";
	   $vals = "NOW(), NOW(), 1, '$password_salt', $listaEmpresa";
    }
	
	foreach($_POST as $key=>$val){
		if($key == 'clave2'){
			
		} else {
				if ($key == 'clave'){
				$keys .= ", $key";
				$vals .= ", '$password'";
			}else if($key == 'url'){
			
			}else{
				$keys .= ", $key";
				$vals .= ", '$val'";
			}
		}
		
		
	}
	
	$insert = insert_bd("clientes", $keys, $vals);
	if($insert){
        
         if($origen == 1){
            $error = "Tu usuario fue creado.";
            header("Location: ../agregar-usuarios?a=1&msje=$error");
            die();
         } else {
            $msg .='<html>
            <head><link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"></head>
            <body style="font-family: \'Lato\', sans-serif; color: #333;">
            <div style="background: #f1f1f1; width: 100%;">
                <div style="background: #fff; border-top: 2px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px; margin: auto; padding: 30px; box-sizing: border-box;">
                    <img src="'.$logo.'" alt="'.$nombre_sitio.'">
                    <p style="font-size: 20px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
                    <p style="font-size: 16px; margin-top: 0;">Se a registrado un nuevo usuario a través de la página web de '.$nombre_sitio.'. <br>
                    Los datos de la persona que se registro son:</p>
                    <ul>';

                                    foreach($_POST as $key=>$val)
                                    {

                                        if ($key != 'clave' AND $key != 'clave2' AND $key != 'nombre_empresa' AND $key != 'codigo_empresa' AND  $key != 'tipo_registro' AND $key != 'enviar' AND $key != $campo_mensaje AND $key != '')
                                        {
                                            $nombreVal = ucwords($key);
                                            $msg .= "<li style='min-height:30px;'><strong>".$nombreVal.":</strong> ".htmlentities($val,ENT_QUOTES,"UTF-8")."</li>";
                                        }
                                    }
                                    $msg .= '</ul>
                            Muchas gracias. <br>
                            Atte,<br>
                            <b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
                        </div>
                    </div>
                </body>
            </html>';
	        
    
            // echo $msg."<br><hr>";
            
            $asuntoRespuesta = "Contacto $nombre_sitio";
            $msg3 = '<html>
            <head><link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"></head>
            <body style="font-family: \'Lato\', sans-serif; color: #333;">
            <div style="background: #f1f1f1; width: 100%;">
                <div style="background: #fff; border-top: 3px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
                    <img src="'.$logo.'" alt="'.$nombre_sitio.'">
                    <h2 style="font-size: 26px; text-transform:uppercase; margin-bottom:5px;color:'.$color_logo.';">Hola, '.$nombre.':</h2>
                    <p style="font-size: 20px; margin-top: 0; color:'.$color_logo.';">Se ha creado tu cuenta en la página web '.$nombre_sitio.'</p>
                    <p style="font-size: 20px; margin-top: 0; color:'.$color_logo.';">A continuación te entregamos tus datos de acceso para que puedas disfrutar de todos nuestros productos:</p>
                    <ul>';

                    foreach($_POST as $key=>$val)
                    {

                        if ($key != 'telefono' AND $key != 'rut' AND $key != 'nombre' AND  $key != 'clave2' AND $key != 'nombre_empresa' AND $key != 'codigo_empresa' AND  $key != 'tipo_registro' AND $key != 'enviar' AND $key != $campo_mensaje AND $key != '')
                        {
                            $nombreVal = ucwords($key);
                            $msg3 .= "<li style='min-height:30px; font-size: 20px; color:'.$color_logo.'; ><strong>".$nombreVal.":</strong> ".htmlentities($val,ENT_QUOTES,"UTF-8")."</li>";
                        }
                    }
                    $msg3 .= '</ul>
                    <p style="font-size: 20px; color:'.$color_logo.';">
                    Muchas gracias. <br>
                    Atte,<br>
                    <b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
                </div>
                <div style="background: #f5f6f7; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
                    <p style="color:'.$color_logo.'; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                    <p>
                        <a href="https://www.facebook.com/" style="margin-right:30px; ">
                            <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                        </a>
                        <a href="https://www.instagram.com/" style="margin-right:30px; ">
                            <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                        </a>
                    </p>

                    <p>
                        <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                        <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                    </p>
                </div>
                
            </div>
            </body>
            </html>';

            // echo $msg3."<br><hr>";
            // die();
            $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
            $mailin->
                addTo('jonathandda@gmail.com','Portal')->
                setFrom('portal@zonapromo.cl', 'Zona Promo')->
                setSubject("$asunto")->
                setText("$msg")->
                    setHtml("$msg");
            $res = $mailin->send();
            $res2 = json_decode($res);

            if ($res2->{'result'} == true) {
                //envio copia al cliente
                $mailinCliente = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
                $mailinCliente->
                    addTo("$correo", "$nombre")->
                    setFrom('portal@zonapromo.cl', 'Zona Promo')->
                    setSubject("$asuntoRespuesta")->
                    setText("$msg3")->
                        setHtml("$msg3");
                $resCliente = $mailinCliente->send();
                $res2Cliente = json_decode($resCliente);
                //fin envio copia al cliente
                
                $error = "Tu cuenta fue creada, por favor inicia sesión.";
                header("Location: ../inicio-sesion?a=1&msje=$error");
                die();
            } else {

               $error = ($lang == 'esp') ? "Error enviando el correo, por favor inténtelo nuevamente." : "We had some dificulties sending the email, please try again later.";
				header("Location: $ruta_retorno?msje=$error");
				die("2");
            }

    
         }
		
	} else{
        if($origen == 1){
            $error = "No fue posible crear usuarios en este momento, inténtalo más tarde.";
            header("Location: ../agregar-usuarios?a=1&msje=$error");
            die();
        } else {
            $error = "No fue posible crear tu cuenta en este momento, inténtalo más tarde.";
            header("Location: ../registro?a=1&msje=$error");
            die();
        }
		
	}
	
?>