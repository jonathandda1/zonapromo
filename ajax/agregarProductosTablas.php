<?php
    include("../admin/conf.php");
    /*================================================
    =            Recibimos Variables POST            =
    ================================================*/
    $idProducto = (isset($_POST['idProducto'])) ? mysqli_real_escape_string($conexion, $_POST['idProducto']) : 0;
    $cantidad = (isset($_POST['cantidad'])) ? mysqli_real_escape_string($conexion, $_POST['cantidad']) : 0;
    $idCotizacion = (isset($_POST['idCotizacion'])) ? mysqli_real_escape_string($conexion, $_POST['idCotizacion']) : 0;
    $idLogo = (isset($_POST['logo'])) ? mysqli_real_escape_string($conexion, $_POST['logo']) : 0;
    $idColor = (isset($_POST['color'])) ? mysqli_real_escape_string($conexion, $_POST['color']) : 0;
    $para = (isset($_POST['para'])) ? mysqli_real_escape_string($conexion, $_POST['para']) : 0;
    $sustituir = (isset($_POST['sustituir'])) ? mysqli_real_escape_string($conexion, $_POST['sustituir']) : 0;
    $nombreLogo = "";
    $nombreColor = "";
    // die(
    //     "idProducto: ".$idProducto."<br>". 
    //     "Cantidad: ".$cantidad."<br>". 
    //     "idCotizacion: ".$idCotizacion."<br>".
    //     "idLogo: ".$idLogo."<br>". 
    //     "idColor: ".$idColor."<br>".
    //     "para: ".$para."<br>". 
    //     "idSustituir: ".$sustituir."<br>"
    // );
    /*=====  End of Recibimos Variables POST  ======*/
    if ($para == "sustituir") {
        $updateSustituto = update_bd("productos_cotizaciones","condicion_boton = 'sin stock',costo_producto = 0, costo_logo = 0, margen_utilidad = 0, precio_unitario_neto = 0, precio_final_neto = 0, utilidad_total = 0, cantidad = 0","cotizacion_id = '$idCotizacion' AND productos_detalle_id  = $sustituir");
        $sumasFinalesCant = consulta_bd("SUM(cantidad),SUM(precio_final_neto) ,SUM(utilidad_total)","productos_cotizaciones","cotizacion_id = $idCotizacion","");
        $cantActual = $sumasFinalesCant[0][0];
        $precioFinalNeto = $sumasFinalesCant[0][1];
        $totalUnitarioNeto = $sumasFinalesCant[0][2];
        $updateCantidad = update_bd("cotizaciones","cant_productos = $cantActual, total_unitario = '$totalUnitarioNeto', total = '$precioFinalNeto'","id=$idCotizacion");
        
    }
    $prod = consulta_bd("pd.sku", "productos_detalles pd", "pd.id = $idProducto", "");
    $sku = $prod[0][0];
    if ($idLogo != 0) {
        $cantColor = consulta_bd("l.nombre", "logo l", "l.id = $idLogo", "l.nombre asc");
        $nombreLogo = $cantColor[0][0];
    }
    if ($idColor != 0) {
        $color = consulta_bd("c.nombre", "color_productos c", "c.id = $idColor", "c.nombre asc");
        $nombreColor = $color[0][0];
    }
    $verificacion = consulta_bd("id, cantidad,precio_unitario_neto,costo_producto, costo_logo", "productos_cotizaciones", "cotizacion_id = $idCotizacion AND productos_detalle_id = $idProducto AND codigo = '$sku'", "");
    $idProdCot = $verificacion[0][0];
    $cantProdCot = $verificacion[0][1];
    // die($idProdCot." ".$cantProdCot." ".$idProducto." ".$sku);
    $cantVerificable = sizeof($verificacion);
    if ($cantVerificable > 0) {
        /*Calculos*/
        if ($para == "sustituir") {
            $cantFinalDetalle = 0;
        }else{
            $cantFinalDetalle = $cantProdCot + $cantidad;
        }
        $precioUnitarioNeto = $verificacion[0][2];
        $precioFinalNeto = $precioUnitarioNeto* $cantFinalDetalle;
        $utilidad_total = $precioFinalNeto - ($verificacion[0][3] + $verificacion[0][4])* $cantFinalDetalle;
        /*Actualizo producto_cotizacion*/
        $detalle = update_bd("productos_cotizaciones","cantidad = $cantFinalDetalle, precio_unitario_neto = '$precioUnitarioNeto', precio_final_neto = '$precioFinalNeto', utilidad_total = '$utilidad_total'","id=$idProdCot");
        /*Calculos GENERALES*/
        $sumasNeto = consulta_bd("SUM(precio_final_neto), SUM(utilidad_total),SUM(cantidad)","productos_cotizaciones","cotizacion_id = $idCotizacion","");
        $finalPrecioNeto = $sumasNeto[0][0];
        $utilidadTotalFinal = $sumasNeto[0][1];
        $cantFinal = $sumasNeto[0][2];
        /*Actualizo cotizaciones*/
        $cantidades = update_bd("cotizaciones","cant_productos = $cantFinal, total = '$finalPrecioNeto', total_unitario= '$utilidadTotalFinal'","id=$idCotizacion");
    }else{
        $campos = "cotizacion_id, productos_detalle_id, cantidad, codigo, fecha_creacion, logo, color, id_color, id_logo";
        $values = "$idCotizacion, $idProducto, $cantidad, '$sku', NOW(),'$nombreLogo', '$nombreColor', '$idColor', '$idLogo'";
        $detalle = insert_bd("productos_cotizaciones","$campos","$values");
        
        $sumasNeto = consulta_bd("SUM(cantidad)","productos_cotizaciones","cotizacion_id = $idCotizacion","");
        $cantFinal = $sumasNeto[0][0];
        $cantidades = update_bd("cotizaciones","cant_productos = $cantFinal","id=$idCotizacion");
    }
                
    /*===========================================================
    =            Creamos un array para leer por JSON            =
    ===========================================================*/
        // $row = array(
        // 'filaProducto' => join('',$output),
        // 'oc' => $oc
        // );
        // if (is_array($row)) {
        //     echo json_encode($row);
        // }
    /*=====  End of Creamos un array para leer por JSON  ======*/
