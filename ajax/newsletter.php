<?php 
header('Content-type: text/html; charset=utf-8');
include("../admin/conf.php");
include("../admin/includes/tienda/cart/inc/functions.inc.php");
include '../Mailin.php';

$nombre_sitio = opciones("nombre_cliente");
$nombre_corto = opciones("dominio");
$noreply = "no-reply@".$nombre_corto;
$url_sitio = "https://".$nombre_corto;
$correo_venta = opciones("correo_venta");
$color_logo = opciones("color_logo");
	
$logo = opciones("logo_mail");
$campo_oculto = 'validacion';
$para = 'contacto@zonapromo.cl';
$asunto = "Newsletter $nombre_sitio";
$ruta_retorno = 'contacto';
$ruta_retorno_exito = 'exito-contacto';
$campo_mensaje = 'mensaje';
$obligatorios = "nombre, email, $campo_mensaje";
$lang = (isset($_GET[lang])) ? "ing": "esp";


$from = "noreply@zonapromo.cl";
//Campos del formulario correspondiente a nombre e email.

$correo = (isset($_POST[correo])) ? mysqli_real_escape_string($conexion, $_POST[correo]) : 0;

$existe_correo = consulta_bd("email","newsletter","email = '$correo'","id desc");
$cant = mysqli_affected_rows($conexion);

if($cant > 0){
	mysqli_close($conexion);
	die('2');
}else{
	insert_bd("newsletter","email, fecha_creacion","'$correo', now()");
    $insert = mysqli_affected_rows($conexion);
	if ($insert > 0) {
    //aca debo enviar el correo a mailchimp*/
	mysqli_close($conexion);
	$msg = '';
    $msg .='
    <html>
        <head>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body>
            <!--[if mso]>
                    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="800">
             <![endif]-->

             <!--[if !mso]> <!---->
            <table border="0" style="max-width: 800px; padding: 10px; margin:0 auto; border-collapse: collapse;">
             <!-- <![endif]-->
                <tr>
                    <td style="background-color: #f1f1f3; padding: 0">
                    <div style="color: #34495e; margin: 10px 20px 10px 20px; text-align: justify;font-family: sans-serif">
                        <a href="'.$url_sitio.'">
                            <img width="255" style="display:block; margin: 10px 20px 10px 20px;" src="'.$logo.'" alt="'.$logo.'"/>
                        </a>
                    </div>
                    </td>
                </tr>   
                <tr>
                    <td style="background-color: #F9F9F9">
                        <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                            <p style="font-size:36px; color: '.$color_logo.';"><strong>Hola, '.$nombre_sitio.':</strong></p>
                            <p>Se ha suscrito un nuevo usuario a trav&eacute;s del formulario de NEWSLETTER. <br />El correo de la persona que se a suscrito es:<br /></p>
                            <ul>';
                        	$msg .= "<li style='min-height:30px;'>Correo: ".htmlentities($correo,ENT_QUOTES,"UTF-8")."</li>";
                            $msg .= '</ul>
                            <p>Muchas gracias<br /> Atte,</p>
                            <p><strong>Equipo de '.$nombre_sitio.'</strong></p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #f1f1f3">
                        <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="background:#f1f1f3;">
                            <tr>
                                <th colspan="4" align="left">
                                    <p style="font-size:26px; color: '.$color_logo.';"><strong>¿Por qué comprar en '.$nombre_sitio.'?</strong></p>
                                </th>
                            </tr>    
                            <tr>
                                <td align="center" style="width:25%;">
                                    <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_envio.png" alt="Imagen de beneficios" class="">
                                </td>
                                <td align="center" style="width:25%;">
                                    <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_calidad.png" alt="Imagen de beneficios" class="">
                                </td>
                                <td align="center" style="width:25%;">
                                    <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_pagoSeguro.png" alt="Imagen de beneficios" class="">
                                </td>
                                <td align="center" style="width:25%;">
                                    <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_medioPago.png" alt="Imagen de beneficios" class="">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="width:25%;">
                                    <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Envio todo Chile</p>
                                </td>
                                <td align="center" style="width:25%;">
                                    <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Calidad y precios bajos</p>
                                </td>
                                <td align="center" style="width:25%;">
                                    <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Pago Seguro</p>
                                </td>
                                <td align="center" style="width:25%;">
                                    <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Todo medio de pago</p>
                                </td>
                            </tr>
                        </table>
                            
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #252c39">
                        <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                            <p style="color:#fff; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                                    <p style="color:#fff; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                                    <p>
                                        <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                            <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                                        </a>
                                        <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                            <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                                        </a>
                                    </p>

                                    <p>
                                        <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                                        <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                                    </p>
                        </div>
                    </td>
                </tr>
            </table>
        </body>
    </html>';
    $msg3 = '
    <html>
        <head>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap" rel="stylesheet">
        <title>'.$nombre_sitio.'</title>
        </head>
        <body>
    <!--[if mso]>
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="800">
    <![endif]-->

    <!--[if !mso]> <!---->
       <table border="0" style="max-width: 800px; padding: 10px; margin:0 auto; border-collapse: collapse;">
    <!-- <![endif]-->
            <tr>
                <td style="background-color: #f1f1f3;">
                <div style="color: #34495e; margin: 10px 20px 10px 20px; text-align: justify;font-family: sans-serif">
                    <a href="'.$url_sitio.'">
                        <img width="255" style="display:block; margin: 10px 20px 10px 20px;" src="'.$logo.'" alt="'.$logo.'"/>
                    </a>
                </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #F9F9F9">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                        <p style="font-size:36px; color: '.$color_logo.';"><strong>Hola, Estimado:</strong></p>
                        <p>Te has registrado con éxito a nuestro newsletter, te enviaremos todas las ofertas e informaciones de nuestros productos.<br /></p>
                        <p>Muchas gracias.<br /> Atte,</p>
                        <p><strong>Equipo de '.$nombre_sitio.'</strong></p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #f1f1f3">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="background:#f1f1f3;">
                        <tr>
                            <th colspan="4" align="left">
                                <p style="font-size:26px; color: '.$color_logo.';"><strong>¿Por qué comprar en '.$nombre_sitio.'?</strong></p>
                            </th>
                        </tr>    
                        <tr>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_envio.png" alt="Imagen de beneficios" class="">
                            </td>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_calidad.png" alt="Imagen de beneficios" class="">
                            </td>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_pagoSeguro.png" alt="Imagen de beneficios" class="">
                            </td>
                            <td align="center" style="width:25%;">
                                <img style="width: 100%; max-width: 150px;  height: auto;" src="'.$url_sitio.'/img/icono_medioPago.png" alt="Imagen de beneficios" class="">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Envio todo Chile</p>
                            </td>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Calidad y precios bajos</p>
                            </td>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Pago Seguro</p>
                            </td>
                            <td align="center" style="width:25%;">
                                <p class="texto_iconos" style="font-size: 15px; color: #009be5; font-weight: bold;">Todo medio de pago</p>
                            </td>
                        </tr>
                    </table>
                        
                    </div>
                </td>
            </tr>
            <tr>
                <td style="background-color: #252c39">
                    <div style="color: #34495e; margin: 20px 10px 10px 30px; text-align: justify;font-family: sans-serif">
                        <p style="color:#fff; font-size:16px;">Equipo de '.$nombre_sitio.'</p>
                                <p style="color:#fff; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
                                <p>
                                    <a href="https://www.facebook.com/houseofcars" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                                    </a>
                                    <a href="https://www.instagram.com/houseofcarsficial/" style="margin-right:30px; ">
                                        <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                                    </a>
                                </p>

                                <p>
                                    <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                                    <a style="color:#fff; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
                                </p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>';

            // echo $msg3."<br><hr>";

    
            $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
            $mailin->
                addTo('jonathandda@gmail.com','Portal')->
                setFrom('portal@zonapromo.cl', 'Zona Promo')->
                setSubject("$asunto")->
                setText("$msg")->
                    setHtml("$msg");
            $res = $mailin->send();
            $res2 = json_decode($res);

            if ($res2->{'result'} == true) {				
                //envio copia al cliente
                $mailinCliente = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
                $mailinCliente->
                    addTo("$correo", "$correo")->
                    setFrom('portal@zonapromo.cl', 'Zona Promo')->
                    setSubject("Suscripción a Newsletter")->
                    setText("$msg3")->
                        setHtml("$msg3");
                $resCliente = $mailinCliente->send();
                $res2Cliente = json_decode($resCliente);
                //fin envio copia al cliente
			     
		    }
            if ($res2->{'result'} == true && $res2Cliente->{'result'} == true) {
                die('1');
            }else{
                die('3');
            }
    }else{
        die('3');
    }
}

?>
