<?php 
include("../admin/conf.php");


$id = (is_numeric($_POST['id'])) ? mysqli_real_escape_string($conexion, $_POST['id']) : 0;
$qty = (is_numeric($_POST['qty'])) ? mysqli_real_escape_string($conexion, $_POST['qty']) : 0;

$cambioManual = (is_numeric($_POST['cambioManual'])) ? mysqli_real_escape_string($conexion, $_POST['cambioManual']) : 0;
/*=================================================================
=            Revisando stock de Popup con cambio de N°            =
=================================================================*/
if ($cambioManual != 0) {
	// Nota: Si es un cambio de numero manual lo que me interesa es la cantidad que viene sin contar lo que ya había
	if(!isset($_COOKIE['cart_alfa_cm'])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}
	$cart = $_COOKIE['cart_alfa_cm'];

	$stock = consulta_bd("stock, stock_reserva, producto_id","productos_detalles","id = $id","");

	$prodPack = consulta_bd("pack, codigos","productos","id = ".$stock[0][2],"");

	if($prodPack[0][0] == 1){
		$codigosProductos = trim($prodPack[0][1]);
		$productosPack = explode(",", $codigosProductos);
		$retorno = "";
		foreach($productosPack as $skuPack) {    
			$skuPack = trim($skuPack);
			$stockPack = consulta_bd("stock, stock_reserva, producto_id, sku, id","productos_detalles","sku = '$skuPack'","");  
			$cantAfectado = mysqli_affected_rows($conexion);
			
			$stockActual = $stockPack[0][0];
			$stockReserva = $stockPack[0][1];
			$stockReal =  $stockActual - $stockReserva;
			//echo "$skuPack ----- $stockActual ----- $stockReserva --------- $stockReal *************";
			//resto mi stock disponible con el stock que esta en la session y devuelvo el monto anterior menos la cantidad solicitada por el cliente		
			$stockDisponible = $stockReal;
			$stockValidado = $stockDisponible - $qty;
		  	if($stockValidado < 0){
				die("-1");
			}
			//$retorno .= "-----".$stockValidado;
		}
		die("1");
	}else {
		//codigo cuando el producto no es pack------
	////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////
		//Resto el stock global con el stock critico
		//$stockVenta = $stock[0][0] - $stock[0][1];
		$stockActual = $stock[0][0];
		$stockReserva = $stock[0][1];
		$stockReal =  $stockActual - $stockReserva;
		//resto mi stock disponible con el stock que esta en la session y devuelvo el monto anterior menos la cantidad solicitada por el cliente		
		$stockDisponible = $stockReal;
		$stockValidado = $stockDisponible - $qty;
			
		die("$stockValidado");
		//fin codigo cuando el producto no es pack------
	////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////
	mysqli_close($conexion);
	}
/*=====  End of Revisando stock de Popup con cambio de N°  ======*/
}else{
	if(!isset($_COOKIE['cart_alfa_cm'])){
		setcookie("cart_alfa_cm", "", time() + (365 * 24 * 60 * 60), "/");
		}
	$cart = $_COOKIE['cart_alfa_cm'];


	$stock = consulta_bd("stock, stock_reserva, producto_id","productos_detalles","id = $id","");

	$prodPack = consulta_bd("pack, codigos","productos","id = ".$stock[0][2],"");

	if($prodPack[0][0] == 1){
		$codigosProductos = trim($prodPack[0][1]);
		$productosPack = explode(",", $codigosProductos);
		$retorno = "";
		foreach($productosPack as $skuPack) {    
			$skuPack = trim($skuPack);
			$stockPack = consulta_bd("stock, stock_reserva, producto_id, sku, id","productos_detalles","sku = '$skuPack'","");  
			$cantAfectado = mysqli_affected_rows($conexion);
			
			$stockActual = $stockPack[0][0];
			$stockReserva = $stockPack[0][1];
			$stockReal =  $stockActual - $stockReserva;
			//echo "$skuPack ----- $stockActual ----- $stockReserva --------- $stockReal *************";
			
			
			//separo la session para ver cuantos productos he agregado con el mismo id
			$items = explode(',',$cart);
			$cantElementosCargados = count($items);
			
			//die("----- $cantElementosCargados   ------    $cart");
			//die("1");
			$cant = 0;
			if($cantElementosCargados > 0 and $cart != ''){
				foreach ($items as $item) {
					//reviso si el producto agregado es un pack, para separarlo y revisar stock.
					$prodStock = consulta_bd("p.id, p.pack, p.codigos","productos_detalles pd, productos p","p.id = pd.producto_id and pd.id = $item","");
					if($prodStock[0][1] == 1){
						$prodCargadoStock = trim($prodStock[0][2]);
						$productosPackStock = explode(",", $prodCargadoStock);
						foreach($productosPackStock as $skuPackStock) { 
							$skuParaId = trim($skuPackStock);
							$idSkuPackStock = consulta_bd("id","productos_detalles","sku = '$skuParaId'","");
							if($stockPack[0][4] == $idSkuPackStock[0][0]){
								$cant += 1;
							}
						}
					} 
					else if ($stockPack[0][4] == $item) {
						$cant += 1;
					}
				}
			}//condicion del count de los productos en el carro de compras
			
			//resto mi stock disponible con el stock que esta en la session y devuelvo el monto anterior menos la cantidad solicitada por el cliente		
			$stockDisponible = $stockReal - $cant;
			$stockValidado = $stockDisponible - $qty;
		  	if($stockValidado < 0){
				die("-1");
			}
			//$retorno .= "-----".$stockValidado;
		}
		die("1");
	} else {
		//codigo cuando el producto no es pack------
	////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////
		//Resto el stock global con el stock critico
		//$stockVenta = $stock[0][0] - $stock[0][1];
		$stockActual = $stock[0][0];
		$stockReserva = $stock[0][1];
		$stockReal =  $stockActual - $stockReserva;
		
		//separo la session para ver cuantos productos he agregado con el mismo id
		$items = explode(',',$cart);
		
		$cantElementosCargados = count($items);
		$cant = 0;
		if($cantElementosCargados > 0 and $cart != ''){
			foreach ($items as $item) {
			//consulto si el item es un pack o un producto
			//reviso si el producto agregado es un pack, para separarlo y revisar stock.
					$prodStock = consulta_bd("p.id, p.pack, p.codigos","productos_detalles pd, productos p","p.id = pd.producto_id and pd.id = $item","");
					if($prodStock[0][1] == 1){
						$prodCargadoStock = trim($prodStock[0][2]);
						$productosPackStock = explode(",", $prodCargadoStock);
						foreach($productosPackStock as $skuPackStock) { 
							$skuParaId = trim($skuPackStock);
							$idSkuPackStock = consulta_bd("id","productos_detalles","sku = '$skuParaId'","");
							if($id == $idSkuPackStock[0][0]){
								$cant += 1;
							}
						}
					} 
					else if ($id == $item) {
						$cant += 1;
					}
			/*if ($id == $item) {
				$cant += 1;
			}*/
			}
		} else {
			if ($id == $item) {
				$cant += 1;
			}
		}
		
		
		//resto mi stock disponible con el stock que esta en la session y devuelvo el monto anterior menos la cantidad solicitada por el cliente		
		$stockDisponible = $stockReal - $cant;
		$stockValidado = $stockDisponible - $qty;
			
		die("$stockValidado");
		//fin codigo cuando el producto no es pack------
	////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////------////----------///////
	mysqli_close($conexion);
	}

}
?>