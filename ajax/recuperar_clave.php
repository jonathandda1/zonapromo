<?php 
	include("../admin/conf.php");
	include '../Mailin.php';
	
	$correo = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : 0;
    $origen = (isset($_POST['origenRecuperar'])) ? mysqli_real_escape_string($conexion, $_POST['origenRecuperar']) : 0;
    if ($origen == "poPup") {
        $existe = consulta_bd("id, nombre", "vendedores", "email = '$correo'", "");
    }else{
        $existe = consulta_bd("id, nombre", "clientes", "email = '$correo'", "");
    }
	$cant2 = mysqli_affected_rows($conexion);
	//Consulto por el correo, si existe ejecuto la sentencia
	if ($cant2 > 0){
		$usuario = $existe[0][1];
		$id = $existe[0][0];
		 	$nombre_sitio = opciones("nombre_cliente");
			$url_sitio = opciones("url_sitio");
			$logo = opciones("logo_mail");
			$asunto = "Nueva contraseña.";
			$color_logo = opciones("color_logo");
			
			$rand = rand(1000, 999999);
			// si el correo no existe indico que no esta en los registros
			$pass_nueva = "Z0N@-PR0M0".$rand;
			$pass_encrypted =  md5($pass_nueva);
			$password_salt = md5($rand);
			$password = hash('md5',$pass_encrypted.$password_salt);
            if ($origen == "poPup") {
                $update = update_bd("vendedores","clave = '$password', password_salt = '$password_salt'","id=$id");
            }else{
                $update = update_bd("clientes","clave = '$password', password_salt = '$password_salt'","id=$id");
            }
			
		    
    $msg2 = '';
    $msg2 .= '<html>
	<head><link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"></head>
	<body style="font-family: \'Lato\', sans-serif; color: #333;">
	<div style="background: #f1f1f1; width: 100%;">
		<div style="background: #fff; border-top: 3px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
            <h2 style="font-size: 26px; text-transform:uppercase; margin-bottom:5px;color:'.$color_logo.';">Hola, '.$usuario.':</h2>
            <p style="font-size: 18px; color:'.$color_logo.';">Se ha solicitado recuperar clave a través de la página web '.$nombre_sitio.'. <br />
            se recomienda cambiarla en cuanto reciba este correo:<br /></p>
            <p style="font-size: 18px; color:'.$color_logo.';">Nueva clave: <strong>'.$pass_nueva.'</strong></p>
            <p style="font-size: 18px; color:'.$color_logo.';">Muchas gracias<br /> Atte,</p>
            <p style="font-size: 18px; color:'.$color_logo.';"><strong>Equipo de '.$nombre_sitio.'</strong></p>
        </div>
        <div style="background: #f5f6f7; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
            <p style="color:'.$color_logo.'; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
            <p>
                <a href="https://www.facebook.com/" style="margin-right:30px; ">
                    <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                </a>
                <a href="https://www.instagram.com/" style="margin-right:30px; ">
                    <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                </a>
            </p>

            <p>
                <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
            </p>
		</div>
        
	</div>
	</body>
	</html>';
        
            $msg2Html = 'Nueva clave: <strong>'.$pass_nueva.'</strong>'; 
        // echo $msg2;
        // die();
            $mailinCliente = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
            $mailinCliente->
                addTo("$correo", "$usuario")->
                setFrom(opciones("setFrom_mail"), opciones("nombre_setFrom_mail"))->
                setSubject("$asunto")->
                setText("$msg2Html")->
                    setHtml("$msg2");
            $resCliente = $mailinCliente->send();
            $res2 = json_decode($resCliente);
        
        
			// send the message, check for errors
            if ($res2->{'result'} == true){
                if ($origen == "poPup") {
                    header("Location: ../home?a=1&msje=te enviamos una nueva clave a tu email: $correo.");
				    die("1");
                }else{
                    header("Location: ../inicio-sesion?a=1&msje=te enviamos una nueva clave a tu email: $correo.");
				    die("1");
                }
            } else{
                if ($origen == "poPup") {
                    header("Location: ../home?a=2&error=No fue posible enviar una nueva clave..");
				    die("2");
                }else{
                    header("Location: ../recuperar-clave?a=2&msje=No fue posible enviar una nueva clave..");
				    die("2");
                }
            }
			
			
	} else {
		//si no existe retorno e indico que no esta en los registros
        if ($origen == "poPup") {
            header("Location: ../home?a=2&error=Usuario no registrado.");
		    die("3");
        }else{
            header("Location: ../recuperar-clave?a=2&msje=Usuario no registrado.");
		    die("3");
        }
	}

?>