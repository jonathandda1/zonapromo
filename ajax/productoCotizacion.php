<?php
include("../admin/conf.php");
$idCotizacion = (is_numeric($_POST['idCotizacion'])) ? mysqli_real_escape_string($conexion, $_POST['idCotizacion']) : 0;

// die($idCotizacion);
$data = array();  
$empresa = consulta_bd("rut_empresa","cotizaciones","id = $idCotizacion","");
$rutEmpresa = $empresa[0][0];
$whereProductoCotizar = "";
$whereProducto = "";
if(isset($_POST["search"]["value"]) && $_POST["search"]["value"] != ""){
    $busqueda = $_POST["search"]["value"];
    $whereProductoCotizar = "WHERE pc.logo LIKE '%$busqueda%' or pc.codigo like '%$busqueda%' or pd.nombre like '%$busqueda%'
	";
}

// $productosCotizar = consulta_bd("id, productos_detalle_id, codigo, logo, color, cantidad, costo_producto, costo_logo,margen_utilidad,precio_unitario_neto, precio_final_neto, utilidad_total, condicion_boton","productos_cotizaciones","cotizacion_id = $idCotizacion $whereProductoCotizar","");

$sql = "SELECT pc.id, pc.productos_detalle_id, pc.codigo, pc.logo, pc.color, pc.cantidad, pc.costo_producto, pc.costo_logo, pc.margen_utilidad,pc.precio_unitario_neto, pc.precio_final_neto, pc.utilidad_total, pc.condicion_boton, pd.nombre, pd.imagen1 FROM productos_cotizaciones pc
INNER JOIN productos_detalles pd
ON pc.productos_detalle_id = pd.id and pc.cotizacion_id = $idCotizacion $whereProductoCotizar";
  $productosCotizar = $conexion->query($sql);
  $number_filter_row = $productosCotizar->num_rows;
// foreach($productosCotizar as $value){
//     var_dump($value["codigo"]);
// }
// die();
foreach($productosCotizar as $value){
    $skuProducto = $value["codigo"];
    // $producto = consulta_bd("nombre, thumbs","productos","codigo_producto_madre = $skuProducto","");
    $ultimaCotizacion = consulta_bd("p.codigo, p.fecha_creacion, p.precio_unitario"," productos_cotizaciones p, cotizaciones c","p.codigo = '$skuProducto' AND c.id = p.cotizacion_id AND c.rut_empresa = '$rutEmpresa'","fecha_creacion Desc limit 2");
    $imagen = $value["imagen1"];
    if ($value["imagen1"] == "" || $value["imagen1"] == null) {
        $imagen = "sin-imagen.jpg"; 
    }
    $sub_array = array();
	$sub_array[] = $value["id"];
    $sub_array[] = $value["codigo"];
	$sub_array[] = "<img class='img-tabla' src='imagenes/productos_detalles/".$imagen."'>";
	$sub_array[] = "<span title='".$value["nombre"]."'>".preview($value["nombre"], 10)."</span>";
    $sub_array[] = $value["logo"];
    $sub_array[] = $value["cantidad"];
    $sub_array[] = $value["costo_producto"];
    $sub_array[] = $value["costo_logo"];
    $sub_array[] = $value["margen_utilidad"];
    $sub_array[] = $value["precio_unitario_neto"];
    $sub_array[] = $value["precio_final_neto"];
    $sub_array[] = $ultimaCotizacion[1][2];
    $sub_array[] = $value["utilidad_total"];
    if ($value["condicion_boton"] == "sin stock") {
        $sub_array[] = '<div class="contBotonTabla">
                            <button accion="conStock" idCotizacion="'.$value["id"].'" class="botonSinStock btnDetalleTablas btnSinStockTablas">Sin Stock</button>            
                        </div>
                        <div class="contBotonTabla">
                            <button class="btnDetalleTablas btnSustitutoGris">Sustituto</button>          
                        </div>';
    }else if($value["condicion_boton"] == "sustituto"){

        $sub_array[] = '<div class="contBotonTabla">
                            <button class="btnDetalleTablas btnNuevo">Sustituto</button>            
                        </div>';
    }else if($value["condicion_boton"] == "nuevo"){
        $sub_array[] = '<div class="contBotonTabla">
                            <button class="btnDetalleTablas btnNuevo">Nuevo</button>            
                        </div>';
    }else{
        $sub_array[] = '<div class="contBotonTabla">
                        <button accion="sinStock" idCotizacion="'.$value["id"].'" class="botonSinStock btnDetalleTablas">Sin Stock</button>            
                    </div>
                    <div class="contBotonTabla">
                        <button idProd="'.$value["productos_detalle_id"].'" class="btnDetalleTablas modalSustitucion">Sustituto</button>          
                    </div>';
    }
	$data[] = $sub_array;
}

function count_all_data($idCotizacion){
	$productosCotizar2 = consulta_bd("id, productos_detalle_id, codigo, logo, color, cantidad","productos_cotizaciones","cotizacion_id = $idCotizacion","");
    $cant = count($productosCotizar2);
	return $cant;
}

$output = array(
	'draw'		=>	intval($_POST['draw']),
	'recordsTotal'	=>	count_all_data($idCotizacion),
	'recordsFiltered'	=>	$number_filter_row,
	'data'		=>	$data
);

echo json_encode($output);