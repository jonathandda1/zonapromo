<?php
header('Content-type: text/html; charset=utf-8');
include("../admin/conf.php");
include("../admin/includes/tienda/cart/inc/functions.inc.php");
include '../Mailin.php';

$nombre_sitio = opciones("nombre_cliente");
$nombre_corto = opciones("dominio");
$noreply = "no-reply@".$nombre_corto;
$url_sitio = "https://".$nombre_corto;
$correo_venta = opciones("correo_venta");
$color_logo = opciones("color_logo");
	
$logo = opciones("logo_mail");
$campo_oculto = 'validacion';
$para = 'contacto@zonapromo.cl';
$ruta_retorno = 'contacto';
$ruta_retorno_exito = 'exito-contacto';
$campo_mensaje = 'mensaje';
$obligatorios = "nombre, email, $campo_mensaje";
$lang = (isset($_GET[lang])) ? "ing": "esp";


define('clave', '6LfZoiIeAAAAAPAgvAOU1YL5UxiHrU72BemCOzOi');
$token = (isset($_POST['token'])) ? mysqli_real_escape_string($conexion, $_POST['token']) : "";
$action = (isset($_POST['action'])) ? mysqli_real_escape_string($conexion, $_POST['action']) : "";
$nombre = (isset($_POST['nombre'])) ? mysqli_real_escape_string($conexion, $_POST['nombre']) : "";
$telefono = (isset($_POST['telefono'])) ? mysqli_real_escape_string($conexion, $_POST['telefono']) : "";
$email = (isset($_POST['email'])) ? mysqli_real_escape_string($conexion, $_POST['email']) : "";
$comentarios = (isset($_POST['mensaje'])) ? mysqli_real_escape_string($conexion, $_POST['mensaje']) : "";
$idCotizar = (isset($_POST['idCotizar'])) ? mysqli_real_escape_string($conexion, $_POST['idCotizar']) : 0;
$tabla = (isset($_POST['tabla'])) ? mysqli_real_escape_string($conexion, $_POST['tabla']) : "";
if ($tabla == "formulario_home") { $ruta_retorno = $url_base."home"; $contacto="Formulario de home"; }
if ($tabla == "formulario_servicioTecnico") { $ruta_retorno = $url_base."servicios"; $contacto="Formulario de servicio técnico"; }
if ($tabla == "formulario_arriendos") { $ruta_retorno = $url_base."arriendos"; $contacto="Formulario de arriendos"; }
if ($tabla == "formulario_hospitales_clinicas") { $ruta_retorno = $url_base."hospitales-clinicas"; $contacto="Formulario de hospitales y clínicas"; }
if ($tabla == "formulario_cotizacion") {
    $producto = consulta_bd("distinct(p.id), p.descripcion_breve, p.id, p.nombre, pd.sku",
    "productos p, productos_detalles pd",
    "p.id = $idCotizar GROUP BY p.id",
    "");
    $cantProd = mysqli_affected_rows($conexion);
    
    //cuando no hay poroducto dirijo al 404
    if($cantProd > 0){
        $nombreProd = $producto[0][3];
        $descripcion = $producto[0][1];
        $codigo = $producto[0][4];
    }
    $ruta_retorno = $url_base."ficha/".$idCotizar."/".url_amigables($nombre).""; $contacto="Formulario de  Cotización";
}
$asunto = "Mensaje a través de $contacto";
if ($nombre != "" && $email != "" && $telefono != "" && $comentarios != "") {
    $cu = curl_init();
    curl_setopt($cu, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($cu, CURLOPT_POST, 1);
    curl_setopt($cu, CURLOPT_POSTFIELDS, http_build_query(array('secret' => clave, 'response' => $token)));
    curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($cu);
    curl_close($cu);

    $datos = json_decode($response, true);
    print_r($datos);

    if($datos['success'] == 1 && $datos['score'] >= 0.5){
        insert_bd("$tabla","nombre, producto_id, telefono, email, comentarios, fecha_creacion","'$nombre', '$idCotizar', '$telefono', '$email', '$comentarios', now()");
        $insert = mysqli_affected_rows($conexion);
        if ($insert > 0) {
            $msg = '';
            $msg .='<html>
            <head><link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"></head>
            <body style="font-family: \'Lato\', sans-serif; color: #333;">
            <div style="background: #f9f9f9; width: 100%;">
                <div style="background: #fff; border-top: 2px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px; margin: auto; padding: 30px; box-sizing: border-box;">
                    <img src="'.$logo.'" alt="'.$nombre_sitio.'">
                    <p style="font-size: 20px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
                    <p style="font-size: 16px; margin-top: 0;">Se ha enviado un nuevo mensaje desde el '.$contacto.'  a través de la página web de '.$nombre_sitio.'. <br>
                    Los datos de la persona que contacta son:</p>
                    <ul>';

                                    foreach($_POST as $key=>$val)
                                    {

                                        if ($key != 'idCotizar' AND $key != 'tabla' AND $key != 'token' AND $key != 'action' AND $key != 'enviar' AND $key != $campo_mensaje AND $key != '')
                                        {
                                            $nombreVal = ucwords($key);
                                            $msg .= "<li style='min-height:30px;'><strong>".$nombreVal.":</strong> ".htmlentities($val,ENT_QUOTES,"UTF-8")."</li>";
                                        }
                                    }
                                    $msg .= '</ul>
                                    <p align="justify" style="font-size:20px; color: #009be5;">El cliente ha dejado el siguiente mensaje: <br /><br />
                                        <em>'.htmlentities($_POST[$campo_mensaje],ENT_QUOTES,"UTF-8").'</em>
                                    </p>

                            <p style="font-size: 16px;">Rogamos contactarse lo antes posible con el cliente.<br><br>
                            Muchas gracias. <br>
                            Atte,<br>
                            <b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
                        </div>
                    </div>
                </body>
            </html>';
            if ($tabla == "formulario_cotizacion") {       
                $msg ='';
                $msg .='<html>
                        <head><link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"></head>
                        <body style="font-family: \'Lato\', sans-serif; color: #333;">
                        <div style="background: #f9f9f9; width: 100%;">
                            <div style="background: #fff; border-top: 2px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px; margin: auto; padding: 30px; box-sizing: border-box;">
                                <img src="'.$logo.'" alt="'.$nombre_sitio.'">
                                <p style="font-size: 20px;margin-bottom:5px;color:'.$color_logo.';"><b>Estimado/a:</b></p>
                                <p style="font-size: 16px; margin-top: 0;">Se ha enviado un nuevo mensaje desde el '.$contacto.'  a través de la página web de '.$nombre_sitio.'. <br>
                                Los datos de la persona que contacta son:</p>
                                <ul>';

                                                foreach($_POST as $key=>$val)
                                                {

                                                    if ($key != 'idCotizar' AND $key != 'tabla' AND $key != 'token' AND $key != 'action' AND $key != 'enviar' AND $key != $campo_mensaje AND $key != '')
                                                    {
                                                        $nombreVal = ucwords($key);
                                                        $msg .= "<li style='min-height:30px;'><strong>".$nombreVal.":</strong> ".htmlentities($val,ENT_QUOTES,"UTF-8")."</li>";
                                                    }
                                                }
                                $msg .= '</ul>
                                <p align="justify" style="font-size:16px; color: #009be5;">El cliente ha dejado el siguiente mensaje: <br /><br />
                                <em>'.htmlentities($_POST[$campo_mensaje],ENT_QUOTES,"UTF-8").'</em>
                                </p>
                                <p align="justify" style="font-size:16px;">El cliente desea cotizar el siguiente producto: <br /><br />
                                    <ul>
                                        <li><strong>Id del producto: </strong>'.$idCotizar.'</li>
                                        <li><strong>Código: </strong>'.$codigo.'</li>
                                        <li><strong>Nombre: </strong>'.$nombreProd.'</li>
                                        <li><strong>Descripcion: </strong>'.$descripcion.'</li>
                                    </ul>
                                <p style="font-size: 16px;">Rogamos contactarse lo antes posible con el cliente.<br><br>
                                Muchas gracias. <br>
                                Atte,<br>
                                <b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
                            </div>
                        </div>
                    </body>
                </html>'; 
                // echo $msg;
                // die();
            }
	        
    
    // echo $msg."<br><hr>";
    $asuntoRespuesta = "Contacto $nombre_sitio";
    // echo $asuntoRespuesta."<br><hr>";
    $msg3 = '<html>
	<head><link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"></head>
	<body style="font-family: \'Lato\', sans-serif; color: #333;">
	<div style="background: #f1f1f1; width: 100%;">
		<div style="background: #fff; border-top: 3px solid #1c61ad; border-bottom: 2px solid #1c61ad; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
			<img src="'.$logo.'" alt="'.$nombre_sitio.'">
            <h2 style="font-size: 26px; text-transform:uppercase; margin-bottom:5px;color:'.$color_logo.';">Hola, '.$nombre.':</h2>
			<p style="font-size: 20px; margin-top: 0; color:'.$color_logo.';">Hemos recibido tu solicitud de contacto, pronto nos comunicaremos contigo.</p>
			<p style="font-size: 20px; color:'.$color_logo.';">
			Muchas gracias. <br>
			Atte,<br>
			<b style="color: '.$color_logo.';">Equipo de '.$nombre_sitio.'</b></p>
		</div>
        <div style="background: #f5f6f7; width: 90%; max-width:650px;margin: auto; padding: 30px; box-sizing: border-box;">
            <p style="color:'.$color_logo.'; font-size:14px;">Si tienes alguna duda o sugerencia, estamos para ayudarte. Contáctanos a traves de nuestro Centro de ayuda.</p>
            <p>
                <a href="https://www.facebook.com/" style="margin-right:30px; ">
                    <img src="'.$url_sitio.'/img/iconoFaceMail.png">
                </a>
                <a href="https://www.instagram.com/" style="margin-right:30px; ">
                    <img src="'.$url_sitio.'/img/iconoInstagramMail.png">
                </a>
            </p>

            <p>
                <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/politicas-de-privacidad">Política de privacidad </a>
                <a style="color:#8b8b8b; font-size:14px;" href="'.$url_sitio.'/terminos-y-condiciones">Términos y condiciones</a>
            </p>
		</div>
        
	</div>
	</body>
	</html>';

            // echo $msg3."<br><hr>";
            // die();

    
            $mailin = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
            $mailin->
                addTo('jonathandda@gmail.com','Portal')->
                setFrom('portal@zonapromo.cl', 'Zona Promo')->
                setSubject("$asunto")->
                setText("$msg")->
                    setHtml("$msg");
            $res = $mailin->send();
            $res2 = json_decode($res);

            if ($res2->{'result'} == true) {
                //envio copia al cliente
                $mailinCliente = new Mailin('contacto@zonapromo.cl', 'kFEDXCSzaMPKvrdV');
                $mailinCliente->
                    addTo("$email", "$nombre")->
                    setFrom('portal@zonapromo.cl', 'Zona Promo')->
                    setSubject("$asuntoRespuesta")->
                    setText("$msg3")->
                        setHtml("$msg3");
                $resCliente = $mailinCliente->send();
                $res2Cliente = json_decode($resCliente);
                //fin envio copia al cliente
                
                
                $mensaje = "Tu solicitud fue aceptada";
                header("location:$ruta_retorno?ok=$mensaje");
                die("");
            } else {

               $error = ($lang == 'esp') ? "Error enviando el correo, por favor inténtelo nuevamente." : "We had some dificulties sending the email, please try again later.";
				header("Location: $ruta_retorno?error=$error");
				die("2");
            }
        }
    }else{
        $error = "No es posible realizar tu solicitud";
        header("location:$ruta_retorno?error=$error");
        die("");
    }
        
} else {
    $error = "Deben rellenarse todos los campos";
    header("location:$ruta_retorno?error=$error");
    die("");
}


?>