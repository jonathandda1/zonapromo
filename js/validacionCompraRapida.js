$(document).ready(function(){
    $("form").on("focus || change",".inputCotizar", function(){
        $(this).css("border", "1px solid #009be5");
      });
     $("form").on("focus || change",".campoGrande2", function(){
        $(this).css("border", "1px solid #009be5");
      });
     $("form").on("focus || change || click","#region, #comuna", function(){
        $("div.compraSelect>div.selector").addClass("azulBottom");
      });

    /*========================================================
    =            Seleccion de input carro compras            =
    ========================================================*/
    $(".contieneSucursal").change(function(){
        $(".contieneSucursal").removeClass("radioSucursalChecked");
        if($(".radioSucursal, this").is(":checked")){
              $(this).addClass("radioSucursalChecked"); 
        }
    });
    
    /*=====  End of Seleccion de input carro compras  ======*/
    
	var anchoVentana = $(window).width();
	
    $(".abrirCarro").click(function() {
        $(".resumenCarroHeader").fadeIn(200);
    });
    $(".cerrarMenuCart").click(function() {
        $(".resumenCarroHeader").fadeOut(200);
    });
    
    $("#tienda1").click(function(){
        $('.contCamposRetiro').css("display","block");
    });
    $("#tienda2").click(function(){
        $('.contCamposRetiro').css("display","none");
    });
   /* boton yo retiro*/
    $("#yoRetiro").click(function(){
        $(".btnRetiro a").removeClass("activo");
        $(this).addClass("activo"); 
        
        var nombre = $("#nombre").val();
        var telefono = $("#telefono").val();
        $("#nombre_retiro").val(nombre);
        $("#telefono_retiro").val(telefono);
    });
    
    $("#otraPersona").click(function(){
        $(".btnRetiro a").removeClass("activo");
        $(this).addClass("activo"); 
        $("#nombre_retiro").val("");
        $("#telefono_retiro").val("");
    });
    
    
    
    
    //$("#contDatosCartVariables").load("tienda/carroCompraRapida.php?comuna_id=0&retiro=1");

	
	
	
	    /*
GUARDAR NUEVA DIRECCION
*/
    

$("#region_PopUp").change(function(){
        var region_id = $(this).val();
        $("#comuna_PopUp").load("tienda/comunas.php?region_id=" + region_id);
        $("#uniform-comuna_PopUp span").html("Seleccione comuna");
    });
$("#guardarDireccionCompraRapida").click(function(){
	var nombre = $("#nombre_PopUp").val();
	var region = $("#regionPopUp").val();
	var comuna = $("#comuna_PopUp").val();
	var calle = $("#calle_PopUp").val();
	var numero = $("#numero_PopUp").val();
	
	console.log(region);
	console.log(comuna);
	if(nombre != "" && region != "" && comuna != "" && calle != "" && numero != ""){
		$("#formNuevaDireccion").submit();
		} else {
			console.log("Faltan campos por completar.");
			Swal.fire({
			  icon: 'error',
			  text: 'Faltan campos por completar.'
			});
            if(nombre == ""){$("#nombre_PopUp").addClass("incompleto");} else {$("#nombre_PopUp").removeClass("incompleto");}
            if(region == 16){$("#uniform-region_PopUp").addClass("incompleto");} else {$("#uniform-region_PopUp").removeClass("incompleto");}
            
            if(comuna == "Seleccione Comuna"){$("#uniform-comuna_PopUp").addClass("incompleto");} else {$("#uniform-comuna_PopUp").removeClass("incompleto");}
            
            if(calle == ""){$("#calle_PopUp").addClass("incompleto");} else {$("#calle_PopUp").removeClass("incompleto");}
            if(numero == ""){$("#numero_PopUp").addClass("incompleto");} else {$("#numero_PopUp").removeClass("incompleto");}
		}
	
});
    
	
	
	/*ASIGNO DIRECCION CUANDO CAMBIO EL SELECT DE DIRECCIONES DEL CLIENTE LOGUEADO */
	$("#direcciones").change(function(){
		var direccion = $(this).val();
        //var usuario_id = $(this).attr("rel");
        //console.log(usuario_id);
		$.ajax({
                url: "ajax/obtenerIdDirecciones.php",
                type: "post",
                dataType: 'json',
                async: true,
                data: {id: direccion},
                success: function(resp) {
					console.log(resp);
					var direccionActual = resp;
					console.log(direccionActual.nombre);
					if(direccionActual.existe == 1){
						/*id direccion Si existe*/
						$("#region").val(direccionActual.region_id);
						$("#uniform-region span").html(direccionActual.region);
						
                        
                        $("#comuna").load("tienda/comunas.php?region_id=" + direccionActual.region_id, function(){
                            $("#comuna").val(direccionActual.comuna_id);
						    $("#uniform-comuna span").html(direccionActual.comuna);
                            
                            $("#contDatosCartVariables2").load("tienda/carroCompraRapida.php?comuna_id="+direccionActual.comuna_id+"&retiro=0");
                            $(".totalesMovil").load("tienda/resumenCompraShortTotales.php?comuna_id="+direccionActual.comuna_id+"&retiro=0");
                            
                        });
						
						$("#direccion").val(direccionActual.calle);

						$("#contDatosCartVariables").load("tienda/carroCompraRapida.php?comuna_id="+direccionActual.comuna_id+"&retiro=0");
                        
                        $("#contDatosCartVariables2").load("tienda/carroCompraRapida.php?comuna_id="+direccionActual.comuna_id+"&retiro=0");
                        $(".totalesMovil").load("tienda/resumenCompraShortTotales.php?comuna_id="+direccionActual.comuna_id+"&retiro=0");
                        
                        
                        
						/*$("#comuna").load("tienda/comunas.php?region_id=" + direccionActual.region_id);
                        $("#comuna").val(direccionActual.comuna_id);
						$("#uniform-comuna span").html(direccionActual.comuna);
						
						
						$("#direccion").val(direccionActual.calle);

						$("#contDatosCartVariables").load("tienda/carroCompraRapida.php?comuna_id="+direccionActual.comuna_id+"&retiro=0");
                        
                        $("#contDatosCartVariables2").load("tienda/carroCompraRapida.php?comuna_id="+direccionActual.comuna_id+"&retiro=0");
                        $(".totalesMovil").load("tienda/resumenCompraShortTotales.php?comuna_id="+direccionActual.comuna_id+"&retiro=0");*/

						} else {
							/*id direccion no existe*/
							Swal.fire("","No fue posible cargar su dirección.","warning");
							}
					}
            });
		});
		
    
    $(".btnCompletarCompra").click(function(){
        $("#formEnvioYPago").submit();
    });
    
    $("#btnVolverResumen, .editSinUsuario").click(function(){
        $("#volverForm").submit();
    });
    
    
    
    /*Validar correo compra rapida*/
	$("#btnValidarMail").click(function() {
		$(".cont_loading").fadeIn(100);
        var correoCompraRapida = $("#email").val();
        if (correoCompraRapida.indexOf('@', 0) == -1 || correoCompraRapida.indexOf('.', 0) == -1) {
            //alertify.error("Ingrese un correo válido");
			//
			Swal.fire("","Ingrese un correo válido","warning");
            $(".cont_loading").fadeOut(100);
            $("#email").val("");
        } else {
            $.ajax({
                url: "ajax/correoCompraRapida.php",
                type: "post",
                async: true,
                data: {correo: correoCompraRapida},
                success: function(data) {
                    var json_string = JSON.stringify(data);
                    //var obj = $.parseJSON(json_string);
                     var obj = jQuery.parseJSON(data);
                    console.log("data:  " + data);
                    console.log("codError:  " + obj.codError);
                    if(obj.codError == 1){
                       Swal.fire("Correo empresa","El correo introducido es una cuenta de empresa, inicie sesion para usarlo o ingrese otra cuenta de mail","warning");
                        $("#email").val("");
                       } else if(obj.codError == 2){
                           console.log("llega");
                           Swal.fire({
                              title: "Tenemos una compra anterior con tu email ¿Son estos tus datos?",
                              text: "Si necesitas modificar tus datos, Inicia Sesión\nNombre: "+ obj.nombre +"\nTeléfono: " + obj.telefono,
                              icon: "warning",
                              showCancelButton: true,
                              confirmButtonText: 'Si',
                              cancelButtonText: 'No'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                $("#nombre").val(obj.nombre);
                                $("#telefono").val(obj.telefono);
                                $("#rut").val(obj.rut);
                                $(".parcheBlanco").fadeOut(100);
                                $(".cont_loading").fadeOut(100);
                                $(".btnTerminarCarro").fadeIn(100);
                            }else{
                                $("#nombre, #telefono, #rut").val("");
                                $(".parcheBlanco").fadeOut(100);
                                $(".cont_loading").fadeOut(100);
                                $(".btnTerminarCarro").fadeIn(100);

                            }
                          })
                           
                       } else {
                           $(".cont_loading").fadeOut(100);
                           $(".parcheBlanco").fadeOut(100);
                           $(".btnTerminarCarro").fadeIn(100);
                       }
                       
                    
					
                    $(".btnValidarMail").addClass("bloqueado");
					//$(".compraYAceptaTerminos").fadeIn(100);
                
                }
            });
			$(".cont_loading").fadeOut(100);
            //$(".formCompraRapida1").submit();
        };
    });
	 /*Validar correo compra rapida*/
	
		
   
	
	
    $("#uniform-facturaSi").click(function() {
        console.log("activo datos de factura");
        $(".contDatosFactura, .triangulo").fadeIn(100);
    });
    $("#uniform-facturaNo").click(function() {
        $(".contDatosFactura, .triangulo").fadeOut(100);
    });
    $("#modificarDatos2").click(function() {
        $("#modificarDatos").trigger("click");
    });
    $("#tienda1").click(function() {
        $("#tabsCompra1").fadeOut(100);
        $("#tabsCompra2").fadeIn(100);
        console.log("cargo los valores por ajax");
		$("#contDatosCartVariables").load("tienda/carroRetiroTienda.php");
        $("#contDatosCartVariables2").load("tienda/carroRetiroTienda.php");
        $(".totalesMovil").load("tienda/resumenCompraShortTotales.php?comuna_id=0&retiro=1");
    });
    
    
    
    
	/*Inicio de session*/
	$("#btnIngresarCompraRapida").click(function(){
		console.log("entra");
		var email = $("#usuarioLogin").val();
		var password = $("#passLogin").val();
		
		
		if(email != '' && password != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#passLogin").addClass("campo_vacio");
				Swal.fire('','Email invalido','error'); 
			} else {
				/*envio formulario para log*/
				$("#formLoginCompraRapida").submit();
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			Swal.fire('','Faltan campos por completar','error'); 
			if(email == '' ){
				$("#usuarioLogin").addClass("campo_vacio");
			} else {
				$("#usuarioLogin").removeClass("campo_vacio");
			};
			
			if(password == '' ){
				$("#passLogin").addClass("campo_vacio");
			} else {
				$("#passLogin").removeClass("campo_vacio");
			};
		};
	});/*fin login*/


    /*Inicio de session*/
	$("#btnInicioVendedora").click(function(){
		let email = $("#vendedoraEmail").val();
		let password = $("#vendedorClave").val();
		if(email != '' && password != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#vendedoraEmail").addClass("campo_vacio");
				Swal.fire('','Email invalido','error'); 
			} else {
				/*envio formulario para log*/
				$("#formLoginVendedora").submit();
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			Swal.fire('','Faltan campos por completar','error'); 
			if(email == '' ){
				$("#vendedoraEmail").addClass("campo_vacio");
			} else {
				$("#vendedoraEmail").removeClass("campo_vacio");
			};
			
			if(password == '' ){
				$("#vendedorClave").addClass("campo_vacio");
			} else {
				$("#vendedorClave").removeClass("campo_vacio");
			};
		};
	});/*fin login*/
	
	
	
	
	
	
    
	$("#solicitarClave").click(function() {
        var usuario = $("#usuarioLogin").val();
        $.ajax({
            type: 'POST',
            url: 'ajax/recuperarClaveCompraRapida.php',
            data: {
                correo: usuario
            },
            dataType: 'JSON',
            success: function(data) {
                if (data == 1) {
					Swal.fire("", "Revisa tu correo, te enviamos una nueva clave.", "success");
                    
                } else if (data == 2) {
					Swal.fire("", "No fue posible enviarte una nueva clave en estos momentos, intenta más tarde.", "warning");
                    
                } else if (data == 3) {
					Swal.fire("", "El correo ingresado no se encuentra en nuestros registros.", "error");
					
                };
            }
        });
    });

    $('input[name="envio"]').on('change', function(){
        var radio = $(this).val();
        var retiro;
        var comuna_id;

        if (radio == 'despachoDomicilio') {
            retiro = 0;
            comuna_id = $('#comuna').val();
            if (comuna_id == "Seleccione Comuna") {
                comuna_id = 0;
            }
            $(".contDireccion").css("display", "block");
            $(".caluga-retiro").css("display", "none");
        }else{
            retiro = 1;
            comuna_id = 0;
            $(".contDireccion").css("display", "none");
            $(".caluga-retiro").css("display", "block");
            let sucursalId = $('input[name=idSucursal]:checked', '.filaSucursalCarro').val();
            if (sucursalId == null || sucursalId == "") {
                sucursalId = 0;
            }
        }

        console.log(comuna_id + ' --- ' + retiro);

        $("#contDatosCartVariables").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro);
        $("#contDatosCartVariables2").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro);
        $(".totalesMovil").load("tienda/resumenCompraShortTotales.php?comuna_id="+comuna_id+"&retiro="+retiro);
    })

 
	
    $("#region").change(function() {
        var region_id = $(this).val();
        $("#comuna").load("tienda/comunas.php?region_id=" + region_id);
        $("#uniform-comuna span").html("Seleccione Comuna");
    });
    $("#comuna").change(function() {
        var comuna_id = $(this).val();

        var radio = $('input[name="envio"]:checked').val();
        var retiro;
        if (radio == 'despachoDomicilio') {
            retiro = 0;
        }else{
            retiro = 1;
        }

        $("#contDatosCartVariables").load("tienda/carroCompraRapida.php?comuna_id="+comuna_id+"&retiro="+retiro);
        $("#contDatosCartVariables2").load("tienda/carroCompraRapida.php?comuna_id="+comuna_id+"&retiro="+retiro);
        $(".totalesMovil").load("tienda/resumenCompraShortTotales.php?comuna_id="+comuna_id+"&retiro="+retiro);
		
        //$(".contCartCompraRapida").load("tienda/carroCompraRapida.php?comuna_id="+comuna_id+"&retiro="+retiro);
    });
    $('#valorCambiante').on('change', function(){
        function update(){
            var mE = $('#montoEnvios').html();
            $(".contLinkMapa").html(mE);
        }
        setTimeout(update, 500);
    });


    /*====================================
    =            Validaciones            =
    ====================================*/
    $("#telefono").change(function() { 
        let telefono = $("#telefono").val();
        if (telefono.length < 9) {
         Swal.fire("", "El teléfono debe tener un mínimo de 9 números", "warning");
         $("#telefono").val('');
         $("#telefono").addClass('campo_vacio');
        }else{
            $("#telefono").removeClass('campo_vacio');
        }
    });   
    /*=====  End of Validaciones  ======*/
    
    $("#comprar, #comprarMovil").click(function() {
        var nombre = $("#nombre").val();
        var email = $("#email").val();
        var telefono = $("#telefono").val();
        var rut = $("#rut").val();
        var region = $("#region").val();
        var comuna = $("#comuna").val();
        var direccion = $("#direccion").val();
        var nombre_retiro = $("#nombre_retiro").val();
        var telefono_retiro = $("#telefono_retiro").val();
        
        var razonSocial = $("#razonSocial").val();
        var direccionFactura = $("#direccionFactura").val();
        var rutFactura = $("#RutFactura").val();
        var giro = $("#giro").val();
        
        var htmlComuna = $("#uniform-comuna span").html();
        
        if ($("#tienda2").is(':checked')) {
            if (nombre != '' && email != '' && telefono != '' && rut != '' && region != '' && region != 'Seleccione region' && region != '0' && comuna != '347' && comuna != '' && comuna != 'sin comuna' && comuna != '0' && direccion != '' && htmlComuna != 'Seleccione Comuna') {
                if (email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
                    //console.error("Correo inválido.");
                    Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Correo inválido.' });
                    $("#email").addClass("incompleto");
                } else { 
                //email es valido, consulto por los datos de facturacion
                    if ($("#facturaSi").is(':checked')) {
                        console.log("Cliente quiere factura");
                        if (razonSocial != '' && direccionFactura != '' && rutFactura != '' && giro != '') {
                            if (razonSocial != '') { $("#razonSocial").removeClass("incompleto");
                            } else {$("#razonSocial").addClass("incompleto");};

                            if (direccionFactura != '') { $("#direccionFactura").removeClass("incompleto");
                            } else { $("#direccionFactura").addClass("incompleto"); };

                            if (rutFactura != '') { $("#RutFactura").removeClass("incompleto");
                            } else { $("#RutFactura").addClass("incompleto"); };

                            if (giro != '') { $("#giro").removeClass("incompleto");
                            } else { $("#giro").addClass("incompleto"); };

                            // console.log("valido correo de factura");
                            // console.log("Correo de factura validado");
                            // console.log("Envio formulario");
                            
                            
                            $("#formCompra").submit();

                        } else {
                           Swal.fire("","Todos los campos de facturación son obligatorios.","warning");

                            if (razonSocial != '') { $("#razonSocial").removeClass("incompleto");
                            } else { $("#razonSocial").addClass("incompleto"); };

                            if (direccionFactura != '') { $("#direccionFactura").removeClass("incompleto");
                            } else { $("#direccionFactura").addClass("incompleto"); };

                            if (rutFactura != '') { $("#RutFactura").removeClass("incompleto");
                            } else { $("#RutFactura").addClass("incompleto");};

                            if (giro != '') { $("#giro").removeClass("incompleto");
                            } else { $("#giro").addClass("incompleto"); }
                        };
                    } else {
                        // console.log("Cliente quiere Boleta");
                        // console.log("Envio formulario");
                        // console.log("comuna: " + comuna);
                        $("#formCompra").submit();
                    };
                }
            } else {
                //campos son vacios por lo que los valido
                Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe rellenar todos los campos resaltados en rojo.' });
                if (nombre != '') { $("#nombre").removeClass("incompleto");
                } else { $("#nombre").addClass("incompleto"); };
                
                if (rut != '') { $("#rut").removeClass("incompleto");
                } else { $("#rut").addClass("incompleto"); };
                
                if (email != '') { $("#email").removeClass("incompleto");
                } else { $("#email").addClass("incompleto"); };
                
                if (telefono != '') { $("#telefono").removeClass("incompleto");
                } else { $("#telefono").addClass("incompleto"); };
                
                if (direccion != '') { $("#direccion").removeClass("incompleto");
                } else { $("#direccion").addClass("incompleto");};
                        
                if (region != '' && region != 'Seleccione region' && region != '0') {
                    // console.log("Region seleccionada");
                    $("#uniform-region").removeClass("incompleto");
                } else {
                    Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe seleccionar una región.' });
                    $("#uniform-region").addClass("incompleto");
                };
                if (comuna != '0' && comuna != '') {
                    // console.log("Comuna seleccionada");
                    $("#uniform-comuna").removeClass("incompleto");
                } else {
                    Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe seleccionar una comuna.' });
                    $("#uniform-comuna").addClass("incompleto");
                };
                
                if (htmlComuna != 'Seleccione Comuna') { $("#uniform-comuna").removeClass("incompleto");
                } else {
                    Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe seleccionar una comuna.' });
                    $("#uniform-comuna").addClass("incompleto");
                };
                
            }
        } else if($("#tienda1").is(':checked')){                
                var nombre_retiro = $("#nombre_retiro").val();
                var telefono_retiro = $("#telefono_retiro").val();

                if (nombre_retiro != "" && telefono_retiro != "" && email != "" && nombre != "" && telefono != "" && rut != "") {
                    if (email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
                        Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Correo inválido.' });
                        $("#email").addClass("incompleto");
                    } else {
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //consulto por facturacion
                        
                        if ($("#facturaSi").is(':checked')) {
                            console.log("Cliente quiere factura");
                            var razonSocial = $("#razonSocial").val();
                            var direccionFactura = $("#direccionFactura").val();
                            var rutFactura = $("#RutFactura").val();
                            var giro = $("#giro").val();
                            if (razonSocial != '' && direccionFactura != '' && rutFactura != '' && giro != '') {
                                if (razonSocial != '') {$("#razonSocial").removeClass("incompleto");
                                } else {$("#razonSocial").addClass("incompleto");};
                                
                                if (direccionFactura != '') {$("#direccionFactura").removeClass("incompleto");
                                } else {$("#direccionFactura").addClass("incompleto");};
                               
                                if (rutFactura != '') {$("#RutFactura").removeClass("incompleto");
                                } else {$("#RutFactura").addClass("incompleto");};
                                
                                if (giro != '') {$("#giro").removeClass("incompleto");
                                } else {$("#giro").addClass("incompleto");};
                                
                                // console.log("valido correo de factura");
                                // console.log("Correo de factura validado");
                                // console.log("Envio formulario");
                                
                                $("#formCompra").submit();
                                                                
                            } else {
                                Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Todos los campos de facturación son obligatorios.' });
                                
                                if (razonSocial != '') { $("#razonSocial").removeClass("incompleto");
                                } else {  $("#razonSocial").addClass("incompleto"); };
                                
                                if (direccionFactura != '') { $("#direccionFactura").removeClass("incompleto");
                                } else { $("#direccionFactura").addClass("incompleto");  };
                                
                                if (rutFactura != '') { $("#RutFactura").removeClass("incompleto");
                                } else { $("#RutFactura").addClass("incompleto"); };
                               
                                if (giro != '') { $("#giro").removeClass("incompleto");
                                } else { $("#giro").addClass("incompleto");  }
                            };
                        } else {
                            console.log("Cliente quiere Boleta");
                            console.log("Envio formulario");
                            $("#formCompra").submit();
                        };
                        
                       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }
                
                } else {
                    Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe rellenar todos los campos resaltados en rojo.' });

                    if (nombre_retiro != '') {
                        $("#nombre_retiro").removeClass("incompleto");
                    } else {
                        $("#nombre_retiro").addClass("incompleto");
                    };

                    if (telefono_retiro != '') {
                        $("#telefono_retiro").removeClass("incompleto");
                    } else {
                        $("#telefono_retiro").addClass("incompleto");
                    };
                    if (email != '') {
                        $("#email").removeClass("incompleto");
                    } else {
                        $("#email").addClass("incompleto");
                    };
                    if (nombre != '') {
                        $("#nombre").removeClass("incompleto");
                    } else {
                        $("#nombre").addClass("incompleto");
                    };
                    
                    if (telefono != '') {
                        $("#telefono").removeClass("incompleto");
                    } else {
                        $("#telefono").addClass("incompleto");
                    };
                    
                    if (rut != '') {
                        $("#rut").removeClass("incompleto");
                    } else {
                        $("#rut").addClass("incompleto");
                    };
                }
            }//fin del else
    });

    
    
    /*===============================================================
    =            Botones de comprar y FORM para regresar            =
    ===============================================================*/
    
    $("#btnTerminarCarro").click(function(){
         if ($("#terminos").is(':checked')) {
             $("#formCompra").submit();
         } else {
             Swal.fire("", "Debe aceptar los terminos y condiciones", "warning");
         }
    });
    $("#volverAlCarro").click(function(){
        $("#formMiCarro2").submit();
    });
    $(".btnVerMiCarro").click(function(){
        $("#formCompra").attr("action", "mi-carro");
        $("#formCompra").submit();
    });
    
    
    /*=====  End of Botones de comprar y FORM para regresar  ======*/
    
    
    /*====================================================================
    =            Agregando favoritos o guardados para despues            =
    ====================================================================*/
    var loadinglike = false;
    $('.like, .btn-lista-ficha').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        var id_pro = $this.attr('rel');

        if($this.find('.fa-heart').hasClass('far')){ // NO GUARDADO
            $this.find('.fa-heart').removeClass('far');
            $this.find('.fa-heart').addClass('fas');

            if($this.parent().hasClass('favoritos')){
                $this.parent().find('span').text('Eliminar de favoritos');
            }

            if(!loadinglike){
                loadinglike = true;
                $.ajax({
                    type: 'GET',
                    url: 'tienda/addLista.php',
                    data: {id:id_pro, action:"add", qty:"1"},
                    cache: false,
                     success:function(rsp) {
                        console.log(rsp);
                        loadinglike = false;
                        $('.contWish').load('tienda/qtyWish.php');
                    }
                });
            }

        }else{ // GUARDADO
            $this.find('.fa-heart').removeClass('fas'); 
            $this.find('.fa-heart').addClass('far');

            if($this.parent().hasClass('favoritos')){
                $this.parent().find('span').text('Agregar a favoritos');
            }

            if(!loadinglike){
                loadinglike = true;
                $.ajax({
                    type: 'GET',
                    url: 'tienda/addLista.php',
                    data: {id:id_pro, action:"delete", qty:"1"},
                    cache: false,
                     success:function() {
                        loadinglike = false;
                        $('.contWish').load('tienda/qtyWish.php');
                    }
                });
            }
        }
    });
    
    /*=====  End of Agregando favoritos o guardados para despues  ======*/
    

    /*=======================================================
    =            Abrir primer filtro de acordion            =
    =======================================================*/
    $( ".accordion-titulo" ).each(function( index ) {
        $(this).addClass("open");
        return false;
    });
    $( ".accordion-content" ).each(function( index ) {
        $(this).css("display","block");
        return false;
    });
    
    /*=====  End of Abrir primer filtro de acordion  ======*/

    /*======================================================
    =            Cambiando apariencia de Grilla            =
    ======================================================*/
    // if (window.matchMedia('(max-width: 490px)').matches) {
    //     $(".grilla").addClass("grillaResponsive");
    //     $(".grillaResponsive").removeClass("grilla");
    // }else{
    //     $(".grillaResponsive").addClass("grilla");
    //     $(".grilla").removeClass("grillaResponsive");
    // }   
    
    /*=====  End of Cambiando apariencia de Grilla  ======*/    

    $('.box_spinner .spin_action').on('click', function(){
        var action = $(this).attr('data-action')
            lugar  = $(this).attr('data-where');
    
        var qty_spinn = parseInt($(this).parent().find('.qty_spin').html());
    
        if (action == 'agregar') {
            if (lugar == 'ficha') {
                $(this).parent().find('.qty_spin').html(qty_spinn + 1).change();
            }
        }else{
            if (qty_spinn > 1) {
                if (lugar == 'ficha') {
                    $(this).parent().find('.qty_spin').html(qty_spinn - 1).change();
                }
            }
        }
    })
    // $('.box_spinnerComp .spin_action').on('click', function(){
    //     var action = $(this).attr('data-action')
    //         lugar  = $(this).attr('data-where');
    
    //     var qty_spinn = parseInt($(this).parent().find('.qty_spin').html());
    
    //     if (action == 'agregar') {
    //         if (lugar == 'ficha') {
    //             $(this).parent().find('.qty_spin').html(qty_spinn + 1);
    //         }
    //     }else{
    //         if (qty_spinn > 1) {
    //             if (lugar == 'ficha') {
    //                 $(this).parent().find('.qty_spin').html(qty_spinn - 1);
    //             }
    //         }
    //     }
    // })


});
