/*
Funciones necesarias para agregar al carro, eliminar un producto del carro o restar un producto
*/
$(document).ready(function(){
	/*===========================================================
	=            Actualizando Popup con cambio de N°            =
	===========================================================*/
	$("#popUp").on("change",".cantidadCarro", function(){
		$(".cont_loading").fadeIn(200);
		var $this 		= $(this);
		var cantidad 	= $this.val();
		var id_pro 		= $this.attr('valId');
		$.ajax({
			type: 'GET',
			url: 'tienda/addCarro.php',
			data: {id:id_pro, action:"update2", qty:cantidad},
			cache: false,
			success:function(resp) {
				$(".cont_loading").fadeOut(200);
				$(".cantItems").load("tienda/qtyTotal.php"); 
				$.ajax({
					type: 'GET',
					url: 'pags/popUpAddToCart.php',
					data: {id:id_pro, qty:cantidad},
					cache: false,
						success:function(resp){
							$(".cont_loading").fadeOut(200);
							$(".cantItems").load("tienda/qtyTotal.php"); 
							$("#contShowCartPopUp").load("tienda/carroPopUp.php");
							$(".cont_loading").fadeOut(200);
						}
				});
			}
		});

	});

	$(".contCantCotizar").on("change",".cantidadCarro", function(){
		$(".cont_loading").fadeIn(200);
		var $this 		= $(this);
		var cantidad 	= $this.val();
		var id_pro 		= $this.attr('valId');
		$.ajax({
			type: 'GET',
			url: 'tienda/addCarro.php',
			data: {id:id_pro, action:"update2", qty:cantidad},
			cache: false,
			success:function() {
				$(".cont_loading").fadeOut(200);
				window.location.reload();
			}
		});


	});
	
	/*=====  End of Actualizando Popup con cambio de N°  ======*/

});


function quitarElementoCarro(id, minimo){
	$(".cont_loading").fadeIn(200);
		var id_pro = id;
		var cantidadDescontar = parseInt(minimo);
		console.log(cantidadDescontar);
			$.ajax({
				type: "GET",
				url: "tienda/addCarro.php",
				data: {id:id_pro, action:"removeSpinner", qty:cantidadDescontar},
				cache: false,
				 success:function(resp) {
					$(".totalHeader").load("tienda/totalCart.php");
					$("#contMiniCart").load("tienda/miniCart.php");
					$(".cantItems").load("tienda/qtyTotal.php");
					
					$("#contValoresResumen").load("tienda/resumenValoresShowCart.php");
					$("#contFilasCarro").load("tienda/showCart.php");
					$("#contProductosGuardados").load("tienda/saveForLater.php");
					$(".cont_loading").fadeOut(200);
					if (resp == 1) {
						Swal.fire({
							icon: 'error',
							text: 'No puedes cambiar la cantidad de un complemento, debes cambiar el producto principal y se actualizara su complemento'
						});
					}
				},
				error: function() {
					$(".cont_loading").fadeOut(200);
				}
			}).done(function(){
				$(".cont_loading").fadeOut(200);
			});	
	}


function agregarElementoCarro(id, minimo){
	console.log("agraga desde carro de compra");
	$(".cont_loading").fadeIn(200);
		var id_pro = id;
		var cantidad = parseInt(minimo);
		var cantActual = $(".cantItems").html();	
		console.log(cantidad);	
			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_validar_stock.php',
				data: {id:id_pro, qty:cantidad},
				cache: false,
				success:function(resp) {
					console.log(resp);
					if(resp >= 0){
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							data: {id:id_pro, action:"addSpinner", qty:cantidad},
							cache: false,
							 success:function(resp) {
								$(".totalHeader").load("tienda/totalCart.php");
								$(".cantItems").load("tienda/qtyTotal.php"); 
								$("#contValoresResumen").load("tienda/resumenValoresShowCart.php");
								$("#contFilasCarro").load("tienda/showCart.php");
								$("#contProductosGuardados").load("tienda/saveForLater.php");
								$(".cont_loading").fadeOut(200);
								if (resp == 1) {
									Swal.fire({
										icon: 'error',
										text: 'No puedes cambiar la cantidad de un complemento, debes cambiar el producto principal y se actualizara su complemento'
									});
								}
							}
						});
					}else{
						$(".cont_loading").fadeOut(200);
						Swal.fire("","No fue posible agregar tu producto por falta de stock","error")
					};
				}
			});
	}
	
function eliminaItemCarro(id){
		var id_pro = id;
		$.ajax({
			type: "GET",
			url: "tienda/addCarro.php",
			data: {id:id_pro, action:"delete"},
			cache: false,
			 success:function(resp) {
				$(".totalHeader").load("tienda/totalCart.php");
				$(".cantItems").load("tienda/qtyTotal.php"); 
				$("#contValoresResumen").load("tienda/resumenValoresShowCart.php");
				$("#contFilasCarro").load("tienda/showCart.php");
				$("#contProductosGuardados").load("tienda/saveForLater.php");
                 
                $("#fila_carro_"+id_pro).fadeOut(100); 
				if (resp == 1) {
					Swal.fire({
						icon: 'error',
						text: 'No puedes eliminar un complemento ya que se encuentra asociado al producto principal'
					  });
				}else{ 
					Swal.fire({
					  icon: 'success',
					  text: 'Producto eliminado con exito'
					});
				}
			},
			error: function() {
				$(".cont_loading").fadeOut(100);
			}
		});
	}

function eliminaItemCarroPopUp(id){
	var id_pro = id;
	$.ajax({
		type: "GET",
		url: "tienda/addCarro.php",
		data: {id:id_pro, action:"delete"},
		cache: false,
			success:function() {
			$(".cantItems").load("tienda/qtyTotal.php"); 
			$("#contShowCartPopUp").load("tienda/carroPopUp.php");
			//$("#totalCartPopUp").load("tienda/totalCart.php");
			$("#fila_carro_"+id_pro).fadeOut(100); 
			Swal.fire({
					icon: 'success',
					text: 'Producto eliminado con exito'
				});
		},
		error: function() {
			$(".cont_loading").fadeOut(100);
		}
	});
}



function agregarElementoCarroPopUp(id, minimo,where=0){
	console.log("agraga desde Pop Up");
	$(".cont_loading").fadeIn(200);
		var id_pro = id;
		var cantidad = parseInt(minimo);
		var cantActual = $(".cantItems").html();	
		console.log(cantidad);	
			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_validar_stock.php',
				data: {id:id_pro, qty:cantidad},
				cache: false,
				success:function(resp) {
					console.log(resp);
					if(resp >= 0){
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							// data: {id:id_pro, action:"add", qty:cantidad},
							data: {id:id_pro, action:"addSpinner", qty:cantidad},
							cache: false,
							 success:function(resp) {
								 /**/
								 $(".cantItems").load("tienda/qtyTotal.php"); 
								 $("#contShowCartPopUp").load("tienda/carroPopUp.php");
								 //$("#totalCartPopUp").load("tienda/totalCart.php");
								 $("#contDatosCartVariables").load("tienda/carroRetiroTienda.php");
        						 $("#contDatosCartVariables2").load("tienda/carroRetiroTienda.php");
        						 $(".totalesMovil").load("tienda/resumenCompraShortTotales.php?comuna_id=0&retiro=1");
								 $(".cont_loading").fadeOut(200);
								//  if (where == 1) {
								// 	 location.reload();
								//  }
								if (resp == 1) {
									Swal.fire({
										icon: 'error',
										text: 'No puedes cambiar la cantidad de un complemento, debes cambiar el producto principal y se actualizara su complemento'
									});
								}
								
							}
						});
					}else{
						$(".cont_loading").fadeOut(200);
						Swal.fire("","No fue posible agregar tu producto por falta de stock","error")
					};
				}
			});
	}

function quitarElementoCarroPopUp(id, minimo,where=0){
	$(".cont_loading").fadeIn(200);
		var id_pro = id;
		var cantidadDescontar = parseInt(minimo);
		console.log(cantidadDescontar);
			$.ajax({
				type: "GET",
				url: "tienda/addCarro.php",
				// data: {id:id_pro, action:"remove", qty:cantidadDescontar},
				data: {id:id_pro, action:"removeSpinner", qty:cantidadDescontar},
				cache: false,
				 success:function(resp) {
					$(".cantItems").load("tienda/qtyTotal.php");
					$("#contShowCartPopUp").load("tienda/carroPopUp.php");
                    //$("#totalCartPopUp").load("tienda/totalCart.php");
					$(".cont_loading").fadeOut(200);
					$("#contDatosCartVariables").load("tienda/carroRetiroTienda.php");
					$("#contDatosCartVariables2").load("tienda/carroRetiroTienda.php");
					$(".totalesMovil").load("tienda/resumenCompraShortTotales.php?comuna_id=0&retiro=1");
					// if (where == 1) {
					// 	location.reload();
					// }
					if (resp == 1) {
						Swal.fire({
							icon: 'error',
							text: 'No puedes cambiar la cantidad de un complemento, debes cambiar el producto principal y se actualizara su complemento'
						});
					}
				},
				error: function() {
					$(".cont_loading").fadeOut(200);
				}
			}).done(function(){
				$(".cont_loading").fadeOut(200);
			});	
	}



function eliminaItemCarro2(id){
		var id_pro = id;
		$.ajax({
			type: "GET",
			url: "tienda/addCarro.php",
			data: {id:id_pro, action:"delete"},
			cache: false,
			 success:function(resp) {
				$(".contCartCompraRapida").load("tienda/resumenCompraShort2.php");
				if (resp == 1) {
					Swal.fire({
						icon: 'error',
						text: 'No puedes eliminar un complemento ya que se encuentra asociado al producto principal'
					  });
				}else{ 
					Swal.fire({
					  icon: 'success',
					  text: 'Producto eliminado con exito'
					});
				}
			},
			error: function() {
				$(".cont_loading").fadeOut(100);
			}
		});
	}


