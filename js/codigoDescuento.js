$(document).ready(function(){
	// $(".contenido").on("click",".editar",function(){
	codigo = null;
	$('#contDatosCartVariables').on('click', '.aplicarDescuento', function(){
		let aux = 0;
		$(".descuento").each(function(){
		    recorrido = $(this).val();
		    if (recorrido != 0 && recorrido != null && aux == 0) {
		    	codigo = recorrido;
		    	aux = 1;
		    }
		});
        // valido codigo
        if (codigo != "" && codigo != null) {
		    comuna_id = $('#comuna').val();
		    // valido Comuna
		    if (comuna_id != 'Seleccione Comuna' && comuna_id != null) {    
		        let radio = $('input[name="instalacion"]:checked').val();
		        // valido Instalación
		        if (radio != "" && radio != null) {
		        	if(radio == "instalacionDomicilio") {
				        retiro = 2;
				        sucursalId = 0;
				    }else if(radio == "instalacionTaller") {
				    	retiro = 3;
				    	sucursalId = $('input[name=idSucursal]:checked', '.seccionSucursal').val();
				    }else if(radio == "despachoDomicilio") {
				        retiro = 0;
				        sucursalId = 0;
				    } else {
				        retiro = 1;
				        sucursalId = $('input[name=idSucursal]:checked', '.seccionSucursal').val();
				    }
				     if ((retiro == 3 && sucursalId == null) ||  (retiro == 1 && sucursalId == null)) {
				            Swal.fire("","Debes elegir primero una sucursal","warning");
				     }else{
				     	/*==============================================
				     	=            Ejecución de las cosas            =
				     	==============================================*/
				     	// Nota: Si se hacen todas las validaciones pertinentes entonces ya podemos enviar la informacion por ajax
				     	$("#loadingCheckDescuento").fadeIn(100);
						$.ajax({
							url         : "tienda/ajax_codigo_descuento.php",
							type        : "post",
							async       : true,
							data		: {codigo:codigo},
							cache: false,
							success     : function(resp){
								if(resp == 1){
									// $(".codigoOK").addClass("aceptado");
									// $(".codigoOK").html('<i class="fas fa-check fa-lg"></i>');
									// $("#loadingCheckDescuento").fadeOut(100);
									$("#contDatosCartVariables").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro+"&idSucursal="+sucursalId);				                    
									function aplicarCambios(){
										$(".descuento").attr('disabled', "disabled");
										$(".aplicarDescuento").css("background", "#e1e1e1");
										$('.aplicarDescuento').attr('disabled', "disabled");
									};
									setTimeout(aplicarCambios, 500);
									/*$(".contCartCompraRapida").load("tienda/carroConDescuento.php");*/
				                    
								} else {
									// $(".codigoOK").removeClass("aceptado");
									// $(".codigoOK").html('<i class="fas fa-ban fa-lg"></i>');
									// $("#loadingCheckDescuento").fadeOut(100);
									$("#contDatosCartVariables").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro+"&idSucursal="+sucursalId);
								};
							}
						});
				     	
				     	
				     	/*=====  End of Ejecución de las cosas  ======*/
				     	

				     }
		        	
		        }else{//ELSE de valido Instalación
		        	Swal.fire("","Debes elegir primero un tipo de instalación","warning");
		        }//Fin de valido Instalación
		    }else{//ELSE de valido Comuna
		    	Swal.fire("","Debes elegir primero una región y una comuna","warning");
		    }//Fin de valido Comuna    	        
    	}else{//ELSE de valido codigo
    		Swal.fire("","Debes ingresar un código para obtener un descuento","warning");
    	}//Fin de valido codigo
    });


    $('#contDatosCartVariables2').on('click', '.aplicarDescuento', function(){
		let aux = 0;
		$(".descuento").each(function(){
		    recorrido = $(this).val();
		    if (recorrido != 0 && recorrido != null && aux == 0) {
		    	codigo = recorrido;
		    	aux = 1;
		    }
		});
	resumen = $("#resumenConfirmacion").val();
	if (resumen != "si") {
        // valido codigo
        if (codigo != "" && codigo != null) {
		    comuna_id = $('#comuna').val();
		    // valido Comuna
		    if (comuna_id != 'Seleccione Comuna' && comuna_id != null) {    
		        let radio = $('input[name="instalacion"]:checked').val();
		        // valido Instalación
		        if (radio != "" && radio != null) {
		        	if(radio == "instalacionDomicilio") {
				        retiro = 2;
				        sucursalId = 0;
				    }else if(radio == "instalacionTaller") {
				    	retiro = 3;
				    	sucursalId = $('input[name=idSucursal]:checked', '.seccionSucursal').val();
				    }else if(radio == "despachoDomicilio") {
				        retiro = 0;
				        sucursalId = 0;
				    } else {
				        retiro = 1;
				        sucursalId = $('input[name=idSucursal]:checked', '.seccionSucursal').val();
				    }
				     if ((retiro == 3 && sucursalId == null) ||  (retiro == 1 && sucursalId == null)) {
				            Swal.fire("","Debes elegir primero una sucursal","warning");
				     }else{
				     	/*==============================================
				     	=            Ejecución de las cosas            =
				     	==============================================*/
				     	// Nota: Si se hacen todas las validaciones pertinentes entonces ya podemos enviar la informacion por ajax
				     	$("#loadingCheckDescuento").fadeIn(100);
						$.ajax({
							url         : "tienda/ajax_codigo_descuento.php",
							type        : "post",
							async       : true,
							data		: {codigo:codigo},
							cache: false,
							success     : function(resp){
								if(resp == 1){
									// $(".codigoOK").addClass("aceptado");
									// $(".codigoOK").html('<i class="fas fa-check fa-lg"></i>');
									// $("#loadingCheckDescuento").fadeOut(100);
									$("#contDatosCartVariables2").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro+"&idSucursal="+sucursalId);				                    
									function aplicarCambios(){
										$(".descuento").attr('disabled', "disabled");
										$(".aplicarDescuento").css("background", "#e1e1e1");
										$('.aplicarDescuento').attr('disabled', "disabled");
									};
									setTimeout(aplicarCambios, 500);
									/*$(".contCartCompraRapida").load("tienda/carroConDescuento.php");*/
				                    
								} else {
									// $(".codigoOK").removeClass("aceptado");
									// $(".codigoOK").html('<i class="fas fa-ban fa-lg"></i>');
									// $("#loadingCheckDescuento").fadeOut(100);
									$("#contDatosCartVariables2").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro+"&idSucursal="+sucursalId);
								};
							}
						});
				     	
				     	
				     	/*=====  End of Ejecución de las cosas  ======*/
				     	

				     }
		        	
		        }else{//ELSE de valido Instalación
		        	Swal.fire("","Debes elegir primero un tipo de instalación","warning");
		        }//Fin de valido Instalación
		    }else{//ELSE de valido Comuna
		    	Swal.fire("","Debes elegir primero una región y una comuna","warning");
		    }//Fin de valido Comuna    	        
    	}else{//ELSE de valido codigo
    		Swal.fire("","Debes ingresar un código para obtener un descuento","warning");
    	}//Fin de valido codigo
    }else{
    	if (codigo != "" && codigo != null) {
    		radio = $("#instalacionEnvio").val();
    		comuna_id = $("#comunaEnvio").val();
    		if(radio == "instalacionDomicilio") {
		        retiro = 2;
		        sucursalId = 0;
		    }else if(radio == "instalacionTaller") {
		    	retiro = 3;
		    	sucursalId = $('#sucursalEnvio').val();
		    }else if(radio == "despachoDomicilio") {
		        retiro = 0;
		        sucursalId = 0;
		    } else {
		        retiro = 1;
		        sucursalId = $('#sucursalEnvio').val();
		    }
		    /*==============================================
	     	=            Ejecución de las cosas            =
	     	==============================================*/
	     	// Nota: Si se hacen todas las validaciones pertinentes entonces ya podemos enviar la informacion por ajax
	     	$("#loadingCheckDescuento").fadeIn(100);
			$.ajax({
				url         : "tienda/ajax_codigo_descuento.php",
				type        : "post",
				async       : true,
				data		: {codigo:codigo},
				cache: false,
				success     : function(resp){
					if(resp == 1){
						$("#contDatosCartVariables2").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro+"&idSucursal="+sucursalId);				                    
						function aplicarCambios(){
							$(".descuento").attr('disabled', "disabled");
							$(".aplicarDescuento").css("background", "#e1e1e1");
							$('.aplicarDescuento').attr('disabled', "disabled");
						};
						setTimeout(aplicarCambios, 500);
					} else {
						$("#contDatosCartVariables2").load("tienda/carroConDescuento.php?comuna_id="+comuna_id+"&retiro="+retiro+"&idSucursal="+sucursalId);
					};
				}
			});
	     	
	     	
	     	/*=====  End of Ejecución de las cosas  ======*/	
    	}else{
    		Swal.fire("","Debes ingresar un código para obtener un descuento","warning");
    	}
    }
    });
});