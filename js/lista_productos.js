/*
Funciones para la lista de favoritos o productos gusrdados para despues
*/
$(function(){
		
	$('#agregarLista').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var cantidad = 1;
			$.ajax({
				type: 'GET',
				url: 'tienda/addLista.php',
				data: {id:id_pro, action:"add", qty:cantidad},
				cache: false,
				 success:function() {
					 $(".cont_loading").fadeOut(200);
					 Swal.fire({
					  icon: 'success',
					  title: 'Mi lista',
					  text: 'Tu producto se agregao con exito a tu lista'
					});
				}
			})
	});
	
	$('#agregarLista').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var cantidad = 1;
			$.ajax({
				type: 'GET',
				url: 'tienda/addLista.php',
				data: {id:id_pro, action:"add", qty:cantidad},
				cache: false,
				 success:function(resp) {
					 console.log(resp);
					 $(".cont_loading").fadeOut(200);
					 $(this).find("i").removeClass("far");
					 $(this).find("i").addClass("fas");
					 Swal.fire("","Tu producto se agregao con exito a tu lista","success");
				}
			})
	});
	
	$('.btnLista').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var cantidad = 1;
			$.ajax({
				type: 'GET',
				url: 'tienda/addLista.php',
				data: {id:id_pro, action:"add", qty:cantidad},
				cache: false,
				 success:function() {
					$(".cont_loading").fadeOut(200);
					Swal.fire("","Producto agregado a tu lista","success");
				}
			}).done(function(){
				$(".cargador").fadeOut(200);
			});
	});
	
	$('.eliminarLista').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var cantidad = 1;
			$.ajax({
				type: 'GET',
				url: 'tienda/addLista.php',
				data: {id:id_pro, action:"delete", qty:cantidad},
				cache: false,
				 success:function() {
					$(".cont_loading").fadeOut(200);
					$("#filaLista_"+id_pro).hide(100);
					Swal.fire("","Producto Eliminado","success");
				}
			})
	});
	
	$('.agregaCarroDesdeLista').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var cantidad = 1;
		var cantActual = $(".cantItems").html();	
		console.log(cantidad);	
			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_validar_stock.php',
				data: {id:id_pro, qty:cantidad},
				cache: false,
				success:function(resp) {
					console.log(resp);
					if(resp >= 0){
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							data: {id:id_pro, action:"add", qty:cantidad},
							cache: false,
							 success:function() {
								$(".cantItems").load("tienda/qtyTotal.php");
								$(".totalHeader").load("tienda/totalCart.php");
								$("#contMiniCart").load("tienda/miniCart.php");
								$.ajax({
									type: 'GET',
									url: 'pags/popUpAddToCart.php',
									data: {id:id_pro, qty:cantidad},
									cache: false,
									 success:function(resp){
										 $(".cont_loading").fadeOut(200);
										 $("#popUp").html(resp);
										 $(".fondoPopUp").fadeIn(100);
									 }
								 });
							}
						});
					} else {
						Swal.fire({
						  icon: 'error',
						  title: '',
						  text: 'stock insuficienteo'
						});
						$(".cont_loading").fadeOut(100);
					};
				}
			}).done(function(){
				$(".cargador").fadeOut(200);
			});
	});
});


function eliminaFavorito(id){
		$(".cont_loading").fadeIn(100);
		var id_pro = id;
		$.ajax({
			type: "GET",
			url: 'tienda/addLista.php',
			data: {id:id_pro, action:"delete"},
			cache: false,
			 success:function() {
				$(".totalHeader").load("tienda/totalCart.php");
				$(".cantItems").load("tienda/qtyTotal.php"); 
				$("#contValoresResumen").load("tienda/resumenValoresShowCart.php");
				$("#contFilasCarro").load("tienda/showCart.php");
				$("#contProductosGuardados").load("tienda/saveForLater.php");
				
				$(".cont_loading").fadeOut(200);
				Swal.fire("","Producto eliminado de tu lista","success");
			},
			error: function() {
				$(".cont_loading").fadeOut(100);
			}
		});
	}

function moverAlCarro(id){
		console.log("agraga desde guardados para despues");
		$(".cont_loading").fadeIn(200);
			var id_pro = id;
			var cantidad = parseInt(1);
			var cantActual = parseInt($("#fila_carro_"+id_pro + ".campoCantCarroResumen").val());	
			if(cantActual < 1){
				cantActual = 1;
				}
			console.log("cantidad actual: "+cantidad);	
				$.ajax({
					type: 'POST',
					url: 'ajax/ajax_validar_stock.php',
					data: {id:id_pro, qty:cantidad},
					cache: false,
					success:function(resp) {
						console.log(resp);
						if(resp >= 0){
							$.ajax({
								type: 'GET',
								url: 'tienda/addCarro.php',
								data: {id:id_pro, action:"add", qty:cantidad},
								cache: false,
								 success:function() {
									$.ajax({
										type: "GET",
										url: 'tienda/addLista.php',
										data: {id:id_pro, action:"delete"},
										cache: false,
										 success:function() {
											$(".totalHeader").load("tienda/totalCart.php");
											$(".cantItems").load("tienda/qtyTotal.php"); 
											$("#contValoresResumen").load("tienda/resumenValoresShowCart.php");
											$("#contFilasCarro").load("tienda/showCart.php");
											$("#contProductosGuardados").load("tienda/saveForLater.php");
											$(".cont_loading").fadeOut(200);
											Swal.fire("","Tu producto fue movido al carro de compras.","success");
										},
										error: function() {
											$(".cont_loading").fadeOut(100);
										}
									});
								}
							});
						}else{
							Swal.fire({
								  icon: 'error',
								  title: 'Stock insuficiente',
								  text: 'No fue posible agregar tu producto por falta de stock'
								})
							$(".cont_loading").fadeOut(200);
						};
					}
				});
			
	}

function agregarLista(id){
	$(".cont_loading").fadeIn(200);
		var id_pro = id;
		var cantidad = 1;
			$.ajax({
				type: 'GET',
				url: 'tienda/addLista.php',
				data: {id:id_pro, action:"add", qty:cantidad},
				cache: false,
				 success:function(resp) {
					 // console.log("asdasdsad" + resp);
                     $(".icono-header-right2 .cantItems").load("tienda/qtyFavoritos.php"); 
					 location.reload();
				}
			});
	}


function quitarLista(id){
	$(".cont_loading").fadeIn(200);
		var id_pro = id;
		var cantidad = 1;
			$.ajax({
				type: 'GET',
				url: 'tienda/addLista.php',
				data: {id:id_pro, action:"delete", qty:cantidad},
				cache: false,
				 success:function() {
					 location.reload();
				}
			});
	}


function guardarParaDespues(id){
	var id = id;
	
	//Elimino del carro
	
	$.ajax({
		type: "GET",
		url: "tienda/addCarro.php",
		data: {id:id, action:"delete"},
		cache: false,
		 success:function(resp) {
			 console.log("elimino del carro de compras" + resp);
			 //agrego a la lista
					$.ajax({
						type: 'GET',
						url: 'tienda/addLista.php',
						data: {id:id, action:"add", qty:1},
						cache: false,
						 success:function(resp2) {
							 console.log("agregado a la lista" + resp2);
							 //actualizo datos
							$(".totalHeader").load("tienda/totalCart.php");
							$(".cantItems").load("tienda/qtyTotal.php"); 
							$("#contValoresResumen").load("tienda/resumenValoresShowCart.php");
							$("#contFilasCarro").load("tienda/showCart.php");
							$("#contProductosGuardados").load("tienda/saveForLater.php");
									
							
							Swal.fire("","Producto guardado para mas tarde","success");

						}
					}).fail(function(resp){
						console.log(resp);
						});//fin ajax
			 }
	});	//fin ajax
	
}