$(function(){    
    /*=============================================
    =            Selectores de Filtros            =
    =============================================*/
    // $(".selectorFiltrosEspeciales").change(function(){
    //     let filtroEsp = $(this).val();
    //     window.location.href = filtroEsp;
    //     // alert(filtroEsp); 
    // });
    // $(".selectorFiltrosEspeciales").change(function(){
    //     let filtroEsp = $(this).val();
    //     let filtroEsp2 = $('option:selected', this).attr("fil");
    //     alert(filtroEsp2); 
    // });

    /*----------  Filtros de Lineas,Categorias  ----------*/
    $('.quitarFiltro').on('click', function() {
        url = $("#url").val();
        let marcaF = $(this).attr('id_marca');
        let todasMarcas = $('.checkMarcas:checked').length;
        if (todasMarcas == 1) {
            url += '/0';
        }else{
            $(".checkMarcas:checked").each(function(index) {
                if (index == 0)
                    if (marcaF != $(this).val()) {
                        url += '/' + $(this).val();
                    }else{
                        url += '/';
                    }
                else
                    if (marcaF != $(this).val()) {
                        url += '-' + $(this).val();
                    }
            });
        }
        let orden = $("#orden").val();
        if (orden != null) {
            url += '/'+orden;
        }
        let oferta = $("#ofertas").val();
        if (oferta != null) {
            url += '/'+oferta+"?";
        }
        if (url.indexOf("?") > -1 == false) {
            url += "?";
        }

        
        let variablesFiltro = $("#variablesFiltros").val();
        if (variablesFiltro != null) {
            url += variablesFiltro;
        }
        // resultado1 = variablesFiltro.includes(anchos);
        // alert(variablesFiltro);         
        window.location.href = url;        
    });
    
    /*=====  End of Selectores de Filtros  ======*/
  

    /*----------  Filtros de Lineas,Categorias  ----------*/
    $('.listadoMarcas').on('change', function() {
        url = $("#url").val();
        let controlMarcas = $('#controlMarcas').val();
        if (controlMarcas == null){
            let todasMarcas = $('.checkMarcas:checked').length;
            if (!todasMarcas) {
                url += '/0';
            }else{
                $(".checkMarcas:checked").each(function(index) {
                    if (index == 0)
                      url += '/' + $(this).val();
                    else
                      url += '-' + $(this).val();
                });
            }
        }
        let orden = $("#orden").val();
        if (orden != null) {
            url += '/'+orden;
        }
        let oferta = $("#ofertas").val();
        if (oferta != null) {
            url += '/'+oferta+"?";
        }
        if (url.indexOf("?") > -1 == false) {
            url += "?";
        }
        // let variablesFiltro = $("#variablesFiltros").val();
        // if (variablesFiltro != null) {
        //     url += variablesFiltro;
        // }
        let filtroEsp = $('.checkFiltroEsp:checked').length;
        filtros = Array();
        filtros2 = Array();
        numFiltroArray = Array();
        if (filtroEsp != 0) {
            $(".checkFiltroEsp:checked").each(function(index) {
                numFiltro = $(this).val().split('=')[0];
                valFiltro = $(this).val().split('=')[1];
                if (numFiltroArray.includes(numFiltro)) {
                    // filtros2.push(`${numFiltro}=${valFiltro}`);
                    filtros[numFiltro] += "-"+valFiltro;
                }else{
                    filtros[numFiltro] = valFiltro;
                    numFiltroArray.push(numFiltro);
                }
                url += '&' + `${numFiltro}=${filtros[numFiltro]}`;
                // url += '&' + $(this).val();
            });
            // console.log(url);
        }
        window.location.href = url;   
        // alert(url);     
    });
    
    /*=====  End of Selectores de Filtros  ======*/
    

    /*================================
    =            aCORDION            =
    ================================*/
    $(".accordion-titulo").click(function(e){  
        e.preventDefault();
        let contenido=$(this).next(".accordion-content");
        if(contenido.css("display")=="none"){ //open        
          contenido.slideDown(250);         
          $(this).addClass("open");
        }
        else{ //close       
          contenido.slideUp(250);
          $(this).removeClass("open");  
        }
    });
    $(".accordion-titulo-ficha").click(function(e){  
        e.preventDefault();
        let contenido=$(this).next(".accordion-content-ficha");
        if(contenido.css("display")=="none"){ //open        
          contenido.slideDown(250);         
          $(this).addClass("open");
        }
        else{ //close       
          contenido.slideUp(250);
          $(this).removeClass("open");  
        }
    });
    
    /*=====  End of aCORDION  ======*/
    

    
    /*============================
    =       Sub  MEnu            =
    ============================*/
    $("header").mouseleave(function(){
        $(".contenido").hide();
    });
    let clickMenu = '';
    $('.subMenus').hover(function(){
        let menu = $(this).attr("menu");
        if (clickMenu != menu) {
            $('.contenido').hide(100);
            clickMenu = menu;
        }
            $(this).children('.contenido').stop();
            $(this).children('.contenido').slideDown();
        // envio de variable
        $.post( 'ajax/menuDesplegable.php', { id: menu} ).done( function( respuesta ){
            $( '.contenido' ).html( respuesta );
            $('.contenido').toggle(100);
        });
        }, function() {
            $(this).children('.contenido').stop();
            $(this).children('.contenido').slideUp();
    });    
    /*=====  End of MEnu  ======*/
    
     /*====================================
    =            Mas Buscadas            =
    ====================================*/
    $('.textBusqueda').on('click', function() {
        let busqueda = $(this).html();
        $('.campo_buscador').val(busqueda);
        $("#formBuscador").submit();
        
    });
    /*=====  End of Mas Buscadas  ======*/
    
    /*===================================================
    =            Empresas y USuarios            =
    ===================================================*/
    // selector dependiente de empresas y usuarios
    $(document).on('change', '#rutEmpresa', function(event) {
        let rut = $(this).val();
        $.ajax({
            type: "POST",
            url: "ajax/ajaxEmpresaUsuario.php",
            data: {rutEmpresa: rut },
            dataType: "json",
            success: function(resp) {
                // console.log(resp);
                $('#selectUsuariosEmpresas').html(resp.usuarios).click();
            }
        });
    });
    /*=====  End of Empresas y Usuarios  ======*/
    
    
    
    
    
    $(".titulosMenuTabs a").click(function(){
        var tab = $(this).attr("rel");
        
        $(".titulosMenuTabs a").removeClass("tabActivo");
        $(this).addClass("tabActivo");
        $(".tabsGenerico").fadeOut(100, function(){
            $("#" + tab).fadeIn(100);
        });
        
    });
    
    
    
    /*Funciones de traking*/
    $(".btnSeguimiento").click(function(){
        console.log("dfgdfgdgf");
        $(".fondoPopUpTraking").fadeIn(100);
        $(".traking").fadeOut(10);
        $(".formPopUp").fadeIn(100);
    });
    $(".cerrarPopUpTraking").click(function(){
        $(".fondoPopUpTraking").fadeOut(100);
    });
    
    $("a.btnBuscarPedido").click(function(){
        var correoSeguimiento = $("#correoSeguimiento").val();
        var ordenDeCompra = $("#ordenDeCompra").val();
        
        if(correoSeguimiento != '' && ordenDeCompra != ''){
            if (correoSeguimiento.indexOf('@', 0) == -1 || correoSeguimiento.indexOf('.', 0) == -1) {
                Swal.fire("","Ingrese un correo válido","warning");
                $("#correoSeguimiento").val("");
                $("#correoSeguimiento").addClass("incompleto");
            } else {
                console.log("envio solicitud por ajax");
                $.ajax({
                    url: "ajax/traking.php",
                    type: "post",
                    dataType: 'json',
                    async: false,
                    data: {oc: ordenDeCompra, correo: correoSeguimiento},
                    success: function(resp) {
                        var json_string = JSON.stringify(resp);
                        var obj = jQuery.parseJSON(json_string);
                        
                        if(obj.estado == 1){
                            /*segun la respuesta cargo el contenido en la etiqueta indicada*/
                            $(".formPopUp").fadeOut(100);
                            $(".traking").html(obj.html);
                            $(".traking").fadeIn(200);
                        } else {
                            Swal.fire("","No encontramos registros con los datos ingresados","error");
                        }
                        console.log("data:  " + resp);
                        console.log("codError:  " + obj.estado);
                        
                        
                    }
                });
            }
        } else {
            Swal.fire({
			  icon: 'error',
			  text: 'Debe ingresar ambos campos.'
			});
            if(correoSeguimiento == ""){
                $("#correoSeguimiento").addClass("incompleto");
            } else {
                $("#correoSeguimiento").removeClass("incompleto");
            }
            
            if(ordenDeCompra == ""){
                $("#ordenDeCompra").addClass("incompleto");
            } else {
                $("#ordenDeCompra").removeClass("incompleto");
            }
            
        }
            
    });
    
    
    $("a.btnSeguimientoInterior").click(function(){
        var correoSeguimiento = $(this).attr("data-rel");
        var ordenDeCompra = $(this).attr("rel");
        
        console.log("envio solicitud por ajax");
        $.ajax({
            url: "ajax/traking.php",
            type: "post",
            dataType: 'json',
            async: false,
            data: {oc: ordenDeCompra, correo: correoSeguimiento},
            success: function(resp) {
                var json_string = JSON.stringify(resp);
                var obj = jQuery.parseJSON(json_string);

                if(obj.estado == 1){
                    /*segun la respuesta cargo el contenido en la etiqueta indicada*/
                    $(".fondoPopUpTraking").fadeIn(100);
                    $(".formPopUp").fadeOut(100);
                    $(".traking").html(obj.html);
                    $(".traking").fadeIn(200);
                } else {
                    Swal.fire("","No encontramos registros con los datos ingresados","error");
                }
                console.log("data:  " + resp);
                console.log("codError:  " + obj.estado);


            }
        });
     });
    /*Funciones de traking*/
    
    
    
    
    /*funciones para el pop up de calcular costo de despacho*/
    $(".btnSimularDespacho").click(function(){
        $(".fondoPopUpDespacho").fadeIn(100);
    });
    
    $(".cerrarPopUpCostoDespacho").click(function(){
        $(".fondoPopUpDespacho").fadeOut(100);
    });
    
    $(".popUpDespacho #region2").change(function(){
        var region_id = $(this).val();
        $(".popUpDespacho #comuna2").load("tienda/comunas.php?region_id=" + region_id);
        $(".popUpDespacho #uniform-comuna2 span").html("Seleccione comuna");
    });
    
    $(".popUpDespacho #comuna2").change(function() {
        var comuna_id = $(this).val();
        /*aca eejecuto la funcion para calcular el despacho y ponerlo como html en span */
        $(".valorCosto").html("valor despacho");
    });
    
    /*funciones para el pop up de calcular costo de despacho*/
    
    
    
    
    
    /*Funcion para calcular las cajas de ceramica segun los metros que necesita el cliente*/
    $(".btnCalcular").click(function(){
        var metrosCliente = $("#cantm2").val();
        var metrosCaja = parseFloat($("#cantm2").attr("rel"));
        
        var cajas = Math.ceil((metrosCliente / metrosCaja) + 1);
        var mensaje = "Debes comprar " + cajas + " cajas para cubrir tus " + metrosCliente+"m2";
        $(".cantidad").html("<input type='text' name='cantidad' id='cantidad' class='campoCantidad' value='"+cajas+"'>");
        $("#cantidad").numeric();
        
        console.log(mensaje);
        Swal.fire({
			  icon: 'success',
			  text: mensaje
			});
    });
    
    
    $("#elementosCarro").click(function(){
        // console.log("abro pop up");
        $.ajax({
            type: 'GET',
            url: 'pags/popUpAddToCart.php',
            data: {},
            cache: false,
             success:function(resp){
                 $(".cont_loading").fadeOut(200);
                 $("#popUp").html(resp);
                 $(".fondoPopUp").fadeIn(100);
             }
         });
    });
    
	 /*Menu responsive */
	 $('.toggle-btn').click(function(){
		$('.filter-btn').toggleClass('open');
	
	 });
	 $('.filter-btn a').click(function(){
		$('.filter-btn').removeClass('open');
	 });
	 
	$('.contBtnMenuResponsive').click(function () {
		$('.hamburger').toggleClass('open');
		$('.contMenu').slideToggle(100);
        if($(".hamburger").hasClass('open')){
            console.log("existe");
            $(".contBtnMenuResponsive").addClass("rojo2");
        } else {
            $(".contBtnMenuResponsive").removeClass("rojo2");
            console.log("no existe");
        }
	});
    
    
    $('.menuEscritorio').click(function () {
		$('.hamburgerEscritorio').toggleClass('open');
		$('.contMenuEscritorio').slideToggle(100);
        if($(".hamburgerEscritorio").hasClass('open')){
            console.log("existe");
            $(".menuEscritorio").addClass("rojo2");
        } else {
            $(".menuEscritorio").removeClass("rojo2");
            console.log("no existe");
        }
	});
    
	
    $(".ItemMenuEscritorio").hover(function(){
        
        var nombreLinea = $(this).find("a span").html();
        var elemento = $(this);
        var id = $(this).attr("rel");
        $("li.ItemMenuEscritorio").removeClass("activo");
        elemento.addClass("activo");
        $( ".grillaCategoriaMenu" ).fadeOut(50);
        
        $("#nombreLineaMenu").html(nombreLinea);
        $( ".grillaCategoriaMenu" ).each(function( index ) {
          //console.log( index + ": " + $( this ).text() );
            if(id == $(this).attr("rel")){
                $(this).fadeIn(50);
            }
        });
    }, function(){
        //$( ".menuCategorias li" ).fadeOut(50);
    });
    
    
    
    
    
    
    
    /*estilos menu responsive*/
	$(".lineaIcono").click(function(){
		console.log("icono lineas");
		$(this).parent().find("ul.submenuCategoria").show(100);
		});
    
    $(".volverLineas").click(function(){
		$(this).parent().parent().find("ul.submenuCategoria").hide(100);
		});
    
	$(".categoriaIcono").click(function(){
		//$("ul.sub").hide();
		console.log("icono categoria");
		$(this).parent().find("ul.subSub").show(100);
		});
	
    $(".volverCategorias").click(function(){
		$(this).parent().parent().find("ul.subSub").hide(100);
		});
   /*estilos menu responsive*/
    
    
    /*estilos footer responsive*/
    $(".accordeonFooter").click(function(){
        $(".contColumnaOcultaMovil").slideUp(100);
        $(this).next().slideToggle(100);
    });
     /*estilos footer responsive*/
    
	$(".abrirFooter").click(function(){
		$(".contrespFooter").hide(100);
		$(this).parent().parent().find(".contrespFooter").show(1000);
		});
	 /*Menu responsive */
	
    /*EFECTO INPUT*/
    $(".label_better").label_better({
      easing: "bounce"
    });
    /*FIN EFECTO INPUT*/
    
	/*Buscador responsive*/ 
	$('.iconoBuscador').click(function () {
		$('.buscador').slideToggle();
	});
	
    
    $(".btnFiltrosMovil").click(function(){
        $(".fondoLateralFiltro").fadeIn(100);
    });
    $(".fondoLateralFiltro").click(function(e){
        if (e.target !== this)
        return;  
        $(".fondoLateralFiltro").fadeOut(100);
    });
    
  
    
    
    $(".filaCarroMovil").click(function(){
       $(".contOcultoCarroResponsive").slideToggle(100); 
    });
    
    /*Botonera responsive de identificacion*/
    $(".contBotonesIdentificacionResponsive a").click(function(){
			$(".contBotonesIdentificacionResponsive a").removeClass("active");
			$(this).addClass("active");
			$(".cont50Identificacion").fadeOut(100);
			var tabs = $(this).attr("rel");
			$("#" + tabs).fadeIn(100);
		});
    
    
    /*BOTONERAS ENVIO Y PAGO RESPONSIVE*/
	$(".contBotoneraEnvioYPago a").click(function(){
		$(".contBotoneraEnvioYPago a").removeClass("activo");
		$(this).addClass("activo");
		var tab = $(this).attr("rel");
		$(".contPaso3Movil").fadeOut(100);
		$("#"+tab).fadeIn(100);
		if(tab == "misDatos"){
			$("#facturaMovil").fadeIn(100);
			}
		
		});
	/*BOTONERAS ENVIO Y PAGO RESPONSIVE*/
    
    $(".btnAceptar").click(function(){
        $(".notificacionFooter").fadeOut(100);
    });
    
	$('#btnNewsletter').click(function(){
		var correo = $('#newsletter').val();
		if(correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
            $("#contMensaje").html("<h4>¡Problemas!</h4><p>El correo electrónico introducido no es correcto</p>");
            $(".notificacionFooter").fadeIn(100);
            /*Swal.fire({
			  icon: 'error',
			  text: 'El correo electrónico introducido no es correcto'
			});*/
			return false;
        } else {
			$.ajax({
				url      : "ajax/newsletter.php",
				type     : "post",
				async    : true,
				data 	 : {correo : correo},
				success  : function(resp){
					console.log(resp);
					if(resp == 1){
						$("#contMensaje").html("<h4>¡Gracias por suscribirte a <br> Zona Promo!</h4><p></p>");
                        $(".notificacionFooter").fadeIn(100);
                    }else if(resp == 2){
						$("#contMensaje").html("<h4>¡Problemas!</h4><p>tu correo ya se encuentra en nuestros registros</p>");
                         $(".notificacionFooter").fadeIn(100);
                    }else{
                        $("#contMensaje").html("<h4>¡Problemas!</h4><p>No se a podido completar el registro intente mas tarde</p>");
                         $(".notificacionFooter").fadeIn(100);
                    }
				}
			});
		};
	});
/*
Agregar correo al newsletter -------------------------	----------------------	----------------------	
*/
	
    
    /*accordeon ficha*/
    $(".tituloFilaAccordeon").click(function(){
        $(".contDescricionesFicha").fadeOut(100);
        $(this).parent().find(".contDescricionesFicha").fadeIn(200);
        
    });
    /*accordeon ficha*/

/*
GUARDAR NUEVA DIRECCION
*/
$("#guardarDireccion").click(function(){
	var nombre = $("#nombre").val();
	var region = $("#region").val();
	var comuna = $("#comuna").val();
	var calle = $("#calle").val();
	var numero = $("#numero").val();
	
	console.log(region);
	console.log(comuna);
	if(nombre != "" && region != "" && comuna != "" && calle != "" && numero != ""){
		$("#formNuevaDireccion").submit();
		} else {
			console.log("Faltan campos por completar.");
			Swal.fire({
			  icon: 'error',
			  text: 'Faltan campos por completar.'
			});
            if(nombre == ""){$("#nombre").addClass("incompleto");} else {$("#nombre").removeClass("incompleto");}
            if(region == 16){$("#uniform-region").addClass("incompleto");} else {$("#uniform-region").removeClass("incompleto");}
            
            if(comuna == "Seleccione Comuna"){$("#uniform-comuna").addClass("incompleto");} else {$("#uniform-comuna").removeClass("incompleto");}
            
            if(calle == ""){$("#calle").addClass("incompleto");} else {$("#calle").removeClass("incompleto");}
            if(numero == ""){$("#numero").addClass("incompleto");} else {$("#numero").removeClass("incompleto");}
		}
	
});


/*Eliminar direcciones guardadas*/	
$(".deleteDireccion").click(function(){
	var id = $(this).attr("rel");
	console.log(id);
	$.ajax({
		url      : "ajax/eliminaDireccion.php",
		type     : "post",
		async    : true,
		data 	 : {id : id},
		success  : function(resp){
			console.log(resp);
			if(resp == 1){
				console.log("dirección eliminada");
				Swal.fire({
					  icon: 'success',
					  text: 'Dirección eliminada.'
					});
					$("#dir_"+id).hide(300);
				
				} else {
					console.log("No fue posible eliminar dirección.");
						Swal.fire({
						  icon: 'error',
						  text: '"No fue posible eliminar dirección.'
						});
					}
		}
	});

});
	
	
	

	
	

	
	
	/*
Registro
*/
    $("#radioEmpresa").click(function(){
        $(".camposOcultos").fadeIn(100);    
    });
    $("#radioPersona").click(function(){
        $(".camposOcultos").fadeOut(100);    
    });
	$("#btnRegistro").click(function(){
		var nombre = $("#nombre").val();
		/*var apellido = $('#apellido').val();*/
        var telefono = $('#telefono').val();
		var rut = $("#rut").val();
		var email = $("#email").val();
		var clave = $("#clave").val();
		var clave2 = $("#clave2").val();
        
        var codigo_empresa = $("#codigo_empresa").val();
        var nombre_empresa = $("#nombre_empresa").val();
        
       
		if (telefono.length < 9) {
           $("#telefono").addClass("campo_vacio");
           Swal.fire('','El número de teléfono debe tener mínimo 9 dígitos','error'); 
        }else if(nombre != '' && email != '' && clave != '' && clave2 !='' && telefono != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				//alertify.error("Email invalido");
				Swal.fire('','Email invalido','error'); 
			} else {
				/* Correo valido */
				if(clave === clave2){
					/* Envio el formulario */
                    if($("#radioEmpresa").is(":checked")){
                        console.log("radio empresa marcado");
                        if(codigo_empresa != '' && nombre_empresa != ''){
                            $("#formularioRegistro").submit();
                        } else {
                            Swal.fire('','Faltan campos por completar','error'); 
                            if(codigo_empresa == '' ){$("#codigo_empresa").addClass("campo_vacio");} else { $("#codigo_empresa").removeClass("campo_vacio");};
                            if(nombre_empresa == '' ){$("#nombre_empresa").addClass("campo_vacio");} else { $("#nombre_empresa").removeClass("campo_vacio");};
                        }
                    } else {
                        $("#formularioRegistro").submit();
                    }
					
				} else {
					/* Claves no coinciden */
					//alertify.error("Claves no coinciden");
					Swal.fire('','Claves no coinciden','error'); 
					$("#clave").addClass("campo_vacio");
					$("#clave2").addClass("campo_vacio");
				};
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			Swal.fire('','Faltan campos por completar','error'); 
			if(nombre == '' ){
				$("#nombre").addClass("campo_vacio");
			} else {
				$("#nombre").removeClass("campo_vacio");
			};

			if(telefono == '' ){
				$("#telefono").addClass("campo_vacio");
			} else {
				$("#telefono").removeClass("campo_vacio");
			};
			
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(rut == '' ){
				$("#rut").addClass("campo_vacio");
			} else {
				$("#rut").removeClass("campo_vacio");
			};
			
			if(clave == '' ){
				$("#clave").addClass("campo_vacio");
			} else {
				$("#clave").removeClass("campo_vacio");
			};
			
			if(clave2 == '' ){
				$("#clave2").addClass("campo_vacio");
			} else {
				$("#clave2").removeClass("campo_vacio");
			};

			
			
		};
		
	});/*fin registro paso 1*/
	
	
	
	
	/*Actualizar */
	$("#btnActualizar").click(function(){
		var nombre = $("#nombre").val();
		var apellido = $('#apellido').val();
		var Rut = $("#rut").val();
		var email = $("#email").val();
		var telefono = $("#telefono").val();
		var clave = $("#clave").val();
		var clave2 = $("#clave2").val();

		
		if(clave == clave2){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				Swal.fire('','Email incorrecto','error'); 
				} else {
					$("#formularioActualizar").submit();
					}
			
			} else if(clave == "" && clave2 == ""){
				if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
					Swal.fire('','Email incorrecto','error'); 
					} else {
						$("#formularioActualizar").submit();
						}
			} else {
					Swal.fire('','Claves no coinciden','error'); 
				}
				
	});/*fin actualizar1*/

    /*Actualizar EMPRESA */
	$("#btnActualizarEmpresaMiCuenta").click(function(){
		var nombreEmpresa = $("#nombreEmpresa").val();
		var rutEmpresa = $("#rutEmpresa").val();
		var direccionFiscal = $("#direccionFiscal").val();
		var giro = $("#giro").val();
        if (nombreEmpresa != "" && rutEmpresa != "" && direccionFiscal != "" && giro != "") {
            $("#formularioActualizarEmpresaMiCuenta").submit();
        }else{
            Swal.fire('Oops!','Faltan campos por completar','error'); 
			if(nombreEmpresa == '' ){
				$("#nombreEmpresa").addClass("campo_vacio");
			} else {
				$("#nombreEmpresa").removeClass("campo_vacio");
			};
            if(rutEmpresa == '' ){
				$("#rutEmpresa").addClass("campo_vacio");
			} else {
				$("#rutEmpresa").removeClass("campo_vacio");
			};
			if(direccionFiscal == '' ){
				$("#direccionFiscal").addClass("campo_vacio");
			} else {
				$("#direccionFiscal").removeClass("campo_vacio");
			};
            if(giro == '' ){
				$("#giro").addClass("campo_vacio");
			} else {
				$("#giro").removeClass("campo_vacio");
			};
        }
	});/*fin actualizar1*/

    /*Actualizar VENDEDOR*/
	$("#btnActualizarVendedor").click(function(){
		let nombre = $("#nombreVendedor").val();
		let apellido = $('#apellidoVendedor').val();
		let rut = $("#rutVendedor").val();
		let email = $("#emailVendedor").val();
		let telefono = $("#telefonoVendedor").val();
		let clave = $("#claveVendedor").val();
		let clave2 = $("#clave2Vendedor").val();
		
		if(clave == clave2){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				Swal.fire('','Email incorrecto','error'); 
			} else if(nombre == "" || apellido == "" || rut == "" || email == "" || telefono == "") {
                Swal.fire('Oops...','Debes rellenar los campos obligatorios resaltados en rojo','error'); 
                if(nombre == '' ){
                    $("#nombreVendedor").addClass("campo_vacio");
                } else {
                    $("#nombreVendedor").removeClass("campo_vacio");
                };
                if(apellido == '' ){
                    $("#apellidoVendedor").addClass("campo_vacio");
                } else {
                    $("#apellidoVendedor").removeClass("campo_vacio");
                };
                if(rut == '' ){
                    $("#rutVendedor").addClass("campo_vacio");
                } else {
                    $("#rutVendedor").removeClass("campo_vacio");
                };
                if(email == '' ){
                    $("#emailVendedor").addClass("campo_vacio");
                } else {
                    $("#emailVendedor").removeClass("campo_vacio");
                };
                if(telefono == '' ){
                    $("#telefonoVendedor").addClass("campo_vacio");
                } else {
                    $("#telefonoVendedor").removeClass("campo_vacio");
                };
			}else{
                $("#formularioActualizarVendedor").submit();
            }
        } else {
            Swal.fire('Oops...','Las claves deben ser iguales','error'); 
        }
				
	});/*fin actualizar VENDEDOR*/
	
	
	
	
    
    /*Actualizar Empresa*/
	$("#btnActualizarEmpresa").click(function(){
		var nombre_empresa = $("#empresa").val();
		var razon_social = $('#razon_social').val();
        var Rut_empresa = $("#Rut_empresa").val();
        var giro = $("#giro").val();
        var direccion_facturacion = $("#direccion_facturacion").val();
		var email_facturacion = $("#email_facturacion").val();
		var telefono_facturacion = $("#telefono_facturacion").val();
		
		
        if(email_facturacion.indexOf('@', 0) == -1 || email_facturacion.indexOf('.', 0) == -1) {
				Swal.fire('','Email incorrecto','error'); 
				} else {
					$("#formularioActualizar").submit();
					}
				
	});/*fin actualizarEmpresa*/
    
	
	
	
	
	/*Inicio de session*/
	$("#inicioSesion").click(function(){
		console.log("entra");
		var email = $("#email").val();
		var password = $("#clave").val();
		
		
		if(email != '' && password != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				//alertify.error("Email invalido");
				Swal.fire('','Email invalido','error'); 
			} else {
				/*envio formulario para log*/
				$("#formularioLogin").submit();
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			Swal.fire('','Faltan campos por completar','error'); 
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(password == '' ){
				$("#clave").addClass("campo_vacio");
			} else {
				$("#clave").removeClass("campo_vacio");
			};
		};
	});/*fin login*/
	
	
	
	
	/*Inicio de session carro de compras*/
	$("#inicioSesionIdentificacion").click(function(){
		
		var email = $("#email").val();
		var password = $("#clave").val();
		
		console.log(password);
		if(email != '' && password != ''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				Swal.fire('','Email invalido','error'); 
			} else {
				/*envio formulario para log*/
				$(".formInicioSesion").submit();
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			Swal.fire('','Faltan campos por completar','error'); 
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(password == '' ){
				$("#clave").addClass("campo_vacio");
			} else {
				$("#clave").removeClass("campo_vacio");
			};
		};
	});/*fin login*/
	
	
	
	
/*Recuperar clave ----------------------------------------------------------------------------------------------------*/
	$('#recuperarClave').click(function(){
		//alert("sdfdsf");
		var correo = $('#email').val();
		if(correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
            Swal.fire('','El correo electrónico introducido no es correcto.','error'); 
			return false;
        } else {
			console.log(correo);
			$("#formRecuperarClave").submit();
		};
	});/*fin funcion*/
/*Recuperar clave ----------------------------------------------------------------------------------------------------*/

/*Recuperar clave Vendedora--*/
$('#recuperarClaveVendedora').click(function(){
    //alert("sdfdsf");
    var correo = $('#email').val();
    if(correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
        Swal.fire('','El correo electrónico introducido no es correcto.','error'); 
        return false;
    } else {
        console.log(correo);
        $("#formRecuperarClave").submit();
    };
});/*fin funcion*/
/*Recuperar clave Vendedora--*/

	
	/* Encvio formulario para crear cuenta a partir de los datos obtenidos de una compra exitosa*/
	$(".btnCuentaExito").click(function(){
		$("#crearCuentaDesdeExito").submit();
		});
	
});







	
	
	/*
Validar rut
*/

$("#rutVendedor").Rut({
    on_error: function(){ 
                    //Swal.fire(type:'error', text:'');
                 Swal.fire('Error','Rut incorrecto','error'); 
                 $("#rutVendedor").addClass("campo_vacio");
                 $("#rutVendedor").val("");
             },
    format_on: 'keyup',
    on_success: function(){ $("#rutVendedor").removeClass("campo_vacio");} 
 });
 $("#rutCotizar").Rut({
    on_error: function(){ 
                    //Swal.fire(type:'error', text:'');
                 Swal.fire('Error','Rut incorrecto','error'); 
                 $("#rutCotizar").addClass("campo_vacio");
                 $("#rutCotizar").val("");
             },
    format_on: 'keyup',
    on_success: function(){ $("#rutCotizar").removeClass("campo_vacio");} 
 });
 $("#rutEmpresaCotizar").Rut({
    on_error: function(){ 
                    //Swal.fire(type:'error', text:'');
                 Swal.fire('Error','Rut incorrecto','error'); 
                 $("#rutEmpresaCotizar").addClass("campo_vacio");
                 $("#rutEmpresaCotizar").val("");
             },
    format_on: 'keyup',
    on_success: function(){ $("#rutEmpresaCotizar").removeClass("campo_vacio");} 
 });
 $("#rutEmpresa").Rut({
    on_error: function(){ 
                    //Swal.fire(type:'error', text:'');
                 Swal.fire('Error','Rut incorrecto','error'); 
                 $("#rutEmpresa").addClass("campo_vacio");
                 $("#rutEmpresa").val("");
             },
    format_on: 'keyup',
    on_success: function(){ $("#rutEmpresa").removeClass("campo_vacio");} 
 });
$("#rut").Rut({
   on_error: function(){ 
   				//Swal.fire(type:'error', text:'');
				Swal.fire('Error','Rut incorrecto','error'); 
				$("#rut").addClass("campo_vacio");
				$("#rut").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#rut").removeClass("campo_vacio");} 
});
$("#rut_retiro").Rut({
   on_error: function(){ 
   				//Swal.fire(type:'error', text:'');
				Swal.fire('Error','Rut incorrecto','error'); 
				$("#rut").addClass("campo_vacio");
				$("#rut").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#rut").removeClass("campo_vacio");} 
});
$("#RutFactura").Rut({
   on_error: function(){ 
   				Swal.fire('Error','Rut incorrecto','error'); 
				$("#RutFactura").addClass("incompleto");
				$("#RutFactura").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#RutFactura").removeClass("incompleto");} 
   
});

$("#Rut_empresa").Rut({
   on_error: function(){ 
   				alertify.error('El rut ingresado es incorrecto'); 
				$("#Rut_empresa").addClass("incompleto");
				$("#Rut_empresa").val("");
			},
   format_on: 'keyup',
   on_success: function(){ $("#Rut_empresa").removeClass("incompleto");} 
   
});
	
$('.countdown').each(function(){
	var $this = $(this);
	var date = $this.attr('rel');
	var countDownDate = new Date(date).getTime();
	
	var x = setInterval(function() {
		var now = new Date().getTime();
		var distance = countDownDate - now;
		
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		$this.empty();
		
        
		if(days == 1){
			$this.append("<div class='columnaCountDown'><div class='cdn'>" +days + "</div><div class='cdt'>Día</div></div><div class='columnaCountDown'><div class='cdn'>" + hours + "</div><div class='cdt'>Hrs</div></div><div class='columnaCountDown'><div class='cdn'>" + minutes + "</div><div class='cdt'>Min</div></div>");
		}else if(days > 0){
			$this.append("<div class='columnaCountDown'><div class='cdn'>" + days + "</div><div class='cdt'>Días</div></div><div class='columnaCountDown'><div class='cdn'>" + hours + "</div><div class='cdt'>Hrs</div></div><div class='columnaCountDown'><div class='cdn'>" + minutes + "</div><div class='cdt'>Min</div></div>");
		}else if(hours == 1){
			$this.append("<div class='columnaCountDown'><div class='cdn'>"+ hours +"</div><div class='cdt'>Hrs</div></div><div class='columnaCountDown'><div class='cdn'>" + minutes + "</div> <div class='cdt'>Min</div></div> <div class='columnaCountDown'><div class='cdn'>" + seconds + "</div> <div class='cdt'>Seg</div></div>");
		}else if(hours > 0){
			$this.append("<div class='columnaCountDown'><div class='cdn'>"+ hours +"</div><div class='cdt'>Hrs</div></div><div class='columnaCountDown'><div class='cdn'>" + minutes + "</div> <div class='cdt'>Min</div></div><div class='columnaCountDown'><div class='cdn'>" + seconds + "</div> <div class='cdt'>Seg</div></div>");
		}else{
			$this.append("<div class='columnaCountDown'><div class='cdn'>"+ minutes + "</div><div class='cdt'>Min</div></div><div class='columnaCountDown'><div class='cdn'>" + seconds + "</div><div class='cdt'>Seg</div></div><div class='columnaCountDown'><div class='cdn'>" + seconds + "</div> <div class='cdt'>Seg</div></div>");	
		}
		
		if (distance < 0) {
			$this.empty();
			$this.append("<div class='ofertaTerminada'>TERMINADA</div>");
		}
	}, 1000);
});

$('#spinner').on( "spinstop", function(e){
	var cant 	= $(this).val();
	var id 	= $(this).parent().parent().attr('rel');

	$('.precio-ficha').load('ajax/precio_ficha.php?cant='+cant+'&id='+id);

	//console.log('cant: ' + cant + ' id: ' + id);
});

var popup = setInterval(IsVisible, 3000);

function IsVisible(){
	if ($('.popup-news').is(':visible')){
		$(".bg-popup").click(function() {
			var nombre_popup = $('.bg-popup').attr('data-cliente');
			$('.bg-popup').fadeOut();
		    $('.popup-news').fadeOut();
		    Cookies.set(nombre_popup,'Newsletter (No Suscrito)',{expires: 14});
		});
	}
	clearInterval(popup);
}

$('.popup-news').click(function (e) {
    e.stopPropagation();
});
$(".popup-news .contenido > .close").on('click', function(e){
	var nombre_popup = $('.bg-popup').attr('data-cliente');
	Cookies.set(nombre_popup,'Newsletter (No Suscrito)',{expires: 14});
	$('.bg-popup').fadeOut();
    $('.popup-news').fadeOut();
})

$('#suscribe-newsletter').on('click', function(e){

	e.preventDefault();
	var nombre = $('#nombre-suscribe').val();
	var email = $('#email-suscribe').val();
	var nombre_popup = $('.bg-popup').attr('data-cliente');

	if (nombre.trim() != '' && email.trim() != '') {
		$.ajax({
			url: 'ajax/popup.php',
			type: 'post',
			data: {nombre: nombre, email: email},
			beforeSend: function(){
				$('#suscribe-newsletter').html('ESPERE POR FAVOR...');
			},
			success: function(res){
				Cookies.set(nombre_popup,'Newsletter Suscrito',{expires: 90});
				$('#suscribe-newsletter').html('SUSCRIBIRME');
				$('.bg-popup').fadeOut();
	    		$('.popup-news').fadeOut();
	    		Swal.fire('','Gracias por suscribirte.','success');
			}
		})
	}else{
		Swal.fire('','Debe completar todos los campos', 'error');
	}
})

/*Eliminar direcciones guardadas*/	
$(".btnEliminaUsuario").click(function(){
	var id = $(this).attr("rel");
	console.log(id);
	$.ajax({
		url      : "ajax/eliminaUsuario.php",
		type     : "post",
		async    : true,
		data 	 : {id : id},
		success  : function(resp){
			console.log(resp);
			if(resp == 1){
				console.log("Usuario eliminado");
				Swal.fire({
					  icon: 'success',
					  text: 'Usuario eliminado.'
					});
					$("#esclavo_"+id).hide(300);
				
				} else {
					console.log("No fue posible eliminar a este usuario.");
						Swal.fire({
						  icon: 'error',
						  text: 'No fue posible eliminar a este usuario.'
						});
					}
		}
	});

});



$(".btnFormServ").click(function(){
		var nombre = $("#nombre").val();
        var telefono = $('#telefono').val();
		var email = $("#email").val();
		var mensaje = $("#mensaje").val();
		
		if(nombre != '' && email != '' && telefono != '' && mensaje !=''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				//alertify.error("Email invalido");
				Swal.fire('','Email invalido','error'); 
			} else {
				/* Correo valido */
				$("#formServicios").submit();
			}; /*Fin validacion email */
		} else {
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			Swal.fire('','Faltan campos por completar','error'); 
			if(nombre == '' ){
				$("#nombre").addClass("campo_vacio");
			} else {
				$("#nombre").removeClass("campo_vacio");
			};

			if(telefono == '' ){
				$("#telefono").addClass("campo_vacio");
			} else {
				$("#telefono").removeClass("campo_vacio");
			};
			
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(mensaje == '' ){
				$("#mensaje").addClass("campo_vacio");
			} else {
				$("#mensaje").removeClass("campo_vacio");
			};

			
			
		};
		
	});/*fin registro paso 1*/


	$(".lineasDestacadas").each(function(index) {
        if (index == 4) {
            $(this).children().children().css("color", "#009be5")
            // console.log(index+"<br>");   
        }
    });