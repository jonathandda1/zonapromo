$(document).ready(function(){

    $('.btnForm').on("click", function(){
        $(".cont_loading").fadeIn(200);
        var nombre = $("#nombre").val();
        var email = $("#email").val();
        var telefono = $("#telefono").val();
        var comentarios = $("#mensaje").val();
        if (nombre != "" && email != "" && telefono != "" && comentarios != "") {

            if (email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
                //console.error("Correo inválido.");
                $(".cont_loading").fadeOut(200);
                Swal.fire({icon: 'error', title: 'Oops...', text: 'Email invalido!' });
                $("#email").addClass("incompleto");
            }else if (telefono.length < 9) {
                $(".cont_loading").fadeOut(200);
                Swal.fire({ icon: 'error', title: 'Oops...', text: 'El teléfono debe tener un mínimo de 9 números!' });
                $("#telefono").val('');
            }else{
                grecaptcha.ready(function() {
                    grecaptcha.execute('6LfZoiIeAAAAALbEyRM_H7eXP3O2X86UJ06wA3lV', {action: 'validarUsuario'}).then(function(token) {
                        $('.formularios').prepend('<input type="hidden" name="token" value="'+token+'"></input>');
                        $('.formularios').prepend('<input type="hidden" name="action" value="validarUsuario"></input>');
                        $('.formularios').submit();
                    });
                });
            }            
        } else {
            $(".cont_loading").fadeOut(200);
            Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debes rellenar todos los campos!' });
            if (nombre != '') { $("#nombre").removeClass("incompleto");
            } else { $("#nombre").addClass("incompleto"); };
            
            if (comentarios != '') { $("#mensaje").removeClass("incompleto");
            } else { $("#mensaje").addClass("incompleto"); };
            
            if (email != '') { $("#email").removeClass("incompleto");
            } else { $("#email").addClass("incompleto"); };
            
            if (telefono != '') { $("#telefono").removeClass("incompleto");
            } else { $("#telefono").addClass("incompleto"); };
        }
    });

    $("#btnFormContacto").click(function(){
        $(".cont_loading").fadeIn(200);
		var nombre = $("#nombre").val();
        var telefono = $('#telefono').val();
		var email = $("#email").val();
		var mensaje = $("#mensaje").val();
		
		if(nombre != '' && email != '' && telefono != '' && mensaje !=''){
			if(email.indexOf('@', 0) == -1 || email.indexOf('.', 0) == -1) {
				/* Correo invalido */
				$("#email").addClass("campo_vacio");
				//alertify.error("Email invalido");
                $(".cont_loading").fadeOut(200);
				Swal.fire('','Email invalido','error'); 
			} else {
				/* Correo valido */
                grecaptcha.ready(function() {
                    grecaptcha.execute('6LfZoiIeAAAAALbEyRM_H7eXP3O2X86UJ06wA3lV', {action: 'validarUsuario'}).then(function(token) {
                        $('#formContacto').prepend('<input type="hidden" name="token" value="'+token+'"></input>');
                        $('#formContacto').prepend('<input type="hidden" name="action" value="validarUsuario"></input>');
                        $("#formContacto").submit();
                    });
                });
			}; /*Fin validacion email */
		} else {
            $(".cont_loading").fadeOut(200);
		/* indico que todos los campos deben estar llenos */
			//alertify.error("Faltan campos por completar");
			Swal.fire('','Faltan campos por completar','error'); 
			if(nombre == '' ){
				$("#nombre").addClass("campo_vacio");
			} else {
				$("#nombre").removeClass("campo_vacio");
			};

			if(telefono == '' ){
				$("#telefono").addClass("campo_vacio");
			} else {
				$("#telefono").removeClass("campo_vacio");
			};
			
			if(email == '' ){
				$("#email").addClass("campo_vacio");
			} else {
				$("#email").removeClass("campo_vacio");
			};
			
			if(mensaje == '' ){
				$("#mensaje").addClass("campo_vacio");
			} else {
				$("#mensaje").removeClass("campo_vacio");
			};
            

			
			
		};
		
	});/*fin registro paso 1*/





});