$(document).ready(function () {
  $("#empresaCotizar").keyup(function () {
    let empresa = $(this).val();
    if (empresa.trim() !== "") {
      $.ajax({
        url: "ajax/autocompletar.php",
        method: "POST",
        data: { empresa: empresa },
        success: function (data) {
          //    console.log(data);
          $("#empresasList").fadeIn();
          $("#empresasList").html(data);
        },
      });
    }else{
      $("#empresasList").fadeOut();
    }
  });
  $(document).on("click", ".liListaEmpresa", function () {
    $("#empresaCotizar").val($(this).text());
    $("#rutEmpresaCotizar").val($(this).attr("rutEmpresa"));
    $("#empresasList").fadeOut();
  });
  $(document).on("blur", "#empresaCotizar", function () {
    $("#empresasList").fadeOut();
  });
});


/*
Funciones necesarias para agregar al carro, eliminar un producto del carro o restar un producto
*/
$(function () {
  $("#btnCotizacion").click(function () {
    var nombre = $("#nombreCotizar").val();
    var apellidos = $("#apellidoCotizar").val();
    var rutCotizar = $("#rutCotizar").val();
    var telefono = $("#telefonoCotizar").val();
    var email = $("#emailCotizar").val();
    var empresa = $("#empresaCotizar").val();
    var rutEmpresaCotizar = $("#rutEmpresaCotizar").val();

    if (
      nombre != "" &&
      apellidos != "" &&
      telefono != "" &&
      email != "" &&
      empresa != "" &&
      rutCotizar != "" &&
      rutEmpresaCotizar != ""
    ) {
      if (email.indexOf("@", 0) == -1 || email.indexOf(".", 0) == -1) {
        Swal.fire("", "Ingrese un correo válido!", "error");
        $("#emailCotizar").val("");
        $("#emailCotizar").addClass("campo_vacio");
      } else if (telefono.length < 9) {
        $("#emailCotizar").removeClass("campo_vacio");
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "El teléfono debe tener un mínimo de 9 números!",
        });
        $("#telefonoCotizar").val("");
        $("#telefonoCotizar").addClass("campo_vacio");
      } else {
        $("#formCotizacion").submit();
      }
    } else {
      Swal.fire("Oops...", "Faltan campos por completar!", "error");
      if (nombre == "") {
        $("#nombreCotizar").addClass("campo_vacio");
      } else {
        $("#nombreCotizar").removeClass("campo_vacio");
      }
      if (apellidos == "") {
        $("#apellidoCotizar").addClass("campo_vacio");
      } else {
        $("#apellidoCotizar").removeClass("campo_vacio");
      }
      if (telefono == "") {
        $("#telefonoCotizar").addClass("campo_vacio");
      } else {
        $("#telefonoCotizar").removeClass("campo_vacio");
      }
      if (email == "") {
        $("#emailCotizar").addClass("campo_vacio");
      } else {
        $("#emailCotizar").removeClass("campo_vacio");
      }
      if (empresa == "") {
        $("#empresaCotizar").addClass("campo_vacio");
      } else {
        $("#empresaCotizar").removeClass("campo_vacio");
      }
      if (rutCotizar == "") {
        $("#rutCotizar").addClass("campo_vacio");
      } else {
        $("#rutCotizar").removeClass("campo_vacio");
      }
      if (rutEmpresaCotizar == "") {
        $("#rutEmpresaCotizar").addClass("campo_vacio");
      } else {
        $("#rutEmpresaCotizar").removeClass("campo_vacio");
      }
    }
  });
});

$("#nuevaCotizacion").on("click", function () {
  let rutEmpresa = $("#rutEmpresa").val();
  let rutUsuario = $("#selectUsuariosEmpresas").val();
  if (
    rutEmpresa != "" &&
    rutEmpresa != null &&
    rutUsuario != "" &&
    rutUsuario != null
  ) {
    $("#formNuevaCotizacion").submit();
  } else {
    Swal.fire(
      "Oops...",
      "Debes elejir una empresa y luego un usuario",
      "error"
    );
  }
});
$("#buscarProductosCotizar,#buscarProductosCotizar2").on("click", function () {
  let busqueda = $("#busquedaCotizar").val();
  let busqueda2 = $("#busquedaCotizar2").val();
  if (busqueda2 != "" && busqueda2 != null) {
    busqueda = busqueda2;
  }
  let idCotizacion = $(this).attr("idCotizacion");
  let para = $(this).attr("para");
  let idProd = $("#idProdEnvio").val();
  if (busqueda != "") {
    $.ajax({
      url: "ajax/busquedaProductos.php",
      method: "post",
      dataType: "json",
      data: {
        busqueda: busqueda,
        idCotizacion: idCotizacion,
        para: para,
        idProd: idProd,
      },
      success: function (resp) {
        // console.log(resp.filaProducto);
        if (resp.para == "sustituir") {
          $(".contProductosBuscadosSustituir").html(resp.filaProducto);
        } else {
          $(".contProductosBuscados").html(resp.filaProducto);
        }
      },
    });
  } else {
    Swal.fire("Oops...", "Debes escribir un nombre o un sku", "error");
  }
});

$(".contProductosBuscados").on("change", ".selectCantidadAgregar", function () {
  let cant = parseInt($(this).val());
  if (cant == 11) {
    //$(".cantidad .selector").remove();
    $(this)
      .parent(".cantidadAgregar")
      .html(
        "<input type='text' name='cantidad' class='campoCantidad cantidadBuscados'>"
      );
    $(this).numeric();
  }
});

$(".contProductosBuscados, .contProductosBuscadosSustituir").on(
  "click",
  "#btnAgregarProductoTabla",
  function () {
    let cant = $(this)
      .parent()
      .parent()
      .children(".contInfoAgregar")
      .children(".cantidadAgregar")
      .children(".cantidadBuscados")
      .val();
    let id = $(this).attr("idProducto");
    let idCotizacion = $(this).attr("idCotizacion");
    let para = $(this).attr("para");
    let sustituir = $(this).attr("prodsustituir");
    let = logo = $(this)
      .parent()
      .parent()
      .children(".contLogoColor")
      .children(".contLabelSelect")
      .children(".logo-producto")
      .val();
    let = color = $(this)
      .parent()
      .parent()
      .children(".contLogoColor")
      .children(".contLabelSelect")
      .children(".color-producto")
      .val();
    if (color == null || color == "") {
      color = 0;
    }
    // console.log(logo+" "+color);
    $.ajax({
      url: "ajax/agregarProductosTablas.php",
      method: "post",
      data: {
        cantidad: cant,
        idProducto: id,
        idCotizacion: idCotizacion,
        logo: logo,
        color: color,
        para: para,
        sustituir: sustituir,
      },
      success: function (respuesta) {
        // console.log(respuesta);
        location.reload();
      },
    });
  }
);

$("#cotizarProducto").on("click", ".botonSinStock", function () {
  let idCotProd = $(this).attr("idCotizacion");
  let accion = $(this).attr("accion");
  let estadoIdActual = $('#estadoIdCotizacion').val();
  if (estadoIdActual == 3) {
    Swal.fire(
      "Oops...",
      "Esta cotización ya fue vendida por lo que no puede ser editada.",
      "warning"
    );
  }else{
  // console.log(logo+" "+color);
    $.ajax({
      url: "ajax/accionBotones.php",
      method: "post",
      data: { idCotProd: idCotProd, accion: accion },
      success: function () {
        location.reload();
      },
    });
  }
});

$("#cotizarProducto").on("click", ".modalSustitucion", function () {
  $("#botonSustituir").click();
  let idProd = $(this).attr("idProd");
  $("#idProdEnvio").val(idProd);
  // $("#modalSustituirProducto").css("display","inline-block");
  // $("body").addClass("fancybox-active compensate-for-scrollbar");
});

$("#estadosSelect").on("change", function () {
  let oc = $("#oc").val();
  let estado_id = $("#estadosSelect").val();
  let estadoIdActual = $('#estadoIdCotizacion').val();
  if (estadoIdActual == 3) {
    Swal.fire(
      "Oops...",
      "Esta cotización ya fue vendida por lo que no puede ser editada.",
      "warning"
    );
  }else{
    $.ajax({
      url: "ajax/actualizarEstado.php",
      method: "post",
      data: { oc: oc, estado_id: estado_id },
      success: function (respuesta) {
        if (respuesta == "exito") {
          Swal.fire("Éxito", "el estado fue cambiado!", "success").then(
            (result) => {
              if (result.isConfirmed) {
                location.reload();
              }
            }
          );
        } else {
          Swal.fire(
            "Oops...",
            "ocurrio un error al intentar cambiar el estado",
            "warning"
          );
        }
      },
    });
  }
});

$("#notaEspecial").on("blur", function () {
  let nota = $(this).val();
  let oc = $(this).attr("oc");
  let estadoIdActual = $('#estadoIdCotizacion').val();
  if (estadoIdActual == 3) {
    Swal.fire(
      "Oops...",
      "Esta cotización ya fue vendida por lo que no puede ser editada.",
      "warning"
    );
  }else{
    $.ajax({
      url: "ajax/actualizarEstado.php",
      method: "post",
      data: { oc: oc, nota: nota },
      success: function (respuesta) {
        if (respuesta == "exito") {
          Swal.fire("Éxito", "La nota especial fue actualizada!", "success").then(
            (result) => {
              if (result.isConfirmed) {
                location.reload();
              }
            }
          );
        } else {
          Swal.fire(
            "Oops...",
            "ocurrio un error al intentar actualizar la nota especial",
            "warning"
          );
        }
      },
    });
  }
});
$("#datosOrden,#formaPago,#datosBanco").on("blur", function () {
  let nota = $(this).val();
  let idVendedor = $(this).attr("idVendedor");
  let campo = $(this).attr("campo");
  let estadoIdActual = $('#estadoIdCotizacion').val();
  if (estadoIdActual == 3) {
    Swal.fire(
      "Oops...",
      "Esta cotización ya fue vendida por lo que no puede ser editada.",
      "warning"
    );
  }else{
    $.ajax({
      url: "ajax/actualizarEstado.php",
      method: "post",
      data: { idVendedor: idVendedor, nota: nota, campo: campo },
      success: function (respuesta) {
        console.log(respuesta);
        if (respuesta == "exito") {
          Swal.fire("Éxito", "Los datos se actualizaron!", "success").then(
            (result) => {
              if (result.isConfirmed) {
                location.reload();
              }
            }
          );
        } else {
          Swal.fire(
            "Oops...",
            "ocurrio un error al intentar actualizar",
            "warning"
          ).then(
            (result) => {
              if (result.isConfirmed) {
                location.reload();
              }
            }
          );
        }
      },
    });
  }
});

$("#example").on("click", "#sinVendedor", function () {
  let oc = $(this).attr("oc");
  let idVendedor = $(this).attr("idVEndedor");
  let rutEmpresa = $(this).attr("rutEmpresa");
  let accion = "empresa_vendedor";
  let url = $("#urlBase").val();
  // alert(url)
  // // console.log(logo+" "+color);
  $.ajax({
    url: "ajax/accionBotones.php",
    method: "post",
    data: {
      oc: oc,
      accion: accion,
      idVendedor: idVendedor,
      rutEmpresa: rutEmpresa,
    },
    success: function (resp) {
      if (resp == "ok") {
        window.location.href =
          "cotizaciones-vendedor?ok=Fuiste asociado como vendedor de la empresa";
      } else if (resp == "existe") {
        window.location.href =
          "cotizaciones-vendedor?error=La empresa ya se encuentra asociada a otro vendedor";
      } else {
        window.location.href =
          "cotizaciones-vendedor?error=Ocurrio un problema al intentar asociar tu id de vendedor con la empresa";
      }
      // location.reload();
    },
  });
});

