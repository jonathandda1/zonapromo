$(function(){
    
    $(".selectCantidad").change(function(){
        var cant = parseInt($("#cantidad").val());
        console.log(cant);
        if(cant == 11){
            //$(".cantidad .selector").remove();
            $(".cantidad").html("<input type='text' name='cantidad' id='cantidad' class='campoCantidad'>");
            $("#cantidad").numeric();
        }
    });

$(document).ready(function(){
	/*Agrego desde el fixed o desde el boton*/
	$('#btn_agregarCarro, #btnAgregarFixed').click(function(){
		$(".cont_loading").fadeIn(200);
		let id_pro = $(this).attr('rel');
		let cantidad = parseInt($("#cantidad").val());
		let color = $('#color-producto').val();
		let cantColor = $('#logo-producto').val();
		let cantActual = $("#elementosCarro .cantItems").html();
        
        var instalacion = parseInt($('input:checkbox[name=instalacion]:checked').val());
		// if(instalacion > 0){
		// 	instalacion = instalacion;
		// 	} else {
		// 		instalacion = 0;
		// 		}
		// console.log("armado"+instalacion);	
        
        
		console.log(cantidad);	
			// $.ajax({
			// 	type: 'POST',
			// 	url: 'ajax/ajax_validar_stock.php',
			// 	data: {id:id_pro, qty:cantidad},
			// 	cache: false,
			// 	success:function(resp) {
			// 		console.log("resp stock: "+resp);
			// 		if(resp >= 0){
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							data: {id:id_pro, action:"add", qty:cantidad, color:color, cantColor: cantColor},
							cache: false,
							 success:function(resp) {
								 console.log("respuesta agregar"+resp);
								var cantTotal = parseInt(cantActual) + parseInt(cantidad);
								$(".totalHeader").load("tienda/totalCart.php");
								$("#elementosCarro .cantItems").load("tienda/qtyTotal.php"); 
                                 
                                 /*agrego el id en la session de armado*/
								// if(instalacion > 0){
								// 	$.get("tienda/addArmado.php", {action: "add", id: instalacion});
								// 	};
								/*agrego el id en la session de armado*/
                                 
								$.ajax({
									type: 'GET',
									url: 'pags/popUpAddToCart.php',
									data: {id:id_pro, qty:cantidad},
									cache: false,
									 success:function(resp){
										 $(".cont_loading").fadeOut(200);
										 $("#popUp").html(resp);
										 $(".fondoPopUp").fadeIn(100);
									 }
								 });
							}
						});
			// 		}else{
			// 			$(".cont_loading").fadeOut(200);
			// 			Swal.fire({
			// 				  icon: 'error',
			// 				  title: 'Stock insuficiente',
			// 				  text: 'No fue posible agregar tu producto por falta de stock'
			// 				})
			// 		};
			// 	}
			// });
		
	});
});
	
	
	
	
	/*agrega desde mis pedidos*/
	
	$('.btnComprarAhora').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var cantidad = parseInt(1);
		var cantActual = $("#elementosCarro .cantItems").html();	
		console.log(cantidad);	
			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_validar_stock.php',
				data: {id:id_pro, qty:cantidad},
				cache: false,
				success:function(resp) {
					console.log("resp stock: "+resp);
					if(resp >= 0){
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							data: {id:id_pro, action:"add", qty:cantidad},
							cache: false,
							 success:function(resp) {
								 console.log("respuesta agregar"+resp);
								var cantTotal = parseInt(cantActual) + parseInt(cantidad);
								$(".totalHeader").load("tienda/totalCart.php");
								$("#elementosCarro .cantItems").load("tienda/qtyTotal.php"); 
								$.ajax({
									type: 'GET',
									url: 'pags/popUpAddToCart.php',
									data: {id:id_pro, qty:cantidad},
									cache: false,
									 success:function(resp){
										 $(".cont_loading").fadeOut(200);
										 $("#popUp").html(resp);
										 $(".fondoPopUp").fadeIn(100);
									 }
								 });
							}
						});
					}else{
						$(".cont_loading").fadeOut(200);
						Swal.fire({
							  icon: 'error',
							  title: 'Stock insuficiente',
							  text: 'No fue posible agregar tu producto por falta de stock'
							})
					};
				}
			});
		
	});
	
	/*===================================================
	=            Haciendo de nuevo un pedido            =
	===================================================*/	
	$('.btnVolverAPedir').click(function(){
	$(".cont_loading").fadeIn(200);
	var id_pro = $(this).attr('idProductos');
	var cantidad = $(this).attr('cantProductos');
	let cantidad2 = cantidad.split(',').map(Number);
	var totalCant = cantidad2.reduce((a, b) => a + b, 0);
	var cantActual = $("#elementosCarro .cantItems").html();
		// Validamos stock
		$.ajax({
			type: 'POST',
			url: 'ajax/ajax_stock_pedir.php',
			data: {id:id_pro, qty:cantidad},
			cache: false,
			success:function(resp) {
				if(resp >= 0){
					// Agregamos productos al carro
					$.ajax({
						type: 'GET',
						url: 'tienda/addCarro.php',
						data: {idPr:id_pro, action:"addPedido", qtyPr:cantidad},
						cache: false,
						 success:function(resp) {
							 console.log("respuesta agregar"+resp);
							var cantTotal = parseInt(cantActual) + parseInt(totalCant);
							$(".totalHeader").load("tienda/totalCart.php");
							$("#elementosCarro .cantItems").load("tienda/qtyTotal.php");
							$(".cantItems").load("tienda/qtyTotal.php");
							// Mostramos el carro popUp 
							$.ajax({
								type: 'GET',
								url: 'pags/popUpAddToCart.php',
								data: {id:id_pro, qty:totalCant},
								cache: false,
								 success:function(resp){
									 $(".cont_loading").fadeOut(200);
									 $("#popUp").html(resp);
									 $(".fondoPopUp").fadeIn(100);
								 }
							 });
						}
					});
				}
				else{
					$(".cont_loading").fadeOut(200);
					Swal.fire({
						  icon: 'error',
						  title: 'Stock insuficiente',
						  text: 'No fue posible agregar tu pedido por falta de stock'
						})
				};
			}
		});
		
	});	
	/*=====  End of Haciendo de nuevo un pedido  ======*/

    /*===================================================
	=            Haciendo de nuevo Cotizacion           =
	===================================================*/	
	$('.volverCotizar').on("click", function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('idProductos');
		var cantidad = $(this).attr('cantProductos');
		var color = $(this).attr('idColores');
		var logo = $(this).attr('idLogos');
		let cantidad2 = cantidad.split(',').map(Number);
		var totalCant = cantidad2.reduce((a, b) => a + b, 0);
		var cantActual = $("#elementosCarro .cantItems").html();
			// Validamos stock
			// $.ajax({
			// 	type: 'POST',
			// 	url: 'ajax/ajax_stock_pedir.php',
			// 	data: {id:id_pro, qty:cantidad},
			// 	cache: false,
			// 	success:function(resp) {
			// 		if(resp >= 0){
						// Agregamos productos al carro
						$.ajax({
							type: 'GET',
							url: 'tienda/addCarro.php',
							data: {idPr:id_pro, action:"addCotizar", qtyPr:cantidad, color:color , cantColor:logo},
							cache: false,
							 success:function(resp) {
								 console.log("respuesta agregar"+resp);
								var cantTotal = parseInt(cantActual) + parseInt(totalCant);
								$(".totalHeader").load("tienda/totalCart.php");
								$("#elementosCarro .cantItems").load("tienda/qtyTotal.php");
								$(".cantItems").load("tienda/qtyTotal.php");
								// Mostramos el carro popUp 
								$.ajax({
									type: 'GET',
									url: 'pags/popUpAddToCart.php',
									data: {id:id_pro, qty:totalCant},
									cache: false,
									 success:function(resp){
										 $(".cont_loading").fadeOut(200);
										 $("#popUp").html(resp);
										 $(".fondoPopUp").fadeIn(100);
									 }
								 });
							}
						});
					// }
			// 		else{
			// 			$(".cont_loading").fadeOut(200);
			// 			Swal.fire({
			// 				  icon: 'error',
			// 				  title: 'Stock insuficiente',
			// 				  text: 'No fue posible agregar tu pedido por falta de stock'
			// 				})
			// 		};
			// 	}
			// });
			
		});	
		/*=====  End of Haciendo de nuevo Cotizacion ======*/
    
    
    
    
    
    /*Agrego varios a la vez*/
	$('#btnAgregarJuntos').click(function(){
		$(".cont_loading").fadeIn(200);
		var id_pro = $(this).attr('rel');
		var cantidad = 1;
		var disponible = 0;
        var ids = id_pro.split(",");
        var cantTotal = ids.length;
        //console.log(ids);
        
        $.each(ids, function( index, value ) {
          /*console.log( index + ": " + value );*/
          var idProducto = parseInt(value);
            $.ajax({
				type: 'POST',
				url: 'ajax/ajax_validar_stock.php',
				data: {id:idProducto, qty:1},
				cache: false,
                async: false,
				success:function(resp) {
                    /*console.log(value);*/
                    if(resp > 0){
                        disponible = disponible + 1;
                        /*console.log("id" +value+ "correcto .....  disponible : " + disponible);*/
                    } else {
                        disponible = disponible;
                        /*console.log("id" +value+ "con problemas");*/
                    }
                    
                    if(index === (cantTotal - 1)){
                        /*console.log("Disponible xxx" + disponible);*/
                        /*aca debo agregar al carro*/
                        if(disponible == cantTotal){
                            console.log("stock disponible: " + disponible);
                            /*ejecuto proceso de agregar al carro*/
                            $.each(ids, function( index2, value2 ) {
                                var idProducto2 = parseInt(value2);
                                $.ajax({
                                    type: 'GET',
                                    url: 'tienda/addCarro.php',
                                    data: {id:idProducto2, action:"add", qty:1},
                                    cache: false,
                                    async: false,
                                     success:function(resp) {
                                        /*console.log("respuesta agregar"+resp);*/
                                        if(index2 === (cantTotal - 1)){
                                            console.log("entro en la condicion");
                                            $("#popUp").load("pags/popUpAddToCart.php", function(){
                                                $(".fondoPopUp").fadeIn(100);
                                                $(".cont_loading").fadeOut(200);
                                            });
                                            $("#elementosCarro .cantItems").load("tienda/qtyTotal.php");
                                        }
                                        
                                    }
                                });
                            });/*fin del segundo ciclo*/
                            
                        } else {
                            /*debo mostrar mensaje de stock no disponible*/
                            console.log("stock no disponible: " + disponible);
                            $(".cont_loading").fadeOut(200);
                            Swal.fire({
                                  icon: 'error',
                                  title: 'Stock insuficiente',
                                  text: 'No fue posible agregar tus productos por falta de stock'
                                })
                        }
                        
                    }
					
                }
            });/*fin primer ajax*/
            
            
        });/*fin del primer ciclo*/
        
    });
    /*Agrego varios a la vez*/
    
    
	
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//cambiar productos al clickear en el color
 
	$("a.varianteColor").click(function(){
		$("a.varianteColor").removeClass("currentColor");
        $(this).addClass("currentColor");
        var productoID = $(this).attr("rel");
        var colorID = $(this).attr("rel2");
        
		$.ajax({
			url      : "ajax/cambioProducto.php",
			type     : "post",
			async    : true,
            dataType: "json",
			data 	 : {id : productoID, color:colorID},
			success  : function(resp){
				console.log("asdasdasd "+resp);
				var json_string = JSON.stringify(resp);
                var obj = $.parseJSON(json_string);
                
                $('.codigo').html("SKU: "+obj.sku);
                $('.precio-ficha').html(obj.precio);
                $('#contDesc').html(obj.porcentajeDescuento);
                $('#imagenesFicha').html(obj.galeria);
                
                $('#contTallasFicha').html(obj.tallas);
                $("select").uniform();
                
                $("#btn_agregarCarro").attr("rel", obj.idCambio);
                $("#btnAgregarFixed").attr("rel", obj.idCambio);
                
                /*Parche*/
                $("#talla").change(function(){
                    var productoID = $(".varianteColor.currentColor").attr("rel");
                    var colorID = $(".varianteColor.currentColor").attr("rel2");
                    var tallaID = $(this).val();
                    $.ajax({
                        url      : "ajax/cambioProductoTalla.php",
                        type     : "post",
                        async    : true,
                        dataType: "json",
                        data 	 : {id : productoID, color:colorID, talla: tallaID},
                        success  : function(resp){
                            var json_string = JSON.stringify(resp);
                            var obj = $.parseJSON(json_string);
                            console.log(obj.sku+" - "+obj.precio);
                            $('.codigo').html("SKU: "+obj.sku);
                            $('.precio-ficha').html(obj.precio);
                            $('#contDesc').html(obj.porcentajeDescuento);
                            $('#imagenesFicha').html(obj.galeria);
                            $("#btn_agregarCarro").attr("rel", obj.idCambio);
                            $("#btnAgregarFixed").attr("rel", obj.idCambio);

                        }
                    })
                });
                /*Parche*/
                
            }
		})
	});


    
    
    
//cambiar productos al clickear en el color
 
	$("#talla").change(function(){
		var productoID = $(".varianteColor.currentColor").attr("rel");
        var colorID = $(".varianteColor.currentColor").attr("rel2");
        var tallaID = $(this).val();
        $.ajax({
			url      : "ajax/cambioProductoTalla.php",
			type     : "post",
			async    : true,
            dataType: "json",
			data 	 : {id : productoID, color:colorID, talla: tallaID},
			success  : function(resp){
				var json_string = JSON.stringify(resp);
                var obj = $.parseJSON(json_string);
                console.log(obj.sku+" - "+obj.precio);
                $('.codigo').html("SKU: "+obj.sku);
                $('.precio-ficha').html(obj.precio);
                $('#contDesc').html(obj.porcentajeDescuento);
                $('#imagenesFicha').html(obj.galeria);
                $("#btn_agregarCarro").attr("rel", obj.idCambio);
                $("#btnAgregarFixed").attr("rel", obj.idCambio);
                
            }
		})
	});
    
    

//funcion para dar formato de moneda a los numeros
    function formato_numeros(valor){
        var valorSinDecinal = parseInt(valor);//quito los decimales
        var valorString = valorSinDecinal.toString();//lo convierto en cadena
        var	Nseparados = valorString.split(""); //convierto la cadena en un arrglo
        var valor2 = "";
        //invierto el arreglo
        $.each(Nseparados, function( i, l){
            valor2 = l + valor2;
        });

        var valor3 = "";
        // lo armo invertido y con puntos
        $.each(valor2, function( i, l){
            //console.log(i +" - "+ l);
            if(i == 3 || i == 6 || i == 9 || i == 12 || i == 15 || i == 18){
                valor3 =  l + "." + valor3 ;
            } else {
                valor3 = l + valor3;
            }
        });

        return "$" + valor3;
    }
    
	// $('#color-producto').on("change", function(){
		// let idProd = $('option:selected', this).attr("idProd");
		// let sku = $('option:selected', this).attr("sku");
		// let cotizacionMinima = $('option:selected', this).attr("cotMin");
		// let imagen = $('option:selected', this).attr("imagen");
		// alert(imagen);
		// if (idProd == null || sku == null) {
		// 	location.reload();
		// }
		// // alert(idProd);
		// $("#img-grande").attr("src","imagenes/productos_detalles/"+imagen);
		// $("#enlaceImagen").attr("href","imagenes/productos_detalles/"+imagen);
		// $('#btn_agregarCarro').attr('rel', idProd);
		// $('#btnAgregarFixed').attr('rel', idProd);
		// $('.codigo').html("Código: "+sku);
		// $('#cotMin').html("Cotización mínima de " +cotizacionMinima+  "productos*");
	// });
    
    
    
    
    
    
    
    
	
	
});